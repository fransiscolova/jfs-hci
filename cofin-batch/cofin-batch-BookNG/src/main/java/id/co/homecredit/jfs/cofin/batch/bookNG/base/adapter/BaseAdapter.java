package id.co.homecredit.jfs.cofin.batch.bookNG.base.adapter;

import org.hibernate.Session;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;

/**
 * Base Adapter For BookNG
 *
 * @author denny.afrizal01
 *
 */
@Repository
@SuppressWarnings("unchecked")
public class BaseAdapter{

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sfJFS=sessionFactory;
    }

    public boolean saveObject(Object object){
        Session session = sfJFS.openSession();
        try{
            session.beginTransaction();
            session.save(object);
            session.getTransaction().commit();
        }catch(HibernateException p){
            session.getTransaction().rollback();
            System.out.println(p);
            return false;
        }finally{
            session.close();
        }
        return true;
    }

    public boolean updateObject(Object object){
        Session session = sfJFS.openSession();
        try{
            session.beginTransaction();
            session.update(object);
            session.getTransaction().commit();
        }catch(HibernateException p){
            session.getTransaction().rollback();
            System.out.println(p);
            return false;
        }finally{
            session.close();
        }
        return true;
    }

    public Integer getValueBetweenDate(String startDate,String endDate){
        Session session = sfJFS.openSession();
        Query query = session.createSQLQuery("select ceil(date '"+endDate+"' - date '"+startDate+"') as value from dual");
        query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        List<Map<String,Object>> list = query.list();
        session.close();
        return Integer.parseInt(list.get(0).get(ColumnName.VALUE).toString());
    }

    public Date addMonths(String tanggal,Integer bulan)throws Exception{
        Session session = sfJFS.openSession();
        Query query = session.createSQLQuery("select add_months(date '"+tanggal+"',"+bulan+") as value from dual");
        Timestamp list = (Timestamp) query.uniqueResult();
        session.close();
        return new Date(list.getTime());
    }

    public boolean updateObjectUsingQuery(String query){
        Session session = sfJFS.openSession();
        try{
            session.beginTransaction();
            SQLQuery xquery = session.createSQLQuery(query);
            xquery.executeUpdate();
            session.getTransaction().commit();
        }catch(HibernateException j){
            session.getTransaction().rollback();
            System.out.println(j);
            return false;
        }finally{
            session.close();
        }
        return true;
    }

}
