package id.co.homecredit.jfs.cofin.batch.bookNG.base.constant;

/**
 * Column Constant
 *
 * @author denny.afrizal01
 *
 */
public class ColumnConstant{

    public static class ColumnName{
        public static final String TEXT_CONTRACT_NUMBER="TEXT_CONTRACT_NUMBER";
        public static final String DUE_DATE="DUE_DATE";
        public static final String PREV_BALANCE="PREV_BALANCE";
        public static final String PENALTY="PENALTY";
        public static final String OUTSTANDING_PRINCIPAL="OUTSTANDING_PRINCIPAL";
        public static final String FEE="FEE";
        public static final String BALANCE="BALANCE";
        public static final String PRINCIPAL="PRINCIPAL";
        public static final String INTEREST="INTEREST";
        public static final String VER="VER";
        public static final String PART_INDEX="PART_INDEX";
        public static final String IS_ACTIVE="IS_ACTIVE";
        public static final String ID="ID";
        public static final String INCOMING_PAYMENT_ID="INCOMING_PAYMENT_ID";
        public static final String DATE_INSTALMENT="DATE_INSTALMENT";
        public static final String INSTALMENT_NUMBER="INSTALMENT_NUMBER";
        public static final String PAYMENT_DATE="PAYMENT_DATE";
        public static final String AMT_PRINCIPAL="AMT_PRINCIPAL";
        public static final String AMT_INTEREST="AMT_INTEREST";
        public static final String AMT_PENALTY="AMT_PENALTY";
        public static final String AMT_OVERPAYMENT="AMT_OVERPAYMENT";
        public static final String DTIME_CREATED="DTIME_CREATED";
        public static final String CREATED_BY="CREATED_BY";
        public static final String DTIME_UPDATED="DTIME_UPDATED";
        public static final String UPDATED_BY="UPDATED_BY";
        public static final String VERSION="VERSION";
        public static final String CURRENT_DUE="CURRENT_DUE";
        public static final String TOTAL_AMT_PAYMENT="TOTAL_AMT_PAYMENT";
        public static final String AMT_PAYMENT="AMT_PAYMENT";
        public static final String DATE_PAYMENT="DATE_PAYMENT";
        public static final String DATE_EXPORT="DATE_EXPORT";
        public static final String PMT_INSTALMENT="PMT_INSTALMENT";
        public static final String PMT_FEE="PMT_FEE";
        public static final String PMT_PENALTY="PMT_PENALTY";
        public static final String PMT_OVERPAYMENT="PMT_OVERPAYMENT";
        public static final String PMT_OTHER="PMT_OTHER";
        public static final String PARENT_ID="PARENT_ID";
        public static final String STATUS="STATUS";
        public static final String REASON="REASON";
        public static final String DTIME_STATUS_UPDATED="DTIME_STATUS_UPDATED";
        public static final String PAYMENT_TYPE="PAYMENT_TYPE";
        public static final String DATE_BANK_PROCESS="DATE_BANK_PROCESS";
        public static final String PMT_PRINCIPAL="PMT_PRINCIPAL";
        public static final String PMT_INTEREST="PMT_INTEREST";
        public static final String ADJ_PAYMENT_DATE="ADJ_PAYMENT_DATE";
        public static final String ARCHIVED="ARCHIVED";
        public static final String EXPORT_DATE="EXPORT_DATE";
        public static final String SKP_CONTRACT="SKP_CONTRACT";
        public static final String SEND_PRINCIPAL="SEND_PRINCIPAL";
        public static final String SEND_INSTALMENT="SEND_INSTALMENT";
        public static final String SEND_TENOR="SEND_TENOR";
        public static final String SEND_RATE="SEND_RATE";
        public static final String DATE_FIRST_DUE="DATE_FIRST_DUE";
        public static final String ACCEPTED_DATE="ACCEPTED_DATE";
        public static final String REJECT_REASON="REJECT_REASON";
        public static final String BANK_INTEREST_RATE="BANK_INTEREST_RATE";
        public static final String BANK_PRINCIPAL="BANK_PRINCIPAL";
        public static final String BANK_INTEREST="BANK_INTEREST";
        public static final String BANK_PROVISION="BANK_PROVISION";
        public static final String BANK_ADMIN_FEE="BANK_ADMIN_FEE";
        public static final String BANK_INSTALLMENT="BANK_INSTALLMENT";
        public static final String BANK_TENOR="BANK_TENOR";
        public static final String BANK_SPLIT_RATE="BANK_SPLIT_RATE";
        public static final String CLIENT_NAME="CLIENT_NAME";
        public static final String AMT_INSTALMENT="AMT_INSTALMENT";
        public static final String AMT_MONTHLY_FEE="AMT_MONTHLY_FEE";
        public static final String ID_AGREEMENT="ID_AGREEMENT";
        public static final String BANK_DECISION_DATE="BANK_DECISION_DATE";
        public static final String BANK_CLAWBACK_DATE="BANK_CLAWBACK_DATE";
        public static final String BANK_CLAWBACK_AMOUNT="BANK_CLAWBACK_AMOUNT";
        public static final String BANK_PRODUCT_CODE="BANK_PRODUCT_CODE";
        public static final String BANK_REFERENCE_NO="BANK_REFERENCE_NO";
        public static final String CUID="CUID";
        public static final String SKF_INSTALMENT_HEAD="SKF_INSTALMENT_HEAD";
        public static final String DTIME_PAYMENT_PAIR="DTIME_PAYMENT_PAIR";
        public static final String DTIME_PAYMENT_UNPAIR="DTIME_PAYMENT_UNPAIR";
        public static final String TEXT_PAYMENT_CHANNEL="TEXT_PAYMENT_CHANNEL";
        public static final String PAYMENT_TYPE_CODE="PAYMENT_TYPE_CODE";
        public static final String TOTAL_AMT="TOTAL_AMT";
        public static final String AMT_PRINCIPAL_T="AMT_PRINCIPAL_T";
        public static final String AMT_INTEREST_T="AMT_INTEREST_T";
        public static final String AMT_ORIG_FEE="AMT_ORIG_FEE";
        public static final String AMT_ET_FEE="AMT_ET_FEE";
        public static final String AMT_MPF_PENALTY_FEE="AMT_MPF_PENALTY_FEE";
        public static final String AMT_MONTHLY_FEE_INS="AMT_MONTHLY_FEE_INS";
        public static final String PAYMENT_PAIRED_STATUS="PAYMENT_PAIRED_STATUS";
        public static final String PAYMENT_CODE_STATUS="PAYMENT_CODE_STATUS";
        public static final String DTIME_PAYMENT_CANCELED="DTIME_PAYMENT_CANCELED";
        public static final String OFI_PRINCIPAL="OFI_PRINCIPAL";
        public static final String TOTAL_OFI_PRINCIPAL="TOTAL_OFI_PRINCIPAL";
        public static final String OFI_INTEREST="OFI_INTEREST";
        public static final String TOTAL_OFI_INTEREST="TOTAL_OFI_INTEREST";
        public static final String OUTSTANDING_OFI="OUTSTANDING_OFI";
        public static final String INT_ACCRUAL_1="INT_ACCRUAL_1";
        public static final String INT_ACCRUAL_2="INT_ACCRUAL_2";
        public static final String VALUE="VALUE";
    }

}
