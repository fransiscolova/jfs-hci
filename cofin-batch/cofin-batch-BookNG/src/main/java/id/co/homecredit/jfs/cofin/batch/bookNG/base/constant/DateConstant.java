package id.co.homecredit.jfs.cofin.batch.bookNG.base.constant;

/**
 * Date Constant
 *
 * @author denny.afrizal01
 *
 */
public class DateConstant{

    public static class FormatDate{
        public static final String TIMESTAMP="yyyy-MM-dd hh:mm:ss.SSS";
        public static final String FORMAT_1="yyyy-MM-dd";
        public static final String FORMAT_2="yyyy/MM/dd";
        public static final String FORMAT_3="yyyy-MM-dd-HH-mm-ss";
        public static final String FORMAT_4="HH-mm-ss";
    }

}
