package id.co.homecredit.jfs.cofin.batch.bookNG.base.constant;

/**
 * Entity Constant
 *
 * @author denny.afrizal01
 *
 */
public class EntityConstant{

    public static class Entity{
        public static final String UUID_GENERATOR="UUID_generator";
        public static final String UUID_GENERATOR_CLASS="id.co.homecredit.jfs.bookng.base.generator.IdGenerator";
    }

}
