package id.co.homecredit.jfs.cofin.batch.bookNG.base.constant;

/**
 * String Constant
 *
 * @author denny.afrizal01
 *
 */
public class StringConstant{

    public static class StringChar{
        public static final String EMPTY="";
        public static final String SPACE=" ";
        public static final String STRIP="-";
        public static final String PIPE="|";
        public static final String SEMICOLON=";";
        public static final String SLASH="/";
        public static final String BSLASH="\\";
        public static final String LEFT_PARENTHESES="(";
        public static final String RIGHT_PARENTHESES=")";
        public static final String COMMA=",";
        public static final String UNDERSCORE="_";
        public static final String DOT=".";
    }

}
