package id.co.homecredit.jfs.cofin.batch.bookNG.base.constant;

/**
 * Table Constant
 *
 * @author denny.afrizal01
 *
 */
public class TableConstant{

    public static class TableName{
        public static final String JFS_SCHEDULE="JFS_SCHEDULE";
        public static final String JFS_PAYMENT_BANK_SIM="JFS_PAYMENT_BANK_SIM";
        public static final String JFS_PAYMENT_INT="JFS_PAYMENT_INT";
        public static final String JFS_INPAY_ALLOCATION_FIX="JFS_INPAY_ALLOCATION_FIX";
        public static final String JFS_CONTRACT="JFS_CONTRACT";
        public static final String JFS_PAYMENT_BSL="JFS_PAYMENT_BSL";
        public static final String JFS_OFI_PAYMENT="JFS_OFI_PAYMENT";
    }

}
