package id.co.homecredit.jfs.cofin.batch.bookNG.base.controller;

import org.springframework.beans.factory.annotation.Autowired;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter.JfsContractAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter.JfsInpayAllocationFixAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter.JfsOfiPaymentAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter.JfsPaymentBslAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter.JfsPaymentBankSimAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter.JfsPaymentIntAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter.JfsScheduleAdapter;

/**
 * Base Controller Of BookNG
 *
 * @author denny.afrizal01
 *
 */

public class BaseController{

    @Autowired
    protected JfsPaymentBankSimAdapter jfsPaymentBankSimAdapter;

    @Autowired
    protected JfsScheduleAdapter jfsScheduleAdapter;

    @Autowired
    protected JfsPaymentIntAdapter jfsPaymentIntAdapter;

    @Autowired
    protected JfsInpayAllocationFixAdapter jfsInpayAllocationFixAdapter;

    @Autowired
    protected JfsContractAdapter jfsContractAdapter;

    @Autowired
    protected JfsPaymentBslAdapter jfsPaymentBslAdapter;

    @Autowired
    protected JfsOfiPaymentAdapter jfsOfiPaymentAdapter;

}
