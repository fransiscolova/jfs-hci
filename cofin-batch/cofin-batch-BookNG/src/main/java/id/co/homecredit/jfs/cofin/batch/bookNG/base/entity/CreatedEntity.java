package id.co.homecredit.jfs.cofin.batch.bookNG.base.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;

/**
 * CreatedEntity For BookNG
 *
 * @author denny.afrizal01
 *
 */

@MappedSuperclass
public abstract class CreatedEntity{
	protected String createdBy;
	protected Date dtimeCreated;
	@Column(name=ColumnName.CREATED_BY)
	public String getCreatedBy(){
		return createdBy;
	}
	public void setCreatedBy(String createdBy){
		this.createdBy=createdBy;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name=ColumnName.DTIME_CREATED)
	public Date getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(Date dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
}
