package id.co.homecredit.jfs.cofin.batch.bookNG.base.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.GenericGenerator;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.EntityConstant.Entity;

/**
 * IdEntity For BookNG
 *
 * @author denny.afrizal01
 *
 */

@MappedSuperclass
public abstract class IdEntity<ID extends Serializable> extends VersionEntity{
	protected String id;
	@Id
	@GeneratedValue(generator=Entity.UUID_GENERATOR)
	@GenericGenerator(name=Entity.UUID_GENERATOR,strategy=Entity.UUID_GENERATOR_CLASS)
	@Column(name=ColumnName.ID,nullable=false,length=32,unique=true)
	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id=id;
	}
}
