package id.co.homecredit.jfs.cofin.batch.bookNG.base.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import org.hibernate.annotations.GenericGenerator;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.EntityConstant.Entity;

/**
 * IdVerEntity For BookNG
 *
 * @author denny.afrizal01
 *
 */

@MappedSuperclass
public abstract class IdVerEntity<ID extends Serializable>{
	protected String id;
	protected Long ver;
	@Id
	@GeneratedValue(generator=Entity.UUID_GENERATOR)
	@GenericGenerator(name=Entity.UUID_GENERATOR,strategy=Entity.UUID_GENERATOR_CLASS)
	@Column(name=ColumnName.ID,nullable=false,length=32,unique=true)
	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id=id;
	}
	@Version
	@Column(name=ColumnName.VER,nullable=false)
	public Long getVer(){
		return ver;
	}
	public void setVer(Long ver){
		this.ver=ver;
	}
}
