package id.co.homecredit.jfs.cofin.batch.bookNG.base.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;

/**
 * UpdatedEntity For BookNG
 *
 * @author denny.afrizal01
 *
 */

@MappedSuperclass
public abstract class UpdatedEntity extends CreatedEntity{
	protected String updatedBy;
	protected Date dtimeUpdated;
	@Column(name=ColumnName.UPDATED_BY)
	public String getUpdatedBy(){
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy){
		this.updatedBy=updatedBy;
	}
	@Column(name=ColumnName.DTIME_UPDATED)
	public Date getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(Date dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
}
