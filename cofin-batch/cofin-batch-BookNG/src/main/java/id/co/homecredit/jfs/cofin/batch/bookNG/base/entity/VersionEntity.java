package id.co.homecredit.jfs.cofin.batch.bookNG.base.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;

/**
 * VersionEntity For BookNG
 *
 * @author denny.afrizal01
 *
 */

@MappedSuperclass
public abstract class VersionEntity extends UpdatedEntity{
	protected Long version;
	@Version
	@Column(name=ColumnName.VERSION,nullable=false)
	public Long getVersion(){
		return version;
	}
	public void setVersion(Long version){
		this.version=version;
	}
}
