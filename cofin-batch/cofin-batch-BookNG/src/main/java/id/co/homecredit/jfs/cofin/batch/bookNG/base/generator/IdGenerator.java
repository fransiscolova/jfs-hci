package id.co.homecredit.jfs.cofin.batch.bookNG.base.generator;

import java.io.Serializable;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * IdGenerator For BookNG
 *
 * @author denny.afrizal01
 *
 */

public class IdGenerator implements IdentifierGenerator{
	
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object){
		return StringGenerator.generateId();
	}

}
