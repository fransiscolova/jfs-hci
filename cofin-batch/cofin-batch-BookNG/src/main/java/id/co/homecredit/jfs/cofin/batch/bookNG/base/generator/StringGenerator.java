package id.co.homecredit.jfs.cofin.batch.bookNG.base.generator;

import java.util.UUID;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.StringConstant.StringChar;

/**
 * StringGenerator For BookNG
 *
 * @author denny.afrizal01
 *
 */

public class StringGenerator{
	
	public static String generateId(){
		return idGenerate().replaceAll(StringChar.STRIP,StringChar.EMPTY).toUpperCase();
	}
	
	public static String idGenerate(){
		return UUID.randomUUID().toString();
	}

}
