package id.co.homecredit.jfs.cofin.batch.bookNG.base.model;

/**
 * ListModel For BookNG
 *
 * @author denny.afrizal01
 *
 */

public class ListModel{
	private String text;
	public String getText(){
		return text;
	}
	public void setText(String text){
		this.text=text;
	}
}
