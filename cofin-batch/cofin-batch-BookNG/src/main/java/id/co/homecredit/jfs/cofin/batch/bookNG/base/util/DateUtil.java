package id.co.homecredit.jfs.cofin.batch.bookNG.base.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.DateConstant.FormatDate;

/**
 * DateUtil For BookNG
 *
 * @author denny.afrizal01
 *
 */

public class DateUtil{

	public static Date datePlusOneDay(Date date){
		return new Date(date.getTime()+TimeUnit.DAYS.toMillis(1));
	}
	
	public static Date dateAddingDays(Date date,Integer days){
		return new Date(date.getTime()+TimeUnit.DAYS.toMillis(days));
	}
	
	public static Date dateToDate(Date date)throws ParseException{
		return stringToDate(dateToString(date));
	}
	
	public static String dateToString(Date date){
		return dateToStringWithFormat(date,FormatDate.FORMAT_1);
	}
	
	public static String dateToStringWithFormat(Date date,String formatDate){
		return getFormat(formatDate).format(date);
	}
	
	public static SimpleDateFormat getFormat(String formatDate){
		return new SimpleDateFormat(formatDate);
	}
	
	public static Date stringToDate(String strDate)throws ParseException{
		return stringToDateWithFormat(strDate,FormatDate.FORMAT_1);
	}
	
	public static Date stringToDateWithFormat(String strDate,String formatDate)throws ParseException{
		return getFormat(formatDate).parse(strDate);
	}
	
	public static Date stringToTimestamp(String strDate)throws ParseException{
		return stringToDateWithFormat(strDate,FormatDate.TIMESTAMP);
	}
	
	public static Date getStartDate(Date date){
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		cld.set(Calendar.HOUR_OF_DAY,0);
		cld.set(Calendar.MINUTE,0);
		cld.set(Calendar.SECOND,0);
		return cld.getTime();
	}
	
	public static Date getEndDate(Date date){
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		cld.set(Calendar.HOUR_OF_DAY,23);
		cld.set(Calendar.MINUTE,59);
		cld.set(Calendar.SECOND,59);
		return cld.getTime();
	}
	
}
