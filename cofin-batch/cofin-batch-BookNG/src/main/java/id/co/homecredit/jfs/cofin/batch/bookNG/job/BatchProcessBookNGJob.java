package id.co.homecredit.jfs.cofin.batch.bookNG.job;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Execute batch process using spring batch.
 *
 * @author muhammad.muflihun
 *
 */
public class BatchProcessBookNGJob implements org.quartz.Job {
    private static final Logger log = LogManager.getLogger(BatchProcessBookNGJob.class);

    @SuppressWarnings("resource")
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String jobName = null;
        String cronExpression = null;
        try {
            jobName = context.getScheduler().getContext().getString("jobName");
            cronExpression = context.getScheduler().getContext().getString("cronExpression");
        } catch (SchedulerException e1) {
            e1.printStackTrace();
        }
        log.debug("passed job name {}", jobName);

        ApplicationContext appContext = new ClassPathXmlApplicationContext(
                "cofin-batch-BookNG-context.xml");
        JobLauncher jobLauncher = (JobLauncher) appContext.getBean("jobLauncher");
        Job job = (Job) appContext.getBean(jobName);
        JobParameters jobParameters = new JobParametersBuilder().addString("jobName", jobName)
                .addString("cronExpression", cronExpression).addDate("runTime", new Date())
                .toJobParameters();

        try {
            JobExecution execution = jobLauncher.run(job, jobParameters);
            log.info("execution status {}", execution.getStatus());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("main batch btpn exception : {}", e.getMessage());
            log.error(e.getStackTrace()[0]);
        }
    }

}
