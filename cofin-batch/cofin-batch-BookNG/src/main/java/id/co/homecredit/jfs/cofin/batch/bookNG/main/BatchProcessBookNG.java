package id.co.homecredit.jfs.cofin.batch.bookNG.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.bookNG.service.BatchProcessBookNGDailyService;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.BatchProcessBookNGMonthlyService;

/**
 * Main class to starting batch process for Permata.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class BatchProcessBookNG {
    private static final Logger log = LogManager.getLogger(BatchProcessBookNG.class);

    /**
     * Run batch executor.
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(final String[] args) throws InterruptedException {
        log.info("starting BookNG main");
        try {
            if (args == null || args.length < 1) {
                log.info("BatchProcessBookNG d|m jobName stepName");
                return;
            }

            @SuppressWarnings("resource")
            ApplicationContext appContext = new ClassPathXmlApplicationContext(
                    "cofin-batch-BookNG-context.xml");
            switch (args[0].charAt(0)) {
                case 'd':
                    // run spring batch depends on number of arguments
                    BatchProcessBookNGDailyService dailyService = appContext
                            .getBean(BatchProcessBookNGDailyService.class);
                    if (args.length == 1) {
                        dailyService.runJobOnce();
                    } else if (args.length == 2) {
                        String jobName = args[1];
                        dailyService.runJobWithQuartz(jobName);
                    } else if (args.length == 3) {
                        String jobName = args[1];
                        String stepName = args[2];
                        dailyService.runStepOnce(jobName, stepName);
                    }
                    break;
                case 'm':
                    // run spring batch depends on number of arguments
                    BatchProcessBookNGMonthlyService monthlyService = appContext
                            .getBean(BatchProcessBookNGMonthlyService.class);
                    if (args.length == 1) {
                        monthlyService.runJobOnce();
                    } else if (args.length == 2) {
                        String jobName = args[1];
                        monthlyService.runJobWithQuartz(jobName);
                    } else if (args.length == 3) {
                        String jobName = args[1];
                        String stepName = args[2];
                        monthlyService.runStepOnce(jobName, stepName);
                    }
                    break;
                default:
                    log.info("Invalid operation: must be (d)aily or (m)onthly.");
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("main batch permata exception : {}", e.getMessage());
            log.error(e.getStackTrace()[0]);
        }
    }
}
