package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter;

import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.adapter.BaseAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsContract;

/**
 * JfsContractAdapter For BookNG
 *
 * @author denny.afrizal01
 *
 */

@SuppressWarnings("unchecked")
public class JfsContractAdapter extends BaseAdapter{

    public JfsContract getByTextContractNumber(String textContractNumber){
        Session session = sfJFS.openSession();
        Criteria crt = session.createCriteria(JfsContract.class);
        crt.add(Restrictions.eq("textContractNumber",textContractNumber));
        List<JfsContract> con = crt.list();
        if(con.size()==0){
            return null;
        }else{
            return con.get(0);
        }
    }

    public List<Map<String,Object>> getListByExportDate(String startDate,String endDate){
        Session session = sfJFS.openSession();
        Query query = session.createSQLQuery("select * from JFS_CONTRACT where STATUS = 'A' and EXPORT_DATE between date '"+startDate+"' and date '"+endDate+"' "
                +"and TEXT_CONTRACT_NUMBER in (select distinct TEXT_CONTRACT_NUMBER from JFS_PAYMENT_INT) order by TEXT_CONTRACT_NUMBER");
        System.out.println(query.getQueryString());
        query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        List<Map<String,Object>> list = query.list();
        session.close();
        return list;
    }

}
