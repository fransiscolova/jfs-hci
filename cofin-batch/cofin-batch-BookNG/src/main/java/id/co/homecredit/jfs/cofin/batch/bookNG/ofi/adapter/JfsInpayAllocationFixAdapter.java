package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter;

import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.adapter.BaseAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsInpayAllocationFix;

/**
 * JfsInpayAllocationFixAdapter For BookNG
 *
 * @author denny.afrizal01
 *
 */

@SuppressWarnings("unchecked")
public class JfsInpayAllocationFixAdapter extends BaseAdapter{

        public JfsInpayAllocationFix getAllocationValue(Long incomingPaymentId,Date dateInstalment){
            Session session = sfJFS.openSession();
            Criteria crt = session.createCriteria(JfsInpayAllocationFix.class);
            crt.add(Restrictions.eq("incomingPaymentId",incomingPaymentId));
            crt.add(Restrictions.eq("dueDate",dateInstalment));
            List<JfsInpayAllocationFix> fix = crt.list();
            if(fix.size()==0){
            return null;
            }else{
            return fix.get(0);
            }
        }

}
