package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.adapter.BaseAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsOfiPayment;

/**
 * JfsOfiPaymentAdapter For BookNG
 *
 * @author denny.afrizal01
 *
 */

@SuppressWarnings("unchecked")
public class JfsOfiPaymentAdapter extends BaseAdapter{

    public JfsOfiPayment getByContractNumberAndPartIndex(String textContractNumber,Integer instalmentNumber){
        Session session = sfJFS.openSession();
        Criteria crt = session.createCriteria(JfsOfiPayment.class);
        crt.add(Restrictions.eq("textContractNumber",textContractNumber));
        crt.add(Restrictions.eq("partIndex",instalmentNumber));
        List<JfsOfiPayment> pay = crt.list();
        if(pay.size()==0){
            return null;
        }else{
            return pay.get(0);
        }
    }

}
