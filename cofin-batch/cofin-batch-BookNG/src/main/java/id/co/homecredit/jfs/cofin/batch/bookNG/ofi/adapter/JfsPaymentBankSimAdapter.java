package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter;

import java.util.List;
import java.text.SimpleDateFormat;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.adapter.BaseAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsPaymentBankSim;

/**
 * JfsPaymentBankSimAdapter For BookNG
 *
 * @author denny.afrizal01
 *
 */

@SuppressWarnings("unchecked")
public class JfsPaymentBankSimAdapter extends BaseAdapter{
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public List<JfsPaymentBankSim> getByContractNumber(String textContractNumber){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentBankSim.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		List<JfsPaymentBankSim> list = crt.list();
		return list;
	}
	
	public JfsPaymentBankSim getByContractNumberAndIncomingPaymentId(String textContractNumber, Long incomingPaymentId){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentBankSim.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.add(Restrictions.eq("incomingPaymentId",incomingPaymentId));
		List<JfsPaymentBankSim> list = crt.list();
		if(list.size()==0){
			return null;
		}else{
			return list.get(0);
		}
	}
	
}
