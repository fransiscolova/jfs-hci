package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter;

import java.util.List;
import java.util.Date;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.adapter.BaseAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.StringConstant.StringChar;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.util.DateUtil;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsPaymentBsl;

/**
 * JfsPaymentBslAdapter For BookNG
 *
 * @author denny.afrizal01
 *
 */

@SuppressWarnings("unchecked")
public class JfsPaymentBslAdapter extends BaseAdapter{

	public JfsPaymentBsl getByIncomingPaymentId(String textContractNumber,Long incomingPaymentId){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentBsl.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.add(Restrictions.eq("incomingPaymentId",incomingPaymentId));
		List<JfsPaymentBsl> list = crt.list();
		if(list.size()==0){
			return null;
		}else{
			return list.get(0);
		}
	}
	
	public List<JfsPaymentBsl> getByTextContractNumber(String textContractNumber){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentBsl.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.addOrder(Order.asc("instalmentNumber"));
		return crt.list();
	}
	
	public String[] getContractBslByDate(Date startDate,Date endDate){
		startDate = DateUtil.getStartDate(startDate);
		endDate = DateUtil.getEndDate(endDate);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentBsl.class);
		crt.add(Restrictions.ge("paymentDate",startDate));
		crt.add(Restrictions.le("paymentDate",endDate));
		crt.addOrder(Order.asc("textContractNumber"));
		crt.addOrder(Order.asc("instalmentNumber"));
		List<JfsPaymentBsl> bsl = crt.list();
		if(bsl.size()==0){
			return new String[0];
		}else{
			String contractNumber = StringChar.EMPTY;
			String tampung = StringChar.EMPTY;
			for(int x=0;x<bsl.size();x++){
				if(!contractNumber.equals(bsl.get(x).getTextContractNumber())){
					contractNumber = bsl.get(x).getTextContractNumber();
					tampung = tampung+contractNumber+",";
				}else{
					System.out.println("Skip Insert For "+contractNumber);
				}
			}
			tampung = tampung.substring(0,tampung.length()-1);
			System.out.println(tampung);
			String[] listContract = tampung.split(",");
			return listContract;
		}
	}
	
}
