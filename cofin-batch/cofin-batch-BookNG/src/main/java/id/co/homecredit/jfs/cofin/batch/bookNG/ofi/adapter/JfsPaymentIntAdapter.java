package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter;

import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.adapter.BaseAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsPaymentInt;

/**
 * JfsPaymentIntAdapter For BookNG
 *
 * @author denny.afrizal01
 *
 */

@SuppressWarnings("unchecked")
public class JfsPaymentIntAdapter extends BaseAdapter{
	
	public Double getTotalInstalment(String textContractNumber,Date todayDate){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentInt.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.add(Restrictions.eq("dateExport",todayDate));
		crt.add(Restrictions.eq("paymentType","R"));
		crt.add(Restrictions.gt("pmtInstalment",0.0));
		List<JfsPaymentInt> list = crt.list();
		if(list.size()==0){
			return 0.0;
		}else{
			Double total = 0.0;
			for(int x=0;x<list.size();x++){
				total = total+list.get(x).getPmtInstalment();
			}
			return total;
		}
	}
	
	public List<JfsPaymentInt> getByDateExport(Date dateExport){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentInt.class);
		crt.add(Restrictions.eq("dateExport",dateExport));
		crt.addOrder(Order.asc("incomingPaymentId"));
		List<JfsPaymentInt> list = crt.list();
		return list;
	}
	
	public List<JfsPaymentInt> getByTextContractNumber(String textContractNumber){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentInt.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.addOrder(Order.asc("dateExport"));
		return crt.list();
	}

}
