package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.adapter.BaseAdapter;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.util.DateUtil;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsSchedule;

/**
 * JfsScheduleAdapter For BookNG
 *
 * @author denny.afrizal01
 *
 */

@SuppressWarnings("unchecked")
public class JfsScheduleAdapter extends BaseAdapter{
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	public JfsSchedule getByTextContractNumberAndDueDate(String textContractNumber,Date dueDate){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsSchedule.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.add(Restrictions.eq("dueDate",dueDate));
		List<JfsSchedule> sch = crt.list();
		if(sch.size()==0){
			return null;
		}else{
			return sch.get(0);
		}
	}
	
	public List<JfsSchedule> getListByContractNumber(String textContractNumber){
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsSchedule.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.addOrder(Order.asc("partIndex"));
		return crt.list();
	}
	
	public JfsSchedule getByContractNumberAndDueDate(String textContractNumber,Date dueDate){
		Session session = sfJFS.openSession();
		Date startDate = DateUtil.getStartDate(dueDate);
		Date endDate = DateUtil.getEndDate(dueDate);
		Criteria crt = session.createCriteria(JfsSchedule.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.add(Restrictions.ge("dueDate",startDate));
		crt.add(Restrictions.le("dueDate",endDate));
		List<JfsSchedule> jfs = crt.list();
		if(jfs.size()==0){
			return null;
		}else{
			return jfs.get(0);
		}
	}
	
	public boolean updateJfsSchedule(String textContractNumber,Integer partIndex,Double intAcr1,Double intAcr2){
		String sql = "update JFS_SCHEDULE set INT_ACCRUAL_1 = "+intAcr1+", INT_ACCRUAL_2 = "+intAcr2+" where TEXT_CONTRACT_NUMBER = '"+textContractNumber+"' and PART_INDEX = "+partIndex+"";
		return updateObjectUsingQuery(sql);
	}
	
}
