package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.controller.BaseController;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsInpayAllocationFix;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsOfiPayment;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsPaymentBsl;

/**
 * JfsOfiPaymentController For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Controller
@RequestMapping(value="/ofi/JfsOfiPayment")
public class JfsOfiPaymentController extends BaseController{
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	SimpleDateFormat tdf = new SimpleDateFormat("yyyy-MM-dd");
	DecimalFormat dec = new DecimalFormat("###,###,###,###,###");
	
	@RequestMapping(value="/NormalCalculationPrincipalAndInterest",method={RequestMethod.GET,RequestMethod.POST})
	public void getNormalCalc(HttpServletRequest request)throws Exception{
		try{
			if(request.getParameter("textContractNumber")==null){
				String[] kontrak = jfsPaymentBslAdapter.getContractBslByDate(tdf.parse(request.getParameter("startDate")),tdf.parse(request.getParameter("endDate")));
				if(kontrak.length==0){
					System.out.println("Cannot Found Contract With Payment Date Between "+sdf.format(request.getParameter("startDate"))+" And "+request.getParameter("endDate")+" In JfsPaymentBsl");
				}else{
					for(int x=0;x<kontrak.length;x++){
						System.out.println("Processed Contract Number "+kontrak[x]+" Start");
						List<JfsPaymentBsl> list = jfsPaymentBslAdapter.getByTextContractNumber(kontrak[x]);
						if(list.size()==0){
							System.out.println("ContractNumber "+kontrak[x]+" Not Found In JfsPaymentBsl");
						}else{
							Double totalPrn = 0.0;
							Double totalInt = 0.0;
							for(int c=0;c<list.size();c++){
								JfsOfiPayment ofi = jfsOfiPaymentAdapter.getByContractNumberAndPartIndex(list.get(c).getTextContractNumber(),list.get(c).getInstalmentNumber());
								if(ofi==null){
									JfsInpayAllocationFix fix = jfsInpayAllocationFixAdapter.getAllocationValue(list.get(c).getIncomingPaymentId(),list.get(c).getDateInstalment());
									Double principal = 0.0;
									Double interest = 0.0;
									if(fix==null){
										principal = list.get(c).getAmtPrincipal()-(0.1*list.get(c).getAmtPrincipal());
										interest = list.get(c).getAmtInterest()-(0.1*list.get(c).getAmtInterest());
									}else{
										principal = list.get(c).getAmtPrincipal()-fix.getAmtPrincipal()-(0.1*list.get(c).getAmtPrincipal());
										interest = list.get(c).getAmtInterest()-fix.getAmtInterest()-(0.1*list.get(c).getAmtInterest());
									}
									principal = (double) Math.round(principal*100)/100;
									interest = (double) Math.round(interest*100)/100;
									totalPrn = totalPrn+principal;
									totalInt = totalInt+interest;
									JfsOfiPayment pay = new JfsOfiPayment();
									pay.setTextContractNumber(list.get(c).getTextContractNumber());
									pay.setDueDate(list.get(c).getDateInstalment());
									pay.setPartIndex(list.get(c).getInstalmentNumber());
									pay.setOfiPrincipal(principal);
									pay.setOfiInterest(interest);
									pay.setTotalOfiPrincipal((double) Math.round(totalPrn*100)/100);
									pay.setTotalOfiInterest((double) Math.round(totalInt*100)/100);
									pay.setOutstandingOfi(0.0);
									pay.setIsActive("Y");
									if(jfsOfiPaymentAdapter.saveObject(pay)){
										System.out.println("Success Save Data With Text Contract Number "+list.get(c).getTextContractNumber()+" And Instalment Number "+list.get(c).getInstalmentNumber());
									}else{
										System.out.println("Failed Save Data With Text Contract Number "+list.get(c).getTextContractNumber()+" And Instalment Number "+list.get(c).getInstalmentNumber());
									}
								}else{
									System.out.println("Contract Number "+list.get(c).getTextContractNumber()+" With Installment Number "+list.get(c).getInstalmentNumber()+" Was Exist In JfsOfiPayment");
								}
							}
							totalPrn = 0.0;
							totalInt = 0.0;
						}
						System.out.println("Processed Contract Number "+kontrak[x]+" Finish");
					}
					System.out.println("Complete To Process Data With Payment Date Between "+sdf.format(tdf.parse(request.getParameter("startDate")))+" And "+sdf.format(tdf.parse(request.getParameter("endDate"))));
				}
			}else{
				List<JfsPaymentBsl> list = jfsPaymentBslAdapter.getByTextContractNumber(request.getParameter("textContractNumber"));
				if(list.size()==0){
					System.out.println("ContractNumber "+request.getParameter("textContractNumber")+" Not Found In JfsPaymentBsl");
				}else{
					System.out.println("Processed Contract Number "+request.getParameter("textContractNumber")+" Start");
					Double totalPrn = 0.0;
					Double totalInt = 0.0;
					for(int x=0;x<list.size();x++){
						JfsOfiPayment ofi = jfsOfiPaymentAdapter.getByContractNumberAndPartIndex(list.get(x).getTextContractNumber(),list.get(x).getInstalmentNumber());
						if(ofi==null){
							JfsInpayAllocationFix fix = jfsInpayAllocationFixAdapter.getAllocationValue(list.get(x).getIncomingPaymentId(),list.get(x).getDateInstalment());
							Double principal = 0.0;
							Double interest = 0.0;
							if(fix==null){
								principal = list.get(x).getAmtPrincipal()-(0.1*list.get(x).getAmtPrincipal());
								interest = list.get(x).getAmtInterest()-(0.1*list.get(x).getAmtInterest());
							}else{
								principal = list.get(x).getAmtPrincipal()-fix.getAmtPrincipal()-(0.1*list.get(x).getAmtPrincipal());
								interest = list.get(x).getAmtInterest()-fix.getAmtInterest()-(0.1*list.get(x).getAmtInterest());
							}
							principal = (double) Math.round(principal*100)/100;
							interest = (double) Math.round(interest*100)/100;
							totalPrn = totalPrn+principal;
							totalInt = totalInt+interest;
							JfsOfiPayment pay = new JfsOfiPayment();
							pay.setTextContractNumber(list.get(x).getTextContractNumber());
							pay.setDueDate(list.get(x).getDateInstalment());
							pay.setPartIndex(list.get(x).getInstalmentNumber());
							pay.setOfiPrincipal(principal);
							pay.setOfiInterest(interest);
							pay.setTotalOfiPrincipal((double) Math.round(totalPrn*100)/100);
							pay.setTotalOfiInterest((double) Math.round(totalInt*100)/100);
							pay.setOutstandingOfi(0.0);	
							pay.setIsActive("Y");
							if(jfsOfiPaymentAdapter.saveObject(pay)){
								System.out.println("Success Save Data With Text Contract Number "+list.get(x).getTextContractNumber()+" And Instalment Number "+list.get(x).getInstalmentNumber());
							}else{
								System.out.println("Failed Save Data With Text Contract Number "+list.get(x).getTextContractNumber()+" And Instalment Number "+list.get(x).getInstalmentNumber());
							}
						}else{
							System.out.println("Contract Number "+list.get(x).getTextContractNumber()+" With Installment Number "+list.get(x).getInstalmentNumber()+" Was Exist In JfsOfiPayment");
						}
					}
					System.out.println("Processed Contract Number "+request.getParameter("textContractNumber")+" Finish");
					totalPrn = 0.0;
					totalInt = 0.0;
					System.out.println("Complete To Process Data With TextContractNumber "+request.getParameter("textContractNumber"));
				}
			}
		}catch(Exception g){
			System.out.println(g);
		}
	}

}
