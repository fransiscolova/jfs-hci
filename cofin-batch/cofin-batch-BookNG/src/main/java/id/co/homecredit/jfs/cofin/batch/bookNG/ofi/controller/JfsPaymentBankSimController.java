package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.google.gson.Gson;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.controller.BaseController;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsContract;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsPaymentBankSim;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsPaymentInt;

/**
 * JfsPaymentBankSimController For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Controller
@RequestMapping(value="/ofi")
public class JfsPaymentBankSimController extends BaseController{
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	SimpleDateFormat tdf = new SimpleDateFormat("yyyy-MM-dd");
	DecimalFormat dec = new DecimalFormat("###,###,###,###,###");
	
	@RequestMapping(value="/JfsPaymentBankSim",method={RequestMethod.GET,RequestMethod.POST})
	public void createSimulater(HttpServletRequest request)throws Exception{
		List<Map<String,Object>> xlist = jfsContractAdapter.getListByExportDate(request.getParameter("startDate"),request.getParameter("endDate"));
		for(int x=0;x<xlist.size();x++){
			String contractNumber = xlist.get(x).get(ColumnName.TEXT_CONTRACT_NUMBER).toString();
			System.out.println("Processed Contract Number "+contractNumber+" Start");
			JfsContract kontrak = jfsContractAdapter.getByTextContractNumber(contractNumber);
			if(kontrak==null){
				System.out.println("Contract Number "+contractNumber+" Not Found");
			}else{
				List<JfsPaymentInt> pay = jfsPaymentIntAdapter.getByTextContractNumber(contractNumber);
				if(pay.size()==0){
					System.out.println("Contract Number "+contractNumber+" Not Found In JfsPaymentBsl");
				}else{
					Date instDate = new Date();
					for(int c=0;c<pay.size();c++){
						if(c==0){
							instDate = kontrak.getDateFirstDue();
						}else{
							instDate = jfsPaymentBankSimAdapter.addMonths(tdf.format(instDate),1);
						}
						JfsPaymentBankSim sim = jfsPaymentBankSimAdapter.getByContractNumberAndIncomingPaymentId(contractNumber,pay.get(c).getIncomingPaymentId());
						if(sim==null){
							Double alloc = 0.0;
							List<JfsPaymentBankSim> xsim = jfsPaymentBankSimAdapter.getByContractNumber(contractNumber);
							if(xsim.size()==0){
								alloc = 0.0;
							}else{
								for(int z=0;z<xsim.size();z++){
									alloc = alloc+xsim.get(z).getAmtPrincipal();
								}
							}
							Double outstandingPrincipal = kontrak.getBankPrincipal()-alloc;
							Double bankInstalment = kontrak.getBankInstallment();
							Double bankInterestRate = kontrak.getBankInterestRate();
							Double interest = outstandingPrincipal*bankInterestRate/1200.0;
							Double principal = bankInstalment-interest;
							JfsPaymentBankSim sav = new JfsPaymentBankSim();
							sav.setIncomingPaymentId(pay.get(c).getIncomingPaymentId());
							sav.setTextContractNumber(pay.get(c).getTextContractNumber());
							sav.setDateInstalment(instDate);
							sav.setIncomingPaymentId(pay.get(c).getIncomingPaymentId());
							sav.setInstalmentNumber(c+1);
							sav.setPaymentDate(pay.get(c).getDatePayment());
							sav.setAmtPrincipal((double) Math.round(principal*1)/1);
							sav.setAmtInterest((double) Math.round(interest*1)/1);
							sav.setAmtPenalty(0.0);
							sav.setAmtOverpayment(0.0);
							sav.setDtimeCreated(new Date());
							sav.setCreatedBy("JFS_System");
							if(jfsPaymentBankSimAdapter.saveObject(sav)){
								System.out.println("Success Save Text Contract Number "+kontrak.getTextContractNumber()+" Incoming Payment ID "+pay.get(c).getIncomingPaymentId());
							}else{
								System.out.println("Failed Save Text Contract Number "+kontrak.getTextContractNumber()+" Incoming Payment ID "+pay.get(c).getIncomingPaymentId());
							}
							System.out.println(new Gson().toJson(sav));
						}else{
							System.out.println("Contract Number "+contractNumber+" With Incoming Payment ID "+pay.get(c).getIncomingPaymentId()+" Already Exist");
						}
					}
				}
			}
			System.out.println("Processed Contract Number "+contractNumber+" Finish");
		}
		System.out.println("Complete To Process Data");
	}
	
}
