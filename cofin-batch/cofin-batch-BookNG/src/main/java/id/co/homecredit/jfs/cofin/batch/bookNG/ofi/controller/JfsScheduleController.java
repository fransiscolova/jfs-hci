package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.controller.BaseController;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.util.DateUtil;
import id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity.JfsSchedule;

/**
 * JfsScheduleController For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Controller
@RequestMapping(value="/ofi/JfsSchedule")
public class JfsScheduleController extends BaseController{
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
	DecimalFormat dec = new DecimalFormat("###,###,###,###,###");

	@RequestMapping(value="/PartnerAccrued",method={RequestMethod.GET,RequestMethod.POST})
	public void accruedPartner(HttpServletRequest request)throws Exception{
		if(request.getParameter("textContractNumber")==null){
			List<Map<String,Object>> xlist = jfsContractAdapter.getListByExportDate(request.getParameter("startDate"),request.getParameter("endDate"));
			if(xlist.size()==0){
				System.out.println("Null Value From JfsContract");
			}else{
				for(int x=0;x<xlist.size();x++){
					String textContractNumber = xlist.get(x).get(ColumnName.TEXT_CONTRACT_NUMBER).toString();
					System.out.println("Processed Contract Number "+textContractNumber+" Start");
					List<JfsSchedule> sch = jfsScheduleAdapter.getListByContractNumber(textContractNumber);
					if(sch.size()==0){
						System.out.println("Contract Number "+textContractNumber+" Not Found In JfsSchedule");
					}else{
						for(int c=0;c<sch.size();c++){
							JfsSchedule jfs = sch.get(c);
							if(jfs.getIntAccrual1()==null){
								if(sch.get(c).getPartIndex()==0){
									Date nextDate = jfsScheduleAdapter.addMonths(dtf.format(sch.get(c).getDueDate()),1);
									JfsSchedule skedul = jfsScheduleAdapter.getByContractNumberAndDueDate(sch.get(c).getTextContractNumber(),nextDate);
									Double xinterest = 0.0;
									if(skedul==null){
										xinterest = 0.0;
									}else{
										xinterest = skedul.getInterest();
									}
									Date accDate = sdf.parse("01."+sdf.format(nextDate).substring(3,5)+"."+sdf.format(nextDate).substring(6,10));
									accDate = DateUtil.dateAddingDays(accDate,-1);
									Integer xdate1 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sch.get(c).getDueDate()),dtf.format(accDate));
									Integer xdate2 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sch.get(c).getDueDate()),dtf.format(nextDate));
									Double intAmount = (Double.parseDouble(xdate1.toString())+1)/Double.parseDouble(xdate2.toString())*xinterest;
									jfs.setIntAccrual1(0.0);
									jfs.setIntAccrual2((double) Math.round(intAmount*100)/100);
									if(jfsScheduleAdapter.updateJfsSchedule(jfs.getTextContractNumber(),jfs.getPartIndex(),jfs.getIntAccrual1(),jfs.getIntAccrual2())){
										System.out.println("Success Update Data With Text Contract Number = "+sch.get(c).getTextContractNumber()+" And Part Index = "+c);
									}else{
										System.out.println("Failed Update Data With Text Contract Number = "+sch.get(c).getTextContractNumber()+" And Part Index = "+c);
									}
								}else{

									String dueDate = sdf.format(sch.get(c).getDueDate());
									Integer bln = Integer.parseInt(dueDate.substring(3,5))-1;
									String bulan = "";
									if(bln<10){
										bulan = "0"+String.valueOf(bln);
									}else{
										bulan = String.valueOf(bln);
									}
									String frmDate = dueDate.substring(0,2)+"."+bulan+"."+dueDate.substring(6,10);
									Date accPeriod = sdf.parse("01."+dueDate.substring(3,5)+"."+dueDate.substring(6,10));
									Date accDate = DateUtil.dateAddingDays(accPeriod,-1);
									Integer accDte1 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(accDate),dtf.format(sch.get(c).getDueDate()));
									Integer accDte2 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sdf.parse(frmDate)),dtf.format(sch.get(c).getDueDate()));
									Double intAcr1 = (Double.parseDouble(accDte1.toString())-1)/Double.parseDouble(accDte2.toString())*sch.get(c).getInterest();
									Date nextMonth = jfsScheduleAdapter.addMonths(dtf.format(sch.get(c).getDueDate()),1);
									JfsSchedule jadwal = jfsScheduleAdapter.getByContractNumberAndDueDate(sch.get(c).getTextContractNumber(),nextMonth);
									Double interest = 0.0;
									if(jadwal==null){
										interest = 0.0;
									}else{
										interest = jadwal.getInterest();
									}
									Date accDate1 = sdf.parse("01."+sdf.format(nextMonth).substring(3,5)+"."+sdf.format(nextMonth).substring(6,10));
									accDate1 = DateUtil.dateAddingDays(accDate1,-1);
									Integer intDate1 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sch.get(c).getDueDate()),dtf.format(accDate1));
									Integer intDate2 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sch.get(c).getDueDate()),dtf.format(nextMonth));
									Double intAcr2 = (Double.parseDouble(intDate1.toString())+1)/Double.parseDouble(intDate2.toString())*interest;
									jfs.setIntAccrual1((double) Math.round(intAcr1*100)/100);
									jfs.setIntAccrual2((double) Math.round(intAcr2*100)/100);
									if(jfsScheduleAdapter.updateJfsSchedule(jfs.getTextContractNumber(),jfs.getPartIndex(),jfs.getIntAccrual1(),jfs.getIntAccrual2())){
										System.out.println("Success Update Data With Text Contract Number = "+sch.get(c).getTextContractNumber()+" And Part Index = "+c);
									}else{
										System.out.println("Failed Update Data With Text Contract Number = "+sch.get(c).getTextContractNumber()+" And Part Index = "+c);
									}
								}
							}else{
								System.out.println("Skip For Calculate Contract Number "+sch.get(c).getTextContractNumber()+" And Part Index "+c);
							}
						}
					}
					System.out.println("Processed Contract Number "+textContractNumber+" Finish");
				}
			}
		}else{
			List<JfsSchedule> sch = jfsScheduleAdapter.getListByContractNumber(request.getParameter("textContractNumber"));
			System.out.println("Processed Contract Number "+request.getParameter("textContractNumber")+" Start");
			if(sch.size()==0){
				System.out.println("Contract Number "+request.getParameter("textContractNumber")+" Not Found In JfsSchedule");
			}else{
				for(int c=0;c<sch.size();c++){
					JfsSchedule jfs = sch.get(c);
					if(jfs.getIntAccrual1()==null){
						if(sch.get(c).getPartIndex()==0){
							Date nextDate = jfsScheduleAdapter.addMonths(dtf.format(sch.get(c).getDueDate()),1);
							JfsSchedule skedul = jfsScheduleAdapter.getByContractNumberAndDueDate(sch.get(c).getTextContractNumber(),nextDate);
							Double xinterest = 0.0;
							if(skedul==null){
								xinterest = 0.0;
							}else{
								xinterest = skedul.getInterest();
							}
							Date accDate = sdf.parse("01."+sdf.format(nextDate).substring(3,5)+"."+sdf.format(nextDate).substring(6,10));
							accDate = DateUtil.dateAddingDays(accDate,-1);
							Integer xdate1 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sch.get(c).getDueDate()),dtf.format(accDate));
							Integer xdate2 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sch.get(c).getDueDate()),dtf.format(nextDate));
							Double intAmount = (Double.parseDouble(xdate1.toString())+1)/Double.parseDouble(xdate2.toString())*xinterest;
							jfs.setIntAccrual1(0.0);
							jfs.setIntAccrual2((double) Math.round(intAmount*100)/100);
							if(jfsScheduleAdapter.updateJfsSchedule(jfs.getTextContractNumber(),jfs.getPartIndex(),jfs.getIntAccrual1(),jfs.getIntAccrual2())){
								System.out.println("Success Update Data With Text Contract Number = "+sch.get(c).getTextContractNumber()+" And Part Index = "+c);
							}else{
								System.out.println("Failed Update Data With Text Contract Number = "+sch.get(c).getTextContractNumber()+" And Part Index = "+c);
							}
						}else{

							String dueDate = sdf.format(sch.get(c).getDueDate());
							Integer bln = Integer.parseInt(dueDate.substring(3,5))-1;
							String bulan = "";
							if(bln<10){
								bulan = "0"+String.valueOf(bln);
							}else{
								bulan = String.valueOf(bln);
							}
							String frmDate = dueDate.substring(0,2)+"."+bulan+"."+dueDate.substring(6,10);
							Date accPeriod = sdf.parse("01."+dueDate.substring(3,5)+"."+dueDate.substring(6,10));
							Date accDate = DateUtil.dateAddingDays(accPeriod,-1);
							Integer accDte1 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(accDate),dtf.format(sch.get(c).getDueDate()));
							Integer accDte2 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sdf.parse(frmDate)),dtf.format(sch.get(c).getDueDate()));
							Double intAcr1 = (Double.parseDouble(accDte1.toString())-1)/Double.parseDouble(accDte2.toString())*sch.get(c).getInterest();
							Date nextMonth = jfsScheduleAdapter.addMonths(dtf.format(sch.get(c).getDueDate()),1);
							JfsSchedule jadwal = jfsScheduleAdapter.getByContractNumberAndDueDate(sch.get(c).getTextContractNumber(),nextMonth);
							Double interest = 0.0;
							if(jadwal==null){
								interest = 0.0;
							}else{
								interest = jadwal.getInterest();
							}
							Date accDate1 = sdf.parse("01."+sdf.format(nextMonth).substring(3,5)+"."+sdf.format(nextMonth).substring(6,10));
							accDate1 = DateUtil.dateAddingDays(accDate1,-1);
							Integer intDate1 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sch.get(c).getDueDate()),dtf.format(accDate1));
							Integer intDate2 = jfsScheduleAdapter.getValueBetweenDate(dtf.format(sch.get(c).getDueDate()),dtf.format(nextMonth));
							Double intAcr2 = (Double.parseDouble(intDate1.toString())+1)/Double.parseDouble(intDate2.toString())*interest;
							jfs.setIntAccrual1((double) Math.round(intAcr1*100)/100);
							jfs.setIntAccrual2((double) Math.round(intAcr2*100)/100);
							if(jfsScheduleAdapter.updateJfsSchedule(jfs.getTextContractNumber(),jfs.getPartIndex(),jfs.getIntAccrual1(),jfs.getIntAccrual2())){
								System.out.println("Success Update Data With Text Contract Number = "+sch.get(c).getTextContractNumber()+" And Part Index = "+c);
							}else{
								System.out.println("Failed Update Data With Text Contract Number = "+sch.get(c).getTextContractNumber()+" And Part Index = "+c);
							}
						
						}
					}else{
						System.out.println("Skip For Calculate Contract Number "+sch.get(c).getTextContractNumber()+" And Part Index "+c);
					}
				}
			}
			System.out.println("Processed Contract Number "+request.getParameter("textContractNumber")+" Finish");
		}
	}
	
}
