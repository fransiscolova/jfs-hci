package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.TableConstant.TableName;
import java.util.Date;

/**
 * JfsContract For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=TableName.JFS_CONTRACT)
public class JfsContract{
	private String textContractNumber;
	private String status;
	private Date exportDate;
	private Long skpContract;
	private Double sendPrincipal;
	private Double sendInstalment;
	private Double sendTenor;
	private Double sendRate;
	private Date dateFirstDue;
	private Date acceptedDate;
	private String rejectReason;
	private Double bankInterestRate;
	private Double bankPrincipal;
	private Double bankInterest;
	private Double bankProvision;
	private Double bankAdminFee;
	private Double bankInstallment;
	private Double bankTenor;
	private Double bankSplitRate;
	private String clientName;
	private Double amtInstalment;
	private Double amtMonthlyFee;
	private Long idAgreement;
	private String reason;
	private Date bankDecisionDate;
	private Date bankClawbackDate;
	private Double bankClawbackAmount;
	private String bankProductCode;
	private String bankReferenceNo;
	private Long cuid;
	@Id
	@Column(name=ColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=ColumnName.STATUS)
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	@Column(name=ColumnName.EXPORT_DATE)
	public Date getExportDate(){
		return exportDate;
	}
	public void setExportDate(Date exportDate){
		this.exportDate=exportDate;
	}
	@Column(name=ColumnName.SKP_CONTRACT)
	public Long getSkpContract(){
		return skpContract;
	}
	public void setSkpContract(Long skpContract){
		this.skpContract=skpContract;
	}
	@Column(name=ColumnName.SEND_PRINCIPAL)
	public Double getSendPrincipal(){
		return sendPrincipal;
	}
	public void setSendPrincipal(Double sendPrincipal){
		this.sendPrincipal=sendPrincipal;
	}
	@Column(name=ColumnName.SEND_INSTALMENT)
	public Double getSendInstalment(){
		return sendInstalment;
	}
	public void setSendInstalment(Double sendInstalment){
		this.sendInstalment=sendInstalment;
	}
	@Column(name=ColumnName.SEND_TENOR)
	public Double getSendTenor(){
		return sendTenor;
	}
	public void setSendTenor(Double sendTenor){
		this.sendTenor=sendTenor;
	}
	@Column(name=ColumnName.SEND_RATE)
	public Double getSendRate(){
		return sendRate;
	}
	public void setSendRate(Double sendRate){
		this.sendRate=sendRate;
	}
	@Column(name=ColumnName.DATE_FIRST_DUE)
	public Date getDateFirstDue(){
		return dateFirstDue;
	}
	public void setDateFirstDue(Date dateFirstDue){
		this.dateFirstDue=dateFirstDue;
	}
	@Column(name=ColumnName.ACCEPTED_DATE)
	public Date getAcceptedDate(){
		return acceptedDate;
	}
	public void setAcceptedDate(Date acceptedDate){
		this.acceptedDate=acceptedDate;
	}
	@Column(name=ColumnName.REJECT_REASON)
	public String getRejectReason(){
		return rejectReason;
	}
	public void setRejectReason(String rejectReason){
		this.rejectReason=rejectReason;
	}
	@Column(name=ColumnName.BANK_INTEREST_RATE)
	public Double getBankInterestRate(){
		return bankInterestRate;
	}
	public void setBankInterestRate(Double bankInterestRate){
		this.bankInterestRate=bankInterestRate;
	}
	@Column(name=ColumnName.BANK_PRINCIPAL)
	public Double getBankPrincipal(){
		return bankPrincipal;
	}
	public void setBankPrincipal(Double bankPrincipal){
		this.bankPrincipal=bankPrincipal;
	}
	@Column(name=ColumnName.BANK_INTEREST)
	public Double getBankInterest(){
		return bankInterest;
	}
	public void setBankInterest(Double bankInterest){
		this.bankInterest=bankInterest;
	}
	@Column(name=ColumnName.BANK_PROVISION)
	public Double getBankProvision(){
		return bankProvision;
	}
	public void setBankProvision(Double bankProvision){
		this.bankProvision=bankProvision;
	}
	@Column(name=ColumnName.BANK_ADMIN_FEE)
	public Double getBankAdminFee(){
		return bankAdminFee;
	}
	public void setBankAdminFee(Double bankAdminFee){
		this.bankAdminFee=bankAdminFee;
	}
	@Column(name=ColumnName.BANK_INSTALLMENT)
	public Double getBankInstallment(){
		return bankInstallment;
	}
	public void setBankInstallment(Double bankInstallment){
		this.bankInstallment=bankInstallment;
	}
	@Column(name=ColumnName.BANK_TENOR)
	public Double getBankTenor(){
		return bankTenor;
	}
	public void setBankTenor(Double bankTenor){
		this.bankTenor=bankTenor;
	}
	@Column(name=ColumnName.BANK_SPLIT_RATE)
	public Double getBankSplitRate(){
		return bankSplitRate;
	}
	public void setBankSplitRate(Double bankSplitRate){
		this.bankSplitRate=bankSplitRate;
	}
	@Column(name=ColumnName.CLIENT_NAME)
	public String getClientName(){
		return clientName;
	}
	public void setClientName(String clientName){
		this.clientName=clientName;
	}
	@Column(name=ColumnName.AMT_INSTALMENT)
	public Double getAmtInstalment(){
		return amtInstalment;
	}
	public void setAmtInstalment(Double amtInstalment){
		this.amtInstalment=amtInstalment;
	}
	@Column(name=ColumnName.AMT_MONTHLY_FEE)
	public Double getAmtMonthlyFee(){
		return amtMonthlyFee;
	}
	public void setAmtMonthlyFee(Double amtMonthlyFee){
		this.amtMonthlyFee=amtMonthlyFee;
	}
	@Column(name=ColumnName.ID_AGREEMENT)
	public Long getIdAgreement(){
		return idAgreement;
	}
	public void setIdAgreement(Long idAgreement){
		this.idAgreement=idAgreement;
	}
	@Column(name=ColumnName.REASON)
	public String getReason(){
		return reason;
	}
	public void setReason(String reason){
		this.reason=reason;
	}
	@Column(name=ColumnName.BANK_DECISION_DATE)
	public Date getBankDecisionDate(){
		return bankDecisionDate;
	}
	public void setBankDecisionDate(Date bankDecisionDate){
		this.bankDecisionDate=bankDecisionDate;
	}
	@Column(name=ColumnName.BANK_CLAWBACK_DATE)
	public Date getBankClawbackDate(){
		return bankClawbackDate;
	}
	public void setBankClawbackDate(Date bankClawbackDate){
		this.bankClawbackDate=bankClawbackDate;
	}
	@Column(name=ColumnName.BANK_CLAWBACK_AMOUNT)
	public Double getBankClawbackAmount(){
		return bankClawbackAmount;
	}
	public void setBankClawbackAmount(Double bankClawbackAmount){
		this.bankClawbackAmount=bankClawbackAmount;
	}
	@Column(name=ColumnName.BANK_PRODUCT_CODE)
	public String getBankProductCode(){
		return bankProductCode;
	}
	public void setBankProductCode(String bankProductCode){
		this.bankProductCode=bankProductCode;
	}
	@Column(name=ColumnName.BANK_REFERENCE_NO)
	public String getBankReferenceNo(){
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo){
		this.bankReferenceNo=bankReferenceNo;
	}
	@Column(name=ColumnName.CUID)
	public Long getCuid(){
		return cuid;
	}
	public void setCuid(Long cuid){
		this.cuid=cuid;
	}
}
