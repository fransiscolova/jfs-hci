package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.TableConstant.TableName;
import javax.persistence.Id;

/**
 * JfsInpayAllocationFix For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=TableName.JFS_INPAY_ALLOCATION_FIX)
public class JfsInpayAllocationFix{
	private String textContractNumber;
	private Date dueDate;
	private Integer partIndex;
	private Double amtPrincipal;
	private Double amtInterest;
	private Integer archived;
	private Date dtimeCreated;
	private Date dtimeUpdated;
	private Long incomingPaymentId;
	private Double amtOverpayment;
	@Column(name=ColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=ColumnName.DUE_DATE)
	public Date getDueDate(){
		return dueDate;
	}
	public void setDueDate(Date dueDate){
		this.dueDate=dueDate;
	}
	@Column(name=ColumnName.PART_INDEX)
	public Integer getPartIndex(){
		return partIndex;
	}
	public void setPartIndex(Integer partIndex){
		this.partIndex=partIndex;
	}
	@Column(name=ColumnName.AMT_PRINCIPAL)
	public Double getAmtPrincipal(){
		return amtPrincipal;
	}
	public void setAmtPrincipal(Double amtPrincipal){
		this.amtPrincipal=amtPrincipal;
	}
	@Column(name=ColumnName.AMT_INTEREST)
	public Double getAmtInterest(){
		return amtInterest;
	}
	public void setAmtInterest(Double amtInterest){
		this.amtInterest=amtInterest;
	}
	@Column(name=ColumnName.ARCHIVED)
	public Integer getArchived(){
		return archived;
	}
	public void setArchived(Integer archived){
		this.archived=archived;
	}
	@Column(name=ColumnName.DTIME_CREATED)
	public Date getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(Date dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
	@Column(name=ColumnName.DTIME_UPDATED)
	public Date getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(Date dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
	@Id
	@Column(name=ColumnName.INCOMING_PAYMENT_ID)
	public Long getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(Long incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=ColumnName.AMT_OVERPAYMENT)
	public Double getAmtOverpayment(){
		return amtOverpayment;
	}
	public void setAmtOverpayment(Double amtOverpayment){
		this.amtOverpayment=amtOverpayment;
	}
}
