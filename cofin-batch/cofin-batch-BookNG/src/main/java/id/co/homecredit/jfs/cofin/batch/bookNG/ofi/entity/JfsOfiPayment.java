package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.TableConstant.TableName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.entity.IdVerEntity;

/**
 * JfsOfiPayment For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=TableName.JFS_OFI_PAYMENT)
public class JfsOfiPayment extends IdVerEntity<String>{
	private String textContractNumber;
	private Date dueDate;
	private Integer partIndex;
	private Double ofiPrincipal;
	private Double ofiInterest;
	private Double totalOfiPrincipal;
	private Double totalOfiInterest;
	private Double outstandingOfi;
	private String isActive;
	@Column(name=ColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=ColumnName.DUE_DATE)
	public Date getDueDate(){
		return dueDate;
	}
	public void setDueDate(Date dueDate){
		this.dueDate=dueDate;
	}
	@Column(name=ColumnName.PART_INDEX)
	public Integer getPartIndex(){
		return partIndex;
	}
	public void setPartIndex(Integer partIndex){
		this.partIndex=partIndex;
	}
	@Column(name=ColumnName.OFI_PRINCIPAL)
	public Double getOfiPrincipal(){
		return ofiPrincipal;
	}
	public void setOfiPrincipal(Double ofiPrincipal){
		this.ofiPrincipal=ofiPrincipal;
	}
	@Column(name=ColumnName.OFI_INTEREST)
	public Double getOfiInterest(){
		return ofiInterest;
	}
	public void setOfiInterest(Double ofiInterest){
		this.ofiInterest=ofiInterest;
	}
	@Column(name=ColumnName.TOTAL_OFI_PRINCIPAL)
	public Double getTotalOfiPrincipal(){
		return totalOfiPrincipal;
	}
	public void setTotalOfiPrincipal(Double totalOfiPrincipal){
		this.totalOfiPrincipal=totalOfiPrincipal;
	}
	@Column(name=ColumnName.TOTAL_OFI_INTEREST)
	public Double getTotalOfiInterest(){
		return totalOfiInterest;
	}
	public void setTotalOfiInterest(Double totalOfiInterest){
		this.totalOfiInterest=totalOfiInterest;
	}
	@Column(name=ColumnName.OUTSTANDING_OFI)
	public Double getOutstandingOfi(){
		return outstandingOfi;
	}
	public void setOutstandingOfi(Double outstandingOfi){
		this.outstandingOfi=outstandingOfi;
	}
	@Column(name=ColumnName.IS_ACTIVE)
	public String getIsActive(){
		return isActive;
	}
	public void setIsActive(String isActive){
		this.isActive=isActive;
	}
}
