package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.TableConstant.TableName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.entity.IdEntity;

/**
 * JfsPaymentBankSim For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=TableName.JFS_PAYMENT_BANK_SIM)
public class JfsPaymentBankSim extends IdEntity<String>{
	private Long incomingPaymentId;
	private String textContractNumber;
	private Date dateInstalment;
	private Integer instalmentNumber;
	private Date paymentDate;
	private Double amtPrincipal;
	private Double amtInterest;
	private Double amtPenalty;
	private Double amtOverpayment;
	@Column(name=ColumnName.INCOMING_PAYMENT_ID)
	public Long getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(Long incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=ColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=ColumnName.DATE_INSTALMENT)
	public Date getDateInstalment(){
		return dateInstalment;
	}
	public void setDateInstalment(Date dateInstalment){
		this.dateInstalment=dateInstalment;
	}
	@Column(name=ColumnName.INSTALMENT_NUMBER)
	public Integer getInstalmentNumber(){
		return instalmentNumber;
	}
	public void setInstalmentNumber(Integer instalmentNumber){
		this.instalmentNumber=instalmentNumber;
	}
	@Column(name=ColumnName.PAYMENT_DATE)
	public Date getPaymentDate(){
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate){
		this.paymentDate=paymentDate;
	}
	@Column(name=ColumnName.AMT_PRINCIPAL)
	public Double getAmtPrincipal(){
		return amtPrincipal;
	}
	public void setAmtPrincipal(Double amtPrincipal){
		this.amtPrincipal=amtPrincipal;
	}
	@Column(name=ColumnName.AMT_INTEREST)
	public Double getAmtInterest(){
		return amtInterest;
	}
	public void setAmtInterest(Double amtInterest){
		this.amtInterest=amtInterest;
	}
	@Column(name=ColumnName.AMT_PENALTY)
	public Double getAmtPenalty(){
		return amtPenalty;
	}
	public void setAmtPenalty(Double amtPenalty){
		this.amtPenalty=amtPenalty;
	}
	@Column(name=ColumnName.AMT_OVERPAYMENT)
	public Double getAmtOverpayment(){
		return amtOverpayment;
	}
	public void setAmtOverpayment(Double amtOverpayment){
		this.amtOverpayment=amtOverpayment;
	}
}
