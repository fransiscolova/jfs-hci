package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.TableConstant.TableName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.entity.IdEntity;

/**
 * JfsPaymentBsl For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=TableName.JFS_PAYMENT_BSL)
public class JfsPaymentBsl extends IdEntity<String>{
	private Long skfInstalmentHead;
	private Long incomingPaymentId;
	private String textContractNumber;
	private Date dateInstalment;
	private Date paymentDate;
	private Date dtimePaymentPair;
	private Date dtimePaymentUnpair;
	private String textPaymentChannel;
	private String paymentTypeCode;
	private String paymentType;
	private Double totalAmt;
	private Double amtPrincipal;
	private Double amtInterest;
	private Double amtPrincipalT;
	private Double amtInterestT;
	private Double amtOverpayment;
	private Double amtPenalty;
	private Double amtOrigFee;
	private Double amtMonthlyFee;
	private Double amtEtFee;
	private Double amtMpfPenaltyFee;
	private Double amtMonthlyFeeIns;
	private Integer instalmentNumber;
	private String paymentPairedStatus;
	private String paymentCodeStatus;
	private Date dtimePaymentCanceled;
	@Column(name=ColumnName.SKF_INSTALMENT_HEAD)
	public Long getSkfInstalmentHead(){
		return skfInstalmentHead;
	}
	public void setSkfInstalmentHead(Long skfInstalmentHead){
		this.skfInstalmentHead=skfInstalmentHead;
	}
	@Column(name=ColumnName.INCOMING_PAYMENT_ID)
	public Long getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(Long incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=ColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=ColumnName.DATE_INSTALMENT)
	public Date getDateInstalment(){
		return dateInstalment;
	}
	public void setDateInstalment(Date dateInstalment){
		this.dateInstalment=dateInstalment;
	}
	@Column(name=ColumnName.PAYMENT_DATE)
	public Date getPaymentDate(){
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate){
		this.paymentDate=paymentDate;
	}
	@Column(name=ColumnName.DTIME_PAYMENT_PAIR)
	public Date getDtimePaymentPair(){
		return dtimePaymentPair;
	}
	public void setDtimePaymentPair(Date dtimePaymentPair){
		this.dtimePaymentPair=dtimePaymentPair;
	}
	@Column(name=ColumnName.DTIME_PAYMENT_UNPAIR)
	public Date getDtimePaymentUnpair(){
		return dtimePaymentUnpair;
	}
	public void setDtimePaymentUnpair(Date dtimePaymentUnpair){
		this.dtimePaymentUnpair=dtimePaymentUnpair;
	}
	@Column(name=ColumnName.TEXT_PAYMENT_CHANNEL)
	public String getTextPaymentChannel(){
		return textPaymentChannel;
	}
	public void setTextPaymentChannel(String textPaymentChannel){
		this.textPaymentChannel=textPaymentChannel;
	}
	@Column(name=ColumnName.PAYMENT_TYPE_CODE)
	public String getPaymentTypeCode(){
		return paymentTypeCode;
	}
	public void setPaymentTypeCode(String paymentTypeCode){
		this.paymentTypeCode=paymentTypeCode;
	}
	@Column(name=ColumnName.PAYMENT_TYPE)
	public String getPaymentType(){
		return paymentType;
	}
	public void setPaymentType(String paymentType){
		this.paymentType=paymentType;
	}
	@Column(name=ColumnName.TOTAL_AMT)
	public Double getTotalAmt(){
		return totalAmt;
	}
	public void setTotalAmt(Double totalAmt){
		this.totalAmt=totalAmt;
	}
	@Column(name=ColumnName.AMT_PRINCIPAL)
	public Double getAmtPrincipal(){
		return amtPrincipal;
	}
	public void setAmtPrincipal(Double amtPrincipal){
		this.amtPrincipal=amtPrincipal;
	}
	@Column(name=ColumnName.AMT_INTEREST)
	public Double getAmtInterest(){
		return amtInterest;
	}
	public void setAmtInterest(Double amtInterest){
		this.amtInterest=amtInterest;
	}
	@Column(name=ColumnName.AMT_PRINCIPAL_T)
	public Double getAmtPrincipalT(){
		return amtPrincipalT;
	}
	public void setAmtPrincipalT(Double amtPrincipalT){
		this.amtPrincipalT=amtPrincipalT;
	}
	@Column(name=ColumnName.AMT_INTEREST_T)
	public Double getAmtInterestT(){
		return amtInterestT;
	}
	public void setAmtInterestT(Double amtInterestT){
		this.amtInterestT=amtInterestT;
	}
	@Column(name=ColumnName.AMT_OVERPAYMENT)
	public Double getAmtOverpayment(){
		return amtOverpayment;
	}
	public void setAmtOverpayment(Double amtOverpayment){
		this.amtOverpayment=amtOverpayment;
	}
	@Column(name=ColumnName.AMT_PENALTY)
	public Double getAmtPenalty(){
		return amtPenalty;
	}
	public void setAmtPenalty(Double amtPenalty){
		this.amtPenalty=amtPenalty;
	}
	@Column(name=ColumnName.AMT_ORIG_FEE)
	public Double getAmtOrigFee(){
		return amtOrigFee;
	}
	public void setAmtOrigFee(Double amtOrigFee){
		this.amtOrigFee=amtOrigFee;
	}
	@Column(name=ColumnName.AMT_MONTHLY_FEE)
	public Double getAmtMonthlyFee(){
		return amtMonthlyFee;
	}
	public void setAmtMonthlyFee(Double amtMonthlyFee){
		this.amtMonthlyFee=amtMonthlyFee;
	}
	@Column(name=ColumnName.AMT_ET_FEE)
	public Double getAmtEtFee(){
		return amtEtFee;
	}
	public void setAmtEtFee(Double amtEtFee){
		this.amtEtFee=amtEtFee;
	}
	@Column(name=ColumnName.AMT_MPF_PENALTY_FEE)
	public Double getAmtMpfPenaltyFee(){
		return amtMpfPenaltyFee;
	}
	public void setAmtMpfPenaltyFee(Double amtMpfPenaltyFee){
		this.amtMpfPenaltyFee=amtMpfPenaltyFee;
	}
	@Column(name=ColumnName.AMT_MONTHLY_FEE_INS)
	public Double getAmtMonthlyFeeIns(){
		return amtMonthlyFeeIns;
	}
	public void setAmtMonthlyFeeIns(Double amtMonthlyFeeIns){
		this.amtMonthlyFeeIns=amtMonthlyFeeIns;
	}
	@Column(name=ColumnName.INSTALMENT_NUMBER)
	public Integer getInstalmentNumber(){
		return instalmentNumber;
	}
	public void setInstalmentNumber(Integer instalmentNumber){
		this.instalmentNumber=instalmentNumber;
	}
	@Column(name=ColumnName.PAYMENT_PAIRED_STATUS)
	public String getPaymentPairedStatus(){
		return paymentPairedStatus;
	}
	public void setPaymentPairedStatus(String paymentPairedStatus){
		this.paymentPairedStatus=paymentPairedStatus;
	}
	@Column(name=ColumnName.PAYMENT_CODE_STATUS)
	public String getPaymentCodeStatus(){
		return paymentCodeStatus;
	}
	public void setPaymentCodeStatus(String paymentCodeStatus){
		this.paymentCodeStatus=paymentCodeStatus;
	}
	@Column(name=ColumnName.DTIME_PAYMENT_CANCELED)
	public Date getDtimePaymentCanceled(){
		return dtimePaymentCanceled;
	}
	public void setDtimePaymentCanceled(Date dtimePaymentCanceled){
		this.dtimePaymentCanceled=dtimePaymentCanceled;
	}
}
