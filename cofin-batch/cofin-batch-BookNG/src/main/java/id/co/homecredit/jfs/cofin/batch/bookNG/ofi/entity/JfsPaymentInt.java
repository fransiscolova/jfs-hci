package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.TableConstant.TableName;

/**
 * JfsPaymentInt For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=TableName.JFS_PAYMENT_INT)
public class JfsPaymentInt{
	private Long incomingPaymentId;
	private String textContractNumber;
	private Double totalAmtPayment;
	private Double amtPayment;
	private Date datePayment;
	private Date dateExport;
	private Double pmtInstalment;
	private Double pmtFee;
	private Double pmtPenalty;
	private Double pmtOverpayment;
	private Double pmtOther;
	private Long parentId;
	private String status;
	private String reason;
	private Date dtimeStatusUpdated;
	private String paymentType;
	private Date dateBankProcess;
	private Double pmtPrincipal;
	private Double pmtInterest;
	private Date adjPaymentDate;
	@Id
	@Column(name=ColumnName.INCOMING_PAYMENT_ID)
	public Long getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(Long incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=ColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=ColumnName.TOTAL_AMT_PAYMENT)
	public Double getTotalAmtPayment(){
		return totalAmtPayment;
	}
	public void setTotalAmtPayment(Double totalAmtPayment){
		this.totalAmtPayment=totalAmtPayment;
	}
	@Column(name=ColumnName.AMT_PAYMENT)
	public Double getAmtPayment(){
		return amtPayment;
	}
	public void setAmtPayment(Double amtPayment){
		this.amtPayment=amtPayment;
	}
	@Column(name=ColumnName.DATE_PAYMENT)
	public Date getDatePayment(){
		return datePayment;
	}
	public void setDatePayment(Date datePayment){
		this.datePayment=datePayment;
	}
	@Column(name=ColumnName.DATE_EXPORT)
	public Date getDateExport(){
		return dateExport;
	}
	public void setDateExport(Date dateExport){
		this.dateExport=dateExport;
	}
	@Column(name=ColumnName.PMT_INSTALMENT)
	public Double getPmtInstalment(){
		return pmtInstalment;
	}
	public void setPmtInstalment(Double pmtInstalment){
		this.pmtInstalment=pmtInstalment;
	}
	@Column(name=ColumnName.PMT_FEE)
	public Double getPmtFee(){
		return pmtFee;
	}
	public void setPmtFee(Double pmtFee){
		this.pmtFee=pmtFee;
	}
	@Column(name=ColumnName.PMT_PENALTY)
	public Double getPmtPenalty(){
		return pmtPenalty;
	}
	public void setPmtPenalty(Double pmtPenalty){
		this.pmtPenalty=pmtPenalty;
	}
	@Column(name=ColumnName.PMT_OVERPAYMENT)
	public Double getPmtOverpayment(){
		return pmtOverpayment;
	}
	public void setPmtOverpayment(Double pmtOverpayment){
		this.pmtOverpayment=pmtOverpayment;
	}
	@Column(name=ColumnName.PMT_OTHER)
	public Double getPmtOther(){
		return pmtOther;
	}
	public void setPmtOther(Double pmtOther){
		this.pmtOther=pmtOther;
	}
	@Column(name=ColumnName.PARENT_ID)
	public Long getParentId(){
		return parentId;
	}
	public void setParentId(Long parentId){
		this.parentId=parentId;
	}
	@Column(name=ColumnName.STATUS)
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	@Column(name=ColumnName.REASON)
	public String getReason(){
		return reason;
	}
	public void setReason(String reason){
		this.reason=reason;
	}
	@Column(name=ColumnName.DTIME_STATUS_UPDATED)
	public Date getDtimeStatusUpdated(){
		return dtimeStatusUpdated;
	}
	public void setDtimeStatusUpdated(Date dtimeStatusUpdated){
		this.dtimeStatusUpdated=dtimeStatusUpdated;
	}
	@Column(name=ColumnName.PAYMENT_TYPE)
	public String getPaymentType(){
		return paymentType;
	}
	public void setPaymentType(String paymentType){
		this.paymentType=paymentType;
	}
	@Column(name=ColumnName.DATE_BANK_PROCESS)
	public Date getDateBankProcess(){
		return dateBankProcess;
	}
	public void setDateBankProcess(Date dateBankProcess){
		this.dateBankProcess=dateBankProcess;
	}
	@Column(name=ColumnName.PMT_PRINCIPAL)
	public Double getPmtPrincipal(){
		return pmtPrincipal;
	}
	public void setPmtPrincipal(Double pmtPrincipal){
		this.pmtPrincipal=pmtPrincipal;
	}
	@Column(name=ColumnName.PMT_INTEREST)
	public Double getPmtInterest(){
		return pmtInterest;
	}
	public void setPmtInterest(Double pmtInterest){
		this.pmtInterest=pmtInterest;
	}
	@Column(name=ColumnName.ADJ_PAYMENT_DATE)
	public Date getAdjPaymentDate(){
		return adjPaymentDate;
	}
	public void setAdjPaymentDate(Date adjPaymentDate){
		this.adjPaymentDate=adjPaymentDate;
	}
}
