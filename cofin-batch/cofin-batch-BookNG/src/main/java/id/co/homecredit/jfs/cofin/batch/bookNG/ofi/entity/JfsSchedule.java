package id.co.homecredit.jfs.cofin.batch.bookNG.ofi.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.ColumnConstant.ColumnName;
import id.co.homecredit.jfs.cofin.batch.bookNG.base.constant.TableConstant.TableName;
import javax.persistence.Id;

/**
 * JfsSchedule For BookNG
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=TableName.JFS_SCHEDULE)
public class JfsSchedule{
	private String textContractNumber;
	private Date dueDate;
	private Integer partIndex;
	private Double principal;
	private Double interest;
	private Double fee;
	private Double outstandingPrincipal;
	private Double prevBalance;
	private Double balance;
	private Double penalty;
	private Long ver;
	private String isActive;
	private Double intAccrual1;
	private Double intAccrual2;
	@Column(name=ColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=ColumnName.DUE_DATE)
	public Date getDueDate(){
		return dueDate;
	}
	public void setDueDate(Date dueDate){
		this.dueDate=dueDate;
	}
	@Id
	@Column(name=ColumnName.PART_INDEX)
	public Integer getPartIndex(){
		return partIndex;
	}
	public void setPartIndex(Integer partIndex){
		this.partIndex=partIndex;
	}
	@Column(name=ColumnName.PRINCIPAL)
	public Double getPrincipal(){
		return principal;
	}
	public void setPrincipal(Double principal){
		this.principal=principal;
	}
	@Column(name=ColumnName.INTEREST)
	public Double getInterest(){
		return interest;
	}
	public void setInterest(Double interest){
		this.interest=interest;
	}
	@Column(name=ColumnName.FEE)
	public Double getFee(){
		return fee;
	}
	public void setFee(Double fee){
		this.fee=fee;
	}
	@Column(name=ColumnName.OUTSTANDING_PRINCIPAL)
	public Double getOutstandingPrincipal(){
		return outstandingPrincipal;
	}
	public void setOutstandingPrincipal(Double outstandingPrincipal){
		this.outstandingPrincipal=outstandingPrincipal;
	}
	@Column(name=ColumnName.PREV_BALANCE)
	public Double getPrevBalance(){
		return prevBalance;
	}
	public void setPrevBalance(Double prevBalance){
		this.prevBalance=prevBalance;
	}
	@Column(name=ColumnName.BALANCE)
	public Double getBalance(){
		return balance;
	}
	public void setBalance(Double balance){
		this.balance=balance;
	}
	@Column(name=ColumnName.PENALTY)
	public Double getPenalty(){
		return penalty;
	}
	public void setPenalty(Double penalty){
		this.penalty=penalty;
	}
	@Column(name=ColumnName.VER)
	public Long getVer(){
		return ver;
	}
	public void setVer(Long ver){
		this.ver=ver;
	}
	@Column(name=ColumnName.IS_ACTIVE)
	public String getIsActive(){
		return isActive;
	}
	public void setIsActive(String isActive){
		this.isActive=isActive;
	}
	@Column(name=ColumnName.INT_ACCRUAL_1)
	public Double getIntAccrual1(){
		return intAccrual1;
	}
	public void setIntAccrual1(Double intAccrual1){
		this.intAccrual1=intAccrual1;
	}
	@Column(name=ColumnName.INT_ACCRUAL_2)
	public Double getIntAccrual2(){
		return intAccrual2;
	}
	public void setIntAccrual2(Double intAccrual2){
		this.intAccrual2=intAccrual2;
	}
}
