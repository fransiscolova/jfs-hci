package id.co.homecredit.jfs.cofin.batch.bookNG.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service interface for triggering spring batch.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface BatchProcessBookNGMonthlyService {

    /**
     * Trigger quartz job to execute spring batch job once.
     *
     * @throws Exception
     */
    @Async
    public void runJobOnce() throws Exception;

    /**
     * Trigger quartz job to execute spring batch job using cron expression.
     *
     * @param jobName
     */
    @Async
    public void runJobWithQuartz(String jobName);

    /**
     * Trigger quartz job to execute spring batch specific step once.
     *
     * @param jobName
     * @param stepName
     * @throws Exception
     */
    @Async
    public void runStepOnce(String jobName, String stepName) throws Exception;
}
