package id.co.homecredit.jfs.cofin.batch.bookNG.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.batch.base.model.JfsAgreement;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsProduct;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;


@Transactional
public interface JfsNotificationService {
	
	
	
	    public List<JfsOfi> getOfi();
	    
	    
	    public Partner getPartner(String paRtnerCode);
	    
	    
	    public List<JfsContract> getJfsContract(Date bankDecisionDate);
	    
	    
	    public List<JfsAgreement> getJfsAgreementById(String id);
	    
	    
	    public List<JfsProduct> getJfsProductByIdAgreement(int idAgreement);

}
