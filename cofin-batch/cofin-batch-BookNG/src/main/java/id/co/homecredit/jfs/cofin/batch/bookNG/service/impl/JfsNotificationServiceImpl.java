package id.co.homecredit.jfs.cofin.batch.bookNG.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.batch.base.dao.JfsAgreementDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.JfsNotificationDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.JfsOfiDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.JfsProductDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.PartnerDao;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsAgreement;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsProduct;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.JfsNotificationService;


@Service
public class JfsNotificationServiceImpl implements JfsNotificationService {

    @Autowired
    private JfsOfiDao jfsOfiDao;
    
    @Autowired
    private PartnerDao partnerDao;
    
    @Autowired 
    private JfsNotificationDao jfsNotificationDao;
    
    @Autowired 
    private JfsAgreementDao jfsAgreementDao;
    
    @Autowired
    private JfsProductDao jfsProductDao;
    
	@Override
	public List<JfsOfi> getOfi() {
		// TODO Auto-generated method stub
		return jfsOfiDao.getOfi();
	}

	@Override
	public Partner getPartner(String paRtnerCode) {
		// TODO Auto-generated method stub
		return partnerDao.getPartnerByCode("BTPN");
	}

	@Override
	public List<JfsContract> getJfsContract(Date bankDecisionDate) {
		// TODO Auto-generated method stub
		return jfsNotificationDao.getContractByBankDecisionDate(bankDecisionDate);
	}

	@Override
	public List<JfsAgreement> getJfsAgreementById(String id) {
		// TODO Auto-generated method stub
		return jfsAgreementDao.getJfsAgreementById(id);
	}

	@Override
	public List<JfsProduct> getJfsProductByIdAgreement(int idAgreement) {
		// TODO Auto-generated method stub
		return jfsProductDao.getJfsProduct(idAgreement);
	}

}
