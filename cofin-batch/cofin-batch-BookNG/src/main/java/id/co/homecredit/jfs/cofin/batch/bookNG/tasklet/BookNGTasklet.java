package id.co.homecredit.jfs.cofin.batch.bookNG.tasklet;

import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.tasklet.BaseTasklet;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.JfsNotificationService;
import id.co.homecredit.jfs.cofin.batch.bookNG.util.BatchBookNGPropertyUtil;

/**
 * Batch step to be extended by other step in PERMATA.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class BookNGTasklet extends BaseTasklet {
    private static final Logger log = LogManager.getLogger(BookNGTasklet.class);

    
   


    /**
     * Send email when row numbers are inconsistent before throw exception. Send email using batch
     * BTPN configuration.
     *
     * @param message
     * @param sql
     * @throws EmailException
     */
    protected void sendEmailThenThrowException(String message, String... sql)
            throws EmailException {
        String subject = BatchBookNGPropertyUtil.get("email.subject.disbursement.rows");
        String to = BatchBookNGPropertyUtil.get("email.to.it.bau");
        String fromEmail = BatchBookNGPropertyUtil.get("email.from.email");
        String fromName = BatchBookNGPropertyUtil.get("email.from.name");
        sendEmailThenThrowException(subject, to, message, fromEmail, fromName, sql);
    }
}
