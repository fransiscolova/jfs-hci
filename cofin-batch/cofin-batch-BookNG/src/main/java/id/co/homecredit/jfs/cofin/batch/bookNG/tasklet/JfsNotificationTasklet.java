package id.co.homecredit.jfs.cofin.batch.bookNG.tasklet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.homecredit.jfs.cofin.batch.base.model.JfsAgreement;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsProduct;
import id.co.homecredit.jfs.cofin.batch.base.util.JmsEngine;
import id.co.homecredit.jfs.cofin.batch.bookNG.constanta.BookNGConstant;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.BatchProcessBookNGDailyService;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.JfsNotificationService;
import id.co.homecredit.jfs.cofin.batch.bookNG.util.BatchBookNGPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Batch step to process payment for PERMATA.
 *
 * @author fransisco.situmorang01
 *
 */
@Component
public class JfsNotificationTasklet extends BookNGTasklet {
	private static final Logger log = LogManager.getLogger(JfsNotificationTasklet.class);

	@Autowired
	JfsNotificationService jfsNotificationService;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		super.execute(contribution, chunkContext);
		sendNotification();
		return RepeatStatus.FINISHED;
	}

	/**
	 * Generate all files by properties.
	 *
	 * @param properties
	 * @throws Exception
	 */
	private void sendNotification() throws Exception {
		log.info("JMS Processing JFSNotification");

		// Generate payment header file
		// SEND OFI BOOKNG HERE

		try {

			// String host = "t3://" + "10.56.5.202:15001";
			String host = "t3://" + "10.56.5.202:15001";

			JmsEngine jmsEngine = new JmsEngine();
			jmsEngine.setContext(host);

			List<JfsContract> jfsContract = jfsNotificationService.getJfsContract(new Date());
			log.info(jfsContract.size());

			for (JfsContract jfs : jfsContract) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				JSONObject objJointNotification = new JSONObject();
				JSONObject objResponse = new JSONObject();
				JSONObject objCompanyShare = new JSONObject();
				
				List<JfsAgreement> jfsAgreements = jfsNotificationService.getJfsAgreementById(jfs.getIdAgreement());
				for (JfsAgreement jfsAgreement : jfsAgreements) {
					objJointNotification.put("jfsPartner", jfsAgreement.getBankCode());

					List<JfsProduct> jfsProducts = jfsNotificationService
							.getJfsProductByIdAgreement(Integer.parseInt(jfs.getIdAgreement()));
					for (JfsProduct jfsProduct : jfsProducts) {

						// get Fee
						JSONObject objCompanyShareDetail = new JSONObject();
						objCompanyShareDetail.put("company", jfsProduct.getBankProductCode().split("_")[0]);
						objCompanyShareDetail.put("itemType", "INSTALMENT_");
						objCompanyShareDetail.put("installmentPartType", "F");
						objCompanyShareDetail.put("traifItemTypeCode", "-");
						objCompanyShareDetail.put("sharePercentage", jfsProduct.getAdminFeeRate());
						objCompanyShare.put("fee", objCompanyShareDetail);

						// get Interest
						objCompanyShareDetail = new JSONObject();
						objCompanyShareDetail.put("company", jfsProduct.getBankProductCode().split("_")[0]);
						objCompanyShareDetail.put("itemType", "PRINCIPAL_PART");
						objCompanyShareDetail.put("installmentPartType", "I");
						objCompanyShareDetail.put("traifItemTypeCode", "-");
						objCompanyShareDetail.put("sharePercentage", jfsProduct.getInterestRate());
						objCompanyShare.put("interest", objCompanyShareDetail);

						// get penalty split rate partner
						objCompanyShareDetail = new JSONObject();
						objCompanyShareDetail.put("company", jfsProduct.getBankProductCode().split("_")[0]);
						objCompanyShareDetail.put("itemType", "INSTALMENT_PART");
						objCompanyShareDetail.put("installmentPartType", "P");
						objCompanyShareDetail.put("traifItemTypeCode", "-");
						objCompanyShareDetail.put("sharePercentage", jfsProduct.getPenaltySplitRate());
						objCompanyShare.put("penalty", objCompanyShareDetail);

						// get penalty split rate HCI
						objCompanyShareDetail = new JSONObject();
						objCompanyShareDetail.put("company", "HCID");
						objCompanyShareDetail.put("itemType", "INSTALMENT_PART");
						objCompanyShareDetail.put("installmentPartType", "P");
						objCompanyShareDetail.put("traifItemTypeCode", "-");
						objCompanyShareDetail.put("sharePercentage", jfsProduct.getPenaltySplitRateHci());
						objCompanyShare.put("penaltyHci", objCompanyShareDetail);

						// get principal split rate HCI
						objCompanyShareDetail = new JSONObject();
						objCompanyShareDetail.put("company", jfsProduct.getBankProductCode().split("_")[0]);
						objCompanyShareDetail.put("itemType", "PRINCIPAL_PART");
						objCompanyShareDetail.put("installmentPartType", "S");
						objCompanyShareDetail.put("traifItemTypeCode", "-");
						objCompanyShareDetail.put("sharePercentage", jfsProduct.getPrincipalSplitRate());
						objCompanyShare.put("principal", objCompanyShareDetail);

						objCompanyShareDetail = new JSONObject();
						objCompanyShareDetail.put("company", "HCID");
						objCompanyShareDetail.put("itemType", "PRINCIPAL_PART");
						objCompanyShareDetail.put("installmentPartType", "S");
						objCompanyShareDetail.put("traifItemTypeCode", "-");
						objCompanyShareDetail.put("sharePercentage", jfsProduct.getPrincipalSplitRateHci());
						objCompanyShare.put("principalHci", objCompanyShareDetail);

					}
					
					
					objJointNotification.put("companyShares", objCompanyShare);
				}

				String uid = UUID.randomUUID().toString();
				objJointNotification.put("id", uid);
				objJointNotification.put("contract_number", jfs.getTextContractNumber());
				objJointNotification.put("eventDate", new Date());
				objJointNotification.put("eventType", "APPROVAL");
				objResponse.put("jfsNotification", objJointNotification);
				// send data
				jmsEngine.sendMessage(objResponse, uid);

			}

			log.info("done send OFI");

			jmsEngine.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
