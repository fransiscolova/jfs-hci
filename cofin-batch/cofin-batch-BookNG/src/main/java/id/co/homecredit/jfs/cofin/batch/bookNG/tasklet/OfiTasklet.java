package id.co.homecredit.jfs.cofin.batch.bookNG.tasklet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.util.JmsEngine;
import id.co.homecredit.jfs.cofin.batch.bookNG.constanta.BookNGConstant;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.BatchProcessBookNGDailyService;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.JfsNotificationService;
import id.co.homecredit.jfs.cofin.batch.bookNG.util.BatchBookNGPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Batch step to process payment for PERMATA.
 *
 * @author fransisco.situmorang01
 *
 */
@Component
public class OfiTasklet extends BookNGTasklet {
	private static final Logger log = LogManager.getLogger(OfiTasklet.class);
	
	
	 @Autowired
	 JfsNotificationService jfsNotificationService;
	 
	 
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		super.execute(contribution, chunkContext);

		sendOfiPayment();
		return RepeatStatus.FINISHED;
	}

	/**
	 * Generate all files by properties.
	 *
	 * @param properties
	 * @throws Exception
	 */
	private void sendOfiPayment() throws Exception {
		log.info("JMS Processing OFI");

		// Generate payment header file
		// SEND OFI BOOKNG HERE

		try {

			//String host = "t3://" + "10.56.5.202:15001";
			String host="t3://" + "10.56.5.202:15001";
			

			JmsEngine jmsEngine = new JmsEngine();
			jmsEngine.setContext(host);
			

			List<JfsOfi> JfsOfiData = jfsNotificationService.getOfi();
			log.info(JfsOfiData.size());
			
			
			
			for (JfsOfi ofi : JfsOfiData) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				JSONObject objResponse = new JSONObject();
				JSONObject obj = new JSONObject();
				JSONObject objMetadata = new JSONObject();
				
				String uid= UUID.randomUUID().toString();
				JSONObject objAtribut=new JSONObject();
				objAtribut.put("messageId",uid);
				objAtribut.put("timeStamp",timestamp);
				objAtribut.put("messageName", "OFIService");
				
				JSONObject objMetaData=new JSONObject();
								
				objMetaData.put("contractCode", ofi.getTextContractNumber());
				objMetaData.put("jfsPartner", "BTPN");
				objMetaData.put("billingDate", DateUtil.dateToString(new Date()));			    
			    BigDecimal  otherFinanceIncomeAmmount=ofi.getOfiAcr1().add(ofi.getOfiAcr2());
			    JSONObject objOfiAmount = new JSONObject();				
			    objOfiAmount.put("value",otherFinanceIncomeAmmount.toString());
			    objOfiAmount.put("currency", "IDR");			    
			    objMetaData.put("otherFinanceIncomeAmmount", objOfiAmount);

			    			    
			    obj.put("contextAttribute",objAtribut);
			    obj.put("contextMetadata", objMetaData);
			    objResponse.put("ofiResponse", obj);		
			    
			    
			  	//send data					
			    jmsEngine.sendMessage(objResponse, uid);
				
				
				
				
			}

			
			log.info("done send OFI");

			jmsEngine.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	
	

}
