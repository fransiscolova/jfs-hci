select
jc.bank_reference_no||';'||
jp.text_contract_number||';'||
to_char(max(jp.date_payment),'dd/mm/yyyy')||';'||
SUM((jp.pmt_instalment+jp.pmt_fee))||';'||
SUM(jp.pmt_principal)||';'||
SUM(jp.pmt_interest)||';'||
SUM(Jp.Pmt_Fee)
from jfs_payment_int jp
join jfs_contract jc on jp.text_contract_number=jc.text_contract_number and Jc.bank_product_code='PERMATA_PRODUCT'
where trunc(jp.date_export)=TRUNC(SYSDATE) and jp.payment_type='T'
      and jp.pmt_principal>=0 and jp.pmt_interest>=0
GROUP BY JP.TEXT_CONTRACT_NUMBER,JC.BANK_REFERENCE_NO
ORDER BY JP.TEXT_CONTRACT_NUMBER