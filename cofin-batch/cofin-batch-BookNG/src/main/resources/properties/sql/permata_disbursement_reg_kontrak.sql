--contract records
select 
RPAD(c.contract_code,17,' ')--NO PIN MF
||' '||RPAD(case when upper(trim(cs.name1))=upper(trim(cs.name2)) then cs.name1 else cs.full_name end,50,' ')--NAME
--||' '||RPAD(NVL(upper(sad.district),' '),30,' ')--BRANCH CODE
||' '||RPAD(NVL(dat.dst_code,' '),4,' ') -- CITY CODE
||' '||RPAD(c.contract_code,17,' ')--NO PK ENDUSER
||' '||RPAD(to_char( case when (to_char(cst.creation_date,'DD')) in ('29','30','31')
					then ADD_MONTHS(i.due_date, -1)	
				else cst.creation_date
				end
				,'dd/MM/yyyy'),10,' ')--EFFECTIVE DATE END USER
	   /* to_char(cst.creation_date,'dd/MM/yyyy') */
||' '||RPAD(fp.credit_amount,12,' ')--INITIAL PRINCIPLE AT MF
||' '||RPAD(fp.goods_price_amount,12,' ')--GODDS VALUE
||' '||RPAD(fp.terms,3,' ')--MF INITIAL TENOR
||' '||RPAD(fp.annuity_amount,12,' ')--AMOUNT INSTALLMENT TO MF
||' '||trim(to_char(case when (power(1+fp.interest_rate,1/12)-1)*1200>100 then 99 else (power(1+fp.interest_rate,1/12)-1)*1200 end,'00.00'))--MF INTEREST TO END USER
||' '||RPAD(fp.credit_amount,12,' ')--PRINCIPLE UPON ASSIGNMENT
||' '||RPAD(jc.send_principal,12,' ')--BANK FINANCED PRINCIPAL
||' '||RPAD(fp.terms,3,' ')--TENOR WITH BANK
||' '||RPAD(to_char(i.due_date,'dd/MM/yyyy'),10,' ')--INSTALLMENT PAYMENT DATE 1P
||' '||RPAD(to_char(i1.due_date,'dd/MM/yyyy'),10,' ')--FINAL INSTALLMENT DATE
||' '||trim(to_char(jc.send_rate,'00.00'))--INTEREST AT THE BANK
||' '||RPAD(jc.send_instalment,12,' ')--AMOUNT OF INSTALLMENT TO BANK
||' '||'1 '--INSTALLMENT PERIOD
||' '||'N'--VEHICLE CONDITION
||' '||'T'--TYPE OF CLOSING
||' '||RPAD(0,12,' ')--INSURANCE PREMIUM AMOUNT
||' '||'E'--PREMIUM PAYMENT
||' '||'DG'--VEHICLE CLASS
||' '||'0 '--CREDIT SCORING
||' '||RPAD(' ',5,' ')--DEBT BURDEN RATIO
||' '||RPAD(to_char(cst.creation_date,'dd/MM/yyyy'),10,' ')--END USER LOAN AGREEMENT DATE
	   /* to_char(cst.creation_date,'dd/MM/yyyy') */
||' '||RPAD(0,12,' ')--AMOUNT PREMIUM PAID
||' '||RPAD(nvl(fp.cash_payment_amount, 0)-nvl((fp.advanced_payment_number * fp.annuity_amount), 0)-nvl(ft.AMT_FEES_IN_ADVANCE, 0),12,' ')--AMOUNT DP
||' '||RPAD(' ',10,' ')--INSURANCE COMPANY
||' '||RPAD(es.bi_economic_sector_code,10,' ')--AREA OF BUSINESS
||' '||RPAD('K',10,' ')--PURPOSE OF USAGE
||' '||' '--DOCUMENT PROOF OF INCOME
||' '||' '--PBB
||' '||' '--SPOUSE KTP
||' '||' '--LICENCING
||' '||' '--DEED
||' '||' '--NPWP
||' '||' '--FINANCING APPLICATION FORM/FPP
||' '||' '--FIDUCIARY
||' '||'0'--IN ARREAR
||' '||RPAD(
        case when fp.goods_price_amount<fp.credit_amount then fp.goods_price_amount+NVL(insurance.amt_premium,0)
        else fp.goods_price_amount end
        ,12,' ')--GOODS VALUE + INSURANCE
||chr(13)
as KONTRAK
from JFS_CONTRACT JC
join owner_int.vh_hom_contract c  on jc.text_contract_number=c.contract_code
left join owner_int.vh_hom_salesroom vs on c.salesroom_id=vs.id 
left join salesroom_address sad on sad.code_salesroom=vs.code
left join zipcode_to_dati_ii dat on dat.zip_code=sad.zip_code
left join owner_int.vh_hom_deal d on c.deal_code=d.code 
left join owner_int.vh_hom_client_snapshot cs on d.client_snapshot_id=cs.id ----and cs.is_new=1 --disable on 21-02-2014 
left join owner_int.vh_hom_contract_status_trans cst on c.id=cst.contract_id and cst.status='N' -- N=sign
left join owner_int.vh_hom_financial_parameters fp on c.id=fp.contract_id and fp.archived = 0 -- 0 = not archived / active
left join (
          select fi.financial_parameters_id
          ,sum ( case when tt.usage_type in ('A', 'S') and tt.charging_periodicity = 'ONE_TIME' and tt.to_principal_flag = 0
                      THEN fi.ITEM_AMOUNT
                      else 0
                  end)amt_fees_in_advance 
          from owner_int.vh_hom_financial_param_item fi 
          left join owner_int.vh_hom_fin_par_item2tarif_it pt on pt.id=fi.id
          left join owner_int.vh_hom_tariff_item ti on ti.id=pt.tariff_item_id
          left join (
              ---Hardcoded beware...
              ---20160621 HoSel 2016.1. informed by Radan to replace 'tt.to_principal_flag' 
            select a.*, case when a.code='TITF_INS_PREMIUM_PRINCIPAL' then 1
                          else 0
                        end as to_principal_flag
            from owner_int.vh_hom_tariff_item_type a                             
          ) tt on tt.code=ti.tariff_item_type_code and tt.active_flag=1
          --left join owner_int.vh_hom_tariff_item_type tt on tt.code=ti.tariff_item_type_code and tt.active_flag=1
          group by fi.financial_parameters_id
)ft on ft.financial_parameters_id=fp.id
left join owner_int.vh_hom_installment i on c.id=i.contract_id and i.installment_version=1 and i.installment_number=1
left join owner_int.vh_hom_installment i1 on c.id=i1.contract_id and i1.installment_version=1 and i1.installment_number=fp.terms
left join 
      (select contract_id,commodity_type_id,row_number() over (partition by contract_id order by price_amount desc) price_order
      from owner_int.vh_hom_commodity) hc on c.id=hc.contract_id and hc.price_order=1
left join owner_int.vh_hom_commodity_type hct on hc.commodity_type_id=hct.id
left join JFS_COMMODITY_GROUP cg on hct.commodity_category_code=cg.hci_code_commodity_category and jc.id_agreement = cg.id_agreement
left join (
    select
    fca.text_contract_number,sum(fia.amt_premium) amt_premium
    from OWNER_DWH.f_contract_ad fca
    join jfs_contract jc on jc.text_contract_number=fca.text_contract_number and jc.bank_product_code='PERMATA_PRODUCT'
    join OWNER_DWH.f_insurance_at fia on fca.skp_credit_case = fia.skp_credit_case and fia.code_insurance_company_payment <> 'XNA'
    join OWNER_DWH.dc_service ds on fia.skp_service = ds.skp_service
    group by fca.text_contract_number
) INSURANCE
ON INSURANCE.TEXT_CONTRACT_NUMBER=JC.TEXT_CONTRACT_NUMBER
left join JFS_ECONOMIC_SECTOR es on cg.btpn_commodity_group=es.bank_commodity_group
--where trunc(jc.export_date)=trunc(sysdate) 
where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement=51 and jc.status='S'
order by jc.id_agreement, c.contract_code