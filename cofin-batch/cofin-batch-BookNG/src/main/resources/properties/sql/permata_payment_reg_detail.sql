select replace(jc.BANK_REFERENCE_NO,chr(13),'')||';'||jp.text_contract_number||';'||jp.date_payment||';'||jp.pmt_instalment
from (select text_contract_number,sum(pmt_instalment) pmt_instalment, sum(pmt_penalty) pmt_penalty, min(date_payment) date_payment from jfs_payment_int 
    where trunc(Date_export)=trunc(sysdate) and payment_type='R'
    and pmt_instalment>0
    group by text_contract_number
    union all
    select text_contract_number,sum(pmt_instalment) pmt_instalment, sum(pmt_penalty) pmt_penalty, min(date_payment) date_payment from jfs_payment_int 
    where trunc(date_export)=trunc(sysdate) and payment_type='R'
    and pmt_instalment<0
    group by text_contract_number) jp
join jfs_contract jc on jp.text_contract_number=jc.text_contract_number and Jc.bank_product_code='PERMATA_PRODUCT'
order by jp.date_payment