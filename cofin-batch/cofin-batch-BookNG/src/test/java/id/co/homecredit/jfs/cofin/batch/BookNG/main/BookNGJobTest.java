package id.co.homecredit.jfs.cofin.batch.BookNG.main;

import java.io.IOException;
import java.util.Set;

import org.apache.commons.mail.EmailException;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.batch.bookNG.util.BatchBookNGPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.EmailUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Test class to starting batch process for BookNG.
 *
 * @author fransisco.situmorang
 *
 */
public class BookNGJobTest extends BaseBatchTest {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Test
    public void countFileRows() throws IOException {
        String pathname = "/var/data/JFS/Export/ENDUSER_ALL_2017-11-23.txt";
        System.out.println(FileUtil.countLines(pathname));
    }

    @Test
    public void getAllKeys() throws Exception {
        String filePath = "/var/data/jfs/properties/permata.reg.properties";
        Set<Object> keys = BatchBookNGPropertyUtil.getAllKeys(filePath);
        for (Object obj : keys) {
            String key = (String) obj;
            System.out.println(key);
        }
    }

    @Test
    public void sendHtmlEmail() throws EmailException {
        String message = "test";
        String hostname = BatchBookNGPropertyUtil.get("smtp.host.name");
        Integer port = Integer.parseInt(BatchBookNGPropertyUtil.get("smtp.port"));
        String username = BatchBookNGPropertyUtil.get("smtp.username");
        String password = BatchBookNGPropertyUtil.get("smtp.password");
        String fromEmail = BatchBookNGPropertyUtil.get("smtp.from.email");
        String fromName = BatchBookNGPropertyUtil.get("smtp.from.name");
        String bounceEmail = BatchBookNGPropertyUtil.get("smtp.bounce.email");
        String textMessage = BatchBookNGPropertyUtil.get("smtp.text");
        EmailUtil emailUtil = new EmailUtil(hostname, port, username, password, fromEmail, fromName,
                bounceEmail, textMessage);

        String subject = BatchBookNGPropertyUtil
                .get("email.subject.disbursement.inconsistent.rows");
        String to = BatchBookNGPropertyUtil.get("email.to");
        emailUtil.sendHtmlEmail(subject, message, to, null, null, new String[0]);
    }

    @Test
    public void testLaunchJob() throws Exception {
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.assertEquals(jobExecution.getStatus(), BatchStatus.COMPLETED);
    }

    @Test
    public void testLaunchStep() throws Exception {
        JobExecution jobExecution = jobLauncherTestUtils.launchStep("continuity");
        Assert.assertEquals(jobExecution.getStatus(), BatchStatus.COMPLETED);
    }
}
