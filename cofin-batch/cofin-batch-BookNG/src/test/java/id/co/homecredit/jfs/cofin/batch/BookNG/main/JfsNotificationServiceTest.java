package id.co.homecredit.jfs.cofin.batch.BookNG.main;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.mail.EmailException;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.batch.base.model.JfsAgreement;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsProduct;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.BatchProcessBookNGDailyService;
import id.co.homecredit.jfs.cofin.batch.bookNG.service.JfsNotificationService;
import id.co.homecredit.jfs.cofin.batch.bookNG.util.BatchBookNGPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.EmailUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Test class to starting batch process for BookNG.
 *
 * @author fransisco.situmorang
 *
 */
public class JfsNotificationServiceTest extends BaseBatchTest {

    @Autowired
    private JfsNotificationService jfsNotificationService;

    /* 
     * get all partner 
     */
    @Test
    public void getAllPartner() throws IOException {
        Partner partners=jfsNotificationService.getPartner("BTPN");
        Assert.assertEquals(partners.getCode(),"BTPN");
    }
    
    @Test
    private void getJfsContractByDecisionDate() throws ParseException {    		
    	List<JfsContract> jfsContracts =jfsNotificationService.getJfsContract(new Date());    
    	System.out.println("Size jfscontract " + (jfsContracts.size()));
    	Assert.assertEquals(true, jfsContracts.size()>0);
    }

   
    @Test
    private void getAgreementById() {   	
    	List<JfsAgreement> jfsAgreements=jfsNotificationService.getJfsAgreementById("51");
    	Assert.assertEquals("NO. PKS ICF/17/2090/N/RB", jfsAgreements.get(0).getCodeAgreement());
    }
    
    
    @Test
    private void getProductByIdAgremeent() {
    	List<JfsProduct> products =jfsNotificationService.getJfsProductByIdAgreement(51);
    	Assert.assertEquals(true,products.size()>0);
    	
    }
    
}
