package id.co.homecredit.jfs.cofin.batch.base.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.batch.base.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.batch.base.model.Agreement;
import id.co.homecredit.jfs.cofin.common.dao.Dao;

/**
 * Dao interface class for {@link Agreement}.
 *
 * @author muhammad.muflihun
 *
 */
public interface AgreementDao extends Dao<Agreement, String> {

    /**
     * Get agreement by alias id.
     *
     * @param aliasId
     * @return agreement
     */
    public Agreement getAgreementByAliasId(String aliasId);

    /**
     * Get agreement by agreement id with fetched all agreement details.
     *
     * @param agreementId
     * @return agreement
     */
    public Agreement getAgreementWithFetchedDetailsById(String agreementId);

    /**
     * Get agreement by agreement id with fetched all product mapping.
     *
     * @param agreementId
     * @return agreement
     */
    public Agreement getAgreementWithFetchedProductMappingsById(String agreementId);

    /**
     * Get all agreements by partner code.
     *
     * @param partnerCode
     * @return agreements
     */
    public List<Agreement> getAllAgreementsByPartnerCode(String partnerCode);

    /**
     * Is agreement exist by partner code and agreement type.
     *
     * @param partnerCode
     * @param agreementType
     * @return
     */
    public Boolean isAgreementExistByPartnerCodeAndAgreementType(String partnerCode,
            AgreementTypeEnum agreementType);

}
