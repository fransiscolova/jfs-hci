package id.co.homecredit.jfs.cofin.batch.base.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.batch.base.model.BatchStep;
import id.co.homecredit.jfs.cofin.common.dao.Dao;

/**
 * Dao interface class for {@link BatchStep}.
 *
 * @author muhammad.muflihun
 *
 */
public interface BatchStepDao extends Dao<BatchStep, String> {

    /**
     * Get all batch step ordered by priority and partner name.
     *
     * @param partnerName
     * @return batchSteps
     */
    public List<BatchStep> getAllBatchStepPrioritizedByPartnerName(String partnerName);

    /**
     * Get next step name by job name and step name.
     *
     * @param jobName
     * @param stepName
     * @return stepName
     */
    public String getNextStepNameByJobNameAndStepName(String jobName, String stepName);

    /**
     * Get priority by job name and step name.
     *
     * @param jobName
     * @param stepName
     * @return
     */
    public Integer getPriorityByJobNameAndStepName(String jobName, String stepName);

    /**
     * Check if step name exist by job name and step name. Return true if exist.
     *
     * @param jobName
     * @param stepName
     * @return boolean
     */
    public Boolean isStepNameExist(String jobName, String stepName);

}
