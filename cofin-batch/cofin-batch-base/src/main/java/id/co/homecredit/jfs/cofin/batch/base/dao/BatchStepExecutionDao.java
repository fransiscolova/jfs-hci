package id.co.homecredit.jfs.cofin.batch.base.dao;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.batch.base.model.BatchStepExecution;

/**
 * Dao interface class for {@link BatchStepExecution}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface BatchStepExecutionDao {

    /**
     * Get batch step execution.
     *
     * @param stepExecutionId
     * @return batchStepExecution
     */
    public BatchStepExecution get(Long stepExecutionId);

    /**
     * Get last step name exclude continuity by job execution id.
     *
     * @param jobExecutionId
     * @return
     */
    public String getLastStepNonContinuityByJobExecutionId(Long jobExecutionId);

    /**
     * Get last step name exclude continuity by job name.
     *
     * @param jobName
     * @return stepName
     */
    public String getLastStepNonContinuityByJobName(String jobName);

    /**
     * Save batch step execution.
     *
     * @param batchStepExecution
     * @return batchStepExecution
     */
    public BatchStepExecution save(BatchStepExecution batchStepExecution);
}
