package id.co.homecredit.jfs.cofin.batch.base.dao;

import org.springframework.transaction.annotation.Transactional;

/**
 * Dao interface class for executing native query.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CommonBatchDao {

    /**
     * Execute SQL procedure query with given param.
     *
     * @param query
     * @return object
     * @return object(s)
     */
    public Integer executeProcedureWithParam(String query, Object param);

    /**
     * Execute SQL query.
     *
     * @param query
     * @return object(s)
     */
    public Object executeQuery(String query);

    /**
     * Execute SQL query then transform it to given class.
     *
     * @param query
     * @param clazz
     * @return object(s)
     */
    public Object executeQueryWithTransformer(String query, Class<?> clazz);

    /**
     * Execute SQL query update.
     *
     * @param query
     * @return object(s)
     */
    public Integer executeUpdate(String query);
}
