package id.co.homecredit.jfs.cofin.batch.base.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.batch.base.model.ExportLog;
import id.co.homecredit.jfs.cofin.common.dao.LogDao;

/**
 * Dao interface class for {@link ExportLog}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ExportLogDao extends LogDao<ExportLog, String> {

    /**
     * Save export log with new propagation.
     *
     * @param exportLog
     * @return exportLog
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ExportLog saveWithNewPropagation(ExportLog exportLog);
}
