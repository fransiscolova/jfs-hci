package id.co.homecredit.jfs.cofin.batch.base.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.tool.schema.extract.spi.ExtractionContext.DatabaseObjectAccess;

import id.co.homecredit.jfs.cofin.batch.base.model.JfsAgreement;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.common.dao.DaoTextNumber;


/**
 * Dao interface class for {@link Partner}.
 *
 * @author fransisco.situmorang
 *
 */
public interface JfsAgreementDao extends DaoIdOnly<JfsAgreement, String> {

    /**
     * Get partner by {@code CODE}.
     *
     * @param code
     * @return partner
     */
    public List<JfsAgreement> getJfsAgreementById(String id);

   
}
