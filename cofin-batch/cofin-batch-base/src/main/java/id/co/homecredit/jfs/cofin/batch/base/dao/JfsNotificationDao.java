package id.co.homecredit.jfs.cofin.batch.base.dao;

import java.util.Date;
import java.util.List;

import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.common.dao.DaoTextNumber;


/**
 * Dao interface class for {@link Partner}.
 *
 * @author fransisco.situmorang
 *
 */
public interface JfsNotificationDao extends DaoTextNumber<JfsContract, String> {

    /**
     * Get jfsContract by {@code CODE}.
     *
     * @param code
     * @return List<jfsContract>
     */
    public List<JfsContract> getContractByBankDecisionDate(Date date);

   
}
