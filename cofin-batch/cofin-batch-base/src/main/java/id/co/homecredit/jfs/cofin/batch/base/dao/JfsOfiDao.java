package id.co.homecredit.jfs.cofin.batch.base.dao;

import java.util.List;


import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.common.dao.DaoTextNumber;


/**
 * Dao interface class for {@link Partner}.
 *
 * @author muhammad.muflihun
 *
 */
public interface JfsOfiDao extends DaoTextNumber<JfsOfi, String> {

    /**
     * Get partner by {@code CODE}.
     *
     * @param code
     * @return partner
     */
    public List<JfsOfi> getOfi();

   
}
