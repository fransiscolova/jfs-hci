package id.co.homecredit.jfs.cofin.batch.base.dao;

import java.util.Date;
import java.util.List;

import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsProduct;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.common.dao.DaoTextNumber;


/**
 * Dao interface class for {@link JfsProductDao}.
 *
 * @author fransisco.situmorang
 *
 */
public interface JfsProductDao extends DaoIdOnly<JfsProduct, String> {

    /**
     * Get jfsContract by {@code CODE}.
     *
     * @param idAgreement
     * @return List<jfsProduct>
     */
    public List<JfsProduct> getJfsProduct(int idAgreement);

    
    
   
}
