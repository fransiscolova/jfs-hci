package id.co.homecredit.jfs.cofin.batch.base.dao;

import java.util.Date;

import id.co.homecredit.jfs.cofin.batch.base.model.PublicHoliday;
import id.co.homecredit.jfs.cofin.common.dao.Dao;

/**
 * Dao interface class for {@link PublicHoliday}.
 *
 * @author muhammad.muflihun
 *
 */
public interface PublicHolidayDao extends Dao<PublicHoliday, String> {

    /**
     * Get public holiday by date.
     *
     * @param date
     * @return public holiday
     */
    public PublicHoliday getPublicHoliday(Date date);

    /**
     * Check if date is public holiday. Return true if date is exist in public holiday.
     *
     * @param date
     * @return boolean
     */
    public Boolean isPublicHoliday(Date date);

}
