package id.co.homecredit.jfs.cofin.batch.base.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.batch.base.dao.BatchStepDao;
import id.co.homecredit.jfs.cofin.batch.base.model.BatchJob;
import id.co.homecredit.jfs.cofin.batch.base.model.BatchStep;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;

/**
 * Dao implement class for {@link BatchJob}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class BatchStepDaoImpl extends DaoImpl<BatchStep, String> implements BatchStepDao {
    private static final Logger log = LogManager.getLogger(BatchStepDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<BatchStep> getAllBatchStepPrioritizedByPartnerName(String partnerName) {
        log.debug("get all batch step prioritized by partner name {}", partnerName);
        Criteria criteria = createCriteria();
        criteria.createAlias("batchJob.partner", "partner");
        criteria.add(Restrictions.eq("partner.code", partnerName));
        criteria.addOrder(Order.asc("priority"));
        return criteria.list();
    }

    @Override
    public String getNextStepNameByJobNameAndStepName(String jobName, String stepName) {
        log.debug("get next step name by job name {} and step name {}", jobName, stepName);
        Integer priority = getPriorityByJobNameAndStepName(jobName, stepName) + 1;

        Criteria criteria = createCriteria();
        criteria.createAlias("batchJob", "batchJob");
        criteria.add(Restrictions.eq("batchJob.jobName", jobName));
        criteria.add(Restrictions.eq("priority", priority));
        criteria.setProjection(Projections.property("stepName"));
        criteria.addOrder(Order.asc("priority"));
        return (String) criteria.uniqueResult();
    }

    @Override
    public Integer getPriorityByJobNameAndStepName(String jobName, String stepName) {
        log.debug("get priority by job name {} and step name {}", jobName, stepName);
        Criteria query = createCriteria();
        query.createAlias("batchJob", "batchJob");
        query.add(Restrictions.eq("batchJob.jobName", jobName));
        query.add(Restrictions.eq("stepName", stepName));
        query.setProjection(Projections.property("priority"));
        return (Integer) query.uniqueResult();
    }

    @Override
    public Boolean isStepNameExist(String jobName, String stepName) {
        log.debug("check if step name is exist by job name {} and step name", jobName, stepName);
        Criteria criteria = createCriteria();
        criteria.createAlias("batchJob", "batchJob");
        criteria.add(Restrictions.eq("batchJob.jobName", jobName));
        criteria.add(Restrictions.eq("stepName", stepName));
        criteria.setProjection(Projections.property("stepName"));
        return ((String) criteria.uniqueResult()) != null;
    }

}
