package id.co.homecredit.jfs.cofin.batch.base.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.batch.base.dao.BatchStepExecutionDao;
import id.co.homecredit.jfs.cofin.batch.base.model.BatchStepExecution;

/**
 * Dao implement class for {@link BatchStepExecution}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class BatchStepExecutionDaoImpl implements BatchStepExecutionDao {
    private static final Logger log = LogManager.getLogger(BatchStepExecutionDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    private Criteria createCriteria() {
        return getSession().createCriteria(BatchStepExecution.class);
    }

    @Override
    public BatchStepExecution get(Long stepExecutionId) {
        return getSession().get(BatchStepExecution.class, stepExecutionId);
    }

    @Override
    public String getLastStepNonContinuityByJobExecutionId(Long jobExecutionId) {
        log.debug("get last step name exclude continuity by job execution id {}", jobExecutionId);
        Criteria criteria = createCriteria();
        criteria.createAlias("batchJobExecution", "batchJobExecution");
        criteria.add(Restrictions.eq("batchJobExecution.jobExecutionId", jobExecutionId));
        criteria.add(Restrictions.ne("stepName", "continuity"));
        criteria.setProjection(Projections.property("stepName"));
        criteria.addOrder(Order.desc("startTime"));
        criteria.setMaxResults(1);
        return (String) criteria.uniqueResult();
    }

    @Override
    public String getLastStepNonContinuityByJobName(String jobName) {
        log.debug("get last step name exclude continuity by job name {}", jobName);
        Criteria criteria = createCriteria();
        criteria.createAlias("batchJobExecution", "batchJobExecution");
        criteria.createAlias("batchJobExecution.batchJobInstance", "batchJobInstance");
        criteria.add(Restrictions.eq("batchJobInstance.jobName", jobName));
        criteria.add(Restrictions.ne("stepName", "continuity"));
        criteria.setProjection(Projections.property("stepName"));
        criteria.addOrder(Order.desc("startTime"));
        criteria.setMaxResults(1);
        return (String) criteria.uniqueResult();
    }

    /**
     * Get current session.
     *
     * @return session
     */
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public BatchStepExecution save(BatchStepExecution batchStepExecution) {
        getSession().saveOrUpdate(batchStepExecution);
        return batchStepExecution;
    }

}
