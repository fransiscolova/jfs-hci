package id.co.homecredit.jfs.cofin.batch.base.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.batch.base.dao.ExportLogDao;
import id.co.homecredit.jfs.cofin.batch.base.model.ExportLog;
import id.co.homecredit.jfs.cofin.common.dao.impl.LogDaoImpl;

/**
 * Dao implement class for {@link ExportLog}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ExportLogDaoImpl extends LogDaoImpl<ExportLog, String> implements ExportLogDao {
    private static final Logger log = LogManager.getLogger(ExportLogDaoImpl.class);

    @Override
    public ExportLog saveWithNewPropagation(ExportLog exportLog) {
        log.debug("save {}", exportLog.toString());
        return super.save(exportLog);
    }

}
