package id.co.homecredit.jfs.cofin.batch.base.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.batch.base.dao.JfsAgreementDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.JfsNotificationDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.JfsOfiDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.PartnerDao;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsAgreement;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoTextNumberImpl;


/**
 * Dao implement class for {@link Partner}.
 *
 * @author fransisco situmorang
 *
 */
@Repository
public class JfsAgreementDaoImpl extends DaoIdOnlyImpl<JfsAgreement, String> implements JfsAgreementDao {
    private static final Logger log = LogManager.getLogger(JfsAgreementDaoImpl.class);

	
	@Override
	public List<JfsAgreement> getJfsAgreementById(String id) {
		// TODO Auto-generated method stub
		 log.debug("get jfs agreement {}");
	     Criteria criteria = createCriteria();
	     criteria.add(Restrictions.eq("idAgreement", Integer.parseInt(id)));
   		 return criteria.list();
	}

}
