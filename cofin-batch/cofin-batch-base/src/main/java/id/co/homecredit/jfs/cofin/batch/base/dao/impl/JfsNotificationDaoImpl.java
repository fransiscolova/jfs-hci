package id.co.homecredit.jfs.cofin.batch.base.dao.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.batch.base.dao.JfsNotificationDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.JfsOfiDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.PartnerDao;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoTextNumberImpl;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;

/**
 * Dao implement class for {@link Partner}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class JfsNotificationDaoImpl extends DaoTextNumberImpl<JfsContract, String> implements JfsNotificationDao {
	private static final Logger log = LogManager.getLogger(JfsNotificationDaoImpl.class);

	@Override
	public List<JfsContract> getContractByBankDecisionDate(Date date) {
		// TODO Auto-generated method stub
		log.debug("get jfs notifications  {}", date);
		String dateString = DateUtil.dateToStringWithFormat(date, "dd-MM-yyyy");
		Criteria criteria = createCriteria();
		try {
			Date toDay = DateUtil.stringToDateWithFormat(dateString, "dd-MM-yyyy");
			criteria.setMaxResults(100);
			criteria.add(Restrictions.eq("bankDecisionDate", toDay));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return criteria.list();
	}

}
