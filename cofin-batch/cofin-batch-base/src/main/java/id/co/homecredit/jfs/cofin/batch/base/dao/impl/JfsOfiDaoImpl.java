package id.co.homecredit.jfs.cofin.batch.base.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.batch.base.dao.JfsOfiDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.PartnerDao;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoTextNumberImpl;


/**
 * Dao implement class for {@link Partner}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class JfsOfiDaoImpl extends DaoTextNumberImpl<JfsOfi, String> implements JfsOfiDao {
    private static final Logger log = LogManager.getLogger(JfsOfiDaoImpl.class);

  
	@Override
	public List<JfsOfi> getOfi() {
		// TODO Auto-generated method stub
		 log.debug("get jfs ofi  {}");
	     Criteria criteria = createCriteria();
		return criteria.list();
	}

}
