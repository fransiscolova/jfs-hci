package id.co.homecredit.jfs.cofin.batch.base.dao.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.batch.base.dao.JfsNotificationDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.JfsOfiDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.JfsProductDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.PartnerDao;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsContract;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsProduct;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoTextNumberImpl;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;

/**
 * Dao implement class for {@link Product}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class JfsProductDaoImpl extends DaoIdOnlyImpl<JfsProduct, String> implements JfsProductDao {
	private static final Logger log = LogManager.getLogger(JfsProductDaoImpl.class);

	@Override
	public List<JfsProduct> getJfsProduct(int idAgreement) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria();
		criteria.add(Restrictions.eq("idAgreement", idAgreement));
		criteria.add(Restrictions.ge("validTo", new Date()));
		criteria.add(Restrictions.le("validFrom", new Date()));

		return criteria.list();
	}

}
