package id.co.homecredit.jfs.cofin.batch.base.dao.impl;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.batch.base.dao.PublicHolidayDao;
import id.co.homecredit.jfs.cofin.batch.base.model.PublicHoliday;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;

/**
 * Dao implements class for {@link PublicHoliday}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class PublicHolidayDaoImpl extends DaoImpl<PublicHoliday, String>
        implements PublicHolidayDao {
    private static final Logger log = LogManager.getLogger(PublicHolidayDaoImpl.class);

    @Override
    public PublicHoliday getPublicHoliday(Date date) {
        log.debug("get public holiday by date {}", date);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.le("validFrom", date));
        criteria.add(Restrictions.ge("validTo", date));
        return (PublicHoliday) criteria.uniqueResult();
    }

    @Override
    public Boolean isPublicHoliday(Date date) {
        log.debug("check if date {} is public holiday", date);
        return !getPublicHoliday(date).equals(null);
    }

}
