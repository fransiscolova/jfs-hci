package id.co.homecredit.jfs.cofin.batch.base.enumeration;

/**
 * File folder enumeration.
 *
 * @author muhammad.muflihun
 *
 */
public enum FileFolderEnum {
    SQL, TEMPLATE_FILE, EXPORT_FILE, EXPORT_BACKUP_FILE, EXPORT_ENCRYPT, EXPORT_ENCRYPT_BACKUP, IMPORT_FILE, IMPORT_DECRYPT;
}
