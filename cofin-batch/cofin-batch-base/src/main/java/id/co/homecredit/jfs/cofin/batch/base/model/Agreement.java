package id.co.homecredit.jfs.cofin.batch.base.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.batch.base.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.common.model.IdValidEntity;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;

/**
 * Entity class for {@code COFIN_AGREEMENT}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "COFIN_AGREEMENT")
public class Agreement extends IdValidEntity {
    private String code;
    private String name;
    private String aliasId;
    private Partner partner;
    private String description;
    private AgreementTypeEnum agreementType;

    @Enumerated(EnumType.STRING)
    @Column(name = "AGREEMENT_TYPE_CODE", nullable = false)
    public AgreementTypeEnum getAgreementType() {
        return agreementType;
    }

    @Column(name = "ALIAS_ID", nullable = false)
    public String getAliasId() {
        return aliasId;
    }

    @Column(name = "CODE")
    public String getCode() {
        return code;
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    @ManyToOne
    @JoinColumn(name = "PARTNER_ID", referencedColumnName = ColumnName.ID, nullable = false)
    public Partner getPartner() {
        return partner;
    }

    public void setAgreementType(AgreementTypeEnum agreementType) {
        this.agreementType = agreementType;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("Agreement [code=");
        builder.append(code);
        builder.append(", name=");
        builder.append(name);
        builder.append(", aliasId=");
        builder.append(aliasId);
        builder.append(", partner=");
        builder.append(partner);
        builder.append(", description=");
        builder.append(description);
        builder.append(", agreementType=");
        builder.append(agreementType);
        builder.append(", validFrom=");
        builder.append(validFrom);
        builder.append(", validTo=");
        builder.append(validTo);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
