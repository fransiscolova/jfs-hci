package id.co.homecredit.jfs.cofin.batch.base.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;

/**
 * Entity class for table {@code COFIN_BATCH_JOB}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "COFIN_BATCH_JOB", indexes = {
        @Index(columnList = "JOB_NAME", name = "IDX_BAT_JOB_JOB_NAME") })
public class BatchJob extends IdEntity {
    private Partner partner;
    private String jobName;
    private String cronExpression;
    private YesNoEnum flagGenerate;

    @Column(name = "CRON_EXPRESSION", nullable = false)
    public String getCronExpression() {
        return cronExpression;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "FLAG_GENERATE", nullable = false, length = 1)
    public YesNoEnum getFlagGenerate() {
        return flagGenerate;
    }

    @Column(name = "JOB_NAME", nullable = false, unique = true)
    public String getJobName() {
        return jobName;
    }

    @ManyToOne
    @JoinColumn(name = "PARTNER_ID", referencedColumnName = ColumnName.ID, nullable = false)
    public Partner getPartner() {
        return partner;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public void setFlagGenerate(YesNoEnum flagGenerate) {
        this.flagGenerate = flagGenerate;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("BatchJob [partner=");
        builder.append(partner);
        builder.append(", jobName=");
        builder.append(jobName);
        builder.append(", cronExpression=");
        builder.append(cronExpression);
        builder.append(", flagGenerate=");
        builder.append(flagGenerate);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
