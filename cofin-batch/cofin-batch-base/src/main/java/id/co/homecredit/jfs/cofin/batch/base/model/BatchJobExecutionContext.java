package id.co.homecredit.jfs.cofin.batch.base.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class for table {@code BATCH_JOB_EXECUTION_CONTEXT}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "BATCH_JOB_EXECUTION_CONTEXT")
public class BatchJobExecutionContext implements Serializable {
    private BatchJobExecution batchJobExecution;
    private String shortContext;
    private String serializedContext;

    @Id
    @ManyToOne
    @JoinColumn(name = "JOB_EXECUTION_ID", referencedColumnName = "JOB_EXECUTION_ID", unique = true, nullable = false)
    public BatchJobExecution getBatchJobExecution() {
        return batchJobExecution;
    }

    @Column(name = "SERIALIZED_CONTEXT")
    public String getSerializedContext() {
        return serializedContext;
    }

    @Column(name = "SHORT_CONTEXT", nullable = false)
    public String getShortContext() {
        return shortContext;
    }

    public void setBatchJobExecution(BatchJobExecution batchJobExecution) {
        this.batchJobExecution = batchJobExecution;
    }

    public void setSerializedContext(String serializedContext) {
        this.serializedContext = serializedContext;
    }

    public void setShortContext(String shortContext) {
        this.shortContext = shortContext;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("BatchJobExecutionContext [batchJobExecution=");
        builder.append(batchJobExecution);
        builder.append(", shortContext=");
        builder.append(shortContext);
        builder.append(", serializedContext=");
        builder.append(serializedContext);
        builder.append("]");
        return builder.toString();
    }

}
