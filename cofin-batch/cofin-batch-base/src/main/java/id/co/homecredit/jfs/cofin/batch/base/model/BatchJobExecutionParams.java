package id.co.homecredit.jfs.cofin.batch.base.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class for table {@code BATCH_JOB_EXECUTION_PARAMS}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "BATCH_JOB_EXECUTION_PARAMS")
public class BatchJobExecutionParams implements Serializable {
    private BatchJobExecution batchJobExecution;
    private String typeCd;
    private String keyName;
    private String stringVal;
    private Date dateVal;
    private Long longVal;
    private BigDecimal doubleVal;
    private String identifying;

    @Id
    @ManyToOne
    @JoinColumn(name = "JOB_EXECUTION_ID", referencedColumnName = "JOB_EXECUTION_ID", unique = true, nullable = false)
    public BatchJobExecution getBatchJobExecution() {
        return batchJobExecution;
    }

    @Column(name = "DATE_VAL")
    public Date getDateVal() {
        return dateVal;
    }

    @Column(name = "DOUBLE_VAL")
    public BigDecimal getDoubleVal() {
        return doubleVal;
    }

    @Column(name = "IDENTIFYING", nullable = false, length = 1)
    public String getIdentifying() {
        return identifying;
    }

    @Column(name = "KEY_NAME", nullable = false)
    public String getKeyName() {
        return keyName;
    }

    @Column(name = "LONG_VAL")
    public Long getLongVal() {
        return longVal;
    }

    @Column(name = "STRING_VAL", nullable = false)
    public String getStringVal() {
        return stringVal;
    }

    @Column(name = "TYPE_CD", nullable = false)
    public String getTypeCd() {
        return typeCd;
    }

    public void setBatchJobExecution(BatchJobExecution batchJobExecution) {
        this.batchJobExecution = batchJobExecution;
    }

    public void setDateVal(Date dateVal) {
        this.dateVal = dateVal;
    }

    public void setDoubleVal(BigDecimal doubleVal) {
        this.doubleVal = doubleVal;
    }

    public void setIdentifying(String identifying) {
        this.identifying = identifying;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public void setLongVal(Long longVal) {
        this.longVal = longVal;
    }

    public void setStringVal(String stringVal) {
        this.stringVal = stringVal;
    }

    public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("BatchJobExecutionParams [batchJobExecution=");
        builder.append(batchJobExecution);
        builder.append(", typeCd=");
        builder.append(typeCd);
        builder.append(", keyName=");
        builder.append(keyName);
        builder.append(", stringVal=");
        builder.append(stringVal);
        builder.append(", dateVal=");
        builder.append(dateVal);
        builder.append(", longVal=");
        builder.append(longVal);
        builder.append(", doubleVal=");
        builder.append(doubleVal);
        builder.append(", identifying=");
        builder.append(identifying);
        builder.append("]");
        return builder.toString();
    }

}
