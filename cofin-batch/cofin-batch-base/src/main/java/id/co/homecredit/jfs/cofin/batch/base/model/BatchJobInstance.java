package id.co.homecredit.jfs.cofin.batch.base.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for table {@code BATCH_JOB_INSTANCE}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "BATCH_JOB_INSTANCE")
public class BatchJobInstance implements Serializable {
    private Long jobInstanceId;
    private Long version;
    private String jobName;
    private String jobKey;

    @Id
    @Column(name = "JOB_INSTANCE_ID", unique = true, nullable = false)
    public Long getJobInstanceId() {
        return jobInstanceId;
    }

    @Column(name = "JOB_KEY", nullable = false)
    public String getJobKey() {
        return jobKey;
    }

    @Column(name = "JOB_NAME", nullable = false)
    public String getJobName() {
        return jobName;
    }

    @Column(name = "VERSION")
    public Long getVersion() {
        return version;
    }

    public void setJobInstanceId(Long jobInstanceId) {
        this.jobInstanceId = jobInstanceId;
    }

    public void setJobKey(String jobKey) {
        this.jobKey = jobKey;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("BatchJobInstance [jobInstanceId=");
        builder.append(jobInstanceId);
        builder.append(", version=");
        builder.append(version);
        builder.append(", jobName=");
        builder.append(jobName);
        builder.append(", jobKey=");
        builder.append(jobKey);
        builder.append("]");
        return builder.toString();
    }

}
