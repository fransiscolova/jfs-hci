package id.co.homecredit.jfs.cofin.batch.base.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class for table {@code BATCH_STEP_EXECUTION}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "BATCH_STEP_EXECUTION")
public class BatchStepExecution implements Serializable {
    private Long stepExecutionId;
    private Long version;
    private String stepName;
    private BatchJobExecution batchJobExecution;
    private Date startTime;
    private Date endTime;
    private String status;
    private Long commitCount;
    private Long readCount;
    private Long filterCount;
    private Long writeCount;
    private Long readSkipCount;
    private Long writeSkipCount;
    private Long processSkipCount;
    private Long rollbackCount;
    private String exitCode;
    private String exitMessage;
    private Date lastUpdated;

    @ManyToOne
    @JoinColumn(name = "JOB_EXECUTION_ID", referencedColumnName = "JOB_EXECUTION_ID", unique = true, nullable = false)
    public BatchJobExecution getBatchJobExecution() {
        return batchJobExecution;
    }

    @Column(name = "COMMIT_COUNT")
    public Long getCommitCount() {
        return commitCount;
    }

    @Column(name = "END_TIME")
    public Date getEndTime() {
        return endTime;
    }

    @Column(name = "EXIT_CODE")
    public String getExitCode() {
        return exitCode;
    }

    @Column(name = "EXIT_MESSAGE")
    public String getExitMessage() {
        return exitMessage;
    }

    @Column(name = "FILTER_COUNT")
    public Long getFilterCount() {
        return filterCount;
    }

    @Column(name = "LAST_UPDATED")
    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Column(name = "PROCESS_SKIP_COUNT")
    public Long getProcessSkipCount() {
        return processSkipCount;
    }

    @Column(name = "READ_COUNT")
    public Long getReadCount() {
        return readCount;
    }

    @Column(name = "READ_SKIP_COUNT")
    public Long getReadSkipCount() {
        return readSkipCount;
    }

    @Column(name = "ROLLBACK_COUNT")
    public Long getRollbackCount() {
        return rollbackCount;
    }

    @Column(name = "START_TIME", nullable = false)
    public Date getStartTime() {
        return startTime;
    }

    @Column(name = "STATUS", length = 10)
    public String getStatus() {
        return status;
    }

    @Id
    @Column(name = "STEP_EXECUTION_ID", unique = true, nullable = false)
    public Long getStepExecutionId() {
        return stepExecutionId;
    }

    @Column(name = "STEP_NAME", nullable = false)
    public String getStepName() {
        return stepName;
    }

    @Column(name = "VERSION", nullable = false)
    public Long getVersion() {
        return version;
    }

    @Column(name = "WRITE_COUNT")
    public Long getWriteCount() {
        return writeCount;
    }

    @Column(name = "WRITE_SKIP_COUNT")
    public Long getWriteSkipCount() {
        return writeSkipCount;
    }

    public void setBatchJobExecution(BatchJobExecution batchJobExecution) {
        this.batchJobExecution = batchJobExecution;
    }

    public void setCommitCount(Long commitCount) {
        this.commitCount = commitCount;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setExitCode(String exitCode) {
        this.exitCode = exitCode;
    }

    public void setExitMessage(String exitMessage) {
        this.exitMessage = exitMessage;
    }

    public void setFilterCount(Long filterCount) {
        this.filterCount = filterCount;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setProcessSkipCount(Long processSkipCount) {
        this.processSkipCount = processSkipCount;
    }

    public void setReadCount(Long readCount) {
        this.readCount = readCount;
    }

    public void setReadSkipCount(Long readSkipCount) {
        this.readSkipCount = readSkipCount;
    }

    public void setRollbackCount(Long rollbackCount) {
        this.rollbackCount = rollbackCount;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStepExecutionId(Long stepExecutionId) {
        this.stepExecutionId = stepExecutionId;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public void setWriteCount(Long writeCount) {
        this.writeCount = writeCount;
    }

    public void setWriteSkipCount(Long writeSkipCount) {
        this.writeSkipCount = writeSkipCount;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("BatchStepExecution [stepExecutionId=");
        builder.append(stepExecutionId);
        builder.append(", version=");
        builder.append(version);
        builder.append(", stepName=");
        builder.append(stepName);
        builder.append(", batchJobExecution=");
        builder.append(batchJobExecution);
        builder.append(", startTime=");
        builder.append(startTime);
        builder.append(", endTime=");
        builder.append(endTime);
        builder.append(", status=");
        builder.append(status);
        builder.append(", commitCount=");
        builder.append(commitCount);
        builder.append(", readCount=");
        builder.append(readCount);
        builder.append(", filterCount=");
        builder.append(filterCount);
        builder.append(", writeCount=");
        builder.append(writeCount);
        builder.append(", readSkipCount=");
        builder.append(readSkipCount);
        builder.append(", writeSkipCount=");
        builder.append(writeSkipCount);
        builder.append(", processSkipCount=");
        builder.append(processSkipCount);
        builder.append(", rollbackCount=");
        builder.append(rollbackCount);
        builder.append(", exitCode=");
        builder.append(exitCode);
        builder.append(", exitMessage=");
        builder.append(exitMessage);
        builder.append(", lastUpdated=");
        builder.append(lastUpdated);
        builder.append("]");
        return builder.toString();
    }

}
