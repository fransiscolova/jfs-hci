package id.co.homecredit.jfs.cofin.batch.base.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdCreate;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;

/**
 * Entity class for table {@code COFIN_EXPORT_LOG}.
 *
 * @author muhammad.muflihun
 *
 */
@AuditTransient
@Entity
@Table(name = "COFIN_EXPORT_LOG")
public class ExportLog extends IdCreate<String> {
    private Long stepExecutionId;
    private String partnerId;
    private String filename;
    private String backupFilename;

    @Column(name = "BACKUP_FILENAME")
    public String getBackupFilename() {
        return backupFilename;
    }

    @Column(name = "FILENAME", nullable = false)
    public String getFilename() {
        return filename;
    }

    @Column(name = "PARTNER_ID", nullable = false)
    public String getPartnerId() {
        return partnerId;
    }

    @Column(name = "STEP_EXECUTION_ID", nullable = false)
    public Long getStepExecutionId() {
        return stepExecutionId;
    }

    public void setBackupFilename(String backupFilename) {
        this.backupFilename = backupFilename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public void setStepExecutionId(Long stepExecutionId) {
        this.stepExecutionId = stepExecutionId;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("ExportLog [stepExecutionId=");
        builder.append(stepExecutionId);
        builder.append(", partnerId=");
        builder.append(partnerId);
        builder.append(", filename=");
        builder.append(filename);
        builder.append(", backupFilename=");
        builder.append(backupFilename);
        builder.append(", id=");
        builder.append(id);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
