package id.co.homecredit.jfs.cofin.batch.base.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.batch.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.batch.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.common.model.TextNumber;


/**
 * Entity class for {@code JFS_CONTRACT}.
 *
 * @author fransisco.situmorang
 *
 */

@Entity
@Table(name=CoreTableName.JFS_CONTRACT)
public class JfsContract extends TextNumber{

	private String status;
	private String exportDate;
	private String skpContract;
	private String sendPrincipal;
	private String sendInstalment;
	private String sendTenor;
	private String sendRate;
	private String dateFirstDue;
	private String acceptedDate;
	private String rejectReason;
	private String bankInterestRate;
	private String bankPrincipal;
	private String bankInterest;
	private String bankProvision;
	private String bankAdminFee;
	private String bankInstallment;
	private String bankTenor;
	private String bankSplitRate;
	private String clientName;
	private String amtInstallment;
	private String amtMonthlyFee;
	private String idAgreement;
	private String reason;
	private Date bankDecisionDate;
	private Date bankClawbackDate;
	private String bankClawbackAmount;
	private String bankProductCode;
	private String bankReferenceNo;
	private String cuid;
	
	
	@Column(name=CoreColumnName.STATUS)
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	@Column(name=CoreColumnName.EXPORT_DATE)
	public String getExportDate(){
		return exportDate;
	}
	public void setExportDate(String exportDate){
		this.exportDate=exportDate;
	}
	@Column(name=CoreColumnName.SKP_CONTRACT)
	public String getSkpContract(){
		return skpContract;
	}
	public void setSkpContract(String skpContract){
		this.skpContract=skpContract;
	}
	@Column(name=CoreColumnName.SEND_PRINCIPAL)
	public String getSendPrincipal(){
		return sendPrincipal;
	}
	public void setSendPrincipal(String sendPrincipal){
		this.sendPrincipal=sendPrincipal;
	}
	@Column(name=CoreColumnName.SEND_INSTALMENT)
	public String getSendInstalment(){
		return sendInstalment;
	}
	public void setSendInstalment(String sendInstalment){
		this.sendInstalment=sendInstalment;
	}
	@Column(name=CoreColumnName.SEND_TENOR)
	public String getSendTenor(){
		return sendTenor;
	}
	public void setSendTenor(String sendTenor){
		this.sendTenor=sendTenor;
	}
	@Column(name=CoreColumnName.SEND_RATE)
	public String getSendRate(){
		return sendRate;
	}
	public void setSendRate(String sendRate){
		this.sendRate=sendRate;
	}
	@Column(name=CoreColumnName.DATE_FIRST_DUE)
	public String getDateFirstDue(){
		return dateFirstDue;
	}
	public void setDateFirstDue(String dateFirstDue){
		this.dateFirstDue=dateFirstDue;
	}
	@Column(name=CoreColumnName.ACCEPTED_DATE)
	public String getAcceptedDate(){
		return acceptedDate;
	}
	public void setAcceptedDate(String acceptedDate){
		this.acceptedDate=acceptedDate;
	}
	@Column(name=CoreColumnName.REJECT_REASON)
	public String getRejectReason(){
		return rejectReason;
	}
	public void setRejectReason(String rejectReason){
		this.rejectReason=rejectReason;
	}
	@Column(name=CoreColumnName.BANK_INTEREST_RATE)
	public String getBankInterestRate(){
		return bankInterestRate;
	}
	public void setBankInterestRate(String bankInterestRate){
		this.bankInterestRate=bankInterestRate;
	}
	@Column(name=CoreColumnName.BANK_PRINCIPAL)
	public String getBankPrincipal(){
		return bankPrincipal;
	}
	public void setBankPrincipal(String bankPrincipal){
		this.bankPrincipal=bankPrincipal;
	}
	@Column(name=CoreColumnName.BANK_INTEREST)
	public String getBankInterest(){
		return bankInterest;
	}
	public void setBankInterest(String bankInterest){
		this.bankInterest=bankInterest;
	}
	@Column(name=CoreColumnName.BANK_PROVISION)
	public String getBankProvision(){
		return bankProvision;
	}
	public void setBankProvision(String bankProvision){
		this.bankProvision=bankProvision;
	}
	@Column(name=CoreColumnName.BANK_ADMIN_FEE)
	public String getBankAdminFee(){
		return bankAdminFee;
	}
	public void setBankAdminFee(String bankAdminFee){
		this.bankAdminFee=bankAdminFee;
	}
	@Column(name=CoreColumnName.BANK_INSTALLMENT)
	public String getBankInstallment(){
		return bankInstallment;
	}
	public void setBankInstallment(String bankInstallment){
		this.bankInstallment=bankInstallment;
	}
	@Column(name=CoreColumnName.BANK_TENOR)
	public String getBankTenor(){
		return bankTenor;
	}
	public void setBankTenor(String bankTenor){
		this.bankTenor=bankTenor;
	}
	@Column(name=CoreColumnName.BANK_SPLIT_RATE)
	public String getBankSplitRate(){
		return bankSplitRate;
	}
	public void setBankSplitRate(String bankSplitRate){
		this.bankSplitRate=bankSplitRate;
	}
	@Column(name=CoreColumnName.CLIENT_NAME)
	public String getClientName(){
		return clientName;
	}
	public void setClientName(String clientName){
		this.clientName=clientName;
	}
	@Column(name=CoreColumnName.AMT_INSTALLMENT)
	public String getAmtInstallment(){
		return amtInstallment;
	}
	public void setAmtInstallment(String amtInstallment){
		this.amtInstallment=amtInstallment;
	}
	@Column(name=CoreColumnName.AMT_MONTHLY_FEE)
	public String getAmtMonthlyFee(){
		return amtMonthlyFee;
	}
	public void setAmtMonthlyFee(String amtMonthlyFee){
		this.amtMonthlyFee=amtMonthlyFee;
	}
	@Column(name=CoreColumnName.ID_AGREEMENT)
	public String getIdAgreement(){
		return idAgreement;
	}
	public void setIdAgreement(String idAgreement){
		this.idAgreement=idAgreement;
	}
	@Column(name=CoreColumnName.REASON)
	public String getReason(){
		return reason;
	}
	public void setReason(String reason){
		this.reason=reason;
	}
	@Column(name=CoreColumnName.BANK_DECISION_DATE)
	public Date getBankDecisionDate(){
		return bankDecisionDate;
	}
	public void setBankDecisionDate(Date bankDecisionDate){
		this.bankDecisionDate=bankDecisionDate;
	}
	@Column(name=CoreColumnName.BANK_CLAWBACK_DATE)
	public Date getBankClawbackDate(){
		return bankClawbackDate;
	}
	public void setBankClawbackDate(Date bankClawbackDate){
		this.bankClawbackDate=bankClawbackDate;
	}
	@Column(name=CoreColumnName.BANK_CLAWBACK_AMOUNT)
	public String getBankClawbackAmount(){
		return bankClawbackAmount;
	}
	public void setBankClawbackAmount(String bankClawbackAmount){
		this.bankClawbackAmount=bankClawbackAmount;
	}
	@Column(name=CoreColumnName.BANK_PRODUCT_CODE)
	public String getBankProductCode(){
		return bankProductCode;
	}
	public void setBankProductCode(String bankProductCode){
		this.bankProductCode=bankProductCode;
	}
	@Column(name=CoreColumnName.BANK_REFERENCE_NUMBER)
	public String getBankReferenceNo(){
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo){
		this.bankReferenceNo=bankReferenceNo;
	}
	@Column(name=CoreColumnName.CUID)
	public String getCuid(){
		return cuid;
	}
	public void setCuid(String cuid){
		this.cuid=cuid;
	}
	@Override
	public String toString(){
		StringBuilder xbuild = new StringBuilder(1000);
		xbuild.append("JfsContract [textContractNumber=");
		xbuild.append(textContractNumber);
		xbuild.append(",status=");
		xbuild.append(status);
		xbuild.append(",exportDate=");
		xbuild.append(exportDate);
		xbuild.append(",skpContract=");
		xbuild.append(skpContract);
		xbuild.append(",sendPrincipal=");
		xbuild.append(sendPrincipal);
		xbuild.append(",sendInstalment=");
		xbuild.append(sendInstalment);
		xbuild.append(",sendTenor=");
		xbuild.append(sendTenor);
		xbuild.append(",sendRate=");
		xbuild.append(sendRate);
		xbuild.append(",dateFirstDue=");
		xbuild.append(dateFirstDue);
		xbuild.append(",acceptedDate=");
		xbuild.append(acceptedDate);
		xbuild.append(",rejectReason=");
		xbuild.append(rejectReason);
		xbuild.append(",bankInterestRate=");
		xbuild.append(bankInterestRate);
		xbuild.append(",bankPrincipal=");
		xbuild.append(bankPrincipal);
		xbuild.append(",bankInterest=");
		xbuild.append(bankInterest);
		xbuild.append(",bankProvision=");
		xbuild.append(bankProvision);
		xbuild.append(",bankAdminFee=");
		xbuild.append(bankAdminFee);
		xbuild.append(",bankInstallment=");
		xbuild.append(bankInstallment);
		xbuild.append(",bankTenor=");
		xbuild.append(bankTenor);
		xbuild.append(",bankSplitRate=");
		xbuild.append(bankSplitRate);
		xbuild.append(",clientName=");
		xbuild.append(clientName);
		xbuild.append(",amtInstallment=");
		xbuild.append(amtInstallment);
		xbuild.append(",amtMonthlyFee=");
		xbuild.append(amtMonthlyFee);
		xbuild.append(",idAgreement=");
		xbuild.append(idAgreement);
		xbuild.append(",reason=");
		xbuild.append(reason);
		xbuild.append(",bankDecisionDate=");
		xbuild.append(bankDecisionDate);
		xbuild.append(",bankClawbackDate=");
		xbuild.append(bankClawbackDate);
		xbuild.append(",bankClawbackAmount=");
		xbuild.append(bankClawbackAmount);
		xbuild.append(",bankProductCode=");
		xbuild.append(bankProductCode);
		xbuild.append(",bankReferenceNo=");
		xbuild.append(bankReferenceNo);
		xbuild.append(",cuid=");
		xbuild.append(cuid);
		xbuild.append("]");
		return xbuild.toString();
	}
}