package id.co.homecredit.jfs.cofin.batch.base.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.BaseEntityTextNumberOnly;
import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.TextNumber;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;

/**
 * Entity class for {@code VW_BOOK_OFI}.
 *
 * @author fransisco.situmroang
 *
 */
@Entity
@Table(name = "JFS_OFI")
public class JfsOfi extends TextNumber {


    private Date dueDate;
    private Integer partIndex;
    private BigDecimal intFi;
    private BigDecimal ofiAcr1;
    private BigDecimal ofiAcr2;
    private Integer ver;
    private String isActive;
    
	
    @Column(name="DUE_DATE")
	public Date getDueDate() {
		return dueDate;
	}

    @Column(name="PART_INDEX")
	public Integer getPartIndex() {
		return partIndex;
	}

    @Column(name="INT_FI")
	public BigDecimal getIntFi() {
		return intFi;
	}

    @Column(name="OFI_ACR_1")
	public BigDecimal getOfiAcr1() {
		return ofiAcr1;
	}

    @Column(name="OFI_ACR_2")
	public BigDecimal getOfiAcr2() {
		return ofiAcr2;
	}

    @Column(name="VER")
	public Integer getVer() {
		return ver;
	}

    @Column(name="IS_ACTIVE")
	public String getIsActive() {
		return isActive;
	}


	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	public void setPartIndex(Integer partIndex) {
		this.partIndex = partIndex;
	}


	public void setIntFi(BigDecimal intFi) {
		this.intFi = intFi;
	}


	public void setOfiAcr1(BigDecimal ofiAcr1) {
		this.ofiAcr1 = ofiAcr1;
	}


	public void setOfiAcr2(BigDecimal ofiAcr2) {
		this.ofiAcr2 = ofiAcr2;
	}


	public void setVer(Integer ver) {
		this.ver = ver;
	}


	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	/**
     * Print each field value for this entity.
    */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("OFI [dueDate=");
        builder.append(dueDate);
        builder.append(", partIndex=");
        builder.append(partIndex);
        builder.append(", partIndex=");
        builder.append(partIndex);
        builder.append(", ofiAcr1=");
        builder.append(ofiAcr1);
        builder.append(", ofiAcr2=");
        builder.append(ofiAcr2);
        builder.append("]");
        return builder.toString();
    }
     

}
