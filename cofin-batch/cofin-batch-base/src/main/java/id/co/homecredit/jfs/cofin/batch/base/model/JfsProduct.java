package id.co.homecredit.jfs.cofin.batch.base.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.batch.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.batch.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;


/**
 * Entity class for {@code JFS_PRODUCT}.
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=CoreTableName.JFS_PRODUCT)
public class JfsProduct extends BaseEntityIdOnly<String>{
	private String bankProductCode;
	private Double interestRate;
	private Double principalSplitRate;
	private Date validFrom;
	private Date validTo;
	private Integer idAgreement;
	private Double adminFeeRate;
	private Double penaltySplitRate;
	private Double penaltySplitRateHci;
	private Double principalSplitRateHci;
	@Column(name=CoreColumnName.BANK_PRODUCT_CODE)
	public String getBankProductCode(){
		return bankProductCode;
	}
	public void setBankProductCode(String bankProductCode){
		this.bankProductCode=bankProductCode;
	}
	
	@Column(name="PENALTY_SPLIT_RATE_HCI")
	public Double getPenaltySplitRateHci() {
		return penaltySplitRateHci;
	}
	public void setPenaltySplitRateHci(Double penaltySplitRateHci) {
		this.penaltySplitRateHci = penaltySplitRateHci;
	}
	@Column(name=CoreColumnName.INTEREST_RATE)
	public Double getInterestRate(){
		return interestRate;
	}
	public void setInterestRate(Double interestRate){
		this.interestRate=interestRate;
	}
	@Column(name=CoreColumnName.PRINCIPAL_SPLIT_RATE)
	public Double getPrincipalSplitRate(){
		return principalSplitRate;
	}
	public void setPrincipalSplitRate(Double principalSplitRate){
		this.principalSplitRate=principalSplitRate;
	}
	@Column(name=CoreColumnName.VALID_FROM)
	public Date getValidFrom(){
		return validFrom;
	}
	public void setValidFrom(Date validFrom){
		this.validFrom=validFrom;
	}
	@Column(name=CoreColumnName.VALID_TO)
	public Date getValidTo(){
		return validTo;
	}
	public void setValidTo(Date validTo){
		this.validTo=validTo;
	}
	@Column(name=CoreColumnName.ID_AGREEMENT)
	public Integer getIdAgreement(){
		return idAgreement;
	}
	public void setIdAgreement(Integer idAgreement){
		this.idAgreement=idAgreement;
	}
	@Column(name=CoreColumnName.ADMIN_FEE_RATE)
	public Double getAdminFeeRate(){
		return adminFeeRate;
	}
	public void setAdminFeeRate(Double adminFeeRate){
		this.adminFeeRate=adminFeeRate;
	}
	@Column(name=CoreColumnName.PENALTY_SPLIT_RATE)
	public Double getPenaltySplitRate(){
		return penaltySplitRate;
	}
	public void setPenaltySplitRate(Double penaltySplitRate){
		this.penaltySplitRate=penaltySplitRate;
	}
	@Column(name=CoreColumnName.PRINCIPAL_SPLIT_RATE_HCI)
	public Double getPrincipalSplitRateHci(){
		return principalSplitRateHci;
	}
	public void setPrincipalSplitRateHci(Double principalSplitRateHci){
		this.principalSplitRateHci=principalSplitRateHci;
	}
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder(1000);
		builder.append("JfsProduct [id=");
		builder.append(id);
		builder.append(",bankProductCode=");
		builder.append(bankProductCode);
		builder.append(",interestRate=");
		builder.append(interestRate);
		builder.append(",principalSplitRate=");
		builder.append(principalSplitRate);
		builder.append(",validFrom=");
		builder.append(validFrom);
		builder.append(",validTo=");
		builder.append(validTo);
		builder.append(",idAgreement=");
		builder.append(idAgreement);
		builder.append(",adminFeeRate=");
		builder.append(adminFeeRate);
		builder.append(",penaltySplitRate=");
		builder.append(penaltySplitRate);
		builder.append(",principalSplitRateHci=");
		builder.append(principalSplitRateHci);
		builder.append("]");
		return builder.toString();
	}
}
