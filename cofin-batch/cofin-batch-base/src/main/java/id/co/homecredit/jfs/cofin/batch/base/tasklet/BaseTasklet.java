package id.co.homecredit.jfs.cofin.batch.base.tasklet;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.dao.BatchStepExecutionDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.CommonBatchDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.ExportLogDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.PartnerDao;
import id.co.homecredit.jfs.cofin.batch.base.enumeration.FileFolderEnum;
import id.co.homecredit.jfs.cofin.batch.base.model.BatchStepExecution;
import id.co.homecredit.jfs.cofin.batch.base.model.ExportLog;
import id.co.homecredit.jfs.cofin.batch.base.model.Partner;
import id.co.homecredit.jfs.cofin.batch.base.util.BatchBasePropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.EmailUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.DateFormat;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.StringChar;

/**
 * Batch step to be extended by other step.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class BaseTasklet implements Tasklet {
    private static final Logger log = LogManager.getLogger(BaseTasklet.class);
    protected static Long stepExecutionId = 0L;
    protected static Partner partner = null;

    @Autowired
    private CommonBatchDao commonBatchDao;
    @Autowired
    private BatchStepExecutionDao batchStepExecutionDao;
    @Autowired
    private ExportLogDao exportLogDao;
    @Autowired
    private PartnerDao partnerDao;

    /**
     * Append filename with given appender before the file extension. <br>
     * Example :
     *
     * filename is filename.txt and appender is 2017-11-11, then the result is
     * filename_2017-11-11.txt.
     *
     * @param filename
     * @param appender
     * @return appendedFilename
     */
    protected String appendFileName(String filename, String appender) {
        int lastPoint = filename.lastIndexOf(".");
        String ext = filename.substring(lastPoint + 1);
        if (lastPoint == -1 || !ext.matches("\\w+")) {
            log.error("error {} doesn't have any extension");
            return null;
        }
        String name = filename.substring(0, lastPoint);
        return name.concat(StringChar.UNDERSCORE).concat(appender).concat(StringChar.DOT)
                .concat(ext);
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        StepContext stepContext = chunkContext.getStepContext();
        stepExecutionId = stepContext.getStepExecution().getId();
        log.info("step execution id {}", stepExecutionId);
        String partnerCode = stepContext.getJobName().split(StringChar.UNDERSCORE)[0];
        if(partnerCode.equals("BookNG")) {
        	  return RepeatStatus.FINISHED;
        }
        
        partner = partnerDao.getPartnerByCode(partnerCode);
        log.info("batch partner {}", partner.toString());
        return RepeatStatus.FINISHED;
    }

    /**
     * Execute sql.
     *
     * @param sql
     */
    protected Object executeSql(String sql) {
        log.debug("execute sql : \n {}", sql);
        return commonBatchDao.executeQuery(sql);
    }

    /**
     * Execute procedure sql by param.
     *
     * @param query
     * @param param
     */
    protected void executeSqlProcedure(String query, Object param) {
        commonBatchDao.executeProcedureWithParam(query, param);
    }

    /**
     * Execute sql update.
     *
     * @param sql
     */
    protected Object executeUpdate(String sql) {
        return commonBatchDao.executeUpdate(sql);
    }

    /**
     * Export file with name as given in parameter, and using sql in given order to get the data for
     * the file.
     *
     * @param exportFileName
     * @param exportSql
     * @throws IOException
     */
    protected String exportToFile(String exportFileName, String... exportSql) throws IOException {
        // prepare export name
        String fileName = getExportFileFullpath(exportFileName);
        fileName = appendFileName(fileName, DateUtil.dateToString(new Date()));
        log.debug("filename after append {}", fileName);

        // prepare backup export name
        String backupName = getExportBackupFullpath(exportFileName);
        backupName = appendFileName(backupName,
                DateUtil.dateToStringWithFormat(new Date(), DateFormat.FORMAT_3));
        log.debug("backup filename after append {}", backupName);

        // execute query to get file content
        List<String> fileLines = new ArrayList<>();
        for (String sql : exportSql) {
            if (sql != null && !sql.isEmpty()) {
                fileLines.addAll(readQueryFromFileAsStringList(sql));
            }
        }

        if (fileLines.size() == 0) {
            log.info("empty file line");
            return null;
        }

        // export to file
        backupName = FileUtil.createFileWithCheck(fileName, fileLines, backupName) ? backupName
                : null;

        // save exported file to log
        saveExportLog(fileName, backupName);
        return fileName;
    }

    /**
     * Get export backup file full path by export file name.
     *
     * @param exportBackupName
     * @return fullpath
     */
    protected String getExportBackupFullpath(String exportBackupName) {
        String filePath = BatchBasePropertyUtil.getFileFolder(FileFolderEnum.EXPORT_BACKUP_FILE);
        return filePath.concat(exportBackupName);
    }

    /**
     * Get export backup file full path by export file name.
     *
     * @param exportEncryptBackupName
     * @return fullpath
     */
    protected String getExportEncryptBackupFullpath(String exportEncryptBackupName) {
        String filePath = BatchBasePropertyUtil.getFileFolder(FileFolderEnum.EXPORT_ENCRYPT_BACKUP);
        return filePath.concat(exportEncryptBackupName);
    }

    /**
     * Get export file full path by export file name.
     *
     * @param exportName
     * @return fullpath
     */
    protected String getExportFileFullpath(String exportName) {
        String filePath = BatchBasePropertyUtil.getFileFolder(FileFolderEnum.EXPORT_FILE);
        return filePath.concat(exportName);
    }

    /**
     * Get export file full path by export file name.
     *
     * @param sqlName
     * @return fullpath
     */
    protected String getSqlFileFullpath(String sqlName) {
        String filePath = BatchBasePropertyUtil.getFileFolder(FileFolderEnum.SQL);
        return filePath.concat(sqlName);
    }

    /**
     * Populate email configuration.
     *
     * @param message
     * @param hostname
     * @param port
     * @param username
     * @param password
     * @param fromEmail
     * @param fromName
     * @param bounceMail
     * @param text
     * @return map
     */
    protected Map<String, String> populateEmailConfig(String message, String hostname, String port,
            String username, String password, String fromEmail, String fromName, String bounceMail,
            String text) {
        Map<String, String> map = new HashMap<>();
        map.put("email.message", message);
        map.put("smtp.hostname", hostname);
        map.put("smpt.port", port);
        map.put("smtp.username", username);
        map.put("smtp.password", password);
        map.put("smtp.from.email", fromEmail);
        map.put("smtp.from.name", fromName);
        map.put("smtp.bounce.mail", bounceMail);
        map.put("smtp.text", text);
        return map;
    }

    /**
     * Read the result from executed query in rows as string list.
     *
     * @param sqlFileProperty
     * @return stringList
     * @throws FileNotFoundException
     */
    @SuppressWarnings("unchecked")
    protected List<String> readQueryFromFileAsStringList(String sqlFile)
            throws FileNotFoundException {
        String query = FileUtil.loadFileContent(sqlFile);
        return (List<String>) commonBatchDao.executeQuery(query);
    }

    /**
     * Save export log by filename and backup filename.
     *
     * @param filename
     * @param backupFilename
     * @return exportLog
     */
    protected ExportLog saveExportLog(String filename, String backupFilename) {
        BatchStepExecution batchStepExecution = batchStepExecutionDao.get(stepExecutionId);
        ExportLog exportLog = new ExportLog();
        exportLog.setCreatedBy(batchStepExecution.getStepName());
        exportLog.setFilename(filename);
        exportLog.setBackupFilename(backupFilename);
        exportLog.setPartnerId(partner.getId());
        exportLog.setStepExecutionId(stepExecutionId);
        exportLog = exportLogDao.saveWithNewPropagation(exportLog);
        log.debug(exportLog.toString());
        return exportLog;
    }

    /**
     * Send html email with custom message.
     *
     * @param subject
     * @param to
     * @param message
     * @param fromEmail
     * @param fromName
     * @param pathFiles
     * @throws EmailException
     */
    private void sendEmail(String subject, String to, String message, String fromEmail,
            String fromName, String... pathFiles) throws EmailException {
        log.debug("send email with message {} with attachment {}", message, pathFiles);
        String hostname = BatchBasePropertyUtil.get("smtp.hostname");
        Integer port = Integer.parseInt(BatchBasePropertyUtil.get("smtp.port"));
        String username = BatchBasePropertyUtil.get("smtp.username");
        String password = BatchBasePropertyUtil.get("smtp.password");
        String bounceEmail = BatchBasePropertyUtil.get("smtp.bounce.mail");
        String textMessage = BatchBasePropertyUtil.get("smtp.text");

        EmailUtil emailUtil = new EmailUtil(hostname, port, username, password, fromEmail, fromName,
                bounceEmail, textMessage);
        emailUtil.sendHtmlEmail(subject, message, to, null, null, pathFiles);
    }

    /**
     * Send email with sql attachment before throw custom exception with message.
     *
     * @param subject
     * @param to
     * @param message
     * @param fromEmail
     * @param fromName
     * @param sql
     * @throws EmailException
     */
    protected void sendEmailThenThrowException(String subject, String to, String message,
            String fromEmail, String fromName, String... sql) throws EmailException {
        sendEmail(subject, to, message, fromEmail, fromName, sql);
        log.info("exit with message {}", message);
        throw new IllegalArgumentException(message);
    }

}
