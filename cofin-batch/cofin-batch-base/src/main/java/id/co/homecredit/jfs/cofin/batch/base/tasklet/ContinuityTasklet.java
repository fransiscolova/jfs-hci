package id.co.homecredit.jfs.cofin.batch.base.tasklet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.dao.BatchStepDao;
import id.co.homecredit.jfs.cofin.batch.base.dao.BatchStepExecutionDao;

/**
 * Controller step to be called by other step to know the next step.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class ContinuityTasklet implements Tasklet {
    private static final Logger log = LogManager.getLogger(ContinuityTasklet.class);

    @Autowired
    private BatchStepDao batchStepDao;
    @Autowired
    private BatchStepExecutionDao batchStepExecutionDao;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        String jobName = chunkContext.getStepContext().getJobName();
        Long jobExecutionId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
        String stepName = batchStepExecutionDao
                .getLastStepNonContinuityByJobExecutionId(jobExecutionId);
        log.debug("previous step name {}", stepName);
        String nextStep = batchStepDao.getNextStepNameByJobNameAndStepName(jobName, stepName);
        log.debug("next step name {}", nextStep);
        ExitStatus exitStatus = null;
        if (nextStep == null || nextStep.isEmpty()) {
            exitStatus = new ExitStatus("NO STEP AHEAD");
        } else {
            exitStatus = new ExitStatus(nextStep);
        }
        contribution.setExitStatus(exitStatus);
        return RepeatStatus.FINISHED;
    }

}
