package id.co.homecredit.jfs.cofin.batch.base.tasklet;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.dao.PublicHolidayDao;
import id.co.homecredit.jfs.cofin.batch.base.model.PublicHoliday;

/**
 * Prerequisite step to be executed before other steps.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class PrerequisiteTasklet implements Tasklet {
    private static final Logger log = LogManager.getLogger(PrerequisiteTasklet.class);

    @Autowired
    private PublicHolidayDao publicHolidayDao;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        log.debug("prerequisite step");
        String jobName = chunkContext.getStepContext().getJobName();
        log.debug("check for job name {}", jobName);

        PublicHoliday publicHoliday = publicHolidayDao.getPublicHoliday(new Date());
        if (publicHoliday != null) {
            String exitMessage = publicHoliday.getDescription().toUpperCase();
            contribution.setExitStatus(new ExitStatus(exitMessage));
            log.info("exit step with message {}", exitMessage);
            return RepeatStatus.FINISHED;
        }
        return RepeatStatus.FINISHED;
    }

}
