package id.co.homecredit.jfs.cofin.batch.base.tasklet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.dao.BatchStepDao;

/**
 * Flow controller step to be called to know the next step.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class SingleContinuityTasklet implements Tasklet {
    private static final Logger log = LogManager.getLogger(SingleContinuityTasklet.class);

    @Autowired
    private BatchStepDao batchStepDao;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        String jobName = chunkContext.getStepContext().getJobName();
        String stepName = (String) chunkContext.getStepContext().getJobParameters().get("stepName");
        log.debug("job name {} and step name {}", jobName, stepName);
        ExitStatus exitStatus = null;
        if (batchStepDao.isStepNameExist(jobName, stepName)) {
            exitStatus = new ExitStatus(stepName);
        } else {
            exitStatus = new ExitStatus("NO STEP NAMED " + stepName);
        }
        contribution.setExitStatus(exitStatus);
        return RepeatStatus.FINISHED;
    }

}
