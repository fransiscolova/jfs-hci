package id.co.homecredit.jfs.cofin.batch.base.util;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.batch.base.enumeration.FileFolderEnum;

/**
 * Helper class to get static text from properties file.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class BatchBasePropertyUtil implements InitializingBean {
    private final static String source = "file:/cofin/data/jfs/properties/batch-base.properties";

    private static BatchBasePropertyUtil singleInst;

    /**
     * Get string property by key.
     *
     * @param key
     * @return string
     */
    public static String get(String key) {
        if (key == null || key.isEmpty()) {
            return "";
        }
        return singleInst.properties.getProperty(key);
    }

    /**
     * Get all properties keys by property path.
     *
     * @param propPath
     * @return set
     * @throws Exception
     */
    public static Set<Object> getAllKeys(String propPath) throws Exception {
        Properties prop = getProperies(propPath);
        return prop.keySet();
    }

    /**
     * Get string of file folder path by file folder enum.
     *
     * @param fileFolder
     * @return string
     */
    public static String getFileFolder(FileFolderEnum fileFolder) {
        String result = null;
        switch (fileFolder) {
            case SQL:
                result = get("path.sql");
                break;
            case TEMPLATE_FILE:
                result = get("path.template");
                break;
            case EXPORT_FILE:
                result = get("path.export");
                break;
            case EXPORT_BACKUP_FILE:
                result = get("path.export.backup");
                break;
            case EXPORT_ENCRYPT:
                result = get("path.export.encrypt");
                break;
            case EXPORT_ENCRYPT_BACKUP:
                result = get("path.export.encrypt.backup");
                break;
            case IMPORT_FILE:
                result = get("path.import");
                break;
            case IMPORT_DECRYPT:
                result = get("path.import.backup");
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * Load properties by property path.
     *
     * @param propPath
     * @return properties
     * @throws Exception
     */
    public static Properties getProperies(String propPath) throws Exception {
        Properties prop = new Properties();
        prop.load(new FileInputStream(propPath));
        return prop;
    }

    @Autowired
    protected ApplicationContext context;

    private Properties properties;

    protected BatchBasePropertyUtil() {
        properties = new Properties();
        singleInst = this;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        properties.load(context.getResource(source).getInputStream());
    }

}
