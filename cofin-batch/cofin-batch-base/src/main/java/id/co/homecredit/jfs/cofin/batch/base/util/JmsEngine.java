package id.co.homecredit.jfs.cofin.batch.base.util;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.batch.base.enumeration.FileFolderEnum;
import id.co.homecredit.jfs.cofin.batch.base.model.JfsOfi;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;

/**
 * Class for send message use jms
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class JmsEngine  {
	private static final Logger log = LogManager.getLogger(JmsEngine.class);
	
	public final static String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";

	public static String JMS_FACTORY = "jms/MyConnectionFactory";

	public static String QUEUE = "MyQueue";

	private QueueConnectionFactory qconFactory;

	private QueueConnection qcon;

	private QueueSession qsession;

	private QueueSender qsender;

	private Queue queue;

	private TextMessage msg;

	public void init(Context ctx, String queueName)
			throws NamingException, JMSException

	{

		qconFactory = (QueueConnectionFactory) ctx.lookup(JMS_FACTORY);

		qcon = qconFactory.createQueueConnection();

		qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

		queue = (Queue) ctx.lookup(queueName);

		qsender = qsession.createSender(queue);

		msg = qsession.createTextMessage();

		qcon.start();

	}

	public void send(String message) throws JMSException {

		msg.setText(message);

		qsender.send(msg);

	}

	public void close() throws JMSException {

		qsender.close();

		qsession.close();

		qcon.close();

	}



	private static InitialContext getInitialContext(String url)

			throws NamingException

	{

		Hashtable<String, String> env = new Hashtable<String, String>();

		env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);

		env.put(Context.PROVIDER_URL, url);

		return new InitialContext(env);

	}
	
	public void setContext(String host)throws Exception {	
		log.info("Initial context");
		    InitialContext ic = getInitialContext(host);		   
		    init(ic, QUEUE);
	}
	
	
	public void sendMessage(JSONObject message,String uid) throws Exception {
		log.info("JMS send message");

		// SEND OFI BOOKNG HERE

		try {

			    //Send JFS
			    String xml = XML.toString(message);							
				send(xml);
				
				//Create file after send message
				java.io.FileWriter fw = new java.io.FileWriter("xmlbackup/" + uid + ".xml");
			    fw.write(xml);
			    fw.close();
				
			
		

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
