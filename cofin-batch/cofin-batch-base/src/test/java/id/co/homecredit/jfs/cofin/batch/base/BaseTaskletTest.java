package id.co.homecredit.jfs.cofin.batch.base;

import java.io.IOException;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Test class to starting batch process for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
public class BaseTaskletTest extends BaseBatchTest {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Test
    public void countFileRows() throws IOException {
        String pathname = "/var/data/JFS/Export/ENDUSER_ALL_2017-11-23.txt";
        System.out.println(FileUtil.countLines(pathname));
    }

    @Test
    public void testLaunchStep() throws Exception {
        JobExecution jobExecution = jobLauncherTestUtils.launchStep("singleFlow");
        Assert.assertEquals(jobExecution.getStatus(), BatchStatus.COMPLETED);
    }

}
