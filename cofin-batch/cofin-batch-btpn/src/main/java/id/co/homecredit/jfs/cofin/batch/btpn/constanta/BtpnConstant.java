package id.co.homecredit.jfs.cofin.batch.btpn.constanta;

/**
 * Btpn constants.
 *
 * @author muhammad.muflihun
 *
 */
public class BtpnConstant {
    public final static String AES_FILE = ".aes";

    // File properties
    public final static String DISBURSEMENT_REG_PROPERTIES = "disbursement.reg.properties";
    public final static String DISBURSEMENT_FF_PROPERTIES = "disbursement.ff.properties";
    public final static String PAYMENT_PROPERTIES = "payment.properties";
    public final static String SALESROOM_PROPERTIES = "salesroom.properties";
    public final static String CONTRACT_CANCELLATION_PROPERTIES = "contract.cancellation.properties";
    public final static String PAYMENT_REVERSAL_PROPERTIES = "payment.reversal.properties";
    public final static String CONTRACT_CANCELLATION15_PROPERTIES = "contract.cancellation15.properties";
    public final static String WO_RECOVERY_PROPERTIES = "wo.recovery.properties";
    public final static String PAYMENT_GIFT_PROPERTIES = "payment.gift.properties";
    public final static String RECONCILIATION_PROPERTIES = "reconciliation.properties";
    public final static String WRITE_OFF_PROPERTIES = "write.off.properties";

}
