package id.co.homecredit.jfs.cofin.batch.btpn.main;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.service.BatchProcessBtpnDailyService;
import id.co.homecredit.jfs.cofin.batch.btpn.service.BatchProcessBtpnMonthlyService;
import id.co.homecredit.jfs.cofin.batch.btpn.util.AESUtil;

/**
 * Main class to start batch process for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class BatchProcessBtpn {
	private static final Logger log = LogManager.getLogger(BatchProcessBtpn.class);

	/**
	 * Run batch executor.
	 *
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(final String[] args) throws InterruptedException {
		log.info("starting BTPN main");

	
		try {

			if (args == null || args.length < 1) {
				log.info("BatchProcessBtpn d|m jobName stepName");
				return;
			}

			@SuppressWarnings("resource")
			ApplicationContext appContext = new ClassPathXmlApplicationContext("cofin-batch-btpn-context.xml");
			switch (args[0].charAt(0)) {
			case 'd':
				// run spring batch depends on number of arguments
				BatchProcessBtpnDailyService dailyService = appContext.getBean(BatchProcessBtpnDailyService.class);
				
				if (args.length == 1) {
					dailyService.runJobOnce();
				} else if (args.length == 2) {
					
					String jobName = args[1];
					dailyService.runJobWithQuartz(jobName);
				} else if (args.length == 3) {
					//sudah benar masuk ke sini
					log.info("Spesific step ");
					String jobName = args[1];
					String stepName = args[2];
					dailyService.runStepOnce(jobName, stepName);
				}
				break;
			case 'm':
				// run spring batch depends on number of arguments
				BatchProcessBtpnMonthlyService monthlyService = appContext
						.getBean(BatchProcessBtpnMonthlyService.class);
				if (args.length == 1) {
					monthlyService.runJobOnce();
				} else if (args.length == 2) {
					String jobName = args[1];
					monthlyService.runJobWithQuartz(jobName);
				} else if (args.length == 3) {
					log.info("Spesific step ");
					String jobName = args[1];
					String stepName = args[2];
					monthlyService.runStepOnce(jobName, stepName);
				}
				break;
			case 't':
				// TODO : for testing java policy
				String pathFile = args[1];
				String destFile = args[2];
				log.info("file to be tested for encryption {}", pathFile);
				log.info("destination path {}", destFile);
				AESUtil.encrypt(pathFile, destFile);
				log.info("encrypt done");
				break;
			default:
				log.info("Invalid operation: must be (d)aily or (m)onthly.");
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("main batch btpn exception : {}", e.getMessage());
			log.error(e.getStackTrace()[0]);
		}
	}

}
