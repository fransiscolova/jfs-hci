package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Date;

import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.enumeration.FileFolderEnum;
import id.co.homecredit.jfs.cofin.batch.base.tasklet.BaseTasklet;
import id.co.homecredit.jfs.cofin.batch.base.util.BatchBasePropertyUtil;
import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.AESUtil;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.DateFormat;

/**
 * Batch step to be extended by other step in BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class BtpnTasklet extends BaseTasklet {
    private static final Logger log = LogManager.getLogger(BtpnTasklet.class);

    /**
     * Decrypt file in given path to the default decrypt path.
     *
     * @param filePaths
     * @throws Exception
     */
    protected void decryptFiles(String... filePaths) throws Exception {
        for (String filePath : filePaths) {
            log.debug("file name in aes {}", filePath);
            String[] splitted = filePath.split("/");
            String fileNonAes = splitted[splitted.length - 1].replaceAll(BtpnConstant.AES_FILE, "");
            String decryptPath = BatchBasePropertyUtil.getFileFolder(FileFolderEnum.IMPORT_DECRYPT);
            String nonAesFile = decryptPath.concat(fileNonAes);
            log.debug("file name after decrypt {}", nonAesFile);
            AESUtil.decrypt(filePath, nonAesFile);

            // save exported file to log
            saveExportLog(nonAesFile, null);
        }
    }

    /**
     * Encrypt file in given path to the default encrypt path.
     *
     * @param filePaths
     * @throws Exception
     */
    protected void encryptFiles(String... filePaths) throws Exception {
        for (String filePath : filePaths) {
            // prepare name file to .aes
            String[] splitted = filePath.split("/");
            String fileInAes = splitted[splitted.length - 1].concat(BtpnConstant.AES_FILE);
            log.debug("file name in aes {}", fileInAes);
            String encryptPath = BatchBasePropertyUtil.getFileFolder(FileFolderEnum.EXPORT_ENCRYPT);
            String aesFile = encryptPath.concat(fileInAes);

            // prepare backup name
            String backupName = getExportEncryptBackupFullpath(fileInAes);
            backupName = appendFileName(backupName,
                    DateUtil.dateToStringWithFormat(new Date(), DateFormat.FORMAT_4));
            log.debug("backup filename after append {}", backupName);

            backupName = FileUtil.moveFileExisting(aesFile, backupName) ? backupName : null;
            AESUtil.encrypt(filePath, aesFile);
            // save exported file to log
            saveExportLog(aesFile, backupName);
        }
    }

    /**
     * Send email when row numbers are inconsistent before throw exception. Send email using batch
     * BTPN configuration.
     *
     * @param message
     * @param sql
     * @throws EmailException
     */
    protected void sendEmailThenThrowException(String message, String... sql)
            throws EmailException {
        String subject = BatchBtpnPropertyUtil.get("email.subject.disbursement.inconsistent.rows");
        String to = BatchBtpnPropertyUtil.get("email.to.it.bau");
        String fromEmail = BatchBtpnPropertyUtil.get("email.from.email");
        String fromName = BatchBtpnPropertyUtil.get("email.from.name");
        sendEmailThenThrowException(subject, to, message, fromEmail, fromName, sql);
    }
}
