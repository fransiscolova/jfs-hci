package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process contract cancellation after 15 days for BTPN.
 *
 * @author fransisco.situmorang
 *
 */
@Component
public class ContractCancellation15Tasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(ContractCancellation15Tasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String ctrCancellation15PropertiesPath = BatchBtpnPropertyUtil
                .get(BtpnConstant.CONTRACT_CANCELLATION15_PROPERTIES);
        Properties ctrCancellation15Prop = BatchBtpnPropertyUtil
                .getProperties(ctrCancellation15PropertiesPath);
        generateContractCancellation15(ctrCancellation15Prop);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generateContractCancellation15(Properties properties) throws Exception {
        log.info("generate contract cancellation after 15 days");

        // Generate payment header file
        String ctrCancellation15HeaderSql = getSqlFileFullpath(
                properties.getProperty("contract.cancellation15.header.sql"));
        String ctrCancellation15Sql = getSqlFileFullpath(
                properties.getProperty("contract.cancellation15.sql"));
        String ctrCancellation15FooterSql = getSqlFileFullpath(
                properties.getProperty("contract.cancellation15.footer.sql"));
        
        //generate detail
        String ctrCancellation15SqlDetailHeader = getSqlFileFullpath(
                properties.getProperty("contract.cancellation15.detail.header.sql"));
        String ctrCancellation15SqlDetail = getSqlFileFullpath(
                properties.getProperty("contract.cancellation15.detail.sql"));
        String ctrCancellation15SqlDetailFooter = getSqlFileFullpath(
                properties.getProperty("contract.cancellation15.detail.footer.sql"));
        
        String exportCtrCancellation15Name = properties.getProperty("contract.cancellation15.name");
        String exportCtrCancellation15NameDetail=properties.getProperty("contract.cancellation15_detail.name");
        
        String ctrCancellation15File = exportToFile(exportCtrCancellation15Name,
                ctrCancellation15HeaderSql, ctrCancellation15Sql, ctrCancellation15FooterSql);
        
        String ctrCancellation15DetailFile = exportToFile(exportCtrCancellation15NameDetail,
        		ctrCancellation15SqlDetailHeader,ctrCancellation15SqlDetail,ctrCancellation15SqlDetailFooter);
       
        
        
        Integer ctrCancellation15FileRows = FileUtil.countLines(ctrCancellation15File);
        log.info("file contract cancellation 15 rows {}", ctrCancellation15FileRows);
        
        
        Integer ctrCancellation15DetailFileRows = FileUtil.countLines(ctrCancellation15DetailFile);
        log.info("file contract cancellation detail 15 rows {}", ctrCancellation15DetailFileRows);
        

        // encrypt all the files generated successfully
        encryptFiles(ctrCancellation15File,ctrCancellation15DetailFile);
     
        
    }

}
