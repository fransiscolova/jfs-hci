package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process contract cancellation for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class ContractCancellationTasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(ContractCancellationTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String ctrCancellationPropertiesPath = BatchBtpnPropertyUtil
                .get(BtpnConstant.CONTRACT_CANCELLATION_PROPERTIES);
        Properties ctrCancellationProp = BatchBtpnPropertyUtil
                .getProperties(ctrCancellationPropertiesPath);
        generateContractCancellation(ctrCancellationProp);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generateContractCancellation(Properties properties) throws Exception {
        log.info("generate contract cancellation");

        // Generate contract cancellation file
        String ctrCancellationHeaderSql = getSqlFileFullpath(
                properties.getProperty("contract.cancellation.header.sql"));
        String ctrCancellationSql = getSqlFileFullpath(
                properties.getProperty("contract.cancellation.sql"));
        String ctrCancellationFooterSql = getSqlFileFullpath(
                properties.getProperty("contract.cancellation.footer.sql"));
        String exportCtrCancellationName = properties.getProperty("contract.cancellation.name");
        String ctrCancellationFile = exportToFile(exportCtrCancellationName,
                ctrCancellationHeaderSql, ctrCancellationSql, ctrCancellationFooterSql);
        Integer ctrCancellationFileRows = FileUtil.countLines(ctrCancellationFile);
        log.info("file contract cancellation rows {}", ctrCancellationFileRows);

        // encrypt all the files generated successfully
        encryptFiles(ctrCancellationFile);
    }

}
