package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.dao.AgreementDao;
import id.co.homecredit.jfs.cofin.batch.base.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process disbursement for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class DisbursementTasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(DisbursementTasklet.class);

    @Autowired
    private AgreementDao agreementDao;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String jobName = chunkContext.getStepContext().getJobName();
        String partnerCode = jobName.split("_")[0];
        for (AgreementTypeEnum agreementType : AgreementTypeEnum.values()) {
            // check if agreement type is exist and active
            if (!agreementDao.isAgreementExistByPartnerCodeAndAgreementType(partnerCode,
                    agreementType)) {
                log.info("agreement not exist for partner code {} and agreement type {}",
                        partnerCode, agreementType);
                continue;
            }

            if (agreementType.equals(AgreementTypeEnum.REG)) {
                String regPropertiesPath = BatchBtpnPropertyUtil
                        .get(BtpnConstant.DISBURSEMENT_REG_PROPERTIES);
                Properties regProp = BatchBtpnPropertyUtil.getProperties(regPropertiesPath);
                generateDisbursement(regProp, agreementType);
            } else if (agreementType.equals(AgreementTypeEnum.FF)) {
                String ffPropertiesPath = BatchBtpnPropertyUtil
                        .get(BtpnConstant.DISBURSEMENT_FF_PROPERTIES);
                Properties ffProp = BatchBtpnPropertyUtil.getProperties(ffPropertiesPath);
                generateDisbursement(ffProp, agreementType);
            } else {
                log.info("no implementation yet for agreement type {}", agreementType);
            }
        }
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties and agreement type.
     *
     * @param properties
     * @param agreementType
     * @throws Exception
     */
    private void generateDisbursement(Properties properties, AgreementTypeEnum agreementType)
            throws Exception {
        log.info("generate disbursement for {}", agreementType);

        // Generate kontrak file
        String kontrakHeaderSql = getSqlFileFullpath(
                properties.getProperty("disbursement.kontrak.header.sql"));
        String kontrakSql = getSqlFileFullpath(properties.getProperty("disbursement.kontrak.sql"));
        String kontrakFooterSql = getSqlFileFullpath(
                properties.getProperty("disbursement.kontrak.footer.sql"));
        String exportKontrakName = properties.getProperty("disbursement.kontrak.name");
        String kontrakFile = exportToFile(exportKontrakName, kontrakHeaderSql, kontrakSql,
                kontrakFooterSql);
        Integer kontrakFileRows = FileUtil.countLines(kontrakFile);
        log.info("kontrak file rows {}", kontrakFileRows);

        // Generate barang file
        String barangHeaderSql = getSqlFileFullpath(
                properties.getProperty("disbursement.barang.header.sql"));
        String barangSql = getSqlFileFullpath(properties.getProperty("disbursement.barang.sql"));
        String barangFooterSql = getSqlFileFullpath(
                properties.getProperty("disbursement.barang.footer.sql"));
        String exportBarangName = properties.getProperty("disbursement.barang.name");
        String barangFile = exportToFile(exportBarangName, barangHeaderSql, barangSql,
                barangFooterSql);
        Integer barangFileRows = FileUtil.countLines(barangFile);
        log.info("barang file rows {}", barangFileRows);
        if (!kontrakFileRows.equals(barangFileRows)) {
            String message = "row number of barang (" + barangFileRows
                    + ") is different than kontrak (" + kontrakFileRows + ")";
            sendEmailThenThrowException(message, barangHeaderSql, barangSql, barangFooterSql);
        }

        // Generate enduser file
        String enduserHeaderSql = getSqlFileFullpath(
                properties.getProperty("disbursement.enduser.header.sql"));
        String enduserSql = getSqlFileFullpath(properties.getProperty("disbursement.enduser.sql"));
        String enduserFooterSql = getSqlFileFullpath(
                properties.getProperty("disbursement.enduser.footer.sql"));
        String exportEnduserName = properties.getProperty("disbursement.enduser.name");
        String enduserFile = exportToFile(exportEnduserName, enduserHeaderSql, enduserSql,
                enduserFooterSql);
        Integer enduserFileRows = FileUtil.countLines(enduserFile);
        log.info("file enduser rows {}", enduserFileRows);
        if (!kontrakFileRows.equals(enduserFileRows)) {
            String message = "row number of enduser (" + enduserFileRows
                    + ") is different than kontrak (" + kontrakFileRows + ")";
            sendEmailThenThrowException(message, enduserHeaderSql, enduserSql, enduserFooterSql);
        }

        // Generate pengurus file
        String pengurusHeaderSql = getSqlFileFullpath(
                properties.getProperty("disbursement.pengurus.header.sql"));
        String pengurusSql = getSqlFileFullpath(
                properties.getProperty("disbursement.pengurus.sql"));
        String pengurusFooterSql = getSqlFileFullpath(
                properties.getProperty("disbursement.pengurus.footer.sql"));
        String exportPengurusName = properties.getProperty("disbursement.pengurus.name");
        String pengurusFile = exportToFile(exportPengurusName, pengurusHeaderSql, pengurusSql,
                pengurusFooterSql);
        Integer pengurusFileRows = FileUtil.countLines(pengurusFile);
        log.info("file pengurus rows {}", pengurusFileRows);
        if (!kontrakFileRows.equals(pengurusFileRows)) {
            String message = "row number of pengurus (" + pengurusFileRows
                    + ") is different than kontrak (" + kontrakFileRows + ")";
            sendEmailThenThrowException(message, pengurusHeaderSql, pengurusSql, pengurusFooterSql);
        }

        // Generate slik file
        String slikHeaderSql = getSqlFileFullpath(
                properties.getProperty("disbursement.slik.header.sql"));
        String slikSql = getSqlFileFullpath(properties.getProperty("disbursement.slik.sql"));
        String slikFooterSql = getSqlFileFullpath(
                properties.getProperty("disbursement.slik.footer.sql"));
        String exportSlikName = properties.getProperty("disbursement.slik.name");
        String slikFile = exportToFile(exportSlikName, slikHeaderSql, slikSql, slikFooterSql);
        Integer slikFileRows = FileUtil.countLines(slikFile);
        log.info("file slik rows {}", slikFileRows);
        if (!kontrakFileRows.equals(slikFileRows)) {
            String message = "row number of slik (" + slikFileRows + ") is different than kontrak ("
                    + kontrakFileRows + ")";
            sendEmailThenThrowException(message, slikHeaderSql, slikSql, slikFooterSql);
        }

        // encrypt all the files generated successfully
        encryptFiles(kontrakFile, barangFile, enduserFile, pengurusFile, slikFile);
    }

}
