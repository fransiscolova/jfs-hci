package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process payment gift for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class PaymentGiftTasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(PaymentGiftTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String paymentGiftPropertiesPath = BatchBtpnPropertyUtil
                .get(BtpnConstant.PAYMENT_GIFT_PROPERTIES);
        Properties paymentGiftProp = BatchBtpnPropertyUtil.getProperties(paymentGiftPropertiesPath);
        generatePaymentGift(paymentGiftProp);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generatePaymentGift(Properties properties) throws Exception {
        log.info("generate payment gift");
        // Generate payment gift header file
        String paymentGiftHeaderSql = getSqlFileFullpath(
                properties.getProperty("payment.gift.header.sql"));
        String paymentGiftSql = getSqlFileFullpath(properties.getProperty("payment.gift.sql"));
        String paymentGiftFooterSql = getSqlFileFullpath(
                properties.getProperty("payment.gift.footer.sql"));
        String exporPaymentGiftName = properties.getProperty("payment.gift.name");

        String paymentGiftFile = exportToFile(exporPaymentGiftName, paymentGiftHeaderSql,
                paymentGiftSql, paymentGiftFooterSql);
        Integer paymentGiftFileRows = FileUtil.countLines(paymentGiftFile);
        log.info("file payment gift rows {}", paymentGiftFileRows);

        // Generate payment gift detail header file
        String paymentGiftDetailHeaderSql = getSqlFileFullpath(
                properties.getProperty("payment.gift.detail.header.sql"));
        String paymentGiftDetailSql = getSqlFileFullpath(
                properties.getProperty("payment.gift.detail.sql"));
        String paymentGiftDetailFooterSql = getSqlFileFullpath(
                properties.getProperty("payment.gift.detail.footer.sql"));
        String exporPaymentGiftDetailName = properties.getProperty("payment.gift.detail.name");
        String exporPaymentGiftDetailCsvName = properties
                .getProperty("payment.gift.detail.csv.name");

        String paymentGiftDetailFile = exportToFile(exporPaymentGiftDetailName,
                paymentGiftDetailHeaderSql, paymentGiftDetailSql, paymentGiftDetailFooterSql);
        String paymentGiftDetailCsvFile = exportToFile(exporPaymentGiftDetailCsvName,
                paymentGiftDetailHeaderSql, paymentGiftDetailSql, paymentGiftDetailFooterSql);

        Integer paymentGiftDetailFileRows = FileUtil.countLines(paymentGiftDetailFile);
        log.info("file payment gift detail rows {}", paymentGiftDetailFileRows);

        if (!paymentGiftFileRows.equals(paymentGiftDetailFileRows)) {
            String message = "row number of payment gift (" + paymentGiftFileRows
                    + ") is different than payment gift detail (" + paymentGiftDetailFileRows + ")";
            sendEmailThenThrowException(message, paymentGiftDetailHeaderSql, paymentGiftDetailSql,
                    paymentGiftDetailFooterSql);
        }

        // encrypt all the files generated successfully
        encryptFiles(paymentGiftFile, paymentGiftDetailFile, paymentGiftDetailCsvFile);
    }

}
