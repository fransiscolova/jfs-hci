package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process payment reversal for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class PaymentReversalTasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(PaymentReversalTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String payReversalPropertiesPath = BatchBtpnPropertyUtil
                .get(BtpnConstant.PAYMENT_REVERSAL_PROPERTIES);
        Properties payReversalProp = BatchBtpnPropertyUtil.getProperties(payReversalPropertiesPath);
        generatePaymentReversal(payReversalProp);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generatePaymentReversal(Properties properties) throws Exception {
        log.info("generate payment reversal");

        // Generate payment reversal file
        String payReversalHeaderSql = getSqlFileFullpath(
                properties.getProperty("payment.reversal.header.sql"));
        String payReversalSql = getSqlFileFullpath(properties.getProperty("payment.reversal.sql"));
        String payReversalFooterSql = getSqlFileFullpath(
                properties.getProperty("payment.reversal.footer.sql"));
        String exportPayReversalName = properties.getProperty("payment.reversal.name");
        String payReversalFile = exportToFile(exportPayReversalName, payReversalHeaderSql,
                payReversalSql, payReversalFooterSql);
        Integer payReversalFileRows = FileUtil.countLines(payReversalFile);
        log.info("file payment reversal rows {}", payReversalFileRows);

        // encrypt all the files generated successfully
        encryptFiles(payReversalFile);
    }
}
