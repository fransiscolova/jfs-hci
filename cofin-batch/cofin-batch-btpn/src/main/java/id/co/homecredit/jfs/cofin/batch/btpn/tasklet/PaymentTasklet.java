package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process payment for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class PaymentTasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(PaymentTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String paymentPropertiesPath = BatchBtpnPropertyUtil.get(BtpnConstant.PAYMENT_PROPERTIES);
        Properties paymentProp = BatchBtpnPropertyUtil.getProperties(paymentPropertiesPath);
        generatePayment(paymentProp);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generatePayment(Properties properties) throws Exception {
        log.info("generate payment");

        // Generate payment header file
        String paymentHeaderSql = getSqlFileFullpath(properties.getProperty("payment.header.sql"));
        String paymentSql = getSqlFileFullpath(properties.getProperty("payment.sql"));
        String paymentFooterSql = getSqlFileFullpath(properties.getProperty("payment.footer.sql"));
        String exportPaymentName = properties.getProperty("payment.name");
        String paymentFile = exportToFile(exportPaymentName, paymentHeaderSql, paymentSql,
                paymentFooterSql);
        Integer paymentFileRows = FileUtil.countLines(paymentFile);
        log.info("file payment rows {}", paymentFileRows);

        // Generate payment detail file
        String paymentDetailHeaderSql = getSqlFileFullpath(
                properties.getProperty("payment.detail.header.sql"));
        String paymentDetailSql = getSqlFileFullpath(properties.getProperty("payment.detail.sql"));
        String paymentDetailFooterSql = getSqlFileFullpath(
                properties.getProperty("payment.detail.footer.sql"));
        String exportPaymentDetailName = properties.getProperty("payment.detail.name");
        String paymentDetailFile = exportToFile(exportPaymentDetailName, paymentDetailHeaderSql,
                paymentDetailSql, paymentDetailFooterSql);
        Integer paymentDetailFileRows = FileUtil.countLines(paymentDetailFile);
        log.info("file payment detail rows {}", paymentDetailFileRows);
        if (!paymentFileRows.equals(paymentDetailFileRows)) {
            String message = "row number of payment detail (" + paymentDetailFileRows
                    + ") is different than payment (" + paymentFileRows + ")";
            sendEmailThenThrowException(message, paymentDetailHeaderSql, paymentDetailSql,
                    paymentDetailFooterSql);
        }

        // encrypt all the files generated successfully
        encryptFiles(paymentFile, paymentDetailFile);
    }

}
