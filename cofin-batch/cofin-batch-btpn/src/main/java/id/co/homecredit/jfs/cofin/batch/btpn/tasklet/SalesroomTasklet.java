package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.io.FileOutputStream;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process salesroom for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class SalesroomTasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(SalesroomTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String salesroomPropertiesPath = BatchBtpnPropertyUtil
                .get(BtpnConstant.SALESROOM_PROPERTIES);
        Properties salesroomProp = BatchBtpnPropertyUtil.getProperties(salesroomPropertiesPath);
        generateSalesroom(salesroomProp, salesroomPropertiesPath);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @param propertiesPath
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private void generateSalesroom(Properties properties, String propertiesPath) throws Exception {
        log.info("generate salesroom");
        String deleteSalesroomSql = getSqlFileFullpath(
                properties.getProperty("salesroom.delete.sql"));
        String deleteSalesroomQuery = FileUtil.loadFileContent(deleteSalesroomSql);
        executeUpdate(deleteSalesroomQuery);
        log.info("delete salesroom success");

        String insertSalesroomSql = getSqlFileFullpath(
                properties.getProperty("salesroom.insert.sql"));
        String markDate = properties.getProperty("salesroom.mark");
        String insertSalesroomQuery = FileUtil.loadFileContent(insertSalesroomSql);
        executeSqlProcedure(insertSalesroomQuery, markDate);
        log.info("insert salesroom success");

        String salesroomPipeSql = getSqlFileFullpath(properties.getProperty("salesroom.pipe.sql"));
        String salesroomPipeName = properties.getProperty("salesroom.pipe.name");
        String salesroomPipeFile = exportToFile(salesroomPipeName, salesroomPipeSql);
        if (salesroomPipeFile == null) {
            log.info("salesroom file is not generated");
            return;
        }
        Integer salesroomPipeRows = FileUtil.countLines(salesroomPipeFile);
        log.info("file salesroom pipe rows {}", salesroomPipeRows);

        String salesroomSemicolonSql = getSqlFileFullpath(
                properties.getProperty("salesroom.semicolon.sql"));
        String salesroomSemicolonName = properties.getProperty("salesroom.semicolon.name");
        String salesroomSemicolonFile = exportToFile(salesroomSemicolonName, salesroomSemicolonSql);
        Integer salesroomSemicolonRows = FileUtil.countLines(salesroomSemicolonFile);
        log.info("file salesroom semicolon rows {}", salesroomSemicolonRows);
        if (!salesroomPipeRows.equals(salesroomSemicolonRows)) {
            String message = "row number of pipe (" + salesroomPipeRows
                    + ") is different than semicolon (" + salesroomSemicolonRows + ")";
            sendEmailThenThrowException(message, salesroomSemicolonSql);
        }

        String salesroomMarkSql = getSqlFileFullpath(properties.getProperty("salesroom.mark.sql"));
        String newMark = ((List<String>) executeSql(FileUtil.loadFileContent(salesroomMarkSql)))
                .get(0);
        properties.setProperty("salesroom.mark", newMark);
        FileOutputStream out = new FileOutputStream(propertiesPath);
        properties.store(out, null);
        out.close();
        log.info("file salesroom mark updated {}", newMark);

        // encrypt all the files generated successfully
        encryptFiles(salesroomPipeFile);
    }

}
