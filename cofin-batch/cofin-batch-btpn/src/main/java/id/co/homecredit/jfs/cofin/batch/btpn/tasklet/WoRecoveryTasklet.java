package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process write off recovery for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class WoRecoveryTasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(WoRecoveryTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String ctrCancellation15PropertiesPath = BatchBtpnPropertyUtil
                .get(BtpnConstant.WO_RECOVERY_PROPERTIES);
        Properties ctrCancellation15Prop = BatchBtpnPropertyUtil
                .getProperties(ctrCancellation15PropertiesPath);
        generateWoRecovery(ctrCancellation15Prop);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generateWoRecovery(Properties properties) throws Exception {
        log.info("generate WO Recovery");

        // Generate payment header file
        String woRecoveryHeaderSql = getSqlFileFullpath(
                properties.getProperty("wo.recovery.header.sql"));
        String woRecoverySql = getSqlFileFullpath(properties.getProperty("wo.recovery.sql"));
        String exportWoRecoveryName = properties.getProperty("wo.recovery.name");
        String woRecoveryFile = exportToFile(exportWoRecoveryName, woRecoveryHeaderSql,
                woRecoverySql);
        Integer woRecoveryFileRows = FileUtil.countLines(woRecoveryFile);
        log.info("file wo recovery rows {}", woRecoveryFileRows);

        // encrypt all the files generated successfully
        encryptFiles(woRecoveryFile);
    }

}
