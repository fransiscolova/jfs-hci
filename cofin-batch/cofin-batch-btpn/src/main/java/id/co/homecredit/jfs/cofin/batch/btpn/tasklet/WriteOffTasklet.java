package id.co.homecredit.jfs.cofin.batch.btpn.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.btpn.constanta.BtpnConstant;
import id.co.homecredit.jfs.cofin.batch.btpn.util.BatchBtpnPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process write off for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class WriteOffTasklet extends BtpnTasklet {
    private static final Logger log = LogManager.getLogger(WriteOffTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String writeOffPropertiesPath = BatchBtpnPropertyUtil
                .get(BtpnConstant.WRITE_OFF_PROPERTIES);
        Properties writeOffProp = BatchBtpnPropertyUtil.getProperties(writeOffPropertiesPath);
        generateWriteOff(writeOffProp, writeOffPropertiesPath);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @param propertiesPath
     * @throws Exception
     */
    private void generateWriteOff(Properties properties, String propertiesPath) throws Exception {
        log.info("generate write off");

        // Generate write-off file
        String writeOffHeaderSql = getSqlFileFullpath(
                properties.getProperty("write.off.header.sql"));
        String writeOffSql = getSqlFileFullpath(properties.getProperty("write.off.sql"));
        String exportWriteOffName = properties.getProperty("write.off.name");
        String writeOffFile = exportToFile(exportWriteOffName, writeOffHeaderSql, writeOffSql);
        Integer writeOffFileRows = FileUtil.countLines(writeOffFile);
        log.info("file write off rows {}", writeOffFileRows);
    }

}
