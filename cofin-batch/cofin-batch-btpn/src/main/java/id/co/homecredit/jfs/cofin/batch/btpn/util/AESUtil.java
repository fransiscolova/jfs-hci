package id.co.homecredit.jfs.cofin.batch.btpn.util;

/**
 * Helper class to encrypt and decrypt aes file.
 *
 * @author muhammad.muflihun
 *
 */
public class AESUtil {
    private static final String KEY = "FiBa1405HcJkt@617^";

    /**
     * Decrypt aes file in inputAesFile to the destination outputFile.
     *
     * @param inputAesFile
     * @param outputFile
     * @throws Exception
     */
    public static void decrypt(String inputAesFile, String outputFile) throws Exception {
        doEncryptDecrypt("d", inputAesFile, outputFile);
    }

    /**
     * Encrypt or decrypt using downloaded class.
     *
     * @param code
     * @param inputFile
     * @param outputAesFile
     */
    private static void doEncryptDecrypt(String code, String inputFile, String outputAesFile) {
        AESCrypt.main(new String[] { code, KEY, inputFile, outputAesFile });
    }

    /**
     * Encrypt file in the inputFile to the destination outputAesFile.
     *
     * @param inputFile
     * @param outputAesFile
     * @throws Exception
     */
    public static void encrypt(String inputFile, String outputAesFile) throws Exception {
        doEncryptDecrypt("e", inputFile, outputAesFile);
    }

}
