select p.text_contract_number
||'|'||c.client_name
||'|'||to_char(p.date_payment,'dd/MM/yyyy')
||'|'||p.pmt_instalment
||'|'||p.pmt_penalty
||'|'||0
||'|'||p.pmt_fee
||'|'||p.payment_type||chr(13)
from 
(select text_contract_number,max(date_payment) date_payment,max(date_export) date_export,sum(pmt_instalment) pmt_instalment,sum(pmt_penalty) pmt_penalty,sum(pmt_fee) pmt_fee,payment_type
  ,sum(pmt_principal) pmt_principal ,sum(pmt_interest) pmt_interest
from jfs_payment_int where payment_type='T' and trunc(date_export)=trunc(sysdate) group by text_contract_number,payment_type) p
inner join jfs_contract c on p.text_contract_number=c.text_contract_number and c.bank_product_code='BTPN_PRODUCT'
where trunc(p.date_export)=trunc(sysdate) and p.payment_type='T'
  and p.pmt_principal>=-10 and p.pmt_interest>=-10