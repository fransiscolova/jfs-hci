select 'TOTAL'||';'||count(text_contract_number)||';'||SUM(pmt_instalment)
from (SELECT JP.TEXT_CONTRACT_NUMBER TEXT_CONTRACT_NUMBER, SUM(JP.PMT_INSTALMENT+JP.PMT_FEE) PMT_INSTALMENT FROM jfs_payment_int jp
        join jfs_contract jc on jp.text_contract_number=jc.text_contract_number and Jc.bank_product_code='BTPN_PRODUCT'
        where trunc(jp.date_export)=TRUNC(SYSDATE) and jp.payment_type='T'
              and jp.pmt_principal>=-10 and jp.pmt_interest>=-10
        GROUP BY JP.TEXT_CONTRACT_NUMBER)