--records
select c.contract_code
||'|' --2.
||'|' --3.
||'|'||(case when upper(trim(cs.name1))=upper(trim(cs.name2)) then cs.name1 else cs.full_name end)--trim(cs.full_name)
||'|' -- poin 5. kode dati ||'0391'
||'|'||''
--hc.engine_number -- 6.
||'|'||''
--substr(hc.model_number||' '||hc.serial_number,1,22)   --7.
||'|' --8.
||'|'||jct.btpn_code_commodity_category     --9.
||'|'||jct.btpn_code_commodity_type  -- poin 10. 
||'|N' --11.
||'|'||to_char(sysdate,'YYYY') -- poin 12.
||'|'|| case  
          when jct.btpn_code_commodity_type in ('CT_CT_PDA', 'CT_CT_MTAB')            
            then 'DGN2'
          else
            cg.btpn_commodity_group
        end
||'|'||'MPF'
--hc.producer
||chr(13)
as KENDARAAN
from jfs_contract jc
left Join owner_int.vh_hom_contract c  on jc.text_contract_number=c.contract_code
left join owner_int.vh_hom_deal d on c.deal_code=d.code 
left join owner_int.vh_hom_client_snapshot cs on d.client_snapshot_id=cs.id ----and cs.is_new=1
--left join owner_int.vh_hom_commodity hc on c.id=hc.contract_id
--left join owner_int.vh_hom_commodity_type hct on hc.commodity_type_id=hct.id
--flexi fast loan purpose
JOIN JFS_COMMODITY_PURPOSE JCP ON JCP.CODE_LOAN_PURPOSE=C.LOAN_PURPOSE
LEFT JOIN JFS_COMMODITY_TYPE JCT ON JCT.HCI_CODE_COMMODITY_TYPE = JCP.COMMODITY_TYPE_CODE
left join JFS_COMMODITY_GROUP cg on jct.hci_code_commodity_category=cg.hci_code_commodity_category
--left join jfs_commodity_type ct on hct.commodity_category_code=ct.hci_code_commodity_category and hct.code=ct.hci_code_commodity_type
--left join JFS_COMMODITY_GROUP cg on hct.commodity_category_code=cg.hci_code_commodity_category
where 
--FLEXI FAST CONTRACT ONLY
jc.id_agreement='4'
and trunc(jc.export_date)=trunc(sysdate)
and jc.status='S'
order by c.contract_code