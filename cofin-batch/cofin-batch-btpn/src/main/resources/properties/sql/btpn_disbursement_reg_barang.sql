select c.contract_code
||'|' --2.
||'|' --3.
||'|'||(case when upper(trim(cs.name1))=upper(trim(cs.name2)) then cs.name1 else cs.full_name end)--trim(cs.full_name)
||'|' -- poin 5. kode dati ||'0391'
||'|'||hc.engine_number -- 6.
||'|'||substr(hc.model_number||' '||hc.serial_number,1,22)   --7.
||'|' --8.
||'|'||ct.btpn_code_commodity_category     --9.
||'|'||ct.btpn_code_commodity_type  -- poin 10. 
||'|N' --11.
||'|'||to_char(sysdate,'YYYY') -- poin 12.
||'|'|| case  
          when ct.btpn_code_commodity_type in ('CT_CT_PDA', 'CT_CT_MTAB')            
            then 'DGN2'
          else
            cg.btpn_commodity_group
        end
||'|'||hc.producer
||chr(13)
as KENDARAAN2
from jfs_contract jc
left join owner_int.Vh_HOM_CONTRACT c  on jc.text_contract_number=c.contract_code 
left join owner_int.vh_hom_deal d on c.deal_code=d.code 
left join owner_int.vh_hom_client_snapshot cs on d.client_snapshot_id=cs.id ----and cs.is_new=1
left join owner_int.vh_hom_commodity hc on c.id=hc.contract_id
left join owner_int.vh_hom_commodity_type hct on hc.commodity_type_id=hct.id
left join jfs_commodity_type ct on hct.commodity_category_code=ct.hci_code_commodity_category and hct.code=ct.hci_code_commodity_type
left join JFS_COMMODITY_GROUP cg on hct.commodity_category_code=cg.hci_code_commodity_category and jc.id_agreement = cg.id_agreement
where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement < 4
AND JC.STATUS='S'
order by jc.id_agreement, c.contract_code