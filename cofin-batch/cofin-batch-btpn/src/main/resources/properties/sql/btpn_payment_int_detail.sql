select replace(jc.BANK_REFERENCE_NO,chr(13),'')||';'||jp.text_contract_number||';'||jp.date_payment||';'||jp.pmt_instalment
from jfs_payment_int jp
INNER join jfs_contract jc on jp.text_contract_number=jc.text_contract_number and Jc.bank_product_code='BTPN_PRODUCT'
where trunc(jp.date_export)=trunc(sysdate) and jp.payment_type='R' and jp.pmt_instalment > 0
and jp.status='P'
order by jp.date_payment