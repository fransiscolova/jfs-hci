select p.text_contract_number
||'|'||c.client_name
||'|'||to_char(p.date_payment,'dd/MM/yyyy')
||'|'||p.pmt_instalment
||'|'||p.pmt_penalty
||'|'||0
||'|'||p.pmt_fee
||'|'||'R'||chr(13)
from jfs_payment_int p
inner join jfs_contract c on p.text_contract_number=c.text_contract_number and c.bank_product_code='BTPN_PRODUCT'
where trunc(p.date_export)=trunc(sysdate) and not parent_id is null and p.payment_type='R'
order by p.text_contract_number