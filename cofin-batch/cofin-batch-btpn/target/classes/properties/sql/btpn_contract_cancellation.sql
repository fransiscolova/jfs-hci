select    replace(CON.BANK_REFERENCE_NO,chr(13),'')
  ||'|'|| CON.BANK_DECISION_DATE
  ||'|'|| CON.TEXT_CONTRACT_NUMBER
  ||'|'|| CON.CLIENT_NAME
  ||'|'|| CC.DTIME_SIGNED
  ||'|'|| trim(to_char(CON.BANK_PRINCIPAL,'999,999,999'))
  ||'|'|| trim(to_char(CC.PAID_INSTALMENT,'999,999,999'))--amt_total_payment
  ||'|'|| trim(to_char(CON.BANK_PRINCIPAL - CC.PAID_INSTALMENT,'999,999,999'))--amt_et
  ||chr(13)
FROM JFS_CANCELED_CONTRACT CC
JOIN JFS_CONTRACT CON ON CON.TEXT_CONTRACT_NUMBER = CC.TEXT_CONTRACT_NUMBER and con.bank_product_code='BTPN_PRODUCT'
WHERE TRUNC(CC.DTIME_EXPORTED) = TRUNC(sysdate)
--AND CC.SERVICE IS NULL;
	--This is X-Sell patch
	--and 'R' = (select con.contract_type from jfs_contract con where con.text_contract_number = con_et.text_contract_number)