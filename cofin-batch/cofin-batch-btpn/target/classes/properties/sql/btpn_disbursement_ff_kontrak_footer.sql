--footer
select 'HCI|'||count(*)||'|'||sum(fp.credit_amount)||chr(13)
from jfs_contract jc
inner join owner_int.vh_hom_contract c  on jc.text_contract_number=c.contract_code
left join owner_int.vh_hom_deal d on c.deal_code=d.code 
left join owner_int.vh_hom_client_snapshot cs on d.client_snapshot_id=cs.id ----and cs.is_new=1 
left join owner_int.vh_hom_contract_status_trans cst on c.id=cst.contract_id and cst.status='N' -- N=sign
left join owner_int.vh_hom_financial_parameters fp on c.id=fp.contract_id
where 
--FLEXI FAST CONTRACT ONLY
jc.id_agreement='4'
and jc.status='S'
and trunc(jc.export_date)=trunc(sysdate)