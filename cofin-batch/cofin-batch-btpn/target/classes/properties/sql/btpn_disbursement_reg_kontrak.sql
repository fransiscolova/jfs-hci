select c.contract_code
||'|'||(case when upper(trim(cs.name1))=upper(trim(cs.name2)) then cs.name1 else cs.full_name end)--trim(cs.full_name)
||'|'||vs.code -- c.salesroom_id
||'|'||c.contract_code
||'|'||to_char( case when (to_char(cst.creation_date,'DD')) in ('29','30','31')
					then ADD_MONTHS(i.due_date, -1)	
				else cst.creation_date
				end
				,'dd/MM/yyyy') 
	   /* to_char(cst.creation_date,'dd/MM/yyyy') */
||'|'||fp.credit_amount
||'|'||fp.goods_price_amount
||'|'||fp.terms
||'|'||fp.annuity_amount
||'|'||trim(to_char(case when (power(1+fp.interest_rate,1/12)-1)*1200>100 then 99 else (power(1+fp.interest_rate,1/12)-1)*1200 end,'00.00'))
||'|'||fp.credit_amount
||'|'||jc.send_principal
||'|'||fp.terms
||'|'||to_char(i.due_date,'dd/MM/yyyy')  --  ad_months only for testing only!!! as date_first_due
||'|'||to_char(i1.due_date,'dd/MM/yyyy')  --  ad_months only for testing only!!! as date_last_due
||'|'||trim(to_char(jc.send_rate,'00.00'))
||'|'||jc.send_instalment
||'|'||1
||'|'||'N'
||'|'||'T'
||'|'||0
||'|'||'E'
||'|'||'DG'
||'|'||'0'
||'|'||''
||'|'||to_char(cst.creation_date,'dd/MM/yyyy')
	   /* to_char(cst.creation_date,'dd/MM/yyyy') */
||'|'||0 --27
||'|'||(nvl(fp.cash_payment_amount, 0)-nvl((fp.advanced_payment_number * fp.annuity_amount), 0)-nvl(ft.AMT_FEES_IN_ADVANCE, 0))
||'|' -- 29. *||0
||'|'||es.bi_economic_sector_code  --'9990' -- 30. bidang usaha *'004100'
||'|'||'K' -- 31. +
||'|'
||'|'
||'|'
||'|'
||'|'
||'|'
||'|'
||'|'
||'|'
||'|'
||'|'
||'|'
||'|'
||chr(13)
as KONTRAK2
from jfs_contract jc
left join owner_int.vh_hom_contract c  on jc.text_contract_number=c.contract_code
left join owner_int.vh_hom_salesroom vs on c.salesroom_id=vs.id 
left join owner_int.vh_hom_deal d on c.deal_code=d.code 
left join owner_int.vh_hom_client_snapshot cs on d.client_snapshot_id=cs.id ----and cs.is_new=1 --disable on 21-02-2014 
left join owner_int.vh_hom_contract_status_trans cst on c.id=cst.contract_id and cst.status='N' -- N=sign
left join owner_int.vh_hom_financial_parameters fp on c.id=fp.contract_id and fp.archived = 0 -- 0 = not archived / active
left join (
          select fi.financial_parameters_id
          ,sum ( case when tt.usage_type in ('A', 'S') and tt.charging_periodicity = 'ONE_TIME' and tt.to_principal_flag = 0
                      THEN fi.ITEM_AMOUNT
                      else 0
                  end)amt_fees_in_advance 
          from owner_int.vh_hom_financial_param_item fi 
          left join owner_int.vh_hom_fin_par_item2tarif_it pt on pt.id=fi.id
          left join owner_int.vh_hom_tariff_item ti on ti.id=pt.tariff_item_id
          left join (
              ---Hardcoded beware...
              ---20160621 HoSel 2016.1. informed by Radan to replace 'tt.to_principal_flag' 
            select a.*, case when a.code='TITF_INS_PREMIUM_PRINCIPAL' then 1
                          else 0
                        end as to_principal_flag
            from owner_int.vh_hom_tariff_item_type a                             
          ) tt on tt.code=ti.tariff_item_type_code and tt.active_flag=1
          --left join owner_int.vh_hom_tariff_item_type tt on tt.code=ti.tariff_item_type_code and tt.active_flag=1
          group by fi.financial_parameters_id
)ft on ft.financial_parameters_id=fp.id
left join owner_int.vh_hom_installment i on c.id=i.contract_id and i.installment_version=1 and i.installment_number=1
left join owner_int.vh_hom_installment i1 on c.id=i1.contract_id and i1.installment_version=1 and i1.installment_number=fp.terms
left join owner_int.vh_hom_commodity hc on c.id=hc.contract_id
left join owner_int.vh_hom_commodity_type hct on hc.commodity_type_id=hct.id
left join JFS_COMMODITY_GROUP cg on hct.commodity_category_code=cg.hci_code_commodity_category and jc.id_agreement = cg.id_agreement
left join JFS_ECONOMIC_SECTOR es on cg.btpn_commodity_group=es.bank_commodity_group
where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement < 4
AND JC.STATUS='S'
order by jc.id_agreement, c.contract_code