select p.text_contract_number
||'|'||c.client_name
||'|'||to_char(p.date_payment,'dd/MM/yyyy')
||'|'||p.pmt_instalment
||'|'||p.pmt_penalty
||'|'||0
||'|'||p.pmt_fee
||'|'||p.payment_type||chr(13)
from jfs_payment_int p
inner join jfs_contract c on p.text_contract_number=c.text_contract_number and c.bank_product_code='BTPN_PRODUCT'
where trunc(p.date_export)=trunc(sysdate) and p.payment_type='R' and p.pmt_instalment > 0
and P.status='P'
order by p.date_payment