-- reconciliation records
select a.text_contract_number
||'|'||a.outstanding_principal
||'|'||a.DPD||chr(13) --||'|'||'ONHAND'
from jfs_reconciliation a
join jfs_contract jc
on jc.text_contract_number=a.text_contract_number
and jc.bank_product_code='BTPN_PRODUCT'
--where a.outstanding_principal>0
WHERE trunc(a.recon_date) = to_Date('1-'||to_char(sysdate,'MON-YYYY'),'DD-MON-YYYY')
order by a.text_contract_number