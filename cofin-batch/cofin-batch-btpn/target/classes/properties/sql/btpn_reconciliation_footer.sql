--footer
select 'HCI|'||count(*)||'|'||sum(a.outstanding_principal)||chr(13)
from jfs_reconciliation a
join jfs_contract jc
on jc.text_contract_number=a.text_contract_number
and jc.bank_product_code='BTPN_PRODUCT'
--where a.outstanding_principal>0
WHERE trunc(a.recon_date) = to_Date('1-'||to_char(sysdate,'MON-YYYY'),'DD-MON-YYYY')