select regexp_replace(jc.bank_reference_no,'[[:space:]]','')||';'||jc.text_contract_number||';'||jc.client_name
||';'|| (select (select sum(js.principal) from JFS_SCHEDULE js where js.text_contract_number = jc.text_contract_number) 
- (select sum(jpa.amt_principal) from JFS_PAYMENT_ALLOCATION jpa where jpa.text_contract_number = jc.text_contract_number) as nilai_hapus_buku from dual)
||';'|| wop.btpn_amt_payment
from jfs_contract jc 
join JFS_WO_PAYMENT wop on wop.text_contract_number = jc.text_contract_number and trunc(wop.date_export) = trunc(sysdate)
