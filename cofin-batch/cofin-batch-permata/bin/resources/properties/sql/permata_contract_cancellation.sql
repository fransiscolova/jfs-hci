select rpad(p.text_contract_number,17,' ')
||' '||rpad(c.client_name,50,' ')
||' '||rpad(to_char(p.dtime_cancelation,'dd/MM/yyyy'),10,' ')
||' '||rpad(case when p.service='COP_1' then c.bank_principal
            else c.bank_principal-nvl(pay.paid_instalment,0)
            end,12,' ')
||' '||rpad(0,12,' ')
||' '||rpad(0,12,' ')
||' '||rpad(0,12,' ')
||' '||rpad(0,12,' ') --diskon denda
from jfs_canceled_contract p
inner join jfs_contract c on p.text_contract_number=c.text_contract_number and c.bank_product_code='PERMATA_PRODUCT'
left join (select pay.text_contract_number,sum(pay.pmt_instalment) PAID_INSTALMENT from jfs_payment_int pay
          join jfs_contract con
          on con.text_Contract_number=pay.text_contract_number
          and con.bank_product_code='PERMATA_PRODUCT'
          GROUP BY PAY.TEXT_CONTRACT_NUMBER) pay
          on pay.text_Contract_number=p.text_contract_number
where trunc(p.dtime_exported)=trunc(sysdate)