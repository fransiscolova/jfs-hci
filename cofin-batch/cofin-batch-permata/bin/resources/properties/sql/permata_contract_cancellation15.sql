select rpad(p.text_contract_number,17,' ')
||' '||rpad(c.client_name,50,' ')
||' '||rpad(to_char(p.date_payment,'dd/MM/yyyy'),10,' ')
||' '||rpad(p.pmt_principal,12,' ')
||' '||rpad(p.pmt_interest,12,' ')
||' '||rpad(p.pmt_penalty,12,' ')
||' '||rpad(p.pmt_fee,12,' ')
||' '||rpad(0,12,' ') --diskon denda
from jfs_payment_int p
inner join jfs_contract c on p.text_contract_number=c.text_contract_number and c.bank_product_code='PERMATA_PRODUCT'
where trunc(p.date_export)=trunc(sysdate) and p.payment_type='T'