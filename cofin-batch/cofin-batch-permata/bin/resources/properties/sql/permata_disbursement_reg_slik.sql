--record
select REPLACE(slikcol, '[CR-LF]', chr(10) ) from 
(select 
RPAD(jc.text_contract_number,17,' ')
||' '||s.KODE_SIFAT_KREDIT_PEMBIAYAAN
||' '||s.KODE_JENIS_KREDIT
||' '||s.KODE_SKIM
||' '||s.BARU_PERPANJANGAN
||' '||s.KODE_JENIS_PENGGUNAAN
||' '||s.KODE_ORIENTASI_PENGGUNAAN
||' '||RPAD(NVL(s.TAKEOVER_DARI,' '),6,' ')
||' '||RPAD(NVL(s.ALAMAT_EMAIL,' '),150,' ')
||' '||RPAD(nvl(s.NO_TELEPON_SELULAR,' '),15,' ')
||' '||RPAD(NVL(replace(replace(trim(s.ALAMAT_TEMPAT_BEKERJA),CHR(13),''),CHR(10)),' '),300,' ')
||' '||RPAD(nvl(replace(replace(trim(s.PENGHASILAN_KOTOR_PER_TAHUN),CHR(13),''),CHR(10)),0),12,' ')
||' '||s.KODE_SUMBER_PENGHASILAN
||' '||RPAD(NVL(s.JUMLAH_TANGGUNGAN,0),2,' ')
||' '||RPAD(nvl(CASE WHEN upper(cs.gender_type)='F' AND xm.dst_code='2' then 'JOHN'
                 WHEN upper(cs.gender_type)='M' AND xm.dst_code='2' then 'JANE'
                ELSE ' ' END 
            ,' '),150,' ')
||' '||RPAD(CASE WHEN xm.DST_CODE='2' then KTP.NUM else ' ' END,16,' ')
||' '||RPAD(CASE WHEN XM.DST_CODE='2' THEN to_char(cif_c.birth,'dd/MM/yyyy') ELSE ' ' END,10,' ')
||' '||trim(s.PERJANJIAN_PISAH_HARTA)
||' '||RPAD(nvl(replace(replace(trim(s.KODE_BIDANG_USAHA),CHR(13),''),CHR(10)),' '),6,' ')
||' '||RPAD(NVL(replace(replace(trim(s.KODE_BENTUK_BADAN_USAHA),CHR(13),''),CHR(10)),' '),2,' ')
||' '||RPAD(replace(replace(trim(s.KODE_GOLONGAN_DEBITUR),CHR(13),''),CHR(10)),4,' ')
||' '||NVL(replace(replace(trim(s.GO_PUBLIC),CHR(13),''),CHR(10)),' ')
||' '||RPAD(NVL(replace(replace(trim(s.PERINGKAT_RATING_DEBITUR),CHR(13),''),CHR(10)),' '),6,' ')
||' '||RPAD(NVL(replace(replace(trim(s.LEMBAGA_PEMERINGKAT_RATING),CHR(13),''),CHR(10)),' '),2,' ')
||' '||RPAD(NVL(replace(replace(trim(s.TANGGAL_PEMERINGKAT),CHR(13),''),CHR(10)),' '),10,' ')
||' '||RPAD(NVL(replace(replace(trim(s.NAMA_GRUP_DEBITUR),CHR(13),''),CHR(10)),' '),150,' ')
||' '||RPAD(NVL(replace(replace(trim(s.KODE_JENIS_AGUNAN),CHR(13),''),CHR(10)),' '),3,' ')
||' '||RPAD(NVL(replace(replace(trim(s.KODE_JENIS_PENGIKATAN),CHR(13),''),CHR(10)),' '),2,' ')
||' '||RPAD(NVL(replace(replace(trim(s.TANGGAL_PENGIKATAN),CHR(13),''),CHR(10)),' '),10,' ')
||' '||RPAD(NVL(replace(replace(trim(s.NAMA_PEMILIK_AGUNAN),CHR(13),''),CHR(10)),' '),150,' ')
||' '||RPAD(NVL(replace(replace(trim(s.ALAMAT_AGUNAN),CHR(13),''),CHR(10)),' '),300,' ')
||' '||NVL(replace(replace(trim(s.STATUS_KREDIT_JOIN),CHR(13),''),CHR(10)),' ')
||' '||RPAD(NVL(replace(replace(trim(s.DATI2),CHR(13),''),CHR(10)),' '),4,' ')
||' '||NVL(replace(replace(trim(s.DIASURANSIKAN),CHR(13),''),CHR(10)),' ')
as slikcol
from
JFS_CONTRACT JC
join
JFS_SLIK_TMP s
on jc.text_contract_number=s.text_contract_number
join owner_int.vh_hom_contract c  on jc.text_contract_number=c.contract_code
join owner_int.vh_hom_deal d on c.deal_code=d.code 
join owner_int.vh_hom_client cl on d.client_id=cl.id
join owner_int.vh_hom_client_snapshot cs on d.client_snapshot_id=cs.id
join owner_int.vh_cif_customer cif_c on cl.cuid=cif_c.cuid
left join owner_int.vh_cif_customer_document cd on cif_c.id=cd.id_customer
join owner_int.vh_cif_document ktp on cd.id_document=ktp.id AND KTP.TYPE='KTP' and KTP.STATUS='a'
left join JFS_code_translate xm on xm.codelist='MARITAL_STATUS' and cs.marital_status_code=xm.src_code
where 
trunc(jc.export_date)=TRUNC(SYSDATE) and 
jc.id_agreement=51
AND JC.STATUS='S'
and date_process=to_char(TRUNC(sysdate),'YYYYMMDD')
order by jc.text_contract_number)