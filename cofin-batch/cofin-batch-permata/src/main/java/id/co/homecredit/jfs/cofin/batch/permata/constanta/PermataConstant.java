package id.co.homecredit.jfs.cofin.batch.permata.constanta;

/**
 * Permata constants.
 *
 * @author muhammad.muflihun
 *
 */
public class PermataConstant {

    // File properties
    public final static String DISBURSEMENT_REG_PROPERTIES = "disbursement.reg.properties";
    public final static String PAYMENT_PROPERTIES = "payment.properties";
    public final static String CONTRACT_CANCELLATION_PROPERTIES = "contract.cancellation.properties";
    public final static String CONTRACT_CANCELLATION15_PROPERTIES = "contract.cancellation15.properties";
    public final static String RECONCILIATION_PROPERTIES = "reconciliation.properties";

}
