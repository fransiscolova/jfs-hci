package id.co.homecredit.jfs.cofin.batch.permata.job;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Execute batch process for specific step using spring batch.
 *
 * @author muhammad.muflihun
 *
 */
public class BatchProcessPermataSpecificStep implements org.quartz.Job {
    private static final Logger log = LogManager.getLogger(BatchProcessPermataSpecificStep.class);

    @SuppressWarnings("resource")
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String stepName = null;
        String jobName = null;
        try {
            jobName = context.getScheduler().getContext().getString("jobName");
            stepName = context.getScheduler().getContext().getString("stepName");
        } catch (SchedulerException e1) {
            e1.printStackTrace();
        }
        log.debug("passed job name {}", jobName);
        log.debug("passed step name {}", stepName);

        ApplicationContext appContext = new ClassPathXmlApplicationContext(
                "cofin-batch-permata-context.xml");
        JobLauncher jobLauncher = (JobLauncher) appContext.getBean("jobLauncher");
        Job job = (Job) appContext.getBean(jobName);
        JobParameters jobParameters = new JobParametersBuilder().addString("jobName", jobName)
                .addString("stepName", stepName).addDate("runTime", new Date()).toJobParameters();

        try {
            JobExecution execution = jobLauncher.run(job, jobParameters);
            log.info("execution status {}", execution.getStatus());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("main batch btpn exception : {}", e.getMessage());
            log.error(e.getStackTrace()[0]);
        }
    }

}
