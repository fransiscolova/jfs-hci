package id.co.homecredit.jfs.cofin.batch.permata.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.permata.service.BatchProcessPermataDailyService;
import id.co.homecredit.jfs.cofin.batch.permata.service.BatchProcessPermataMonthlyService;

/**
 * Main class to starting batch process for Permata.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class BatchProcessPermata {
    private static final Logger log = LogManager.getLogger(BatchProcessPermata.class);

    /**
     * Run batch executor.
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(final String[] args) throws InterruptedException {
        log.info("starting PERMATA main");
        try {
            if (args == null || args.length < 1) {
                log.info("BatchProcessPermata d|m jobName stepName");
                return;
            }

            @SuppressWarnings("resource")
            ApplicationContext appContext = new ClassPathXmlApplicationContext(
                    "cofin-batch-permata-context.xml");
            switch (args[0].charAt(0)) {
                case 'd':
                    // run spring batch depends on number of arguments
                    BatchProcessPermataDailyService dailyService = appContext
                            .getBean(BatchProcessPermataDailyService.class);
                    if (args.length == 1) {
                        dailyService.runJobOnce();
                    } else if (args.length == 2) {
                        String jobName = args[1];
                        dailyService.runJobWithQuartz(jobName);
                    } else if (args.length == 3) {
                        String jobName = args[1];
                        String stepName = args[2];
                        dailyService.runStepOnce(jobName, stepName);
                    }
                    break;
                case 'm':
                    // run spring batch depends on number of arguments
                    BatchProcessPermataMonthlyService monthlyService = appContext
                            .getBean(BatchProcessPermataMonthlyService.class);
                    if (args.length == 1) {
                        monthlyService.runJobOnce();
                    } else if (args.length == 2) {
                        String jobName = args[1];
                        monthlyService.runJobWithQuartz(jobName);
                    } else if (args.length == 3) {
                        String jobName = args[1];
                        String stepName = args[2];
                        monthlyService.runStepOnce(jobName, stepName);
                    }
                    break;
                default:
                    log.info("Invalid operation: must be (d)aily or (m)onthly.");
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("main batch permata exception : {}", e.getMessage());
            log.error(e.getStackTrace()[0]);
        }
    }
}
