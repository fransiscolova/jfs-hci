package id.co.homecredit.jfs.cofin.batch.permata.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.batch.base.dao.BatchJobDao;
import id.co.homecredit.jfs.cofin.batch.permata.job.BatchProcessPermataJob;
import id.co.homecredit.jfs.cofin.batch.permata.job.BatchProcessPermataSpecificStep;
import id.co.homecredit.jfs.cofin.batch.permata.service.BatchProcessPermataMonthlyService;

/**
 * Service implement for triggering spring batch.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class BatchProcessPermataMonthlyServiceImpl implements BatchProcessPermataMonthlyService {
    private static final Logger log = LogManager
            .getLogger(BatchProcessPermataMonthlyServiceImpl.class);

    @Autowired
    private BatchJobDao batchJobDao;

    @Override
    public void runJobOnce() throws Exception {
        try {
            JobDetail jd = JobBuilder.newJob(BatchProcessPermataJob.class).build();
            Trigger trigger = TriggerBuilder.newTrigger().startNow().build();

            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.scheduleJob(jd, trigger);
            scheduler.getContext().put("jobName", "PERMATA_monthly_job");
            scheduler.start();
            log.info("Batch successfully started");
        } catch (SchedulerException e) {
            e.printStackTrace();
            log.error("service exception : {}", e.getMessage());
            log.error(e.getStackTrace()[0]);
        }
    }

    @Override
    public void runJobWithQuartz(String jobName) {
        try {
            String cronExpression = batchJobDao.getCronExpressionByJobName(jobName);
            log.debug("cron {}", cronExpression);

            CronScheduleBuilder cronBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
            JobDetail jd = JobBuilder.newJob(BatchProcessPermataJob.class).build();
            Trigger trigger = TriggerBuilder.newTrigger().withSchedule(cronBuilder).build();

            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.scheduleJob(jd, trigger);
            scheduler.getContext().put("jobName", jobName);
            scheduler.getContext().put("cronExpression", cronExpression);
            scheduler.start();
            log.info("Batch {} successfully triggered with cron expression {}", jobName,
                    cronExpression);
        } catch (SchedulerException e) {
            e.printStackTrace();
            log.error("main batch {} exception : {}", jobName, e.getMessage());
            log.error(e.getStackTrace()[0]);
        }
    }

    @Override
    public void runStepOnce(String jobName, String stepName) throws Exception {
        try {
            JobDetail jd = JobBuilder.newJob(BatchProcessPermataSpecificStep.class).build();
            Trigger trigger = TriggerBuilder.newTrigger().startNow().build();

            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.scheduleJob(jd, trigger);
            scheduler.getContext().put("jobName", jobName);
            scheduler.getContext().put("stepName", stepName);
            scheduler.start();
            log.info("Batch successfully started");
        } catch (SchedulerException e) {
            e.printStackTrace();
            log.error("service exception : {}", e.getMessage());
            log.error(e.getStackTrace()[0]);
        }
    }
}
