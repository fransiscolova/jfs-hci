package id.co.homecredit.jfs.cofin.batch.permata.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.permata.constanta.PermataConstant;
import id.co.homecredit.jfs.cofin.batch.permata.util.BatchPermataPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process contract cancellation 15 for PERMATA.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class ContractCancellation15Tasklet extends PermataTasklet {
    private static final Logger log = LogManager.getLogger(ContractCancellation15Tasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String ctrCancellation15PropertiesPath = BatchPermataPropertyUtil
                .get(PermataConstant.CONTRACT_CANCELLATION15_PROPERTIES);
        Properties ctrCancellation15Prop = BatchPermataPropertyUtil
                .getProperties(ctrCancellation15PropertiesPath);
        generateCtrCancellation15(ctrCancellation15Prop);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generateCtrCancellation15(Properties properties) throws Exception {
        log.info("generate contract cancellation 15");

        // Generate payment header file
        String ctrCancellation15Sql = getSqlFileFullpath(
                properties.getProperty("contract.cancellation15.sql"));
        String exportCtrCancellation15Name = properties.getProperty("contract.cancellation15.name");
        String ctrCancellation15File = exportToFile(exportCtrCancellation15Name,
                ctrCancellation15Sql);
        
        //detail
        String ctrCancellation15SqlDetail = getSqlFileFullpath(
                properties.getProperty("contract.cancellation15.detail.sql"));
        String exportCtrCancellation15DetailName = properties.getProperty("contract.cancellation15.detail.name");
        String ctrCancellation15DetailFile = exportToFile(exportCtrCancellation15DetailName,
        		ctrCancellation15SqlDetail);
        
        
        if (ctrCancellation15DetailFile == null) {
            log.info("contract cancellation after 15 detail file is not generated");
            return;
        }
        
        
        if (ctrCancellation15File == null) {
            log.info("contract cancellation after 15 file is not generated");
            return;
        }
        
        
        Integer ctrCancellation15FileRows = FileUtil.countLines(ctrCancellation15File);
        log.info("file contract cancellation 15 rows {}", ctrCancellation15FileRows);
        
        Integer ctrCancellation15DetailFileRows = FileUtil.countLines(ctrCancellation15DetailFile);
        log.info("file contract cancellation 15 detail rows {}", ctrCancellation15DetailFileRows);
        
        
    }

}
