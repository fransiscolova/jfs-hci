package id.co.homecredit.jfs.cofin.batch.permata.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.permata.constanta.PermataConstant;
import id.co.homecredit.jfs.cofin.batch.permata.util.BatchPermataPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process contract cancellation for PERMATA.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class ContractCancellationTasklet extends PermataTasklet {
    private static final Logger log = LogManager.getLogger(ContractCancellationTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String ctrCancellationPropertiesPath = BatchPermataPropertyUtil
                .get(PermataConstant.CONTRACT_CANCELLATION_PROPERTIES);
        Properties ctrCancellationProp = BatchPermataPropertyUtil
                .getProperties(ctrCancellationPropertiesPath);
        generateContractCancellation(ctrCancellationProp);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generateContractCancellation(Properties properties) throws Exception {
        log.info("generate contract cancellation");

        // Generate contract cancellation file
        String ctrCancellationSql = getSqlFileFullpath(
                properties.getProperty("contract.cancellation.sql"));
        String exportCtrCancellationName = properties.getProperty("contract.cancellation.name");
        String ctrCancellationFile = exportToFile(exportCtrCancellationName, ctrCancellationSql);
        if (ctrCancellationFile == null) {
            log.info("contract cancellation file is not generated");
            return;
        }
        Integer ctrCancellationFileRows = FileUtil.countLines(ctrCancellationFile);
        log.info("file contract cancellation rows {}", ctrCancellationFileRows);
    }

}
