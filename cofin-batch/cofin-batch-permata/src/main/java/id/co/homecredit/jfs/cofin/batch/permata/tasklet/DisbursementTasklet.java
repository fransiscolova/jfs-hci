package id.co.homecredit.jfs.cofin.batch.permata.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.dao.AgreementDao;
import id.co.homecredit.jfs.cofin.batch.base.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.batch.permata.constanta.PermataConstant;
import id.co.homecredit.jfs.cofin.batch.permata.util.BatchPermataPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process disbursement for PERMATA.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class DisbursementTasklet extends PermataTasklet {
    private static final Logger log = LogManager.getLogger(DisbursementTasklet.class);

    @Autowired
    private AgreementDao agreementDao;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String jobName = chunkContext.getStepContext().getJobName();
        String partnerCode = jobName.split("_")[0];
        for (AgreementTypeEnum agreementType : AgreementTypeEnum.values()) {
            // check if agreement type is exist and active
            if (!agreementDao.isAgreementExistByPartnerCodeAndAgreementType(partnerCode,
                    agreementType)) {
                log.info("agreement not exist for partner code {} and agreement type {}",
                        partnerCode, agreementType);
                continue;
            }

            if (agreementType.equals(AgreementTypeEnum.REG)) {
                String regPropertiesPath = BatchPermataPropertyUtil
                        .get(PermataConstant.DISBURSEMENT_REG_PROPERTIES);
                Properties regProp = BatchPermataPropertyUtil.getProperties(regPropertiesPath);
                generateDisbursement(regProp, agreementType);
            } else {
                log.info("no implementation yet for agreement type {}", agreementType);
            }
        }
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties and agreement type.
     *
     * @param properties
     * @param agreementType
     * @throws Exception
     */
    private void generateDisbursement(Properties properties, AgreementTypeEnum agreementType)
            throws Exception {
        log.info("generate disbursement for {}", agreementType);

        // Generate kontrak file
        String kontrakSql = getSqlFileFullpath(properties.getProperty("disbursement.kontrak.sql"));
        String exportKontrakName = properties.getProperty("disbursement.kontrak.name");
        String kontrakFile = exportToFile(exportKontrakName, kontrakSql);
        Integer kontrakFileRows = FileUtil.countLines(kontrakFile);
        log.info("kontrak file rows {}", kontrakFileRows);

        // Generate barang file
        String barangSql = getSqlFileFullpath(properties.getProperty("disbursement.barang.sql"));
        String exportBarangName = properties.getProperty("disbursement.barang.name");
        String barangFile = exportToFile(exportBarangName, barangSql);
        Integer barangFileRows = FileUtil.countLines(barangFile);
        log.info("barang file rows {}", barangFileRows);
        if (barangFileRows != null && !kontrakFileRows.equals(barangFileRows)) {
            String message = "row number of barang (" + barangFileRows
                    + ") is different than kontrak (" + kontrakFileRows + ")";
            sendEmailThenThrowException(message, barangSql);
        }

        // Generate enduser file
        String enduserSql = getSqlFileFullpath(properties.getProperty("disbursement.enduser.sql"));
        String exportEnduserName = properties.getProperty("disbursement.enduser.name");
        String enduserFile = exportToFile(exportEnduserName, enduserSql);
        Integer enduserFileRows = FileUtil.countLines(enduserFile);
        log.info("file enduser rows {}", enduserFileRows);
        if (enduserFileRows != null && !kontrakFileRows.equals(enduserFileRows)) {
            String message = "row number of enduser (" + enduserFileRows
                    + ") is different than kontrak (" + kontrakFileRows + ")";
            sendEmailThenThrowException(message, enduserSql);
        }

        // Generate pengurus file
        String pengurusSql = getSqlFileFullpath(
                properties.getProperty("disbursement.pengurus.sql"));
        String exportPengurusName = properties.getProperty("disbursement.pengurus.name");
        String pengurusFile = exportToFile(exportPengurusName, pengurusSql);
        Integer pengurusFileRows = FileUtil.countLines(pengurusFile);
        log.info("file pengurus rows {}", pengurusFileRows);
        if (pengurusFileRows != null && !kontrakFileRows.equals(pengurusFileRows)) {
            String message = "row number of pengurus (" + pengurusFileRows
                    + ") is different than kontrak (" + kontrakFileRows + ")";
            sendEmailThenThrowException(message, pengurusSql);
        }

        // Generate slik file
        String slikSql = getSqlFileFullpath(properties.getProperty("disbursement.slik.sql"));
        String exportSlikName = properties.getProperty("disbursement.slik.name");
        String slikFile = exportToFile(exportSlikName, slikSql);
        Integer slikFileRows = FileUtil.countLines(slikFile);
        log.info("file slik rows {}", slikFileRows);
        if (slikFileRows != null && !kontrakFileRows.equals(slikFileRows)) {
            String message = "row number of slik (" + slikFileRows + ") is different than kontrak ("
                    + kontrakFileRows + ")";
            sendEmailThenThrowException(message, slikSql);
        }
    }

}
