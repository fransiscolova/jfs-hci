package id.co.homecredit.jfs.cofin.batch.permata.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.permata.constanta.PermataConstant;
import id.co.homecredit.jfs.cofin.batch.permata.util.BatchPermataPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process payment for PERMATA.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class PaymentTasklet extends PermataTasklet {
    private static final Logger log = LogManager.getLogger(PaymentTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String paymentPropertiesPath = BatchPermataPropertyUtil
                .get(PermataConstant.PAYMENT_PROPERTIES);
        Properties paymentProp = BatchPermataPropertyUtil.getProperties(paymentPropertiesPath);
        generatePayment(paymentProp);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @throws Exception
     */
    private void generatePayment(Properties properties) throws Exception {
        log.info("generate payment");

        // Generate payment header file

        String paymentSql = getSqlFileFullpath(properties.getProperty("payment.sql"));
        String exportPaymentName = properties.getProperty("payment.name");
        String paymentFile = exportToFile(exportPaymentName, paymentSql);
        Integer paymentFileRows = FileUtil.countLines(paymentFile);
        log.info("file payment rows {}", paymentFileRows);

        // Generate payment detail file
        String paymentDetailSql = getSqlFileFullpath(properties.getProperty("payment.detail.sql"));
        String exportPaymentDetailName = properties.getProperty("payment.detail.name");
        String paymentDetailFile = exportToFile(exportPaymentDetailName,
                paymentDetailSql);
        Integer paymentDetailFileRows = FileUtil.countLines(paymentDetailFile);
        log.info("file payment detail rows {}", paymentDetailFileRows);
        
        if(paymentFileRows==null) {
        	log.info("file payment rows is empty{}", paymentFileRows);	
        }else {
        if (!paymentFileRows.equals(paymentDetailFileRows)) {
            String message = "row number of payment detail (" + paymentDetailFileRows
                    + ") is different than payment (" + paymentFileRows + ")";
            sendEmailThenThrowException(message, paymentDetailSql
                    );
        }
        }
    }

}
