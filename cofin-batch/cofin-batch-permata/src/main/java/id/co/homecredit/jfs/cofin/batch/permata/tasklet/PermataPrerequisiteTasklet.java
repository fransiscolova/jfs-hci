package id.co.homecredit.jfs.cofin.batch.permata.tasklet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.dao.BatchJobDao;
import id.co.homecredit.jfs.cofin.batch.base.tasklet.PrerequisiteTasklet;

/**
 * Prerequisite step to be executed before other steps.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class PermataPrerequisiteTasklet extends PrerequisiteTasklet {
    private static final Logger log = LogManager.getLogger(PermataPrerequisiteTasklet.class);

    @Autowired
    private BatchJobDao batchJobDao;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        log.debug("PERMATA prerequisite step");
        String jobName = chunkContext.getStepContext().getJobName();
        log.debug("check for job name {}", jobName);

        // do holiday check
        super.execute(contribution, chunkContext);

        log.debug("check PERMATA flag generate");
        String partnerName = jobName.split("_")[0];
        if (!batchJobDao.isFlagGenerateActiveByPartnerName(partnerName)) {
            String exitMessage = "FLAG GENERATE DEACTIVED";
            contribution.setExitStatus(new ExitStatus(exitMessage));
            log.info("exit step with message {}", exitMessage);
            return RepeatStatus.FINISHED;
        }
        return RepeatStatus.FINISHED;
    }

}
