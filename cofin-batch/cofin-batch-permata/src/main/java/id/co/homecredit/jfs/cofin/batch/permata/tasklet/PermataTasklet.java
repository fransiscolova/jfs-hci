package id.co.homecredit.jfs.cofin.batch.permata.tasklet;

import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.base.tasklet.BaseTasklet;
import id.co.homecredit.jfs.cofin.batch.permata.util.BatchPermataPropertyUtil;

/**
 * Batch step to be extended by other step in PERMATA.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class PermataTasklet extends BaseTasklet {
    private static final Logger log = LogManager.getLogger(PermataTasklet.class);

    /**
     * Send email when row numbers are inconsistent before throw exception. Send email using batch
     * BTPN configuration.
     *
     * @param message
     * @param sql
     * @throws EmailException
     */
    protected void sendEmailThenThrowException(String message, String... sql)
            throws EmailException {
        String subject = BatchPermataPropertyUtil.get("email.subject.disbursement.rows");
        String to = BatchPermataPropertyUtil.get("email.to.it.bau");
        String fromEmail = BatchPermataPropertyUtil.get("email.from.email");
        String fromName = BatchPermataPropertyUtil.get("email.from.name");
        sendEmailThenThrowException(subject, to, message, fromEmail, fromName, sql);
    }
}
