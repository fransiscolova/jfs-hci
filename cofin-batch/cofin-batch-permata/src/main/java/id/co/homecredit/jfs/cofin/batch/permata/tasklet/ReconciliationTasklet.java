package id.co.homecredit.jfs.cofin.batch.permata.tasklet;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.batch.permata.constanta.PermataConstant;
import id.co.homecredit.jfs.cofin.batch.permata.util.BatchPermataPropertyUtil;
import id.co.homecredit.jfs.cofin.common.util.FileUtil;

/**
 * Batch step to process reconciliation for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class ReconciliationTasklet extends PermataTasklet {
    private static final Logger log = LogManager.getLogger(ReconciliationTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
        super.execute(contribution, chunkContext);
        String reconciliationPropertiesPath = BatchPermataPropertyUtil
                .get(PermataConstant.RECONCILIATION_PROPERTIES);
        Properties reconciliationProp = BatchPermataPropertyUtil
                .getProperties(reconciliationPropertiesPath);
        generateReconciliation(reconciliationProp, reconciliationPropertiesPath);
        return RepeatStatus.FINISHED;
    }

    /**
     * Generate all files by properties.
     *
     * @param properties
     * @param propertiesPath
     * @throws Exception
     */
    private void generateReconciliation(Properties properties, String propertiesPath)
            throws Exception {
        log.info("generate reconciliation");

        // get date reconciliation
        String dateReconciliationSql = getSqlFileFullpath(
                properties.getProperty("reconciliation.date.sql"));
        String reconDate = readQueryFromFileAsStringList(dateReconciliationSql).get(0);
        log.info("reconciliation date obtained {}", reconDate);

        // execute reconciliation procedure
        String insertReconciliationSql = getSqlFileFullpath(
                properties.getProperty("reconciliation.insert.sql"));
        String insertReconciliationQuery = FileUtil.loadFileContent(insertReconciliationSql);
        executeSqlProcedure(insertReconciliationQuery, reconDate);
        log.info("procedure insert reconciliation success");

        // generate reconciliation file
        String reconciliationSql = getSqlFileFullpath(properties.getProperty("reconciliation.sql"));
        String exportReconciliationName = properties.getProperty("reconciliation.name");
        String reconciliationFile = exportToFile(exportReconciliationName, reconciliationSql);
        Integer reconciliationFileRows = FileUtil.countLines(reconciliationFile);
        log.info("reconciliation file rows {}", reconciliationFileRows);
    }

}
