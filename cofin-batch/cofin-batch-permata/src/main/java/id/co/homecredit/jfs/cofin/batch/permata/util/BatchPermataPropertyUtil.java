package id.co.homecredit.jfs.cofin.batch.permata.util;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Helper class to get static text from properties file.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class BatchPermataPropertyUtil implements InitializingBean {
    private final static String source = "file:/cofin/data/jfs/properties/batch-permata.properties";

    private static BatchPermataPropertyUtil singleInst;

    /**
     * Get string property by key.
     *
     * @param key
     * @return string
     */
    public static String get(String key) {
        if (key == null || key.isEmpty()) {
            return "";
        }
        return singleInst.properties.getProperty(key);
    }

    /**
     * Get all properties keys by property path.
     *
     * @param propPath
     * @return set
     * @throws Exception
     */
    public static Set<Object> getAllKeys(String propPath) throws Exception {
        Properties prop = getProperties(propPath);
        return prop.keySet();
    }

    /**
     * Load properties by property path.
     *
     * @param propPath
     * @return properties
     * @throws Exception
     */
    public static Properties getProperties(String propPath) throws Exception {
        Properties prop = new Properties();
        prop.load(new FileInputStream(propPath));
        return prop;
    }

    @Autowired
    protected ApplicationContext context;

    private Properties properties;

    protected BatchPermataPropertyUtil() {
        properties = new Properties();
        singleInst = this;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        properties.load(context.getResource(source).getInputStream());
    }

}
