--records
select 
RPAD(c.contract_code,17,' ')--NO PIN/NOMOR KONTRAK
||' '||RPAD(' ',15,' ')--CAR REG NUMBER / NOMOR FAKTUR
||' '||RPAD(' ',10,' ')--CAR REG DATE / TGL FAKTUR
||' '||RPAD((case when upper(trim(cs.name1))=upper(trim(cs.name2)) then cs.name1 else cs.full_name end),50,' ')--NAME ON CAR REGISTRATION
||' '||RPAD(' ',4,' ')--PLACE ON CAR REGISTRATION
||' '||RPAD(NVL(substr(hc.model_number||' '||hc.serial_number,1,22),' '),22,' ')--SERIAL NUMBER
||' '||RPAD(NVL(hc.engine_number,' '),22,' ')--CHASSIS NUMBER
||' '||RPAD(' ',10,' ')--LICENSE PLATE NUMBER
||' '||RPAD(NVL(Hct.Commodity_Category_Code,' '),30,' ')--BRAND/MODEL/COMMODITY CATEGORY
||' '||RPAD(NVL(HCT.CODE,' '),30,' ')--COMMODITY TYPE
||' '||'N'--CONDITION OF GOODS
||' '||to_char(sysdate,'YYYY')--ASSEMBLY_YEAR
||' '||RPAD(case  
          when ct.btpn_code_commodity_type in ('CT_CT_PDA', 'CT_CT_MTAB')            
            then 'DGN2'
          else
            cg.btpn_commodity_group
        end,5,' ')--TYPE PF GOODS/DGN CODE
||' '||RPAD(NVL(hc.producer,' '),51,' ')--TYPE OF GOODS
||' '||RPAD(fp.goods_price_amount,12,' ')--GOODS PRICE
||' '||RPAD(fp.goods_price_amount+NVL(INSURANCE.AMT_PREMIUM,0),12,' ')--GOODS VALUE + INSURANCE
||chr(13)
as BARANG
from JFS_CONTRACT JC
join owner_int.Vh_HOM_CONTRACT c  on jc.text_contract_number=c.contract_code 
left join owner_int.vh_hom_deal d on c.deal_code=d.code 
left join owner_int.vh_hom_client_snapshot cs on d.client_snapshot_id=cs.id ----and cs.is_new=1
left join 
  (select contract_id, commodity_type_id, engine_number, model_number, producer, serial_number,ROW_NUMBER() OVER (PARTITION BY CONTRACT_ID ORDER BY PRICE_AMOUNT DESC) rown
    from owner_int.vh_hom_commodity) hc on c.id=hc.contract_id and hc.rown=1
left join owner_int.vh_hom_commodity_type hct on hc.commodity_type_id=hct.id
left join owner_int.vh_hom_financial_parameters fp on c.id=fp.contract_id
left join (
    select
    fca.text_contract_number,sum(fia.amt_premium) amt_premium
    from OWNER_DWH.f_contract_ad fca
    join jfs_contract jc on jc.text_contract_number=fca.text_contract_number and jc.bank_product_code='PERMATA_PRODUCT'
    join OWNER_DWH.f_insurance_at fia on fca.skp_credit_case = fia.skp_credit_case and fia.code_insurance_company_payment <> 'XNA'
    join OWNER_DWH.dc_service ds on fia.skp_service = ds.skp_service
    group by fca.text_contract_number
) insurance on insurance.text_contract_number=jc.text_contract_number
left join jfs_commodity_type ct on hct.commodity_category_code=ct.hci_code_commodity_category and hct.code=ct.hci_code_commodity_type
left join JFS_COMMODITY_GROUP cg on hct.commodity_category_code=cg.hci_code_commodity_category and jc.id_agreement = cg.id_agreement
--where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement < 4
where trunc(jc.export_date)=TRUNC(SYSDATE) and jc.id_agreement=51 and jc.status='S'
order by jc.id_agreement, c.contract_code