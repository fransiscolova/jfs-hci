select 
RPAD(c.contract_code,17,' ')--CONTRACT NUMBER
||' '||RPAD(case when upper(trim(cs.name1))=upper(trim(cs.name2)) 
              then trim(cs.name1)
            else trim(cs.full_name)
       end,50,' ')--ENDUSER NAME
||' '||RPAD(case when cs.nickname is not null and length(cs.nickname) > 2
              then cs.nickname
            when cs.name1 is not null and length(cs.name1) > 2
              then cs.name1
            when cs.name2 is not null and length(cs.name2) > 2
              then cs.name2
            when cs.name3 is not null and length(cs.name3) > 2
              then cs.name3
            else cs.full_name
       end,50,' ')--NICKNAME=03
||' '||RPAD(xe.dst_code,4,' ')--EDUCATION_STATUS
||' '||RPAD(xe.description,50,' ')--STATUS DESCRIPTION
||' '||'907'--DEBTOR CLASS
||' '||case when upper(cs.gender_type)='F' then '2' else '1' end--GENDER
||' '||RPAD(ktp.num,30,' ')--ID CARD NO
||' '||RPAD(NVL(pas.num,' '),30,' ')--PASSPORT
||' '||RPAD(' ',30,' ')--BIRTH CERTIFICATE NUMBER
||' '||RPAD(cif_c.birth_place,50,' ')--PLACE OF BIRTH
||' '||RPAD(to_char(cif_c.birth,'dd/MM/yyyy'),10,' ')--DATE OF BIRTH
||' '||RPAD(' ',10,' ')--DATE OF FINAL DEED
||' '||RPAD(' ',20,' ')--TAX REGISTRATION/NPWP
||' '||RPAD(case when adr.street_name is not null
              and adr.street_name <> '0'
              and adr.street_name <> '00'
              and adr.street_name <> '99'
              and upper(adr.street_name) <> 'XNA'
            then  case
                    when UPPER(adr.street_name) like '%JL%' then adr.street_name
                    when UPPER(adr.street_name) like '%JALAN%' then adr.street_name
                    when UPPER(adr.street_name) like '%GG%' then adr.street_name
                    when UPPER(adr.street_name) like '%GANG%' then adr.street_name
                    else 'Jl '||adr.street_name 
                  end
        end
      ||case when adr.house_number is not null 
              and adr.house_number <> '0'
              and adr.house_number <> '00'
              and adr.house_number <> '99'
              and upper(adr.house_number) <> 'XNA'
            then ' '||adr.house_number 
        end
      ||' RT' || adr.block || ' RW'|| adr.block_set 
      ,100,' ')--DEBTOR ADDRESS
||' '||RPAD(upper(regexp_replace(regexp_replace(case when tw.name is null 
                                          then adr.town_value         
                                          else tw.name end
                                    , '*[[:punct:]]', ' ')
                      ,'( *[  ])',' ')
      ),50,' ')--SUB DISTRICT
||' '||RPAD(upper(regexp_replace(regexp_replace(case when sd.name is null 
                                          then adr.sub_district_value 
                                          else sd.name end
                                    , '*[[:punct:]]', ' ')
                      ,'( *[  ])',' ')
      ),50,' ')--KECAMATAN
--||' '||RPAD(case when xd.dst_code is null and adr.zipcode_value is not null then xy.dst_code
--            when xd.dst_code is null and adr.zipcode_value is null then xz.dst_code
--            when xd.dst_code is not null then xd.dst_code
--            end,4,' ')--DATI2
||' '||RPAD(case
  when zz.dati2_code is not null then zz.dati2_code
  when zy.dati2_code is not null then zy.dati2_code
  ELSE ' '
end,4,' ')--DATI2
||' '||RPAD(nvl(case when zc.zip_code is null then adr.zipcode_value else zc.zip_code end,' '),5,' ') --zc.zip_code
||' '||RPAD(' ',4,' ')--PHONE AREA CODE
||' '||RPAD(NVL(hc.value1,' '),10,' ') --PHONE NUMBER
||' '||RPAD('ID',3,' ') --COUNTRY OF DOMICILE
||' '||RPAD(nvl(cs.name4,' '),50,' ')--MAIDEN NAME OF BIOLOGICAL MOTHER
||' '||case when er.economical_status_code='RETIRED' then '012' 
            when er.economical_status_code='STUDENT' then '013' 
            when er.economical_status_code='SELFEMPLOYED' then '014'
            when er.employer_industry_code='ARMY_POLICE' then '011' 
            when er.economical_status_code='STATE_EMPLOYEE' then '010' 
            when er.employer_industry_code='GOVERNMENT' then '010' 
            else '099' end--OCCUPATION_CODE
||' '||RPAD(nvl(case when LENGTH(er.name) < 50
          then er.name
        else
          SUBSTR(er.name, 1, 50)
        end,' '),50,' ')--PLACE OF WORK
||' '||RPAD(nvl(xi.dst_code,'9990'),5,' ')--FIELD OF BUSINESS
||' '||'1 '--ID TYPE
||' '||RPAD(nvl(xm.dst_code,'30'),2,' ')--MARITAL STATUS
||' '||'ID'--KEWARGANEGARAAN
||' '||RPAD(nvl(xr.dst_code,'7'),2,' ')--RELIGION
||' '||RPAD(fd.occupation_income_amount+nvl(fd.by_work_income_amount,0),12,' ') -- income 
||' '||RPAD(to_char(ktp.valid_to,'dd/MM/yyyy'),10,' ')--ID EXPIRED
||chr(13)
as ENDUSER 
from JFS_CONTRACT JC 
join owner_int.vh_hom_contract c  on jc.text_contract_number=c.contract_code 
left join owner_int.vh_hom_deal d on c.deal_code=d.code 
left join owner_int.vh_hom_client_snapshot cs on d.client_snapshot_id=cs.id
left join owner_int.vh_hom_client cl on d.client_id=cl.id
left join owner_int.vh_cif_customer cif_c on cl.cuid=cif_c.cuid
left join owner_int.vh_cif_customer_document cd on cif_c.id=cd.id_customer
left join owner_int.vh_cif_document ktp on cd.id_document=ktp.id
left join (
   select doc.id,doc.num,cd.id_customer from owner_int.Vh_CIF_DOCUMENT doc
   left join owner_int.vh_cif_customer_document cd on doc.id=cd.id_document
   where doc.type='PASSPORT' and doc.status='a' 
) pas on cif_c.id=pas.id_customer
left join owner_int.vh_hom_client_snap2address csn on d.client_snapshot_id=csn.client_snapshot_id and role_type='PERMANENT'
left join owner_int.vh_hom_address adr on csn.address_id=adr.id 
left join owner_int.vh_hom_town tw on adr.town_code=tw.code 
left join owner_int.Vh_HOM_DISTRICT ds on adr.district_code=ds.code
left join owner_int.vh_hom_subdistrict sd on adr.sub_district_code=sd.code 
left join owner_int.Vh_HOM_ZIP_CODE zc on adr.zipcode_code=zc.code 
LEFT JOIN zipcode_to_dati_ii zy on zy.zip_code=adr.zipcode_value
LEFT JOIN zipcode_to_dati_ii zz on zz.zip_code=zc.zip_code
--left join v_zipcode_mapping vy on vy.zip_code=adr.zipcode_value
--left join v_zipcode_mapping vz on vz.zip_code=zc.zip_code
--left join jfs_code_translate xd on xd.codelist='DISTRICT' and adr.district_code=xd.src_code
--left join jfs_code_translate xy on xy.codelist='DISTRICT' and vy.district=xy.src_code
--left join jfs_code_translate xz on xz.codelist='DISTRICT' and vz.district=xz.src_code
left join (
          select hc1.value1,csc.client_snapshot_id from owner_int.vh_hom_contact hc1
          left join owner_int.Vh_HOM_CLIENT_SNAP2CONTACT csc on hc1.id=csc.contact_id
          where hc1.contact_type_code='HOME_PHONE'
) hc on d.client_snapshot_id=hc.client_snapshot_id 
--left join owner_int.vh_hom_employment he on d.client_snapshot_id=he.id
--left join owner_int.vh_hom_employer er on he.employer_id=er.id  
left join (
    --select cl.id_cuid cuid,upper(es.code_economical_status) economical_status_code --,ce.skp_employment_type
    --      ,ji.code_job_industry employer_industry_code--,ce.skp_job_profession--,ce.skp_trust_level,ce.dtime_valid_to
    --      ,er.name_employer name
    --from owner_dwh.dc_client cl 
    --left join OWNER_DWH.f_client_employer_tt ce on cl.skp_client=ce.skp_client
    --left join owner_dwh.dc_employer er on ce.skp_employer=er.skp_employer
    --left join owner_dwh.CL_ECONOMICAL_STATUS es on ce.skp_economical_status=es.skp_economical_status
    --left join owner_dwh.CL_JOB_INDUSTRY ji on ce.skp_job_industry=ji.skp_job_industry
    --where ce.flag_deleted='N' and ce.code_status='a' and ce.flag_current='Y'
	--OWNER INT COUNTERPART
	select cl.CUID, TRIM(UPPER(ce.ECONOMICAL_STATUS)) economical_status_code,
		TRIM(UPPER(ce.INDUSTRY)) employer_industry_code,
		er.NAME
	from OWNER_INT.VH_CIF_CUSTOMER cl
	left join OWNER_INT.VH_CIF_EMPLOYMENT ce on cl.ID = ce.ID_CUSTOMER
	left join OWNER_INT.VH_CIF_EMPLOYER er on ce.ID_EMPLOYER = er.ID
	WHERE ce.status = 'a' AND CUID IS NOT NULL
) er on er.cuid=cl.cuid
left join JFS_code_translate xe on xe.codelist='EDUCATION_TYPE' and cs.education_code=xe.src_code
left join JFS_code_translate xi on xi.codelist='INDUSTRY' and er.employer_industry_code=xi.src_code
left join JFS_code_translate xm on xm.codelist='MARITAL_STATUS' and cs.marital_status_code=xm.src_code
left join jfs_code_translate xr on xm.codelist='RELIGION' and cs.religion_code=xr.src_code
left join owner_int.vh_hom_financial_data fd on d.client_snapshot_id=fd.client_snapshot_id
where trunc(jc.export_date)=trunc(sysdate) AND JC.STATUS='S'
--where (TRUNC(jc.export_date) in ('2-NOV-2016','3-NOV-2016') AND JC.STATUS='R')
--where TRUNC(jc.export_date)='4-NOV-2016'
and ktp.type='KTP' and ktp.status='a' -- and jc.text_contract_number='3504534935'
and jc.id_agreement = 51
order by jc.id_agreement, c.contract_code