--record
select 
rpad(c.contract_code,17,' ')--NO PIN/KONTRAK
||' '||RPAD(' ',50,' ')--NAME OF MANAGER
||' '||RPAD(' ',30,' ')--ID CARD OF MANAGER
||' '||RPAD(' ',20,' ')--MANAGER'S TAX REG NO
||' '--SEX
||' '||RPAD(' ',5,' ')--OWNERSHIP PORTION
||' '||RPAD(' ',2,' ')--POSITION CODE
||' '||RPAD(' ',199,' ')--ADDRESS
||' '||RPAD(' ',50,' ')--SUB DISTRICT
||' '||RPAD(' ',50,' ')--KECAMATAN
||' '||RPAD(' ',4,' ')--DISTRICT OF DEBTOR
||' '||RPAD(' ',5,' ')--POSTAL CODE
||chr(13) as PENGURUS
from JFS_CONTRACT JC
inner join owner_int.Vh_HOM_CONTRACT c  on jc.text_contract_number=c.contract_code 
where trunc(jc.export_date)=TRUNC(SYSDATE) and jc.id_agreement=51 AND JC.STATUS='S'
--where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement < 4
order by jc.id_agreement, c.contract_code