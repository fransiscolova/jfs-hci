select 
rpad(p.text_contract_number,17,' ')
||' '||rpad(c.client_name,50,' ')
||' '||rpad(to_char(p.date_payment,'dd/MM/yyyy'),10,' ')
||' '||rpad(p.pmt_instalment,12,' ')
||' '||rpad(p.pmt_penalty,12,' ')
||' '||rpad(0,12,' ')
||' '||rpad(to_char(p.adj_payment_date,'dd/MM/yyyy'),10,' ')
from 
  (select text_contract_number,sum(pmt_instalment) pmt_instalment, sum(pmt_penalty) pmt_penalty, min(date_payment) date_payment,max(adj_payment_Date) adj_payment_date from jfs_payment_int 
    where trunc(date_export)=trunc(sysdate) and payment_type='R'
    and pmt_instalment>0
    group by text_contract_number
    union all
    select text_contract_number,sum(pmt_instalment) pmt_instalment, sum(pmt_penalty) pmt_penalty, min(date_payment) date_payment,max(adj_payment_Date) adj_payment_date from jfs_payment_int 
    where trunc(date_export)=trunc(sysdate) and payment_type='R'
    and pmt_instalment<0
    group by text_contract_number
    ) p
inner join jfs_contract c on p.text_contract_number=c.text_contract_number and c.bank_product_code='PERMATA_PRODUCT'
order by p.date_payment