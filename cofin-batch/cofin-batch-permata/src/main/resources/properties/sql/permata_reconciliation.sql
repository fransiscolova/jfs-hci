-- reconciliation records
select 
rpad(a.text_contract_number,17,' ')
||' '||rpad(jc.client_name,50,' ')
||' '||rpad(a.outstanding_principal,12,' ')
||' '||rpad(a.inst_number,3,' ')
||' '||rpad(a.DPD,3,' ')
from jfs_reconciliation a
join jfs_contract jc
on jc.text_contract_number=a.text_contract_number
and jc.bank_product_code='PERMATA_PRODUCT'
--where a.outstanding_principal>0
WHERE trunc(a.recon_date) = to_Date('1-'||to_char(sysdate,'MON-YYYY'),'DD-MON-YYYY')
order by a.text_contract_number