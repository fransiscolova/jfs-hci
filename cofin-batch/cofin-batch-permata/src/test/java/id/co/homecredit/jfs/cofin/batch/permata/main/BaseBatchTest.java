package id.co.homecredit.jfs.cofin.batch.permata.main;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * Abstract test class for batch.
 *
 * @author muhammad.muflihun
 *
 */
@ContextConfiguration(locations = { "classpath*:cofin-batch-permata-test-context.xml" })
public abstract class BaseBatchTest extends AbstractTestNGSpringContextTests {

}
