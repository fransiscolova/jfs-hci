package id.co.homecredit.jfs.cofin.common.dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.common.model.BaseEntity;

/**
 * Abstract dao interface for common purpose.
 *
 * @author muhammad.muflihun
 */
@Transactional
public abstract interface Dao<ET extends BaseEntity<ID>, ID extends Serializable> {

    /**
     * Delete entity.
     *
     * @param entity
     */
    public void delete(ET entity);

    /**
     * Delete entity list.
     *
     * @param entities
     */
    public void deleteAll(List<ET> entities);

    /**
     * Delete list entity by id.
     *
     * @param ids
     */
    public void deleteAllByIds(List<ID> ids);

    /**
     * Delete all data for entity.
     *
     */
    public void deleteAllData();

    /**
     * Delete entity by id.
     *
     * @param id
     */
    public void deleteById(ID id);

    /**
     * Get entity by id.
     *
     * @param id
     * @return entity
     */
    public ET get(ID id);

    /**
     * Get all entity by deleted status.
     *
     * @param includeDeleted
     * @return entities
     */
    public List<ET> getAll(Boolean includeDeleted);

    /**
     * Get total count for entity by deleted status.
     *
     * @param includeDeleted
     * @return count
     */
    public BigInteger getTotalCount(Boolean includeDeleted);

    /**
     * Check entity if exist by id.
     *
     * @param id
     * @return true if exist, false if not
     */
    public Boolean isExist(ID id);

    /**
     * Save or update entity.
     *
     * @param entity
     * @return entity
     */
    public ET save(ET entity);

    /**
     * Save or update entity list.
     *
     * @param entities
     * @return entities
     */
    public List<ET> save(List<ET> entities);
    
    /**
     * Update Object In Database Using Query String.
     *
     * @param query
     * @return query
     */
    public boolean updateObjectWithQuery(String query);
    
}
