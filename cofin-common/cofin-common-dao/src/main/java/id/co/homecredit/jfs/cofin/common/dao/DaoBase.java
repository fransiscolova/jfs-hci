package id.co.homecredit.jfs.cofin.common.dao;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract interface DaoBase{

    /**
     * Update Object In Database Using Query String.
     *
     * @param query
     * @return query
     */
    public boolean updateObjectWithQuery(String query);

    /**
     * Save Object
     *
     * @param object
     * @return object
     */
    public boolean saveObject(Object object);

}
