package id.co.homecredit.jfs.cofin.common.dao.impl;

import id.co.homecredit.jfs.cofin.common.dao.DaoBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoBaseImpl implements DaoBase{
    private static final Logger log = LogManager.getLogger(DaoBaseImpl.class);

    @Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sfJFS = sessionFactory;
    }

    /**
     * Update Object In Database Using Query String.
     *
     * @param script
     * @return query
     */
    public boolean updateObjectWithQuery(String script){
        Session session = sfJFS.openSession();
        try{
            session.beginTransaction();
            Query query = session.createSQLQuery(script);
            query.executeUpdate();
            session.getTransaction().commit();
            log.debug(query.getQueryString());
        }catch(HibernateException d){
            log.debug(d);
            session.getTransaction().rollback();
            return false;
        }finally{
            session.close();
        }
        return true;
    }

    /**
     * Save Object
     *
     * @param object
     * @return object
     */
    public boolean saveObject(Object object){
        Session session = sfJFS.openSession();
        try{
            session.beginTransaction();
            session.save(object);
            session.getTransaction().commit();
        }catch(HibernateException f){
            session.getTransaction().rollback();
            f.printStackTrace();
            return false;
        }finally{
            session.close();
        }
        return true;
    }

}
