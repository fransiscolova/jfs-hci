package id.co.homecredit.jfs.cofin.common.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;

/**
 * Dao implement for common purpose.
 *
 * @author muhammad.muflihun
 */
@SuppressWarnings("unchecked")
public class DaoIdOnlyImpl<ET extends BaseEntityIdOnly<ID>, ID extends Serializable> extends DaoBaseImpl implements DaoIdOnly<ET, ID> {
	private static final Logger log = LogManager.getLogger(DaoIdOnlyImpl.class);
	
    private Class<ET> entityClass;
    @Autowired
    private SessionFactory sessionFactory;
    
    /**
     * Constructor to define the entity class.
     */
    public DaoIdOnlyImpl() {
        Type[] actualTypeArg = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments();
        entityClass = (Class<ET>) actualTypeArg[0];
    }
    
    /**
     * Compose entity criteria with alias and field {@code DELETED} restriction.
     *
     * @param alias
     * @param includeDeleted
     * @return criteria
     */
    private Criteria composeCriteria(String alias, Boolean includeDeleted) {
        Criteria criteria;
        if (alias == null || alias.isEmpty()) {
            criteria = getSession().createCriteria(entityClass);
        } else {
            criteria = getSession().createCriteria(entityClass, alias);
        }
        if (!includeDeleted) {
        	
//        	try {
//            criteria.add(Restrictions.eq(ColumnName.DELETED, YesNoEnum.N));
//        	}catch(Exception e){
//        		log.error(e.getMessage());
//        	}
//        	
        	}
        return criteria;
    }
    
    /**
     * Create criteria.
     *
     * @return criteria
     */
    protected Criteria createCriteria() {
        return composeCriteria(null, false);
    }

    /**
     * Create criteria with alias.
     *
     * @param alias
     * @return criteria
     */
    protected Criteria createCriteria(String alias) {
        return composeCriteria(alias, false);
    }
    
    /**
     * Create criteria with includeDeleted.
     *
     * @param includeDeleted
     * @return criteria
     */
    protected Criteria createCriteria(Boolean includeDeleted) {
        return composeCriteria(null, includeDeleted);
    }

    /**
     * Create common SQL/native query.
     *
     * @param queryString
     * @return query
     */
    protected Query createNativeQuery(String queryString) {
        return getSession().createSQLQuery(queryString);
    }

    /**
     * Create HQL query.
     *
     * @param queryString
     * @return query
     */
    protected Query createQuery(String queryString) {
        return getSession().createQuery(queryString);
    }

    @Override
    public void delete(ET entity) {
        
            getSession().delete(entity);

    }

    @Override
    public void deleteAll(List<ET> entities) {
        for (ET entity : entities) {
            delete(entity);
        }
    }

    @Override
    public void deleteAllByIds(List<ID> ids) {
        for (ID id : ids) {
            deleteById(id);
        }
    }

    @Override
    public void deleteAllData() {
        deleteAll(getAll(false));
    }

    @Override
    public void deleteById(ID id) {
        delete((ET) getSession().load(entityClass, id));
    }

    @Override
    public ET get(ID id) {
        return (ET) getSession().get(entityClass, id);
    }

    @Override
    public List<ET> getAll(Boolean includeDeleted) {
        return composeCriteria(null, includeDeleted).list();
    }

    /**
     * Get current session.
     *
     * @return session
     */
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public BigInteger getTotalCount(Boolean includeDeleted) {
        Criteria criteria;
        if (includeDeleted) {
            criteria = getSession().createCriteria(entityClass);
        } else {
            criteria = createCriteria();
        }
        Long totalCount = (Long) criteria
                .setProjection(Projections
                        .count(entityClass.getSuperclass().getDeclaredFields()[0].getName()))
                .uniqueResult();
        return BigInteger.valueOf(totalCount);
    }

    @Override
    public Boolean isExist(ID id) {
        return get(id) != null;
    }

    @Override
    public ET save(ET entity) {	
    	getSession().saveOrUpdate(entity);
        return entity;
    }

    @Override
    public List<ET> save(List<ET> entities) {
        for (ET entity : entities) {
            save(entity);
        }
        return entities;
    }
    
/////////////////////////////////////////////Add By Denny Afrizal////////////////////////////////////////////
    
    @Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }
    
    /**
     * Create criteria.
     *
     * @return criteria
     */
    protected Criteria createNewCriteria() {
        return composeCriteria();
    }
    
    /**
     * Compose entity criteria restriction.
     *
     * @return criteria
     */
    private Criteria composeCriteria() {
        Criteria criteria = getSession().createCriteria(entityClass);
        return criteria;
    }
    
    @Override
    public List<ET> getAll() {
        return composeCriteria().list();
    }
    
    /**
     * Execute SQL Query.
     *
     * @param script
     * @return query
     */
    public List<Map<String,Object>> executeSelectQuery(String script){
    	Session session = sfJFS.openSession();
    	Query query = session.createSQLQuery(script);
    	log.debug(query.getQueryString());
    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
    	List<Map<String,Object>> hasil = query.list();
    	return hasil;
    }
    
    /**
     * Update Object In Database.
     *
     * @param object
     * @return object
     */
    public Object updateObject(Object object){
    	Session session = sfJFS.openSession();
    	try{
    		session.beginTransaction();
    		session.update(object);
    		session.getTransaction().commit();
    	}catch(HibernateException t){
    		session.getTransaction().rollback();
    		t.printStackTrace();
    		return false;
    	}finally{
    		session.close();
    	}
    	return true;
    }
    
/////////////////////////////////////////////Add By Denny Afrizal////////////////////////////////////////////

}
