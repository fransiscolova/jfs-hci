package id.co.homecredit.jfs.cofin.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;

/**
 * This abstract class extend {@link CreateUpdate}. Additionally cater information about
 * {@code VERSION} and {@code DELETED}.
 *
 * @author muhammad.muflihun
 *
 */
@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> extends CreateUpdate {
    protected YesNoEnum deleted = YesNoEnum.N;
    protected Long version;

    /**
     * Value in enum string as 'Y' or 'N'.
     *
     * @return string
     */
    @Enumerated(EnumType.STRING)
    @Column(name = ColumnName.DELETED, nullable = false, length = 1)
    public YesNoEnum getDeleted() {
        return deleted;
    }

    /**
     * Get version.
     * <p>
     * By default version will be started from 0 when not set manually by the system and then auto
     * increments each time the entity updated by system.
     *
     * @return version
     */
    @Version
    @Column(name = ColumnName.VERSION, nullable = false)
    public Long getVersion() {
        return version;
    }

    /**
     * Set deleted in enum string as 'Y' or 'N'.
     *
     * @param deleted
     */
    public void setDeleted(YesNoEnum deleted) {
        this.deleted = deleted;
    }

    /**
     * Set version.
     * <p>
     * By default version will be started from 0 when not set manually by the system and then auto
     * increments each time the entity updated by system.
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

}
