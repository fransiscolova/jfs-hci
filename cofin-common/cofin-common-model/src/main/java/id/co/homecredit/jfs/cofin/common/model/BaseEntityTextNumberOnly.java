package id.co.homecredit.jfs.cofin.common.model;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import org.hibernate.annotations.GenericGenerator;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.Entity;

/**
 * This abstract class extend {@link CreateUpdate}. Additionally cater information about
 * {@code VERSION} and {@code DELETED}.
 *
 * @author muhammad.muflihun
 *
 */
@MappedSuperclass
public abstract class BaseEntityTextNumberOnly<ID extends Serializable>   {
	
	protected String textContractNumber;
	  

    /**
     * Get creator textContractNumber.
     *
     * @return textContractNumber
     */
    @Column(name = ColumnName.TEXT_CONTRACT_NUMBER, nullable = false, updatable = false)
    public String getTextContractNumber() {
        return textContractNumber;
    }

    /**
     * Set creator name.
     *
     * @param textContractNumber
     */
    public void setTextContractNumber(String textContractNumber) {
        this.textContractNumber = textContractNumber;
    }
}
