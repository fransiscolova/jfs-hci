package id.co.homecredit.jfs.cofin.common.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;

/**
 * Abstract class for cater information about {@code CREATED BY} and {@code CREATED DATE}.
 *
 * @author muhammad.muflihun
 *
 */
@MappedSuperclass
public abstract class Create {
    protected String createdBy;
    protected Date createdDate = new Date();

    /**
     * Get creator name.
     *
     * @return name
     */
    @Column(name = ColumnName.CREATED_BY, nullable = false, updatable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Get created date using format timestamp.
     *
     * @return date
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = ColumnName.CREATED_DATE, nullable = false, updatable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Set creator name.
     *
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Set created date. By default is using new {@link Date}.
     *
     * @param createdDate
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
