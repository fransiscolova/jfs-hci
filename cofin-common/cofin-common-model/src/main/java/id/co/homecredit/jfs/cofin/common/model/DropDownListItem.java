package id.co.homecredit.jfs.cofin.common.model;

/**
 * Model For Accommodate Value In Combo Box
 *
 * @author denny.afrizal01
 *
 */
public class DropDownListItem {
	private String text;
	private String value;
	public String getText() {
		return text;
	}
	public DropDownListItem(String text,String value){
		setValue(value);
		setText(text);
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
