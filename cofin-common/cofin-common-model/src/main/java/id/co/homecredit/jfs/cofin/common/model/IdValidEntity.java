package id.co.homecredit.jfs.cofin.common.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;

/**
 * This abstract class extend {@link IdEntity}. Additionally cater information about
 * {@code VALID FROM} and {@code VALID TO}.
 *
 * @author muhammad.muflihun
 *
 */
@MappedSuperclass
public abstract class IdValidEntity extends IdEntity {
    protected Date validFrom;
    protected Date validTo;

    /**
     * Get valid from.
     *
     * @return date
     */
    @Temporal(TemporalType.DATE)
    @Column(name = ColumnName.VALID_FROM)
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * Get valid to.
     *
     * @return date
     */
    @Temporal(TemporalType.DATE)
    @Column(name = ColumnName.VALID_TO)
    public Date getValidTo() {
        return validTo;
    }

    /**
     * Set valid from.
     *
     * @param validFrom
     */
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Set valid to.
     *
     * @param validTo
     */
    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

}
