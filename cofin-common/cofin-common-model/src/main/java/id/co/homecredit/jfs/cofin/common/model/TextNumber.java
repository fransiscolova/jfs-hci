package id.co.homecredit.jfs.cofin.common.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;

/**
 * Abstract class for cater information about {@code CREATED BY} and {@code CREATED DATE}.
 *
 * @author muhammad.muflihun
 *
 */
@MappedSuperclass
public abstract class TextNumber {
    protected String textContractNumber;
  

    /**
     * Get creator textContractNumber.
     *
     * @return textContractNumber
     */
    @Id
    @Column(name = ColumnName.TEXT_CONTRACT_NUMBER)
    public String getTextContractNumber() {
        return textContractNumber;
    }

    /**
     * Set creator name.
     *
     * @param textContractNumber
     */
    public void setTextContractNumber(String textContractNumber) {
        this.textContractNumber = textContractNumber;
    }


}
