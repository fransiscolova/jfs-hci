package id.co.homecredit.jfs.cofin.common.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;

/**
 * Abstract class for cater information about {@code UPDATED BY} and {@code UPDATED DATE}.
 *
 * @author muhammad.muflihun
 *
 */
@MappedSuperclass
public abstract class Update {
    protected String updatedBy;
    protected Date updatedDate;

    /**
     * Get updater name.
     *
     * @return name
     */
    @Column(name = ColumnName.UPDATED_BY)
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Get updated date using format timestamp.
     *
     * @return date
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = ColumnName.UPDATED_DATE)
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Set updater name.
     *
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * Set updated date.
     *
     * @param updatedDate
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}
