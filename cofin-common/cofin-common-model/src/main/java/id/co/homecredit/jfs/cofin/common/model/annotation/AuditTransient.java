package id.co.homecredit.jfs.cofin.common.model.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Custom annotation interface to be used in entity or specific field that will be excluded when
 * audited.
 *
 * @author muhammad.muflihun
 *
 */
@Target({ FIELD, TYPE })
@Retention(RUNTIME)
public @interface AuditTransient {

}
