package id.co.homecredit.jfs.cofin.common.model.dto;

import id.co.homecredit.jfs.cofin.common.model.BaseEntity;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;

/**
 * Abstract dto for abstract entity {@link BaseEntity}.
 *
 * @author muhammad.muflihun
 *
 */
public abstract class BaseEntityDto extends CreateUpdateDto {
    protected Long version;
    protected YesNoEnum deleted;

    /**
     * Value in enum string as 'Y' or 'N'.
     *
     * @return string
     */
    public YesNoEnum getDeleted() {
        return deleted;
    }

    /**
     * Value in boolean.
     *
     * @return boolean
     */
    public Boolean getIsDeleted() {
        return deleted.equals(YesNoEnum.Y);
    }

    /**
     * Get version.
     *
     * @return version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Set deleted in enum string as 'Y' or 'N'.
     *
     * @param deleted
     */
    public void setDeleted(YesNoEnum deleted) {
        this.deleted = deleted;
    }

    /**
     * Set deleted in boolean, true means 'Y' or false means 'N'.
     *
     * @param deleted
     */
    public void setIsDeleted(Boolean deleted) {
        this.deleted = deleted ? YesNoEnum.Y : YesNoEnum.N;
    }

    /**
     * Set version.
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

}
