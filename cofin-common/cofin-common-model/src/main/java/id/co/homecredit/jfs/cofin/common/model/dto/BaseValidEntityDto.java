package id.co.homecredit.jfs.cofin.common.model.dto;

import java.util.Date;

import id.co.homecredit.jfs.cofin.common.model.BaseValidEntity;

/**
 * Abstract dto for abstract entity {@link BaseValidEntity}.
 *
 * @author muhammad.muflihun
 *
 */
public abstract class BaseValidEntityDto extends BaseEntityDto {
    protected Date validFrom;
    protected Date validTo;

    /**
     * Get valid from.
     *
     * @return date
     */
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * Get valid to.
     *
     * @return date
     */
    public Date getValidTo() {
        return validTo;
    }

    /**
     * Set valid from.
     *
     * @param validFrom
     */
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Set valid to.
     *
     * @param validTo
     */
    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

}
