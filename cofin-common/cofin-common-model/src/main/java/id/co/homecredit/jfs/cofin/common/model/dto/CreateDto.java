package id.co.homecredit.jfs.cofin.common.model.dto;

import java.util.Date;

import id.co.homecredit.jfs.cofin.common.model.Create;

/**
 * Abstract dto for abstract entity {@link Create}.
 *
 * @author muhammad.muflihun
 *
 */
public abstract class CreateDto {
    protected String createdBy;
    protected Date createdDate;

    /**
     * Get creator name.
     *
     * @return name
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Get created date using format timestamp.
     *
     * @return date
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Set creator name.
     *
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Set created date.
     *
     * @param createdDate
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
