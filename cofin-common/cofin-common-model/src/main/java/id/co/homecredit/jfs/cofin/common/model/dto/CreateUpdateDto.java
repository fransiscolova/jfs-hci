package id.co.homecredit.jfs.cofin.common.model.dto;

import java.util.Date;

import id.co.homecredit.jfs.cofin.common.model.CreateUpdate;

/**
 * Abstract dto for abstract entity {@link CreateUpdate}.
 *
 * @author muhammad.muflihun
 *
 */
public abstract class CreateUpdateDto extends CreateDto {
    protected String updatedBy;
    protected Date updatedDate;

    /**
     * Get updater name.
     *
     * @return name
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Get updated date using format timestamp.
     *
     * @return date
     */
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Set updater name.
     *
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * Set updated date.
     *
     * @param updatedDate
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}
