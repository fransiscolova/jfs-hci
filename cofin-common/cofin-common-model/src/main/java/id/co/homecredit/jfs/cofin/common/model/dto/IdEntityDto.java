package id.co.homecredit.jfs.cofin.common.model.dto;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;

/**
 * Abstract dto for abstract entity {@link IdEntity}.
 *
 * @author muhammad.muflihun
 *
 */
public abstract class IdEntityDto extends BaseEntityDto {
    protected String id;

    /**
     * Get id.
     *
     * @return string
     */
    public String getId() {
        return id;
    }

    /**
     * Set id.
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

}
