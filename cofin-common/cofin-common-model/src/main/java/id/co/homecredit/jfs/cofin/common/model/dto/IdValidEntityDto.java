package id.co.homecredit.jfs.cofin.common.model.dto;

import java.util.Date;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;

/**
 * Abstract dto for abstract entity {@link IdEntity}.
 *
 * @author muhammad.muflihun
 *
 */
public abstract class IdValidEntityDto extends IdEntityDto {
    protected Date validFrom;
    protected Date validTo;

    /**
     * Get valid from.
     *
     * @return date
     */
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * Get valid to.
     *
     * @return date
     */
    public Date getValidTo() {
        return validTo;
    }

    /**
     * Set valid from.
     *
     * @param validFrom
     */
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Set valid to.
     *
     * @param validTo
     */
    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

}
