package id.co.homecredit.jfs.cofin.common.model.enumeration;

/**
 * Enumeration for {@code CONTRACT_STATUS}.
 *
 * @author muhammad.muflihun
 *
 */
public enum ContractStatusEnum {
    S("Approved"), 
    A("Active"), 
    T("Cancelled"), 
    K("Finished"), 
    Z("Returned"), 
    P("In pre-process"), 
    R("In process"), 
    L("Paid off"), 
    N("Signed"), 
    H("Written-off"), 
    O("Rejected"), 
    Q("Sold"), 
    W("Select Status");
    
    private String description;
    
    ContractStatusEnum(String description) {
        this.description = description;
    }

    /**
     * Get contract status description.
     * 
     * @return description
     */
    public String getDescription() {
        return description;
    }
}
