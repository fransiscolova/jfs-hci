package id.co.homecredit.jfs.cofin.common.model.enumeration;

/**
 * Enumeration for column {@code LOGIN ACTION}.
 * 
 * @author muhammad.muflihun
 *
 */
public enum LoginEnum {
    LOGIN("User login"),
    LOGOUT("User logout");
    
    private String description;

    LoginEnum(String description) {
        this.description = description;
    }

    /**
     * Get login description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
