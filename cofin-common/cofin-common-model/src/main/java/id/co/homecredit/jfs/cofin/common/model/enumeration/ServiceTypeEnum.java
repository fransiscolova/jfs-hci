package id.co.homecredit.jfs.cofin.common.model.enumeration;

/**
 * Enumeration for {@link SERVICE_TYPE}.
 *
 * @author muhammad.muflihun
 *
 */
public enum ServiceTypeEnum {
    FER("Full early repayment"),
    RELIP("Installment plan for REL transaction"),
    PAYHOL("Payment holidays"),
    LOYALTY("Loyalty"),
    CRDPST("Standard payment card"),
    GRREL("Grace period for REL"),
    SMSN("SMS notification"),
    INSLI("Life additional protection"),
    INSGO("Goods service"),
    INSGN("General additional protection"),
    GRPER("Grace Period"),
    GIFTP("Gift payment"),
    PREFDD("Preferred due date"),
    CRDBAS("CRDBAS"),
    RELER("RELER"),
    COP("Cooling-off period"),
    FBACK("Fees back"),
    CHECKTL("Checking terms of loan"),
    LRES("Loan restructuring"),
    CONS("Consolidation"),
    CHDD("Change of due date"),
    CHDDR("Change of due date with request");

    private String description;

    ServiceTypeEnum(String description) {
        this.description = description;
    }

    /**
     * Get service type description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }
}
