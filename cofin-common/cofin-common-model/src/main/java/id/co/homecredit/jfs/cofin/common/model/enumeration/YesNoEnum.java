package id.co.homecredit.jfs.cofin.common.model.enumeration;

/**
 * Enumeration for column {@code IS DELETED}.
 *
 * @author muhammad.muflihun
 *
 */
public enum YesNoEnum {
    Y("Yes"), 
    N("No");

    private String description;

    YesNoEnum(String description) {
        this.description = description;
    }

    /**
     * Get deleted description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
