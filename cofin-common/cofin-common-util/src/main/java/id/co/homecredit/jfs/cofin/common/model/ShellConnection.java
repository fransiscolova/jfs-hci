package id.co.homecredit.jfs.cofin.common.model;

/**
 * Model for shell connection.
 *
 * @author muhammad.muflihun
 *
 */
public class ShellConnection {
    private String username;
    private String password;
    private String ip;
    private String host;
    private Integer port;
    private Integer timeout;

    public ShellConnection() {
    }

    public ShellConnection(String username, String password, String ip, String host, Integer port) {
        this.username = username;
        this.password = password;
        this.ip = ip;
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public String getIp() {
        return ip;
    }

    public String getPassword() {
        return password;
    }

    public int getPort() {
        return port;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public String getUsername() {
        return username;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
