package id.co.homecredit.jfs.cofin.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.Calendar;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.DateFormat;

/**
 * Helper class for customizing date.
 *
 * @author muhammad.muflihun
 *
 */
public class DateUtil {

    /**
     * Return tomorrow date after given date.
     *
     * @param date
     * @return date
     */
    public static Date datePlusOneDay(Date date) {
        return new Date(date.getTime() + TimeUnit.DAYS.toMillis(1));
    }

    /**
     * Format {@code date} to string using 'yyyy-MM-dd', then convert it again to date without hours
     * and minutes value.
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static Date dateToDate(Date date) throws ParseException {
        return stringToDate(dateToString(date));
    }

    /**
     * Format {@code date} to string using 'yyyy-MM-dd'.
     *
     * @param date
     * @return dateString
     */
    public static String dateToString(Date date) {
        return dateToStringWithFormat(date, DateFormat.FORMAT_1);
    }

    /**
     * Format {@code date} to string using given {@code dateFormat}.
     *
     * @param date
     * @param dateFormat
     * @return dateString
     */
    public static String dateToStringWithFormat(Date date, String dateFormat) {
        return getFormat(dateFormat).format(date);
    }

    /**
     * Return simple date format using {@code dateFormat}.
     *
     * @param dateFormat
     * @return simpleDateFormat
     */
    private static SimpleDateFormat getFormat(String dateFormat) {
        return new SimpleDateFormat(dateFormat);
    }

    /**
     * Format {@code dateString} to date using given format 'yyyy-MM-dd'.
     * <p>
     * Throw parse exception when fail to convert to date.
     *
     * @param dateString
     * @return date
     * @throws ParseException
     */
    public static Date stringToDate(String dateString) throws ParseException {
        return stringToDateWithFormat(dateString, DateFormat.FORMAT_1);
    }

    /**
     * Format {@code dateString} to date using given {@code dateFormat}.
     * <p>
     * Throw parse exception when fail to convert to date.
     *
     * @param dateString
     * @param dateFormat
     * @return date
     * @throws ParseException
     */
    public static Date stringToDateWithFormat(String dateString, String dateFormat)
            throws ParseException {
        return getFormat(dateFormat).parse(dateString);
    }

    /**
     * Format {@code dateString} to date using given format 'yyyy-MM-dd HH:mm:ss.SSS'.
     * <p>
     * Throw parse exception when fail to convert to date.
     *
     * @param dateString
     * @return date
     * @throws ParseException
     */
    public static Date stringToTimestamp(String dateString) throws ParseException {
        return stringToDateWithFormat(dateString, DateFormat.TIMESTAMP);
    }

    /**
     * Get Start Date
     *
     * @param date
     * @return
     */
    public static Date getStartDate(Date date){
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        cld.set(Calendar.HOUR_OF_DAY,0);
        cld.set(Calendar.MINUTE,0);
        cld.set(Calendar.SECOND,0);
        return cld.getTime();
    }

    /**
     * Get End Date
     *
     * @param date
     * @return
     */
    public static Date getEndDate(Date date){
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        cld.set(Calendar.HOUR_OF_DAY,23);
        cld.set(Calendar.MINUTE,59);
        cld.set(Calendar.SECOND,59);
        return cld.getTime();
    }
}
