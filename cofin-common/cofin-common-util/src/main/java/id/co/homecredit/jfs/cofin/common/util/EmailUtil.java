package id.co.homecredit.jfs.cofin.common.util;

import java.io.File;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.StringChar;

/**
 * Email composer util.
 *
 * @author muhammad.muflihun
 *
 */
public class EmailUtil {
    private static final Logger log = LogManager.getLogger(EmailUtil.class);
    private String username;
    private String password;
    private String hostname;
    private Integer port;
    private String fromEmail;
    private String fromName;
    private String bounceEmail;
    private String textMessage;

    public EmailUtil(String hostname, Integer port, String username, String password,
            String fromEmail, String fromName, String bounceEmail, String textMessage) {
        this.hostname = hostname;
        this.port = port;
        this.username = username;
        this.password = password;
        this.fromEmail = fromEmail;
        this.fromName = fromName;
        this.bounceEmail = bounceEmail;
        this.textMessage = textMessage;
    }

    /**
     * Attach file to email.
     *
     * @param email
     * @param pathFile
     * @throws EmailException
     */
    private void attachFile(MultiPartEmail email, String pathFile) throws EmailException {
        if (pathFile != null && !pathFile.isEmpty()) {
            File file = new File(pathFile);
            if (file != null && file.exists()) {
                email.attach(file);
            }
        }
    }

    /**
     * Attach multiple file to email.
     *
     * @param email
     * @param pathFiles
     * @throws EmailException
     */
    private void attachMultipleFile(MultiPartEmail email, String... pathFiles)
            throws EmailException {
        for (String pathFile : pathFiles) {
            attachFile(email, pathFile);
        }
    }

    /**
     * Send html email with attached files.
     *
     * @param subject
     * @param message
     * @param to
     * @param cc
     * @param bcc
     * @param pathFiles
     * @throws EmailException
     */
    public void sendHtmlEmail(String subject, String message, String to, String cc, String bcc,
            String... pathFiles) throws EmailException {
        HtmlEmail email = new HtmlEmail();
        setDefaultEmailCredential(email);
        setToReceiver(email, to);
        setCcReceiver(email, cc);
        setBccReceiver(email, bcc);
        setDefaultHtmlContent(email, subject, message);
        attachMultipleFile(email, pathFiles);
        email.send();
    }

    /**
     * Send html email with attached file.
     *
     * @param subject
     * @param message
     * @param to
     * @param cc
     * @param bcc
     * @param pathFile
     * @throws EmailException
     */
    public void sendHtmlEmail(String subject, String message, String to, String cc, String bcc,
            String pathFile) throws EmailException {
        HtmlEmail email = new HtmlEmail();
        setDefaultEmailCredential(email);
        setToReceiver(email, to);
        setCcReceiver(email, cc);
        setBccReceiver(email, bcc);
        setDefaultHtmlContent(email, subject, message);
        attachFile(email, pathFile);
        email.send();
    }

    /**
     * Send simple email without attachment.
     *
     * @param subject
     * @param message
     * @param to
     * @param cc
     * @param bcc
     * @throws EmailException
     */
    public void sendSimpleEmail(String subject, String message, String to, String cc, String bcc)
            throws EmailException {
        Email email = new SimpleEmail();
        setDefaultEmailCredential(email);
        setToReceiver(email, to);
        setCcReceiver(email, cc);
        setBccReceiver(email, bcc);
        setContent(email, subject, message);
        email.send();
    }

    /**
     * Set bcc to email.
     *
     * @param email
     * @param bcc
     * @throws EmailException
     */
    private void setBccReceiver(Email email, String bcc) throws EmailException {
        if (bcc != null && !bcc.isEmpty()) {
            if (!bcc.contains(StringChar.SEMICOLON)) {
                email.addBcc(bcc);
            } else {
                String[] recievers = StringUtil.splitterSemicolon(bcc);
                email.addBcc(recievers);
            }
        }
    }

    /**
     * Set cc to email.
     *
     * @param email
     * @param cc
     * @throws EmailException
     */
    private void setCcReceiver(Email email, String cc) throws EmailException {
        if (cc != null && !cc.isEmpty()) {
            if (!cc.contains(StringChar.SEMICOLON)) {
                email.addCc(cc.toLowerCase());
            } else {
                String[] recievers = StringUtil.splitterSemicolon(cc);
                email.addCc(recievers);
            }
        }
    }

    /**
     * Compose email content.
     *
     * @param email
     * @param subject
     * @param message
     * @throws EmailException
     */
    private void setContent(Email email, String subject, String message) throws EmailException {
        email.setSubject(subject);
        email.setMsg(message);
    }

    /**
     * Set default email credential from properties.
     *
     * @param mail
     * @throws EmailException
     */
    private void setDefaultEmailCredential(Email mail) throws EmailException {
        setEmailCredential(mail, hostname, port, username, password, fromEmail, fromName,
                bounceEmail);
    }

    /**
     * Compose email html content.
     *
     * @param email
     * @param subject
     * @param message
     * @throws EmailException
     */
    private void setDefaultHtmlContent(HtmlEmail email, String subject, String message)
            throws EmailException {
        setHtmlContent(email, subject, message, textMessage);
    }

    /**
     * Set default email credential.
     *
     * @param mail
     * @param hostName
     * @param port
     * @param username
     * @param password
     * @param fromEmail
     * @param fromName
     * @param bounceEmail
     * @throws EmailException
     */
    private void setEmailCredential(Email mail, String hostName, Integer port, String username,
            String password, String fromEmail, String fromName, String bounceEmail)
            throws EmailException {
        mail.setHostName(hostName);
        mail.setSmtpPort(port);
        mail.setAuthenticator(new DefaultAuthenticator(username, password));
        mail.setFrom(fromEmail, fromName);
        mail.setBounceAddress(bounceEmail);
    }

    /**
     * Compose email html content.
     *
     * @param email
     * @param subject
     * @param message
     * @param textMsg
     * @throws EmailException
     */
    private void setHtmlContent(HtmlEmail email, String subject, String message, String textMsg)
            throws EmailException {
        setContent(email, subject, message);
        email.setTextMsg(textMsg);
    }

    /**
     * Set to to email.
     *
     * @param email
     * @param to
     * @throws EmailException
     */
    private void setToReceiver(Email email, String to) throws EmailException {
        if (to != null && !to.isEmpty()) {
            if (!to.contains(StringChar.SEMICOLON)) {
                email.addTo(to);
            } else {
                String[] recievers = StringUtil.splitterSemicolon(to);
                email.addTo(recievers);
            }
        }
    }
}
