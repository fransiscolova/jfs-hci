package id.co.homecredit.jfs.cofin.common.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;

import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.Html;

/**
 * File composer util.
 *
 * @author muhammad.muflihun
 *
 */
public class FileUtil {

    /**
     * Count number of line from file.
     *
     * @param filename
     * @return integer
     * @throws IOException
     */
    public static Integer countLines(String filename) throws IOException {
        InputStream is = null;
        try {
            is = new BufferedInputStream(new FileInputStream(filename));
            byte[] c = new byte[1024];
            Integer count = 0;
            Integer readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } finally {
            if (is == null) {
                return null;
            }
            is.close();
        }
    }

    /**
     * Create new file or append data if file is already exist. Return true if file created
     * successfully.
     *
     * @param fileName
     * @param datas
     * @return
     * @throws IOException
     */
    public static Boolean createFile(String fileName, List<String> datas) throws IOException {
        File fileToWrite = new File(fileName);
        for (String data : datas) {
            FileUtils.writeStringToFile(fileToWrite, data.concat(Html.ENTER), true);
        }
        return Boolean.TRUE;
    }

    /**
     * Create file with checking if already exist then move it to another directory. Return true if
     * file created backup file successfully.
     *
     * @param filePath
     * @param datas
     * @param backupPath
     * @return boolean
     * @throws IOException
     */
    public static Boolean createFileWithCheck(String filePath, List<String> datas,
            String backupPath) throws IOException {
        Boolean moved = moveFileExisting(filePath, backupPath);
        File file = new File(filePath);
        for (String data : datas) {
            FileUtils.writeStringToFile(file, data.concat(Html.ENTER), true);
        }
        return moved;
    }

    /**
     * Load file content from given file path into single string.
     *
     * @param filePath
     * @return string
     * @throws FileNotFoundException
     */
    @SuppressWarnings("resource")
    public static String loadFileContent(String filePath) throws FileNotFoundException {
        return new Scanner(new File(filePath)).useDelimiter("\\Z").next();
    }

    /**
     * Load file content from given file path into multiple lines.
     *
     * @param filePath
     * @return string
     * @throws FileNotFoundException
     */
    @SuppressWarnings("resource")
    public static List<String> loadFileContents(String filePath) throws FileNotFoundException {
        List<String> lines = new ArrayList<>();
        Scanner scanner = new Scanner(new File(filePath)).useDelimiter("\\Z");
        while (scanner.hasNext()) {
            lines.add(scanner.next());
        }
        return lines;
    }

    /**
     * Check if file already exist then move it to backup path.
     *
     * @param filePath
     * @param backupPath
     * @return boolean
     * @throws IOException
     */
    public static Boolean moveFileExisting(String filePath, String backupPath) throws IOException {
        File file = new File(filePath);
        if (file.exists() && !file.isDirectory()) {
            FileUtils.moveFile(file, new File(backupPath));
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
