package id.co.homecredit.jfs.cofin.common.util;

import java.io.Serializable;
import java.util.UUID;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * Custom id generator using {@link UUID}.
 *
 * @author muhammad.muflihun
 *
 */
public class IdGenerator implements IdentifierGenerator {

    /**
     * Return 32 alphanumeric char from random {@link UUID} in uppercase without any separator.
     */
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) {
        return StringUtil.uuidGeneratorShort();
    }

}
