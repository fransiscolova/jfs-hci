package id.co.homecredit.jfs.cofin.common.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.DateFormat;
import net.sf.ezmorph.object.DateMorpher;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;
import net.sf.json.util.JSONUtils;
import net.sf.json.util.PropertyFilter;

/**
 * Handling all kind of JSON process like convert from JSON to java object or vice versa using net
 * sf json.
 *
 * @author muhammad.muflihun
 *
 */
public class JSONUtil {

    /**
     * Custom value processor for class {@link Date}, {@link BigDecimal}, and {@link BigInteger}.
     */
    private static JsonValueProcessor jsonValueProcessor = new JsonValueProcessor() {

        @SuppressWarnings("static-access")
        private Object process(Object bean, JsonConfig jsonConfig) {
            JSONObject jsonObject = null;
            if (bean != null) {
                if (bean instanceof java.sql.Date) {
                    bean = new Date(((java.sql.Date) bean).getTime());
                }
                if (bean instanceof Date) {
                    // jsonObject = JSONObject.fromObject(bean);
                    return new SimpleDateFormat(DateFormat.TIMESTAMP).format(bean);
                } else if (bean instanceof BigInteger) {
                    return ((BigInteger) bean).toString();
                } else if (bean instanceof BigDecimal) {
                    return ((BigDecimal) bean).toString();
                } else {
                    jsonObject = new JSONObject(true);
                }
                return jsonObject;
            }
            return jsonObject.fromObject(bean, jsonConfig);
        }

        @Override
        public Object processArrayValue(Object value, JsonConfig jsonConfig) {
            return process(value, jsonConfig);
        }

        @Override
        public Object processObjectValue(String key, Object value, JsonConfig jsonConfig) {
            return process(value, jsonConfig);
        }
    };

    /**
     * Custom property filter when convert entity to JSON will exclude field with
     * {@code AuditTransient} annotation.
     */
    private static PropertyFilter jsonPropertyFilter = new PropertyFilter() {
        @Override
        public boolean apply(Object source, String name, Object value) {
            try {
                Field field = source.getClass().getDeclaredField(name);
                Annotation[] annotations = field.getAnnotations();
                for (Annotation annotation : annotations) {
                    if (annotation.toString().contains("AuditTransient")) {
                        return true;
                    }
                }
            } catch (Exception e) {
                // System.out.println("json exception : " + e.getStackTrace()[0]);
            }
            return false;
        }
    };

    /**
     * Convert object to json.
     *
     * @param object
     * @return json
     */
    public static JSON convertToJSON(Object object) {
        return JSONSerializer.toJSON(object, getJsonConfig());
    }

    /**
     * Convert object to json array.
     *
     * @param object
     * @return jsonArray
     */
    public static JSONArray convertToJSONArray(Object object) {
        return (JSONArray) convertToJSON(object);
    }

    /**
     * Get custom json config.
     *
     * @return
     */
    private static JsonConfig getJsonConfig() {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class, jsonValueProcessor);
        // jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        jsonConfig.setJsonPropertyFilter(jsonPropertyFilter);
        return jsonConfig;
    }

    /**
     * Get object from json.
     *
     * @param json
     * @param clazz
     * @return clazz
     */
    @SuppressWarnings("unchecked")
    public static <T> T getObjectFromJSON(JSON json, Class<T> clazz) {
        JsonConfig jsonConfig = getJsonConfig();
        jsonConfig.setRootClass(clazz);
        JSONUtils.getMorpherRegistry()
                .registerMorpher(new DateMorpher(new String[] { DateFormat.TIMESTAMP }));

        JSONObject jsonObject = JSONObject.fromObject(json, jsonConfig);
        return (T) JSONObject.toBean(jsonObject, clazz, new HashMap<>());
    }

    /**
     * Get object from json array.
     *
     * @param array
     * @param clazz
     * @return clazz
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getObjectFromJSONArray(JSONArray array, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        JsonConfig jsonConfig = getJsonConfig();
        jsonConfig.setRootClass(clazz);
        JSONUtils.getMorpherRegistry()
                .registerMorpher(new DateMorpher(new String[] { DateFormat.TIMESTAMP }));

        for (Object json : array) {
            JSONObject jsonObject = JSONObject.fromObject(json, jsonConfig);
            list.add((T) JSONObject.toBean(jsonObject, clazz, new HashMap<>()));
        }
        return list;
    }

    /**
     * Convert json to map.
     *
     * @param json
     * @return map
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> jsonToMap(String json) {
        return JSONObject.fromObject(json);
    }

    public JSONUtil() {
        super();
    }
}
