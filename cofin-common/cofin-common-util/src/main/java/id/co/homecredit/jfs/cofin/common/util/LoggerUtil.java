package id.co.homecredit.jfs.cofin.common.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Common logger.
 *
 * @author muhammad.muflihun
 *
 */
public class LoggerUtil {
    private static final Logger log = LogManager.getLogger(LoggerUtil.class);

    public void debug(String className, String message) {
        log.debug(className + " : " + message);
    }

    public void error(String className, String message) {
        log.error(className + " : " + message);
    }

    public void info(String className, String message) {
        log.info(className + " : " + message);
    }

    public void trace(String className, String message) {
        log.trace(className + " : " + message);
    }

    public void warning(String className, String message) {
        log.warn(className + " : " + message);
    }
}
