package id.co.homecredit.jfs.cofin.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import id.co.homecredit.jfs.cofin.common.model.ShellConnection;

/**
 * SSH composer util.
 *
 * @author muhammad.muflihun
 *
 */
public class SSHUtil {
    private JSch jschSSHChannel;
    private String strUserName;
    private String strConnectionIP;
    private Integer intConnectionPort;
    private String strPassword;
    private Session sesConnection;
    private Integer intTimeOut;

    public SSHUtil() {

    }

    public SSHUtil(String userName, String password, String connectionIP, String knownHostsFileName,
            Integer connectionPort, Integer timeout) {
        jschSSHChannel = new JSch();
        strUserName = userName;
        strPassword = password;
        strConnectionIP = connectionIP;
        intConnectionPort = connectionPort;
        intTimeOut = timeout;
    }

    public void close() {
        sesConnection.disconnect();
    }

    public void connect() throws JSchException {
        sesConnection = jschSSHChannel.getSession(strUserName, strConnectionIP, intConnectionPort);
        sesConnection.setPassword(strPassword);
        sesConnection.connect(intTimeOut);
    }

    public Boolean removeAndWriteToFile(ShellConnection shellConn, String filename,
            List<String> texts) {
        try {
            SSHUtil sshUtil = new SSHUtil(shellConn.getUsername(), shellConn.getPassword(),
                    shellConn.getIp(), shellConn.getHost(), shellConn.getPort(),
                    shellConn.getTimeout());
            sshUtil.connect();
            sshUtil.sendCommand("rm -f " + filename);
            sshUtil.sendCommand("touch " + filename);

            for (String text : texts) {
                sshUtil.sendCommand("printf '" + text + "\n' >> " + filename);
            }
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Boolean.FALSE;
    }

    public List<String> sendCommand(String command) {
        List<String> message = new ArrayList<>();
        try {
            Channel channel = sesConnection.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            InputStream inputStream = channel.getInputStream();
            channel.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                message.add(line);
            }
            channel.disconnect();
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return null;
        } catch (JSchException jSchException) {
            jSchException.printStackTrace();
            return null;
        }
        return message;
    }

    public List<String> sendCommandStdAlone(String command, ShellConnection shellConn)
            throws JSchException {
        SSHUtil sshUtil = new SSHUtil(shellConn.getUsername(), shellConn.getPassword(),
                shellConn.getIp(), shellConn.getHost(), shellConn.getPort(),
                shellConn.getTimeout());
        sshUtil.connect();
        List<String> message = sshUtil.sendCommand(command);
        sshUtil.close();
        return message;
    }
}
