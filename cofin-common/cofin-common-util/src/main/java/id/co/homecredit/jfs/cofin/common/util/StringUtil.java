package id.co.homecredit.jfs.cofin.common.util;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.StringChar;

/**
 * Helper class for customizing string.
 *
 * @author muhammad.muflihun
 *
 */
public class StringUtil {

    /**
     * Split {@code string} using semicolon. This also clean-up any leading or trailing white space.
     *
     * @param string
     * @param delimiter
     * @return stringList
     */
    public static List<String> splitAndTrimAsList(String string) {
        return splitAndTrimAsList(string, StringChar.SEMICOLON);
    }

    /**
     * Split {@code string} by given {@code delimiter}. This also clean-up any leading or trailing
     * white space. Return as array.
     *
     * @param string
     * @param delimiter
     * @return stringArray
     */
    public static String[] splitAndTrim(String string, String delimiter) {
        String spaceRegex = "\\s*";
        return string.trim().split(spaceRegex.concat(delimiter).concat(spaceRegex));
    }

    /**
     * Split {@code string} using semicolon. This also clean-up any leading or trailing white space.
     * Then will join each elements as sql parameter.
     *
     * @param string
     * @return sqlParameter
     */
    public static String splitAndTrimAndJoinToSql(String string) {
        String sqlParameter = "";
        List<String> strings = splitAndTrimAsList(string);
        for (String str : strings) {
            sqlParameter += "'" + str + "',";
        }
        return sqlParameter.substring(0, sqlParameter.length() - 1);
    }

    /**
     * Split {@code string} by given {@code delimiter}. This also clean-up any leading or trailing
     * white space. Return as list.
     *
     * @param string
     * @param delimiter
     * @return stringList
     */
    public static List<String> splitAndTrimAsList(String string, String delimiter) {
        return Arrays.asList(splitAndTrim(string, delimiter));
    }

    /**
     * Split {@code string} by given {@code delimiter}.
     *
     * @param string
     * @param delimiter
     * @return stringArray
     */
    public static String[] splitter(String string, String delimiter) {
        return string.split(delimiter);
    }

    /**
     * Split {@code string} by pipe.
     *
     * @param string
     * @return stringArray
     */
    public static String[] splitterPipe(String string) {
        return splitter(string, StringChar.BSLASH.concat(StringChar.PIPE));
    }

    /**
     * Split {@code string} by semicolon.
     *
     * @param string
     * @return stringArray
     */
    public static String[] splitterSemicolon(String string) {
        return splitter(string, StringChar.SEMICOLON);
    }

    /**
     * Generate string of random UUID.
     *
     * @return string
     */
    public static String uuidGenerator() {
        return UUID.randomUUID().toString();
    }

    /**
     * Generate string of random UUID without strip.
     *
     * @return string
     */
    public static String uuidGeneratorShort() {
        return uuidGenerator().replaceAll(StringChar.STRIP, StringChar.EMPTY_STRING).toUpperCase();
    }

    /**
     * Pad parameter {@code str} with white-space on the left until match {@code length} characters.
     * <br>
     * If {@code str} is longer than {@code length}, no padding will be done.
     * <p>
     * ex: <br>
     * str = "123" and n = 5 then " 123". <br>
     * str = "abcdef" and n = 3 then "abcdef" <br>
     * str = "" and n = 10 then " " <br>
     *
     * @param str
     * @param length
     * @return paddedString
     */
    public String leftPaddingSpace(String str, Integer length) {
        return String.format("%1$" + length + "s", str == null ? StringChar.EMPTY_STRING : str);
    }

    /**
     * Pad parameter {@code str} with white-space on the right until match {@code length}
     * characters. <br>
     * If {@code str} is longer than {@code length}, no padding will be done.
     * <p>
     * ex: <br>
     * str = "123" and n = 5 then "123 ". <br>
     * str = "abcdef" and n = 3 then "abcdef" <br>
     * str = "" and n = 10 then " " <br>
     *
     * @param str
     * @param length
     * @return paddedString
     */
    public String rightPaddingSpace(String str, Integer length) {
        return String.format("%1$-" + length + "s", str == null ? StringChar.EMPTY_STRING : str);
    }

    /**
     * Separate multiple email receiver with semicolon.
     *
     * @param datas
     * @return string
     */
    public String separateEmailReceiver(String... receivers) {
        return separateWithDelimiter(StringChar.SEMICOLON, receivers);
    }

    /**
     * Separate datas with chosen delimiter.
     *
     * @param delimiter
     * @param datas
     * @return string
     */
    private String separateWithDelimiter(String delimiter, String... datas) {
        StringBuilder sb = new StringBuilder(255);
        for (String data : datas) {
            sb.append(data).append(delimiter);
        }
        return sb.toString();
    }
}
