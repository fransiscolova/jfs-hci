package id.co.homecredit.jfs.cofin.common.util;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * Abstract test class for common util.
 *
 * @author muhammad.muflihun
 *
 */
@ContextConfiguration(locations = { "classpath*:cofin-common-util-test-context.xml" })
public abstract class BaseUtilTest extends AbstractTestNGSpringContextTests {

}
