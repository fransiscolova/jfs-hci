package id.co.homecredit.jfs.cofin.common.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.time.StopWatch;
import org.testng.annotations.Test;

import net.sf.json.JSON;

/**
 * Class test for {@link JSONUtil}.
 *
 * @author muhammad.muflihun
 *
 */
public class JSONUtilTest extends BaseUtilTest {

    private JSONUtil jsonUtil;

    @Test
    public void convertToJSON() {
        Map<String, Object> map = new HashMap<>();
        map.put("key1", 123456);
        map.put("key2", "some text");
        map.put("key3", 1000L);
        // map.put("key4", new Date());
        map.put("key5", new BigDecimal("918273012831.1234"));
        map.put("key6", new BigInteger("918273012831"));
        StopWatch sw = new StopWatch();
        sw.start();
        JSON json = jsonUtil.convertToJSON(map);
        System.out.println(json);
        sw.stop();
        System.out.println(sw.getTime());
    }

}
