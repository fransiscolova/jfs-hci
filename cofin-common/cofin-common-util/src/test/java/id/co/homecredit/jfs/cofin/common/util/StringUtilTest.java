package id.co.homecredit.jfs.cofin.common.util;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.StringChar;

/**
 * Class test for {@link StringUtil}.
 *
 * @author muhammad.muflihun
 *
 */
public class StringUtilTest extends BaseUtilTest {
    private StringUtil stringUtil = new StringUtil();

    @Test
    public void lastIndex() {
        // String filename = "BARANG_ALL";
        // String filename = "BARANG_ALL.txt";
        String filename = "BARANG.BARANG.ALL.txt";
        String appender = "2018-01-03";
        int lastPoint = filename.lastIndexOf(".");
        System.out.println(lastPoint);
        String ext = filename.substring(lastPoint + 1);
        System.out.println(ext);
        if (lastPoint == -1 || !ext.matches("\\w+")) {
            System.out.println("error");
            return;
        }
        String name = filename.substring(0, lastPoint);
        System.out.println(name.concat(StringChar.UNDERSCORE).concat(appender)
                .concat(StringChar.DOT).concat(ext));
    }

    @Test
    public void leftPaddingSpace() {
        String data = "12345";
        String paddedString = stringUtil.leftPaddingSpace(data, 15);
        System.out.println(paddedString);
        Assert.assertTrue(paddedString.length() == 15);
    }

    @Test
    public void rightPaddingSpace() {
        String data = "abcde";
        String paddedString = stringUtil.rightPaddingSpace(data, 15);
        System.out.println(paddedString);
        Assert.assertTrue(paddedString.length() == 15);
    }

    @Test
    public void separateEmailReceiver() {
        String receptor = stringUtil.separateEmailReceiver("email@test.com", "email2@test.com",
                "email3@test.com");
        System.out.println(receptor);
        Assert.assertTrue(receptor.contains(";"));
    }

    @Test
    public void splitAndTrim() {
        String string = " CAT_MP ; CAT_DP;CAT_AA ;";
        List<String> list = stringUtil.splitAndTrimAsList(string);
        System.out.println(list);
        for (String data : list) {
            System.out.println(data);
        }
    }

    @Test
    public void splitAndTrimAndJoinToSql() {
        String string = " CAT_MP ; CAT_DP;CAT_AA ;";
        System.out.println(stringUtil.splitAndTrimAndJoinToSql(string));
    }

    @Test
    public void splitSlash() {
        String some = "/var/me/text.txt";
        String[] splitted = some.split("/");
        splitted[splitted.length - 1] += ".aes";
        System.out.println(String.join("/", splitted));
    }

    @Test
    public void splitter() {
        String receptor = "email@test.com;email2@test.com";
        String[] data = stringUtil.splitterSemicolon(receptor);
        System.out.println(data.toString());
        Assert.assertTrue(data.length == 2);
    }

    @Test
    public void splitterPipe() {
        String datas = "aaa|bbb|c|d|e";
        String[] data = stringUtil.splitterPipe(datas);
        for (String dat : data) {
            System.out.println(dat);
        }
        Assert.assertTrue(data.length == 5);
    }

    @Test
    public void uuidGenerator() {
        String uuid = stringUtil.uuidGenerator();
        System.out.println(uuid);
        Assert.assertTrue(uuid.length() == 36);
    }

    @Test
    public void uuidGeneratorShort() {
        String uuid = stringUtil.uuidGeneratorShort();
        System.out.println(uuid);
        Assert.assertTrue(uuid.length() == 32);
    }
}
