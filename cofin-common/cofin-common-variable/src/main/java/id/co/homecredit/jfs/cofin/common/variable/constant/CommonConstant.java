package id.co.homecredit.jfs.cofin.common.variable.constant;

/**
 * Common constant.
 *
 * @author muhammad.muflihun
 *
 */
public class CommonConstant {

    /**
     * Constant configuration for entity column.
     *
     * @author muhammad.muflihun
     *
     */
    public static class ColumnName {
        public static final String TEXT_CONTRACT_NUMBER="TEXT_CONTRACT_NUMBER";
        public static final String CREATED_BY = "created_by";
        public static final String CREATED_DATE = "created_date";
        public static final String UPDATED_BY = "updated_by";
        public static final String UPDATED_DATE = "updated_date";
        public static final String VALID_FROM = "valid_from";
        public static final String VALID_TO = "valid_to";
        public static final String DELETED = "deleted";
        public static final String VERSION = "version";
        public static final String ID = "id";
        public static final String IS_DELETE = "IS_DELETE";
    }

    /**
     * Constant for date formatting.
     *
     * @author muhammad.muflihun
     *
     */
    public static class DateFormat {
        public static final String TIMESTAMP = "yyyy-MM-dd hh:mm:ss.SSS";
        public static final String FORMAT_1 = "yyyy-MM-dd";
        public static final String FORMAT_2 = "yyyy/MM/dd";
        public static final String FORMAT_3 = "yyyy-MM-dd-HH-mm-ss";
        public static final String FORMAT_4 = "HH-mm-ss";
    }

    /**
     * Constant configuration for entity.
     *
     * @author muhammad.muflihun
     *
     */
    public static class Entity {
        public static final String UUID_GENERATOR = "UUID_generator";
        public static final String UUID_GENERATOR_CLASS = "id.co.homecredit.jfs.cofin.common.util.IdGenerator";
        public static final Boolean ACTIVATE_SOFT_DELETE = true;
    }

    /**
     * Constant for HTML.
     *
     * @author muhammad.muflihun
     *
     */
    public static class Html {
        public static final String ENTER = "\n";
        public static final String TAB_SPACE = "\t";
    }

    /**
     * Constant for string character.
     *
     * @author muhammad.muflihun
     *
     */
    public static class StringChar {
        public static final String EMPTY_STRING = "";
        public static final String SPACE = " ";
        public static final String STRIP = "-";
        public static final String PIPE = "|";
        public static final String SEMICOLON = ";";
        public static final String SLASH = "/";
        public static final String BSLASH = "\\";
        public static final String LEFT_PARENTHESES = "(";
        public static final String RIGHT_PARENTHESES = ")";
        public static final String COMMA = ",";
        public static final String UNDERSCORE = "_";
        public static final String DOT = ".";
    }
}
