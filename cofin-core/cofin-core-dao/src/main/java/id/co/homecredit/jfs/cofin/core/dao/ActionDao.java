package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Action;

/**
 * Dao interface class for {@link Action}.
 *
 * @author Fransisco.situmorang
 *
 */
public interface ActionDao extends Dao<Action, String> {

    /**
     * Get action by name.
     *
     * @param name
     * @return action
     */
    public Action getByName(String name);

    /**
     * Get action by role_code.
     *
     * @param role_code
     * @return action
     */
    public Action getByRoleCode(String roleCode);
    
    /**
     * Get Action Like Name
     * 
     * @param name
     * @return action
     */
    public List<Action> getActionLikeName(String name);

}
