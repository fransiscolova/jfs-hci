package id.co.homecredit.jfs.cofin.core.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.common.dao.LogDao;
import id.co.homecredit.jfs.cofin.core.model.AuditLog;

/**
 * Dao interface class for {@link AuditLog}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ActivityLogDao extends LogDao<AuditLog, String> {

    /**
     * Save {@link AuditLog} with propagation requires new.
     *
     * @param activityLog
     * @return activityLog
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public AuditLog saveWithNewPropagation(AuditLog activityLog);

}
