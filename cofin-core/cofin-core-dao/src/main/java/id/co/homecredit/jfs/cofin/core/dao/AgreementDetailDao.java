package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.AgreementDetail;

/**
 * Dao interface class for {@link AgreementDetail}.
 *
 * @author muhammad.muflihun
 *
 */
public interface AgreementDetailDao extends Dao<AgreementDetail, String> {

    /**
     * Get active agreement detail by agreement id and date.
     *
     * @param agreementId
     * @param date
     * @return agreementDetail
     */
    public AgreementDetail getActiveAgreementDetailByAgreementIdAndDate(String agreementId,
            Date date);

    /**
     * Get all agreement detail by agreement id.
     *
     * @param agreementId
     * @return agreementDetails
     */
    public List<AgreementDetail> getAllAgreementDetailByAgreementId(String agreementId);
}
