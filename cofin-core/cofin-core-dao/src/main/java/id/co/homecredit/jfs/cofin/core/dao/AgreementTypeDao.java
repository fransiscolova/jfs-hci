package id.co.homecredit.jfs.cofin.core.dao;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.AgreementType;

/**
 * Dao Interface Class For {@link AgreementType}
 * 
 * @author denny.afrizal01
 *
 */
public interface AgreementTypeDao extends Dao<AgreementType,String>{

}
