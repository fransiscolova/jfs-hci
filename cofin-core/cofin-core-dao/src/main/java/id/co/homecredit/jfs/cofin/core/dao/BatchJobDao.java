package id.co.homecredit.jfs.cofin.core.dao;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.BatchJob;

/**
 * Dao interface class for {@link BatchJob}.
 *
 * @author muhammad.muflihun
 *
 */
public interface BatchJobDao extends Dao<BatchJob, String> {

    /**
     * Get cron expression from batch job by partner name.
     *
     * @param partnerName
     * @return cronExpression
     */
    public String getCronExpressionByPartnerName(String partnerName);

    /**
     * Check if flag generate is active by partner name. Returns true if flag set to 'Y'.
     *
     * @param partnerName
     * @return boolean
     */
    public Boolean isFlagGenerateActiveByPartnerName(String partnerName);

}
