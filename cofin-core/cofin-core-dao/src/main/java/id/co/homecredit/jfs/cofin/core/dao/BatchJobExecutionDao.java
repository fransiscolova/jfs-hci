package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.BatchJobExecution;

/**
 * Dao interface class for {@link BatchJobExecution}.
 *
 * @author fransisco.situmorang
 *
 */
@Transactional
public interface BatchJobExecutionDao {

    /**
     * Get Batch_JOB_EXECUTION
     *
     * @param jobinstanceId
     * @return batchJobExecutionId
     */
    public List<BatchJobExecution> getBatchJobExecutionByJobInstanceId(String jobname);

    /**
     * Get Batch_JOB_EXECUTION
     *
     * @param jobinstanceId,createDateFrom,createDateFrom
     * @return batchJobExecutionId
     */
    public List<BatchJobExecution> getBatchJobExecutionByJobInstanceId(String jobname,
            Date createDateFrom, Date createDateTo);

}
