package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.BatchJob;
import id.co.homecredit.jfs.cofin.core.model.BatchJobInstance;

/**
 * Dao interface class for {@link BatchJob}.
 *
 * @author fransisco.situmorang
 *
 */
@Transactional
public interface BatchJobInstanceDao {

    public BatchJobInstance getAllBatchInstance();

    /**
     * Get Batch_JOB_INSTANCE.
     *
     * @param name
     * @return batchjobname
     */
    public List<BatchJobInstance> getBatchInstanceByName(String jobname);
}
