package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.BatchStep;

/**
 * Dao interface class for {@link BatchStep}.
 *
 * @author muhammad.muflihun
 *
 */
public interface BatchStepDao extends Dao<BatchStep, String> {

    /**
     * Get all batch step ordered by priority and partner name.
     *
     * @param partnerName
     * @return batchSteps
     */
    public List<BatchStep> getAllBatchStepPrioritizedByPartnerName(String partnerName);

    public List<BatchStep> getBatchStepByJobId(String jobsId);

    /*
     * Get BatchStep
     * @param jobId return list BatchId
     */

    /**
     * Get next step name by current step name.
     *
     * @param stepName
     * @return stepName
     */
    public String getNextStepNameByStepName(String stepName);

}
