package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.BatchStepExecution;

/**
 * Dao interface class for {@link BatchStepExecution}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface BatchStepExecutionDao {

    /**
     * Get batch step execution.
     *
     * @param stepExecutionId
     * @return batchStepExecution
     */
    public BatchStepExecution get(Long stepExecutionId);

    /**
     * Get all batch step execution.
     *
     * @param jobExecutionId
     * @return list batchStepExecution
     */
    public List<BatchStepExecution> getBatchStepExecutionByJobExecutionId(Long jobExecutionId);

    /**
     * Get last step name exclude flow by job name.
     *
     * @param jobName
     * @return stepName
     */
    public String getLastStepNonFlowByJobName(String jobName);

    /**
     * Save batch step execution.
     *
     * @param batchStepExecution
     * @return batchStepExecution
     */
    public BatchStepExecution save(BatchStepExecution batchStepExecution);
}
