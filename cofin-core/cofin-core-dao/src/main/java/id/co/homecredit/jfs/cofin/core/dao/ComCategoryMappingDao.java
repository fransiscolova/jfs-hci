package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.ComCategoryMapping;

/**
 * Dao interface class for {@link ComCategoryMapping}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ComCategoryMappingDao extends Dao<ComCategoryMapping, String> {

    /**
     * Get all com category mappings by partner id.
     *
     * @param partnerId
     * @return comCategoryMappings
     */
    public List<ComCategoryMapping> getAllComCategoryMappingsByPartnerId(String partnerId);

}
