package id.co.homecredit.jfs.cofin.core.dao;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.ComTypeMapping;

/**
 * Dao interface class for {@link ComTypeMapping}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ComTypeMappingDao extends Dao<ComTypeMapping, String> {

}
