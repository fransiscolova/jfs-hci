package id.co.homecredit.jfs.cofin.core.dao;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.CommodityCategory;

/**
 * Dao interface class for {@link CommodityCategory}.
 *
 * @author muhammad.muflihun
 *
 */
public interface CommodityCategoryDao extends Dao<CommodityCategory, String> {

}
