package id.co.homecredit.jfs.cofin.core.dao;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.CommodityType;

/**
 * Dao interface class for {@link CommodityType}.
 *
 * @author muhammad.muflihun
 *
 */
public interface CommodityTypeDao extends Dao<CommodityType, String> {

}
