package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.common.model.enumeration.ContractStatusEnum;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.Contract;
import id.co.homecredit.jfs.cofin.core.model.Partner;

/**
 * Dao interface class for {@link Contract}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ContractDao extends Dao<Contract, String> {

    /**
     * Get all contracts .
     *
     * @param contractCode,
     * @param status
     * @param partner
     * @param agreement
     * @return contracts
     * @throws Exception
     */
    List<Contract> getAllContractsInquiry(String contractCode, ContractStatusEnum status,
            Partner partner, Agreement agreement, JSONObject dateFilter) throws Exception;

    /**
     * Get contract by contract number.
     *
     * @param contractNumber
     * @return contract
     */
    public Contract getContractByContractNumber(String contractNumber);
    
}
