package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.ContractStatus;

/**
 * Dao interface class for {@link ContractStatus}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ContractStatusDao extends Dao<ContractStatus, String> {

    /**
     * Get all contract statuses by contract number.
     *
     * @param contractNumber
     * @return contractStatuses
     */
    public List<ContractStatus> getAllContractStatusesByContractNumber(String contractNumber);

}
