package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;

/**
 * Dao interface class for {@link CriteriaEligibility}.
 *
 * @author muhammad.muflihun
 *
 */
public interface CriteriaEligibilityDao extends Dao<CriteriaEligibility, String> {

    /**
     * Get all criteria eligibilities by agreement id.
     *
     * @param agreementId
     * @return criteriaELigibility
     */
    public List<CriteriaEligibility> getAllCriteriaEligibilitiesByAgreementId(String agreementId);

}
