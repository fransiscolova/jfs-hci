package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;

/**
 * Dao interface class for {@link CriteriaMapping}.
 *
 * @author muhammad.muflihun
 *
 */
public interface CriteriaMappingDao extends Dao<CriteriaMapping, String> {

    /**
     * Get all criteria mappings by agreement id.
     *
     * @param agreementId
     * @return criteriaMappings
     */
    public List<CriteriaMapping> getAllCriteriaMappingsByAgreementId(String agreementId);

    /**
     * Get all criteria mappings by criteria.
     *
     * @param criteiraEnum
     * @return criteriaMappings
     */
    public List<CriteriaMapping> getAllCriteriaMappingsByCriteria(CriteriaEnum criteiraEnum);

    /**
     * Get all criteria mappings by date.
     *
     * @param date
     * @return criteriaMappings
     */
    public List<CriteriaMapping> getAllCriteriaMappingsByDate(Date date);
}
