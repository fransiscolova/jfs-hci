package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.EconomicSector;

/**
 * Dao interface class for {@link EconomicSector}.
 *
 * @author muhammad.muflihun
 *
 */
public interface EconomicSectorDao extends Dao<EconomicSector, String> {

    /**
     * Get all economic sectors by partner id.
     *
     * @param partnerId
     * @return economicSectors
     */
    public List<EconomicSector> getAllEconomicSectorsByPartnerId(String partnerId);

    /**
     * Get economic sector by bank commodity group and BI economic sector code.
     *
     * @param bankCommodityGroup
     * @param biCommoditySectorCode
     * @return economicSector
     */
    public EconomicSector getEconomicSectorByBankCommodityGroupAndBIEconomicSectorCode(
            String bankCommodityGroup, String biEconomicSectorCode);

    /**
     * Get economic sector by partner id and bank commodity group.
     *
     * @param partnerId
     * @param bankCommodityGroup
     * @return economicSector
     */
    public EconomicSector getEconomicSectorByPartnerIdAndBankCommodityGroup(String partnerId,
            String bankCommodityGroup);

}
