package id.co.homecredit.jfs.cofin.core.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.common.dao.LogDao;
import id.co.homecredit.jfs.cofin.core.model.ExportLog;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;

/**
 * Dao interface class for {@link ExportLog}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ExportLogDao extends LogDao<ExportLog, String> {

    /**
     * Get all export log by step execution id.
     *
     * @param stepExecutionId
     * @return exportLogs
     */
    public List<ExportLog> getExportLogByStepExecutionId(Long stepExecutionId);

    /**
     * Get latest export log by step execution id, agreement type, and date.
     *
     * @param stepExecutionId
     * @param agreementType
     * @param date
     * @return exportLog
     */
    public ExportLog getLatestExportLogByStepExecutionIdAndAgreementTypeAndDate(
            Long stepExecutionId, AgreementTypeEnum agreementType, Date date) throws ParseException;

    /**
     * Save export log with new propagation.
     *
     * @param exportLog
     * @return exportLog
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ExportLog saveWithNewPropagation(ExportLog exportLog);
}
