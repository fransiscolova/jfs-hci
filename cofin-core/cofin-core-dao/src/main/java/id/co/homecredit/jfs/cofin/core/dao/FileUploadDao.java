package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;

/**
 * Dao interface class for {@link Action}.
 *
 * @author Fransisco.situmorang
 *
 */
public interface FileUploadDao extends Dao<FileUpload, String> {

    

    public List<FileUpload> getFileUpload(String processDate,String confirmFile);
	
	

}
