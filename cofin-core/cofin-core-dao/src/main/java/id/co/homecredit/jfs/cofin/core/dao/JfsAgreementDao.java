package id.co.homecredit.jfs.cofin.core.dao;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.core.model.JfsAgreement;

/**
 * Dao Interface Class For {@link JfsAgreement}
 * 
 * @author denny.afrizal01
 *
 */
public interface JfsAgreementDao extends DaoIdOnly<JfsAgreement,String>{

	/**
     * Execute Select Query
     * 
     * @param query
     * @return script
     */
	public List<Map<String,Object>> executeSelectQuery(String query);
	
	/**
	 * Get Agreement By Agreement ID
	 * 
	 * @apram id
	 * @return id
	 * @throws ParseException 
	 */
	public JfsAgreement getJfsAgreementById(Long agreementId);
	
	/**
     * Get JFS Agreement Value By Partner ID
     * 
     * @param partnerId
     * @return jfsAgreement
     */
    public List<JfsAgreement> getJfsAgreementByPartnerId(String partnerId)throws Exception;
    
    /**
     * Get Max Value Of JFS Agreement
     * 
     * @param ""
     * @return agreementId
     */
    public String getMaxIdAgreement();
	
}
