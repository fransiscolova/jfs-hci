package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;
import id.co.homecredit.jfs.cofin.core.model.JfsContract;

/**
 * Dao interface class for {@link JfsContract}.
 *
 * @author denny.afrizal01
 *
 */

public interface JfsContractDao{

    /**
     * Get Value Of JFS Contract
     * 
     * @param contractNumber
     * @return jfaContract
     */
    public List<JfsContract> getJfsContractWithoutDate(String contractNumber, String status, Long idAgreement);
    
    /**
     * Get Value Of JFS Contract With Date Criteria
     *
     * @param contractNumber
     * @return jfsContract
     */
    public List<JfsContract> getJfsContractWithDate(String contractNumber, String status, Long idAgreement, Date exportDate);

    /**
     * Get Value Of JFS Agreement Between Export Date
     *
     * @param contractNumber
     * @return jfsAgreement
     */
    public List<JfsContract> getJfsContractBetweenDate(String contractNumber,String status,Long idAgreement,Date dateFrom,Date dateTo);

    
    /**
     * Get Header Value Of JFS Contract
     *
     * @param contractNumber
     * @value jfsContract
     */
    public JfsContract getHeaderJfsContract(String contractNumber);

//    /**
//     * Get Detail JfsContract
//     *
//     * @param incomingPaymentId
//     * @value jfsContract
//     */
//    public JfsContract getDetailJfsContract(String textContractNumber);
	
}
