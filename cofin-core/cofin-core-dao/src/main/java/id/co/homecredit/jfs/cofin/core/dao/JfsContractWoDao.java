package id.co.homecredit.jfs.cofin.core.dao;

import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsContractWo;

/**
 * Dao Interface Class For {@link JfsContractWo}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface JfsContractWoDao{

	/**
	 * Get Value Of WO Contract By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 * @throws ParseException 
	 */
	public JfsContractWo getContractWoByContractNumber(String contractNumber);
	
}
