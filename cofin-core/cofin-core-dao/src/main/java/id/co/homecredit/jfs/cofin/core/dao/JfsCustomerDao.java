package id.co.homecredit.jfs.cofin.core.dao;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsCustomer;
import id.co.homecredit.jfs.cofin.core.model.dto.JfsCustomerBsl;

/**
 * Dao Interface Class For {@link JfsCustomer}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface JfsCustomerDao {
	
	/**
	 * Get Value Of Detail Customer From BSL By CUID
	 * 
	 * @param cuid
	 * @return cuid
	 * @throws ServiceException 
	 * @throws RemoteException 
	 * @throws MalformedURLException 
	 */
	public JfsCustomerBsl getDetailCustomer(String cuid)throws MalformedURLException,RemoteException;

}
