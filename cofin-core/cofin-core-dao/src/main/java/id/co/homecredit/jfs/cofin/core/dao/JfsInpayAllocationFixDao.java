package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import id.co.homecredit.jfs.cofin.core.model.JfsInpayAllocationFix;

/**
 * Dao Interface Class For {@link JfsInpayAllocationFix}
 * 
 * @author denny.afrizal01
 *
 */
public interface JfsInpayAllocationFixDao{

	/**
	 * Get Allocation Amount
	 * 
	 * @param incomingPaymentId
	 * @return incomingPaymentId
	 */
	public JfsInpayAllocationFix getAllocationAmountValue(Long incomingPaymentId,Date dateInstalment);
	
}
