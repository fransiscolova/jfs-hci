package id.co.homecredit.jfs.cofin.core.dao;

import java.text.ParseException;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.core.model.JfsOfiPayment;

/**
 * Dao Interface Class For {@link JfsOfiPayment}
 * 
 * @author denny.afrizal01
 *
 */
public interface JfsOfiPaymentDao extends DaoIdOnly<JfsOfiPayment,String>{

	/**
	 * Dao Interface Class For Insert To JFS_OFI_PAYMENT
	 * 
	 * @param sql
	 * @return sql
	 * @throws ParseException 
	 */
	public boolean saveOfiPayment(String sql)throws Exception;
	
}
