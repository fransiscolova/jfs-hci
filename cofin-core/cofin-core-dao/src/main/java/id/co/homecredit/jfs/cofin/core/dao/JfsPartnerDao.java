package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;
import id.co.homecredit.jfs.cofin.core.model.JfsPartner;

/**
 * Dao Interface Class For {@link JfsPartner}
 * 
 * @author denny.afrizal01
 *
 */
public interface JfsPartnerDao{

	/**
	 * Get Name Of Partner
	 * 
	 * @param partnerId
	 * @return name
	 */
	public String getPartnerName(String partnerId);
	
	/**
	 * Get All Partner
	 * 
	 * @return partner
	 */
	public List<JfsPartner> getAllPartner();
	
	/**
	 * Search JFS Partner Value By Name
	 * 
	 * @param id
	 * @return JfsPartner
	 */
	public JfsPartner getPartnerValueById(String id);

	/**
	 * Save Object Of JFS Partner
	 *
	 * @param object
	 * @return object
	 */
	public boolean saveObject(Object object);

	/**
	 * Update Object Using Query Language
	 *
	 * @param script
	 * @return script
	 */
	public boolean updateObjectWithQuery(String script);
	
}
