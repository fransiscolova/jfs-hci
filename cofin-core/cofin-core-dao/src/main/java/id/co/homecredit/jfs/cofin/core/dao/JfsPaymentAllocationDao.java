package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentAllocation;

/**
 * Dao Interface Class For {@link JFsPaymentAllocation}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface JfsPaymentAllocationDao{

	/**
	 * Get Payment Allocation By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public List<JfsPaymentAllocation> getAllocationPartnerByContractNumber(String contractNumber);
	
}
