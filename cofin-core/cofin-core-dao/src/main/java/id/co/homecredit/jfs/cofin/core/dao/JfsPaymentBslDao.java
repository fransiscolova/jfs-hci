package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentBsl;

/**
 * Dao Interface Class For {@link JfsPaymentBsl}
 * 
 * @author denny.afrizal01
 *
 */

public interface JfsPaymentBslDao extends DaoIdOnly<JfsPaymentBsl,String>{
	
	/**
	 * Get BSL Data By Date
	 * 
	 * @param paymentDate
	 * @return paymentDate
	 */
	public List<JfsPaymentBsl> getBslPaymentByDate(Date paymentDate);

	/**
	 * Get BSL Data By Contract Number
	 *
	 * @param textContractNumber
	 * @return textContractNumber
	 */
	public List<JfsPaymentBsl> getBslPaymentByContractNumber(String textContractNumber);

	/**
	 * Get Payment BSL Between Date
	 *
	 * @param dateFrom
	 * @return dateTo
	 */
	public List<JfsPaymentBsl> getBslPaymentBetweenDate(Date dateFrom,Date dateTo);
	
}
