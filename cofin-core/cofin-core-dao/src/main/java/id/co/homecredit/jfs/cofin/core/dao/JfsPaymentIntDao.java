package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentInt;

/**
 * Dao Interface Class For {@link JfsPaymentInt}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface JfsPaymentIntDao{

	/**
	 * Get JFS Payment Int Value By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public List<JfsPaymentInt> getPaymentIntByContractNumber(String contractNumber);

	/**
	 * Get JFS Payment Int By Contract Number And Incoming Payment Id
	 *
	 * @param incomingPaymentId
	 * @return textContractNumber
	 */
	public JfsPaymentInt getByContractNumberAndIncomingPayment(String textContractNumber,Long incomingPaymentId);
	
}
