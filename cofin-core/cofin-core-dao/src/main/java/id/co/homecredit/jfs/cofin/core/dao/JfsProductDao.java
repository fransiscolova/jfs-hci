package id.co.homecredit.jfs.cofin.core.dao;

import java.text.ParseException;
import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.core.model.JfsProduct;

/**
 * Dao Interface Class For {@link JfsProduct}
 * 
 * @author denny.afrizal01
 *
 */
public interface JfsProductDao extends DaoIdOnly<JfsProduct,String>{
	
	/**
	 * Get Detail Product By Agreement ID
	 * 
	 * @param id
	 * @return id
	 * @throws ParseException 
	 */
	public List<JfsProduct> getProductByAgreementId(Long agreementId) throws ParseException;
	
	/**
	 * Get JFS Product By ID
	 * 
	 * @param id
	 * @return id
	 * @throws ParseException
	 */
	public JfsProduct getProductById(String id)throws ParseException;

}
