package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;
import java.util.Map;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.core.model.JfsProductHci;

/**
 * Dao interface class for {@link JfsProductHci}.
 *
 * @author denny.afrizal01
 *
 */
public interface JfsProductHciDao extends DaoIdOnly<JfsProductHci,String>{
    
    /**
     * Execute Select Query
     * 
     * @param script
     * @return script
     */
	public List<Map<String,Object>> executeSelectQuery(String script);
	
	/**
	 * Get JFS Product HCI Value By ID
	 * 
	 * @param id
	 * @return id
	 */
	public JfsProductHci getJfsProductHciById(String id);
}