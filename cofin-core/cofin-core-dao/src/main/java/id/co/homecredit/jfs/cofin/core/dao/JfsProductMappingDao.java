package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;
import java.text.ParseException;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.core.model.JfsProductMapping;

/**
 * Dao Interface Class For {@link JfsProductMapping}
 * 
 * @author denny.afrizal01
 *
 */
public interface JfsProductMappingDao extends DaoIdOnly<JfsProductMapping,String>{
	
	/**
	 * Get All Product Mapping By Agreement ID
	 * 
	 * @param agreementId
	 * @return agreementId
	 */
	public List<JfsProductMapping> getProductMappingByAgreementId(Integer agreementId)throws ParseException;
	
	/**
	 * Get Product Mapping By ID
	 * 
	 * @param id
	 * @return id
	 */
	public JfsProductMapping getProductMappingById(String id)throws ParseException;

}
