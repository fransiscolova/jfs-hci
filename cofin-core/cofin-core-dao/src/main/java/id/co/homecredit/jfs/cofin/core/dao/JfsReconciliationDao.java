package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsReconciliation;

/**
 * Dao Interface Class For {@link JfsReconciliation}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface JfsReconciliationDao{
	
	/**
	 * Get Value Of JFS Reconcilistion By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public List<JfsReconciliation> getReconciliationByContractNumber(String contractNumber);

}
