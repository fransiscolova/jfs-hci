package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsScheduleBsl;

/**
 * Dao Interface Class For {@link JfsScheduleBsl}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface JfsScheduleBslDao{

	/**
	 * Get Value Of Schedule From BSL By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public List<JfsScheduleBsl> getScheduleBslByContractNumber(String contractNumber);
	
}
