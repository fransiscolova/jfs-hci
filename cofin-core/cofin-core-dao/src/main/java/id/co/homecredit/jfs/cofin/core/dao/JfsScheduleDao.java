package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsSchedule;

/**
 * Dao Interface Class For {@link JfsSchedule}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface JfsScheduleDao{

	/**
	 * Get Value Of Schedule From Bank By Contract Number
	 * 
	 * @param textContractNumber
	 * @return contractNumber
	 */
	public JfsSchedule getScheduleJfs(String textContractNumber,Integer partIndex);
	
	/**
	 * Get Max Due Date
	 * 
	 * @param textContractNumber
	 * @return dueDate
	 */
	public Date getMaxDueDate(String textContractNumber)throws Exception;
	
	/**
	 * Get Max Balance
	 * 
	 * @param textContractNumber
	 * @return balance
	 */
	public Double getMaxBalanceValue(String textContractNumber);
	
}
