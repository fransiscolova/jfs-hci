package id.co.homecredit.jfs.cofin.core.dao;

import id.co.homecredit.jfs.cofin.common.dao.LogDao;
import id.co.homecredit.jfs.cofin.core.model.LoginLog;
import id.co.homecredit.jfs.cofin.core.model.Role;

/**
 * Dao interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
public interface LoginLogDao extends LogDao<LoginLog, String> {

    /**
     * Get latest login log by username.
     *
     * @param username
     * @return loginLog
     */
    public LoginLog getLatestLoginLogByUsername(String username);
}
