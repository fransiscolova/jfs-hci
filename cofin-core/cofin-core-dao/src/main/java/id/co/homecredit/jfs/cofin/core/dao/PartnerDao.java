package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Partner;

/**
 * Dao interface class for {@link Partner}.
 *
 * @author muhammad.muflihun
 *
 */
public interface PartnerDao extends Dao<Partner, String> {

    /**
     * Get partner by {@code CODE}.
     *
     * @param code
     * @return partner
     */
    public Partner getPartnerByCode(String code);

    /**
     * Get partner by {@name name}.
     *
     * @param name
     * @return partner
     */
    public Partner getPartnerByName(String name);
}
