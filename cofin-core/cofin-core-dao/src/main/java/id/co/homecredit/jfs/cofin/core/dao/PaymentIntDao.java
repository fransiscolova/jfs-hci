package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.PaymentInt;

/**
 * Dao interface class for {@link PaymentInt}.
 *
 * @author muhammad.muflihun
 *
 */
public interface PaymentIntDao extends Dao<PaymentInt, String> {

    /**
     * Get all payment ints by contract number.
     *
     * @param contractNumber
     * @return paymentInts
     */
    public List<PaymentInt> getAllPaymentIntsByContractNumber(String contractNumber);

    /**
     * Get payment int by incoming payment id.
     *
     * @param incomingPaymentId
     * @return paymentInt
     */
    public PaymentInt getPaymentIntByIncomingPaymentId(String incomingPaymentId);
}
