package id.co.homecredit.jfs.cofin.core.dao;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Product;

/**
 * Dao interface class for {@link Product}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ProductDao extends Dao<Product, String> {

}
