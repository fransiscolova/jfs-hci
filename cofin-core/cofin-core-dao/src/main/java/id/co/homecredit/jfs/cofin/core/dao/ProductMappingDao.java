package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.ProductMapping;

/**
 * Dao interface class for {@link ProductMapping}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ProductMappingDao extends Dao<ProductMapping, String> {

    /**
     * Get all product mappings by agreement id and date.
     *
     * @param agreementId
     * @param date
     * @return productMappings
     */
    public List<ProductMapping> getAllProductMappingsByAgreementIdAndDate(String agreementId,
            Date date);

    /**
     * Get all product mappings by product code and date.
     *
     * @param productCode
     * @param date
     * @return productMappings
     */
    public List<ProductMapping> getAllProductMappingsByProductCodeAndDate(String productCode,
            Date date);

    /**
     * Get product mapping by product code, partner code, and date.
     *
     * @param productCode
     * @param partnerCode
     * @param date
     * @return productMapping
     */
    public ProductMapping getProductMappingByProductCodeAndPartnerCodeAndDate(String productCode,
            String partnerCode, Date date);
}
