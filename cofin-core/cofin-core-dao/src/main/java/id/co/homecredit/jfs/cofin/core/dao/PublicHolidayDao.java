package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.PublicHoliday;

/**
 * Dao interface class for {@link PublicHoliday}.
 *
 * @author muhammad.muflihun
 *
 */
public interface PublicHolidayDao extends Dao<PublicHoliday, String> {

    /**
     * Get public holiday by date.
     *
     * @param date
     * @return public holiday
     */
    public PublicHoliday getPublicHoliday(Date date);

    /**
     * Check if date is public holiday. Return true if date is exist in public holiday.
     *
     * @param date
     * @return boolean
     */
    public Boolean isPublicHoliday(Date date);

}
