package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.RoleAction;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Dao interface class for {@link RoleAction}.
 *
 * @author Fransisco.situmorang
 *
 */
public interface RoleActionDao extends Dao<RoleAction, String> {

    /**
     * Get all role actions by action string.
     *
     * @param name
     * @return list roleAction
     */
    public List<RoleAction> getAllRoleActionsByAction(String actionUrl);

    /**
     * Get all role actions by role.
     *
     * @return roleAction
     */
    public List<RoleAction> getAllRoleActionsByRole(RoleEnum role);

}
