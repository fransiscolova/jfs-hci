package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;
import java.text.ParseException;
import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Dao interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
public interface RoleDao extends Dao<Role, String> {

    /**
     * Get role by role enum.
     *
     * @param roleEnum
     * @return role
     */
    public Role get(RoleEnum roleEnum);

    /**
     * Get all unused role
     *
     * @param roleEnum
     * @return roles
     */
    public List<Role> getAllUnsedRole(List<Enum> col);
    
    /**
     * Get List Roles
     * 
     * @param role
     * @return role
     */
    public List<Role> getListRoles(List<Enum> col);
    
    /**
     * Get Role Value By Role
     * 
     * @param role
     * @return role
     */
    public Role getRoleValueByRole(RoleEnum role)throws ParseException;

}
