package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;

/**
 * Dao interface class for {@link RoleMapping}.
 *
 * @author muhammad.muflihun
 *
 */
public interface RoleMappingDao extends Dao<RoleMapping, String> {

    /**
     * Get all role mappings by username and date.
     *
     * @param username
     * @return roleMappings
     */
    public List<RoleMapping> getAllRoleMappingsByUsername(String username);

    /**
     * Get all valid role mappings by username and date.
     *
     * @param username
     * @param date
     * @return roleMappings
     */
    public List<RoleMapping> getAllValidRoleMappingsByUsername(String username, Date date);

}
