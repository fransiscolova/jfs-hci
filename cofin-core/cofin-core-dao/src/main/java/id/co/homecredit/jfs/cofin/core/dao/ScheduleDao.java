package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Schedule;

/**
 * Dao interface class for {@link Schedule}.
 *
 * @author muhammad.muflihun
 *
 */
public interface ScheduleDao extends Dao<Schedule, String> {

    /**
     * Get all schedules by contract number.
     *
     * @param contractNumber
     * @return schedules
     */
    public List<Schedule> getAllSchedulesByContractNumber(String contractNumber);

}
