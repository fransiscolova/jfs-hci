package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.common.dao.DaoId;
import id.co.homecredit.jfs.cofin.common.dao.DaoIdOnly;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Schedule;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationAllocation;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Dao interface class for {@link Action}.
 *
 * @author Fransisco.situmorang
 *
 */
public interface TempJfsConfirmationAllocationDao extends DaoIdOnly<TempJfsConfirmationAllocation, String> {
 
	  /**
     * run procedure

     *
     */
    public void runProcedurePermata(String query);
}
