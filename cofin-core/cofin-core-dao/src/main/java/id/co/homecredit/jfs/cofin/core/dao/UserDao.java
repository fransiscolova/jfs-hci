package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.User;

/**
 * Dao interface class for {@link User}.
 *
 * @author muhammad.muflihun
 *
 */
public interface UserDao extends Dao<User, String> {

    /**
     * Get all unmapped users.
     *
     * @return users
     */
    public List<User> getAllUnmappedUsers();

    /**
     * Get user
     *
     * @param username
     * @return user
     */

    public User getUserByUserName(String username);

}
