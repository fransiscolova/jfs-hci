package id.co.homecredit.jfs.cofin.core.dao;

import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.VwCofinContractDetail;

/**
 * Dao Interface Class For {@link VwCofinContractDetail}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface VwCofinContractDetailDao{

	/**
	 * Get Contract Detail By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public VwCofinContractDetail getDetailContractByContractNumber(String contractNumber);
	
}
