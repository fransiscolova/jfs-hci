package id.co.homecredit.jfs.cofin.core.dao;

import org.springframework.transaction.annotation.Transactional;

/**
 * Dao Interface Class For {@link VwCofinTown}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public abstract interface VwCofinTownDao {

	/**
	 * Get Description Of Town
	 * 
	 * @param townCode
	 * @return townCode
	 */
	public String getTownDescription(String townCode);
	
	/**
	 * Get Description Of Subdistrict
	 * 
	 * @param subdistrictCode
	 * @return subdistrictCode
	 */
	public String getSubdistrictDescription(String subdistrictCode);
	
	/**
	 * Get Description Of District
	 * 
	 * @Param districtCode
	 * @return districtCode
	 */
	public String getDistrictDescription(String districtCode);
	
}
