package id.co.homecredit.jfs.cofin.core.dao.config;

import java.util.Map;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoBaseImpl;
import id.co.homecredit.jfs.cofin.core.dao.service.CustomerServiceV5Service;

/**
 * Class For Configuration Of WSDL Setting
 * 
 * @author denny.afrizal01
 *
 */
@Configuration
@SuppressWarnings("unchecked")
public class WSDLConfig{
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }
	
	private String getConfig(String category,String key01,String key02){
		Session session = sfJFS.openSession();
		String sql = "select * from COFIN_CONFIG where CATEGORY = '"+category+"' and KEY_01 = '"+key01+"' and KEY_02 = '"+key02+"'";
		Query query = session.createSQLQuery(sql);
		System.out.println(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		session.close();
		if(list.size()==0){
			return "";
		}else{
			return list.get(0).get("VALUE").toString();
		}
	}
	
	@Bean
	public CustomerServiceV5Service customerServiceV5Service(){
		String endpoint = getConfig("OPEN_API","CustomerService","Url");
		String username = getConfig("OPEN_API","CustomerService","USERNAME");
		String password = getConfig("OPEN_API","CustomerService","PASSWORD");
		System.out.println("Access Url = "+endpoint+", UserName = "+username+", Password = "+password);
		return new CustomerServiceV5Service(endpoint,username,password);
	}
	
}
