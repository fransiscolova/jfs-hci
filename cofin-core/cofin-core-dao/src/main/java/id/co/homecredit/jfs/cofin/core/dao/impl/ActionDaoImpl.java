package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.model.Action;

/**
 * Dao implement class for {@link Action}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class ActionDaoImpl extends DaoImpl<Action, String> implements ActionDao {
    private static final Logger log = LogManager.getLogger(ActionDaoImpl.class);

    @Override
    public Action getByName(String name) {
        log.debug("get role by name {}", name);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("name", name));
        return (Action) criteria.uniqueResult();
    }

    @Override
    public Action getByRoleCode(String roleCode) {
        log.debug("get role by role_code {}", roleCode);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("role_code", roleCode));
        return (Action) criteria.uniqueResult();
    }
    
    @Override
    public List<Action> getActionLikeName(String name){
    	log.debug("Get Role Action By Name Like {}", name);
    	Criteria crit = createCriteria();
    	crit.add(Restrictions.like("name", name));
    	return crit.list();
    }

}
