package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.LogDaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ActivityLogDao;
import id.co.homecredit.jfs.cofin.core.model.AuditLog;

/**
 * Dao implement class for {@link AuditLog}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ActivityLogDaoImpl extends LogDaoImpl<AuditLog, String> implements ActivityLogDao {
    private static final Logger log = LogManager.getLogger(ActivityLogDaoImpl.class);

    @Override
    public AuditLog saveWithNewPropagation(AuditLog activityLog) {
        log.debug("save activity log with propagation new {}", activityLog.toString());
        return super.save(activityLog);
    }

}
