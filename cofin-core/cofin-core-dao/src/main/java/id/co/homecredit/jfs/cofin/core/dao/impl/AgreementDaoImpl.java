package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.AgreementDao;
import id.co.homecredit.jfs.cofin.core.model.Agreement;

/**
 * Dao implement class for {@link Agreement}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class AgreementDaoImpl extends DaoImpl<Agreement, String> implements AgreementDao {
    private static final Logger log = LogManager.getLogger(AgreementDaoImpl.class);

    @Override
    public Agreement getAgreementByAliasId(String aliasId) {
        log.debug("get agreement by agreement alias id {}", aliasId);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("aliasId", aliasId));
        return (Agreement) criteria.uniqueResult();
    }

    public Agreement getAgreementWithFetchedDetailsById(String agreementId) {
        log.debug("get agreement by agreement id {} with fetched agreement details", agreementId);
        Criteria criteria = composeCriteria(true);
        criteria.add(Restrictions.eq("id", agreementId));
        criteria.setFetchMode("agreementDetails", FetchMode.JOIN);
        return (Agreement) criteria.uniqueResult();
    }

    @Override
    public Agreement getAgreementWithFetchedProductMappingsById(String agreementId) {
        log.debug("get agreement by agreement id {} with fetched product mappings", agreementId);
        Criteria criteria = composeCriteria(true);
        criteria.add(Restrictions.eq("id", agreementId));
        criteria.setFetchMode("productMappings", FetchMode.JOIN);
        return (Agreement) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Agreement> getAllAgreementsByPartnerCode(String partnerCode) {
        log.debug("Get All Agreements By Partner Code {}", partnerCode);
        Criteria criteria = createCriteria();
        criteria.createAlias("partner", "partner");
        criteria.add(Restrictions.eq("partner.id", partnerCode));
        return criteria.list();
    }

}
