package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.AgreementDetailDao;
import id.co.homecredit.jfs.cofin.core.model.AgreementDetail;

/**
 * Dao implement class for {@link AgreementDetail}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class AgreementDetailDaoImpl extends DaoImpl<AgreementDetail, String>
        implements AgreementDetailDao {
    private static final Logger log = LogManager.getLogger(AgreementDetailDaoImpl.class);

    @Override
    public AgreementDetail getActiveAgreementDetailByAgreementIdAndDate(String agreementId,
            Date date) {
        log.debug("get active agreement by agreement id {} and date {}", agreementId, date);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("agreement.id", agreementId));
        criteria.add(Restrictions.le("validFrom", date));
        criteria.add(Restrictions.ge("validTo", date));
        return (AgreementDetail) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AgreementDetail> getAllAgreementDetailByAgreementId(String agreementId) {
        log.debug("get all agreement by agreement id {}", agreementId);
        return createCriteria().add(Restrictions.eq("agreement.id", agreementId)).list();
    }

}
