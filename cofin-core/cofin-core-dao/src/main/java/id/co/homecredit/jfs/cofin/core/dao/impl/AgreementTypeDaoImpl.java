package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.AgreementTypeDao;
import id.co.homecredit.jfs.cofin.core.model.AgreementType;

/**
 * Dao Implement Class For {@link AgreementTypeDao}
 * 
 * @author denny.afrizal01
 *
 */
@Repository
public class AgreementTypeDaoImpl extends DaoImpl<AgreementType,String> implements AgreementTypeDao{
	private static final Logger log = LogManager.getLogger(AgreementTypeDaoImpl.class);
}
