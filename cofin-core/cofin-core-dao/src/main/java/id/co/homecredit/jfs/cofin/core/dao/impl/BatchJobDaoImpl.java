package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.dao.BatchJobDao;
import id.co.homecredit.jfs.cofin.core.model.BatchJob;

/**
 * Dao implement class for {@link BatchJob}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class BatchJobDaoImpl extends DaoImpl<BatchJob, String> implements BatchJobDao {
    private static final Logger log = LogManager.getLogger(BatchJobDaoImpl.class);

    @Override
    public String getCronExpressionByPartnerName(String partnerName) {
        log.debug("get cron expression by partner name {}", partnerName);
        Criteria criteria = createCriteria();
        criteria.createAlias("partner", "partner");
        criteria.add(Restrictions.eq("partner.code", partnerName));
        criteria.setProjection(Projections.property("cronExpression"));
        return (String) criteria.uniqueResult();
    }

    @Override
    public Boolean isFlagGenerateActiveByPartnerName(String partnerName) {
        log.debug("check flag generate if active by partner name {}", partnerName);
        Criteria criteria = createCriteria();
        criteria.createAlias("partner", "partner");
        criteria.add(Restrictions.eq("partner.code", partnerName));
        criteria.setProjection(Projections.property("flagGenerate"));
        YesNoEnum enu = (YesNoEnum) criteria.uniqueResult();
        return enu.equals(YesNoEnum.Y);
    }
}
