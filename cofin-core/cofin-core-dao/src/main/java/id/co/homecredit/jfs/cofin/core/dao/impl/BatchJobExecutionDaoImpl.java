package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.core.dao.BatchJobExecutionDao;
import id.co.homecredit.jfs.cofin.core.model.BatchJobExecution;
import id.co.homecredit.jfs.cofin.core.model.BatchStepExecution;

/**
 * Dao implement class for {@link BatchStepExecution}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class BatchJobExecutionDaoImpl implements BatchJobExecutionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<BatchJobExecution> getBatchJobExecutionByJobInstanceId(String jobName) {
        // TODO Auto-generated method stub
        Criteria criteria = getSession().createCriteria(BatchJobExecution.class);
        criteria.createAlias("batchJobInstance", "jobInstance");
        criteria.add(Restrictions.eq("jobInstance.jobName", jobName));
        return criteria.list();
    }

    @Override
    public List<BatchJobExecution> getBatchJobExecutionByJobInstanceId(String jobname,
            Date createDateFrom, Date createDateTo) {
        // TODO Auto-generated method stub
        Criteria criteria = getSession().createCriteria(BatchJobExecution.class);
        criteria.createAlias("batchJobInstance", "jobInstance");
        criteria.add(Restrictions.eq("jobInstance.jobName", jobname));
        criteria.add(Restrictions.ge("createTime", createDateFrom));
        criteria.add(Restrictions.le("createTime", createDateTo));

        return criteria.list();
    }

    /**
     * Get current session.
     *
     * @return session
     */
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

}
