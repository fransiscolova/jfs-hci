package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.core.dao.BatchJobInstanceDao;
import id.co.homecredit.jfs.cofin.core.model.BatchJobInstance;
import id.co.homecredit.jfs.cofin.core.model.BatchStepExecution;

/**
 * Dao implement class for {@link BatchStepExecution}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class BatchJobInstanceDaoImpl implements BatchJobInstanceDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Criteria createCriteria() {
        return getSession().createCriteria(BatchJobInstance.class);
    }

    @Override
    public BatchJobInstance getAllBatchInstance() {
        // TODO Auto-generated method stub
        Criteria criteria = createCriteria();
        return (BatchJobInstance) criteria.uniqueResult();
    }

    @Override
    public List<BatchJobInstance> getBatchInstanceByName(String jobname) {
        // TODO Auto-generated method stub
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("jobName", jobname));
        return criteria.list();
    }

    /**
     * Get current session.
     *
     * @return session
     */
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

}
