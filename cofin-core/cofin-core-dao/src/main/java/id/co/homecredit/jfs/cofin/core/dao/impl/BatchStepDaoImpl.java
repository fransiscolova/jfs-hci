package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.BatchStepDao;
import id.co.homecredit.jfs.cofin.core.model.BatchJob;
import id.co.homecredit.jfs.cofin.core.model.BatchStep;

/**
 * Dao implement class for {@link BatchJob}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class BatchStepDaoImpl extends DaoImpl<BatchStep, String> implements BatchStepDao {
    private static final Logger log = LogManager.getLogger(BatchStepDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<BatchStep> getAllBatchStepPrioritizedByPartnerName(String partnerName) {
        log.debug("get all batch step prioritized by partner name {}", partnerName);
        Criteria criteria = createCriteria();
        criteria.createAlias("batchJob.partner", "partner");
        criteria.add(Restrictions.eq("partner.code", partnerName));
        criteria.addOrder(Order.asc("priority"));
        return criteria.list();
    }

    @Override
    public List<BatchStep> getBatchStepByJobId(String jobsId) {
        // TODO Auto-generated method stub
        Criteria criteria = createCriteria();
        criteria.createAlias("batchJob", "batchJob");
        log.debug(jobsId);
        criteria.add(Restrictions.eq("batchJob.id", jobsId));
        return criteria.list();
    }

    @Override
    public String getNextStepNameByStepName(String stepName) {
        log.debug("get next step name by current step name {}", stepName);
        Criteria subquery = createCriteria();
        subquery.add(Restrictions.eq("stepName", stepName));
        subquery.setProjection(Projections.property("priority"));
        Integer priority = (Integer) subquery.uniqueResult();

        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("priority", priority + 1));
        criteria.setProjection(Projections.property("stepName"));
        criteria.addOrder(Order.asc("priority"));
        return (String) criteria.uniqueResult();
    }

}
