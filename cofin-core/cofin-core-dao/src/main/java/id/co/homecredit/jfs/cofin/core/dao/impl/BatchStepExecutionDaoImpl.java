package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.core.dao.BatchStepExecutionDao;
import id.co.homecredit.jfs.cofin.core.model.BatchStepExecution;

/**
 * Dao implement class for {@link BatchStepExecution}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class BatchStepExecutionDaoImpl implements BatchStepExecutionDao {
    private static final Logger log = LogManager.getLogger(BatchStepExecutionDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    private Criteria createCriteria() {
        return getSession().createCriteria(BatchStepExecution.class);
    }

    @Override
    public BatchStepExecution get(Long stepExecutionId) {
        return (BatchStepExecution) getSession().get(BatchStepExecution.class, stepExecutionId);
    }

    @Override
    public List<BatchStepExecution> getBatchStepExecutionByJobExecutionId(Long jobExecutionId) {
        // TODO Auto-generated method stub
        log.debug("get step by jobExecutionId {}", jobExecutionId);
        Criteria criteria = createCriteria();
        criteria.createAlias("batchJobExecution", "batchJobExecution");
        criteria.add(Restrictions.eq("batchJobExecution.jobExecutionId", jobExecutionId));
        criteria.add(Restrictions.ne("stepName", "continuity"));
        criteria.add(Restrictions.ne("stepName", "BTPN_daily_prerequisite"));
        criteria.add(Restrictions.ne("stepName", "PERMATA_daily_prerequisite"));
        criteria.add(Restrictions.ne("stepName", "BTPN_monthly_prerequisite"));
        criteria.add(Restrictions.ne("stepName", "PERMATA_monthly_prerequisite"));
        
        return criteria.list();
    }

    @Override
    public String getLastStepNonFlowByJobName(String jobName) {
        log.debug("get last step name exclude flow by job name {}", jobName);
        Criteria criteria = createCriteria();
        criteria.createAlias("batchJobExecution", "batchJobExecution");
        criteria.createAlias("batchJobExecution.batchJobInstance", "batchJobInstance");
        criteria.add(Restrictions.eq("batchJobInstance.jobName", jobName));
        criteria.add(Restrictions.ne("stepName", "flow"));
        criteria.setProjection(Projections.property("stepName"));
        criteria.addOrder(Order.desc("startTime"));
        criteria.setMaxResults(1);
        return (String) criteria.uniqueResult();
    }

    /**
     * Get current session.
     *
     * @return session
     */
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public BatchStepExecution save(BatchStepExecution batchStepExecution) {
        getSession().saveOrUpdate(batchStepExecution);
        return batchStepExecution;
    }

}
