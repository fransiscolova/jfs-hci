package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.CofinConfigDao;
import id.co.homecredit.jfs.cofin.core.model.CofinConfig;
import id.co.homecredit.jfs.cofin.common.model.DropDownListItem;

/**
 * Dao implement class for {@link CofinConfig}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
@SuppressWarnings("unchecked")
public class CofinConfigDaoImpl extends DaoImpl<CofinConfig, String> implements CofinConfigDao {
    private static final Logger log = LogManager.getLogger(CofinConfigDaoImpl.class);


	@Override
	public List<CofinConfig> getByCategory(String category) {
		log.debug("get cofinConfig by category {}", category);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("category", category));
        return  criteria.list();
	}

	@Override
	public List<CofinConfig> getByCategoryKey1(String category, String key1) {
		log.debug("get cofinConfig by category {}", category);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("category", category));
        criteria.add(Restrictions.eq("key01", key1));      
        return  criteria.list();
	}

	@Override
	public String getByCategoryKey2(String category,String key01,String key02){
		log.debug("Get Configuration By Category = {}, Key01 = {}, key02 = {}",category,key01,key02);
		Criteria criteria = createCriteria();
		criteria.add(Restrictions.eq("category", category));
        criteria.add(Restrictions.eq("key01", key01));
        criteria.add(Restrictions.eq("key02", key02));
        List<CofinConfig> conf = criteria.list();
        String value = conf.get(0).getValue();
        return value;        
	}
	
	@Override
	public List<DropDownListItem> getJfsContractStatus(){
		log.debug("Get Value Of JFS Contract Status");
		Criteria crt = createCriteria();
		crt.add(Restrictions.eq("category","JFS_CONTRACT"));
		crt.add(Restrictions.eq("key01","STATUS"));
		List<CofinConfig> config = crt.list();
		List<DropDownListItem> list = new ArrayList<DropDownListItem>();
		for(int x=0;x<config.size();x++){
			list.add(new DropDownListItem(config.get(x).getDesc01(),config.get(x).getValue()));
		}
		return list;
	}

	@Override
	public CofinConfig getDescriptionByValue(String value){
		log.debug("Get Description By Value {}",value);
		Criteria crt = createCriteria();
		crt.add(Restrictions.eq("value",value));
		List<CofinConfig> config = crt.list();
		if(config.size()==0){
			return null;
		}else{
			return config.get(0);
		}
	}

}
