package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ComCategoryMappingDao;
import id.co.homecredit.jfs.cofin.core.model.ComCategoryMapping;

/**
 * Dao implement class for {@link ComCategoryMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ComCategoryMappingDaoImpl extends DaoImpl<ComCategoryMapping, String>
        implements ComCategoryMappingDao {
    private static final Logger log = LogManager.getLogger(ComCategoryMappingDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<ComCategoryMapping> getAllComCategoryMappingsByPartnerId(String partnerId) {
        log.debug("get all com category mappings by partner id {]", partnerId);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("partner.id", partnerId));
        return criteria.list();
    }

}
