package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ComTypeMappingDao;
import id.co.homecredit.jfs.cofin.core.model.ComTypeMapping;

/**
 * Dao implement class for {@link ComTypeMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ComTypeMappingDaoImpl extends DaoImpl<ComTypeMapping, String>
        implements ComTypeMappingDao {
    private static final Logger log = LogManager.getLogger(ComTypeMappingDaoImpl.class);

}
