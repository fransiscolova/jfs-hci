package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.CommodityCategoryDao;
import id.co.homecredit.jfs.cofin.core.model.CommodityCategory;

/**
 * Dao implement class for {@link CommodityCategory}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class CommodityCategoryDaoImpl extends DaoImpl<CommodityCategory, String>
        implements CommodityCategoryDao {
    private static final Logger log = LogManager.getLogger(CommodityCategoryDaoImpl.class);

}
