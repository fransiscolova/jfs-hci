package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.CommodityTypeDao;
import id.co.homecredit.jfs.cofin.core.model.CommodityType;

/**
 * Dao implement class for {@link CommodityType}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class CommodityTypeDaoImpl extends DaoImpl<CommodityType, String>
        implements CommodityTypeDao {
    private static final Logger log = LogManager.getLogger(CommodityTypeDaoImpl.class);

}
