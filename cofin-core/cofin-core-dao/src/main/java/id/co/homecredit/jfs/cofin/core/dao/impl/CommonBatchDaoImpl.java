package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.core.dao.CommonBatchDao;

/**
 * Dao implement class for executing native query.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class CommonBatchDaoImpl implements CommonBatchDao {
    private static final Logger log = LogManager.getLogger(CommonBatchDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Convert java type to hibernate type.
     *
     * @param clazz
     * @return basicType
     */
    @SuppressWarnings("rawtypes")
    private AbstractSingleColumnStandardBasicType convertType(Class<?> clazz) {
        if (clazz.equals(BigDecimal.class)) {
            return BigDecimalType.INSTANCE;
        } else if (clazz.equals(Date.class)) {
            return DateType.INSTANCE;
        } else if (clazz.equals(Integer.class)) {
            return IntegerType.INSTANCE;
        } else if (clazz.equals(Long.class)) {
            return LongType.INSTANCE;
        }
        return StringType.INSTANCE;
    }

    /**
     * Create SQL query from string.
     *
     * @param query
     * @return sqlQuery
     */
    private SQLQuery createSqlQuery(String query) {
        log.debug("create sql query using : {}", query);
        return getSession().createSQLQuery(query);
    }

    @Override
    public Integer executeProcedureWithParam(String query, Object param) {
        log.debug("param {}", param);
        SQLQuery sqlQuery = createSqlQuery(query);
        sqlQuery.setParameter(0, param);
        return sqlQuery.executeUpdate();
    }

    @Override
    public Object executeQuery(String query) {
        return createSqlQuery(query).list();
    }

    @Override
    public Object executeQueryWithTransformer(String query, Class<?> clazz) {
        log.debug("create query and transform to {}", clazz);
        SQLQuery sqlQuery = createSqlQuery(query);
        setProjectionToScalar(sqlQuery, clazz);
        sqlQuery.setResultTransformer(Transformers.aliasToBean(clazz));
        return sqlQuery.list();
    }

    @Override
    public Integer executeUpdate(String query) {
        return createSqlQuery(query).executeUpdate();
    }

    /**
     * Get current session.
     *
     * @return session
     */
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Set SQL query projection to scalar.
     *
     * @param query
     * @param clazz
     */
    private void setProjectionToScalar(SQLQuery query, Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            log.debug("field name {} with type {}", field.getName(), field.getType());
            query.addScalar(field.getName(), convertType(field.getType()));
        }
    }

}
