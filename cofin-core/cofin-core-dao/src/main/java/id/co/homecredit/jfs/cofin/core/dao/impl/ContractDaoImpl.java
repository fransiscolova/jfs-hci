package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.model.enumeration.ContractStatusEnum;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.dao.ContractDao;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.Contract;
import id.co.homecredit.jfs.cofin.core.model.Partner;

/**
 * Dao implement class for {@link Contract}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ContractDaoImpl extends DaoImpl<Contract, String> implements ContractDao {
    private static final Logger log = LogManager.getLogger(ContractDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<Contract> getAllContractsInquiry(String contractCode, ContractStatusEnum status,
            Partner partner, Agreement agreement, JSONObject dateFilter) throws Exception {

        Criteria criteria = composeCriteria(true);

        try {
            Date bankClawBackFrom = DateUtil
                    .stringToDateWithFormat(dateFilter.getString("clawBackDateFrom"), "MM/dd/yyyy");
            Date bankClawBackTo = DateUtil
                    .stringToDateWithFormat(dateFilter.getString("clawBackDateTo"), "MM/dd/yyyy");

            criteria.add(Restrictions.ge("bankClawbackDate", bankClawBackFrom));
            criteria.add(Restrictions.le("bankClawbackDate", bankClawBackTo));
        } catch (Exception e) {
            log.debug("empty clawback date");
        }

        try {
            Date creationDateFrom = DateUtil
                    .stringToDateWithFormat(dateFilter.getString("creationDateFrom"), "MM/dd/yyyy");
            Date creationDateTo = DateUtil
                    .stringToDateWithFormat(dateFilter.getString("creationDateTo"), "MM/dd/yyyy");

            criteria.add(Restrictions.ge("createdDate", creationDateFrom));
            criteria.add(Restrictions.le("createdDate", creationDateTo));
        } catch (Exception e) {
            log.debug("empty create date");
        }

        if (!contractCode.trim().isEmpty()) {
            log.debug("contract code" + contractCode);
            criteria.add(Restrictions.eq("contractNumber", contractCode));
        }

        if (!status.getDescription().equalsIgnoreCase("Select Status")) {
            log.debug("status" + status);
            criteria.add(Restrictions.eq("status", status));
        }

        if (partner != null) {
            log.debug("partner" + partner.getId());
            criteria.createAlias("agreement.partner", "partner");
            criteria.add(Restrictions.eq("partner.id", partner.getId()));
        }

        if (agreement != null) {
            log.debug("agreementid" + agreement.getId());
            criteria.add(Restrictions.eq("agreement.id", agreement.getId()));
        }

        return criteria.list();
    }

    @Override
    public Contract getContractByContractNumber(String contractNumber) {
        log.debug("get contract by contract number {}", contractNumber);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("contractNumber", contractNumber));
        return (Contract) criteria.uniqueResult();
    }
    
}
