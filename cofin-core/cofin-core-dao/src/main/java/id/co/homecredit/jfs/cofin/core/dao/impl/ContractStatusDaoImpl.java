package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ContractStatusDao;
import id.co.homecredit.jfs.cofin.core.model.ContractStatus;

/**
 * Dao implement class for {@link ContractStatus}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ContractStatusDaoImpl extends DaoImpl<ContractStatus, String>
        implements ContractStatusDao {
    private static final Logger log = LogManager.getLogger(ContractStatusDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<ContractStatus> getAllContractStatusesByContractNumber(String contractNumber) {
        log.debug("get all contract statuses by contract number {}", contractNumber);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("contractNumber", contractNumber));
        return criteria.list();
    }

}
