package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.CriteriaEligibilityDao;
import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;

/**
 * Dao Implement class for {@link CriteriaEligibility}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class CriteriaEligibilityDaoImpl extends DaoImpl<CriteriaEligibility, String>
        implements CriteriaEligibilityDao {
    private static final Logger log = LogManager.getLogger(CriteriaEligibilityDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<CriteriaEligibility> getAllCriteriaEligibilitiesByAgreementId(String agreementId) {
        log.debug("get all criteria eligibilities by agreement id {}", agreementId);
        Criteria criteria = createCriteria();
        criteria.createAlias("criteriaMapping.agreement", "agreement");
        criteria.add(Restrictions.eq("agreement.id", agreementId));
        return criteria.list();
    }

}
