package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.CriteriaMappingDao;
import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;

/**
 * Dao implement class for {@link CriteriaMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class CriteriaMappingDaoImpl extends DaoImpl<CriteriaMapping, String>
        implements CriteriaMappingDao {
    private static final Logger log = LogManager.getLogger(CriteriaMappingDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<CriteriaMapping> getAllCriteriaMappingsByAgreementId(String agreementId) {
        log.debug("get all criteria mappings by agreement id {}", agreementId);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("agreement.id", agreementId));
        criteria.add(Restrictions.isNull("criteriaMappingParent"));
        criteria.addOrder(Order.asc("criteriaMappingParent"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CriteriaMapping> getAllCriteriaMappingsByCriteria(CriteriaEnum criteiraEnum) {
        log.debug("get all criteria mappings by criteria enum {}", criteiraEnum);
        Criteria criteria = createCriteria();
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("criteria", criteiraEnum));
        criteria.addOrder(Order.asc("partner.priority"));
        criteria.addOrder(Order.asc("criteriaMappingParent"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CriteriaMapping> getAllCriteriaMappingsByDate(Date date) {
        log.debug("get all criteria mappings by date {}", date);
        Criteria criteria = createCriteria();
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.le("validFrom", date));
        criteria.add(Restrictions.ge("validTo", date));
        criteria.addOrder(Order.asc("partner.priority"));
        criteria.addOrder(Order.asc("criteriaMappingParent"));
        return criteria.list();
    }

}
