package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.EconomicSectorDao;
import id.co.homecredit.jfs.cofin.core.model.EconomicSector;

/**
 * Dao implement class for {@link EconomicSector}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class EconomicSectorDaoImpl extends DaoImpl<EconomicSector, String>
        implements EconomicSectorDao {
    private static final Logger log = LogManager.getLogger(EconomicSectorDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<EconomicSector> getAllEconomicSectorsByPartnerId(String partnerId) {
        log.debug("get all economic sectors by partner id {}", partnerId);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("partner.id", partnerId));
        return criteria.list();
    }

    @Override
    public EconomicSector getEconomicSectorByBankCommodityGroupAndBIEconomicSectorCode(
            String bankCommodityGroup, String biEconomicSectorCode) {
        log.debug("get economic sector by bank commodity group {} and BI economic sector code {}",
                biEconomicSectorCode, bankCommodityGroup);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("bankCommodityGroup", bankCommodityGroup));
        criteria.add(Restrictions.eq("biEconomicSectorCode", biEconomicSectorCode));
        return (EconomicSector) criteria.uniqueResult();
    }

    @Override
    public EconomicSector getEconomicSectorByPartnerIdAndBankCommodityGroup(String partnerId,
            String bankCommodityGroup) {
        log.debug("get economic sector by partner id {} and bank commodity group {}", partnerId,
                bankCommodityGroup);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("partner.id", partnerId));
        criteria.add(Restrictions.eq("bankCommodityGroup", bankCommodityGroup));
        return (EconomicSector) criteria.uniqueResult();
    }

}
