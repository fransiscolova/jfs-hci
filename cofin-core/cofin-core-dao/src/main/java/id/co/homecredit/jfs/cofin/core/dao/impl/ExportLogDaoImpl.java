package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.LogDaoImpl;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.dao.ExportLogDao;
import id.co.homecredit.jfs.cofin.core.model.ExportLog;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;

/**
 * Dao implement class for {@link ExportLog}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ExportLogDaoImpl extends LogDaoImpl<ExportLog, String> implements ExportLogDao {
    private static final Logger log = LogManager.getLogger(ExportLogDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<ExportLog> getExportLogByStepExecutionId(Long stepExecutionId) {
        log.debug("get all export log by step execution id {}", stepExecutionId);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("stepExecutionId", stepExecutionId));
        return criteria.list();
    }

    @Override
    public ExportLog getLatestExportLogByStepExecutionIdAndAgreementTypeAndDate(
            Long stepExecutionId, AgreementTypeEnum agreementType, Date date)
            throws ParseException {
        log.debug("get latest export log by step execution id {}, agreement type {}, and date {}",
                stepExecutionId, agreementType, date);
        Date minDate = DateUtil.dateToDate(date);
        Date maxDate = DateUtil.datePlusOneDay(minDate);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("stepExecutionId", stepExecutionId));
        criteria.add(Restrictions.eq("agreementType", agreementType));
        criteria.add(Restrictions.ge("createdDate", minDate));
        criteria.add(Restrictions.lt("createdDate", maxDate));
        criteria.addOrder(Order.desc("createdDate"));
        criteria.setMaxResults(1);
        return (ExportLog) criteria.uniqueResult();
    }

    @Override
    public ExportLog saveWithNewPropagation(ExportLog exportLog) {
        log.debug("save {}", exportLog.toString());
        return super.save(exportLog);
    }

}
