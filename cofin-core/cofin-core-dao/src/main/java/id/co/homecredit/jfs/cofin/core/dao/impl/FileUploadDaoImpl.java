package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.FileUploadDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;

/**
 * Dao implement class for {@link Action}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class FileUploadDaoImpl extends DaoImpl<FileUpload, String> implements FileUploadDao {
	private static final Logger log = LogManager.getLogger(FileUploadDaoImpl.class);

	@Override
	public List<FileUpload> getFileUpload(String processDate, String confirmFile) {
		log.debug("get fileupload  by processDate ,confirmFile {}", processDate, confirmFile);
		Criteria criteria = createCriteria();
		criteria.createAlias("cofinConfirmFile", "cofinConfirmFile");

		try {
			criteria.add(Restrictions.eq("processDate", DateUtil.stringToDateWithFormat(processDate,"MM/dd/yyyy")));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		criteria.add(Restrictions.eq("cofinConfirmFile.id", confirmFile));

		return criteria.list();

	}

}
