package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.ImportTempFileDao;
import id.co.homecredit.jfs.cofin.core.dao.TempJfsConfirmationFileDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.temp.ImportTempFile;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Dao implement class for {@link Action}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class ImportTempFileDaoImpl extends DaoIdOnlyImpl<ImportTempFile, String> implements ImportTempFileDao {
    private static final Logger log = LogManager.getLogger(ImportTempFileDaoImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Override
    public void runProcedurePermata(String query) {
        log.debug("run procedure {}", query);
        Query querys =getSession().createSQLQuery(query);       
        querys.executeUpdate();
        log.debug("finish run procedure {}", query);
    }


   /**
    * Get current session.
    *
    * @return session
    */
   private Session getSession() {
       return sessionFactory.getCurrentSession();
   }
}
