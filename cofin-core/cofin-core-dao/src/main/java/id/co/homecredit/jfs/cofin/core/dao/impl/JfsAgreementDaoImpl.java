package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.Logger;
import java.util.Map;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.ArrayList;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.dao.JfsAgreementDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.JfsAgreement;


/**
 * Dao Implement Class For {@link JfsAgreement}
 * 
 * @author denny.afrizal01
 *
 */
@Repository
public class JfsAgreementDaoImpl extends DaoIdOnlyImpl<JfsAgreement,String> implements JfsAgreementDao{
	private static final Logger log = LogManager.getLogger(JfsAgreementDaoImpl.class);

	@Override
	public JfsAgreement getJfsAgreementById(Long agreementId){
		log.debug("Get JFS Agreement By ID {}",agreementId);
		Criteria crt = createCriteria();
		crt.add(Restrictions.eq("idAgreement",agreementId));
		List<JfsAgreement> agree = crt.list();
		if(agree.size()==0){
			return null;
		}else{
			return agree.get(0);
		}
	}
	
//	@Override
//	public JfsAgreement getJfsAgreementById(Long agreementId) throws ParseException{
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//		log.debug("Get JFS Agreement By ID {}",agreementId);
//		Session session = sfJFS.openSession();
//		Query query = session.createSQLQuery("select * from JFS_AGREEMENT where ID_AGREEMENT = '"+agreementId+"'");
//		log.debug(query.getQueryString());
//    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//    	List<Map<String,Object>> daftar = query.list();
//		JfsAgreement agree = new JfsAgreement();
//		if(daftar.size()>0){
//			agree.setCodePartner(daftar.get(0).get("CODE_PARTNER").toString());
//	    	agree.setCodeAgreement(daftar.get(0).get("CODE_AGREEMENT").toString());
//	    	agree.setNameAgreement(daftar.get(0).get("NAME_AGREEMENT").toString());
//	    	agree.setValidFrom(format.parse(daftar.get(0).get("VALID_FROM").toString().substring(0,10)));
//	    	agree.setValidTo(format.parse(daftar.get(0).get("VALID_TO").toString().substring(0,10)));
//	    	agree.setBankCode(daftar.get(0).get("BANK_CODE").toString());
//	    	agree.setIdAgreement(Long.parseLong(daftar.get(0).get("ID_AGREEMENT").toString()));
//	    	agree.setPartnerId(daftar.get(0).get("PARTNER_ID").toString());
//	    	agree.setIsDelete(daftar.get(0).get("IS_DELETE").toString());
//	    	if(daftar.get(0).get("INT_RATE")==null){
//	    		agree.setIntRate(0.0);
//	    	}else{
//	    		agree.setIntRate(Double.parseDouble(daftar.get(0).get("INT_RATE").toString()));
//	    	}
//	    	if(daftar.get(0).get("PRIN_SPLIT_RATE")==null){
//	    		agree.setPrinSplitRate(0.0);
//	    	}else{
//	    		agree.setPrinSplitRate(Double.parseDouble(daftar.get(0).get("PRIN_SPLIT_RATE").toString()));
//	    	}
//	    	if(daftar.get(0).get("ADM_FEE_RATE")==null){
//	    		agree.setAdmFeeRate(0.0);
//	    	}else{
//	    		agree.setAdmFeeRate(Double.parseDouble(daftar.get(0).get("ADM_FEE_RATE").toString()));
//	    	}
//	    	if(daftar.get(0).get("DESCRIPTION")==null){
//	    		agree.setDescription("");
//	    	}else{
//	    		agree.setDescription(daftar.get(0).get("DESCRIPTION").toString());
//	    	}
//	    	if(daftar.get(0).get("CREATED_BY")==null){
//	    		agree.setCreatedBy("cofin_system");
//	    	}else{
//	    		agree.setCreatedBy(daftar.get(0).get("CREATED_BY").toString());
//	    	}
//	    	if(daftar.get(0).get("CREATED_DATE")==null){
//	    		agree.setCreatedDate(new Date());
//	    	}else{
//	    		agree.setCreatedDate(format.parse(daftar.get(0).get("CREATED_DATE").toString().substring(0,10)));
//	    	}
//	    	if(daftar.get(0).get("UPDATED_BY")==null){
//	    		agree.setUpdatedBy("cofin_system");
//	    	}else{
//	    		agree.setUpdatedBy(daftar.get(0).get("UPDATED_BY").toString());
//	    	}
//	    	if(daftar.get(0).get("UPDATED_DATE")==null){
//	    		agree.setUpdatedDate(new Date());
//	    	}else{
//	    		agree.setUpdatedDate(format.parse(daftar.get(0).get("UPDATED_DATE").toString().substring(0,10)));
//	    	}
//	    	agree.setId(daftar.get(0).get("ID").toString());
//	    	agree.setFkAgreementType(daftar.get(0).get("FK_AGREEMENT_TYPE").toString());
//			session.close();
//			return agree;
//		}else{
//			session.close();
//			return null;
//		}
//	}

	@Override
	public List<JfsAgreement> getJfsAgreementByPartnerId(String partnerId){
		log.debug("Get Value Of JFS Agreement By Partner ID {}",partnerId);
		Criteria crt = createCriteria();
		crt.add(Restrictions.eq("partnerId",partnerId));
		crt.addOrder(Order.asc("idAgreement"));
		return crt.list();
	}
	
//	@Override
//	public List<JfsAgreement> getJfsAgreementByPartnerId(String partnerId)throws Exception{
//		log.debug("Get Value Of JFS Agreement By Partner ID {}",partnerId);
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		String xquery = "select * from JFS_AGREEMENT where PARTNER_ID = '"+partnerId+"' order by ID_AGREEMENT";
//		Session session = sfJFS.openSession();
//		Query query = session.createSQLQuery(xquery);
//		log.debug(query.getQueryString());
//		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//    	List<Map<String,Object>> hasil = query.list();
//    	List<JfsAgreement> agreement = new ArrayList<JfsAgreement>();
//    	for(int x=0;x<hasil.size();x++){
//    		JfsAgreement agree = new JfsAgreement();
//    		agree.setCodePartner(hasil.get(x).get("CODE_PARTNER").toString());
//    		agree.setCodeAgreement(hasil.get(x).get("CODE_AGREEMENT").toString());
//    		agree.setNameAgreement(hasil.get(x).get("NAME_AGREEMENT").toString());
//    		agree.setValidFrom(sdf.parse(hasil.get(x).get("VALID_FROM").toString()));
//    		agree.setValidTo(sdf.parse(hasil.get(x).get("VALID_TO").toString()));
//    		agree.setBankCode(hasil.get(x).get("BANK_CODE").toString());
//    		agree.setIdAgreement(Integer.parseInt(hasil.get(x).get("ID_AGREEMENT").toString()));
//    		agree.setPartnerId(partnerId);
//    		agree.setIsDelete(YesNoEnum.valueOf(hasil.get(x).get("IS_DELETE").toString()));
//    		if(hasil.get(x).get("INT_RATE")==null){
//	    		agree.setIntRate(0.0);
//	    	}else{
//	    		agree.setIntRate(Double.parseDouble(hasil.get(x).get("INT_RATE").toString()));
//	    	}
//	    	if(hasil.get(x).get("PRIN_SPLIT_RATE")==null){
//	    		agree.setPrinSplitRate(0.0);
//	    	}else{
//	    		agree.setPrinSplitRate(Double.parseDouble(hasil.get(x).get("PRIN_SPLIT_RATE").toString()));
//	    	}
//	    	if(hasil.get(x).get("ADM_FEE_RATE")==null){
//	    		agree.setAdmFeeRate(0.0);
//	    	}else{
//	    		agree.setAdmFeeRate(Double.parseDouble(hasil.get(x).get("ADM_FEE_RATE").toString()));
//	    	}
//	    	if(hasil.get(x).get("DESCRIPTION")==null){
//	    		agree.setDescription("");
//	    	}else{
//	    		agree.setDescription(hasil.get(x).get("DESCRIPTION").toString());
//	    	}
//	    	if(hasil.get(x).get("CREATED_BY")==null){
//	    		agree.setCreatedBy("cofin_system");
//	    	}else{
//	    		agree.setCreatedBy(hasil.get(x).get("CREATED_BY").toString());
//	    	}
//	    	if(hasil.get(x).get("CREATED_DATE")==null){
//	    		agree.setCreatedDate(new Date());
//	    	}else{
//	    		agree.setCreatedDate(sdf.parse(hasil.get(x).get("CREATED_DATE").toString().substring(0,10)));
//	    	}
//	    	if(hasil.get(x).get("UPDATED_BY")==null){
//	    		agree.setUpdatedBy("cofin_system");
//	    	}else{
//	    		agree.setUpdatedBy(hasil.get(x).get("UPDATED_BY").toString());
//	    	}
//	    	if(hasil.get(x).get("UPDATED_DATE")==null){
//	    		agree.setUpdatedDate(new Date());
//	    	}else{
//	    		agree.setUpdatedDate(sdf.parse(hasil.get(x).get("UPDATED_DATE").toString().substring(0,10)));
//	    	}
//	    	agree.setId(hasil.get(x).get("ID").toString());
//	    	agree.setFkAgreementType(hasil.get(x).get("FK_AGREEMENT_TYPE").toString());
//    		agreement.add(agree);
//    	}
//    	session.close();
//    	return agreement;
//	}
	
	@Override
	public String getMaxIdAgreement(){
		log.debug("Get Max Value Of Agreement ID");
		String sql = "select max(ID_AGREEMENT) as AGREEMENT_ID from JFS_AGREEMENT";
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery(sql);
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		String xid = "";
		if(list.size()==0){
			xid = "0";
		}else{
			xid = list.get(0).get("AGREEMENT_ID").toString();
		}
		session.close();
		return xid;
	}
	
}
