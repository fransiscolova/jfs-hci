package id.co.homecredit.jfs.cofin.core.dao.impl;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.text.DecimalFormat;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.model.JfsContract;
import id.co.homecredit.jfs.cofin.core.dao.JfsContractDao;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.StringChar;

/**
 * Service implement class for {@link JfsContract}.
 *
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class JfsContractDaoImpl implements JfsContractDao{
	public static final Logger log = LogManager.getLogger(JfsContractDaoImpl.class);
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }

    @Override
	public List<JfsContract> getJfsContractWithoutDate(String contractNumber,String status,Long idAgreement){
		log.debug("Get Value Of JFS Contract Without Date Criteria");
    	Session session = sfJFS.openSession();
    	Criteria crt = session.createCriteria(JfsContract.class);
    	crt.add(Restrictions.like("textContractNumber",contractNumber,MatchMode.ANYWHERE));
		crt.add(Restrictions.like("status",status,MatchMode.ANYWHERE));
		if(idAgreement > 0){
			crt.add(Restrictions.eq("idAgreement",idAgreement));
		}
    	crt.addOrder(Order.desc("exportDate"));
		crt.setMaxResults(5000);
    	return crt.list();
	}

	@Override
	public List<JfsContract> getJfsContractWithDate(String textContractNumber,String status,Long idAgreement,Date exportDate){
		log.debug("Get Value Of JFS Contract With Date Criteria");
		Date startDate = DateUtil.getStartDate(exportDate);
		Date endDate = DateUtil.getEndDate(exportDate);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsContract.class);
		crt.add(Restrictions.like("textContractNumber",textContractNumber,MatchMode.ANYWHERE));
		crt.add(Restrictions.like("status",status,MatchMode.ANYWHERE));
		if(idAgreement > 0){
			crt.add(Restrictions.eq("idAgreement",idAgreement));
		}
		crt.add(Restrictions.ge("exportDate",startDate));
		crt.add(Restrictions.le("exportDate",endDate));
		crt.addOrder(Order.desc("exportDate"));
		crt.setMaxResults(5000);
		return crt.list();
	}

	@Override
	public List<JfsContract> getJfsContractBetweenDate(String textContractNumber,String status,Long idAgreement,Date dateFrom,Date dateTo){
		log.debug("Get Value Of JFS Contract With Between Date Criteria");
		Date startDate = DateUtil.getStartDate(dateFrom);
		Date endDate = DateUtil.getEndDate(dateTo);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsContract.class);
		crt.add(Restrictions.like("textContractNumber",textContractNumber,MatchMode.ANYWHERE));
		crt.add(Restrictions.like("status",status,MatchMode.ANYWHERE));
		if(idAgreement > 0){
			crt.add(Restrictions.eq("idAgreement",idAgreement));
		}
		crt.add(Restrictions.ge("exportDate",startDate));
		crt.add(Restrictions.le("exportDate",endDate));
		crt.addOrder(Order.desc("exportDate"));
		crt.setMaxResults(5000);
		return crt.list();
	}

	@Override
	public JfsContract getHeaderJfsContract(String textContractNumber){
		log.debug("Get Header Value Of JFS Contract By ID {}",textContractNumber);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsContract.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		List<JfsContract> cont = crt.list();
		if(cont.size()==0){
			return null;
		}else{
			return cont.get(0);
		}
	}
    
//    @Override
//    public JfsContract getHeaderJfsContract(String contractNumber){
//    	log.debug("Get Header Value Of JFS Contract By ID {}",contractNumber);
//    	String sql = "select a.TEXT_CONTRACT_NUMBER,b.CODE_AGREEMENT,a.CLIENT_NAME,a.SEND_TENOR,a.CUID,"
//    				 +"case when a.STATUS = 'A' then 'ACTIVE' else 'INACTIVE' end as STATUS,a.BANK_CLAWBACK_DATE,"
//    				 +"(select DESC_01 from COFIN_CONFIG where value = a.STATUS) as JFSSTATUS,a.BANK_CLAWBACK_AMOUNT,"
//    				 +"a.EXPORT_DATE,a.DATE_FIRST_DUE,a.AMT_INSTALMENT,a.ID_AGREEMENT,a.BANK_DECISION_DATE,"
//    				 +"(select DUE_DATE from JFS_SCHEDULE where TEXT_CONTRACT_NUMBER = a.TEXT_CONTRACT_NUMBER "
//    				 +"and PART_INDEX = a.SEND_TENOR) as DATE_LAST_DUE "
//    				 +"from JFS_CONTRACT a LEFT OUTER JOIN JFS_AGREEMENT b ON a.ID_AGREEMENT = b.ID_AGREEMENT "
//    				 +"where a.TEXT_CONTRACT_NUMBER = '"+contractNumber+"'";
//    	DecimalFormat rph = new DecimalFormat("###,###,###,###,###");
//    	Session session = sfJFS.openSession();
//    	Query query = session.createSQLQuery(sql);
//    	log.debug(query.getQueryString());
//    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//    	List<Map<String,Object>> hasil = query.list();
//    	JfsContract contract = new JfsContract();
//    	if(hasil.get(0).get("CUID")==null){
//    		contract.setCuid("-");
//    	}else{
//    		contract.setCuid(hasil.get(0).get("CUID").toString());
//    	}
//    	contract.setTextContractNumber(contractNumber);
//    	contract.setSkpContract(hasil.get(0).get("CODE_AGREEMENT").toString());
//    	contract.setClientName(hasil.get(0).get("CLIENT_NAME").toString());
//    	contract.setSendTenor(hasil.get(0).get("SEND_TENOR").toString());
//    	contract.setReason(hasil.get(0).get("STATUS").toString());
//    	contract.setStatus(hasil.get(0).get("JFSSTATUS").toString());
//    	contract.setExportDate(hasil.get(0).get("EXPORT_DATE").toString().substring(0,10));
//    	contract.setDateFirstDue(hasil.get(0).get("DATE_FIRST_DUE").toString().substring(0,10));
//    	contract.setAmtInstallment(rph.format(Double.parseDouble(hasil.get(0).get("AMT_INSTALMENT").toString())).replace(",","."));
//    	contract.setRejectReason(hasil.get(0).get("DATE_LAST_DUE").toString().substring(0,10));
//    	contract.setIdAgreement(hasil.get(0).get("ID_AGREEMENT").toString());
//    	if(hasil.get(0).get("BANK_DECISION_DATE")==null){
//    		contract.setSendPrincipal("-");
//    		contract.setBankDecisionDate("-");
//    	}else{
//    		contract.setSendPrincipal("Disbursed");
//    		contract.setBankDecisionDate(hasil.get(0).get("BANK_DECISION_DATE").toString().substring(0,10));
//    	}
//    	if(hasil.get(0).get("BANK_CLAWBACK_DATE")==null){
//    		contract.setBankClawbackDate("-");
//    	}else{
//    		contract.setBankClawbackDate(hasil.get(0).get("BANK_CLAWBACK_DATE").toString().substring(0,10));
//    	}
//    	if(hasil.get(0).get("BANK_CLAWBACK_AMOUNT")==null){
//    		contract.setBankClawbackAmount("0");
//    	}else{
//    		contract.setBankClawbackAmount(rph.format(Double.parseDouble(hasil.get(0).get("BANK_CLAWBACK_AMOUNT").toString())).replace(",","."));
//    	}
//    	session.close();
//    	return contract;
//    }
//
//    @Override
//	public JfsContract getDetailJfsContract(String textContractNumber){
//    	log.debug("Get Detail Of JFS Contract By Text Contrac {}",textContractNumber);
//    	Session session = sfJFS.openSession();
//    	Query query = session.createSQLQuery("select * from JFS_CONTRACT where TEXT_CONTRACT_NUMBER = '"+textContractNumber+"'");
//    	log.debug(query.getQueryString());
//    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//    	List<Map<String,Object>> hasil = query.list();
//    	session.close();
//    	if(hasil.size()==0){
//    		return null;
//		}else{
//    		List<JfsContract> kontrak = new ArrayList<JfsContract>();
//			for(int x=0;x<hasil.size();x++){
//				JfsContract kon = new JfsContract();
//				kon.setTextContractNumber(hasil.get(x).get("TEXT_CONTRACT_NUMBER").toString());
//				kon.setStatus(hasil.get(x).get("STATUS").toString());
//				kon.setExportDate(hasil.get(x).get("EXPORT_DATE").toString().substring(0,10));
//				if(hasil.get(x).get("SKP_CONTRACT")==null){
//					kon.setSkpContract("-");
//				}else{
//					kon.setSkpContract(hasil.get(x).get("SKP_CONTRACT").toString());
//				}
//				if(hasil.get(x).get("SEND_PRINCIPAL")==null){
//					kon.setSendPrincipal("-");
//				}else{
//					kon.setSendPrincipal(hasil.get(x).get("SEND_PRINCIPAL").toString());
//				}
//				if(hasil.get(x).get("SEND_INSTALMENT")==null){
//					kon.setSendInstalment("-");
//				}else{
//					kon.setSendInstalment(hasil.get(x).get("SEND_INSTALMENT").toString());
//				}
//				if(hasil.get(x).get("SEND_TENOR")==null){
//					kon.setSendTenor("-");
//				}else{
//					kon.setSendTenor(hasil.get(x).get("SEND_TENOR").toString());
//				}
//				if(hasil.get(x).get("SEND_RATE")==null){
//					kon.setSendRate("-");
//				}else{
//					kon.setSendRate(hasil.get(x).get("SEND_RATE").toString());
//				}
//				if(hasil.get(x).get("DATE_FIRST_DUE")==null){
//					kon.setDateFirstDue("-");
//				}else{
//					kon.setDateFirstDue(hasil.get(x).get("DATE_FIRST_DUE").toString().substring(0,10));
//				}
//				if(hasil.get(x).get("ACCEPTED_DATE")==null){
//					kon.setAcceptedDate("-");
//				}else{
//					kon.setAcceptedDate(hasil.get(x).get("ACCEPTED_DATE").toString().substring(0,10));
//				}
//				if(hasil.get(x).get("REJECT_REASON")==null){
//					kon.setRejectReason("-");
//				}else{
//					kon.setRejectReason(hasil.get(x).get("REJECT_REASON").toString());
//				}
//				if(hasil.get(x).get("BANK_INTEERST_RATE")==null){
//					kon.setBankInterestRate("-");
//				}else{
//					kon.setBankInterestRate(hasil.get(x).get("BANK_INTEREST_RATE").toString());
//				}
//				if(hasil.get(x).get("BANK_PRINCIPAL")==null){
//					kon.setBankPrincipal("-");
//				}else{
//					kon.setBankPrincipal(hasil.get(x).get("BANK_PRINCIPAL").toString());
//				}
//				if(hasil.get(x).get("BANK_INTEREST")==null){
//					kon.setBankInterest("-");
//				}else{
//					kon.setBankInterest(hasil.get(x).get("BANK_INTEREST").toString());
//				}
//				if(hasil.get(x).get("BANK_PROVISION")==null){
//					kon.setBankProvision("-");
//				}else{
//					kon.setBankProvision(hasil.get(x).get("BANK_PROVISION").toString());
//				}
//				if(hasil.get(x).get("BANK_ADMIN_FEE")==null){
//					kon.setBankAdminFee("-");
//				}else{
//					kon.setBankAdminFee(hasil.get(x).get("BANK_ADMIN_FEE").toString());
//				}
//				if(hasil.get(x).get("BANK_INSTALLMENT")==null){
//					kon.setBankInstallment("-");
//				}else{
//					kon.setBankInstallment(hasil.get(x).get("BANK_INSTALLMENT").toString());
//				}
//				if(hasil.get(x).get("BANK_TENOR")==null){
//					kon.setBankTenor("-");
//				}else{
//					kon.setBankTenor(hasil.get(x).get("BANK_TENOR").toString());
//				}
//				if(hasil.get(x).get("BANK_SPLIT_RATE")==null){
//					kon.setBankSplitRate("-");
//				}else{
//					kon.setBankSplitRate(hasil.get(x).get("BANK_SPLIT_RATE").toString());
//				}
//				if(hasil.get(x).get("CLIENT_NAME")==null){
//					kon.setClientName("-");
//				}else{
//					kon.setClientName(hasil.get(x).get("CLIENT_NAME").toString());
//				}
//				if(hasil.get(x).get("AMT_INSTALMENT")==null){
//					kon.setAmtInstallment("-");
//				}else{
//					kon.setAmtInstallment(hasil.get(x).get("AMT_INSTALMENT").toString());
//				}
//				if(hasil.get(x).get("AMT_MONTHLY_FEE")==null){
//					kon.setAmtMonthlyFee("-");
//				}else{
//					kon.setAmtMonthlyFee(hasil.get(x).get("AMT_MONTHLY_FEE").toString());
//				}
//				kon.setIdAgreement(hasil.get(x).get("ID_AGREEMENT").toString());
//				if(hasil.get(x).get("REASON")==null){
//					kon.setReason("-");
//				}else{
//					kon.setReason(hasil.get(x).get("REASON").toString());
//				}
//				if(hasil.get(x).get("BANK_DECISION_DATE")==null){
//					kon.setBankDecisionDate("-");
//				}else{
//					kon.setBankDecisionDate(hasil.get(x).get("BANK_DECISION_DATE").toString());
//				}
//				if(hasil.get(x).get("BANK_CLAWBACK_DATE")==null){
//					kon.setBankClawbackDate("-");
//				}else{
//					kon.setBankClawbackDate(hasil.get(x).get("BANK_CLAWBACK_DATE").toString());
//				}
//				if(hasil.get(x).get("BANK_CLAWBACK_AMOUNT")==null){
//					kon.setBankClawbackAmount("-");
//				}else{
//					kon.setBankClawbackAmount(hasil.get(x).get("BANK_CLAWBACK_AMOUNT").toString());
//				}
//				kon.setBankProductCode(hasil.get(x).get("BANK_PRODUCT_CODE").toString());
//				if(hasil.get(x).get("BANK_REFERENCE_NO")==null){
//					kon.setBankReferenceNo("-");
//				}else{
//					kon.setBankReferenceNo(hasil.get(x).get("BANK_REFERENCE_NO").toString());
//				}
//				if(hasil.get(x).get("CUID")==null){
//					kon.setCuid("-");
//				}else{
//					kon.setCuid(hasil.get(x).get("CUID").toString());
//				}
//				kontrak.add(kon);
//			}
//			return kontrak.get(0);
//		}
//	}
	
}
