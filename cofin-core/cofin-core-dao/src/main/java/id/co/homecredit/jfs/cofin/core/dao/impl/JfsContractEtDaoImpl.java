package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.dao.JfsContractEtDao;
import id.co.homecredit.jfs.cofin.core.model.JfsContractEt;

/**
 * Dao Implement Class For {@link JfsContractEtDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class JfsContractEtDaoImpl implements JfsContractEtDao{
	private static final Logger log = LogManager.getLogger(JfsContractEtDaoImpl.class);
    
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }

    @Override
	public List<JfsContractEt> getContractEtByContractNumber(String contractNumber){
		log.debug("Get Value Of JFS Contract ET By Contract Number {}",contractNumber);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsContractEt.class);
		crt.add(Restrictions.eq("textContractNumber",contractNumber));
		crt.addOrder(Order.desc("dtimeCreated"));
		return crt.list();
	}
	
//    @Override
//    public List<JfsContractEt> getContractEtByContractNumber(String contractNumber){
//    	log.debug("Get Value Of JFS Contract ET By Contract Number {}",contractNumber);
//    	DecimalFormat dec = new DecimalFormat("###,###,###,###,###");
//    	String sql = "select a.TEXT_CONTRACT_NUMBER,a.DATE_ET,b.DATE_EXPORT,b.DATE_PAYMENT,b.DATE_BANK_PROCESS,"
//    				 +"CASE WHEN a.CNT_ET_DAYS <= 15 THEN 'Within 15' ELSE 'After' END as REQUESTTYPE,"
//    				 +"REPLACE(REPLACE(to_char(a.AMT_ET,'999,999,999,999,999'),',','.'),' ','') as AMT_ET,"
//    				 +"CASE WHEN (select count(DISTINCT TOTAL_AMT_PAYMENT) from JFS_PAYMENT_INT where STATUS = 'A' "
//    				 +"and PAYMENT_TYPE = 'T' and TEXT_CONTRACT_NUMBER = a.TEXT_CONTRACT_NUMBER) <= 1 THEN 'Full' ELSE 'Partial' END as TYPE,"
//    				 +"(select DESC_01 from COFIN_CONFIG where VALUE = b.STATUS) as STATUS "
//    				 +"from JFS_CONTRACT_ET a left outer join JFS_PAYMENT_INT b on a.INCOMING_PAYMENT_ID = b.INCOMING_PAYMENT_ID "
//    				 +"where a.TEXT_CONTRACT_NUMBER = '"+contractNumber+"' order by a.DTIME_CREATED desc";
//    	Session session = sfJFS.openSession();
//    	Query query = session.createSQLQuery(sql);
//    	log.debug(query.getQueryString());
//    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//    	List<Map<String,Object>> list = query.list();
//    	List<JfsContractEt> contEt = new ArrayList<JfsContractEt>();
//    	for(int x=0;x<list.size();x++){
//    		JfsContractEt cont = new JfsContractEt();
//    		cont.setTextContractNumber(String.valueOf(x+1));
//    		cont.setDateEt(list.get(x).get("DATE_ET").toString().substring(0,10));
//    		cont.setDateSigned(list.get(x).get("DATE_EXPORT").toString().substring(0,10));
//    		cont.setDateBankDisbursed(list.get(x).get("DATE_PAYMENT").toString().substring(0,10));
//    		cont.setDateBankPrinted(list.get(x).get("DATE_BANK_PROCESS").toString().substring(0,10));
//    		cont.setCntEtDays(list.get(x).get("REQUESTTYPE").toString());
//    		cont.setAmtEt(list.get(x).get("AMT_ET").toString());
//    		cont.setClientName(list.get(x).get("TYPE").toString());
//    		cont.setBankReferenceNo(list.get(x).get("STATUS").toString());
//    		contEt.add(cont);
//    	}
//    	session.close();
//    	return contEt;
//    }
    
}
