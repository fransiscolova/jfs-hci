package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.dao.JfsContractWoDao;
import id.co.homecredit.jfs.cofin.core.model.JfsContractWo;

/**
 * Dao Implement Class For {@link JfsContractWoDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class JfsContractWoDaoImpl implements JfsContractWoDao{
	private static final Logger log = LogManager.getLogger(JfsContractWoDaoImpl.class);
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }

    @Override
	public JfsContractWo getContractWoByContractNumber(String contractNumber){
		log.debug("Get Contract WO Value By Contract Number {}",contractNumber);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsContractWo.class);
		crt.add(Restrictions.eq("textContractNumber",contractNumber));
		List<JfsContractWo> wo = crt.list();
		if(wo.size()==0){
			return null;
		}else{
			return wo.get(0);
		}
	}
	
//	@Override
//	public JfsContractWo getContractWoByContractNumber(String contractNumber) throws ParseException{
//		log.debug("Get Contract WO Value By Contract Number {}",contractNumber);
//		Session session = sfJFS.openSession();
//		Query query = session.createSQLQuery("select * from JFS_CONTRACT_WO where TEXT_CONTRACT_NUMBER = '"+contractNumber+"'");
//		log.debug(query.getQueryString());
//		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//		DecimalFormat rph = new DecimalFormat("###,###,###,###,###");
//		List<Map<String,Object>> list = query.list();
//		JfsContractWo wo = new JfsContractWo();
//		if(list.size()==0){
//			wo.setTextContractNumber(contractNumber);
//			wo.setExportDate("-");
//			wo.setWoDate("-");
//			wo.setWoReason("-");
//			wo.setWoAmount("0.0");
//			wo.setOutstandingWoAmount("0.0");
//			wo.setStatus("-");
//			wo.setDtimeStatusUpdated("-");
//			wo.setDateBankProcess("-");
//		}else{
//			wo.setTextContractNumber(contractNumber);
//			wo.setExportDate(list.get(0).get("EXPORT_DATE").toString().substring(0,10));
//			if(list.get(0).get("WO_DATE")==null){
//				wo.setWoDate("-");
//			}else{
//				wo.setWoDate(list.get(0).get("WO_DATE").toString().substring(0,10));
//			}
//			if(list.get(0).get("WO_REASON")==null){
//				wo.setWoReason("-");
//			}else{
//				wo.setWoReason(list.get(0).get("WO_REASON").toString());
//			}
//			wo.setWoAmount(rph.format(Double.parseDouble(list.get(0).get("WO_AMOUNT").toString())));
//			wo.setOutstandingWoAmount(rph.format(Double.parseDouble(list.get(0).get("OUTSTANDING_WO_AMOUNT").toString())));
//			wo.setStatus(list.get(0).get("STATUS").toString());
//			if(list.get(0).get("DTIME_STATUS_UPDATED")==null){
//				wo.setDtimeStatusUpdated("-");
//			}else{
//				wo.setDtimeStatusUpdated(list.get(0).get("DTIME_STATUS_UPDATED").toString().substring(0,10));
//			}
//			if(list.get(0).get("DATE_BANK_PROCESS")==null){
//				wo.setDateBankProcess("-");
//			}else{
//				wo.setDateBankProcess(list.get(0).get("DATE_BANK_PROCESS").toString().substring(0,10));
//			}
//		}
//		session.close();
//		return wo;
//	}
	
}
