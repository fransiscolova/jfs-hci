package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.dao.JfsCustomerDao;
import id.co.homecredit.jfs.cofin.core.dao.service.CustomerServiceV5Service;
import id.co.homecredit.jfs.cofin.core.model.dto.JfsCustomerBsl;
import net.homecredit.hss.ws.customer.customer.v5.Customer;
import net.homecredit.hss.ws.customer.customer.v5.CustomerAdditionalData;
import net.homecredit.hss.ws.customer.customer.v5.CustomerAddress;
import net.homecredit.hss.ws.customer.customer.v5.CustomerBirth;
import net.homecredit.hss.ws.customer.customer.v5.CustomerEmployment;
import net.homecredit.hss.ws.customer.customer.v5.CustomerFinancialData;
import net.homecredit.hss.ws.customer.customer.v5.CustomerPersonalData;
import net.homecredit.hss.ws.customer.customer.v5.Document;
import net.homecredit.hss.ws.customer.customerservice.v5.GetCustomerRequest;
import net.homecredit.hss.ws.customer.customerservice.v5.GetCustomerResponse;

/**
 * Dao Implements Class For {@link JfsCustomerDao}
 * 
 * @author denny.afrizal01
 *
 */
@Repository
@Transactional
public class JfsCustomerDaoImpl implements JfsCustomerDao{
	private static final Logger log = LogManager.getLogger(JfsCustomerDaoImpl.class);
	
	private CustomerServiceV5Service customerServiceV5Service;
	
	@Autowired
	public JfsCustomerDaoImpl(CustomerServiceV5Service customerServiceV5Service){
		this.customerServiceV5Service=customerServiceV5Service;
	}
	
	@Override
	public JfsCustomerBsl getDetailCustomer(String cuid)throws MalformedURLException,RemoteException{
		log.debug("Get Detail Customer From BSL By CUID {}",cuid);
		JfsCustomerBsl bsl = new JfsCustomerBsl();
		GetCustomerRequest req = new GetCustomerRequest();
		req.setCuid(Long.valueOf(cuid));
		GetCustomerResponse serv = customerServiceV5Service.getCustomer(req);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat dec = new DecimalFormat("###,###,###,###,###");
		if(serv != null){
			Customer cust = serv.getCustomer();
			CustomerBirth cb = cust.getBirth();
			CustomerPersonalData cpd = cust.getPersonalData();
			CustomerFinancialData cfd = cust.getFinancialData();
			List<CustomerEmployment> ce = cust.getEmployments();
			List<CustomerAddress> ca = cust.getAddresses();
			List<CustomerAdditionalData> cad = cust.getAdditionalData();
			List<Document> doc = cust.getDocuments();
			bsl.setCuid(cuid);
			bsl.setLegalName(cust.getName().getName1()+(cust.getName().getNameMid() == null ? "" : " "+cust.getName().getNameMid())+" "+cust.getName().getName2());
			bsl.setFirstName(cust.getName().getName1());
			bsl.setLastName(cust.getName().getName2());
			if(cust.getGender().equals("M")){
				bsl.setGender("Male");
			}else{
				bsl.setGender("Female");
			}
			bsl.setMothersName(cust.getName().getName4());
			bsl.setBirthDate(sdf.format(cb.getBirthDate().toGregorianCalendar().getTime()));
			bsl.setBirthPlace(cb.getBirthPlace());
			bsl.setEducation(cpd.getEducation());
			bsl.setMaritalStatus(cpd.getMaritalStatus());
			bsl.setNoDependents(String.valueOf(cpd.getDependentPersonNum()));
			String religion = "";
			for(int x=0;x<cad.size();x++){
				String flag = cad.get(x).getFlagType();
				if(flag.equals("Religion")){
					religion = cad.get(x).getCharValue();
				}
			}
			bsl.setReligion(religion);
			if(ce.get(0).getProfession()==null){
				bsl.setOccupation("EMPLOYEE");
			}else{
				bsl.setOccupation(ce.get(0).getProfession());
			}
			bsl.setPlaceWork(ce.get(0).getEmployer().getName());
			bsl.setFieldBusiness(ce.get(0).getIndustry().replace(" ",""));
			bsl.setMonthlyIncome(dec.format(cfd.getIncome()).replace(",","."));
			String type = "";
			String no = "";
			String valid = "";
			for(int x=0;x<doc.size();x++){
				String xdoc = doc.get(x).getType();
				if(xdoc.equals("KTP")){
					type = "KTP";
					no = doc.get(x).getNumber();
					if(doc.get(x).getValidTo()==null){
						valid = "2099-12-31";
					}else{
						valid = sdf.format(doc.get(x).getValidTo().toGregorianCalendar().getTime());
					}
				}
			}
			if(type.equals("")){
				type = doc.get(0).getType();
				no = doc.get(0).getNumber();
				if(doc.get(0).getValidTo()==null){
					valid = "2099-12-31";
				}else{
					valid = sdf.format(doc.get(0).getValidTo().toGregorianCalendar().getTime());
				}
			}
			bsl.setTypeIdCard(type);
			bsl.setNoIdCard(no);
			bsl.setExDateIdCard(valid);
			bsl.setStreetName(ca.get(0).getAddress().getStreet().getName());
			if(ca.get(0).getAddress().getTown().getCode()==null){
				if(ca.get(0).getAddress().getTown().getName()==null){
					bsl.setTown("");
				}else{
					bsl.setTown(ca.get(0).getAddress().getTown().getName());
				}
			}else{
				bsl.setTown(ca.get(0).getAddress().getTown().getCode());
			}
			if(ca.get(0).getAddress().getSubdistrict().getCode()==null){
				if(ca.get(0).getAddress().getSubdistrict().getName()==null){
					bsl.setSubdistrict("");
				}else{
					bsl.setSubdistrict(ca.get(0).getAddress().getSubdistrict().getName());
				}
			}else{
				bsl.setSubdistrict(ca.get(0).getAddress().getSubdistrict().getCode());
			}
			if(ca.get(0).getAddress().getDistrict().getCode()==null){
				if(ca.get(0).getAddress().getDistrict().getName()==null){
					bsl.setDistrict("");
				}else{
					bsl.setDistrict(ca.get(0).getAddress().getDistrict().getName());
				}
			}else{
				bsl.setDistrict(ca.get(0).getAddress().getDistrict().getCode());
			}
			bsl.setRt(ca.get(0).getAddress().getBlock());
			bsl.setRw(ca.get(0).getAddress().getBlockSet());
			bsl.setZipCode(ca.get(0).getAddress().getZip().getCode());
		}
		return bsl;
	}

}
