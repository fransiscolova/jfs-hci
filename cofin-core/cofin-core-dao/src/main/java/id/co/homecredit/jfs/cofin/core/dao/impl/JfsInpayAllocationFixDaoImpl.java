package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.dao.JfsInpayAllocationFixDao;
import id.co.homecredit.jfs.cofin.core.model.JfsInpayAllocationFix;

/**
 * Dao Implement Class For {@link JfsInpayAllocationFixDao}
 * 
 * @author denny.afrizal01
 *
 */
@Repository
@SuppressWarnings("unchecked")
public class JfsInpayAllocationFixDaoImpl implements JfsInpayAllocationFixDao{
	private static final Logger log = LogManager.getLogger(JfsInpayAllocationFixDaoImpl.class);
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }

	@Override
	public JfsInpayAllocationFix getAllocationAmountValue(Long incomingPaymentId,Date dateInstalment){
		log.debug("Get Payment Allocation By Incoming Payment Id {} And Date Instalment {}",incomingPaymentId,dateInstalment);
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select * from JFS_INPAY_ALLOCATION_FIX where INCOMING_PAYMENT_ID = '"+String.valueOf(incomingPaymentId)+"' and DUE_DATE = DATE '"+dateInstalment.toString().substring(0,10)+"'");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		session.close();
		if(list.size()==0){
			return null;
		}else{
			List<JfsInpayAllocationFix> inpay = new ArrayList<JfsInpayAllocationFix>();
			for(int x=0;x<list.size();x++){
				JfsInpayAllocationFix alloc = new JfsInpayAllocationFix();
				alloc.setTextContractNumber(list.get(x).get("TEXT_CONTRACT_NUMBER").toString());
				alloc.setDueDate(list.get(x).get("DUE_DATE").toString().substring(0,10));
				alloc.setPartIndex(list.get(x).get("PART_INDEX").toString());
				alloc.setAmtPrincipal(list.get(x).get("AMT_PRINCIPAL").toString());
				alloc.setAmtInterest(list.get(x).get("AMT_INTEREST").toString());
				if(list.get(x).get("ARCHIEVED")==null){
					alloc.setArchieved("0");
				}else{
					alloc.setArchieved(list.get(x).get("ARCHIEVED").toString());
				}
				alloc.setDtimeCreated(list.get(x).get("DTIME_CREATED").toString().substring(0,10));
				if(list.get(x).get("DTIME_UPDATED")==null){
					alloc.setDtimeUpdated("");
				}else{
					alloc.setDtimeUpdated(list.get(x).get("DTIME_UPDATED").toString().substring(0,10));
				}
				alloc.setIncomingPaymentId(list.get(x).get("INCOMING_PAYMENT_ID").toString());
				if(list.get(x).get("AMT_OVERPAYMENT")==null){
					alloc.setAmtOverPayment("0");
				}else{
					alloc.setAmtOverPayment(list.get(x).get("AMT_OVERPAYMENT").toString());
				}
				inpay.add(alloc);
			}
			return inpay.get(0);
		}
	}

}
