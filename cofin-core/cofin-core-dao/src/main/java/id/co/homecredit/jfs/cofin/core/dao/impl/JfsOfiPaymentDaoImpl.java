package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.core.dao.JfsOfiPaymentDao;
import id.co.homecredit.jfs.cofin.core.model.JfsOfiPayment;

/**
 * Dao Implements Class For {@link JfsOfiPaymentDao}
 * 
 * @author denny.afrizal01
 *
 */
@Repository
@SuppressWarnings("unchecked")
public class JfsOfiPaymentDaoImpl extends DaoIdOnlyImpl<JfsOfiPayment,String> implements JfsOfiPaymentDao{
	private static final Logger log = LogManager.getLogger(JfsOfiPaymentDaoImpl.class);
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public boolean saveOfiPayment(String sql)throws Exception{
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery(sql);
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		for(int x=0;x<list.size();x++){
			Date dueDate = sdf.parse(list.get(x).get("DUE_DATE").toString().substring(0,10));
			Date payment = sdf.parse(list.get(x).get("PAYMENT_DATE").toString().substring(0,10));
			JfsOfiPayment pay = new JfsOfiPayment();
			pay.setIncomingPaymentId(Long.parseLong(list.get(x).get("INCOMING_PAYMENT_ID").toString()));
			pay.setTextContractNumber(list.get(x).get("TEXT_CONTRACT_NUMBER").toString());
			pay.setDueDate(dueDate);
			pay.setPaymentDate(payment);
			pay.setOfiPrincipal(Double.parseDouble(list.get(x).get("OFI_PRINCIPAL").toString()));
			pay.setOfiInterest(Double.parseDouble(list.get(x).get("OFI_INTEREST").toString()));
			pay.setDtimeCreated(new Date());
			pay.setCreatedBy("JFS_SYSTEM");
			pay.setVer(1);
			pay.setIsActive("Y");
//			addObject(pay);
		}
		session.close();
		return true;
	}
	
}
