package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.dao.JfsPartnerDao;
import id.co.homecredit.jfs.cofin.core.model.JfsPartner;

/**
 * Dao Implement Class For {@link JfsPartner}
 * 
 * @author denny.afrizal01
 *
 */
@Repository
@SuppressWarnings("unchecked")
public class JfsPartnerDaoImpl implements JfsPartnerDao{
	private static final Logger log = LogManager.getLogger(JfsPartnerDaoImpl.class);

	@Autowired
	protected SessionFactory sfJFS;
	public void setSessionFactory(SessionFactory sessionFactory){
		this.sfJFS = sessionFactory;
	}
	
	@Override
	public String getPartnerName(String partnerId){
		log.debug("Search Name Of Partner {}",partnerId);
		String xname = "";
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select name as name from JFS_PARTNER where id = '"+partnerId+"'");
		log.debug(query.getQueryString());
    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		if(list.size()==0){
			xname = partnerId;
		}else{
			xname = list.get(0).get("NAME").toString();
		}
		session.close();
		return xname;
	}

	@Override
    public List<JfsPartner> getAllPartner(){
	    log.debug("Get All JFS Partner Value");
	    Session session = sfJFS.openSession();
	    Criteria crt = session.createCriteria(JfsPartner.class);
	    crt.addOrder(Order.asc("priority"));
	    try{
	        List<JfsPartner> partner = crt.list();
	        return partner;
        }catch(Exception l){
	        System.out.println(l);
	        l.printStackTrace();
	        return new ArrayList<JfsPartner>();
        }
    }
	
//	@Override
//	public List<JfsPartner> getAllPartner(){
//		log.debug("Get All Jfs Partner Value");
//		List<JfsPartner> partner = new ArrayList<JfsPartner>();
//		Session session = sfJFS.openSession();
//		Query query = session.createSQLQuery("select * from JFS_PARTNER where priority is not null order by ID");
//		log.debug(query.getQueryString());
//		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//		List<Map<String,Object>> list = query.list();
//		for(int x=0;x<list.size();x++){
//			JfsPartner part = new JfsPartner();
//			part.setId(list.get(x).get("ID").toString());
//			part.setName(list.get(x).get("NAME").toString());
//			part.setAddress(list.get(x).get("ADDRESS").toString());
//			part.setMainContact(list.get(x).get("MAIN_CONTACT").toString());
//			part.setPhoneNumber(list.get(x).get("PHONE_NUMBER").toString());
//			part.setEmail(list.get(x).get("EMAIL").toString());
//			part.setIsDelete(YesNoEnum.valueOf(list.get(x).get("IS_DELETE").toString()));
//			part.setIsCheckingEligibility(YesNoEnum.valueOf(list.get(x).get("IS_CHECKING_ELIGIBILITY").toString()));
//			part.setPriority(Long.parseLong(list.get(x).get("PRIORITY").toString()));
//			partner.add(part);
//		}
//		session.close();
//		return partner;
//	}

	@Override
	public JfsPartner getPartnerValueById(String id){
		log.debug("Get JFS Partner Value By Partner ID {}",id);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPartner.class);
		crt.add(Restrictions.eq("id",id));
		List<JfsPartner> part = crt.list();
		if(part.size()==0){
			return null;
		}else{
			return part.get(0);
		}
	}
	
//	@Override
//	public JfsPartner getPartnerValueById(String id){
//		log.debug("Get JFS Partner Value By Partner ID {}",id);
//		Session session = sfJFS.openSession();
//		Query query = session.createSQLQuery("select * from JFS_PARTNER where ID = '"+id+"'");
//		log.debug(query.getQueryString());
//		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//		List<Map<String,Object>> list = query.list();
//		JfsPartner part = new JfsPartner();
//		part.setId(list.get(0).get("ID").toString());
//		part.setName(list.get(0).get("NAME").toString());
//		part.setAddress(list.get(0).get("ADDRESS").toString());
//		part.setMainContact(list.get(0).get("MAIN_CONTACT").toString());
//		part.setPhoneNumber(list.get(0).get("PHONE_NUMBER").toString());
//		part.setEmail(list.get(0).get("EMAIL").toString());
//		part.setIsDelete(list.get(0).get("IS_DELETE").toString());
//		part.setIsCheckingEligibility(list.get(0).get("IS_CHECKING_ELIGIBILITY").toString());
//		part.setPriority(Long.parseLong(list.get(0).get("PRIORITY").toString()));
//		session.close();
//		return part;
//	}

    /**
     * Update Object In Database Using Query String.
     *
     * @param script
     * @return query
     */
    public boolean updateObjectWithQuery(String script){
        Session session = sfJFS.openSession();
        try{
            session.beginTransaction();
            org.hibernate.query.Query query = session.createSQLQuery(script);
            query.executeUpdate();
            session.getTransaction().commit();
            log.debug(query.getQueryString());
        }catch(HibernateException d){
            log.debug(d);
            session.getTransaction().rollback();
            return false;
        }finally{
            session.close();
        }
        return true;
    }

    /**
     * Save Object
     *
     * @param object
     * @return object
     */
    public boolean saveObject(Object object){
        Session session = sfJFS.openSession();
        try{
            session.beginTransaction();
            session.save(object);
            session.getTransaction().commit();
        }catch(HibernateException f){
            session.getTransaction().rollback();
            f.printStackTrace();
            return false;
        }finally{
            session.close();
        }
        return true;
    }

}
