package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.dao.JfsPaymentAllocationDao;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentAllocation;

/**
 * Dao Implements Class For {@link JfsPaymentAllocationDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class JfsPaymentAllocationDaoImpl implements JfsPaymentAllocationDao{
	private static final Logger log = LogManager.getLogger(JfsPaymentAllocationDaoImpl.class);
    
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }
	
    @Override
    public List<JfsPaymentAllocation> getAllocationPartnerByContractNumber(String contractNumber){
    	log.debug("Get Payment Allocation By Contract Number {}",contractNumber);
    	String sql = "select a.PART_INDEX,a.DUE_DATE,a.AMT_PRINCIPAL,a.AMT_INTEREST,b.INCOMING_PAYMENT_ID,c.DATE_PAYMENT,a.DTIME_CREATED from JFS_PAYMENT_ALLOCATION a "
    				 +"left outer join JFS_INPAY_ALLOCATION_FIX b on a.TEXT_CONTRACT_NUMBER = b.TEXT_CONTRACT_NUMBER and a.DUE_DATE = b.DUE_DATE "
    				 +"left outer join JFS_PAYMENT_INT c on b.INCOMING_PAYMENT_ID = c.INCOMING_PAYMENT_ID where a.TEXT_CONTRACT_NUMBER = '"+contractNumber+"' order by b.INCOMING_PAYMENT_ID";
    	Session session = sfJFS.openSession();
    	Query query = session.createSQLQuery(sql);
    	log.debug(query.getQueryString());
    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
    	DecimalFormat dec = new DecimalFormat("###,###,###,###,###");
    	List<Map<String,Object>> list = query.list();
    	List<JfsPaymentAllocation> alloc = new ArrayList<JfsPaymentAllocation>();
    	for(int x=0;x<list.size();x++){
    		JfsPaymentAllocation pay = new JfsPaymentAllocation();
    		pay.setPartIndex(list.get(x).get("PART_INDEX").toString());
    		pay.setDueDate(list.get(x).get("DUE_DATE").toString().substring(0,10));
    		pay.setAmtPrincipal(dec.format(Double.parseDouble(list.get(x).get("AMT_PRINCIPAL").toString())).replace(",","."));
    		pay.setAmtInterest(dec.format(Double.parseDouble(list.get(x).get("AMT_INTEREST").toString())).replace(",","."));
    		pay.setArchieved(list.get(x).get("INCOMING_PAYMENT_ID").toString());
    		pay.setDtimeUpdated(list.get(x).get("DATE_PAYMENT").toString().substring(0,10));
    		pay.setDtimeCreated(list.get(x).get("DTIME_CREATED").toString().substring(0,10));
    		alloc.add(pay);
    	}
    	session.close();
    	return alloc;
    }
	
}
