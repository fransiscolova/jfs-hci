package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.core.dao.JfsPaymentBslDao;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentBsl;

/**
 * Dao Implement Class For {@link JfsPaymentBslDao}
 * 
 * @author denny.afrizal01
 *
 */

@Repository
@SuppressWarnings("unchecked")
public class JfsPaymentBslDaoImpl extends DaoIdOnlyImpl<JfsPaymentBsl,String> implements JfsPaymentBslDao{
	private static final Logger log = LogManager.getLogger(JfsPaymentBslDaoImpl.class);
	
	@Override
	public List<JfsPaymentBsl> getBslPaymentByDate(Date paymentDate){
		log.debug("Get BSL Payment By Date {}",paymentDate);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentBsl.class);
		crt.add(Restrictions.eq("paymentDate",paymentDate));
		crt.addOrder(Order.asc("textContractNumber"));
		crt.addOrder(Order.asc("incomingPaymentId"));
		crt.addOrder(Order.asc("skfInstalmentHead"));
		List<JfsPaymentBsl> bsl = crt.list();
		return bsl;
	}

	@Override
	public List<JfsPaymentBsl> getBslPaymentByContractNumber(String textContractNumber){
		log.debug("Get Payment BSL By Contract Number {}",textContractNumber);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentBsl.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.addOrder(Order.asc("instalmentNumber"));
		List<JfsPaymentBsl> list = crt.list();
		return list;
	}

	@Override
	public List<JfsPaymentBsl> getBslPaymentBetweenDate(Date dateFrom,Date dateTo){
		log.debug("Get Payment BSL Between {} And {}",dateFrom,dateTo);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentBsl.class);
		crt.add(Restrictions.between("paymentDate",dateFrom,dateTo));
		crt.addOrder(Order.asc("textContractNumber"));
		crt.addOrder(Order.asc("incomingPaymentId"));
		crt.addOrder(Order.asc("skfInstalmentHead"));
		List<JfsPaymentBsl> list = crt.list();
		System.out.println("Jumlah Record = "+list.size());
		return list;
	}

}
