package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.dao.JfsPaymentIntDao;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentInt;

/**
 * Dao Implement Class For {@link JfsPaymentIntDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class JfsPaymentIntDaoImpl implements JfsPaymentIntDao{
	private static final Logger log = LogManager.getLogger(JfsPaymentIntDaoImpl.class);
    
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }

    @Override
	public List<JfsPaymentInt> getPaymentIntByContractNumber(String contractNumber){
		log.debug("Get Value Of JFS Payment Int By Contract Number {}",contractNumber);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentInt.class);
		crt.add(Restrictions.eq("textContractNumber",contractNumber));
		crt.addOrder(Order.asc("incomingPaymentId"));
		return crt.list();
	}

	@Override
	public JfsPaymentInt getByContractNumberAndIncomingPayment(String textContractNumber,Long incomingPaymentId){
		log.debug("Get Value Of JFS Payment Int By Contract Number {} And Incoming Payment {}",textContractNumber,incomingPaymentId);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsPaymentInt.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.add(Restrictions.eq("incomingPaymentId",incomingPaymentId));
		List<JfsPaymentInt> xint = crt.list();
		if(xint.size()==0){
			return null;
		}else{
			return xint.get(0);
		}
	}
	
}
