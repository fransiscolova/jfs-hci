package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;
import java.util.Map;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.core.dao.JfsProductDao;
import id.co.homecredit.jfs.cofin.core.model.JfsProduct;

/**
 * Dao Implement Class For{@link JfsProduct}
 * 
 * @author denny.afrizal01
 *
 */
@Repository
public class JfsProductDaoImpl extends DaoIdOnlyImpl<JfsProduct,String> implements JfsProductDao{
	private static final Logger log = LogManager.getLogger(JfsProductDaoImpl.class);
	
	@Override
	public List<JfsProduct> getProductByAgreementId(Long agreementId) throws ParseException{
		log.debug("Get JFS Product Detail By Agreement ID {}",agreementId);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select SYS.STANDARD.TO_CHAR(ID) as ID,INTEREST_RATE,PRINCIPAL_SPLIT_RATE,PENALTY_SPLIT_RATE,PRINCIPAL_SPLIT_RATE_HCI,ADMIN_FEE_RATE,VALID_FROM,VALID_TO from JFS_PRODUCT where ID_AGREEMENT = '"+agreementId+"' order by VALID_FROM desc");
		log.debug(query.getQueryString());
    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
    	List<Map<String,Object>> daftar = query.list();
    	List<JfsProduct> detail = new ArrayList<JfsProduct>();
    	String status = "";
    	for(int x=0;x<daftar.size();x++){
    		if(x==0){
    			status = "ACTIVE";
    		}else{
    			status = "INACTIVE";
    		}
    		JfsProduct isi = new JfsProduct();
    		isi.setBankProductCode(daftar.get(x).get("VALID_FROM").toString().substring(0,10)+"-"+daftar.get(x).get("VALID_TO").toString().substring(0,10)+"-"+status);
    		isi.setInterestRate(Double.parseDouble(daftar.get(x).get("INTEREST_RATE").toString()));
    		isi.setPrincipalSplitRate(Double.parseDouble(daftar.get(x).get("PRINCIPAL_SPLIT_RATE").toString()));
    		isi.setPenaltySplitRate(Double.parseDouble(daftar.get(x).get("PENALTY_SPLIT_RATE").toString()));
    		isi.setPrincipalSplitRateHci(Double.parseDouble(daftar.get(x).get("PRINCIPAL_SPLIT_RATE_HCI").toString()));
    		isi.setValidFrom(format.parse(daftar.get(x).get("VALID_FROM").toString()));
    		isi.setValidTo(format.parse(daftar.get(x).get("VALID_TO").toString()));
    		isi.setIdAgreement(Long.parseLong(String.valueOf(x+1)));
    		isi.setAdminFeeRate(Double.parseDouble(daftar.get(x).get("ADMIN_FEE_RATE").toString()));
    		isi.setId(daftar.get(x).get("ID").toString());
    		detail.add(isi);
    	}
    	session.close();
    	return detail;
	}
	
	@Override
	public JfsProduct getProductById(String id)throws ParseException{
		log.debug("Get JFS Product By ID {}",id);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select * from JFS_PRODUCT where ID = '"+id+"'");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> hasil = query.list();
		JfsProduct product = new JfsProduct();
		product.setBankProductCode(hasil.get(0).get("BANK_PRODUCT_CODE").toString());
		product.setInterestRate(Double.parseDouble(hasil.get(0).get("INTEREST_RATE").toString()));
		product.setPrincipalSplitRate(Double.parseDouble(hasil.get(0).get("PRINCIPAL_SPLIT_RATE").toString()));
		product.setValidFrom(format.parse(hasil.get(0).get("VALID_FROM").toString()));
		product.setValidTo(format.parse(hasil.get(0).get("VALID_TO").toString()));
		product.setAdminFeeRate(Double.parseDouble(hasil.get(0).get("ADMIN_FEE_RATE").toString()));
		product.setId(id);
		product.setPenaltySplitRate(Double.parseDouble(hasil.get(0).get("PENALTY_SPLIT_RATE").toString()));
		product.setPrincipalSplitRateHci(Double.parseDouble(hasil.get(0).get("PRINCIPAL_SPLIT_RATE_HCI").toString()));
		session.close();
		return product;
	}

}
