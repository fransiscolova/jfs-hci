package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.core.dao.JfsProductHciDao;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.JfsProductHci;

/**
 * Dao implement class for {@link JfsProductHci}.
 *
 * @author denny.afrizal01
 *
 */
@Repository
public class JfsProductHciDaoImpl extends DaoIdOnlyImpl<JfsProductHci,String> implements JfsProductHciDao{
    private static final Logger log = LogManager.getLogger(JfsProductHciDaoImpl.class);
    
    @Override
    public JfsProductHci getJfsProductHciById(String id){
    	log.info("Get JFS Product HCI Value By ID {}",id);
    	Criteria crt = createNewCriteria();
    	crt.add(Restrictions.eq("codeProduct",id));
    	return (JfsProductHci) crt.uniqueResult();
    }
    
}