package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoIdOnlyImpl;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.dao.JfsProductMappingDao;
import id.co.homecredit.jfs.cofin.core.model.JfsProductMapping;

/**
 * Dao Implement Class For {@link JfsProductMapping}
 * 
 * @author denny.afrizal01
 *
 */
@Repository
public class JfsProductMappingDaoImpl extends DaoIdOnlyImpl<JfsProductMapping,String> implements JfsProductMappingDao{
	private static final Logger log = LogManager.getLogger(JfsProductMappingDaoImpl.class);
	
	@Override
	public List<JfsProductMapping> getProductMappingByAgreementId(Integer agreementId)throws ParseException{
		log.debug("Get All Product Mapping By Agreement ID {}",agreementId);
		List<JfsProductMapping> mapping = new ArrayList<JfsProductMapping>();
		String sql = "select DISTINCT SYS.STANDARD.TO_CHAR(a.ID) as ID,a.CODE_PRODUCT,b.NAME_PRODUCT,a.VALID_FROM,a.VALID_TO,"
					 +"CASE WHEN a.IS_DELETE = 'Y' THEN 'INACTIVE' ELSE 'ACTIVE' END AS STATUS "
					 +"from JFS_PRODUCT_MAPPING a LEFT OUTER JOIN JFS_PRODUCT_HCI b ON a.CODE_PRODUCT = b.CODE_PRODUCT "
					 +"where a.ID_AGREEMENT = '"+agreementId+"' and b.NAME_PRODUCT IS NOT NULL order by a.CODE_PRODUCT";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery(sql);
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> hasil = query.list();
		for(int x=0;x<hasil.size();x++){
			JfsProductMapping prod = new JfsProductMapping();
			prod.setIdAgreement(Long.parseLong(String.valueOf(x+1)));
			prod.setId(hasil.get(x).get("ID").toString());
			prod.setCodeProduct(hasil.get(x).get("CODE_PRODUCT").toString());
			prod.setBankProductCode(hasil.get(x).get("NAME_PRODUCT").toString());
			prod.setValidFrom(format.parse(hasil.get(x).get("VALID_FROM").toString()));
			prod.setValidTo(format.parse(hasil.get(x).get("VALID_TO").toString()));
			prod.setCreatedBy(hasil.get(x).get("VALID_FROM").toString().substring(0,10)+"-"+hasil.get(x).get("VALID_TO").toString().substring(0,10));
			prod.setUpdatedBy(hasil.get(x).get("STATUS").toString());
			mapping.add(prod);
		}
		session.close();
		return mapping;
	}
	
	@Override
	public JfsProductMapping getProductMappingById(String id)throws ParseException{
		log.debug("Get Product Mapping By ID {}",id);
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select * from JFS_PRODUCT_MAPPING where ID = '"+id+"'");
		SimpleDateFormat xformat = new SimpleDateFormat("yyyy-MM-dd");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> obj = query.list();
		JfsProductMapping prod = new JfsProductMapping();
		prod.setCodeProduct(obj.get(0).get("CODE_PRODUCT").toString());
		prod.setBankProductCode(obj.get(0).get("BANK_PRODUCT_CODE").toString());
		prod.setValidFrom(xformat.parse(obj.get(0).get("VALID_FROM").toString()));
		prod.setValidTo(xformat.parse(obj.get(0).get("VALID_TO").toString()));
		prod.setIdAgreement(Long.parseLong(obj.get(0).get("ID_AGREEMENT").toString()));
		prod.setDtimeInserted(xformat.parse(obj.get(0).get("DTIME_INSERTED").toString()));
		if(obj.get(0).get("IS_DELETE")==null){
			prod.setIsDelete(YesNoEnum.N);
		}else{
			prod.setIsDelete(YesNoEnum.valueOf(obj.get(0).get("IS_DELETE").toString()));
		}
		if(obj.get(0).get("CREATED_BY")==null){
			prod.setCreatedBy("cofin_system");
		}else{
			prod.setCreatedBy(obj.get(0).get("CREATED_BY").toString());
		}
		if(obj.get(0).get("CREATED_DATE")==null){
			prod.setCreatedDate(new Date());
		}else{
			prod.setCreatedDate(xformat.parse(obj.get(0).get("CREATED_DATE").toString()));
		}
		if(obj.get(0).get("UPDATED_BY")==null){
			prod.setUpdatedBy("cofin_system");
		}else{
			prod.setUpdatedBy(obj.get(0).get("UPDATED_BY").toString());
		}
		if(obj.get(0).get("UPDATED_DATE")==null){
			prod.setUpdatedDate(new Date());
		}else{
			prod.setUpdatedDate(xformat.parse(obj.get(0).get("UPDATED_DATE").toString()));
		}
		return prod;
	}
	
}
