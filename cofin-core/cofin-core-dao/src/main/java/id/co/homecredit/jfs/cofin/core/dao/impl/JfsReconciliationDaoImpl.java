package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import java.util.List;
import java.util.Map;
import java.text.DecimalFormat;
import java.util.ArrayList;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.model.JfsReconciliation;
import id.co.homecredit.jfs.cofin.core.dao.JfsReconciliationDao;

/**
 * Dao Implement Class For {@link JfsReconciliationDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class JfsReconciliationDaoImpl implements JfsReconciliationDao{
	private static final Logger log = LogManager.getLogger(JfsReconciliationDaoImpl.class);
    
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }

    @Override
	public List<JfsReconciliation> getReconciliationByContractNumber(String contractNumber){
		log.debug("Get Value Of JFS Reconciliation By Contract Number {}",contractNumber);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsReconciliation.class);
		crt.add(Restrictions.eq("textContractNumber",contractNumber));
		crt.addOrder(Order.asc("reconId"));
		return crt.list();
	}
	
//    @Override
//    public List<JfsReconciliation> getReconciliationByContractNumber(String contractNumber){
//    	log.debug("Get Value Of JFS Reconciliation By Contract Number {}",contractNumber);
//    	Session session = sfJFS.openSession();
//    	Query query = session.createSQLQuery("select * from JFS_RECONCILIATION where TEXT_CONTRACT_NUMBER = '"+contractNumber+"' order by RECON_ID");
//    	log.debug(query.getQueryString());
//    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//    	DecimalFormat dec = new DecimalFormat("###,###,###,###,###");
//    	List<Map<String,Object>> list = query.list();
//    	List<JfsReconciliation> recon = new ArrayList<JfsReconciliation>();
//    	for(int x=0;x<list.size();x++){
//    		JfsReconciliation rec = new JfsReconciliation();
//    		rec.setReconId(String.valueOf(x+1));
//    		rec.setTextContractNumber(contractNumber);
//    		rec.setReconDate(list.get(x).get("RECON_DATE").toString().substring(0,10));
//    		rec.setOutstandingPrincipal(dec.format(Double.parseDouble(list.get(x).get("OUTSTANDING_PRINCIPAL").toString())).replace(",","."));
//    		rec.setDpd(list.get(x).get("DPD").toString());
//    		if(list.get(x).get("INST_NUMBER")==null){
//    			rec.setInstNumber("-");
//    		}else{
//    			rec.setInstNumber(list.get(x).get("INST_NUMBER").toString());
//    		}
//    		recon.add(rec);
//    	}
//    	session.close();
//    	return recon;
//    }

}
