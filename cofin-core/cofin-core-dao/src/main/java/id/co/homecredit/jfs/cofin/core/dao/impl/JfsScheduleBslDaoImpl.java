package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.dao.JfsScheduleBslDao;
import id.co.homecredit.jfs.cofin.core.model.JfsScheduleBsl;

/**
 * Dao Implement Class For {@link JfsScheduleBslDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class JfsScheduleBslDaoImpl implements JfsScheduleBslDao{
	public static final Logger log = LogManager.getLogger(JfsScheduleBslDaoImpl.class);
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }
	
	@Override
	public List<JfsScheduleBsl> getScheduleBslByContractNumber(String contractNumber){
		log.debug("Get Value Of BSL Schedule By Contract Number {}",contractNumber);
		DecimalFormat rph = new DecimalFormat("###,###,###,###,###");
		DecimalFormat dec = new DecimalFormat("###,###,###,###,###.##");
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select * from JFS_SCHEDULE_BSL where TEXT_CONTRACT_NUMBER = '"+contractNumber+"' order by PART_INDEX");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		List<JfsScheduleBsl> sch = new ArrayList<JfsScheduleBsl>();
		for(int x=0;x<list.size();x++){
			JfsScheduleBsl bsl = new JfsScheduleBsl();
			bsl.setTextContractNumber(contractNumber);
			bsl.setDueDate(list.get(x).get("DUE_DATE").toString().substring(0,10));
			bsl.setPartIndex(list.get(x).get("PART_INDEX").toString());
			String principal = dec.format(Double.parseDouble(list.get(x).get("PRINCIPAL").toString().replace(",","."))).replace(".","~");
			principal = principal.replace(",",".");
			bsl.setPrincipal(principal.replace("~",","));
			String interest = dec.format(Double.parseDouble(list.get(x).get("INTEREST").toString().replace(",","."))).replace(".","~");
			interest = interest.replace(",",".");
			bsl.setInterest(interest.replace("~",","));
			if(list.get(x).get("FEE")==null){
				bsl.setFee("0");
			}else{
				bsl.setFee(rph.format(Double.parseDouble(list.get(x).get("FEE").toString())).replace(",","."));
			}
			String outstanding = "";
			if(list.get(x).get("OUTSTANDING_PRINCIPAL")==null){
				outstanding = "0";
			}else{
				outstanding = dec.format(Double.parseDouble(list.get(x).get("OUTSTANDING_PRINCIPAL").toString().replace(",","."))).replace(".","~");
				outstanding = outstanding.replace(",",".");
			}
			bsl.setOutstandingPrincipal(outstanding.replace("~",","));
			if(list.get(x).get("PREV_BALANCE")==null){
				bsl.setPrevBalance("0");
			}else{
				bsl.setPrevBalance(rph.format(Double.parseDouble(list.get(x).get("PREV_BALANCE").toString())).replace(",","."));
			}
			if(list.get(x).get("BALANCE")==null){
				bsl.setBalance("0");
			}else{
				bsl.setBalance(rph.format(Double.parseDouble(list.get(x).get("BALANCE").toString())).replace(",","."));
			}
			if(list.get(x).get("PENALTY")==null){
				bsl.setPenalty("0");
			}else{
				bsl.setPenalty(rph.format(Double.parseDouble(list.get(x).get("PENALTY").toString())).replace(",","."));
			}
			if(list.get(x).get("VER")==null){
				bsl.setVer("1");
			}else{
				bsl.setVer(list.get(x).get("VER").toString());
			}
			if(list.get(x).get("IS_ACTIVE")==null){
				bsl.setIsActive("Y");
			}else{
				bsl.setIsActive(list.get(x).get("IS_ACTIVE").toString());
			}
			sch.add(bsl);
		}
		session.close();
		return sch;
	}
	
}
