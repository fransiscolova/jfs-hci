package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.dao.JfsScheduleDao;
import id.co.homecredit.jfs.cofin.core.model.JfsSchedule;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Dao Implement Class For {@link JfsScheduleDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class JfsScheduleDaoImpl implements JfsScheduleDao{
	public static final Logger log = LogManager.getLogger(JfsScheduleDaoImpl.class);
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Override
	public JfsSchedule getScheduleJfs(String textContractNumber,Integer partIndex){
		log.debug("Get Value Of Bank Schedule By Contract Number {} And Part Index {}",textContractNumber,partIndex);
		Session session = sfJFS.openSession();
		Criteria crt = session.createCriteria(JfsSchedule.class);
		crt.add(Restrictions.eq("textContractNumber",textContractNumber));
		crt.add(Restrictions.eq("partIndex",partIndex));
		List<JfsSchedule> sch = crt.list();
		if(sch.size()==0){
			return null;
		}else{
			return sch.get(0);
		}
	}

//	@Override
//	public JfsSchedule getScheduleJfs(String textContractNumber,String partIndex)throws Exception{
//    	log.debug("Get Value Of Bank Schedule By Contract Number {} And Part Index {}",textContractNumber,partIndex);
//    	Session session = sfJFS.openSession();
//    	Query query = session.createSQLQuery("select * from JFS_SCHEDULE where TEXT_CONTRACT_NUMBER = '"+textContractNumber+"' and PART_INDEX = '"+partIndex+"'");
//    	log.debug(query.getQueryString());
//    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//    	List<Map<String,Object>> list = query.list();
//    	session.close();
//    	if(list.size()==0){
//    		return null;
//		}else{
//    		List<JfsSchedule> skedul = new ArrayList<JfsSchedule>();
//    		for(int x=0;x<list.size();x++){
//				JfsSchedule jfs = new JfsSchedule();
//				jfs.setTextContractNumber(list.get(x).get(CoreColumnName.TEXT_CONTRACT_NUMBER).toString());
//				jfs.setDueDate(sdf.parse(list.get(x).get(CoreColumnName.DUE_DATE).toString().substring(0,10)));
//				jfs.setPartIndex(Integer.parseInt(list.get(x).get(CoreColumnName.PART_INDEX).toString()));
//				jfs.setPrincipal(Double.parseDouble(list.get(x).get(CoreColumnName.PRINCIPAL).toString()));
//				jfs.setInterest(Double.parseDouble(list.get(x).get(CoreColumnName.INTEREST).toString()));
//				if(list.get(x).get(CoreColumnName.FEE)==null){
//					jfs.setFee(0.0);
//				}else{
//					jfs.setFee(Double.parseDouble(list.get(x).get(CoreColumnName.FEE).toString()));
//				}
//				jfs.setOutstandingPrincipal(Double.parseDouble(list.get(x).get(CoreColumnName.OUTSTANDING_PRINCIPAL).toString()));
//				if(list.get(x).get(CoreColumnName.PREV_BALANCE)==null){
//					jfs.setPrevBalance(0.0);
//				}else{
//					jfs.setPrevBalance(Double.parseDouble(list.get(x).get(CoreColumnName.PREV_BALANCE).toString()));
//				}
//				if(list.get(x).get(CoreColumnName.BALANCE)==null){
//					jfs.setBalance(0.0);
//				}else{
//					jfs.setBalance(Double.parseDouble(list.get(x).get(CoreColumnName.BALANCE).toString()));
//				}
//				if(list.get(x).get(CoreColumnName.PENALTY)==null){
//					jfs.setPenalty(0.0);
//				}else{
//					jfs.setPenalty(Double.parseDouble(list.get(x).get(CoreColumnName.PENALTY).toString()));
//				}
//				if(list.get(x).get(CoreColumnName.VERSION)==null){
//					jfs.setVer(Long.parseLong("1"));
//				}else{
//					jfs.setVer(Long.parseLong(list.get(x).get(CoreColumnName.VERSION).toString()));
//				}
//				if(list.get(x).get(CoreColumnName.IS_ACTIVE)==null){
//					jfs.setIsActive("Y");
//				}else{
//					jfs.setIsActive(list.get(x).get(CoreColumnName.IS_ACTIVE).toString());
//				}
//				skedul.add(jfs);
//			}
//			return skedul.get(0);
//		}
//	}
	
	@Override
	public Date getMaxDueDate(String textContractNumber)throws Exception{
		log.debug("Get Max Due Date From Contract Number {}",textContractNumber);
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select max(DUE_DATE) as DUE_DATE from JFS_SCHEDULE where TEXT_CONTRACT_NUMBER = '"+textContractNumber+"'");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> hasil = query.list();
		session.close();
		if(hasil==null){
			return new Date();
		}else{
			return sdf.parse(hasil.get(0).get(CoreColumnName.DUE_DATE).toString().substring(0,10));
		}
	}
	
	@Override
	public Double getMaxBalanceValue(String textContractNumber){
		log.debug("Get Max Balance Value From Contract {}",textContractNumber);
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select max(BALANCE) as BALANCE from JFS_SCHEDULE where TEXT_CONTRACT_NUMBER = '"+textContractNumber+"'");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		session.close();
		if(list==null){
			return 0.0;
		}else{
			return Double.parseDouble(list.get(0).get(CoreColumnName.BALANCE).toString());
		}
	}

}
