package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.LogDaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.LoginLogDao;
import id.co.homecredit.jfs.cofin.core.model.LoginLog;
import id.co.homecredit.jfs.cofin.core.model.Role;

/**
 * Dao implement class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class LoginLogDaoImpl extends LogDaoImpl<LoginLog, String> implements LoginLogDao {
    private static final Logger log = LogManager.getLogger(LoginLogDaoImpl.class);

    @Override
    public LoginLog getLatestLoginLogByUsername(String username) {
        log.debug("get latest login log by username {}", username);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("createdBy", username));
        criteria.addOrder(Order.desc("createdDate"));
        criteria.setMaxResults(1);
        return (LoginLog) criteria.uniqueResult();
    }

}
