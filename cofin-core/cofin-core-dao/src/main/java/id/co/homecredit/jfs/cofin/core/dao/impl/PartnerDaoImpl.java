package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.dao.PartnerDao;
import id.co.homecredit.jfs.cofin.core.model.Partner;

/**
 * Dao implement class for {@link Partner}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class PartnerDaoImpl extends DaoImpl<Partner, String> implements PartnerDao {
    private static final Logger log = LogManager.getLogger(PartnerDaoImpl.class);

    @Override
    public Partner getPartnerByCode(String code) {
        log.debug("get partner by code {}", code);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("code", code));
        return (Partner) criteria.uniqueResult();
    }

    @Override
    public Partner getPartnerByName(String name) {
        log.debug("get partner by name {}", name);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("name", name));
        return (Partner) criteria.uniqueResult();
    }

}
