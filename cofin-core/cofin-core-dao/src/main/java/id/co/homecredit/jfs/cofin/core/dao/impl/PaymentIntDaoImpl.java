package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.PaymentIntDao;
import id.co.homecredit.jfs.cofin.core.model.PaymentInt;

/**
 * Dao implements class for {@link PaymentInt}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class PaymentIntDaoImpl extends DaoImpl<PaymentInt, String> implements PaymentIntDao {
    private static final Logger log = LogManager.getLogger(PaymentIntDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<PaymentInt> getAllPaymentIntsByContractNumber(String contractNumber) {
        log.debug("get all payment ints by contract number {}", contractNumber);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("contractNumber", contractNumber));
        return criteria.list();
    }

    @Override
    public PaymentInt getPaymentIntByIncomingPaymentId(String incomingPaymentId) {
        log.debug("get payment int by incoming payment id {}", incomingPaymentId);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("incomingPaymentId", incomingPaymentId));
        return (PaymentInt) criteria.uniqueResult();
    }

}
