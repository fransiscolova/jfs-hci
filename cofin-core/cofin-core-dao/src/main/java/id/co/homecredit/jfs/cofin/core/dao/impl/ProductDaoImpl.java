package id.co.homecredit.jfs.cofin.core.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ProductDao;
import id.co.homecredit.jfs.cofin.core.model.Product;

/**
 * Dao implement class for {@link Product}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ProductDaoImpl extends DaoImpl<Product, String> implements ProductDao {
    private static final Logger log = LogManager.getLogger(ProductDaoImpl.class);

}
