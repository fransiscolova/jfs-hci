package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ProductMappingDao;
import id.co.homecredit.jfs.cofin.core.model.ProductMapping;

/**
 * Dao implement class for {@link ProductMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ProductMappingDaoImpl extends DaoImpl<ProductMapping, String>
        implements ProductMappingDao {
    private static final Logger log = LogManager.getLogger(ProductMappingDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductMapping> getAllProductMappingsByAgreementIdAndDate(String agreementId,
            Date date) {
        log.debug("get all product mappings by agreement id {} and date {}", agreementId, date);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("agreement.id", agreementId));
        criteria.add(Restrictions.le("validFrom", date));
        criteria.add(Restrictions.ge("validTo", date));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductMapping> getAllProductMappingsByProductCodeAndDate(String productCode,
            Date date) {
        log.debug("get all product mappings by product code {} and date {}", productCode, date);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("product.code", productCode));
        criteria.add(Restrictions.le("validFrom", date));
        criteria.add(Restrictions.ge("validTo", date));
        return criteria.list();
    }

    @Override
    public ProductMapping getProductMappingByProductCodeAndPartnerCodeAndDate(String productCode,
            String partnerCode, Date date) {
        log.debug("get product mapping by product code {}, partner code {}, and date {}",
                productCode, partnerCode, date);
        Criteria criteria = createCriteria();
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("product.code", productCode));
        criteria.add(Restrictions.eq("partner.code", partnerCode));
        criteria.add(Restrictions.le("validFrom", date));
        criteria.add(Restrictions.ge("validTo", date));
        return (ProductMapping) criteria.uniqueResult();
    }

}
