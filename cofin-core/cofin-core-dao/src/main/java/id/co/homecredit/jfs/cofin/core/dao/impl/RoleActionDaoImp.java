package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.RoleActionDao;
import id.co.homecredit.jfs.cofin.core.model.RoleAction;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Dao implement class for {@link RoleAction}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class RoleActionDaoImp extends DaoImpl<RoleAction, String> implements RoleActionDao {
    private static final Logger log = LogManager.getLogger(RoleActionDaoImp.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<RoleAction> getAllRoleActionsByAction(String actionUrl) {
       // log.debug("get all role action by action {}", actionUrl);
        // Criteria criteria = composeCriteria(true);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("action.action", actionUrl));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RoleAction> getAllRoleActionsByRole(RoleEnum role) {
       // log.debug("get all role action by role {}", role);
        // Criteria criteria = composeCriteria(true);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("role.role", role));
        return criteria.list();
    }

}
