package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.Date;
import java.text.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import java.text.SimpleDateFormat;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.dao.RoleDao;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Dao implement class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class RoleDaoImpl extends DaoImpl<Role, String> implements RoleDao {
    private static final Logger log = LogManager.getLogger(RoleDaoImpl.class);

    @Override
    public Role get(RoleEnum roleEnum) {
        log.debug("get role by role enum {}", roleEnum);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("role", roleEnum));
        log.info(criteria.toString());
        return (Role) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Role> getAllUnsedRole(List<Enum> col) {
        log.debug("get role by role enum {}", col);
        Criteria criteria = createCriteria();
        if (col.size() > 0) {
            criteria.add(Restrictions.not(Restrictions.in("role", col)));
        }
        log.info(criteria.toString());
        return criteria.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Role> getListRoles(List<Enum> col){
    	log.debug("Get Role By Role Enum {}", col);
    	Criteria crit = createCriteria();
    	if(col.size()>0){
    		crit.add(Restrictions.in("role", col));
    	}
    	log.info(crit.toString());
    	return crit.list();
    }
    
    @Override
    public Role getRoleValueByRole(RoleEnum role)throws ParseException{
    	log.debug("Get Role Value By Role {}",role);
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	Session session = sfJFS.openSession();
    	Query query = session.createSQLQuery("select * from COFIN_ROLE where ROLE = '"+String.valueOf(role)+"'");
    	log.debug(query.getQueryString());
    	query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
    	List<Map<String,Object>> hasil = query.list();
    	Role xrole = new Role();
    	xrole.setRole(role);
    	xrole.setCreatedBy(hasil.get(0).get("CREATED_BY").toString());
    	xrole.setCreatedDate(sdf.parse(hasil.get(0).get("CREATED_DATE").toString()));
    	if(hasil.get(0).get("UPDATED_BY")==null){
    		xrole.setUpdatedBy("");
    	}else{
    		xrole.setUpdatedBy(hasil.get(0).get("UPDATED_BY").toString());
    	}
    	if(hasil.get(0).get("UPDATED_DATE")==null){
    		xrole.setUpdatedDate(new Date());
    	}else{
    		xrole.setUpdatedDate(sdf.parse(hasil.get(0).get("UPDATED_DATE").toString()));
    	}
    	xrole.setDeleted(YesNoEnum.valueOf(hasil.get(0).get("DELETED").toString()));
    	xrole.setVersion(Long.parseLong(hasil.get(0).get("VERSION").toString()));
    	xrole.setDescription(hasil.get(0).get("DESCRIPTION").toString());
    	return xrole;
    }

}
