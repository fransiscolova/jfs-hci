package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.RoleMappingDao;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;

/**
 * Dao implement class for {@link RoleMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class RoleMappingDaoImpl extends DaoImpl<RoleMapping, String> implements RoleMappingDao {
    private static final Logger log = LogManager.getLogger(RoleMappingDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<RoleMapping> getAllRoleMappingsByUsername(String username) {
        log.debug("get all valid role mappings by username {}", username);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("user.username", username).ignoreCase());
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RoleMapping> getAllValidRoleMappingsByUsername(String username, Date date) {
        log.debug("get all valid role mappings by username {} and date {}", username, date);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("user.username", username).ignoreCase());
        criteria.add(Restrictions.le("validFrom", date));
        criteria.add(Restrictions.ge("validTo", date));
        return criteria.list();
    }

}
