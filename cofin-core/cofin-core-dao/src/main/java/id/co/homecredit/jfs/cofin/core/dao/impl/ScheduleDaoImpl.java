package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ScheduleDao;
import id.co.homecredit.jfs.cofin.core.model.Schedule;

/**
 * Dao implements class for {@link Schedule}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class ScheduleDaoImpl extends DaoImpl<Schedule, String> implements ScheduleDao {
    private static final Logger log = LogManager.getLogger(ScheduleDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<Schedule> getAllSchedulesByContractNumber(String contractNumber) {
        log.debug("get all schedules by contract number {}", contractNumber);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("contractNumber", contractNumber));
        return criteria.list();
    }

}
