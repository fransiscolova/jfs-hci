package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.UserDao;
import id.co.homecredit.jfs.cofin.core.model.User;

/**
 * Dao implement class for {@link User}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class UserDaoImpl extends DaoImpl<User, String> implements UserDao {
    private static final Logger log = LogManager.getLogger(UserDaoImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getAllUnmappedUsers() {
        log.debug("get all unmapped users");
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.isEmpty("roleMappings"));
        criteria.setFetchMode("roleMappings", FetchMode.JOIN);
        return criteria.list();
    }

    @Override
    public User getUserByUserName(String username) {
        log.debug("get all unmapped users");
        Criteria criteria = composeCriteria(true);
        criteria.add(Restrictions.eq("username", username));
        return (User) criteria.uniqueResult();

    }

}
