package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.core.dao.VwCofinContractDetailDao;
import id.co.homecredit.jfs.cofin.core.model.VwCofinContractDetail;

/**
 * Dao Implement Class For {@link VwCofinContractDetailDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class VwCofinContractDetailDaoImpl implements VwCofinContractDetailDao{
	private static final Logger log = LogManager.getLogger(VwCofinContractDetailDaoImpl.class);
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }
    
    @Override
    public VwCofinContractDetail getDetailContractByContractNumber(String contractNumber){
		log.debug("Get Detail Contract By Contract Number {}",contractNumber);
		VwCofinContractDetail detail = new VwCofinContractDetail();
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select * from VW_COFIN_CONTRACT_DETAIL where TEXT_CONTRACT_NUMBER = '"+contractNumber+"'");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		DecimalFormat dec = new DecimalFormat("###,###,###,###,###");
		DecimalFormat per = new DecimalFormat("###,###,###,###,###.##");
		detail.setTextContractNumber(list.get(0).get("TEXT_CONTRACT_NUMBER").toString());
		detail.setJfsContractStatus(list.get(0).get("JFS_CONTRACT_STATUS").toString());
		if(list.get(0).get("STATUS")==null){
			detail.setStatus("-");
		}else{
			detail.setStatus(list.get(0).get("STATUS").toString());
		}
		if(list.get(0).get("CONTRACT_PROCESSING_TYPE")==null){
			detail.setContractProcessingType("-");
		}else{
			detail.setContractProcessingType(list.get(0).get("CONTRACT_PROCESSING_TYPE").toString());
		}
		if(list.get(0).get("REGISTRATION_STATUS")==null){
			detail.setRegistrationStatus("-");
		}else{
			detail.setRegistrationStatus(list.get(0).get("REGISTRATION_STATUS").toString());
		}
		if(list.get(0).get("CONTRACT_TYPE")==null){
			detail.setContractType("-");
		}else{
			detail.setContractType(list.get(0).get("CONTRACT_TYPE").toString());
		}
		if(list.get(0).get("LOAN_PURPOSE")==null){
			detail.setLoanPurpose("-");
		}else{
			detail.setLoanPurpose(list.get(0).get("LOAN_PURPOSE").toString());
		}
		if(list.get(0).get("PRODUCT_ID")==null){
			detail.setProductId("-");
		}else{
			detail.setProductId(list.get(0).get("PRODUCT_ID").toString());
		}
		if(list.get(0).get("PRODUCT_CODE")==null){
			detail.setProductCode("-");
		}else{
			detail.setProductCode(list.get(0).get("PRODUCT_CODE").toString());
		}
		if(list.get(0).get("PRODUCT_NAME")==null){
			detail.setProductName("-");
		}else{
			detail.setProductName(list.get(0).get("PRODUCT_NAME").toString());
		}
		if(list.get(0).get("PRODUCT_DESCRIPTION")==null){
			detail.setProductDescription("-");
		}else{
			detail.setProductDescription(list.get(0).get("PRODUCT_DESCRIPTION").toString());
		}
		if(list.get(0).get("CURRENCY_CODE")==null){
			detail.setCurrencyCode("-");
		}else{
			detail.setCurrencyCode(list.get(0).get("CURRENCY_CODE").toString());
		}
		if(list.get(0).get("SALE_DESCRIPTION")==null){
			detail.setSaleDescription("-");
		}else{
			detail.setSaleDescription(list.get(0).get("SALE_DESCRIPTION").toString());
		}
		if(list.get(0).get("PRODUCT_PROFILE_ID")==null){
			detail.setProductProfileId("-");
		}else{
			detail.setProductProfileId(list.get(0).get("PRODUCT_PROFILE_ID").toString());
		}
		if(list.get(0).get("PRODUCT_PROFILE_CODE")==null){
			detail.setProductProfileCode("-");
		}else{
			detail.setProductProfileCode(list.get(0).get("PRODUCT_PROFILE_CODE").toString());
		}
		if(list.get(0).get("PRODUCT_PROFILE_NAME")==null){
			detail.setProductProfileName("-");
		}else{
			detail.setProductProfileName(list.get(0).get("PRODUCT_PROFILE_NAME").toString());
		}
		if(list.get(0).get("APPLICATION_FORM")==null){
			detail.setApplicationForm("-");
		}else{
			detail.setApplicationForm(list.get(0).get("APPLICATION_FORM").toString());
		}
		if(list.get(0).get("INIT_TRANSACTION_TYPE")==null){
			detail.setInitTransactionType("-");
		}else{
			detail.setInitTransactionType(list.get(0).get("INIT_TRANSACTION_TYPE").toString());
		}
		if(list.get(0).get("SALESROOM_ID")==null){
			detail.setSalesroomId("-");
		}else{
			detail.setSalesroomId(list.get(0).get("SALESROOM_ID").toString());
		}
		if(list.get(0).get("BUSINESS_AREA_CODE")==null){
			detail.setBusinessAreaCode("-");
		}else{
			detail.setBusinessAreaCode(list.get(0).get("BUSINESS_AREA_CODE").toString());
		}
		if(list.get(0).get("SALESROOM_NAME")==null){
			detail.setSalesroomName("-");
		}else{
			detail.setSalesroomName(list.get(0).get("SALESROOM_NAME").toString());
		}
		if(list.get(0).get("SALESROOM_BUSINESS_MODEL_CODE")==null){
			detail.setSalesroomBusinessModelCode("-");
		}else{
			detail.setSalesroomBusinessModelCode(list.get(0).get("SALESROOM_BUSINESS_MODEL_CODE").toString());
		}
		if(list.get(0).get("SALESROOM_CATEGORY_CODE")==null){
			detail.setSalesroomCategoryCode("-");
		}else{
			detail.setSalesroomCategoryCode(list.get(0).get("SALESROOM_CATEGORY_CODE").toString());
		}
		if(list.get(0).get("PAPERLESS")==null){
			detail.setPaperless("-");
		}else{
			detail.setPaperless(list.get(0).get("PAPERLESS").toString());
		}
		if(list.get(0).get("COMMODITY_TYPE_ID")==null){
			detail.setCommodityTypeId("-");
		}else{
			detail.setCommodityTypeId(list.get(0).get("COMMODITY_TYPE_ID").toString());
		}
		if(list.get(0).get("COMMODITY_TYPE_CODE")==null){
			detail.setCommodityTypeCode("-");
		}else{
			detail.setCommodityTypeCode(list.get(0).get("COMMODITY_TYPE_CODE").toString());
		}
		if(list.get(0).get("COMMODITY_CATEGORY_CODE")==null){
			detail.setCommodityCategoryCode("-");
		}else{
			detail.setCommodityCategoryCode(list.get(0).get("COMMODITY_CATEGORY_CODE").toString());
		}
		if(list.get(0).get("BANK_COMMODITY_CATEGORY_CODE")==null){
			detail.setBankCommodityCategoryCode("-");
		}else{
			detail.setBankCommodityCategoryCode(list.get(0).get("BANK_COMMODITY_CATEGORY_CODE").toString());
		}
		if(list.get(0).get("COMMODITY_CATEGORY_DESCRIPTION")==null){
			detail.setCommodityCategoryDescription("-");
		}else{
			detail.setCommodityCategoryDescription(list.get(0).get("COMMODITY_CATEGORY_DESCRIPTION").toString());
		}
		if(list.get(0).get("HCI_COMMODITY_TYPE_CODE")==null){
			detail.setHciCommodityTypeCode("-");
		}else{
			detail.setHciCommodityTypeCode(list.get(0).get("HCI_COMMODITY_TYPE_CODE").toString());
		}
		if(list.get(0).get("BANK_COMMODITY_TYPE_CODE")==null){
			detail.setBankCommodityTypeCode("-");
		}else{
			detail.setBankCommodityTypeCode(list.get(0).get("BANK_COMMODITY_TYPE_CODE").toString());
		}
		if(list.get(0).get("COMMODITY_TYPE_DESC")==null){
			detail.setCommodityTypeDesc("-");
		}else{
			detail.setCommodityTypeDesc(list.get(0).get("COMMODITY_TYPE_DESC").toString());
		}
		if(list.get(0).get("DELIVERY_TYPE_CODE")==null){
			detail.setDeliveryTypeCode("-");
		}else{
			detail.setDeliveryTypeCode(list.get(0).get("DELIVERY_TYPE_CODE").toString());
		}
		if(list.get(0).get("MODEL_NUMBER")==null){
			detail.setModelNumber("-");
		}else{
			detail.setModelNumber(list.get(0).get("MODEL_NUMBER").toString());
		}
		if(list.get(0).get("PRODUCER")==null){
			detail.setProducer("-");
		}else{
			detail.setProducer(list.get(0).get("PRODUCER").toString());
		}
		if(list.get(0).get("SERIAL_NUMBER")==null){
			detail.setSerialNumber("-");
		}else{
			detail.setSerialNumber(list.get(0).get("SERIAL_NUMBER").toString());
		}
		if(list.get(0).get("ENGINE_NUMBER")==null){
			detail.setEngineNumber("-");
		}else{
			detail.setEngineNumber(list.get(0).get("ENGINE_NUMBER").toString());
		}
		if(list.get(0).get("GOODS_PRICE")==null){
			detail.setGoodsPrice("0");
		}else{
			detail.setGoodsPrice(dec.format(Double.parseDouble(list.get(0).get("GOODS_PRICE").toString())).replace(",","."));
		}
		if(list.get(0).get("GOODS_PRICE_CURR")==null){
			detail.setGoodsPriceCurr("-");
		}else{
			detail.setGoodsPriceCurr(list.get(0).get("GOODS_PRICE_CURR").toString());
		}
		if(list.get(0).get("UNTAXED_PRICE_AMOUNT")==null){
			detail.setUntaxedPriceAmount("0,00");
		}else{
			String untaxed = per.format(Double.parseDouble(list.get(0).get("UNTAXED_PRICE_AMOUNT").toString().replace(",","."))).replace(".","~");
			untaxed = untaxed.replace(",",".");
			detail.setUntaxedPriceAmount(untaxed.replace("~",","));
		}
		if(list.get(0).get("UNTAXED_PRICE_CURR")==null){
			detail.setUntaxedPriceCurr("-");
		}else{
			detail.setUntaxedPriceCurr(list.get(0).get("UNTAXED_PRICE_CURR").toString());
		}
		if(list.get(0).get("SERVICE_TYPE_CODE")==null){
			detail.setServiceTypeCode("-");
		}else{
			detail.setServiceTypeCode(list.get(0).get("SERVICE_TYPE_CODE").toString());
		}
		if(list.get(0).get("SERVICE_CODE")==null){
			detail.setServiceCode("-");
		}else{
			detail.setServiceCode(list.get(0).get("SERVICE_CODE").toString());
		}
		if(list.get(0).get("SERVICE_NAME")==null){
			detail.setServiceName("-");
		}else{
			detail.setServiceName(list.get(0).get("SERVICE_NAME").toString());
		}
		if(list.get(0).get("SERVICE_DESCRIPTION")==null){
			detail.setServiceDescription("-");
		}else{
			detail.setServiceDescription(list.get(0).get("SERVICE_DESCRIPTION").toString());
		}
		if(list.get(0).get("TARIFF_CODE")==null){
			detail.setTariffCode("-");
		}else{
			detail.setTariffCode(list.get(0).get("TARIFF_CODE").toString());
		}
		if(list.get(0).get("TARIFF_NAME")==null){
			detail.setTariffName("-");
		}else{
			detail.setTariffName(list.get(0).get("TARIFF_NAME").toString());
		}
		if(list.get(0).get("TARIFF_TYPE_CODE")==null){
			detail.setTariffTypeCode("-");
		}else{
			detail.setTariffTypeCode(list.get(0).get("TARIFF_TYPE_CODE").toString());
		}
		if(list.get(0).get("TARIFF_CURRENCY_CODE")==null){
			detail.setTariffCurrencyCode("-");
		}else{
			detail.setTariffCurrencyCode(list.get(0).get("TARIFF_CURRENCY_CODE").toString());
		}
		return detail;
	}
	
}
