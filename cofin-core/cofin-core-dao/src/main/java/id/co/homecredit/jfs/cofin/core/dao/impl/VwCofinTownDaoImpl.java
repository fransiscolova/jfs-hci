package id.co.homecredit.jfs.cofin.core.dao.impl;

import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import id.co.homecredit.jfs.cofin.core.dao.VwCofinTownDao;

/**
 * Dao Implements Class For {@link VwCofinTownDao}
 * 
 * @author denny.afrizal01
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class VwCofinTownDaoImpl implements VwCofinTownDao{
	private static final Logger log = LogManager.getLogger(VwCofinTownDaoImpl.class);
	
	@Autowired
    protected SessionFactory sfJFS;
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sfJFS = sessionFactory;
    }
    
    @Override
	public String getTownDescription(String townCode){
		log.debug("Get Town Description By Town Code {}",townCode);
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select * from VW_COFIN_TOWN where TOWN_CODE = '"+townCode+"'");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		if(list.size()==0){
			return townCode;
		}else{
			return list.get(0).get("TOWN_NAME").toString();
		}
	}
	
	@Override
	public String getSubdistrictDescription(String subdistrictCode){
		log.debug("Get Subdistrict Description By Subdistrict Code {}",subdistrictCode);
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select * from VW_COFIN_TOWN where SUBDISTRICT_CODE = '"+subdistrictCode+"'");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		if(list.size()==0){
			return subdistrictCode;
		}else{
			return list.get(0).get("SUBDISTRICT_NAME").toString();
		}
	}
	
	@Override
	public String getDistrictDescription(String districtCode){
		log.debug("Get District Description By District Code {}",districtCode);
		Session session = sfJFS.openSession();
		Query query = session.createSQLQuery("select * from VW_COFIN_TOWN where DISTRICT_CODE = '"+districtCode+"'");
		log.debug(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> list = query.list();
		if(list.size()==0){
			return districtCode;
		}else{
			return list.get(0).get("DISTRICT_NAME").toString();
		}
	}
    
}
