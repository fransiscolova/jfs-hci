package id.co.homecredit.jfs.cofin.core.dao.service;

import java.net.Authenticator;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.rmi.RemoteException;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.homecredit.hss.ws.customer.customerservice.v5.CustomerWSv5;
import net.homecredit.hss.ws.customer.customerservice.v5.CustomerWSv5Soap11QSService;
import net.homecredit.hss.ws.customer.customerservice.v5.GetCustomerRequest;
import net.homecredit.hss.ws.customer.customerservice.v5.GetCustomerResponse;

/**
 * Service Class For Customer Service V5 WSDL
 * 
 * @author denny.afrizal01
 *
 */
public class CustomerServiceV5Service {
	public static final Logger log = LogManager.getLogger(CustomerServiceV5Service.class);
	
	private CustomerWSv5 customerWSv5;
	
	private String endpoint;
	private String username;
	private String password;
	public CustomerServiceV5Service(String endpoint,String username,String password){
		this.endpoint=endpoint;
		this.username=username;
		this.password=password;
	}
	
	private CustomerWSv5 getCustomerWSv5()throws MalformedURLException{
		log.debug("getCustomerWSv5, url = {}, username = {}, password = {}",endpoint,username,password);
		if(customerWSv5 != null){
			log.info("customerWSv5 Is Not Null");
			return customerWSv5;
		}else{
			CookieHandler.setDefault(new CookieManager(null,CookiePolicy.ACCEPT_ALL));
			Authenticator.setDefault(new Authenticator(){
				@Override
				protected PasswordAuthentication getPasswordAuthentication(){
					return new PasswordAuthentication(username,password.toCharArray());
				}
			});
			URL url = new URL(endpoint);
			QName qname = new QName("http://homecredit.net/hss/ws/customer/customerservice/v5","CustomerWSv5Soap11QSService");
			CustomerWSv5Soap11QSService service = new CustomerWSv5Soap11QSService(url,qname);
			log.info("Access {}",endpoint);
			CustomerWSv5 customerWSv5 = service.getCustomerWSv5Soap11QSPort();
			BindingProvider prov = (BindingProvider) customerWSv5;
			prov.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,endpoint);
			prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY,username);
			prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY,password);
			this.customerWSv5=customerWSv5;
			return customerWSv5;
		}
	}
	
	public GetCustomerResponse getCustomer(GetCustomerRequest request)throws MalformedURLException,RemoteException{
		customerWSv5=getCustomerWSv5();
		GetCustomerResponse response = customerWSv5.getCustomer(request);
		return response;
	}
	
}
