package id.co.homecredit.jfs.cofin.core.dao.upload;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;

/**
 * Dao interface class for {@link Action}.
 *
 * @author Fransisco.situmorang
 *
 */
public interface CofinConfirmFileDao extends Dao<CofinConfirmFile, String> {

	/*
	 * get confirmfile byid
	 * 
	 * @param id
	 * 
	 * return CofinConfirmFile;
	 */
	
	public CofinConfirmFile getByID(String id) ;
	
	/*
	 * get confirmFile by processId
	 * 
	 * @param id
	 * 
	 * return List<CofinConfirmFile>
	 * 
	 */
	
	 public List<CofinConfirmFile> getFile(String processId);
	 
	 
	
	
	
}
