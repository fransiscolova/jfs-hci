package id.co.homecredit.jfs.cofin.core.dao.upload;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmProcess;

/**
 * Dao interface class for {@link Action}.
 *
 * @author Fransisco.situmorang
 *
 */
public interface CofinConfirmProcessDao extends Dao<CofinConfirmProcess, String> {

	/*
	 * get process by partnerId
	 * 
	 * @param partnerId
	 * 
	 * @return List<CofinConfirmProcess>
	 */

	public List<CofinConfirmProcess> getProcessByPartner(String partnerId);

	/*
	 * get process by partnerId
	 * 
	 * @param partnerId
	 * 
	 * @return List<CofinConfirmProcess>
	 */

	public List<CofinConfirmProcess> getProcessByPartnerAndIdType(String partnerId, String processTypeId);

}
