package id.co.homecredit.jfs.cofin.core.dao.upload;

import java.util.List;

import id.co.homecredit.jfs.cofin.common.dao.Dao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;

/**
 * Dao interface class for {@link Action}.
 *
 * @author Fransisco.situmorang
 *
 */
public interface CofinProcessTypeDao extends Dao<CofinProcessType, String> {

	void runProcedurePermata(String query);
	
	
	
	public List<CofinProcessType> getByCategoryId(String categoryId);



	void executeProcedure(String query);
	

}
