package id.co.homecredit.jfs.cofin.core.dao.upload.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.FileUploadDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmFileDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;

/**
 * Dao implement class for {@link Action}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class CofinConfirmFileDaoImpl extends DaoImpl<CofinConfirmFile, String> implements CofinConfirmFileDao {
	private static final Logger log = LogManager.getLogger(CofinConfirmFileDaoImpl.class);

	@Override
	public CofinConfirmFile getByID(String id) {
		log.debug("get CofinConfirmFile by id {}", id);
		Criteria criteria = createCriteria();
		//criteria.createAlias("cofinConfirmProcess", "cofinConfirmProcess");
		//criteria.createAlias("cofinConfirmProcess.cofinProcessTypes", "cofinProcessType");		
		criteria.add(Restrictions.eq("id", id));
		//criteria.add(Restrictions.eq("cofinProcessType.period", "Daily"));
		return (CofinConfirmFile) criteria.uniqueResult();
	}

	
	
	@Override
	public List<CofinConfirmFile> getFile(String processId) {
		// TODO Auto-generated method stub
		log.debug("get CofinConfirmFile by processId {}", processId);
		Criteria criteria=createCriteria();
		criteria.createAlias("cofinConfirmProcess", "cofinConfirmProcess" );
		criteria.add(Restrictions.eq("cofinConfirmProcess.id", processId));		
		return criteria.list();
	}

	

}
