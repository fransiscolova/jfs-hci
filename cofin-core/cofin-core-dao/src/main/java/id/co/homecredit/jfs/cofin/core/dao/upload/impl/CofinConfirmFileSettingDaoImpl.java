package id.co.homecredit.jfs.cofin.core.dao.upload.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.FileUploadDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmFileDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmFileSettingDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFileSetting;

/**
 * Dao implement class for {@link Action}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class CofinConfirmFileSettingDaoImpl extends DaoImpl<CofinConfirmFileSetting, String> implements CofinConfirmFileSettingDao {
	private static final Logger log = LogManager.getLogger(CofinConfirmFileSettingDaoImpl.class);


	@Override
	public List<CofinConfirmFileSetting> getHeaderRecord(String fk,String recordType) {
		log.debug("get CofinConfirmFileSetting by fk,recordtype {}", fk,recordType);
		Criteria criteria = createCriteria();
		criteria.createAlias("cofinConfirmFile", "cofinConfirmFile");
		criteria.add(Restrictions.eq("recordType", recordType));
		criteria.add(Restrictions.neOrIsNotNull("columnDestination", "x"));		
		criteria.add(Restrictions.eq("cofinConfirmFile.id", fk));			
		return criteria.list();
	
	}

	

}
