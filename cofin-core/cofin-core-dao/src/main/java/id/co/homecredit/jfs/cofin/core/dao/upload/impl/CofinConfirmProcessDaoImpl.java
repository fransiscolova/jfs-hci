package id.co.homecredit.jfs.cofin.core.dao.upload.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.FileUploadDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmFileDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmProcessDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmProcess;

/**
 * Dao implement class for {@link Action}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class CofinConfirmProcessDaoImpl extends DaoImpl<CofinConfirmProcess, String> implements CofinConfirmProcessDao {
	private static final Logger log = LogManager.getLogger(CofinConfirmProcessDaoImpl.class);

	@Override
	public List<CofinConfirmProcess> getProcessByPartner(String partnerId) {
		// TODO Auto-generated method stub
		log.debug("get CofinConfirmFile by partnerId {}", partnerId);
		Criteria criteria=createCriteria();
		criteria.createAlias("partner", "partner");
		criteria.add(Restrictions.eq("partner.id", partnerId));
		return criteria.list();
	}

	@Override
	public List<CofinConfirmProcess> getProcessByPartnerAndIdType(String partnerId, String processTypeId) {
		// TODO Auto-generated method stub
		log.debug("get CofinConfirmFile by partnerId and processId{}", partnerId,processTypeId);
		Criteria criteria=createCriteria();
		criteria.createAlias("partner", "partner");
		criteria.createAlias("cofinProcessType", "cofinProcessType");		
		criteria.add(Restrictions.eq("partner.id", partnerId));
		criteria.add(Restrictions.eq("cofinProcessType.id", processTypeId));
		return criteria.list();
	}



}
