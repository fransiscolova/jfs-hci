package id.co.homecredit.jfs.cofin.core.dao.upload.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.FileUploadDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmFileDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinProcessCategoryDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinProcessTypeDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessCategory;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;

/**
 * Dao implement class for {@link Action}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class CofinProcessCategoryDaoImpl extends DaoImpl<CofinProcessCategory, String> implements CofinProcessCategoryDao {
	private static final Logger log = LogManager.getLogger(CofinProcessCategoryDaoImpl.class);

	
	 @Autowired
	    private SessionFactory sessionFactory;
	    
	    
	    @Override
	    public void runProcedurePermata(String query) {
	        //log.debug("run procedure {}", query);
	        Query querys =getSession().createSQLQuery(query);       
	        querys.executeUpdate();
	        //log.debug("finish run procedure {}", query);
	    }

	   /**
	    * Get current session.
	    *
	    * @return session
	    */
	   private Session getSession() {
	       return sessionFactory.getCurrentSession();
	   }
	

	

}
