package id.co.homecredit.jfs.cofin.core.dao.upload.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.common.dao.impl.DaoImpl;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.FileUploadDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmFileDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinProcessTypeDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;

/**
 * Dao implement class for {@link Action}.
 *
 * @author fransisco.situmorang
 *
 */
@Repository
public class CofinProcessTypeDaoImpl extends DaoImpl<CofinProcessType, String> implements CofinProcessTypeDao {
	private static final Logger log = LogManager.getLogger(CofinProcessTypeDaoImpl.class);

	
	 @Autowired
	    private SessionFactory sessionFactory;
	    
	    
	    @Override
	    public void runProcedurePermata(String query) {
	        //log.debug("run procedure {}", query);
			Query querys =getSession().createQuery(query);
	        querys.executeUpdate();      
	        log.debug("finish run procedure {}", query.toString());
	    }
	    
	    
	    @Override
	    public void executeProcedure(String query) {
	        //log.debug("param {}", param);
	        SQLQuery sqlQuery = getSession().createSQLQuery(query);
	         sqlQuery.executeUpdate();
	    }

	   /**
	    * Get current session.
	    *
	    * @return session
	    */
	   private Session getSession() {
	       return sessionFactory.getCurrentSession();
	   }

	@Override
	public List<CofinProcessType> getByCategoryId(String categoryId) {
		// TODO Auto-generated method stub
		Criteria criteria=createCriteria();
		criteria.createAlias("cofinProcessCategory", "cofinProcessCategory");
		criteria.add(Restrictions.eq("cofinProcessCategory.id",categoryId));
		
		return criteria.list();
	}
	

	
	 
	   
	   
	   
	   
}
