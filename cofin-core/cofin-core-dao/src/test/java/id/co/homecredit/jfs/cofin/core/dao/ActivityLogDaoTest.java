package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.AuditLog;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.ActivityEnum;

/**
 * Test class for {@link ActivityLogDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class ActivityLogDaoTest extends BaseDaoTest {

    @Autowired
    private ActivityLogDao activityLogDao;

    @Test
    public void get() {
        List<AuditLog> activityLogs = activityLogDao.getAll(true);
        Assert.assertFalse(activityLogs.isEmpty());
        for (AuditLog activityLog : activityLogs) {
            System.out.println(activityLog.toString());
        }
    }

    @Test
    public void save() {
        AuditLog activityLog = new AuditLog();
        activityLog.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        activityLog.setActivity(ActivityEnum.INSERT);
        activityLog.setDataBefore(null);
        activityLog.setDataAfter(null);
        activityLog = activityLogDao.save(activityLog);
        Assert.assertNotNull(activityLog);
        System.out.println(activityLog);
    }

}
