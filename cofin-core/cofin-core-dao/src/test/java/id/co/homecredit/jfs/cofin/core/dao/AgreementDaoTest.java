package id.co.homecredit.jfs.cofin.core.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.ProductMapping;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PartnerEnum;

/**
 * Test class for {@link AgreementDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class AgreementDaoTest extends BaseDaoTest {

    @Autowired
    private AgreementDao agreementDao;
    @Autowired
    private PartnerDao partnerDao;

    @Test
    public void getAgreementByIdAlias() {
        String idAlias = "51";
        Agreement agreement = agreementDao.getAgreementByAliasId(idAlias);
        Assert.assertNotNull(agreement);
        System.out.println(agreement.toString());
    }

    @Test
    public void getAll() {
        List<Agreement> agreements = agreementDao.getAll(true);
        Assert.assertFalse(agreements.isEmpty());
        System.out.println("size is " + agreements.size());
        for (Agreement agreement : agreements) {
            System.out.println(agreement.toString());
        }
    }

    @Test
    public void getAllAgreementByPartnerCode() {
        String partnerCode = PartnerEnum.PERMATA.name();
        List<Agreement> agreements = agreementDao.getAllAgreementsByPartnerCode(partnerCode);
        Assert.assertFalse(agreements.isEmpty());
    }

    @Test
    public void getAllProductMappings() {
        String partnerCode = PartnerEnum.PERMATA.name();
        List<Agreement> agreements = agreementDao.getAllAgreementsByPartnerCode(partnerCode);
        for (Agreement agreement : agreements) {
            List<ProductMapping> productMappings = agreement.getProductMappings();
            Assert.assertFalse(productMappings.isEmpty());
            for (ProductMapping productMapping : productMappings) {
                System.out.println(productMapping.toString());
            }
        }
    }

    @Test
    public void save() throws ParseException {
        Agreement agreement = new Agreement();
        agreement.setAgreementType(AgreementTypeEnum.EZ10);
        agreement.setCode("NO. PKS.087/DIR/TFI/VI/2014");
        agreement.setName("PERMATA JFS AGREEMENT for " + AgreementTypeEnum.EZ10.getDescription());
        agreement.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        agreement.setCreatedDate(new Date());
        agreement.setValidFrom(new Date());
        agreement.setValidTo(DateUtil.stringToDate("3000-12-31"));
        agreement.setAliasId("52");
        agreement.setPartner(partnerDao.getPartnerByCode(PartnerEnum.PERMATA.name()));
        agreement = agreementDao.save(agreement);
        System.out.println(agreement.toString());
        Assert.assertNotNull(agreement.getId());
    }
}
