package id.co.homecredit.jfs.cofin.core.dao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.AgreementDetail;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementEnum;

/**
 * Test class for {@link AgreementDetailDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class AgreementDetailDaoTest extends BaseDaoTest {

    @Autowired
    private AgreementDetailDao agreementDetailDao;
    @Autowired
    private AgreementDao agreementDao;

    @Test
    public void getActiveAgreementDetailByAgreementIdAndDate() {
        String agreementId = AgreementEnum.BTPN_REG.getId();
        Date date = new Date();
        AgreementDetail agreementDetail = agreementDetailDao
                .getActiveAgreementDetailByAgreementIdAndDate(agreementId, date);
        Assert.assertNotNull(agreementDetail);
        System.out.println(agreementDetail.toString());
    }

    @Test
    public void getAllAgreementDetails() {
        String agreementId = AgreementEnum.BTPN_REG.getId();
        List<AgreementDetail> agreementDetails = agreementDao.get(agreementId)
                .getAgreementDetails();
        Assert.assertFalse(agreementDetails.isEmpty());
        for (AgreementDetail agreementDetail : agreementDetails) {
            System.out.println(agreementDetail.toString());
        }
    }

    @Test
    public void save() throws ParseException {
        String agreementId = AgreementEnum.BTPN_REG.getId();
        AgreementDetail agreementDetail = new AgreementDetail();
        agreementDetail.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        agreementDetail.setCreatedDate(new Date());
        agreementDetail.setAgreement(agreementDao.get(agreementId));
        agreementDetail.setAdminFeeRate(new BigDecimal("0.01"));
        agreementDetail.setInterestRate(new BigDecimal("28"));
        agreementDetail.setPenaltySplitRate(new BigDecimal("0"));
        agreementDetail.setPrincipalSplitRate(new BigDecimal("0.9"));
        agreementDetail.setValidFrom(DateUtil.stringToDate("2016-12-01"));
        agreementDetail.setValidTo(DateUtil.stringToDate("2017-10-01"));
        agreementDetail = agreementDetailDao.save(agreementDetail);
        Assert.assertNotNull(agreementDetail.getId());
        System.out.println(agreementDetail.toString());
    }

}
