package id.co.homecredit.jfs.cofin.core.dao;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;

/**
 * Abstract test class for core dao.
 *
 * @author muhammad.muflihun
 *
 */
@ContextConfiguration(locations = { "classpath*:cofin-core-dao-test-context.xml" })
public abstract class BaseDaoTest extends AbstractTransactionalTestNGSpringContextTests {

}
