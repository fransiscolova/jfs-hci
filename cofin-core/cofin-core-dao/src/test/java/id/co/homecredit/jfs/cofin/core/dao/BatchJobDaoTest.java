package id.co.homecredit.jfs.cofin.core.dao;

import static org.testng.Assert.assertNotNull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.BatchJob;

/**
 * Test class for {@link BatchJobDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class BatchJobDaoTest extends BaseDaoTest {

    @Autowired
    private BatchJobDao batchJobDao;

    @Test
    public void getAll() {
        List<BatchJob> bjs = batchJobDao.getAll(true);
        System.out.println(bjs.size());
        for (BatchJob bj : bjs) {
            System.out.println(bj.toString());
        }
    }

    @Test
    public void getCronExp() {
        String partnerName = "BTPN";
        System.out.println(batchJobDao.getCronExpressionByPartnerName(partnerName));
    }

    @Test
    public void getFlagGenerate() {
        String partnerName = "BTPN";
        Boolean batchJob = batchJobDao.isFlagGenerateActiveByPartnerName(partnerName);
        assertNotNull(batchJob);
        System.out.println(batchJob.toString());
    }

}
