package id.co.homecredit.jfs.cofin.core.dao;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.BatchStep;

/**
 * @author muhammad.muflihun
 *
 */
public class BatchStepDaoTest extends BaseDaoTest {

    @Autowired
    private BatchStepDao batchStepDao;

    @Test
    public void getNextStepName() {
        String stepName = "flow";
        String nextStep = batchStepDao.getNextStepNameByStepName(stepName);
        assertNotNull(nextStep);
        System.out.println(nextStep);
    }

    @Test
    public void getPrioritized() {
        String partnerName = "BTPN";
        List<BatchStep> batchSteps = batchStepDao
                .getAllBatchStepPrioritizedByPartnerName(partnerName);
        assertFalse(batchSteps.isEmpty());
        for (BatchStep batchStep : batchSteps) {
            System.out.println(batchStep.toString());
        }
    }

}
