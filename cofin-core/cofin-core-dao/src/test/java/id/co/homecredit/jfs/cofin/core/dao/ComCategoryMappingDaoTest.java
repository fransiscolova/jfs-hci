package id.co.homecredit.jfs.cofin.core.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.ComCategoryMapping;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CommodityCategoryEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PartnerEnum;

/**
 * Class test for {@link ComCategoryMappingDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class ComCategoryMappingDaoTest extends BaseDaoTest {

    @Autowired
    private ComCategoryMappingDao commodityMappingDao;
    @Autowired
    private CommodityCategoryDao commodityCategoryDao;
    @Autowired
    private PartnerDao partnerDao;

    @Test
    public void save() {
        PartnerEnum partnerEnum = PartnerEnum.BTPN;
        String partnerId = partnerEnum.getId();
        CommodityCategoryEnum categoryEnum = CommodityCategoryEnum.CAT_GG;

        ComCategoryMapping commodityMapping = new ComCategoryMapping();
        commodityMapping.setPartner(partnerDao.get(partnerId));
        commodityMapping.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        commodityMapping.setCommodityCategory(commodityCategoryDao.get(categoryEnum.name()));
        commodityMapping.setPartnerCommodityCategory("BTPN_CAT");
        commodityMapping = commodityMappingDao.save(commodityMapping);
        System.out.println(commodityMapping);
    }

}
