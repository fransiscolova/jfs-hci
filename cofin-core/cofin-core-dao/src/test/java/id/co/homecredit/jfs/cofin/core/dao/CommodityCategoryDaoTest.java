package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.CommodityCategory;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CommodityCategoryEnum;

/**
 * Test class for {@link CommodityCategoryDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class CommodityCategoryDaoTest extends BaseDaoTest {

    @Autowired
    private CommodityCategoryDao commodityCategoryDao;

    @Test
    public void save() {
        for (CommodityCategoryEnum enu : CommodityCategoryEnum.values()) {
            CommodityCategory commodityCategory = new CommodityCategory();
            commodityCategory.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
            commodityCategory.setCreatedDate(new Date());
            commodityCategory.setCode(enu.name());
            commodityCategory.setDescription(enu.getDescription());
            commodityCategory = commodityCategoryDao.save(commodityCategory);
            System.out.println(commodityCategory);
        }
    }

}
