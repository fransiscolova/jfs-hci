package id.co.homecredit.jfs.cofin.core.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

/**
 * Test class for {@link CommonBatchDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class CommonBatchDaoTest extends BaseDaoTest {

    @Autowired
    private CommonBatchDao commonBatchDao;

    @Test
    public void executeSql() {
        String query = "select 'HCI|5|'||to_char(sysdate,'ddMMyyhh24miss')||'|'||(select code from cofin_agreement where alias_id = '2')||chr(13) from dual";
        System.out.println(commonBatchDao.executeQuery(query));
    }

    @Test
    public void executeSqlProcedure() {
        String query = "{ call P_JFS_SALESROOM(?) }";
        String param = "2017-11-27_14:45:09";
        System.out.println(commonBatchDao.executeProcedureWithParam(query, param));
    }
}
