package id.co.homecredit.jfs.cofin.core.dao;

import static org.testng.Assert.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.Contract;

/**
 * Test class for {@link ContractDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class ContractDaoTest extends BaseDaoTest {

    @Autowired
    private ContractDao contractDao;

    @Test
    public void getContractByContractNumber() {
        String contractNumber = "";
        Contract contract = contractDao.getContractByContractNumber(contractNumber);
        assertNotNull(contract);
        System.out.println(contract.toString());
    }

}
