package id.co.homecredit.jfs.cofin.core.dao;

import static org.testng.Assert.assertFalse;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.ContractStatus;

/**
 * Test class for {@link ContractStatusDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class ContractStatusDaoTest extends BaseDaoTest {

    @Autowired
    private ContractStatusDao contractStatusDao;

    @Test
    public void getAllContractStatusesByContractNumber() {
        String contractNumber = "";
        List<ContractStatus> contractStatuses = contractStatusDao
                .getAllContractStatusesByContractNumber(contractNumber);
        assertFalse(contractStatuses.isEmpty());
        for (ContractStatus contractStatus : contractStatuses) {
            System.out.println(contractStatus);
        }
    }

}
