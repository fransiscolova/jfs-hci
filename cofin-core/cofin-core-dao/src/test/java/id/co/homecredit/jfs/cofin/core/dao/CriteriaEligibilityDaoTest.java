package id.co.homecredit.jfs.cofin.core.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.EligibilityEnum;

/**
 * @author muhammad.muflihun
 *
 */
public class CriteriaEligibilityDaoTest extends BaseDaoTest {

    @Autowired
    private CriteriaEligibilityDao criteriaEligibilityDao;
    @Autowired
    private CriteriaMappingDao CriteriaMappingDao;

    @Test
    public void getCriteriaEligibilityByAgreementId() {
        String agreementId = AgreementEnum.PERMATA_REG.getId();
        List<CriteriaEligibility> criteriaEligibilities = criteriaEligibilityDao
                .getAllCriteriaEligibilitiesByAgreementId(agreementId);
        Assert.assertFalse(criteriaEligibilities.isEmpty());
        for (CriteriaEligibility criteriaEligibility : criteriaEligibilities) {
            System.out.println(criteriaEligibility);
        }
    }

    @Test
    public void save() throws ParseException {
        String criteriaMappingId = "CA75DED608B7490EBDE6E2841EE512BE";
        String value = "CAT_MP; CAT_SP";
        EligibilityEnum eligibility = EligibilityEnum.IN;

        CriteriaEligibility criteriaMappingEligibility = new CriteriaEligibility();
        criteriaMappingEligibility.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        criteriaMappingEligibility.setCreatedDate(new Date());
        criteriaMappingEligibility.setValidFrom(DateUtil.stringToDate("2017-20-01"));
        criteriaMappingEligibility.setValidTo(DateUtil.stringToDate("3000-20-01"));
        criteriaMappingEligibility.setCriteriaMapping(CriteriaMappingDao.get(criteriaMappingId));
        criteriaMappingEligibility.setEligibility(eligibility);
        criteriaMappingEligibility.setValue(value);
        criteriaMappingEligibility = criteriaEligibilityDao.save(criteriaMappingEligibility);
        Assert.assertNotNull(criteriaMappingEligibility.getId());
        System.out.println(criteriaMappingEligibility.toString());
    }

}
