package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;

/**
 * Test class for {@link CriteriaMappingDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class CriteriaMappingDaoTest extends BaseDaoTest {

    @Autowired
    private CriteriaMappingDao criteriaMappingDao;
    @Autowired
    private AgreementDao agreementDao;

    @Test
    public void getAllCriteriaMappingsByAgreementId() {
        String agreementId = AgreementEnum.PERMATA_REG.getId();
        List<CriteriaMapping> criteriaMappings = criteriaMappingDao
                .getAllCriteriaMappingsByAgreementId(agreementId);
        Assert.assertFalse(criteriaMappings.isEmpty());
        for (CriteriaMapping criteriaMapping : criteriaMappings) {
            System.out.println(criteriaMapping);
        }
    }

    @Test
    public void getCriteriaMappingById() {
        String id = "B6501A52C0E34790893E8EA7282196DC";
        CriteriaMapping criteriaMapping = criteriaMappingDao.get(id);
        Assert.assertNotNull(criteriaMapping);
    }

    @Test
    public void save() {
        AgreementEnum agreementEnum = AgreementEnum.BTPN_REG;
        CriteriaEnum criteriaEnum = CriteriaEnum.GROUP;
        String parentId = "5C11763E12EF4EF19CE7D714EAB680A7";

        CriteriaMapping criteriaMapping = new CriteriaMapping();
        criteriaMapping.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        criteriaMapping.setAgreement(agreementDao.get(agreementEnum.getId()));
        criteriaMapping.setCriteria(criteriaEnum);
        criteriaMapping.setAliasId("PERMATA_REG_108_02");
        criteriaMapping.setCriteriaMappingParent(criteriaMappingDao.get(parentId));
        criteriaMapping.setCriteriaLevel(1);
        criteriaMapping = criteriaMappingDao.save(criteriaMapping);
        Assert.assertNotNull(criteriaMapping.getId());
        System.out.println(criteriaMapping);
    }

}
