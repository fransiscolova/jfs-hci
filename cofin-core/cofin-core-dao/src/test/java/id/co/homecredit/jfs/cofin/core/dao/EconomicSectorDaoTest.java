package id.co.homecredit.jfs.cofin.core.dao;

import static org.testng.Assert.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.EconomicSector;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PartnerEnum;

/**
 * Test class for {@link EconomicSectorDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class EconomicSectorDaoTest extends BaseDaoTest {

    @Autowired
    private EconomicSectorDao economicSectorDao;
    @Autowired
    private PartnerDao partnerDao;

    @Test
    public void save() {
        String bankCommodityGroup = "DGN24";
        String biEconomicSectorCode = "003100";
        String description = "Rumah Tangga untuk Pemilikan Furnitur dan Peralatan Rumah Tangga";

        Partner partner = partnerDao.get(PartnerEnum.BTPN.getId());
        EconomicSector economicSector = new EconomicSector();
        economicSector.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        economicSector.setPartner(partner);
        economicSector.setBankCommodityGroup(bankCommodityGroup);
        economicSector.setBiEconomicSectorCode(biEconomicSectorCode);
        economicSector.setDescription(description);
        economicSector = economicSectorDao.save(economicSector);
        assertNotNull(economicSector.getId());
        System.out.println(economicSector.toString());
    }
}
