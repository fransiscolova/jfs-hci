package id.co.homecredit.jfs.cofin.core.dao;

import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.ExportLog;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;

/**
 * Test class for {@link ExportLogDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class ExportLogDaoTest extends BaseDaoTest {

    @Autowired
    private ExportLogDao exportLogDao;
    @Autowired
    private BatchStepExecutionDao batchStepExecutionDao;

    @Test
    public void getLatestExportLog() throws ParseException {
        Long stepExecutionId = 0L;
        AgreementTypeEnum agreementType = AgreementTypeEnum.REG;
        Date date = new Date();
        ExportLog exportLog = exportLogDao
                .getLatestExportLogByStepExecutionIdAndAgreementTypeAndDate(stepExecutionId,
                        agreementType, date);
        Assert.assertNotNull(exportLog);
        System.out.println(exportLog.toString());
    }

    @Test
    public void save() {
        Long stepExecutionId = 0L;
        batchStepExecutionDao.get(stepExecutionId);
        ExportLog exportLog = new ExportLog();
        exportLog.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        exportLog.setFilename("/var/var/var/var.txt");
        exportLog.setStepExecutionId(stepExecutionId);
        exportLog = exportLogDao.save(exportLog);
        Assert.assertNotNull(exportLog.getId());
        System.out.println(exportLog.toString());
    }

}
