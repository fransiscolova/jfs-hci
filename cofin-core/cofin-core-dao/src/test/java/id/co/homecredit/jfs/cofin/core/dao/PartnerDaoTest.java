package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PartnerEnum;

/**
 * Test class for {@link PartnerDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class PartnerDaoTest extends BaseDaoTest {
    @Autowired
    private PartnerDao partnerDao;

    @Test
    public void getAll() {
        List<Partner> partners = partnerDao.getAll(false);
        Assert.assertNotNull(partners);
        for (Partner partner : partners) {
            System.out.println(partner.toString());
        }
    }

    @Test
    public void getPartnerAgreements() {
        String code = PartnerEnum.PERMATA.name();
        Partner partner = partnerDao.getPartnerByCode(code);
        Assert.assertNotNull(partner);
        System.out.println(partner.getAgreements());
    }

    @Test
    public void save() {
        Partner partner = new Partner();
        partner.setCode("BTPN");
        partner.setName("Bank Tabungan Pensiun Negara");
        partner.setAddress(
                "Menara BTPN - CBD Mega Kuningan Jl. Dr. Ide Anak Agung Gde Agung Kav 5.5 - 5.6");
        partner.setContactName("Prasetyo");
        partner.setContactPhone("02130026200");
        partner.setContactEmail("info@btpn.com");
        // partner.setIsEligibilityChecked(isEligibilityChecked);
        partner.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        partner.setCreatedDate(new Date());
        partner = partnerDao.save(partner);
        System.out.println(partner.toString());
        Assert.assertNotNull(partner.getId());
    }
}
