package id.co.homecredit.jfs.cofin.core.dao;

import static org.testng.Assert.assertFalse;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.PaymentInt;

/**
 * Test class for {@link PaymentIntDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class PaymentIntDaoTest extends BaseDaoTest {

    @Autowired
    private PaymentIntDao paymentIntDao;

    @Test
    public void getAllPaymentIntsByContractNumber() {
        String contractNumber = "3300016291";
        List<PaymentInt> paymentInts = paymentIntDao
                .getAllPaymentIntsByContractNumber(contractNumber);
        assertFalse(paymentInts.isEmpty());
        for (PaymentInt paymentInt : paymentInts) {
            System.out.println(paymentInt.toString());
        }
    }

}
