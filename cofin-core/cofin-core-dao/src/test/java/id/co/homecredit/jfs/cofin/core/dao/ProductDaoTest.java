package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.Product;
import id.co.homecredit.jfs.cofin.core.model.ProductMapping;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;

/**
 * Test class for {@link ProductDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class ProductDaoTest extends BaseDaoTest {

    @Autowired
    private ProductDao productDao;

    @Test
    public void getAllProductMappings() {
        String productCode = "MP_AP";
        Product product = productDao.get(productCode);
        List<ProductMapping> productMappings = product.getProductMappings();
        Assert.assertFalse(productMappings.isEmpty());
        for (ProductMapping productMapping : productMappings) {
            System.out.println(productMapping.toString());
        }
    }

    @Test
    public void save() {
        Product product = new Product();
        product.setCode("test");
        product.setName("tist");
        product.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        product.setCreatedDate(new Date());
        product = productDao.save(product);
        Assert.assertNotNull(product);
        System.out.println(product.toString());
    }
}
