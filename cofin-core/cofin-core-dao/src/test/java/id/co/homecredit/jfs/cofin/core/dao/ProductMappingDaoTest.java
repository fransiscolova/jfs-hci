package id.co.homecredit.jfs.cofin.core.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.ProductMapping;

/**
 * Test class for {@link ProductMappingDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class ProductMappingDaoTest extends BaseDaoTest {

    @Autowired
    private ProductMappingDao productMappingDao;

    @Test
    public void getAllProductMapping() {
        List<ProductMapping> productMappings = productMappingDao.getAll(false);
        Assert.assertNotNull(productMappings);
        for (ProductMapping productMapping : productMappings) {
            System.out.println(productMapping.toString());
        }
    }

    @Test
    public void getProductMappingByProductCodeAndPartnerCode() {
        String productCode = "ALE_KA";
        String partnerCode = "BTPN";
        ProductMapping productMapping = productMappingDao
                .getProductMappingByProductCodeAndPartnerCodeAndDate(productCode, partnerCode,
                        new Date());
        Assert.assertNotNull(productMapping);
        System.out.println(productMapping.toString());
    }
}
