package id.co.homecredit.jfs.cofin.core.dao;

import static org.testng.Assert.assertNotNull;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.PublicHoliday;

/**
 * @author muhammad.muflihun
 *
 */
public class PublicHolidayDaoTest extends BaseDaoTest {

    @Autowired
    private PublicHolidayDao publicHolidayDao;

    @Test
    public void getPublicHoliday() {
        Date today = new Date();
        PublicHoliday publicHoliday = publicHolidayDao.getPublicHoliday(today);
        assertNotNull(publicHoliday);
        System.out.println(publicHoliday.toString());
    }

}
