package id.co.homecredit.jfs.cofin.core.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Test class for {@link ActivityDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class RoleDaoTest extends BaseDaoTest {

    @Autowired
    private RoleDao roleDao;

    @Test
    public void save() {
        for (RoleEnum enu : RoleEnum.values()) {
            Role role = new Role();
            role.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
            role.setRole(enu);
            role.setDescription(enu.getDescription());
            role = roleDao.save(role);
            Assert.assertNotNull(role);
            System.out.println(role);
        }
    }

}
