package id.co.homecredit.jfs.cofin.core.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;
import id.co.homecredit.jfs.cofin.core.model.User;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Test class for {@link RoleMappingDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class RoleMappingDaoTest extends BaseDaoTest {

    @Autowired
    private RoleMappingDao roleMappingDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    @Test
    public void getAllValidRoleMappingsByUsername() {
        String username = "muhammad.muflihun";
        List<RoleMapping> roleMappings = roleMappingDao.getAllValidRoleMappingsByUsername(username,
                new Date());
        Assert.assertFalse(roleMappings.isEmpty());
        for (RoleMapping roleMapping : roleMappings) {
            System.out.println(roleMapping.toString());
        }
    }

    @Test
    public void save() throws ParseException {
        String username = "muhammad.muflihun";
        Role role = roleDao.get(RoleEnum.DEV);
        User user = userDao.get(username);

        RoleMapping roleMapping = new RoleMapping();
        roleMapping.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        roleMapping.setRole(role);
        roleMapping.setUser(user);
        roleMapping.setValidFrom(DateUtil.stringToDate("2017-11-01"));
        roleMapping.setValidTo(DateUtil.stringToDate("3000-01-01"));
        roleMapping = roleMappingDao.save(roleMapping);
        Assert.assertNotNull(roleMapping);
        System.out.println(roleMapping.toString());
    }

}
