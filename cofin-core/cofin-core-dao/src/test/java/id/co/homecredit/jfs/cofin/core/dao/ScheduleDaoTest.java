package id.co.homecredit.jfs.cofin.core.dao;

import static org.testng.Assert.assertFalse;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.Schedule;

/**
 * Test class for {@link ScheduleDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class ScheduleDaoTest extends BaseDaoTest {

    @Autowired
    private ScheduleDao scheduleDao;

    @Test
    public void getAllSchedulesByContractNumber() {
        String contractNumber = "3300210958";
        List<Schedule> schedules = scheduleDao.getAllSchedulesByContractNumber(contractNumber);
        assertFalse(schedules.isEmpty());
        for (Schedule schedule : schedules) {
            System.out.println(schedule.toString());
        }
    }
}
