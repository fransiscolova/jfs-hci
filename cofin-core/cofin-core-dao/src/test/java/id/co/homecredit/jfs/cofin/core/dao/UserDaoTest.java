package id.co.homecredit.jfs.cofin.core.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.User;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;

/**
 * Test class for {@link UserDao}.
 *
 * @author muhammad.muflihun
 *
 */
public class UserDaoTest extends BaseDaoTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void get() {
        User user = userDao.get("muhammad.muflihun");
        Assert.assertNotNull(user);
        System.out.println(user.toString());
    }

    @Test
    public void getAllUnmappedUsers() {
        List<User> unmappedUsers = userDao.getAllUnmappedUsers();
        Assert.assertFalse(unmappedUsers.isEmpty());
        for (User user : unmappedUsers) {
            System.out.println(user.toString());
        }
    }

    @Test
    public void save() {
        String username = "muhammad.muflihun";
        String firstName = "Mufti";
        String lastName = "Muflihun";

        User user = new User();
        user.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user = userDao.save(user);
        Assert.assertNotNull(user);
        System.out.println(user);
    }

}
