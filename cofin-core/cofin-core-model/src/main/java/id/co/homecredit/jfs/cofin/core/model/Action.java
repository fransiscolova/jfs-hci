package id.co.homecredit.jfs.cofin.core.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.BaseEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_ACTION}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_ACTION)
public class Action extends BaseEntity<String> {
    private String action;
    private String name;
    
    
    @AuditTransient
    private List<RoleAction> roleAction;

    @Id
    @Column(name = CoreColumnName.ACTION, nullable = false, unique = true)
    public String getAction() {
        return action;
    }

    @Column(name = CoreColumnName.NAME)
    public String getName() {
        return name;
    }

    @OneToMany(mappedBy = "action")
    public List<RoleAction> getRoleAction() {
        return roleAction;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoleAction(List<RoleAction> roleAction) {
        this.roleAction = roleAction;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("Action [action=");
        builder.append(action);
        builder.append(", name=");
        builder.append(name);
        builder.append(", roleAction=");
        builder.append(roleAction);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
