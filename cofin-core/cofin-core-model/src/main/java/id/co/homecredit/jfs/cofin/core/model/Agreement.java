package id.co.homecredit.jfs.cofin.core.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdValidEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;

/**
 * Entity class for {@code COFIN_AGREEMENT}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_AGREEMENT, indexes = {
        @Index(columnList = CoreColumnName.ALIAS_ID, name = "IDX_AGR_ALIAS_ID") })
public class Agreement extends IdValidEntity {
    private String code;
    private String name;
    private String aliasId;
    private Partner partner;
    private String description;
    private AgreementTypeEnum agreementType;
    @AuditTransient
    private List<AgreementDetail> agreementDetails;
    @AuditTransient
    private List<ProductMapping> productMappings;

    @OneToMany(mappedBy = "agreement")
    public List<AgreementDetail> getAgreementDetails() {
        return agreementDetails;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.AGREEMENT_TYPE_CODE, nullable = false)
    public AgreementTypeEnum getAgreementType() {
        return agreementType;
    }

    @Column(name = CoreColumnName.ALIAS_ID, nullable = false, unique = true)
    public String getAliasId() {
        return aliasId;
    }

    @Column(name = CoreColumnName.CODE)
    public String getCode() {
        return code;
    }

    @Column(name = CoreColumnName.DESCRIPTION)
    public String getDescription() {
        return description;
    }

    @Column(name = CoreColumnName.NAME)
    public String getName() {
        return name;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.PARTNER_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public Partner getPartner() {
        return partner;
    }

    @OneToMany(mappedBy = "agreement")
    public List<ProductMapping> getProductMappings() {
        return productMappings;
    }

    public void setAgreementDetails(List<AgreementDetail> agreementDetails) {
        this.agreementDetails = agreementDetails;
    }

    public void setAgreementType(AgreementTypeEnum agreementType) {
        this.agreementType = agreementType;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public void setProductMappings(List<ProductMapping> productMappings) {
        this.productMappings = productMappings;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("Agreement [code=");
        builder.append(code);
        builder.append(", name=");
        builder.append(name);
        builder.append(", aliasId=");
        builder.append(aliasId);
        builder.append(", partner=");
        builder.append(partner.toString());
        builder.append(", description=");
        builder.append(description);
        builder.append(", agreementType=");
        builder.append(agreementType);
        builder.append(", validFrom=");
        builder.append(validFrom);
        builder.append(", validTo=");
        builder.append(validTo);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
