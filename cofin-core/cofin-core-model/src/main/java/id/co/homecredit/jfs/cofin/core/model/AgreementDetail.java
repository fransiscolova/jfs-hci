package id.co.homecredit.jfs.cofin.core.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdValidEntity;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for table {@code COFIN_AGREEMENT_DETAIL}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_AGREEMENT_DETAIL)
public class AgreementDetail extends IdValidEntity {
    private Agreement agreement;
    private BigDecimal interestRate;
    private BigDecimal principalSplitRate;
    private BigDecimal adminFeeRate;
    private BigDecimal penaltySplitRate;

    @Column(name = CoreColumnName.ADMIN_FEE_RATE, nullable = false, precision = 2)
    public BigDecimal getAdminFeeRate() {
        return adminFeeRate;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.AGREEMENT_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public Agreement getAgreement() {
        return agreement;
    }

    @Column(name = CoreColumnName.INTEREST_RATE, nullable = false, precision = 2)
    public BigDecimal getInterestRate() {
        return interestRate;
    }

    @Column(name = CoreColumnName.PENALTY_SPLIT_RATE, nullable = false, precision = 2)
    public BigDecimal getPenaltySplitRate() {
        return penaltySplitRate;
    }

    @Column(name = CoreColumnName.PRINCIPAL_SPLIT_RATE, nullable = false, precision = 2)
    public BigDecimal getPrincipalSplitRate() {
        return principalSplitRate;
    }

    public void setAdminFeeRate(BigDecimal adminFeeRate) {
        this.adminFeeRate = adminFeeRate;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public void setPenaltySplitRate(BigDecimal penaltySplitRate) {
        this.penaltySplitRate = penaltySplitRate;
    }

    public void setPrincipalSplitRate(BigDecimal principalSplitRate) {
        this.principalSplitRate = principalSplitRate;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("AgreementDetail [agreement=");
        builder.append(agreement);
        builder.append(", interestRate=");
        builder.append(interestRate);
        builder.append(", principalSplitRate=");
        builder.append(principalSplitRate);
        builder.append(", adminFeeRate=");
        builder.append(adminFeeRate);
        builder.append(", penaltySplitRate=");
        builder.append(penaltySplitRate);
        builder.append(", validFrom=");
        builder.append(validFrom);
        builder.append(", validTo=");
        builder.append(validTo);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
