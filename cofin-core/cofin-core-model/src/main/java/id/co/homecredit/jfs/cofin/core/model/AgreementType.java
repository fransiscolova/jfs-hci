package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@link COFIN_AGREEMENT_TYPE}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.COFIN_AGREEMENT_TYPE)
public class AgreementType extends IdEntity{
	private String code;
	private String name;
	private String agreementCategory;
	@Column(name=CoreColumnName.CODE)
	public String getCode(){
		return code;
	}
	public void setCode(String code){
		this.code=code;
	}
	@Column(name=CoreColumnName.NAME)
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name=name;
	}
	@Column(name=CoreColumnName.AGREEMENT_CATEGORY)
	public String getAgreementCategory(){
		return agreementCategory;
	}
	public void setAgreementCategory(String agreementCategory){
		this.agreementCategory=agreementCategory;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("AgreementType [id=");
		json.append(id);
		json.append(",code=");
		json.append(code);
		json.append(",name=");
		json.append(name);
		json.append(",agreementCategory=");
		json.append(agreementCategory);
		json.append(",createdBy=");
		json.append(createdBy);
		json.append(",createdDate=");
		json.append(createdDate);
		json.append(",updatedBy=");
		json.append(updatedDate);
		json.append(",updatedDate=");
		json.append(updatedDate);
		json.append(",deleted=");
		json.append(deleted);
		json.append(",version=");
		json.append(version);
		json.append("]");
		return json.toString();
	}
}
