package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdCreate;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.ActivityEnum;

/**
 * Entity class for {@code COFIN_AUDIT_LOG}.
 *
 * @author muhammad.muflihun
 *
 */
@AuditTransient
@Entity
@Table(name = CoreTableName.COFIN_AUDIT_LOG)
public class AuditLog extends IdCreate<String> {
    private ActivityEnum activity;
    private String fullClassName;
    private String dataBefore;
    private String dataAfter;

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.ACTIVITY, nullable = false)
    public ActivityEnum getActivity() {
        return activity;
    }

    @Column(name = CoreColumnName.DATA_AFTER)
    public String getDataAfter() {
        return dataAfter;
    }

    @Column(name = CoreColumnName.DATA_BEFORE)
    public String getDataBefore() {
        return dataBefore;
    }

    @Column(name = CoreColumnName.FULL_CLASSNAME)
    public String getFullClassName() {
        return fullClassName;
    }

    public void setActivity(ActivityEnum activity) {
        this.activity = activity;
    }

    public void setDataAfter(String dataAfter) {
        this.dataAfter = dataAfter;
    }

    public void setDataBefore(String dataBefore) {
        this.dataBefore = dataBefore;
    }

    public void setFullClassName(String fullClassName) {
        this.fullClassName = fullClassName;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("AuditLog [activity=");
        builder.append(activity);
        builder.append(", fullClassName=");
        builder.append(fullClassName);
        builder.append(", dataBefore=");
        builder.append(dataBefore);
        builder.append(", dataAfter=");
        builder.append(dataAfter);
        builder.append(", id=");
        builder.append(id);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
