package id.co.homecredit.jfs.cofin.core.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.batch.core.BatchStatus;

/**
 * Entity class for table {@code BATCH_JOB_EXECUTION}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "BATCH_JOB_EXECUTION")
public class BatchJobExecution implements Serializable {
    private long jobExecutionId;;
    private long version;
    private BatchJobInstance batchJobInstance;
    private Date createTime;
    private Date startTime;
    private Date endTime;
    private BatchStatus status;
    private String exitCode;
    private String exitMessage;
    private Date lastUpdated;
    private String jobConfigurationLocation;

    @ManyToOne
    @JoinColumn(name = "JOB_INSTANCE_ID", referencedColumnName = "JOB_INSTANCE_ID", nullable = false)
    public BatchJobInstance getBatchJobInstance() {
        return batchJobInstance;
    }

    @Column(name = "CREATE_TIME", nullable = false)
    public Date getCreateTime() {
        return createTime;
    }

    @Column(name = "END_TIME")
    public Date getEndTime() {
        return endTime;
    }

    @Column(name = "EXIT_CODE")
    public String getExitCode() {
        return exitCode;
    }

    @Column(name = "EXIT_MESSAGE")
    public String getExitMessage() {
        return exitMessage;
    }

    @Column(name = "JOB_CONFIGURATION_LOCATION")
    public String getJobConfigurationLocation() {
        return jobConfigurationLocation;
    }

    @Id
    @Column(name = "JOB_EXECUTION_ID", unique = true, nullable = false)
    public Long getJobExecutionId() {
        return jobExecutionId;
    }

    @Column(name = "LAST_UPDATED")
    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Column(name = "START_TIME")
    public Date getStartTime() {
        return startTime;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", length = 10)
    public BatchStatus getStatus() {
        return status;
    }

    @Column(name = "VERSION")
    public Long getVersion() {
        return version;
    }

    public void setBatchJobInstance(BatchJobInstance batchJobInstance) {
        this.batchJobInstance = batchJobInstance;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setExitCode(String exitCode) {
        this.exitCode = exitCode;
    }

    public void setExitMessage(String exitMessage) {
        this.exitMessage = exitMessage;
    }

    public void setJobConfigurationLocation(String jobConfigurationLocation) {
        this.jobConfigurationLocation = jobConfigurationLocation;
    }

    public void setJobExecutionId(Long jobExecutionId) {
        this.jobExecutionId = jobExecutionId;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setStatus(BatchStatus status) {
        this.status = status;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("BatchJobExecution [jobExecutionId=");
        builder.append(jobExecutionId);
        builder.append(", version=");
        builder.append(version);
        builder.append(", batchJobInstance=");
        builder.append(batchJobInstance);
        builder.append(", createTime=");
        builder.append(createTime);
        builder.append(", startTime=");
        builder.append(startTime);
        builder.append(", endTime=");
        builder.append(endTime);
        builder.append(", status=");
        builder.append(status);
        builder.append(", exitCode=");
        builder.append(exitCode);
        builder.append(", exitMessage=");
        builder.append(exitMessage);
        builder.append(", lastUpdated=");
        builder.append(lastUpdated);
        builder.append(", jobConfigurationLocation=");
        builder.append(jobConfigurationLocation);
        builder.append("]");
        return builder.toString();
    }

}
