package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for table {@code COFIN_BATCH_STEP}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_BATCH_STEP, indexes = {
        @Index(columnList = CoreColumnName.STEP_NAME, name = "IDX_BAT_STE_STEP_NAME") })
public class BatchStep extends IdEntity {
    private BatchJob batchJob;
    private String stepName;
    private Integer priority;

    @ManyToOne
    @JoinColumn(name = CoreColumnName.BATCH_JOB_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public BatchJob getBatchJob() {
        return batchJob;
    }

    @Column(name = CoreColumnName.PRIORITY)
    public Integer getPriority() {
        return priority;
    }

    @Column(name = CoreColumnName.STEP_NAME, nullable = false, unique = true)
    public String getStepName() {
        return stepName;
    }

    public void setBatchJob(BatchJob batchJob) {
        this.batchJob = batchJob;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("BatchStep [batchJob=");
        builder.append(batchJob);
        builder.append(", stepName=");
        builder.append(stepName);
        builder.append(", priority=");
        builder.append(priority);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
