package id.co.homecredit.jfs.cofin.core.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class for table {@code BATCH_STEP_EXECUTION_CONTEXT}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "BATCH_STEP_EXECUTION_CONTEXT")
public class BatchStepExecutionContext implements Serializable {
    private BatchStepExecution batchStepExecution;
    private String shortContext;
    private String serializedContext;

    @Id
    @ManyToOne
    @JoinColumn(name = "STEP_EXECUTION_ID", referencedColumnName = "STEP_EXECUTION_ID", unique = true, nullable = false)
    public BatchStepExecution getBatchStepExecution() {
        return batchStepExecution;
    }

    @Column(name = "SERIALIZED_CONTEXT")
    public String getSerializedContext() {
        return serializedContext;
    }

    @Column(name = "SHORT_CONTEXT", nullable = false)
    public String getShortContext() {
        return shortContext;
    }

    public void setBatchStepExecution(BatchStepExecution batchStepExecution) {
        this.batchStepExecution = batchStepExecution;
    }

    public void setSerializedContext(String serializedContext) {
        this.serializedContext = serializedContext;
    }

    public void setShortContext(String shortContext) {
        this.shortContext = shortContext;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("BatchStepExecutionContext [batchStepExecution=");
        builder.append(batchStepExecution);
        builder.append(", shortContext=");
        builder.append(shortContext);
        builder.append(", serializedContext=");
        builder.append(serializedContext);
        builder.append("]");
        return builder.toString();
    }

}
