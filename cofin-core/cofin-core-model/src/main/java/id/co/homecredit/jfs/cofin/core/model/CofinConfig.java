package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_CONFIG}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_CONFIG)
public class CofinConfig extends IdEntity {
    private String category;
    private String key01;
    private String key02;
    private String key03;
    private String value;
    private String desc01;
    private String desc02;
    private String desc03;
   
    @Column(name=CoreColumnName.CATEGORY)
	public String getCategory() {
		return category;
	}
    
    @Column(name=CoreColumnName.KEY_01)
	public String getKey01() {
		return key01;
	}
    
    @Column(name=CoreColumnName.KEY_02)
	public String getKey02() {
		return key02;
	}
        
    @Column(name=CoreColumnName.KEY_03)	
	public String getKey03() {
		return key03;
	}
   
    @Column(name=CoreColumnName.VALUE)
	public String getValue() {
		return value;
	}
    
    @Column(name=CoreColumnName.DESC_01)
	public String getDesc01() {
		return desc01;
	}
    
    @Column(name=CoreColumnName.DESC_02)
	public String getDesc02() {
		return desc02;
	}
    
    
    @Column(name=CoreColumnName.DESC_03)
	public String getDesc03() {
		return desc03;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public void setKey01(String key01) {
		this.key01 = key01;
	}
	public void setKey02(String key02) {
		this.key02 = key02;
	}
	public void setKey03(String key03) {
		this.key03 = key03;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setDesc01(String desc01) {
		this.desc01 = desc01;
	}
	public void setDesc02(String desc02) {
		this.desc02 = desc02;
	}
	public void setDesc03(String desc03) {
		this.desc03 = desc03;
	}
    
    
    
}
