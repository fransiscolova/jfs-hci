package id.co.homecredit.jfs.cofin.core.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for table {@code COFIN_COM_CATEGORY_MAPPING}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_COM_CATEGORY_MAPPING)
public class ComCategoryMapping extends IdEntity {
    private Partner partner;
    private CommodityCategory commodityCategory;
    private String partnerCommodityCategory;
    private String description;
    @AuditTransient
    private List<ComTypeMapping> comTypeMappings;

    @ManyToOne
    @JoinColumn(name = CoreColumnName.COMMODITY_CATEGORY_CODE, referencedColumnName = CoreColumnName.CODE, nullable = false)
    public CommodityCategory getCommodityCategory() {
        return commodityCategory;
    }

    @OneToMany(mappedBy = "comCategoryMapping")
    public List<ComTypeMapping> getComTypeMappings() {
        return comTypeMappings;
    }

    @Column(name = CoreColumnName.DESCRIPTION)
    public String getDescription() {
        return description;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.PARTNER_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public Partner getPartner() {
        return partner;
    }

    @Column(name = CoreColumnName.PARTNER_COM_CATEGORY_CODE, nullable = false)
    public String getPartnerCommodityCategory() {
        return partnerCommodityCategory;
    }

    public void setCommodityCategory(CommodityCategory commodityCategory) {
        this.commodityCategory = commodityCategory;
    }

    public void setComTypeMappings(List<ComTypeMapping> comTypeMappings) {
        this.comTypeMappings = comTypeMappings;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public void setPartnerCommodityCategory(String partnerCommodityCategory) {
        this.partnerCommodityCategory = partnerCommodityCategory;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("CommodityMapping [partner=");
        builder.append(partner);
        builder.append(", commodityCategory=");
        builder.append(commodityCategory);
        builder.append(", partnerCommodityCategory=");
        builder.append(partnerCommodityCategory);
        builder.append(", description=");
        builder.append(description);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
