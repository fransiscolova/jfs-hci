package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for table {@code COFIN_COM_TYPE_MAPPING}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_COM_TYPE_MAPPING)
public class ComTypeMapping extends IdEntity {
    private Partner partner;
    private ComCategoryMapping comCategoryMapping;
    private CommodityType commodityType;
    private String partnerCommodityType;
    private String description;

    @ManyToOne
    @JoinColumn(name = CoreColumnName.COM_CATEGORY_MAPPING_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public ComCategoryMapping getComCategoryMapping() {
        return comCategoryMapping;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.COMMODITY_TYPE_CODE, referencedColumnName = CoreColumnName.CODE, nullable = false)
    public CommodityType getCommodityType() {
        return commodityType;
    }

    @Column(name = CoreColumnName.DESCRIPTION)
    public String getDescription() {
        return description;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.PARTNER_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public Partner getPartner() {
        return partner;
    }

    @Column(name = CoreColumnName.PARTNER_COM_TYPE_CODE, nullable = false)
    public String getPartnerCommodityType() {
        return partnerCommodityType;
    }

    public void setComCategoryMapping(ComCategoryMapping comCategoryMapping) {
        this.comCategoryMapping = comCategoryMapping;
    }

    public void setCommodityType(CommodityType commodityType) {
        this.commodityType = commodityType;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public void setPartnerCommodityType(String partnerCommodityType) {
        this.partnerCommodityType = partnerCommodityType;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("CommodityMapping [partner=");
        builder.append(partner);
        builder.append(", commodityType=");
        builder.append(commodityType);
        builder.append(", partnerCommodityType=");
        builder.append(partnerCommodityType);
        builder.append(", description=");
        builder.append(description);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
