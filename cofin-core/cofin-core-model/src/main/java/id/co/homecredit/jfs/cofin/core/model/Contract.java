package id.co.homecredit.jfs.cofin.core.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.enumeration.ContractStatusEnum;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_CONTRACT}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_CONTRACT, indexes = {
        @Index(columnList = CoreColumnName.CONTRACT_NUMBER, name = "IDX_CON_CONTRACT_NUMBER") })
public class Contract extends IdEntity {
    private String contractNumber;
    private String cuid;
    private String clientName;
    private ContractStatusEnum status;
    private Agreement agreement;
    private Date exportDate;
    private Date firstDueDate;
    private Date acceptedDate;
    private String skpContract;
    private BigDecimal sendPrincipal;
    private BigDecimal sendInstalment;
    private Integer sendTenor;
    private BigDecimal sendRate;
    private BigDecimal instalmentAmount;
    private BigDecimal monthlyFeeAmount;
    private String reason;
    private String rejectReason;
    private BigDecimal bankInterestRate;
    private BigDecimal bankPrincipal;
    private BigDecimal bankInterest;
    private BigDecimal bankProvision;
    private BigDecimal bankAdminFee;
    private BigDecimal bankInstallment;
    private Integer bankTenor;
    private BigDecimal bankSplitRate;
    private Date bankDecisionDate;
    private Date bankClawbackDate;
    private BigDecimal bankClawbackAmount;
    private String bankReferenceNo;

    @Column(name = CoreColumnName.ACCEPTED_DATE)
    public Date getAcceptedDate() {
        return acceptedDate;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.AGREEMENT_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public Agreement getAgreement() {
        return agreement;
    }

    @Column(name = CoreColumnName.BANK_ADMIN_FEE, precision = 2)
    public BigDecimal getBankAdminFee() {
        return bankAdminFee;
    }

    @Column(name = CoreColumnName.BANK_CLAWBACK_AMOUNT, precision = 2)
    public BigDecimal getBankClawbackAmount() {
        return bankClawbackAmount;
    }

    @Column(name = CoreColumnName.BANK_CLAWBACK_DATE)
    public Date getBankClawbackDate() {
        return bankClawbackDate;
    }

    @Column(name = CoreColumnName.BANK_DECISION_DATE)
    public Date getBankDecisionDate() {
        return bankDecisionDate;
    }

    @Column(name = CoreColumnName.BANK_INSTALLMENT, precision = 2)
    public BigDecimal getBankInstallment() {
        return bankInstallment;
    }

    @Column(name = CoreColumnName.BANK_INTEREST, precision = 2)
    public BigDecimal getBankInterest() {
        return bankInterest;
    }

    @Column(name = CoreColumnName.BANK_INTEREST_RATE, precision = 2)
    public BigDecimal getBankInterestRate() {
        return bankInterestRate;
    }

    @Column(name = CoreColumnName.BANK_PRINCIPAL, precision = 2)
    public BigDecimal getBankPrincipal() {
        return bankPrincipal;
    }

    @Column(name = CoreColumnName.BANK_PROVISION, precision = 2)
    public BigDecimal getBankProvision() {
        return bankProvision;
    }

    @Column(name = CoreColumnName.BANK_REFERENCE_NO)
    public String getBankReferenceNo() {
        return bankReferenceNo;
    }

    @Column(name = CoreColumnName.BANK_SPLIT_RATE, precision = 2)
    public BigDecimal getBankSplitRate() {
        return bankSplitRate;
    }

    @Column(name = CoreColumnName.BANK_TENOR)
    public Integer getBankTenor() {
        return bankTenor;
    }

    @Column(name = CoreColumnName.CLIENT_NAME)
    public String getClientName() {
        return clientName;
    }

    @Column(name = CoreColumnName.CONTRACT_NUMBER, nullable = false)
    public String getContractNumber() {
        return contractNumber;
    }

    @Column(name = CoreColumnName.CUID)
    public String getCuid() {
        return cuid;
    }

    @Column(name = CoreColumnName.EXPORT_DATE)
    public Date getExportDate() {
        return exportDate;
    }

    @Column(name = CoreColumnName.FIRST_FUE_DATE)
    public Date getFirstDueDate() {
        return firstDueDate;
    }

    @Column(name = CoreColumnName.INSTALMENT_AMOUNT, precision = 2)
    public BigDecimal getInstalmentAmount() {
        return instalmentAmount;
    }

    @Column(name = CoreColumnName.MONTHLY_FEE_AMOUNT, precision = 2)
    public BigDecimal getMonthlyfeeAmount() {
        return monthlyFeeAmount;
    }

    @Column(name = CoreColumnName.REASON)
    public String getReason() {
        return reason;
    }

    @Column(name = CoreColumnName.REJECT_REASON)
    public String getRejectReason() {
        return rejectReason;
    }

    @Column(name = CoreColumnName.SEND_INSTALMENT, precision = 2, nullable = false)
    public BigDecimal getSendInstalment() {
        return sendInstalment;
    }

    @Column(name = CoreColumnName.SEND_PRINCIPAL, precision = 2, nullable = false)
    public BigDecimal getSendPrincipal() {
        return sendPrincipal;
    }

    @Column(name = CoreColumnName.SEND_RATE, precision = 2, nullable = false)
    public BigDecimal getSendRate() {
        return sendRate;
    }

    @Column(name = CoreColumnName.SEND_TENOR, nullable = false)
    public Integer getSendTenor() {
        return sendTenor;
    }

    @Column(name = CoreColumnName.SKP_CONTRACT)
    public String getSkpContract() {
        return skpContract;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.STATUS, nullable = false)
    public ContractStatusEnum getStatus() {
        return status;
    }

    public void setAcceptedDate(Date acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public void setBankAdminFee(BigDecimal bankAdminFee) {
        this.bankAdminFee = bankAdminFee;
    }

    public void setBankClawbackAmount(BigDecimal bankClawbackAmount) {
        this.bankClawbackAmount = bankClawbackAmount;
    }

    public void setBankClawbackDate(Date bankClawbackDate) {
        this.bankClawbackDate = bankClawbackDate;
    }

    public void setBankDecisionDate(Date bankDecisionDate) {
        this.bankDecisionDate = bankDecisionDate;
    }

    public void setBankInstallment(BigDecimal bankInstallment) {
        this.bankInstallment = bankInstallment;
    }

    public void setBankInterest(BigDecimal bankInterest) {
        this.bankInterest = bankInterest;
    }

    public void setBankInterestRate(BigDecimal bankInterestRate) {
        this.bankInterestRate = bankInterestRate;
    }

    public void setBankPrincipal(BigDecimal bankPrincipal) {
        this.bankPrincipal = bankPrincipal;
    }

    public void setBankProvision(BigDecimal bankProvision) {
        this.bankProvision = bankProvision;
    }

    public void setBankReferenceNo(String bankReferenceNo) {
        this.bankReferenceNo = bankReferenceNo;
    }

    public void setBankSplitRate(BigDecimal bankSplitRate) {
        this.bankSplitRate = bankSplitRate;
    }

    public void setBankTenor(Integer bankTenor) {
        this.bankTenor = bankTenor;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public void setCuid(String cuid) {
        this.cuid = cuid;
    }

    public void setExportDate(Date exportDate) {
        this.exportDate = exportDate;
    }

    public void setFirstDueDate(Date firstDueDate) {
        this.firstDueDate = firstDueDate;
    }

    public void setInstalmentAmount(BigDecimal instalmentAmount) {
        this.instalmentAmount = instalmentAmount;
    }

    public void setMonthlyfeeAmount(BigDecimal monthlyFeeAmount) {
        this.monthlyFeeAmount = monthlyFeeAmount;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public void setSendInstalment(BigDecimal sendInstalment) {
        this.sendInstalment = sendInstalment;
    }

    public void setSendPrincipal(BigDecimal sendPrincipal) {
        this.sendPrincipal = sendPrincipal;
    }

    public void setSendRate(BigDecimal sendRate) {
        this.sendRate = sendRate;
    }

    public void setSendTenor(Integer sendTenor) {
        this.sendTenor = sendTenor;
    }

    public void setSkpContract(String skpContract) {
        this.skpContract = skpContract;
    }

    public void setStatus(ContractStatusEnum status) {
        this.status = status;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(2000);
        builder.append("Contract [contractNumber=");
        builder.append(contractNumber);
        builder.append(", clientName=");
        builder.append(clientName);
        builder.append(", cuid=");
        builder.append(cuid);
        builder.append(", status=");
        builder.append(status);
        builder.append(", exportDate=");
        builder.append(exportDate);
        builder.append(", skpContract=");
        builder.append(skpContract);
        builder.append(", sendPrincipal=");
        builder.append(sendPrincipal);
        builder.append(", sendInstalment=");
        builder.append(sendInstalment);
        builder.append(", sendTenor=");
        builder.append(sendTenor);
        builder.append(", sendRate=");
        builder.append(sendRate);
        builder.append(", firstDueDate=");
        builder.append(firstDueDate);
        builder.append(", acceptedDate=");
        builder.append(acceptedDate);
        builder.append(", rejectReason=");
        builder.append(rejectReason);
        builder.append(", bankInterestRate=");
        builder.append(bankInterestRate);
        builder.append(", bankPrincipal=");
        builder.append(bankPrincipal);
        builder.append(", bankInterest=");
        builder.append(bankInterest);
        builder.append(", bankProvision=");
        builder.append(bankProvision);
        builder.append(", bankAdminFee=");
        builder.append(bankAdminFee);
        builder.append(", bankInstallment=");
        builder.append(bankInstallment);
        builder.append(", bankTenor=");
        builder.append(bankTenor);
        builder.append(", bankSplitRate=");
        builder.append(bankSplitRate);
        builder.append(", instalmentAmount=");
        builder.append(instalmentAmount);
        builder.append(", monthlyFeeAmount=");
        builder.append(monthlyFeeAmount);
        builder.append(", agreement=");
        builder.append(agreement);
        builder.append(", reason=");
        builder.append(reason);
        builder.append(", bankDecisionDate=");
        builder.append(bankDecisionDate);
        builder.append(", bankClawbackDate=");
        builder.append(bankClawbackDate);
        builder.append(", bankClawbackAmount=");
        builder.append(bankClawbackAmount);
        builder.append(", bankReferenceNo=");
        builder.append(bankReferenceNo);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
