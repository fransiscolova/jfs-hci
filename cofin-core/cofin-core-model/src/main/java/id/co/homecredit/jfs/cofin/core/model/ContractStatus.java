package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.enumeration.ContractStatusEnum;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_CONTRACT_STATUS}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_CONTRACT_STATUS, indexes = {
        @Index(columnList = CoreColumnName.CONTRACT_NUMBER, name = "IDX_CON_STA_CONTRACT_NUMBER") })
public class ContractStatus extends IdEntity {
    private String contractNumber;
    private ContractStatusEnum status;
    private Integer countDpd;
    private Date bankProcessDate;

    @Column(name = CoreColumnName.BANK_PROCESS_DATE)
    public Date getBankProcessDate() {
        return bankProcessDate;
    }

    @Column(name = CoreColumnName.CONTRACT_NUMBER, nullable = false)
    public String getContractNumber() {
        return contractNumber;
    }

    @Column(name = CoreColumnName.COUNT_DPD, nullable = false)
    public Integer getCountDpd() {
        return countDpd;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.STATUS, nullable = false)
    public ContractStatusEnum getStatus() {
        return status;
    }

    public void setBankProcessDate(Date bankProcessDate) {
        this.bankProcessDate = bankProcessDate;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public void setCountDpd(Integer countDpd) {
        this.countDpd = countDpd;
    }

    public void setStatus(ContractStatusEnum status) {
        this.status = status;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("ContractStatus [contractNumber=");
        builder.append(contractNumber);
        builder.append(", status=");
        builder.append(status);
        builder.append(", countDpd=");
        builder.append(countDpd);
        builder.append(", bankProcessDate=");
        builder.append(bankProcessDate);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
