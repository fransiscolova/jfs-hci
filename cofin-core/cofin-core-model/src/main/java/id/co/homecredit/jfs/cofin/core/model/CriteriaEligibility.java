package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdValidEntity;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.EligibilityEnum;

/**
 * Entity class for {@code COFIN_CRITERIA_ELIGIBILITY}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_CRITERIA_ELIGIBILITY)
public class CriteriaEligibility extends IdValidEntity {
    private CriteriaMapping criteriaMapping;
    private EligibilityEnum eligibility;
    private String value;

    @ManyToOne
    @JoinColumn(name = CoreColumnName.CRITERIA_MAPPING_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public CriteriaMapping getCriteriaMapping() {
        return criteriaMapping;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.ELIGIBILITY_CODE, nullable = false)
    public EligibilityEnum getEligibility() {
        return eligibility;
    }

    @Column(name = CoreColumnName.VALUE, nullable = false)
    public String getValue() {
        return value;
    }

    public void setCriteriaMapping(CriteriaMapping criteriaMapping) {
        this.criteriaMapping = criteriaMapping;
    }

    public void setEligibility(EligibilityEnum eligibility) {
        this.eligibility = eligibility;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("CriteriaMappingEligibility [criteriaMapping=");
        builder.append(criteriaMapping);
        builder.append(", eligibility=");
        builder.append(eligibility);
        builder.append(", value=");
        builder.append(value);
        builder.append(", validFrom=");
        builder.append(validFrom);
        builder.append(", validTo=");
        builder.append(validTo);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
