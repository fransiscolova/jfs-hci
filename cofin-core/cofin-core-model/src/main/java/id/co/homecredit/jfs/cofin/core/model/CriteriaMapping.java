package id.co.homecredit.jfs.cofin.core.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.OperatorEnum;

/**
 * Entity class for {@code COFIN_CRITERIA_MAPPING}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_CRITERIA_MAPPING, indexes = {
        @Index(columnList = CoreColumnName.ALIAS_ID, name = "IDX_CRI_MAP_ALIAS_ID") })
public class CriteriaMapping extends IdEntity {
    private Agreement agreement;
    private CriteriaEnum criteria;
    private String aliasId;
    private CriteriaMapping criteriaMappingParent;
    private Integer criteriaLevel;
    private OperatorEnum groupOperator;
    @AuditTransient
    private List<CriteriaMapping> criteriaMappingChilds;
    @AuditTransient
    private List<CriteriaEligibility> criteriaEligibilities;

    @ManyToOne
    @JoinColumn(name = CoreColumnName.AGREEMENT_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public Agreement getAgreement() {
        return agreement;
    }

    @Column(name = CoreColumnName.ALIAS_ID, nullable = false)
    public String getAliasId() {
        return aliasId;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.CRITERIA_CODE, nullable = false)
    public CriteriaEnum getCriteria() {
        return criteria;
    }

    @OneToMany(mappedBy = "criteriaMapping")
    public List<CriteriaEligibility> getCriteriaEligibilities() {
        return criteriaEligibilities;
    }

    @Column(name = CoreColumnName.CRITERIA_LEVEL, nullable = false)
    public Integer getCriteriaLevel() {
        return criteriaLevel;
    }

    @OneToMany(mappedBy = "criteriaMappingParent")
    public List<CriteriaMapping> getCriteriaMappingChilds() {
        return criteriaMappingChilds;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.CRITERIA_MAPPING_PARENT_ID, referencedColumnName = ColumnName.ID)
    public CriteriaMapping getCriteriaMappingParent() {
        return criteriaMappingParent;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.GROUP_OPERATOR)
    public OperatorEnum getGroupOperator() {
        return groupOperator;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public void setCriteria(CriteriaEnum criteria) {
        this.criteria = criteria;
    }

    public void setCriteriaEligibilities(List<CriteriaEligibility> criteriaEligibilities) {
        this.criteriaEligibilities = criteriaEligibilities;
    }

    public void setCriteriaLevel(Integer criteriaLevel) {
        this.criteriaLevel = criteriaLevel;
    }

    public void setCriteriaMappingChilds(List<CriteriaMapping> criteriaMappingChilds) {
        this.criteriaMappingChilds = criteriaMappingChilds;
    }

    public void setCriteriaMappingParent(CriteriaMapping criteriaMappingParent) {
        this.criteriaMappingParent = criteriaMappingParent;
    }

    public void setGroupOperator(OperatorEnum groupOperator) {
        this.groupOperator = groupOperator;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("CriteriaMapping [agreement=");
        builder.append(agreement);
        builder.append(", criteria=");
        builder.append(criteria);
        builder.append(", aliasId=");
        builder.append(aliasId);
        builder.append(", criteriaMappingParent=");
        builder.append(criteriaMappingParent);
        builder.append(", criteriaLevel=");
        builder.append(criteriaLevel);
        builder.append(", groupOperator=");
        builder.append(groupOperator);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
