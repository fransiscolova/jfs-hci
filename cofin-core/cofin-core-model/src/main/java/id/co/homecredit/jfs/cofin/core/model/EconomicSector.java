package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_ECONOMIC_SECTOR}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_ECONOMIC_SECTOR, indexes = {
        @Index(columnList = CoreColumnName.BANK_COMMODITY_GROUP, name = "IDX_ECO_SEC_BI_BAN_COM_GROUP"),
        @Index(columnList = CoreColumnName.BI_ECONOMIC_SECTOR_CODE, name = "IDX_ECO_SEC_BI_COM_SEC_CODE") })
public class EconomicSector extends IdEntity {
    private Partner partner;
    private String bankCommodityGroup;
    private String biEconomicSectorCode;
    private String description;

    @Column(name = CoreColumnName.BANK_COMMODITY_GROUP, nullable = false)
    public String getBankCommodityGroup() {
        return bankCommodityGroup;
    }

    @Column(name = CoreColumnName.BI_ECONOMIC_SECTOR_CODE, nullable = false)
    public String getBiEconomicSectorCode() {
        return biEconomicSectorCode;
    }

    @Column(name = CoreColumnName.DESCRIPTION)
    public String getDescription() {
        return description;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.PARTNER_ID, referencedColumnName = CoreColumnName.ID, nullable = false)
    public Partner getPartner() {
        return partner;
    }

    public void setBankCommodityGroup(String bankCommodityGroup) {
        this.bankCommodityGroup = bankCommodityGroup;
    }

    public void setBiEconomicSectorCode(String biEconomicSectorCode) {
        this.biEconomicSectorCode = biEconomicSectorCode;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("EconomicSector [partner=");
        builder.append(partner);
        builder.append(", bankCommodityGroup=");
        builder.append(bankCommodityGroup);
        builder.append(", biEconomicSectorCode=");
        builder.append(biEconomicSectorCode);
        builder.append(", description=");
        builder.append(description);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
