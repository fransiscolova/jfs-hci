package id.co.homecredit.jfs.cofin.core.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;

/**
 * Entity class for {@code COFIN_FILE_UPLOAD}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_FILE_UPLOAD)
public class FileUpload extends IdEntity {
    private Partner partner;
    private String description;
    private String memo;
    private String fileName;
    private String backupFileName;
    private String processType;
    private Date processDate;
    private String status;
    private BigDecimal totalRow;
    private CofinConfirmFile cofinConfirmFile;
    private String filePath;
    
    @AuditTransient
    private List<TempJfsConfirmationFile> tempJfsConfirmationFile;

    
    
    @OneToMany(mappedBy = "fileUpload")
    public List<TempJfsConfirmationFile> getTempJfsConfirmationFile() {
        return tempJfsConfirmationFile;
    }
    
    @Column(name=CoreColumnName.TOTAL_ROW)
    public BigDecimal getTotalRow() {
		return totalRow;
	}



	@Column(name=CoreColumnName.STATUS)
    public String getStatus() {
		return status;
	}

	

    @Column(name=CoreColumnName.PROCESS_DATE)
    public Date getProcessDate() {
		return processDate;
	}

	@Column(name=CoreColumnName.FILE_NAME)
    public String getFileName() {
  		return fileName;
  	}
	

	@Column(name = CoreColumnName.DESCRIPTION)
    public String getDescription() {
        return description;
    }

	@Column(name=CoreColumnName.BACKUP_FILENAME)
    public String getBackupFileName() {
		return backupFileName;
	}


	@Column(name=CoreColumnName.PROCESS_TYPE)
	public String getProcessType() {
		return processType;
	}


	@Column(name = CoreColumnName.MEMO)
    public String getMemo() {
        return memo;
    }

	public void setTempJfsConfirmationFile(List<TempJfsConfirmationFile> tempJfsConfirmationFile) {
        this.tempJfsConfirmationFile = tempJfsConfirmationFile;
    }
	
    @OneToOne
    @JoinColumn(name = CoreColumnName.PARTNER_ID, referencedColumnName = ColumnName.ID,nullable = false)
    public Partner getPartner() {
        return partner;
    }

    @OneToOne
    @JoinColumn(name = "FK_CONFIRM_FILE", referencedColumnName = ColumnName.ID,nullable = false)
    public CofinConfirmFile getCofinConfirmFile() {
		return cofinConfirmFile;
	}

    
    
    
    @Column(name = "FILE_PATH")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setCofinConfirmFile(CofinConfirmFile cofinConfirmFile) {
		this.cofinConfirmFile = cofinConfirmFile;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

    
	public void setProcessType(String processType) {
		this.processType = processType;
	}

    
	public void setBackupFileName(String backupFileName) {
		this.backupFileName = backupFileName;
	}

    
    public void setDescription(String description) {
        this.description = description;
    }

   
    public void setTotalRow(BigDecimal totalRow) {
		this.totalRow = totalRow;
	}
    public void setPartner(Partner partner) {
        this.partner = partner;
    }


    public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
    
  
}
