package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@code JFS_AGREEMENT}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_AGREEMENT)
public class JfsAgreement extends BaseEntityIdOnly<String>{
	private String codePartner;
	private String codeAgreement;
	private String nameAgreement;
	private Date validFrom;
	private Date validTo;
	private String bankCode;
	private Long idAgreement;
	private String partnerId;
	private String isDelete;
	private Double intRate;
	private Double prinSplitRate;
	private Double admFeeRate;
	private String description;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private String fkAgreementType;
	@Column(name=CoreColumnName.CODE_PARTNER,nullable=false)
	public String getCodePartner(){
		return codePartner;
	}
	public void setCodePartner(String codePartner){
		this.codePartner=codePartner;
	}
	@Column(name=CoreColumnName.CODE_AGREEMENT,nullable=false)
	public String getCodeAgreement(){
		return codeAgreement;
	}
	public void setCodeAgreement(String codeAgreement){
		this.codeAgreement=codeAgreement;
	}
	@Column(name=CoreColumnName.NAME_AGREEMENT,nullable=false)
	public String getNameAgreement(){
		return nameAgreement;
	}
	public void setNameAgreement(String nameAgreement){
		this.nameAgreement=nameAgreement;
	}
	@Column(name=CoreColumnName.VALID_FROM,nullable=false)
	public Date getValidFrom(){
		return validFrom;
	}
	public void setValidFrom(Date validFrom){
		this.validFrom=validFrom;
	}
	@Column(name=CoreColumnName.VALID_TO,nullable=false)
	public Date getValidTo(){
		return validTo;
	}
	public void setValidTo(Date validTo){
		this.validTo=validTo;
	}
	@Column(name=CoreColumnName.BANK_CODE,nullable=false)
	public String getBankCode(){
		return bankCode;
	}
	public void setBankCode(String bankCode){
		this.bankCode=bankCode;
	}
	@Column(name=CoreColumnName.ID_AGREEMENT,nullable=false,unique=true)
	public Long getIdAgreement(){
		return idAgreement;
	}
	public void setIdAgreement(Long idAgreement){
		this.idAgreement=idAgreement;
	}
	@Column(name=CoreColumnName.PARTNER_ID,nullable=false)
	public String getPartnerId(){
		return partnerId;
	}
	public void setPartnerId(String partnerId){
		this.partnerId=partnerId;
	}
	@Column(name=CoreColumnName.IS_DELETE)
	public String getIsDelete(){
		return isDelete;
	}
	public void setIsDelete(String isDelete){
		this.isDelete=isDelete;
	}
	@Column(name=CoreColumnName.INT_RATE)
	public Double getIntRate(){
		return intRate;
	}
	public void setIntRate(Double intRate){
		this.intRate=intRate;
	}
	@Column(name=CoreColumnName.PRIN_SPLIT_RATE)
	public Double getPrinSplitRate(){
		return prinSplitRate;
	}
	public void setPrinSplitRate(Double prinSplitRate){
		this.prinSplitRate=prinSplitRate;
	}
	@Column(name=CoreColumnName.ADM_FEE_RATE)
	public Double getAdmFeeRate(){
		return admFeeRate;
	}
	public void setAdmFeeRate(Double admFeeRate){
		this.admFeeRate=admFeeRate;
	}
	@Column(name=CoreColumnName.DESCRIPTION)
	public String getDescription(){
		return description;
	}
	public void setDescription(String description){
		this.description=description;
	}
	@Column(name=CoreColumnName.CREATED_BY)
	public String getCreatedBy(){
		return createdBy;
	}
	public void setCreatedBy(String createdBy){
		this.createdBy=createdBy;
	}
	@Column(name=CoreColumnName.CREATED_DATE)
	public Date getCreatedDate(){
		return createdDate;
	}
	public void setCreatedDate(Date createdDate){
		this.createdDate=createdDate;
	}
	@Column(name=CoreColumnName.UPDATED_BY)
	public String getUpdatedBy(){
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy){
		this.updatedBy=updatedBy;
	}
	@Column(name=CoreColumnName.UPDATED_DATE)
	public Date getUpdatedDate(){
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate=updatedDate;
	}
	@Column(name=CoreColumnName.FK_AGREEMENT_TYPE)
	public String getFkAgreementType(){
		return fkAgreementType;
	}
	public void setFkAgreementType(String fkAgreementType){
		this.fkAgreementType=fkAgreementType;
	}
	/**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("JfsAgreement [codePartner=");
        builder.append(codePartner);
        builder.append(", codeAgreememnt=");
        builder.append(codeAgreement);
        builder.append(", validFrom=");
        builder.append(validFrom);
        builder.append(", validTo=");
        builder.append(validTo);
        builder.append(", bankCode=");
        builder.append(bankCode);
        builder.append(", idAgreement=");
        builder.append(idAgreement);
        builder.append(", partnerId=");
        builder.append(partnerId);
        builder.append(", isDelete=");
        builder.append(isDelete);
        builder.append(", intRate=");
        builder.append(intRate);
        builder.append(", prinSplitRate=");
        builder.append(prinSplitRate);
        builder.append(", admFeeRate=");
        builder.append(admFeeRate);
        builder.append(", description=");
        builder.append(description);
        builder.append(", id=");
        builder.append(id);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(",fkAgreementType=");
        builder.append(fkAgreementType);
        builder.append("]");
        return builder.toString();
    }
}
