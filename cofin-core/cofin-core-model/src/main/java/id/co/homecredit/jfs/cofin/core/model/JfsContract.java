package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity class for {@code JFS_CONTRACT}.
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=CoreTableName.JFS_CONTRACT)
public class JfsContract{
	private String textContractNumber;
	private String status;
	private Date exportDate;
	private Long skpContract;
	private Double sendPrincipal;
	private Double sendInstalment;
	private Integer sendTenor;
	private Double sendRate;
	private Date dateFirstDue;
	private Date acceptedDate;
	private String rejectReason;
	private Double bankInterestRate;
	private Double bankPrincipal;
	private Double bankInterest;
	private Double bankProvision;
	private Double bankAdminFee;
	private Double bankInstallment;
	private Double bankTenor;
	private Double bankSplitRate;
	private String clientName;
	private Double amtInstallment;
	private Double amtMonthlyFee;
	private Long idAgreement;
	private String reason;
	private Date bankDecisionDate;
	private Date bankClawbackDate;
	private Double bankClawbackAmount;
	private String bankProductCode;
	private String bankReferenceNo;
	private Long cuid;
	@Id
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.STATUS)
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	@Column(name=CoreColumnName.EXPORT_DATE)
	public Date getExportDate(){
		return exportDate;
	}
	public void setExportDate(Date exportDate){
		this.exportDate=exportDate;
	}
	@Column(name=CoreColumnName.SKP_CONTRACT)
	public Long getSkpContract(){
		return skpContract;
	}
	public void setSkpContract(Long skpContract){
		this.skpContract=skpContract;
	}
	@Column(name=CoreColumnName.SEND_PRINCIPAL)
	public Double getSendPrincipal(){
		return sendPrincipal;
	}
	public void setSendPrincipal(Double sendPrincipal){
		this.sendPrincipal=sendPrincipal;
	}
	@Column(name=CoreColumnName.SEND_INSTALMENT)
	public Double getSendInstalment(){
		return sendInstalment;
	}
	public void setSendInstalment(Double sendInstalment){
		this.sendInstalment=sendInstalment;
	}
	@Column(name=CoreColumnName.SEND_TENOR)
	public Integer getSendTenor(){
		return sendTenor;
	}
	public void setSendTenor(Integer sendTenor){
		this.sendTenor=sendTenor;
	}
	@Column(name=CoreColumnName.SEND_RATE)
	public Double getSendRate(){
		return sendRate;
	}
	public void setSendRate(Double sendRate){
		this.sendRate=sendRate;
	}
	@Column(name=CoreColumnName.DATE_FIRST_DUE)
	public Date getDateFirstDue(){
		return dateFirstDue;
	}
	public void setDateFirstDue(Date dateFirstDue){
		this.dateFirstDue=dateFirstDue;
	}
	@Column(name=CoreColumnName.ACCEPTED_DATE)
	public Date getAcceptedDate(){
		return acceptedDate;
	}
	public void setAcceptedDate(Date acceptedDate){
		this.acceptedDate=acceptedDate;
	}
	@Column(name=CoreColumnName.REJECT_REASON)
	public String getRejectReason(){
		return rejectReason;
	}
	public void setRejectReason(String rejectReason){
		this.rejectReason=rejectReason;
	}
	@Column(name=CoreColumnName.BANK_INTEREST_RATE)
	public Double getBankInterestRate(){
		return bankInterestRate;
	}
	public void setBankInterestRate(Double bankInterestRate){
		this.bankInterestRate=bankInterestRate;
	}
	@Column(name=CoreColumnName.BANK_PRINCIPAL)
	public Double getBankPrincipal(){
		return bankPrincipal;
	}
	public void setBankPrincipal(Double bankPrincipal){
		this.bankPrincipal=bankPrincipal;
	}
	@Column(name=CoreColumnName.BANK_INTEREST)
	public Double getBankInterest(){
		return bankInterest;
	}
	public void setBankInterest(Double bankInterest){
		this.bankInterest=bankInterest;
	}
	@Column(name=CoreColumnName.BANK_PROVISION)
	public Double getBankProvision(){
		return bankProvision;
	}
	public void setBankProvision(Double bankProvision){
		this.bankProvision=bankProvision;
	}
	@Column(name=CoreColumnName.BANK_ADMIN_FEE)
	public Double getBankAdminFee(){
		return bankAdminFee;
	}
	public void setBankAdminFee(Double bankAdminFee){
		this.bankAdminFee=bankAdminFee;
	}
	@Column(name=CoreColumnName.BANK_INSTALLMENT)
	public Double getBankInstallment(){
		return bankInstallment;
	}
	public void setBankInstallment(Double bankInstallment){
		this.bankInstallment=bankInstallment;
	}
	@Column(name=CoreColumnName.BANK_TENOR)
	public Double getBankTenor(){
		return bankTenor;
	}
	public void setBankTenor(Double bankTenor){
		this.bankTenor=bankTenor;
	}
	@Column(name=CoreColumnName.BANK_SPLIT_RATE)
	public Double getBankSplitRate(){
		return bankSplitRate;
	}
	public void setBankSplitRate(Double bankSplitRate){
		this.bankSplitRate=bankSplitRate;
	}
	@Column(name=CoreColumnName.CLIENT_NAME)
	public String getClientName(){
		return clientName;
	}
	public void setClientName(String clientName){
		this.clientName=clientName;
	}
	@Column(name=CoreColumnName.AMT_INSTALLMENT)
	public Double getAmtInstallment(){
		return amtInstallment;
	}
	public void setAmtInstallment(Double amtInstallment){
		this.amtInstallment=amtInstallment;
	}
	@Column(name=CoreColumnName.AMT_MONTHLY_FEE)
	public Double getAmtMonthlyFee(){
		return amtMonthlyFee;
	}
	public void setAmtMonthlyFee(Double amtMonthlyFee){
		this.amtMonthlyFee=amtMonthlyFee;
	}
	@Column(name=CoreColumnName.ID_AGREEMENT)
	public Long getIdAgreement(){
		return idAgreement;
	}
	public void setIdAgreement(Long idAgreement){
		this.idAgreement=idAgreement;
	}
	@Column(name=CoreColumnName.REASON)
	public String getReason(){
		return reason;
	}
	public void setReason(String reason){
		this.reason=reason;
	}
	@Column(name=CoreColumnName.BANK_DECISION_DATE)
	public Date getBankDecisionDate(){
		return bankDecisionDate;
	}
	public void setBankDecisionDate(Date bankDecisionDate){
		this.bankDecisionDate=bankDecisionDate;
	}
	@Column(name=CoreColumnName.BANK_CLAWBACK_DATE)
	public Date getBankClawbackDate(){
		return bankClawbackDate;
	}
	public void setBankClawbackDate(Date bankClawbackDate){
		this.bankClawbackDate=bankClawbackDate;
	}
	@Column(name=CoreColumnName.BANK_CLAWBACK_AMOUNT)
	public Double getBankClawbackAmount(){
		return bankClawbackAmount;
	}
	public void setBankClawbackAmount(Double bankClawbackAmount){
		this.bankClawbackAmount=bankClawbackAmount;
	}
	@Column(name=CoreColumnName.BANK_PRODUCT_CODE)
	public String getBankProductCode(){
		return bankProductCode;
	}
	public void setBankProductCode(String bankProductCode){
		this.bankProductCode=bankProductCode;
	}
	@Column(name=CoreColumnName.BANK_REFERENCE_NO)
	public String getBankReferenceNo(){
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo){
		this.bankReferenceNo=bankReferenceNo;
	}
	@Column(name=CoreColumnName.CUID)
	public Long getCuid(){
		return cuid;
	}
	public void setCuid(Long cuid){
		this.cuid=cuid;
	}
	@Override
	public String toString(){
		StringBuilder xbuild = new StringBuilder(1000);
		xbuild.append("JfsContract [textContractNumber=");
		xbuild.append(textContractNumber);
		xbuild.append(",status=");
		xbuild.append(status);
		xbuild.append(",exportDate=");
		xbuild.append(exportDate);
		xbuild.append(",skpContract=");
		xbuild.append(skpContract);
		xbuild.append(",sendPrincipal=");
		xbuild.append(sendPrincipal);
		xbuild.append(",sendInstalment=");
		xbuild.append(sendInstalment);
		xbuild.append(",sendTenor=");
		xbuild.append(sendTenor);
		xbuild.append(",sendRate=");
		xbuild.append(sendRate);
		xbuild.append(",dateFirstDue=");
		xbuild.append(dateFirstDue);
		xbuild.append(",acceptedDate=");
		xbuild.append(acceptedDate);
		xbuild.append(",rejectReason=");
		xbuild.append(rejectReason);
		xbuild.append(",bankInterestRate=");
		xbuild.append(bankInterestRate);
		xbuild.append(",bankPrincipal=");
		xbuild.append(bankPrincipal);
		xbuild.append(",bankInterest=");
		xbuild.append(bankInterest);
		xbuild.append(",bankProvision=");
		xbuild.append(bankProvision);
		xbuild.append(",bankAdminFee=");
		xbuild.append(bankAdminFee);
		xbuild.append(",bankInstallment=");
		xbuild.append(bankInstallment);
		xbuild.append(",bankTenor=");
		xbuild.append(bankTenor);
		xbuild.append(",bankSplitRate=");
		xbuild.append(bankSplitRate);
		xbuild.append(",clientName=");
		xbuild.append(clientName);
		xbuild.append(",amtInstallment=");
		xbuild.append(amtInstallment);
		xbuild.append(",amtMonthlyFee=");
		xbuild.append(amtMonthlyFee);
		xbuild.append(",idAgreement=");
		xbuild.append(idAgreement);
		xbuild.append(",reason=");
		xbuild.append(reason);
		xbuild.append(",bankDecisionDate=");
		xbuild.append(bankDecisionDate);
		xbuild.append(",bankClawbackDate=");
		xbuild.append(bankClawbackDate);
		xbuild.append(",bankClawbackAmount=");
		xbuild.append(bankClawbackAmount);
		xbuild.append(",bankProductCode=");
		xbuild.append(bankProductCode);
		xbuild.append(",bankReferenceNo=");
		xbuild.append(bankReferenceNo);
		xbuild.append(",cuid=");
		xbuild.append(cuid);
		xbuild.append("]");
		return xbuild.toString();
	}
}