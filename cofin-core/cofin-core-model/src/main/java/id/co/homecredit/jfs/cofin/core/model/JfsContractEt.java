package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@link JfsContractEt}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_CONTRACT_ET)
public class JfsContractEt{
	private String bankReferenceNo;
	private Date dateBankDisbursed;
	private String textContractNumber;
	private String clientName;
	private Date dateSigned;
	private Double amtTotalPayment;
	private Double amtEt;
	private Integer cntEtDays;
	private Date dateBankEt;
	private Double amtBankDisbursed;
	private Double amtBankPayment;
	private Double amtBankEt;
	private Date dtimeCreated;
	private String createdBy;
	private Date dateBankPrinted;
	private Date dtimeUpdated;
	private String updatedBy;
	private Double bankPrincipal;
	private Date dateEt;
	private Long incomingPaymentId;
	@Column(name=CoreColumnName.BANK_REFERENCE_NO)
	public String getBankReferenceNo(){
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo){
		this.bankReferenceNo=bankReferenceNo;
	}
	@Column(name=CoreColumnName.DATE_BANK_DISBURSED)
	public Date getDateBankDisbursed(){
		return dateBankDisbursed;
	}
	public void setDateBankDisbursed(Date dateBankDisbursed){
		this.dateBankDisbursed=dateBankDisbursed;
	}
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.CLIENT_NAME)
	public String getClientName(){
		return clientName;
	}
	public void setClientName(String clientName){
		this.clientName=clientName;
	}
	@Column(name=CoreColumnName.DATE_SIGNED)
	public Date getDateSigned(){
		return dateSigned;
	}
	public void setDateSigned(Date dateSigned){
		this.dateSigned=dateSigned;
	}
	@Column(name=CoreColumnName.AMT_TOTAL_PAYMENT)
	public Double getAmtTotalPayment(){
		return amtTotalPayment;
	}
	public void setAmtTotalPayment(Double amtTotalPayment){
		this.amtTotalPayment=amtTotalPayment;
	}
	@Column(name=CoreColumnName.AMT_ET)
	public Double getAmtEt(){
		return amtEt;
	}
	public void setAmtEt(Double amtEt){
		this.amtEt=amtEt;
	}
	@Column(name=CoreColumnName.CNT_ET_DAYS)
	public Integer getCntEtDays(){
		return cntEtDays;
	}
	public void setCntEtDays(Integer cntEtDays){
		this.cntEtDays=cntEtDays;
	}
	@Column(name=CoreColumnName.DATE_BANK_ET)
	public Date getDateBankEt(){
		return dateBankEt;
	}
	public void setDateBankEt(Date dateBankEt){
		this.dateBankEt=dateBankEt;
	}
	@Column(name=CoreColumnName.AMT_BANK_DISBURSED)
	public Double getAmtBankDisbursed(){
		return amtBankDisbursed;
	}
	public void setAmtBankDisbursed(Double amtBankDisbursed){
		this.amtBankDisbursed=amtBankDisbursed;
	}
	@Column(name=CoreColumnName.AMT_BANK_PAYMENT)
	public Double getAmtBankPayment(){
		return amtBankPayment;
	}
	public void setAmtBankPayment(Double amtBankPayment){
		this.amtBankPayment=amtBankPayment;
	}
	@Column(name=CoreColumnName.AMT_BANK_ET)
	public Double getAmtBankEt(){
		return amtBankEt;
	}
	public void setAmtBankEt(Double amtBankEt){
		this.amtBankEt=amtBankEt;
	}
	@Id
	@Column(name=CoreColumnName.DTIME_CREATED)
	public Date getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(Date dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
	@Column(name=CoreColumnName.CREATED_BY)
	public String getCreatedBy(){
		return createdBy;
	}
	public void setCreatedBy(String createdBy){
		this.createdBy=createdBy;
	}
	@Column(name=CoreColumnName.DATE_BANK_PRINTED)
	public Date getDateBankPrinted(){
		return dateBankPrinted;
	}
	public void setDateBankPrinted(Date dateBankPrinted){
		this.dateBankPrinted=dateBankPrinted;
	}
	@Column(name=CoreColumnName.DTIME_UPDATED)
	public Date getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(Date dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
	@Column(name=CoreColumnName.UPDATED_BY)
	public String getUpdatedBy(){
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy){
		this.updatedBy=updatedBy;
	}
	@Column(name=CoreColumnName.BANK_PRINCIPAL)
	public Double getBankPrincipal(){
		return bankPrincipal;
	}
	public void setBankPrincipal(Double bankPrincipal){
		this.bankPrincipal=bankPrincipal;
	}
	@Column(name=CoreColumnName.DATE_ET)
	public Date getDateEt(){
		return dateEt;
	}
	public void setDateEt(Date dateEt){
		this.dateEt=dateEt;
	}
	@Column(name=CoreColumnName.INCOMING_PAYMENT_ID)
	public Long getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(Long incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsContractEt [bankReferenceNo=");
		json.append(bankReferenceNo);
		json.append(",dateBankDisbursed=");
		json.append(dateBankDisbursed);
		json.append(",textContractNumber=");
		json.append(textContractNumber);
		json.append(",clientName=");
		json.append(clientName);
		json.append(",dateSigned=");
		json.append(dateSigned);
		json.append(",amtTotalPayment=");
		json.append(amtTotalPayment);
		json.append(",amtEt=");
		json.append(amtEt);
		json.append(",cntEtDays=");
		json.append(cntEtDays);
		json.append(",dateBankEt=");
		json.append(dateBankEt);
		json.append(",amtBankDisbursed=");
		json.append(amtBankDisbursed);
		json.append(",amtBankPayment=");
		json.append(amtBankPayment);
		json.append(",amtBankEt=");
		json.append(amtBankEt);
		json.append(",dtimeCreated=");
		json.append(dtimeCreated);
		json.append(",createdBy=");
		json.append(createdBy);
		json.append(",dateBankPrinted=");
		json.append(dateBankPrinted);
		json.append(",dtimeUpdated=");
		json.append(dtimeUpdated);
		json.append(",updatedBy=");
		json.append(updatedBy);
		json.append(",bankPrincipal=");
		json.append(bankPrincipal);
		json.append(",dateEt=");
		json.append(dateEt);
		json.append(",incomingPaymentId=");
		json.append(incomingPaymentId);
		json.append("]");
		return json.toString();
	}
}
