package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@code JFS_CONTRACT_WO}
 * 
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=CoreTableName.JFS_CONTRACT_WO)
public class JfsContractWo{
	private String textContractNumber;
	private Date exportDate;
	private Date woDate;
	private String woReason;
	private Double woAmount;
	private Double outstandingWoAmount;
	private String status;
	private Date dtimeStatusUpdated;
	private Date dateBankProcess;
	@Id
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.EXPORT_DATE)
	public Date getExportDate(){
		return exportDate;
	}
	public void setExportDate(Date exportDate){
		this.exportDate=exportDate;
	}
	@Column(name=CoreColumnName.WO_DATE)
	public Date getWoDate(){
		return woDate;
	}
	public void setWoDate(Date woDate){
		this.woDate=woDate;
	}
	@Column(name=CoreColumnName.WO_REASON)
	public String getWoReason(){
		return woReason;
	}
	public void setWoReason(String woReason){
		this.woReason=woReason;
	}
	@Column(name=CoreColumnName.WO_AMOUNT)
	public Double getWoAmount(){
		return woAmount;
	}
	public void setWoAmount(Double woAmount){
		this.woAmount=woAmount;
	}
	@Column(name=CoreColumnName.OUTSTANDING_WO_AMOUNT)
	public Double getOutstandingWoAmount(){
		return outstandingWoAmount;
	}
	public void setOutstandingWoAmount(Double outstandingWoAmount){
		this.outstandingWoAmount=outstandingWoAmount;
	}
	@Column(name=CoreColumnName.STATUS)
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	@Column(name=CoreColumnName.DTIME_STATUS_UPDATED)
	public Date getDtimeStatusUpdated(){
		return dtimeStatusUpdated;
	}
	public void setDtimeStatusUpdated(Date dtimeStatusUpdated){
		this.dtimeStatusUpdated=dtimeStatusUpdated;
	}
	@Column(name=CoreColumnName.DATE_BANK_PROCESS)
	public Date getDateBankProcess(){
		return dateBankProcess;
	}
	public void setDateBankProcess(Date dateBankProcess){
		this.dateBankProcess=dateBankProcess;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsPaymentWo [textContractNumber=");
		json.append(textContractNumber);
		json.append(",exportDate=");
		json.append(exportDate);
		json.append(",woDate=");
		json.append(woDate);
		json.append(",woReason=");
		json.append(woReason);
		json.append(",woAmount=");
		json.append(woAmount);
		json.append(",outstandingWoAmount=");
		json.append(outstandingWoAmount);
		json.append(",status=");
		json.append(status);
		json.append(",dtimeStatusUpdated=");
		json.append(dtimeStatusUpdated);
		json.append(",dateBankProcess=");
		json.append(dateBankProcess);
		json.append("]");
		return json.toString();
	}
}
