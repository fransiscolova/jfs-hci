package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@link JFS_CUSTOMER}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_CUSTOMER)
public class JfsCustomer {

	private String cuid;
	private String nameFull;
	private String nameMother;
	private String dateBirth;
	private String textIdentificationDocNumber;
	private String nameBirthPlace;
	@Id
	@Column(name=CoreColumnName.CUID)
	public String getCuid(){
		return cuid;
	}
	public void setCuid(String cuid){
		this.cuid=cuid;
	}
	@Column(name=CoreColumnName.NAME_FULL)
	public String getNameFull(){
		return nameFull;
	}
	public void setNameFull(String nameFull){
		this.nameFull=nameFull;
	}
	@Column(name=CoreColumnName.NAME_MOTHER)
	public String getNameMother(){
		return nameMother;
	}
	public void setNameMother(String nameMother){
		this.nameMother=nameMother;
	}
	@Column(name=CoreColumnName.DATE_BIRTH)
	public String getDateBirth(){
		return dateBirth;
	}
	public void setDateBirth(String dateBirth){
		this.dateBirth=dateBirth;
	}
	@Column(name=CoreColumnName.TEXT_IDENTIFICATION_DOC_NUMBER)
	public String getTextIdentificationDocNumber(){
		return textIdentificationDocNumber;
	}
	public void setTextIdentificationDocNumber(String textIdentificationDocNumber){
		this.textIdentificationDocNumber=textIdentificationDocNumber;
	}
	@Column(name=CoreColumnName.NAME_BIRTH_PLACE)
	public String getNameBirthPlace(){
		return nameBirthPlace;
	}
	public void setNameBirthPlace(String nameBirthPlace){
		this.nameBirthPlace=nameBirthPlace;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsCustomer [cuid=");
		json.append(cuid);
		json.append(",nameFull=");
		json.append(nameFull);
		json.append(",nameMother=");
		json.append(nameMother);
		json.append(",dateBirth=");
		json.append(dateBirth);
		json.append(",textIdentificationDocNumber=");
		json.append(textIdentificationDocNumber);
		json.append(",nameBirthPlace=");
		json.append(nameBirthPlace);
		json.append("]");
		return json.toString();
	}

}
