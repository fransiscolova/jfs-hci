package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity Class For {@code JFS_INPAY_ALLOCATION_FIX}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_INPAY_ALLOCATION_FIX)
public class JfsInpayAllocationFix{
	private String textContractNumber;
	private String dueDate;
	private String partIndex;
	private String amtPrincipal;
	private String amtInterest;
	private String archieved;
	private String dtimeCreated;
	private String dtimeUpdated;
	private String incomingPaymentId;
	private String amtOverpayment;
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.DUE_DATE)
	public String getDueDate(){
		return dueDate;
	}
	public void setDueDate(String dueDate){
		this.dueDate=dueDate;
	}
	@Column(name=CoreColumnName.PART_INDEX)
	public String getPartIndex(){
		return partIndex;
	}
	public void setPartIndex(String partIndex){
		this.partIndex=partIndex;
	}
	@Column(name=CoreColumnName.AMT_PRINCIPAL)
	public String getAmtPrincipal(){
		return amtPrincipal;
	}
	public void setAmtPrincipal(String amtPrincipal){
		this.amtPrincipal=amtPrincipal;
	}
	@Column(name=CoreColumnName.AMT_INTEREST)
	public String getAmtInterest(){
		return amtInterest;
	}
	public void setAmtInterest(String amtInterest){
		this.amtInterest=amtInterest;
	}
	@Column(name=CoreColumnName.ARCHIEVED)
	public String getArchieved(){
		return archieved;
	}
	public void setArchieved(String archieved){
		this.archieved=archieved;
	}
	@Column(name=CoreColumnName.DTIME_CREATED)
	public String getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(String dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
	@Column(name=CoreColumnName.DTIME_UPDATED)
	public String getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(String dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
	@Id
	@Column(name=CoreColumnName.INCOMING_PAYMENT_ID)
	public String getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(String incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=CoreColumnName.AMT_OVERPAYMENT)
	public String getAmtOverpayment(){
		return amtOverpayment;
	}
	public void setAmtOverPayment(String amtOverpayment){
		this.amtOverpayment=amtOverpayment;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsInpayAllocationFix [textContractNumber=");
		json.append(textContractNumber);
		json.append(",dueDate=");
		json.append(dueDate);
		json.append(",partIndex=");
		json.append(partIndex);
		json.append(",amtPrincipal=");
		json.append(amtPrincipal);
		json.append(",amtInterest=");
		json.append(amtInterest);
		json.append(",archieved=");
		json.append(archieved);
		json.append(",dtimeCreated=");
		json.append(dtimeCreated);
		json.append(",dtimeUpdated=");
		json.append(dtimeUpdated);
		json.append(",incomingPaymentId=");
		json.append(incomingPaymentId);
		json.append(",amtOverpayment=");
		json.append(amtOverpayment);
		json.append("]");
		return json.toString();
	}
}
