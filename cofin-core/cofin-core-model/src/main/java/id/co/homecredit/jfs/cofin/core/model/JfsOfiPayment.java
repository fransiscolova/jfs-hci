package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity Class For {@link JFS_OFI_PAYMENT}
 * 
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=CoreTableName.JFS_OFI_PAYMENT)
public class JfsOfiPayment extends BaseEntityIdOnly<String>{
	private Long incomingPaymentId;
	private String textContractNumber;
	private Date dueDate;
	private Date paymentDate;
	private Double ofiPrincipal;
	private Double ofiInterest;
	private Date dtimeCreated;
	private String createdBy;
	private Date dtimeUpdated;
	private String updatedBy;
	private Integer ver;
	private String isActive;
	@Column(name=CoreColumnName.INCOMING_PAYMENT_ID)
	public Long getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(Long incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.DUE_DATE)
	public Date getDueDate(){
		return dueDate;
	}
	public void setDueDate(Date dueDate){
		this.dueDate=dueDate;
	}
	@Column(name=CoreColumnName.PAYMENT_DATE)
	public Date getPaymentDate(){
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate){
		this.paymentDate=paymentDate;
	}
	@Column(name=CoreColumnName.OFI_PRINCIPAL)
	public Double getOfiPrincipal(){
		return ofiPrincipal;
	}
	public void setOfiPrincipal(Double ofiPrincipal){
		this.ofiPrincipal=ofiPrincipal;
	}
	@Column(name=CoreColumnName.OFI_INTEREST)
	public Double getOfiInterest(){
		return ofiInterest;
	}
	public void setOfiInterest(Double ofiInterest){
		this.ofiInterest=ofiInterest;
	}
	@Column(name=CoreColumnName.DTIME_CREATED)
	public Date getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(Date dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
	@Column(name=CoreColumnName.CREATED_BY)
	public String getCreatedBy(){
		return createdBy;
	}
	public void setCreatedBy(String createdBy){
		this.createdBy=createdBy;
	}
	@Column(name=CoreColumnName.DTIME_UPDATED)
	public Date getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(Date dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
	@Column(name=CoreColumnName.UPDATED_BY)
	public String getUpdatedBy(){
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy){
		this.updatedBy=updatedBy;
	}
	@Column(name=CoreColumnName.VER)
	public Integer getVer(){
		return ver;
	}
	public void setVer(Integer ver){
		this.ver=ver;
	}
	@Column(name=CoreColumnName.IS_ACTIVE)
	public String getIsActive(){
		return isActive;
	}
	public void setIsActive(String isActive){
		this.isActive=isActive;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsOfiPayment [id=");
		json.append(id);
		json.append(",incomingPaymentId=");
		json.append(incomingPaymentId);
		json.append(",textContractNumber=");
		json.append(textContractNumber);
		json.append(",ofiPrincipal=");
		json.append(ofiPrincipal);
		json.append(",ofiInterest=");
		json.append(ofiInterest);
		json.append(",dtimeCreated=");
		json.append(dtimeCreated);
		json.append(",createdBy=");
		json.append(createdBy);
		json.append(",dtimeUpdated=");
		json.append(dtimeUpdated);
		json.append(",updatedBy=");
		json.append(updatedBy);
		json.append(",ver=");
		json.append(ver);
		json.append(",isActive=");
		json.append(isActive);
		json.append("]");
		return json.toString();
	}
}
