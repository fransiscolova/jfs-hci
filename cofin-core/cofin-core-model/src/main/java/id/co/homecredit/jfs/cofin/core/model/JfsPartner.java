package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity class for {@code JFS_PARTNER}
 * 
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=CoreTableName.JFS_PARTNER)
public class JfsPartner{
	private String id;
	private String name;
	private String address;
	private String mainContact;
	private String phoneNumber;
	private String email;
	private String isDelete;
	private String isCheckingEligibility;
	private Long priority;
	@Id@Column(name=CoreColumnName.ID)
	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id=id;
	}
	@Column(name=CoreColumnName.NAME)
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name=name;
	}
	@Column(name=CoreColumnName.ADDRESS)
	public String getAddress(){
		return address;
	}
	public void setAddress(String address){
		this.address=address;
	}
	@Column(name=CoreColumnName.MAIN_CONTACT)
	public String getMainContact(){
		return mainContact;
	}
	public void setMainContact(String mainContact){
		this.mainContact=mainContact;
	}
	@Column(name=CoreColumnName.PHONE_NUMBER)
	public String getPhoneNumber(){
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber=phoneNumber;
	}
	@Column(name=CoreColumnName.EMAIL)
	public String getEmail(){
		return email;
	}
	public void setEmail(String email){
		this.email=email;
	}
	@Column(name=CoreColumnName.IS_DELETE)
	public String getIsDelete(){
		return isDelete;
	}
	public void setIsDelete(String isDelete){
		this.isDelete=isDelete;
	}
	@Column(name=CoreColumnName.IS_CHECKING_ELIGIBILITY)
	public String getIsCheckingEligibility(){
		return isCheckingEligibility;
	}
	public void setIsCheckingEligibility(String isCheckingEligibility){
		this.isCheckingEligibility=isCheckingEligibility;
	}
	@Column(name=CoreColumnName.PRIORITY)
	public Long getPriority(){
		return priority;
	}
	public void setPriority(Long priority){
		this.priority=priority;
	}
	@Override
	public String toString(){
		StringBuilder xjson = new StringBuilder(1000);
		xjson.append("JfsPartner [id=");
		xjson.append(id);
		xjson.append(",name=");
		xjson.append(name);
		xjson.append(",address=");
		xjson.append(address);
		xjson.append(",mainContact=");
		xjson.append(mainContact);
		xjson.append(",phoneNumber=");
		xjson.append(phoneNumber);
		xjson.append(",email=");
		xjson.append(email);
		xjson.append(",isDelete=");
		xjson.append(isDelete);
		xjson.append(",isCheckingEligibility=");
		xjson.append(isCheckingEligibility);
		xjson.append(",priority=");
		xjson.append(priority);
		xjson.append("]");
		return xjson.toString();
	}
}
