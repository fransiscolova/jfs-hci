package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;

/**
 * Entity Class For {@link JFS_PAYMENT_ALLOCATION}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_PAYMENT_ALLOCATION)
public class JfsPaymentAllocation extends BaseEntityIdOnly<String>{
	private String textContractNumber;
	private String dueDate;
	private String partIndex;
	private String amtPrincipal;
	private String amtInterest;
	private String archieved;
	private String dtimeCreated;
	private String dtimeUpdated;
	private String paymentType;
	private String amtPenalty;
	private String amtFee;
	private String reference;
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.DUE_DATE)
	public String getDueDate(){
		return dueDate;
	}
	public void setDueDate(String dueDate){
		this.dueDate=dueDate;
	}
	@Column(name=CoreColumnName.PART_INDEX)
	public String getPartIndex(){
		return partIndex;
	}
	public void  setPartIndex(String partIndex){
		this.partIndex=partIndex;
	}
	@Column(name=CoreColumnName.AMT_PRINCIPAL)
	public String getAmtPrincipal(){
		return amtPrincipal;
	}
	public void setAmtPrincipal(String amtPrincipal){
		this.amtPrincipal=amtPrincipal;
	}
	@Column(name=CoreColumnName.AMT_INTEREST)
	public String getAmtInterest(){
		return amtInterest;
	}
	public void setAmtInterest(String amtInterest){
		this.amtInterest=amtInterest;
	}
	@Column(name=CoreColumnName.ARCHIEVED)
	public String getArchieved(){
		return archieved;
	}
	public void setArchieved(String archieved){
		this.archieved=archieved;
	}
	@Column(name=CoreColumnName.DTIME_CREATED)
	public String getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(String dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
	@Column(name=CoreColumnName.DTIME_UPDATED)
	public String getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(String dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
	@Column(name=CoreColumnName.PAYMENT_TYPE)
	public String getPaymentType(){
		return paymentType;
	}
	public void setPaymentType(String paymentType){
		this.paymentType=paymentType;
	}
	@Column(name=CoreColumnName.AMT_PENALTY)
	public String getAmtPenalty(){
		return amtPenalty;
	}
	public void setAmtPenalty(String amtPenalty){
		this.amtPenalty=amtPenalty;
	}
	@Column(name=CoreColumnName.AMT_FEE)
	public String getAmtFee(){
		return amtFee;
	}
	public void setAmtFee(String amtFee){
		this.amtFee=amtFee;
	}
	@Column(name=CoreColumnName.REFERENCE)
	public String getReference(){
		return reference;
	}
	public void setReference(String reference){
		this.reference=reference;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsPaymentAllocation [id=");
		json.append(id);
		json.append(",textContractNumber=");
		json.append(textContractNumber);
		json.append(",dueDate=");
		json.append(dueDate);
		json.append(",partIndex=");
		json.append(partIndex);
		json.append(",amtPrincipal=");
		json.append(amtPrincipal);
		json.append(",amtInterest=");
		json.append(amtInterest);
		json.append(",archieved=");
		json.append(archieved);
		json.append(",dtimeCreated=");
		json.append(dtimeCreated);
		json.append(",dtimeUpdated=");
		json.append(dtimeUpdated);
		json.append(",paymentType=");
		json.append(paymentType);
		json.append(",amtPenalty=");
		json.append(amtPenalty);
		json.append(",amtFee=");
		json.append(amtFee);
		json.append(",reference=");
		json.append(reference);
		json.append("]");
		return json.toString();
	}
}
