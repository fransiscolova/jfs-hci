package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdDate;

/**
 * Entity Class For {@link JFS_PAYMENt_BANK_SIM}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_PAYMENT_BANK_SIM)
public class JfsPaymentBankSim extends BaseEntityIdDate<String>{
	private Long incomingPaymentId;
	private String textContractNumber;
	private Date dateInstalment;
	private Integer instalmentNumber;
	private Date paymentDate;
	private Double amtPrincipal;
	private Double amtInterest;
	private Double amtPenalty;
	private Double amtOverpayment;
	private Date dtimeCreated;
	private String createdBy;
	private Date dtimeUpdated;
	private String updatedBy;
	private Integer version;
	@Column(name=CoreColumnName.INCOMING_PAYMENT_ID)
	public Long getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(Long incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.DATE_INSTALMENT)
	public Date getDateInstalment(){
		return dateInstalment;
	}
	public void setDateInstalment(Date dateInstalment){
		this.dateInstalment=dateInstalment;
	}
	@Column(name=CoreColumnName.INSTALMENT_NUMBER)
	public Integer getInstalmentNumber(){
		return instalmentNumber;
	}
	public void setInstalmentNumber(Integer instalmentNumber){
		this.instalmentNumber=instalmentNumber;
	}
	@Column(name=CoreColumnName.PAYMENT_DATE)
	public Date getPaymentDate(){
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate){
		this.paymentDate=paymentDate;
	}
	@Column(name=CoreColumnName.AMT_PRINCIPAL)
	public Double getAmtPrincipal(){
		return amtPrincipal;
	}
	public void setAmtPrincipal(Double amtPrincipal){
		this.amtPrincipal=amtPrincipal;
	}
	@Column(name=CoreColumnName.AMT_INTEREST)
	public Double getAmtInterest(){
		return amtInterest;
	}
	public void setAmtInterest(Double amtInterest){
		this.amtInterest=amtInterest;
	}
	@Column(name=CoreColumnName.AMT_PENALTY)
	public Double getAmtPenalty(){
		return amtPenalty;
	}
	public void setAmtPenalty(Double amtPenalty){
		this.amtPenalty=amtPenalty;
	}
	@Column(name=CoreColumnName.AMT_OVERPAYMENT)
	public Double getAmtOverpayment(){
		return amtOverpayment;
	}
	public void setAmtOverpayment(Double amtOverpayment){
		this.amtOverpayment=amtOverpayment;
	}
	@Column(name=CoreColumnName.DTIME_CREATED)
	public Date getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(Date dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
	@Column(name=CoreColumnName.CREATED_BY)
	public String getCreatedBy(){
		return createdBy;
	}
	public void setCreatedBy(String createdBy){
		this.createdBy=createdBy;
	}
	@Column(name=CoreColumnName.DTIME_UPDATED)
	public Date getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(Date dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
	@Column(name=CoreColumnName.UPDATED_BY)
	public String getUpdatedBy(){
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy){
		this.updatedBy=updatedBy;
	}
	@Column(name=CoreColumnName.VERSION)
	public Integer getVersion(){
		return version;
	}
	public void setVersion(Integer version){
		this.version=version;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsPaymentBankSim [id=");
		json.append(id);
		json.append(",incomingPaymentId=");
		json.append(incomingPaymentId);
		json.append(",textContractNumber=");
		json.append(textContractNumber);
		json.append(",dateInstalment=");
		json.append(dateInstalment);
		json.append(",instalmentNumber=");
		json.append(instalmentNumber);
		json.append(",paymentDate=");
		json.append(paymentDate);
		json.append(",amtPrincipal=");
		json.append(amtPrincipal);
		json.append(",amtInterest=");
		json.append(amtInterest);
		json.append(",amtPenalty=");
		json.append(amtPenalty);
		json.append(",amtOverpayment=");
		json.append(amtOverpayment);
		json.append(",dtimeCreated=");
		json.append(dtimeCreated);
		json.append(",createdBy=");
		json.append(createdBy);
		json.append(",dtimeUpdated=");
		json.append(dtimeUpdated);
		json.append(",updatedBy=");
		json.append(updatedBy);
		json.append(",version=");
		json.append(version);
		json.append("]");
		return json.toString();
	}
}
