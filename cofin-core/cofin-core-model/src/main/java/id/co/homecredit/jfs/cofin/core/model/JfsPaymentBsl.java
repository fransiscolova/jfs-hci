package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.*;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@code JFS_PAYMENT_BSL}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_PAYMENT_BSL)
public class JfsPaymentBsl extends BaseEntityIdOnly<String>{
	private Long skfInstalmentHead;
	private Long incomingPaymentId;
	private String textContractNumber;
	private Date dateInstalment;
	private Date paymentDate;
	private Date dtimePaymentPair;
	private Date dtimePaymentUnpair;
	private String textPaymentChannel;
	private String paymentTypeCode;
	private String paymentType;
	private Double totalAmt;
	private Double amtPrincipal;
	private Double amtInterest;
	private Double amtPrincipalT;
	private Double amtInterestT;
	private Double amtOverpayment;
	private Double amtPenalty;
	private Double amtOrigFee;
	private Double amtMonthlyFee;
	private Double amtEtFee;
	private Double amtMpfPenaltyFee;
	private Double amtMonthlyFeeIns;
	private Date dtimeCreated;
	private String createdBy;
	private Date dtimeUpdated;
	private String updatedBy;
	private Integer version;
	private Integer instalmentNumber;
	private String debtTarifItemTypeCode;
	@Column(name=CoreColumnName.SKF_INSTALMENT_HEAD)
	public Long getSkfInstalmentHead(){
		return skfInstalmentHead;
	}
	public void setSkfInstalmentHead(Long skfInstalmentHead){
		this.skfInstalmentHead=skfInstalmentHead;
	}
	@Column(name=CoreColumnName.INCOMING_PAYMENT_ID)
	public Long getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(Long incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.DATE_INSTALMENT)
	public Date getDateInstalment(){
		return dateInstalment;
	}
	public void setDateInstalment(Date dateInstalment){
		this.dateInstalment=dateInstalment;
	}
	@Column(name=CoreColumnName.PAYMENT_DATE)
	public Date getPaymentDate(){
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate){
		this.paymentDate=paymentDate;
	}
	@Column(name=CoreColumnName.DTIME_PAYMENT_PAIR)
	public Date getDtimePaymentPair(){
		return dtimePaymentPair;
	}
	public void setDtimePaymentPair(Date dtimePaymentPair){
		this.dtimePaymentPair=dtimePaymentPair;
	}
	@Column(name=CoreColumnName.DTIME_PAYMENT_UNPAIR)
	public Date getDtimePaymentUnpair(){
		return dtimePaymentUnpair;
	}
	public void setDtimePaymentUnpair(Date dtimePaymentUnpair){
		this.dtimePaymentUnpair=dtimePaymentUnpair;
	}
	@Column(name=CoreColumnName.TEXT_PAYMENT_CHANNEL)
	public String getTextPaymentChannel(){
		return textPaymentChannel;
	}
	public void setTextPaymentChannel(String textPaymentChannel){
		this.textPaymentChannel=textPaymentChannel;
	}
	@Column(name=CoreColumnName.PAYMENT_TYPE_CODE)
	public String getPaymentTypeCode(){
		return paymentTypeCode;
	}
	public void setPaymentTypeCode(String paymentTypeCode){
		this.paymentTypeCode=paymentTypeCode;
	}
	@Column(name=CoreColumnName.PAYMENT_TYPE)
	public String getPaymentType(){
		return paymentType;
	}
	public void setPaymentType(String paymentType){
		this.paymentType=paymentType;
	}
	@Column(name=CoreColumnName.TOTAL_AMT)
	public Double getTotalAmt(){
		return totalAmt;
	}
	public void setTotalAmt(Double totalAmt){
		this.totalAmt=totalAmt;
	}
	@Column(name=CoreColumnName.AMT_PRINCIPAL)
	public Double getAmtPrincipal(){
		return amtPrincipal;
	}
	public void setAmtPrincipal(Double amtPrincipal){
		this.amtPrincipal=amtPrincipal;
	}
	@Column(name=CoreColumnName.AMT_INTEREST)
	public Double getAmtInterest(){
		return amtInterest;
	}
	public void setAmtInterest(Double amtInterest){
		this.amtInterest=amtInterest;
	}
	@Column(name=CoreColumnName.AMT_PRINCIPAL_T)
	public Double getAmtPrincipalT(){
		return amtPrincipalT;
	}
	public void setAmtPrincipalT(Double amtPrincipalT){
		this.amtPrincipalT=amtPrincipalT;
	}
	@Column(name=CoreColumnName.AMT_INTEREST_T)
	public Double getAmtInterestT(){
		return amtInterestT;
	}
	public void setAmtInterestT(Double amtInterestT){
		this.amtInterestT=amtInterestT;
	}
	@Column(name=CoreColumnName.AMT_OVERPAYMENT)
	public Double getAmtOverpayment(){
		return amtOverpayment;
	}
	public void setAmtOverpayment(Double amtOverpayment){
		this.amtOverpayment=amtOverpayment;
	}
	@Column(name=CoreColumnName.AMT_PENALTY)
	public Double getAmtPenalty(){
		return amtPenalty;
	}
	public void setAmtPenalty(Double amtPenalty){
		this.amtPenalty=amtPenalty;
	}
	@Column(name=CoreColumnName.AMT_ORIG_FEE)
	public Double getAmtOrigFee(){
		return amtOrigFee;
	}
	public void setAmtOrigFee(Double amtOrigFee){
		this.amtOrigFee=amtOrigFee;
	}
	@Column(name=CoreColumnName.AMT_MONTHLY_FEE)
	public Double getAmtMonthlyFee(){
		return amtMonthlyFee;
	}
	public void setAmtMonthlyFee(Double amtMonthlyFee){
		this.amtMonthlyFee=amtMonthlyFee;
	}
	@Column(name=CoreColumnName.AMT_ET_FEE)
	public Double getAmtEtFee(){
		return amtEtFee;
	}
	public void setAmtEtFee(Double amtEtFee){
		this.amtEtFee=amtEtFee;
	}
	@Column(name=CoreColumnName.AMT_MPF_PENALTY_FEE)
	public Double getAmtMpfPenaltyFee(){
		return amtMpfPenaltyFee;
	}
	public void setAmtMpfPenaltyFee(Double amtMpfPenaltyFee){
		this.amtMpfPenaltyFee=amtMpfPenaltyFee;
	}
	@Column(name=CoreColumnName.AMT_MONTHLY_FEE_INS)
	public Double getAmtMonthlyFeeIns(){
		return amtMonthlyFeeIns;
	}
	public void setAmtMonthlyFeeIns(Double amtMonthlyFeeIns){
		this.amtMonthlyFeeIns=amtMonthlyFeeIns;
	}
	@Column(name=CoreColumnName.DTIME_CREATED)
	public Date getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(Date dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
	@Column(name=CoreColumnName.CREATED_BY)
	public String getCreatedBy(){
		return createdBy;
	}
	public void setCreatedBy(String createdBy){
		this.createdBy=createdBy;
	}
	@Column(name=CoreColumnName.DTIME_UPDATED)
	public Date getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(Date dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
	@Column(name=CoreColumnName.UPDATED_BY)
	public String getUpdatedBy(){
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy){
		this.updatedBy=updatedBy;
	}
	@Column(name=CoreColumnName.VERSION)
	public Integer getVersion(){
		return version;
	}
	public void setVersion(Integer version){
		this.version=version;
	}
	@Column(name=CoreColumnName.INSTALMENT_NUMBER)
	public Integer getInstalmentNumber(){
		return instalmentNumber;
	}
	public void setInstalmentNumber(Integer instalmentNumber){
		this.instalmentNumber=instalmentNumber;
	}
	@Column(name=CoreColumnName.DEBT_TARIF_ITEM_TYPE_CODE)
	public String getDebtTarifItemTypeCode(){
		return debtTarifItemTypeCode;
	}
	public void setDebtTarifItemTypeCode(String debtTarifItemTypeCode){
		this.debtTarifItemTypeCode=debtTarifItemTypeCode;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsPaymentBsl [id="); 
		json.append(id);
		json.append(",skfInstalmentHead="); 
		json.append(skfInstalmentHead);
		json.append(",incomingPaymentId="); 
		json.append(incomingPaymentId);
		json.append(",textContractNumber="); 
		json.append(textContractNumber);
		json.append(",dateInstalment="); 
		json.append(dateInstalment);
		json.append(",paymentDate="); 
		json.append(paymentDate);
		json.append(",dtimePaymentPair=");
		json.append(dtimePaymentPair);
		json.append(",dtimePaymentUnpair=");
		json.append(dtimePaymentUnpair);
		json.append(",textPaymentChannel="); 
		json.append(textPaymentChannel);
		json.append(",paymentTypeCode="); 
		json.append(paymentTypeCode);
		json.append(",paymentType="); 
		json.append(paymentType);
		json.append(",totalAmt="); 
		json.append(totalAmt);
		json.append(",amtPrincipal=");
		json.append(amtPrincipal);
		json.append(",amtInterest="); 
		json.append(amtInterest);
		json.append(",amtPrincipalT="); 
		json.append(amtPrincipalT);
		json.append(",amtInterestT="); 
		json.append(amtInterestT);
		json.append(",amtOverpayment="); 
		json.append(amtOverpayment);
		json.append(",amtPenalty="); 
		json.append(amtPenalty);
		json.append(",amtOrigFee="); 
		json.append(amtOrigFee);
		json.append(",amtMonthlyFee="); 
		json.append(amtMonthlyFee);
		json.append(",amtEtFee="); 
		json.append(amtEtFee);
		json.append(",amtMpfPenaltyFee="); 
		json.append(amtMpfPenaltyFee);
		json.append(",amtMonthlyFeeIns=");
		json.append(amtMonthlyFeeIns);
		json.append(",dtimeCreated=");
		json.append(dtimeCreated);
		json.append(",createdBy=");
		json.append(createdBy);
		json.append(",dtimeUpdated=");
		json.append(dtimeUpdated);
		json.append(",updatedBy=");
		json.append(updatedBy);
		json.append(",version=");
		json.append(version);
		json.append(",instalmentNumber=");
		json.append(instalmentNumber);
		json.append(",debtTarifItemTypeCode=");
		json.append(debtTarifItemTypeCode);
		json.append("]");
		return json.toString();
	}

	private String id;

	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
