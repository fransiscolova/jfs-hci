package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@link JFS_PAYMENT_HCI_PORTION}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_PAYMENT_HCI_PORTION)
public class JfsPaymentHciPortion{
	private String id;
	private String skfInstalmentHead;
	private String incomingPaymentId;
	private String textContractNumber;
	private String dateInstalment;
	private String paymentDate;
	private String paymentType;
	private String amtPrincipal;
	private String amtInterest;
	private String amtPrincipalT;
	private String amtInterestT;
	private String amtMonthlyFee;
	private String amtEtFee;
	private String amtPenalty;
	private String dtimeCreated;
	private String createdBy;
	private String dtimeUpdated;
	private String updatedBy;
	private String ver;
	private String isActive;
	@Id
	@Column(name=CoreColumnName.ID)
	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id=id;
	}
	@Column(name=CoreColumnName.SKF_INSTALMENT_HEAD)
	public String getSkfInstalmentHead(){
		return skfInstalmentHead;
	}
	public void setInstalmentSkfHead(String skfInstalmentHead){
		this.skfInstalmentHead=skfInstalmentHead;
	}
	@Column(name=CoreColumnName.INCOMING_PAYMENT_ID)
	public String getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(String incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.DATE_INSTALMENT)
	public String getDateInstalment(){
		return dateInstalment;	
	}
	public void setDateInstalment(String dateInstalment){
		this.dateInstalment=dateInstalment;
	}
	@Column(name=CoreColumnName.PAYMENT_DATE)
	public String getPaymentDate(){
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate){
		this.paymentDate=paymentDate;
	}
	@Column(name=CoreColumnName.PAYMENT_TYPE)
	public String getPaymentType(){
		return paymentType;
	}
	public void setPaymentType(String paymentType){
		this.paymentType=paymentType;
	}
	@Column(name=CoreColumnName.AMT_PRINCIPAL)
	public String getAmtPrincipal(){
		return amtPrincipal;
	}
	public void setAmtPrincipal(String amtPrincipal){
		this.amtPrincipal=amtPrincipal;
	}
	@Column(name=CoreColumnName.AMT_INTEREST)
	public String getAmtInterest(){
		return amtInterest;
	}
	public void setAmtInterest(String amtInterest){
		this.amtInterest=amtInterest;
	}
	@Column(name=CoreColumnName.AMT_PRINCIPAL_T)
	public String getAmtPrincipalT(){
		return amtPrincipalT;
	}
	public void setAmtPrincipalT(String amtPrincipalT){
		this.amtPrincipalT=amtPrincipalT;
	}
	@Column(name=CoreColumnName.AMT_INTEREST_T)
	public String getAmtInterestT(){
		return amtInterestT;
	}
	public void setAmtInterestT(String amtInterestT){
		this.amtInterestT=amtInterestT;
	}
	@Column(name=CoreColumnName.AMT_MONTHLY_FEE)
	public String getAmtMonthlyFee(){
		return amtMonthlyFee;
	}
	public void setAmtMonthlyFee(String amtMonthlyFee){
		this.amtMonthlyFee=amtMonthlyFee;
	}
	@Column(name=CoreColumnName.AMT_ET_FEE)
	public String getAmtEtFee(){
		return amtEtFee;
	}
	public void setAmtEtFee(String amtEtFee){
		this.amtEtFee=amtEtFee;
	}
	@Column(name=CoreColumnName.AMT_PENALTY)
	public String getAmtPenalty(){
		return amtPenalty;
	}
	public void setAmtPenalty(String amtPenalty){
		this.amtPenalty=amtPenalty;
	}
	@Column(name=CoreColumnName.DTIME_CREATED)
	public String getDtimeCreated(){
		return dtimeCreated;
	}
	public void setDtimeCreated(String dtimeCreated){
		this.dtimeCreated=dtimeCreated;
	}
	@Column(name=CoreColumnName.CREATED_BY)
	public String getCreatedBy(){
		return createdBy;
	}
	public void setCreatedBy(String createdBy){
		this.createdBy=createdBy;
	}
	@Column(name=CoreColumnName.DTIME_UPDATED)
	public String getDtimeUpdated(){
		return dtimeUpdated;
	}
	public void setDtimeUpdated(String dtimeUpdated){
		this.dtimeUpdated=dtimeUpdated;
	}
	@Column(name=CoreColumnName.UPDATED_BY)
	public String getUpdatedBy(){
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy){
		this.updatedBy=updatedBy;
	}
	@Column(name=CoreColumnName.VER)
	public String getVer(){
		return ver;
	}
	public void setVer(String ver){
		this.ver=ver;
	}
	@Column(name=CoreColumnName.IS_ACTIVE)
	public String getIsActive(){
		return isActive;
	}
	public void setIsActive(String isActive){
		this.isActive=isActive;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsPaymentHciPortion [id=");
		json.append(id);
		json.append(",skfInstalmentHead=");
		json.append(skfInstalmentHead);
		json.append(",incomingPaymentId=");
		json.append(incomingPaymentId);
		json.append(",textContractNumber=");
		json.append(textContractNumber);
		json.append(",dateInstalment=");
		json.append(dateInstalment);
		json.append(",paymentDate=");
		json.append(paymentDate);
		json.append(",paymentType=");
		json.append(paymentType);
		json.append(",amtPrincipal=");
		json.append(amtPrincipal);
		json.append(",amtInterest=");
		json.append(amtInterest);
		json.append(",amtPrincipalT=");
		json.append(amtPrincipalT);
		json.append(",amtInterestT=");
		json.append(amtInterestT);
		json.append(",amtMonthlyFee=");
		json.append(amtMonthlyFee);
		json.append(",amtEtFee=");
		json.append(amtEtFee);
		json.append(",amtPenalty=");
		json.append(amtPenalty);
		json.append(",dtimeCreated=");
		json.append(dtimeCreated);
		json.append(",createdby=");
		json.append(createdBy);
		json.append(",dtimeUpdated=");
		json.append(dtimeUpdated);
		json.append(",updatedBy=");
		json.append(updatedBy);
		json.append(",ver=");
		json.append(ver);
		json.append(",isActive=");
		json.append(isActive);
		json.append("]");
		return json.toString();
	}
}
