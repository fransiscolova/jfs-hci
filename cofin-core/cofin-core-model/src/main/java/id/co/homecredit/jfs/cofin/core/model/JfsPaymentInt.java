package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@code JFS_PAYMENT_INT}
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_PAYMENT_INT)
public class JfsPaymentInt{
	private String incomingPaymentId;
	private String textContractNumber;
	private String totalAmtPayment;
	private String amtPayment;
	private String datePayment;
	private String dateExport;
	private String pmtInstallment;
	private String pmtFee;
	private String pmtPenalty;
	private String pmtOverpayment;
	private String pmtOther;
	private String parentId;
	private String status;
	private String reason;
	private String dtimeStatusUpdated;
	private String paymentType;
	private String dateBankProcess;
	private String pmtPrincipal;
	private String pmtInterest;
	private String adjPaymentDate;
	@Id
	@Column(name=CoreColumnName.INCOMING_PAYMENT_ID)
	public String getIncomingPaymentId(){
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(String incomingPaymentId){
		this.incomingPaymentId=incomingPaymentId;
	}
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.TOTAL_AMT_PAYMENT)
	public String getTotalAmtPayment(){
		return totalAmtPayment;
	}
	public void setTotalAmtPayment(String totalAmtPayment){
		this.totalAmtPayment=totalAmtPayment;
	}
	@Column(name=CoreColumnName.AMT_PAYMENT)
	public String getAmtPayment(){
		return amtPayment;
	}
	public void setAmtPayment(String amtPayment){
		this.amtPayment=amtPayment;
	}
	@Column(name=CoreColumnName.DATE_PAYMENT)
	public String getDatePayment(){
		return datePayment;
	}
	public void setDatePayment(String datePayment){
		this.datePayment=datePayment;
	}
	@Column(name=CoreColumnName.DATE_EXPORT)
	public String getDateExport(){
		return dateExport;
	}
	public void setDateExport(String dateExport){
		this.dateExport=dateExport;
	}
	@Column(name=CoreColumnName.PMT_INSTALLMENT)
	public String getPmtInstallment(){
		return pmtInstallment;
	}
	public void setPmtInstallment(String pmtInstallment){
		this.pmtInstallment=pmtInstallment;
	}
	@Column(name=CoreColumnName.PMT_FEE)
	public String getPmtFee(){
		return pmtFee;
	}
	public void setPmtFee(String pmtFee){
		this.pmtFee=pmtFee;
	}
	@Column(name=CoreColumnName.PMT_PENALTY)
	public String getPmtPenalty(){
		return pmtPenalty;
	}
	public void setPmtPenalty(String pmtPenalty){
		this.pmtPenalty=pmtPenalty;
	}
	@Column(name=CoreColumnName.PMT_OVERPAYMENT)
	public String getPmtOverPayment(){
		return pmtOverpayment;
	}
	public void setPmtOverPayment(String pmtOverpayment){
		this.pmtOverpayment=pmtOverpayment;
	}
	@Column(name=CoreColumnName.PMT_OTHER)
	public String getPmtOther(){
		return pmtOther;
	}
	public void setPmtOther(String pmtOther){
		this.pmtOther=pmtOther;
	}
	@Column(name=CoreColumnName.PARENT_ID)
	public String getParentId(){
		return parentId;
	}
	public void setParentId(String parentId){
		this.parentId=parentId;
	}
	@Column(name=CoreColumnName.STATUS)
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	@Column(name=CoreColumnName.REASON)
	public String getReason(){
		return reason;
	}
	public void setReason(String reason){
		this.reason=reason;
	}
	@Column(name=CoreColumnName.DTIME_STATUS_UPDATED)
	public String getDtimeStatusUpdated(){
		return dtimeStatusUpdated;
	}
	public void setDtimeStatusUpdated(String dtimeStatusUpdated){
		this.dtimeStatusUpdated=dtimeStatusUpdated;
	}
	@Column(name=CoreColumnName.PAYMENT_TYPE)
	public String getPaymentType(){
		return paymentType;
	}
	public void setPaymentType(String paymentType){
		this.paymentType=paymentType;
	}
	@Column(name=CoreColumnName.DATE_BANK_PROCESS)
	public String getDateBankProcess(){
		return dateBankProcess;
	}
	public void setDateBankProcess(String dateBankProcess){
		this.dateBankProcess=dateBankProcess;
	}
	@Column(name=CoreColumnName.PMT_PRINCIPAL)
	public String getPmtPrincipal(){
		return pmtPrincipal;
	}
	public void setPmtPrincipal(String pmtPrincipal){
		this.pmtPrincipal=pmtPrincipal;
	}
	@Column(name=CoreColumnName.PMT_INTEREST)
	public String getPmtInterest(){
		return pmtInterest;
	}
	public void setPmtInterest(String pmtInterest){
		this.pmtInterest=pmtInterest;
	}
	@Column(name=CoreColumnName.ADJ_PAYMENT_DATE)
	public String getAdjPaymentDate(){
		return adjPaymentDate;
	}
	public void setAdjPaymentDate(String adjPaymentDate){
		this.adjPaymentDate=adjPaymentDate;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsPaymentInt [incomingPaymentId=");
		json.append(incomingPaymentId);
		json.append(",textContractNumber=");
		json.append(textContractNumber);
		json.append(",totalAmtPayment=");
		json.append(totalAmtPayment);
		json.append(",amtPayment=");
		json.append(amtPayment);
		json.append(",datePayment=");
		json.append(datePayment);
		json.append(",dateExport=");
		json.append(dateExport);
		json.append(",pmtInstallment=");
		json.append(pmtInstallment);
		json.append(",pmtFee=");
		json.append(pmtFee);
		json.append(",pmtPenalty=");
		json.append(pmtPenalty);
		json.append(",pmtOverpayment=");
		json.append(pmtOverpayment);
		json.append(",pmtOther=");
		json.append(pmtOther);
		json.append(",parentId=");
		json.append(parentId);
		json.append(",status=");
		json.append(status);
		json.append(",reason=");
		json.append(reason);
		json.append(",dtimeStatusUpdated=");
		json.append(dtimeStatusUpdated);
		json.append(",paymentType=");
		json.append(paymentType);
		json.append(",dateBankProcess=");
		json.append(dateBankProcess);
		json.append(",pmtPrincipal=");
		json.append(pmtPrincipal);
		json.append(",pmtInterest=");
		json.append(pmtInterest);
		json.append(",adjPaymentDate=");
		json.append(adjPaymentDate);
		json.append("]");
		return json.toString();
	}
}