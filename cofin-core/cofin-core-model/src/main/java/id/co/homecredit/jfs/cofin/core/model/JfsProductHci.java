package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity class for {@code JFS_PRODUCT_HCI}.
 *
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=CoreTableName.JFS_PRODUCT_HCI)
public class JfsProductHci extends BaseEntityIdOnly<String>{
	private String codeProduct;
	private String nameProduct;
	private Date dtimeInserted;
	@Column(name=CoreColumnName.CODE_PRODUCT)
	public String getCodeProduct(){
		return codeProduct;
	}
	public void setCodeProduct(String codeProduct){
		this.codeProduct=codeProduct;
	}
	@Column(name=CoreColumnName.NAME_PRODUCT)
	public String getNameProduct(){
		return nameProduct;
	}
	public void setNameProduct(String nameProduct){
		this.nameProduct=nameProduct;
	}
	@Column(name=CoreColumnName.DTIME_INSERTED)
	public Date getDtimeInserted(){
		return dtimeInserted;
	}
	public void setDtimeInserted(Date dtimeInserted){
		this.dtimeInserted=dtimeInserted;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsProductHci [codeProduct=");
		json.append(codeProduct);
		json.append(",nameProduct=");
		json.append(nameProduct);
		json.append(",dtimeInserted=");
		json.append(dtimeInserted);
		json.append("id=");
		json.append(id);
		json.append("]");
		return json.toString();
	}
}
