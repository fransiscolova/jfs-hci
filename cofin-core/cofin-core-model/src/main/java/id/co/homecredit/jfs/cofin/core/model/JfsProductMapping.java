package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@code JFS_PRODUCT_MAPPING}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_PRODUCT_MAPPING)
public class JfsProductMapping extends BaseEntityIdOnly<String>{
	private String codeProduct;
	private String bankProductCode;
	private Date validFrom;
	private Date validTo;
	private Long idAgreement;
	private Date dtimeInserted;
	private YesNoEnum isDelete;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	@Column(name=CoreColumnName.CODE_PRODUCT)
	public String getCodeProduct(){
		return codeProduct;
	}
	public void setCodeProduct(String codeProduct){
		this.codeProduct=codeProduct;
	}
	@Column(name=CoreColumnName.BANK_PRODUCT_CODE)
	public String getBankProductCode(){
		return bankProductCode;
	}
	public void setBankProductCode(String bankProductCode){
		this.bankProductCode=bankProductCode;
	}
	@Column(name=CoreColumnName.VALID_FROM)
	public Date getValidFrom(){
		return validFrom;
	}
	public void setValidFrom(Date validFrom){
		this.validFrom=validFrom;
	}
	@Column(name=CoreColumnName.VALID_TO)
	public Date getValidTo(){
		return validTo;
	}
	public void setValidTo(Date validTo){
		this.validTo=validTo;
	}
	@Column(name=CoreColumnName.ID_AGREEMENT)
	public Long getIdAgreement(){
		return idAgreement;
	}
	public void setIdAgreement(Long idAgreement){
		this.idAgreement=idAgreement;
	}
	@Column(name=CoreColumnName.DTIME_INSERTED)
	public Date getDtimeInserted(){
		return dtimeInserted;
	}
	public void setDtimeInserted(Date dtimeInserted){
		this.dtimeInserted=dtimeInserted;
	}
	@Column(name=CoreColumnName.IS_DELETE)
	public YesNoEnum getIsDelete(){
		return isDelete;
	}
	public void setIsDelete(YesNoEnum isDelete){
		this.isDelete=isDelete;
	}
	@Column(name=CoreColumnName.CREATED_BY)
	public String getCreatedBy(){
		return createdBy;
	}
	public void setCreatedBy(String createdBy){
		this.createdBy=createdBy;
	}
	@Column(name=CoreColumnName.CREATED_DATE)
	public Date getCreatedDate(){
		return createdDate;
	}
	public void setCreatedDate(Date createdDate){
		this.createdDate=createdDate;
	}
	@Column(name=CoreColumnName.UPDATED_BY)
	public String getUpdatedBy(){
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy){
		this.updatedBy=updatedBy;
	}
	@Column(name=CoreColumnName.UPDATED_DATE)
	public Date getUpdatedDate(){
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate=updatedDate;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder();
		json.append("JfsProductMapping [id=");
		json.append(id);
		json.append(",codeProduct=");
		json.append(codeProduct);
		json.append(",bankProductCode=");
		json.append(bankProductCode);
		json.append(",validFrom=");
		json.append(validFrom);
		json.append(",validTo=");
		json.append(validTo);
		json.append(",idAgreement=");
		json.append(idAgreement);
		json.append(",dtimeInserted=");
		json.append(dtimeInserted);
		json.append(",isDelete=");
		json.append(isDelete);
		json.append(",createdBy=");
		json.append(createdBy);
		json.append(",createdDate=");
		json.append(createdDate);
		json.append(",updatedBy=");
		json.append(updatedBy);
		json.append(",updatedDate=");
		json.append(updatedDate);
		json.append("]");
		return json.toString();
	}
}
