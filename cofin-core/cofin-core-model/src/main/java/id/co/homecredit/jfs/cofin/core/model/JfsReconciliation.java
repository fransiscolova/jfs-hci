package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@code JFS_RECONCILIATION}
 * 
 * @author denny.afrizal01
 *
 */

@Entity
@Table(name=CoreTableName.JFS_RECONCILIATION)
public class JfsReconciliation{
	private String reconId;
	private Date reconDate;
	private String textContractNumber;
	private Double outstandingPrincipal;
	private Double dpd;
	private Integer instNumber;
	@Id
	@Column(name=CoreColumnName.RECON_ID)
	public String getReconId(){
		return reconId;
	}
	public void setReconId(String reconId){
		this.reconId=reconId;
	}
	@Column(name=CoreColumnName.RECON_DATE)
	public Date getReconDate(){
		return reconDate;
	}
	public void setReconDate(Date reconDate){
		this.reconDate=reconDate;
	}
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.OUTSTANDING_PRINCIPAL)
	public Double getOutstandingPrincipal(){
		return outstandingPrincipal;
	}
	public void setOutstandingPrincipal(Double outstandingPrincipal){
		this.outstandingPrincipal=outstandingPrincipal;
	}
	@Column(name=CoreColumnName.DPD)
	public Double getDpd(){
		return dpd;
	}
	public void setDpd(Double dpd){
		this.dpd=dpd;
	}
	@Column(name=CoreColumnName.INST_NUMBER)
	public Integer getInstNumber(){
		return instNumber;
	}
	public void setInstNumber(Integer instNumber){
		this.instNumber=instNumber;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsReconciliation [reconId=");
		json.append(reconId);
		json.append(",reconDate=");
		json.append(reconDate);
		json.append(",textContractNumber=");
		json.append(textContractNumber);
		json.append(",outstandingPrincipal=");
		json.append(outstandingPrincipal);
		json.append(",dpd=");
		json.append(dpd);
		json.append(",instNumber=");
		json.append(instNumber);
		json.append("]");
		return json.toString();
	}
}
