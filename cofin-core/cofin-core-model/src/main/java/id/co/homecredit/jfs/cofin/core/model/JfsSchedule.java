package id.co.homecredit.jfs.cofin.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code JFS_SCHEDULE}.
 *
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_SCHEDULE)
public class JfsSchedule{
    private String textContractNumber;
    private Date dueDate;
    private Integer partIndex;
    private Double principal;
    private Double interest;
    private Double fee;
    private Double outstandingPrincipal;
    private Double prevBalance;
    private Double balance;
    private Double penalty;
    private Long ver;
    private String isActive;
    @Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
    public String getTextContractNumber(){
        return textContractNumber;
    }
    public void setTextContractNumber(String textContractNumber){
        this.textContractNumber=textContractNumber;
    }
    @Column(name=CoreColumnName.DUE_DATE)
    public Date getDueDate(){
        return dueDate;
    }
    public void setDueDate(Date dueDate){
        this.dueDate=dueDate;
    }
    @Id
    @Column(name=CoreColumnName.PART_INDEX)
    public Integer getPartIndex(){
        return partIndex;
    }
    public void setPartIndex(Integer partIndex){
        this.partIndex=partIndex;
    }
    @Column(name=CoreColumnName.PRINCIPAL)
    public Double getPrincipal(){
        return principal;
    }
    public void setPrincipal(Double principal){
        this.principal=principal;
    }
    @Column(name=CoreColumnName.INTEREST)
    public Double getInterest(){
        return interest;
    }
    public void setInterest(Double interest){
        this.interest=interest;
    }
    @Column(name=CoreColumnName.FEE)
    public Double getFee(){
        return fee;
    }
    public void setFee(Double fee){
        this.fee=fee;
    }
    @Column(name=CoreColumnName.OUTSTANDING_PRINCIPAL)
    public Double getOutstandingPrincipal(){
        return outstandingPrincipal;
    }
    public void setOutstandingPrincipal(Double outstandingPrincipal){
        this.outstandingPrincipal=outstandingPrincipal;
    }
    @Column(name=CoreColumnName.PREV_BALANCE)
    public Double getPrevBalance(){
        return prevBalance;
    }
    public void setPrevBalance(Double prevBalance){
        this.prevBalance=prevBalance;
    }
    @Column(name=CoreColumnName.BALANCE)
    public Double getBalance(){
        return balance;
    }
    public void setBalance(Double balance){
        this.balance=balance;
    }
    @Column(name=CoreColumnName.PENALTY)
    public Double getPenalty(){
        return penalty;
    }
    public void setPenalty(Double penalty){
        this.penalty=penalty;
    }
    @Column(name=CoreColumnName.VER)
    public Long getVer(){
        return ver;
    }
    public void setVer(Long ver){
        this.ver=ver;
    }
    @Column(name=CoreColumnName.IS_ACTIVE)
    public String getIsActive(){
        return isActive;
    }
    public void setIsActive(String isActive){
        this.isActive=isActive;
    }
    @Override
    public String toString(){
        StringBuilder json = new StringBuilder(1000);
        json.append("JfsSchedule [textContractNumber=");
        json.append(textContractNumber);
        json.append(",dueDate=");
        json.append(dueDate);
        json.append(",partIndex=");
        json.append(partIndex);
        json.append(",principal=");
        json.append(principal);
        json.append(",interest=");
        json.append(interest);
        json.append(",fee=");
        json.append(fee);
        json.append(",outstandingPrincipal=");
        json.append(outstandingPrincipal);
        json.append(",prevBalance=");
        json.append(prevBalance);
        json.append(",balance=");
        json.append(balance);
        json.append(",penalty=");
        json.append(penalty);
        json.append(",ver=");
        json.append(ver);
        json.append(",isActive=");
        json.append(isActive);
        json.append("]");
        return json.toString();
    }
}
