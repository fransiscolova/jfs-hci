package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@link JFS_SCHEDULE_BSL}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.JFS_SCHEDULE_BSL)
public class JfsScheduleBsl{
	private String textContractNumber;
	private String dueDate;
	private String partIndex;
	private String principal;
	private String interest;
	private String fee;
	private String outstandingPrincipal;
	private String prevBalance;
	private String balance;
	private String penalty;
	private String ver;
	private String isActive;
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.DUE_DATE)
	public String getDueDate(){
		return dueDate;
	}
	public void setDueDate(String dueDate){
		this.dueDate=dueDate;
	}
	@Id
	@Column(name=CoreColumnName.PART_INDEX)
	public String getPartIndex(){
		return partIndex;
	}
	public void setPartIndex(String partIndex){
		this.partIndex=partIndex;
	}
	@Column(name=CoreColumnName.PRINCIPAL)
	public String getPrincipal(){
		return principal;
	}
	public void setPrincipal(String principal){
		this.principal=principal;
	}
	@Column(name=CoreColumnName.INTEREST)
	public String getInterest(){
		return interest;
	}
	public void setInterest(String interest){
		this.interest=interest;
	}
	@Column(name=CoreColumnName.FEE)
	public String getFee(){
		return fee;
	}
	public void setFee(String fee){
		this.fee=fee;
	}
	@Column(name=CoreColumnName.OUTSTANDING_PRINCIPAL)
	public String getOutstandingPrincipal(){
		return outstandingPrincipal;
	}
	public void setOutstandingPrincipal(String outstandingPrincipal){
		this.outstandingPrincipal=outstandingPrincipal;
	}
	@Column(name=CoreColumnName.PREV_BALANCE)
	public String getPrevBalance(){
		return prevBalance;
	}
	public void setPrevBalance(String prevBalance){
		this.prevBalance=prevBalance;
	}
	@Column(name=CoreColumnName.BALANCE)
	public String getBalance(){
		return balance;
	}
	public void setBalance(String balance){
		this.balance=balance;
	}
	@Column(name=CoreColumnName.PENALTY)
	public String getPenalty(){
		return penalty;
	}
	public void setPenalty(String penalty){
		this.penalty=penalty;
	}
	@Column(name=CoreColumnName.VER)
	public String getVer(){
		return ver;
	}
	public void setVer(String ver){
		this.ver=ver;
	}
	@Column(name=CoreColumnName.IS_ACTIVE)
	public String getIsActive(){
		return isActive;
	}
	public void setIsActive(String isActive){
		this.isActive=isActive;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("JfsScheduleBsl [textContractNumber=");
		json.append(textContractNumber);
		json.append(",dueDate=");
		json.append(dueDate);
		json.append(",partIndex=");
		json.append(partIndex);
		json.append(",principal=");
		json.append(principal);
		json.append(",interest=");
		json.append(interest);
		json.append(",fee=");
		json.append(fee);
		json.append(",outstandingPrincipal=");
		json.append(outstandingPrincipal);
		json.append(",prevBalance=");
		json.append(prevBalance);
		json.append(",balance=");
		json.append(balance);
		json.append(",penalty=");
		json.append(penalty);
		json.append(",ver=");
		json.append(ver);
		json.append(",isActive=");
		json.append(isActive);
		json.append("]");
		return json.toString();
	}
}
