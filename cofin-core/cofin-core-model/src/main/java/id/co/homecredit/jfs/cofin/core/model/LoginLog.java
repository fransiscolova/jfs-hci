package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdCreate;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.model.enumeration.LoginEnum;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_LOGIN_LOG}.
 *
 * @author muhammad.muflihun
 *
 */
@AuditTransient
@Entity
@Table(name = CoreTableName.COFIN_LOGIN_LOG)
public class LoginLog extends IdCreate<String> {
    private LoginEnum action;

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.ACTION, nullable = false)
    public LoginEnum getAction() {
        return action;
    }

    public void setAction(LoginEnum action) {
        this.action = action;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("LoginLog [actionDate=");
        builder.append(action);
        builder.append(", id=");
        builder.append(id);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
