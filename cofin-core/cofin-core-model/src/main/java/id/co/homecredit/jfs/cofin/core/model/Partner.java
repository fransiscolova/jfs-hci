package id.co.homecredit.jfs.cofin.core.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_PARTNER}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_PARTNER)
public class Partner extends IdEntity {
    private String name;
    private String code;
    private String address;
    private String contactName;
    private String contactPhone;
    private String contactEmail;
    private Integer priority;
    private YesNoEnum isEligibilityChecked = YesNoEnum.Y;
    @AuditTransient
    private List<Agreement> agreements;
    @AuditTransient
    private List<ComCategoryMapping> commodityMappings;

    @Column(name = CoreColumnName.ADDRESS)
    public String getAddress() {
        return address;
    }

    @OneToMany(mappedBy = "partner")
    public List<Agreement> getAgreements() {
        return agreements;
    }

    @Column(name = CoreColumnName.CODE, nullable = false)
    public String getCode() {
        return code;
    }

    @OneToMany(mappedBy = "partner")
    public List<ComCategoryMapping> getCommodityMappings() {
        return commodityMappings;
    }

    @Column(name = CoreColumnName.CONTACT_EMAIL)
    public String getContactEmail() {
        return contactEmail;
    }

    @Column(name = CoreColumnName.CONTACT_NAME)
    public String getContactName() {
        return contactName;
    }

    @Column(name = CoreColumnName.CONTACT_PHONE)
    public String getContactPhone() {
        return contactPhone;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.IS_ELIGIBILITY_CHECKED, length = 1)
    public YesNoEnum getIsEligibilityChecked() {
        return isEligibilityChecked;
    }

    @Column(name = CoreColumnName.NAME)
    public String getName() {
        return name;
    }

    @Column(name = CoreColumnName.PRIORITY)
    public Integer getPriority() {
        return priority;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAgreements(List<Agreement> agreements) {
        this.agreements = agreements;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCommodityMappings(List<ComCategoryMapping> commodityMappings) {
        this.commodityMappings = commodityMappings;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public void setIsEligibilityChecked(YesNoEnum isEligibilityChecked) {
        this.isEligibilityChecked = isEligibilityChecked;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("Partner [name=");
        builder.append(name);
        builder.append(", code=");
        builder.append(code);
        builder.append(", address=");
        builder.append(address);
        builder.append(", contactName=");
        builder.append(contactName);
        builder.append(", contactPhone=");
        builder.append(contactPhone);
        builder.append(", contactEmail=");
        builder.append(contactEmail);
        builder.append(", priority=");
        builder.append(priority);
        builder.append(", isEligibilityChecked=");
        builder.append(isEligibilityChecked);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
