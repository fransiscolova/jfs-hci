package id.co.homecredit.jfs.cofin.core.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PaymentTypeEnum;

/**
 * Entity class for {@code COFIN_PAYMENT_INT}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_PAYMENT_INT, indexes = {
        @Index(columnList = CoreColumnName.CONTRACT_NUMBER, name = "IDX_PAY_INT_CONTRACT_NUMBER"),
        @Index(columnList = CoreColumnName.INCOMING_PAYMENT_ID, name = "IDX_PAY_INT_INC_PAYMENT_ID") })
public class PaymentInt extends IdEntity {
    private String incomingPaymentId;
    private String contractNumber;
    private Date paymentDate;
    private Date exportDate;
    private BigDecimal paymentTotal;
    private BigDecimal payment;
    private BigDecimal paymentInstalment;
    private BigDecimal paymentPrincipal;
    private BigDecimal paymentInterest;
    private BigDecimal paymentFee;
    private BigDecimal paymentPenalty;
    private BigDecimal paymentOver;
    private BigDecimal paymentOther;
    private PaymentTypeEnum paymentType;
    private String parentId;
    private String status;
    private String reason;
    private Date bankProcessDate;

    @Column(name = CoreColumnName.BANK_PROCESS_DATE)
    public Date getBankProcessDate() {
        return bankProcessDate;
    }

    @Column(name = CoreColumnName.CONTRACT_NUMBER, nullable = false)
    public String getContractNumber() {
        return contractNumber;
    }

    @Column(name = CoreColumnName.EXPORT_DATE, nullable = false)
    public Date getExportDate() {
        return exportDate;
    }

    @Column(name = CoreColumnName.INCOMING_PAYMENT_ID, nullable = false)
    public String getIncomingPaymentId() {
        return incomingPaymentId;
    }

    @Column(name = CoreColumnName.PARENT_ID)
    public String getParentId() {
        return parentId;
    }

    @Column(name = CoreColumnName.PAYMENT, nullable = false, precision = 2)
    public BigDecimal getPayment() {
        return payment;
    }

    @Column(name = CoreColumnName.PAYMENT_DATE, nullable = false)
    public Date getPaymentDate() {
        return paymentDate;
    }

    @Column(name = CoreColumnName.PAYMENT_FEE, nullable = false, precision = 2)
    public BigDecimal getPaymentFee() {
        return paymentFee;
    }

    @Column(name = CoreColumnName.PAYMENT_INSTALMENT, nullable = false, precision = 2)
    public BigDecimal getPaymentInstalment() {
        return paymentInstalment;
    }

    @Column(name = CoreColumnName.PAYMENT_INTEREST, precision = 2)
    public BigDecimal getPaymentInterest() {
        return paymentInterest;
    }

    @Column(name = CoreColumnName.PAYMENT_OTHER, precision = 2)
    public BigDecimal getPaymentOther() {
        return paymentOther;
    }

    @Column(name = CoreColumnName.PAYMENT_OVER, precision = 2)
    public BigDecimal getPaymentOver() {
        return paymentOver;
    }

    @Column(name = CoreColumnName.PAYMENT_PENALTY, nullable = false, precision = 2)
    public BigDecimal getPaymentPenalty() {
        return paymentPenalty;
    }

    @Column(name = CoreColumnName.PAYMENT_PRINCIPAL, precision = 2)
    public BigDecimal getPaymentPrincipal() {
        return paymentPrincipal;
    }

    @Column(name = CoreColumnName.PAYMENT_TOTAL, nullable = false, precision = 2)
    public BigDecimal getPaymentTotal() {
        return paymentTotal;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.PAYMENT_TYPE)
    public PaymentTypeEnum getPaymentType() {
        return paymentType;
    }

    @Column(name = CoreColumnName.REASON)
    public String getReason() {
        return reason;
    }

    @Column(name = CoreColumnName.STATUS)
    public String getStatus() {
        return status;
    }

    public void setBankProcessDate(Date bankProcessDate) {
        this.bankProcessDate = bankProcessDate;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public void setExportDate(Date exportDate) {
        this.exportDate = exportDate;
    }

    public void setIncomingPaymentId(String incomingPaymentId) {
        this.incomingPaymentId = incomingPaymentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setPaymentFee(BigDecimal paymentFee) {
        this.paymentFee = paymentFee;
    }

    public void setPaymentInstalment(BigDecimal paymentInstalment) {
        this.paymentInstalment = paymentInstalment;
    }

    public void setPaymentInterest(BigDecimal paymentInterest) {
        this.paymentInterest = paymentInterest;
    }

    public void setPaymentOther(BigDecimal paymentOther) {
        this.paymentOther = paymentOther;
    }

    public void setPaymentOver(BigDecimal paymentOver) {
        this.paymentOver = paymentOver;
    }

    public void setPaymentPenalty(BigDecimal paymentPenalty) {
        this.paymentPenalty = paymentPenalty;
    }

    public void setPaymentPrincipal(BigDecimal paymentPrincipal) {
        this.paymentPrincipal = paymentPrincipal;
    }

    public void setPaymentTotal(BigDecimal paymentTotal) {
        this.paymentTotal = paymentTotal;
    }

    public void setPaymentType(PaymentTypeEnum paymentType) {
        this.paymentType = paymentType;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("PaymentInt [incomingPaymentId=");
        builder.append(incomingPaymentId);
        builder.append(", contractNumber=");
        builder.append(contractNumber);
        builder.append(", paymentDate=");
        builder.append(paymentDate);
        builder.append(", exportDate=");
        builder.append(exportDate);
        builder.append(", paymentTotal=");
        builder.append(paymentTotal);
        builder.append(", payment=");
        builder.append(payment);
        builder.append(", paymentInstalment=");
        builder.append(paymentInstalment);
        builder.append(", paymentPrincipal=");
        builder.append(paymentPrincipal);
        builder.append(", paymentInterest=");
        builder.append(paymentInterest);
        builder.append(", paymentFee=");
        builder.append(paymentFee);
        builder.append(", paymentPenalty=");
        builder.append(paymentPenalty);
        builder.append(", paymentOver=");
        builder.append(paymentOver);
        builder.append(", paymentOther=");
        builder.append(paymentOther);
        builder.append(", paymentType=");
        builder.append(paymentType);
        builder.append(", parentId=");
        builder.append(parentId);
        builder.append(", status=");
        builder.append(status);
        builder.append(", reason=");
        builder.append(reason);
        builder.append(", bankProcessDate=");
        builder.append(bankProcessDate);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
