package id.co.homecredit.jfs.cofin.core.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.BaseEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_PRODUCT}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_PRODUCT)
public class Product extends BaseEntity<String> {
    private String code;
    private String name;
    @AuditTransient
    private List<ProductMapping> productMappings;

    @Id
    @Column(name = CoreColumnName.CODE, nullable = false)
    public String getCode() {
        return code;
    }

    @Column(name = CoreColumnName.NAME)
    public String getName() {
        return name;
    }

    @OneToMany(mappedBy = "product")
    public List<ProductMapping> getProductMappings() {
        return productMappings;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProductMappings(List<ProductMapping> productMappings) {
        this.productMappings = productMappings;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("Product [code=");
        builder.append(code);
        builder.append(", name=");
        builder.append(name);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
