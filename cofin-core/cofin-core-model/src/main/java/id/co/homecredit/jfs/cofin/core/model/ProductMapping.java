package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdValidEntity;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_PRODUCT_MAPPING}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_PRODUCT_MAPPING)
public class ProductMapping extends IdValidEntity {
    private Product product;
    private Agreement agreement;

    @ManyToOne
    @JoinColumn(name = CoreColumnName.AGREEMENT_ID, referencedColumnName = ColumnName.ID, nullable = false)
    public Agreement getAgreement() {
        return agreement;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.PRODUCT_CODE, referencedColumnName = CoreColumnName.CODE, nullable = false)
    public Product getProduct() {
        return product;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("ProductMapping [product=");
        builder.append(product);
        builder.append(", agreement=");
        builder.append(agreement);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
