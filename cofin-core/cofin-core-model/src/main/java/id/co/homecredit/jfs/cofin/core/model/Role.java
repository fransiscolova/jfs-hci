package id.co.homecredit.jfs.cofin.core.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.BaseEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Entity class for {@code COFIN_ROLE}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_ROLE)
public class Role extends BaseEntity<String> {
    private RoleEnum role;
    private String description;
    @AuditTransient
    private List<RoleMapping> roleMappings;

    @Column(name = CoreColumnName.DESCRIPTION)
    public String getDescription() {
        return description;
    }

    @Id
    @Enumerated(EnumType.STRING)
    @Column(name = CoreColumnName.ROLE, nullable = false, unique = true)
    public RoleEnum getRole() {
        return role;
    }

    @OneToMany(mappedBy = "role")
    public List<RoleMapping> getRoleMappings() {
        return roleMappings;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public void setRoleMappings(List<RoleMapping> roleMappings) {
        this.roleMappings = roleMappings;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("Role [role=");
        builder.append(role);
        builder.append(", description=");
        builder.append(description);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
