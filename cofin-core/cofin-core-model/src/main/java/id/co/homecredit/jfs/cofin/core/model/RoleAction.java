package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdValidEntity;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_ROLE_ACTION}.
 *
 * @author fransisco.situmorang
 */
@Entity
@Table(name = CoreTableName.COFIN_ROLE_ACTION)
public class RoleAction extends IdValidEntity {
    private Role role;
    private Action action;

    @ManyToOne
    @JoinColumn(name = CoreColumnName.ACTION, referencedColumnName = CoreColumnName.ACTION, nullable = false)
    public Action getAction() {
        return action;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.ROLE_CODE, referencedColumnName = CoreColumnName.ROLE, nullable = false)
    public Role getRole() {
        return role;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Print each field value for this entity.
   
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("RoleAction [role=");
        builder.append(role.toString());
        builder.append(", action=");
        builder.append(action.toString());
        builder.append(", validFrom=");
        builder.append(validFrom);
        builder.append(", validTo=");
        builder.append(validTo);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }
  */
}
