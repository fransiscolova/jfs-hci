package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdValidEntity;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_ROLE_MAPPING}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_ROLE_MAPPING)
public class RoleMapping extends IdValidEntity {
    private Role role;
    private User user;
    private User adminUser;

    @ManyToOne
    @JoinColumn(name = CoreColumnName.ADMIN_USER, referencedColumnName = CoreColumnName.USERNAME, updatable = false)
    public User getAdminUser() {
        return adminUser;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.ROLE_CODE, referencedColumnName = CoreColumnName.ROLE, nullable = false)
    public Role getRole() {
        return role;
    }

    @ManyToOne
    @JoinColumn(name = CoreColumnName.USERNAME, referencedColumnName = CoreColumnName.USERNAME, nullable = false)
    public User getUser() {
        return user;
    }

    public void setAdminUser(User adminUser) {
        this.adminUser = adminUser;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("RoleMapping [role=");
        builder.append(role);
        builder.append(", user=");
        builder.append(user);
        builder.append(", superUser=");
        builder.append(adminUser);
        builder.append(", validFrom=");
        builder.append(validFrom);
        builder.append(", validTo=");
        builder.append(validTo);
        builder.append(", id=");
        builder.append(id);
        builder.append(", version=");
        builder.append(version);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
