package id.co.homecredit.jfs.cofin.core.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_SCHEDULE}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_SCHEDULE, indexes = {
        @Index(columnList = CoreColumnName.CONTRACT_NUMBER, name = "IDX_SCH_CONTRACT_NUMBER") })
public class Schedule extends IdEntity {
    private String contractNumber;
    private Date dueDate;
    private Integer terms;
    private BigDecimal principal;
    private BigDecimal interest;
    private BigDecimal fee;
    private BigDecimal outstandingPrincipal;
    private BigDecimal previousBalance;
    private BigDecimal balance;
    private BigDecimal penalty;

    @Column(name = CoreColumnName.BALANCE, precision = 2)
    public BigDecimal getBalance() {
        return balance;
    }

    @Column(name = CoreColumnName.CONTRACT_NUMBER, nullable = false)
    public String getContractNumber() {
        return contractNumber;
    }

    @Column(name = CoreColumnName.DUE_DATE, nullable = false)
    public Date getDueDate() {
        return dueDate;
    }

    @Column(name = CoreColumnName.FEE, precision = 2)
    public BigDecimal getFee() {
        return fee;
    }

    @Column(name = CoreColumnName.INTEREST, nullable = false, precision = 2)
    public BigDecimal getInterest() {
        return interest;
    }

    @Column(name = CoreColumnName.OUTSTANDING_PRINCIPAL, nullable = false, precision = 2)
    public BigDecimal getOutstandingPrincipal() {
        return outstandingPrincipal;
    }

    @Column(name = CoreColumnName.PENALTY, precision = 2)
    public BigDecimal getPenalty() {
        return penalty;
    }

    @Column(name = CoreColumnName.PREVIOUS_BALANCE, precision = 2)
    public BigDecimal getPreviousBalance() {
        return previousBalance;
    }

    @Column(name = CoreColumnName.PRINCIPAL, nullable = false, precision = 2)
    public BigDecimal getPrincipal() {
        return principal;
    }

    @Column(name = CoreColumnName.TERMS, nullable = false)
    public Integer getTerms() {
        return terms;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public void setOutstandingPrincipal(BigDecimal outstandingPrincipal) {
        this.outstandingPrincipal = outstandingPrincipal;
    }

    public void setPenalty(BigDecimal penalty) {
        this.penalty = penalty;
    }

    public void setPreviousBalance(BigDecimal previousBalance) {
        this.previousBalance = previousBalance;
    }

    public void setPrincipal(BigDecimal principal) {
        this.principal = principal;
    }

    public void setTerms(Integer terms) {
        this.terms = terms;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("Schedule [contractNumber=");
        builder.append(contractNumber);
        builder.append(", dueDate=");
        builder.append(dueDate);
        builder.append(", terms=");
        builder.append(terms);
        builder.append(", principal=");
        builder.append(principal);
        builder.append(", interest=");
        builder.append(interest);
        builder.append(", fee=");
        builder.append(fee);
        builder.append(", outstandingPrincipal=");
        builder.append(outstandingPrincipal);
        builder.append(", previousBalance=");
        builder.append(previousBalance);
        builder.append(", balance=");
        builder.append(balance);
        builder.append(", penalty=");
        builder.append(penalty);
        builder.append(", id=");
        builder.append(id);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
