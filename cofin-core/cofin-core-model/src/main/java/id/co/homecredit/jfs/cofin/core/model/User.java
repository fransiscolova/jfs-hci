package id.co.homecredit.jfs.cofin.core.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.BaseEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;

/**
 * Entity class for {@code COFIN_USER}.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_USER)
public class User extends BaseEntity<String> {
    private String username;
    private String firstName;
    private String lastName;
    @AuditTransient
    private List<RoleMapping> roleMappings;

    @Column(name = CoreColumnName.FIRST_NAME)
    public String getFirstName() {
        return firstName;
    }

    @Column(name = CoreColumnName.LAST_NAME)
    public String getLastName() {
        return lastName;
    }

    @OneToMany(mappedBy = "user")
    public List<RoleMapping> getRoleMappings() {
        return roleMappings;
    }

    @Id
    @Column(name = CoreColumnName.USERNAME)
    public String getUsername() {
        return username;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setRoleMappings(List<RoleMapping> roleMappings) {
        this.roleMappings = roleMappings;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Print each field value for this entity.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("User [username=");
        builder.append(username);
        builder.append(", firstName=");
        builder.append(firstName);
        builder.append(", lastName=");
        builder.append(lastName);
        builder.append(", deleted=");
        builder.append(deleted);
        builder.append(", version=");
        builder.append(version);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", updatedDate=");
        builder.append(updatedDate);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append("]");
        return builder.toString();
    }

}
