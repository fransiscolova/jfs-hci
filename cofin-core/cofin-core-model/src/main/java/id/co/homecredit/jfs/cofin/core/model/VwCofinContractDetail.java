package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For {@link VW_COFIN_CONTRACT_DETAIL}
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.VW_COFIN_CONTRACT_DETAIL)
public class VwCofinContractDetail{
	private String textContractNumber;
	private String jfsContractStatus;
	private String status;
	private String contractProcessingType;
	private String registrationStatus;
	private String contractType;
	private String loanPurpose;
	private String productId;
	private String productCode;
	private String productName;
	private String productDescription;
	private String currencyCode;
	private String saleDescription;
	private String productProfileId;
	private String productProfileCode;
	private String productProfileName;
	private String applicationForm;
	private String initTransactionType;
	private String salesroomId;
	private String businessAreaCode;
	private String salesroomName;
	private String salesroomBusinessModelCode;
	private String salesroomCategoryCode;
	private String paperless;
	private String commodityTypeId;
	private String commodityTypeCode;
	private String commodityCategoryCode;
	private String bankCommodityCategoryCode;
	private String commodityCategoryDescription;
	private String hciCommodityTypeCode;
	private String bankCommodityTypeCode;
	private String commodityTypeDesc;
	private String deliveryTypeCode;
	private String modelNumber;
	private String producer;
	private String serialNumber;
	private String engineNumber;
	private String goodsPrice;
	private String goodsPriceCurr;
	private String untaxedPriceAmount;
	private String untaxedPriceCurr;
	private String serviceTypeCode;
	private String serviceCode;
	private String serviceName;
	private String serviceDescription;
	private String tariffCode;
	private String tariffName;
	private String tariffTypeCode;
	private String tariffCurrencyCode;
	@Id
	@Column(name=CoreColumnName.TEXT_CONTRACT_NUMBER)
	public String getTextContractNumber(){
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber){
		this.textContractNumber=textContractNumber;
	}
	@Column(name=CoreColumnName.JFS_CONTRACT_STATUS)
	public String getJfsContractStatus(){
		return jfsContractStatus;
	}
	public void setJfsContractStatus(String jfsContractStatus){
		this.jfsContractStatus=jfsContractStatus;
	}
	@Column(name=CoreColumnName.STATUS)
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	@Column(name=CoreColumnName.CONTRACT_PROCESSING_TYPE)
	public String getContractProcessingType(){
		return contractProcessingType;
	}
	public void setContractProcessingType(String contractProcessingType){
		this.contractProcessingType=contractProcessingType;
	}
	@Column(name=CoreColumnName.REGISTRATION_STATUS)
	public String getRegistrationStatus(){
		return registrationStatus;
	}
	public void setRegistrationStatus(String registrationStatus){
		this.registrationStatus=registrationStatus;
	}
	@Column(name=CoreColumnName.CONTRACT_TYPE)
	public String getContractType(){
		return contractType;
	}
	public void setContractType(String contractType){
		this.contractType=contractType;
	}
	@Column(name=CoreColumnName.LOAN_PURPOSE)
	public String getLoanPurpose(){
		return loanPurpose;
	}
	public void setLoanPurpose(String loanPurpose){
		this.loanPurpose=loanPurpose;
	}
	@Column(name=CoreColumnName.PRODUCT_ID)
	public String getProductId(){
		return productId;
	}
	public void setProductId(String productId){
		this.productId=productId;
	}
	@Column(name=CoreColumnName.PRODUCT_CODE)
	public String getProductCode(){
		return productCode;
	}
	public void setProductCode(String productCode){
		this.productCode=productCode;
	}
	@Column(name=CoreColumnName.PRODUCT_NAME)
	public String getProductName(){
		return productName;
	}
	public void setProductName(String productName){
		this.productName=productName;
	}
	@Column(name=CoreColumnName.PRODUCT_DESCRIPTION)
	public String getProductDescription(){
		return productDescription;
	}
	public void setProductDescription(String productDescription){
		this.productDescription=productDescription;
	}
	@Column(name=CoreColumnName.CURRENCY_CODE)
	public String getCurrencyCode(){
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode){
		this.currencyCode=currencyCode;
	}
	@Column(name=CoreColumnName.SALE_DESCRIPTION)
	public String getSaleDescription(){
		return saleDescription;
	}
	public void setSaleDescription(String saleDescription){
		this.saleDescription=saleDescription;
	}
	@Column(name=CoreColumnName.PRODUCT_PROFILE_ID)
	public String getProductProfileId(){
		return productProfileId;
	}
	public void setProductProfileId(String productProfileId){
		this.productProfileId=productProfileId;
	}
	@Column(name=CoreColumnName.PRODUCT_PROFILE_CODE)
	public String getProductProfileCode(){
		return productProfileCode;
	}
	public void setProductProfileCode(String productProfileCode){
		this.productProfileCode=productProfileCode;
	}
	@Column(name=CoreColumnName.PRODUCT_PROFILE_NAME)
	public String getProductProfileName(){
		return productProfileName;
	}
	public void setProductProfileName(String productProfileName){
		this.productProfileName=productProfileName;
	}
	@Column(name=CoreColumnName.APPLICATION_FORM)
	public String getApplicationForm(){
		return applicationForm;
	}
	public void setApplicationForm(String applicationForm){
		this.applicationForm=applicationForm;
	}
	@Column(name=CoreColumnName.INIT_TRANSACTION_TYPE)
	public String getInitTransactionType(){
		return initTransactionType;
	}
	public void setInitTransactionType(String initTransactionType){
		this.initTransactionType=initTransactionType;
	}
	@Column(name=CoreColumnName.SALESROOM_ID)
	public String getSalesroomId(){
		return salesroomId;
	}
	public void setSalesroomId(String salesroomId){
		this.salesroomId=salesroomId;
	}
	@Column(name=CoreColumnName.BUSINESS_AREA_CODE)
	public String getBusinessAreaCode(){
		return businessAreaCode;
	}
	public void setBusinessAreaCode(String businessAreaCode){
		this.businessAreaCode=businessAreaCode;
	}
	@Column(name=CoreColumnName.SALESROOM_NAME)
	public String getSalesroomName(){
		return salesroomName;
	}
	public void setSalesroomName(String salesroomName){
		this.salesroomName=salesroomName;
	}
	@Column(name=CoreColumnName.SALESROOM_BUSINESS_MODEL_CODE)
	public String getSalesroomBusinessModelCode(){
		return salesroomBusinessModelCode;
	}
	public void setSalesroomBusinessModelCode(String salesroomBusinessModelCode){
		this.salesroomBusinessModelCode=salesroomBusinessModelCode;
	}
	@Column(name=CoreColumnName.SALESROOM_CATEGORY_CODE)
	public String getSalesroomCategoryCode(){
		return salesroomCategoryCode;
	}
	public void setSalesroomCategoryCode(String salesroomCategoryCode){
		this.salesroomCategoryCode=salesroomCategoryCode;
	}
	@Column(name=CoreColumnName.PAPERLESS)
	public String getPaperless(){
		return paperless;
	}
	public void setPaperless(String paperless){
		this.paperless=paperless;
	}
	@Column(name=CoreColumnName.COMMODITY_TYPE_ID)
	public String getCommodityTypeId(){
		return commodityTypeId;
	}
	public void setCommodityTypeId(String commodityTypeId){
		this.commodityTypeId=commodityTypeId;
	}
	@Column(name=CoreColumnName.COMMODITY_TYPE_CODE)
	public String getCommodityTypeCode(){
		return commodityTypeCode;
	}
	public void setCommodityTypeCode(String commodityTypeCode){
		this.commodityTypeCode=commodityTypeCode;
	}
	@Column(name=CoreColumnName.COMMODITY_CATEGORY_CODE)
	public String getCommodityCategoryCode(){
		return commodityCategoryCode;
	}
	public void setCommodityCategoryCode(String commodityCategoryCode){
		this.commodityCategoryCode=commodityCategoryCode;
	}
	@Column(name=CoreColumnName.BANK_COMMODITY_CATEGORY_CODE)
	public String getBankCommodityCategoryCode(){
		return bankCommodityCategoryCode;
	}
	public void setBankCommodityCategoryCode(String bankCommodityCategoryCode){
		this.bankCommodityCategoryCode=bankCommodityCategoryCode;
	}
	@Column(name=CoreColumnName.COMMODITY_CATEGORY_DESCRIPTION)
	public String getCommodityCategoryDescription(){
		return commodityCategoryDescription;
	}
	public void setCommodityCategoryDescription(String commodityCategoryDescription){
		this.commodityCategoryDescription=commodityCategoryDescription;
	}
	@Column(name=CoreColumnName.HCI_COMMODITY_TYPE_CODE)
	public String getHciCommodityTypeCode(){
		return hciCommodityTypeCode;
	}
	public void setHciCommodityTypeCode(String hciCommodityTypeCode){
		this.hciCommodityTypeCode=hciCommodityTypeCode;
	}
	@Column(name=CoreColumnName.BANK_COMMODITY_TYPE_CODE)
	public String getBankCommodityTypeCode(){
		return bankCommodityTypeCode;
	}
	public void setBankCommodityTypeCode(String bankCommodityTypeCode){
		this.bankCommodityTypeCode=bankCommodityTypeCode;
	}
	@Column(name=CoreColumnName.COMMODITY_TYPE_DESC)
	public String getCommodityTypeDesc(){
		return commodityTypeDesc;
	}
	public void setCommodityTypeDesc(String commodityTypeDesc){
		this.commodityTypeDesc=commodityTypeDesc;
	}
	@Column(name=CoreColumnName.DELIVERY_TYPE_CODE)
	public String getDeliveryTypeCode(){
		return deliveryTypeCode;
	}
	public void setDeliveryTypeCode(String deliveryTypeCode){
		this.deliveryTypeCode=deliveryTypeCode;
	}
	@Column(name=CoreColumnName.MODEL_NUMBER)
	public String getModelNumber(){
		return modelNumber;
	}
	public void setModelNumber(String modelNumber){
		this.modelNumber=modelNumber;
	}
	@Column(name=CoreColumnName.PRODUCER)
	public String getProducer(){
		return producer;
	}
	public void setProducer(String producer){
		this.producer=producer;
	}
	@Column(name=CoreColumnName.SERIAL_NUMBER)
	public String getSerialNumber(){
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber){
		this.serialNumber=serialNumber;
	}
	@Column(name=CoreColumnName.ENGINE_NUMBER)
	public String getEngineNumber(){
		return engineNumber;
	}
	public void setEngineNumber(String engineNumber){
		this.engineNumber=engineNumber;
	}
	@Column(name=CoreColumnName.GOODS_PRICE)
	public String getGoodsPrice(){
		return goodsPrice;
	}
	public void setGoodsPrice(String goodsPrice){
		this.goodsPrice=goodsPrice;
	}
	@Column(name=CoreColumnName.GOODS_PRICE_CURR)
	public String getGoodsPriceCurr(){
		return goodsPriceCurr;
	}
	public void setGoodsPriceCurr(String goodsPriceCurr){
		this.goodsPriceCurr=goodsPriceCurr;
	}
	@Column(name=CoreColumnName.UNTAXED_PRICE_AMOUNT)
	public String getUntaxedPriceAmount(){
		return untaxedPriceAmount;
	}
	public void setUntaxedPriceAmount(String untaxedPriceAmount){
		this.untaxedPriceAmount=untaxedPriceAmount;
	}
	@Column(name=CoreColumnName.UNTAXED_PRICE_CURR)
	public String getUntaxedPriceCurr(){
		return untaxedPriceCurr;
	}
	public void setUntaxedPriceCurr(String untaxedPriceCurr){
		this.untaxedPriceCurr=untaxedPriceCurr;
	}
	@Column(name=CoreColumnName.SERVICE_TYPE_CODE)
	public String getServiceTypeCode(){
		return serviceTypeCode;
	}
	public void setServiceTypeCode(String serviceTypeCode){
		this.serviceTypeCode=serviceTypeCode;
	}
	@Column(name=CoreColumnName.SERVICE_CODE)
	public String getServiceCode(){
		return serviceCode;
	}
	public void setServiceCode(String serviceCode){
		this.serviceCode=serviceCode;
	}
	@Column(name=CoreColumnName.SERVICE_NAME)
	public String getServiceName(){
		return serviceName;
	}
	public void setServiceName(String serviceName){
		this.serviceName=serviceName;
	}
	@Column(name=CoreColumnName.SERVICE_DESCRIPTION)
	public String getServiceDescription(){
		return serviceDescription;
	}
	public void setServiceDescription(String serviceDescription){
		this.serviceDescription=serviceDescription;
	}
	@Column(name=CoreColumnName.TARIFF_CODE)
	public String getTariffCode(){
		return tariffCode;
	}
	public void setTariffCode(String tariffCode){
		this.tariffCode=tariffCode;
	}
	@Column(name=CoreColumnName.TARIFF_NAME)
	public String getTariffName(){
		return tariffName;
	}
	public void setTariffName(String tariffName){
		this.tariffName=tariffName;
	}
	@Column(name=CoreColumnName.TARIFF_TYPE_CODE)
	public String getTariffTypeCode(){
		return tariffTypeCode;
	}
	public void setTariffTypeCode(String tariffTypeCode){
		this.tariffTypeCode=tariffTypeCode;
	}
	@Column(name=CoreColumnName.TARIFF_CURRENCY_CODE)
	public String getTariffCurrencyCode(){
		return tariffCurrencyCode;
	}
	public void setTariffCurrencyCode(String tariffCurrencyCode){
		this.tariffCurrencyCode=tariffCurrencyCode;
	}
	@Override
	public String toString(){
		StringBuilder json=new StringBuilder(1000);
		json.append("VwCofinContractDetail [textContractNumber=");
		json.append(textContractNumber);
		json.append(",jfsContractStatus=");
		json.append(jfsContractStatus);
		json.append(",status=");
		json.append(status);
		json.append(",contractProcessingType=");
		json.append(contractProcessingType);
		json.append(",registrationStatus=");
		json.append(registrationStatus);
		json.append(",contractType=");
		json.append(contractType);
		json.append(",loanPurpose=");
		json.append(loanPurpose);
		json.append(",productId=");
		json.append(productId);
		json.append(",productCode=");
		json.append(productCode);
		json.append(",productName=");
		json.append(productName);
		json.append(",productDescription=");
		json.append(productDescription);
		json.append(",currencyCode=");
		json.append(currencyCode);
		json.append(",saleDescription=");
		json.append(saleDescription);
		json.append(",productProfileId=");
		json.append(productProfileId);
		json.append(",productProfileCode=");
		json.append(productProfileCode);
		json.append(",productProfileName=");
		json.append(productProfileName);
		json.append(",applicationForm=");
		json.append(applicationForm);
		json.append(",initTransactionType=");
		json.append(initTransactionType);
		json.append(",salesroomId=");
		json.append(salesroomId);
		json.append(",businessAreaCode=");
		json.append(businessAreaCode);
		json.append(",salesroomName=");
		json.append(salesroomName);
		json.append(",salesroomBusinessModelCode=");
		json.append(salesroomBusinessModelCode);
		json.append(",salesroomCategoryCode=");
		json.append(salesroomCategoryCode);
		json.append(",paperless=");
		json.append(paperless);
		json.append(",commodityTypeId=");
		json.append(commodityTypeId);
		json.append(",commodityTypeCode=");
		json.append(commodityTypeCode);
		json.append(",commodityCategoryCode=");
		json.append(commodityCategoryCode);
		json.append(",bankCommodityCategoryCode=");
		json.append(bankCommodityCategoryCode);
		json.append(",commodityCategoryDescription=");
		json.append(commodityCategoryDescription);
		json.append(",hciCommodityTypeCode=");
		json.append(hciCommodityTypeCode);
		json.append(",bankCommodityTypeCode=");
		json.append(bankCommodityTypeCode);
		json.append(",commodityTypeDesc=");
		json.append(commodityTypeDesc);
		json.append(",deliveryTypeCode=");
		json.append(deliveryTypeCode);
		json.append(",modelNumber=");
		json.append(modelNumber);
		json.append(",producer=");
		json.append(producer);
		json.append(",serialNumber=");
		json.append(serialNumber);
		json.append(",engineNumber=");
		json.append(engineNumber);
		json.append(",goodsPrice=");
		json.append(goodsPrice);
		json.append(",goodsPriceCurr=");
		json.append(goodsPriceCurr);
		json.append(",untaxedPriceAmount=");
		json.append(untaxedPriceAmount);
		json.append(",untaxedPriceCurr=");
		json.append(untaxedPriceCurr);
		json.append(",serviceTypeCode=");
		json.append(serviceTypeCode);
		json.append(",serviceCode=");
		json.append(serviceCode);
		json.append(",serviceName=");
		json.append(serviceName);
		json.append(",serviceDescription=");
		json.append(serviceDescription);
		json.append(",tariffCode=");
		json.append(tariffCode);
		json.append(",tariffName=");
		json.append(tariffName);
		json.append(",tariffTypeCode=");
		json.append(tariffTypeCode);
		json.append(",tariffCurrencyCode=");
		json.append(tariffCurrencyCode);
		json.append("]");
		return json.toString();
	}
}