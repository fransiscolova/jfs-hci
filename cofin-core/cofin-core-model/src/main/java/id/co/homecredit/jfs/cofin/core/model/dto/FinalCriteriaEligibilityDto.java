package id.co.homecredit.jfs.cofin.core.model.dto;

import java.util.List;

import id.co.homecredit.jfs.cofin.core.model.Agreement;

public class FinalCriteriaEligibilityDto {
    private Agreement agreement;
    private List<FlatCriteriaEligibilityDto> flatCriteriaEligibilityDtos;

    public Agreement getAgreement() {
        return agreement;
    }

    public List<FlatCriteriaEligibilityDto> getFlatCriteriaEligibilityDtos() {
        return flatCriteriaEligibilityDtos;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public void setFlatCriteriaEligibilityDtos(
            List<FlatCriteriaEligibilityDto> flatCriteriaEligibilityDtos) {
        this.flatCriteriaEligibilityDtos = flatCriteriaEligibilityDtos;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("FinalCriteriaEligibilityDto [agreement=");
        builder.append(agreement);
        builder.append(", flatCriteriaEligibilityDtos=");
        builder.append(flatCriteriaEligibilityDtos);
        builder.append("]");
        return builder.toString();
    }

}
