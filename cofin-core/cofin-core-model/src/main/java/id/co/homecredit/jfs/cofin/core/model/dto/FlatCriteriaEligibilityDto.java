package id.co.homecredit.jfs.cofin.core.model.dto;

import java.util.List;
import java.util.Map;

import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.EligibilityEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.OperatorEnum;

/**
 * @author muhammad.muflihun
 *
 */
public class FlatCriteriaEligibilityDto {
    private CriteriaEnum criteria;
    private List<Map<EligibilityEnum, String>> listMap;
    private OperatorEnum operator = OperatorEnum.AND;
    private List<FlatCriteriaEligibilityDto> flatDtos;
    private Integer level;

    public CriteriaEnum getCriteria() {
        return criteria;
    }

    public List<FlatCriteriaEligibilityDto> getFlatDtos() {
        return flatDtos;
    }

    public Integer getLevel() {
        return level;
    }

    public List<Map<EligibilityEnum, String>> getListMap() {
        return listMap;
    }

    public OperatorEnum getOperator() {
        return operator;
    }

    public void setCriteria(CriteriaEnum criteria) {
        this.criteria = criteria;
    }

    public void setFlatDtos(List<FlatCriteriaEligibilityDto> flatDtos) {
        this.flatDtos = flatDtos;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public void setListMap(List<Map<EligibilityEnum, String>> listMap) {
        this.listMap = listMap;
    }

    public void setOperator(OperatorEnum operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FlatCriteriaEligibilityDto [criteria=");
        builder.append(criteria);
        builder.append(", listMap=");
        builder.append(listMap);
        builder.append(", operator=");
        builder.append(operator);
        builder.append(", flatDtos=");
        builder.append(flatDtos);
        builder.append(", level=");
        builder.append(level);
        builder.append("]");
        return builder.toString();
    }

}
