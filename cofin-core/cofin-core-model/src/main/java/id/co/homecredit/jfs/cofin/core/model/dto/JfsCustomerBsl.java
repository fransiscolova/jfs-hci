package id.co.homecredit.jfs.cofin.core.model.dto;

/**
 * Model For Accomodate Customer Detail From BSL
 * 
 * @author denny.afrizal01
 *
 */
public class JfsCustomerBsl {
	private String cuid;
	private String firstName;
	private String lastName;
	private String legalName;
	private String gender;
	private String mothersName;
	private String birthPlace;
	private String birthDate;
	private String education;
	private String maritalStatus;
	private String noDependents;
	private String religion;
	private String occupation;
	private String placeWork;
	private String fieldBusiness;
	private String monthlyIncome;
	private String typeIdCard;
	private String noIdCard;
	private String exDateIdCard;
	private String streetName;
	private String town;
	private String subdistrict;
	private String district;
	private String rt;
	private String rw;
	private String zipCode;
	public String getCuid() {
		return cuid;
	}
	public void setCuid(String cuid) {
		this.cuid = cuid;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLegalName() {
		return legalName;
	}
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMothersName() {
		return mothersName;
	}
	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getNoDependents() {
		return noDependents;
	}
	public void setNoDependents(String noDependents) {
		this.noDependents = noDependents;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getPlaceWork() {
		return placeWork;
	}
	public void setPlaceWork(String placeWork) {
		this.placeWork = placeWork;
	}
	public String getFieldBusiness() {
		return fieldBusiness;
	}
	public void setFieldBusiness(String fieldBusiness) {
		this.fieldBusiness = fieldBusiness;
	}
	public String getMonthlyIncome() {
		return monthlyIncome;
	}
	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}
	public String getTypeIdCard() {
		return typeIdCard;
	}
	public void setTypeIdCard(String typeIdCard) {
		this.typeIdCard = typeIdCard;
	}
	public String getNoIdCard() {
		return noIdCard;
	}
	public void setNoIdCard(String noIdCard) {
		this.noIdCard = noIdCard;
	}
	public String getExDateIdCard() {
		return exDateIdCard;
	}
	public void setExDateIdCard(String exDateIdCard) {
		this.exDateIdCard = exDateIdCard;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getSubdistrict() {
		return subdistrict;
	}
	public void setSubdistrict(String subdistrict) {
		this.subdistrict = subdistrict;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getRt() {
		return rt;
	}
	public void setRt(String rt) {
		this.rt = rt;
	}
	public String getRw() {
		return rw;
	}
	public void setRw(String rw) {
		this.rw = rw;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}
