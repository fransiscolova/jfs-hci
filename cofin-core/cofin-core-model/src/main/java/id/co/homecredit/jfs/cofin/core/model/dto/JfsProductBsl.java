package id.co.homecredit.jfs.cofin.core.model.dto;

/**
 * Entity Class For Accomodate Product Detail From BSL
 * 
 * @author denny.afrizal01
 *
 */
public class JfsProductBsl {
	private String productCode;
	private String productName;
	private String hcidComCategory;
	private String partnerComCategory;
	private String hcidComType;
	private String partnerComType;
	private String manufacturer;
	private String modelNumber;
	private String goodsPrice;
	private String salesRoomCode;
	private String salesRoomName;
	private String premiumAmount;
	private String premiumPayment;
	private String requestDate;
	private String totalAmount;
	private String paidAmount;
	private String remainingAmount;
	private String etDueDate;
	private String etCompilationDate;
	private String numberGiftPayment;
	private String dateEvaluation;
	private String eligibilityType;
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getHcidComCategory() {
		return hcidComCategory;
	}
	public void setHcidComCategory(String hcidComCategory) {
		this.hcidComCategory = hcidComCategory;
	}
	public String getPartnerComCategory() {
		return partnerComCategory;
	}
	public void setPartnerConCategory(String partnerComCategory) {
		this.partnerComCategory = partnerComCategory;
	}
	public String getHcidComType() {
		return hcidComType;
	}
	public void setHcidComType(String hcidComType) {
		this.hcidComType = hcidComType;
	}
	public String getPartnerComType() {
		return partnerComType;
	}
	public void setPartnerComType(String partnerComType) {
		this.partnerComType = partnerComType;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModelNumber() {
		return modelNumber;
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getGoodsPrice() {
		return goodsPrice;
	}
	public void setGoodsPrice(String goodsPrice) {
		this.goodsPrice = goodsPrice;
	}
	public String getSalesRoomCode() {
		return salesRoomCode;
	}
	public void setSalesRoomCode(String salesRoomCode) {
		this.salesRoomCode = salesRoomCode;
	}
	public String getSalesRoomName() {
		return salesRoomName;
	}
	public void setSalesRoomName(String salesRoomName) {
		this.salesRoomName = salesRoomName;
	}
	public String getPremiumAmount() {
		return premiumAmount;
	}
	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
	public String getPremiumPayment() {
		return premiumPayment;
	}
	public void setPremiumPayment(String premiumPayment) {
		this.premiumPayment = premiumPayment;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getRemainingAmount() {
		return remainingAmount;
	}
	public void setRemainingAmount(String remainingAmount) {
		this.remainingAmount = remainingAmount;
	}
	public String getEtDueDate() {
		return etDueDate;
	}
	public void setEtDueDate(String etDueDate) {
		this.etDueDate = etDueDate;
	}
	public String getEtCompilationDate() {
		return etCompilationDate;
	}
	public void setEtCompilationDate(String etCompilationDate) {
		this.etCompilationDate = etCompilationDate;
	}
	public String getNumberGiftPayment() {
		return numberGiftPayment;
	}
	public void setNumberGiftPayment(String numberGiftPayment) {
		this.numberGiftPayment = numberGiftPayment;
	}
	public String getDateEvaluation() {
		return dateEvaluation;
	}
	public void setDateEvaluation(String dateEvaluation) {
		this.dateEvaluation = dateEvaluation;
	}
	public String getEligibilityType() {
		return eligibilityType;
	}
	public void setEligibilityType(String eligibilityType) {
		this.eligibilityType = eligibilityType;
	}
}
