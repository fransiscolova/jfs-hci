package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration for {@code Activity}.
 * 
 * @author muhammad.muflihun
 *
 */
public enum ActivityEnum {
    INSERT("Insert new data"),
    UPDATE("Update existing data"),
    SOFT_DELETE("Soft delete existing data"),
    UNDO_DELETE("Undo delete existing data"),
    DELETE("Completely delete data");
    
    private String description;

    ActivityEnum(String description) {
        this.description = description;
    }

    /**
     * Get activity description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }
}
