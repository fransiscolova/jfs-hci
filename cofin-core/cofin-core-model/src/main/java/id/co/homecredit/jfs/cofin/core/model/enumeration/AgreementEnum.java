package id.co.homecredit.jfs.cofin.core.model.enumeration;

import id.co.homecredit.jfs.cofin.core.model.Agreement;

/**
 * Enumeration equals entity {@link Agreement}.
 *
 * @author muhammad.muflihun
 *
 */
public enum AgreementEnum {
    BTPN_REG("F8497F7682784A6694593EAFC8A33BDD", "2", AgreementTypeEnum.REG), 
    BTPN_EZ10("9613C069D16945B3B6FB75FE48A1BC31", "3", AgreementTypeEnum.EZ10), 
    BTPN_FF("4FB2D012EA8A440DA13AF58FFDD08FFE", "4", AgreementTypeEnum.FF), 
    BTPN_ZERO("30F3CBF094F94B868D095F02D4D34629", "5", AgreementTypeEnum.ZERO), 
    PERMATA_REG("245E17CFDEE447E9842DB960CA29D668", "51", AgreementTypeEnum.REG);

    private String id;
    private String idAlias;
    private AgreementTypeEnum agreementType;

    AgreementEnum(String id, String idAlias, AgreementTypeEnum agreementType) {
        this.id = id;
        this.idAlias = idAlias;
        this.agreementType = agreementType;
    }

    /**
     * Get agreement type.
     *
     * @return agreementType
     */
    public AgreementTypeEnum getAgreementType() {
        return agreementType;
    }

    /**
     * Get agreement id.
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Get agreement id alias.
     *
     * @return idAlias
     */
    public String getIdAlias() {
        return idAlias;
    }

}
