package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration for {@code AgreementType}.
 *
 * @author muhammad.muflihun
 *
 */
public enum AgreementTypeEnum {
    REG("Regular"), 
    EZ10("Easyten"), 
    FF("Flexy Fast"), 
    ZERO("Zero Product");

    private String description;

    AgreementTypeEnum(String description) {
        this.description = description;
    }

    /**
     * Get agreement type description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
