package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration for entity {@code COFIN_COMMODITY_CATEGORY}.
 * 
 * @author muhammad.muflihun
 *
 */
public enum CommodityCategoryEnum {
    CAT_CD("Consumer durable"), 
    CAT_MP("Mobile phone"), 
    CAT_CD_FUR("Furniture"), 
    CAT_TV("Television"), 
    CAT_GG("Gadget"), 
    CAT_LAP("Laptop / desktop"), 
    CAT_SP("Smart phone");
    
    private String description;
    
    CommodityCategoryEnum(String description) {
        this.description = description;
    }

    /**
     * Get commodity category description.
     * 
     * @return description
     */
    public String getDescription() {
        return description;
    }
    
}
