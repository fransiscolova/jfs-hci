package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration for entity {@code COFIN_COMMODITY_TYPE}.
 *
 * @author muhammad.muflihun
 *
 */
public enum CommodityTypeEnum {
    CT_CT_GC("Game console (Playstation / Xbox / dll)"),
    CT_HA_PVO("Kamera foto / Video"),
    CT_HA_STB("Aora STB"),
    CT_HA_WM("Mesin cuci"),
    CT_HA_CTV("Televisi"),
    CD_HA_AS("Audio / Headphone speaker"),
    CT_TE_SP("Samsung Promo Mobile Phone"),
    CT_TRA("Transport - Flights, Trains, Ferry etc"),
    CT_GG_SP("Samsung Promo Tab"),
    CT_HA_MWO("Microwave Oven"),
    CT_TE_MP("Handphone / Smart Phones"),
    CT_HA_BIC("Sepeda"),
    CT_WAA("Wedding & anniversary Accessories"),
    CT_SPE("Specialist - Dentistry, Optical, Consultations"),
    CT_CRU("Repair or cosmetical upgrade of car"),
    CT_CT_MTAB("Tablet"),
    CT_HA_ACO("Pendingin udara"),
    CT_HA_VCL("Alat pembersih kecil (vacuum cleaner dll)"),
    CT_FUR_BED("");

    private String description;

    CommodityTypeEnum(String description) {
        this.description = description;
    }

    /**
     * Get commodity type description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
