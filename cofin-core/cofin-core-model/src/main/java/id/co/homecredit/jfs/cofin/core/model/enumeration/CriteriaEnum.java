package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration for {@code Criteria}.
 *
 * @author muhammad.muflihun
 *
 */
public enum CriteriaEnum {
    GROUP("Make criteria as one group"),
    AGE("Customer age"),
    CONTRACT_STATUS_CODE("Contract status code"),
    DOWN_PAYMENT("Contract down payment"),
    KTP_STATUS("Customer KTP's active status"),
    SERVICE_TYPE_CODE("Contract service type code"),
    TERMS("Contract terms"),
    TOTAL_OUTSTANDING_PRINCIPAL("Total loan to partner"),
    COMMODITY_CATEGORY("Contract commodity category"),
    COMMODITY_TYPE("Contract commodity type"),
    TOTAL_FINANCED_AMOUNT("Total amount that will be financed by partner"),
    TOTAL_ACTIVE_FINANCED_AMOUNT_PER_CUSTOMER("Total active amount that will be financed by partner per customer");

    private String description;

    CriteriaEnum(String description) {
        this.description = description;
    }

    /**
     * Get role description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }
 
}
