package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration for {@code Eligibility}.
 *
 * @author muhammad.muflihun
 *
 */
public enum EligibilityEnum {
    EQUALS("Value must equals with inserted parameter."),
    NOT_EQUALS("Value must not equals with inserted parameter."),
    MINIMUM("Minimum value cannot be lower than inserted parameter."),
    MAXIMUM("Maximum value cannot be lower than inserted parameter."),
    IN("Value must equals with one of the inserted parameters."),
    NOT_IN("Value must not equals with any of the inserted parameters.");

    private String description;

    EligibilityEnum(String description) {
        this.description = description;
    }

    /**
     * Get role description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
