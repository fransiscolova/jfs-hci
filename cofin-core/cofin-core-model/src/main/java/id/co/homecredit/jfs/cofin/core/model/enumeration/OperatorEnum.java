package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration equals entity {@link Operator}.
 *
 * @author muhammad.muflihun
 *
 */
public enum OperatorEnum {
    OR("One of parameter conditions must be fulfilled."),
    AND("All parameter conditions must be fulfilled.");

    private String description;

    OperatorEnum(String description) {
        this.description = description;
    }

    /**
     * Get role description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
