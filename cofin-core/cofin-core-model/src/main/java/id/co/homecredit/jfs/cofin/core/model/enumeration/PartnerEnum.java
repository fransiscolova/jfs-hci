package id.co.homecredit.jfs.cofin.core.model.enumeration;

import id.co.homecredit.jfs.cofin.core.model.Partner;

/**
 * Enumeration equals entity {@link Partner}.
 *
 * @author muhammad.muflihun
 *
 */
public enum PartnerEnum {
    BTPN("DD623E14AE644A408E52F495A756EAC0"), 
    PERMATA("DB57E85359684F13A2B8DAA1493FDF1F");
    
    private String id;
    
    PartnerEnum(String id) {
        this.id = id;
    }

    /**
     * Get partner id.
     * 
     * @return
     */
    public String getId() {
        return id;
    }
    
    
}
