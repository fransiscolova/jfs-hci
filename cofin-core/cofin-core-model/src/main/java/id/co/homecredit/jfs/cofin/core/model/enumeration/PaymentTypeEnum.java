package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration for {@code PAYMENT_TYPE}.
 *
 * @author muhammad.muflihun
 *
 */
public enum PaymentTypeEnum {
    R("Reguler"),
    T("ET after"),
    U("Small under-payment"), 
    O("Cancel"), 
    L("Last"), 
    B("Others"), 
    G("Gift"); 

    private String description;

    PaymentTypeEnum(String description) {
        this.description = description;
    }

    /**
     * Get role description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
