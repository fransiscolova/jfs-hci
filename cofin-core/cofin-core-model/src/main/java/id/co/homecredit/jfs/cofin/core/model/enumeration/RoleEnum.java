package id.co.homecredit.jfs.cofin.core.model.enumeration;

/**
 * Enumeration for table {@link MST_ROLE}.
 *
 * @author muhammad.muflihun
 *
 */
public enum RoleEnum {
    DEV("God"),
    TESTER("Demi-God"),
    ADMIN("Role For Registering Another Role In JFS"), 
    JFS("Role For Registering JFS Partner"), 
    BO("Role For Back Office Team"),
    USER("Role For Common User"),
	PO("Product Owner");

    private String description;

    RoleEnum(String description) {
        this.description = description;
    }

    /**
     * Get role description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

}
