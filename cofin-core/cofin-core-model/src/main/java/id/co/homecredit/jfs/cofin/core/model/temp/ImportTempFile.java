package id.co.homecredit.jfs.cofin.core.model.temp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.BaseEntity;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityId;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.TmpTableName;

/**
 * Entity class for {@code COFIN_TEMP_CONFIRMATION_FILE}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = TmpTableName.IMPORT_TMP)
public class ImportTempFile extends BaseEntityIdOnly<String> {
	private String col1;
	private String col2;
	private String col3;
	private String col4;
	private String col5;
	private String col6;
	private String col8;
	private String col7;	
	private String col9;
	private String col10;
	private String col11;
	private String col12;
	private String col13;
	private String col14;
	private String col15;
	private String col16;
	private String col17;
	private String col18;
	private String col19;
	private String col120;
	private String col121;
	private String col122;
	private String col123;
	private String col124;
	private String col125;
	private String col126;
	private String col127;
	private String col128;
	private String col129;
	private String col130;
	private FileUpload fileUpload;
	
	
	
	
	@ManyToOne
    @JoinColumn(name = CoreColumnName.REFERENCE, referencedColumnName = ColumnName.ID, nullable = false)
    public FileUpload getFileUpload() {
        return fileUpload;
    }



	@Column(name = "COL1")
	public String getCol1() {
		return col1;
	}



	@Column(name = "COL2")
	public String getCol2() {
		return col2;
	}



	@Column(name = "COL3")
	public String getCol3() {
		return col3;
	}



	@Column(name = "COL4")
	public String getCol4() {
		return col4;
	}



	@Column(name = "COL5")
	public String getCol5() {
		return col5;
	}



	@Column(name = "COL6")
	public String getCol6() {
		return col6;
	}

	@Column(name = "COL7")
	public String getCol7() {
		return col7;
	}

	@Column(name = "COL8")
	public String getCol8() {
		return col8;
	}



	@Column(name = "COL9")
	public String getCol9() {
		return col9;
	}



	@Column(name = "COL10")
	public String getCol10() {
		return col10;
	}



	@Column(name = "COL11")
	public String getCol11() {
		return col11;
	}



	@Column(name = "COL12")
	public String getCol12() {
		return col12;
	}



	@Column(name = "COL13")
	public String getCol13() {
		return col13;
	}



	@Column(name = "COL14")
	public String getCol14() {
		return col14;
	}



	@Column(name = "COL15")
	public String getCol15() {
		return col15;
	}

	@Column(name = "COL16")
	public String getCol16() {
		return col16;
	}

	

	@Column(name = "COL17")
	public String getCol17() {
		return col17;
	}



	@Column(name = "COL18")
	public String getCol18() {
		return col18;
	}



	@Column(name = "COL19")
	public String getCol19() {
		return col19;
	}



	@Column(name = "COL20")
	public String getCol120() {
		return col120;
	}



	@Column(name = "COL21")
	public String getCol121() {
		return col121;
	}



	@Column(name = "COL22")
	public String getCol122() {
		return col122;
	}



	@Column(name = "COL23")
	public String getCol123() {
		return col123;
	}



	@Column(name = "COL24")
	public String getCol124() {
		return col124;
	}



	@Column(name = "COL25")
	public String getCol125() {
		return col125;
	}



	@Column(name = "COL26")
	public String getCol126() {
		return col126;
	}



	@Column(name = "COL27")
	public String getCol127() {
		return col127;
	}



	@Column(name = "COL28")
	public String getCol128() {
		return col128;
	}



	@Column(name = "COL29")
	public String getCol129() {
		return col129;
	}



	@Column(name = "COL30")
	public String getCol130() {
		return col130;
	}





	public void setCol1(String col1) {
		this.col1 = col1;
	}




	public void setCol2(String col2) {
		this.col2 = col2;
	}




	public void setCol3(String col3) {
		this.col3 = col3;
	}




	public void setCol4(String col4) {
		this.col4 = col4;
	}




	public void setCol5(String col5) {
		this.col5 = col5;
	}




	public void setCol6(String col6) {
		this.col6 = col6;
	}

	public void setCol7(String col7) {
		this.col7 = col7;
	}


	public void setCol8(String col8) {
		this.col8 = col8;
	}




	public void setCol9(String col9) {
		this.col9 = col9;
	}




	public void setCol10(String col10) {
		this.col10 = col10;
	}




	public void setCol11(String col11) {
		this.col11 = col11;
	}




	public void setCol12(String col12) {
		this.col12 = col12;
	}




	public void setCol13(String col13) {
		this.col13 = col13;
	}




	public void setCol14(String col14) {
		this.col14 = col14;
	}




	public void setCol15(String col15) {
		this.col15 = col15;
	}


	public void setCol16(String col16) {
		this.col16 = col16;
	}

	public void setCol17(String col17) {
		this.col17 = col17;
	}




	public void setCol18(String col18) {
		this.col18 = col18;
	}




	public void setCol19(String col19) {
		this.col19 = col19;
	}




	public void setCol120(String col120) {
		this.col120 = col120;
	}




	public void setCol121(String col121) {
		this.col121 = col121;
	}




	public void setCol122(String col122) {
		this.col122 = col122;
	}




	public void setCol123(String col123) {
		this.col123 = col123;
	}




	public void setCol124(String col124) {
		this.col124 = col124;
	}




	public void setCol125(String col125) {
		this.col125 = col125;
	}




	public void setCol126(String col126) {
		this.col126 = col126;
	}




	public void setCol127(String col127) {
		this.col127 = col127;
	}




	public void setCol128(String col128) {
		this.col128 = col128;
	}




	public void setCol129(String col129) {
		this.col129 = col129;
	}




	public void setCol130(String col130) {
		this.col130 = col130;
	}






	public void setFileUpload(FileUpload fileUpload) {
		this.fileUpload = fileUpload;
	}
	
	
	
	
	
	

	// @Column(name = CoreColumnName.ACTION)
	// public String getAction() {
	// return action;
	// }

	/**
	 * Print each field value for this entity.
	 * 
	 * @Override public String toString() { StringBuilder builder = new
	 *           StringBuilder(1000); builder.append("Action [action=");
	 *           builder.append(action); builder.append(", name=");
	 * 
	 *           builder.append(createdDate); builder.append("]"); return
	 *           builder.toString(); }
	 */
}
