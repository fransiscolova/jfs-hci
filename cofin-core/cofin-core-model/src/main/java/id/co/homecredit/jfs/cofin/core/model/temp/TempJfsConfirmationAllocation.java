package id.co.homecredit.jfs.cofin.core.model.temp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.BaseEntity;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityId;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityIdOnly;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.TmpTableName;

/**
 * Entity class for {@code COFIN_TEMP_CONFIRMATION_FILE}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = TmpTableName.COFIN_TEMP_CONFIRMATION_ALLOCATION)
public class TempJfsConfirmationAllocation extends BaseEntityIdOnly<String> {
	private String recordNo;
	private String batchNumber;
	private Date batchDate;
	private Date exportTime;
	private String contractNumber;
	private String clientName;
	private Date dueDate;
	private Date payemntDate;
	private BigDecimal instalment;
	private BigDecimal amtPrincipal;
	private BigDecimal amtBankInterest;
	private BigDecimal interestAdvance;
	private BigDecimal amtPenalty;
	private FileUpload fileUpload;

	
	
	@ManyToOne
    @JoinColumn(name = CoreColumnName.REFERENCE, referencedColumnName = ColumnName.ID, nullable = false)
    public FileUpload getFileUpload() {
        return fileUpload;
    }


	@Column(name="RECORD_NO")
	public String getRecordNo() {
		return recordNo;
	}


	@Column(name="BATCH_NUMBER")
	public String getBatchNumber() {
		return batchNumber;
	}


	@Column(name="BATCH_DATE")
	public Date getBatchDate() {
		return batchDate;
	}


	@Column(name="EXPORT_TIME")
	public Date getExportTime() {
		return exportTime;
	}


	@Column(name="CONTRACT_NUMBER")
	public String getContractNumber() {
		return contractNumber;
	}


	@Column(name="CLIENT_NAME")
	public String getClientName() {
		return clientName;
	}


	@Column(name="DUE_DATE")
	public Date getDueDate() {
		return dueDate;
	}


	@Column(name="PAYMENT_DATE")
	public Date getPayemntDate() {
		return payemntDate;
	}


	@Column(name="INSTALLMENT")
	public BigDecimal getInstalment() {
		return instalment;
	}


	@Column(name="AMT_PRINCIPAL")
	public BigDecimal getAmtPrincipal() {
		return amtPrincipal;
	}


	@Column(name="AMT_BANK_INTEREST")
	public BigDecimal getAmtBankInterest() {
		return amtBankInterest;
	}


	@Column(name="INTEREST_ADVANCE")
	public BigDecimal getInterestAdvance() {
		return interestAdvance;
	}


	@Column(name="AMT_PENALTY")
	public BigDecimal getAmtPenalty() {
		return amtPenalty;
	}


	
	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}



	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}



	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}



	public void setExportTime(Date exportTime) {
		this.exportTime = exportTime;
	}



	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}



	public void setClientName(String clientName) {
		this.clientName = clientName;
	}



	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}



	public void setPayemntDate(Date payemntDate) {
		this.payemntDate = payemntDate;
	}



	public void setInstalment(BigDecimal instalment) {
		this.instalment = instalment;
	}



	public void setAmtPrincipal(BigDecimal amtPrincipal) {
		this.amtPrincipal = amtPrincipal;
	}



	public void setAmtBankInterest(BigDecimal amtBankInterest) {
		this.amtBankInterest = amtBankInterest;
	}



	public void setInterestAdvance(BigDecimal interestAdvance) {
		this.interestAdvance = interestAdvance;
	}



	public void setAmtPenalty(BigDecimal amtPenalty) {
		this.amtPenalty = amtPenalty;
	}



	public void setFileUpload(FileUpload fileUpload) {
		this.fileUpload = fileUpload;
	}
	
	
	// @Column(name = CoreColumnName.ACTION)
	// public String getAction() {
	// return action;
	// }

	/**
	 * Print each field value for this entity.
	 * 
	 * @Override public String toString() { StringBuilder builder = new
	 *           StringBuilder(1000); builder.append("Action [action=");
	 *           builder.append(action); builder.append(", name=");
	 * 
	 *           builder.append(createdDate); builder.append("]"); return
	 *           builder.toString(); }
	 */
}
