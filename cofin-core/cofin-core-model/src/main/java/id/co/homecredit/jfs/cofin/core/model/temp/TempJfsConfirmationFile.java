package id.co.homecredit.jfs.cofin.core.model.temp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import id.co.homecredit.jfs.cofin.common.model.BaseEntity;
import id.co.homecredit.jfs.cofin.common.model.BaseEntityId;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.TmpTableName;

/**
 * Entity class for {@code COFIN_TEMP_CONFIRMATION_FILE}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = TmpTableName.COFIN_TEMP_CONFIRMATION_FILE)
public class TempJfsConfirmationFile extends BaseEntityId<String> implements Serializable{
	private String record;
	private Double checksum;
	private String batchNumber;
	private Date batchDate;
	private Double splitRate;
	private Date exportTime;
	private BigDecimal amtBankPrincipal;
	private BigDecimal amtBankInterest;
	private BigDecimal amtPrincipal;
	private BigDecimal bankRate;
	private Double amtBankProvision;
	private Double amtBankAdminFee;
	private Double bankTenor;
	private String clientName;
	private FileUpload fileUpload;

	@ManyToOne
    @JoinColumn(name = CoreColumnName.REFERENCE, referencedColumnName = ColumnName.ID, nullable = false)
    public FileUpload getFileUpload() {
        return fileUpload;
    }
	
	@Column(name = CoreColumnName.RECORD)
	public String getRecord() {
		return record;
	}

	@Column(name = CoreColumnName.CHECKSUM)
	public Double getChecksum() {
		return checksum;
	}

	@Column(name = CoreColumnName.BATCH_NUMBER)
	public String getBatchNumber() {
		return batchNumber;
	}

	@Column(name = CoreColumnName.BATCH_DATE)
	public Date getBatchDate() {
		return batchDate;
	}

	@Column(name = CoreColumnName.SPLIT_RATE)
	public Double getSplitRate() {
		return splitRate;
	}

	@Column(name = CoreColumnName.EXPORT_TIME)
	public Date getExportTime() {
		return exportTime;
	}

	@Column(name = CoreColumnName.AMT_BANK_PRINCIPAL)
	public BigDecimal getAmtBankPrincipal() {
		return amtBankPrincipal;
	}

	@Column(name = CoreColumnName.AMT_BANK_INTEREST)
	public BigDecimal getAmtBankInterest() {
		return amtBankInterest;
	}

	@Column(name = CoreColumnName.AMT_PRINCIPAL)
	public BigDecimal getAmtPrincipal() {
		return amtPrincipal;
	}

	@Column(name = CoreColumnName.BANK_RATE)
	public BigDecimal getBankRate() {
		return bankRate;
	}

	@Column(name = CoreColumnName.AMT_BANK_PROVISION)
	public Double getAmtBankProvision() {
		return amtBankProvision;
	}

	@Column(name = CoreColumnName.AMT_BANK_ADMIN_FEE)
	public Double getAmtBankAdminFee() {
		return amtBankAdminFee;
	}

	@Column(name = CoreColumnName.BANK_TENOR)
	public Double getBankTenor() {
		return bankTenor;
	}

	@Column(name = CoreColumnName.CLIENT_NAME)
	public String getClientName() {
		return clientName;
	}

	// setter
	public void setFileUpload(FileUpload fileUpload) {
        this.fileUpload = fileUpload;
    }
	
	
	public void setRecord(String record) {
		this.record = record;
	}

	public void setChecksum(Double checksum) {
		this.checksum = checksum;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public void setSplitRate(Double splitRate) {
		this.splitRate = splitRate;
	}

	public void setExportTime(Date exportTime) {
		this.exportTime = exportTime;
	}

	public void setAmtBankPrincipal(BigDecimal amtBankPrincipal) {
		this.amtBankPrincipal = amtBankPrincipal;
	}

	public void setAmtBankInterest(BigDecimal amtBankInterest) {
		this.amtBankInterest = amtBankInterest;
	}

	public void setAmtPrincipal(BigDecimal amtPrincipal) {
		this.amtPrincipal = amtPrincipal;
	}

	public void setBankRate(BigDecimal bankRate) {
		this.bankRate = bankRate;
	}

	public void setAmtBankProvision(Double amtBankProvision) {
		this.amtBankProvision = amtBankProvision;
	}

	public void setAmtBankAdminFee(Double amtBankAdminFee) {
		this.amtBankAdminFee = amtBankAdminFee;
	}

	public void setBankTenor(Double bankTenor) {
		this.bankTenor = bankTenor;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	// @Column(name = CoreColumnName.ACTION)
	// public String getAction() {
	// return action;
	// }

	/**
	 * Print each field value for this entity.
	 * 
	 * @Override public String toString() { StringBuilder builder = new
	 *           StringBuilder(1000); builder.append("Action [action=");
	 *           builder.append(action); builder.append(", name=");
	 * 
	 *           builder.append(createdDate); builder.append("]"); return
	 *           builder.toString(); }
	 */
}
