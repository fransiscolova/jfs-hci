package id.co.homecredit.jfs.cofin.core.model.upload;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Entity class for {@code COFIN_FILE_UPLOAD}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_CONFIRM_FILE)
public class CofinConfirmFile extends IdEntity {

	CofinConfirmProcess cofinConfirmProcess;
	private String fileName;
	private String fileDesc;
	private String title;
	private int row;
	private String header;
	private String tableName;
	private int priority;
	private int totalColumn;
	private String splitBy;
	private String procedureName;

	
	@AuditTransient
	List<CofinConfirmFileSetting> cofinConfirmFileSetting;
	
	@AuditTransient
	List<FileUpload> fileUpload;
	
	
	
	
	
	@OneToMany(mappedBy="cofinConfirmFile")
	public List<CofinConfirmFileSetting> getCofinConfirmFileSetting() {
		return cofinConfirmFileSetting;
	}

	
	@ManyToOne
	@JoinColumn(name="FK_CONFIRM_PROCESS",referencedColumnName="ID",nullable=false)
	public CofinConfirmProcess getCofinConfirmProcess() {
		return cofinConfirmProcess;
	}

	@Column(name="FILE_NAME")
	public String getFileName() {
		return fileName;
	}

	@Column(name="FILE_DESC")
	public String getFileDesc() {
		return fileDesc;
	}

	@Column(name="TITLE")
	public String getTitle() {
		return title;
	}

	@Column(name="TITLE_ROW_INDEX")
	public int getRow() {
		return row;
	}

	@Column(name="PRIORITY")
	public int getPriority() {
		return priority;
	}

	@Column(name="COLUMN_HEADER")
	public String getHeader() {
		return header;
	}

	@Column(name="TOTAL_COLUMN")
	public int getTotalColumn() {
		return totalColumn;
	}

    

	@Column(name="TABLE_NAME")
	public String getTableName() {
		return tableName;
	}
	
	
	
	@Column(name="SPLIT_BY")
	public String getSplitBy() {
		return splitBy;
	}

	
	
	@OneToMany(mappedBy="cofinConfirmFile")
	public List<FileUpload> getFileUpload() {
		return fileUpload;
	}

	@Column(name="PROCEDURE_NAME")
	public String getProcedureName() {
		return procedureName;
	}
	
	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}
	
	
	public void setFileUpload(List<FileUpload> fileUpload) {
		this.fileUpload = fileUpload;
	}


	public void setSplitBy(String splitBy) {
		this.splitBy = splitBy;
	}


	public void setTotalColumn(int totalColumn) {
		this.totalColumn = totalColumn;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}


	public void setCofinConfirmProcess(CofinConfirmProcess cofinConfirmProcess) {
		this.cofinConfirmProcess = cofinConfirmProcess;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	
	public void setCofinConfirmFileSetting(List<CofinConfirmFileSetting> cofinConfirmFileSetting) {
		this.cofinConfirmFileSetting = cofinConfirmFileSetting;
	}

	
}
