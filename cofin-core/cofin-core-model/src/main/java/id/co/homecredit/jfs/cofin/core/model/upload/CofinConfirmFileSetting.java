package id.co.homecredit.jfs.cofin.core.model.upload;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Entity class for {@code COFIN_FILE_UPLOAD}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_CONFIRM_FILE_SETTING)
public class CofinConfirmFileSetting extends IdEntity {
	CofinConfirmFile cofinConfirmFile;
	private String recordType;
	private String dataSpource;
	private int dataIndex;
	private String dataType;
	private String splitType;
	private String splitDesc;
	private String columnDestination;
	private String defaultData;
	private int splitIndex;
	private String valuePattern;
	private String compareValue;
	private String subString;
	private String replace;
	private String contain;
	

	@ManyToOne
	@JoinColumn(name = "FK_CONFIRM_FILE", referencedColumnName = "ID", nullable = false)
	public CofinConfirmFile getCofinConfirmFile() {
		return cofinConfirmFile;
	}

	@Column(name = "RECORD_TYPE")
	public String getRecordType() {
		return recordType;
	}

	@Column(name = "DATA_SOURCE")
	public String getDataSpource() {
		return dataSpource;
	}

	@Column(name = "DATA_INDEX")
	public int getDataIndex() {
		return dataIndex;
	}

	@Column(name = "DATA_TYPE")
	public String getDataType() {
		return dataType;
	}

	@Column(name = "SPLIT_TYPE")
	public String getSplitType() {
		return splitType;
	}

	@Column(name = "SPLIT_DESC")
	public String getSplitDesc() {
		return splitDesc;
	}

	@Column(name = "COLUMN_DESTINATION")
	public String getColumnDestination() {
		return columnDestination;
	}

	@Column(name = "DEFAULT_VALUE")
	public String getDefaultData() {
		return defaultData;
	}

	@Column(name = "SPLIT_INDEX")
	public int getSplitIndex() {
		return splitIndex;
	}

	
	
	@Column(name = "VALUE_PATTERN")
	public String getValuePattern() {
		return valuePattern;
	}

	@Column(name = "COMPARE_VALUE")
	public String getCompareValue() {
		return compareValue;
	}

	
	@Column(name = "SUBSTRING")
	public String getSubString() {
		return subString;
	}

	
	
	@Column(name = "REPLACE")
	public String getReplace() {
		return replace;
	}

	
	
	@Column(name = "CONTAIN")
	public String getContain() {
		return contain;
	}
	
	
	

	public void setContain(String contain) {
		this.contain = contain;
	}

	public void setReplace(String replace) {
		this.replace = replace;
	}

	public void setSubString(String subString) {
		this.subString = subString;
	}

	public void setValuePattern(String valuePattern) {
		this.valuePattern = valuePattern;
	}

	public void setCompareValue(String compareValue) {
		this.compareValue = compareValue;
	}

	public void setSplitIndex(int splitIndex) {
		this.splitIndex = splitIndex;
	}

	public void setCofinConfirmFile(CofinConfirmFile cofinConfirmFile) {
		this.cofinConfirmFile = cofinConfirmFile;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public void setDataSpource(String dataSpource) {
		this.dataSpource = dataSpource;
	}

	public void setDataIndex(int dataIndex) {
		this.dataIndex = dataIndex;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public void setSplitType(String splitType) {
		this.splitType = splitType;
	}

	public void setSplitDesc(String splitDesc) {
		this.splitDesc = splitDesc;
	}

	public void setColumnDestination(String columnDestination) {
		this.columnDestination = columnDestination;
	}

	public void setDefaultData(String defaultData) {
		this.defaultData = defaultData;
	}

}
