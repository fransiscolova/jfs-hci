package id.co.homecredit.jfs.cofin.core.model.upload;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Entity class for {@code COFIN_FILE_UPLOAD}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_CONFIRM_PROCESS)
public class CofinConfirmProcess extends IdEntity {
    CofinProcessType cofinProcessType;
    Partner partner;
    String processCode;
    String name;
    String descritption;
    
    @AuditTransient
    List<CofinConfirmFile> cofinConfirmFile;
    
    
    @OneToMany(mappedBy="cofinConfirmProcess")
    public List<CofinConfirmFile> getCofinConfirmFile() {
		return cofinConfirmFile;
	}

	@ManyToOne
    @JoinColumn(name="FK_PROCESS_TYPE",referencedColumnName="ID",nullable=false)
	public CofinProcessType getCofinProcessType() {
		return cofinProcessType;
	}
    
    @ManyToOne
    @JoinColumn(name="FK_PARTNER",referencedColumnName="ID",nullable=false)
	public Partner getPartner() {
		return partner;
	}
    
    
    @Column(name="PROCESS_TYPE_CODE")
	public String getProcessCode() {
		return processCode;
	}
    
    @Column(name="NAME")
	public String getName() {
		return name;
	}
    
    @Column(name="Description")
	public String getDescritption() {
		return descritption;
	}
	public void setCofinProcessType(CofinProcessType cofinProcessType) {
		this.cofinProcessType = cofinProcessType;
	}
	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDescritption(String descritption) {
		this.descritption = descritption;
	}
    

	public void setCofinConfirmFile(List<CofinConfirmFile> cofinConfirmFile) {
		this.cofinConfirmFile = cofinConfirmFile;
	}

  
}
