package id.co.homecredit.jfs.cofin.core.model.upload;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import id.co.homecredit.jfs.cofin.common.model.IdEntity;
import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Entity class for {@code COFIN_FILE_UPLOAD}.
 *
 * @author fransisco.situmorang
 *
 */
@Entity
@Table(name = CoreTableName.COFIN_PROCESS_TYPE)
public class CofinProcessType extends IdEntity {

	private CofinProcessCategory cofinProcessCategory;
	private String code;
	private String period;
	private String name;
	private String description;

	@AuditTransient
	List<CofinConfirmProcess> cofinConfirmProcess;

	@OneToMany(mappedBy = "cofinProcessType")
	public List<CofinConfirmProcess> getCofinConfirmProcess() {
		return cofinConfirmProcess;
	}

	@ManyToOne
	@JoinColumn(name = "FK_PROCESS_CATEGORY", referencedColumnName = "ID", nullable = false)
	public CofinProcessCategory getCofinProcessCategory() {
		return cofinProcessCategory;
	}

	public String getCode() {
		return code;
	}

	public String getPeriod() {
		return period;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setCofinProcessCategory(CofinProcessCategory cofinProcessCategory) {
		this.cofinProcessCategory = cofinProcessCategory;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCofinConfirmProcess(List<CofinConfirmProcess> cofinConfirmProcess) {
		this.cofinConfirmProcess = cofinConfirmProcess;
	}

}
