package id.co.homecredit.jfs.cofin.core.model;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreTableName;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant.CoreColumnName;

/**
 * Entity Class For Town Master
 * 
 * @author denny.afrizal01
 *
 */
@Entity
@Table(name=CoreTableName.VW_COFIN_TOWN)
public class vwCofinTown {
	private String townCode;
	private String townName;
	private String subdistrictCode;
	private String subdistrictName;
	private String districtCode;
	private String districtName;
	@Id
	@Column(name=CoreColumnName.TOWN_CODE)
	public String getTownCode(){
		return townCode;
	}
	public void setTownCode(String townCode){
		this.townCode=townCode;
	}
	@Column(name=CoreColumnName.TOWN_NAME)
	public String getTownName(){
		return townName;
	}
	public void setTownName(String townName){
		this.townName=townName;
	}
	@Column(name=CoreColumnName.SUBDISTRICT_CODE)
	public String getSubdistrictCode(){
		return subdistrictCode;
	}
	public void setSubdistrictCode(String subdistrictCode){
		this.subdistrictCode=subdistrictCode;
	}
	@Column(name=CoreColumnName.SUBDISTRICT_NAME)
	public String getSubdistrictName(){
		return subdistrictName;
	}
	public void setSubdistrictName(String subdistrictName){
		this.subdistrictName=subdistrictName;
	}
	@Column(name=CoreColumnName.DISTRICT_CODE)
	public String getDistrictCode(){
		return districtCode;
	}
	public void setDistrictCode(String districtCode){
		this.districtCode=districtCode;
	}
	@Column(name=CoreColumnName.DISTRICT_NAME)
	public String getDistrictName(){
		return districtName;
	}
	public void setDistrictName(String districtName){
		this.districtName=districtName;
	}
	@Override
	public String toString(){
		StringBuilder json = new StringBuilder(1000);
		json.append("vwCofinTown [toenCode=");
		json.append(townCode);
		json.append(",townName=");
		json.append(townName);
		json.append(",subdistrictCode=");
		json.append(subdistrictCode);
		json.append(",subdistrictName=");
		json.append(subdistrictName);
		json.append(",districtCode=");
		json.append(districtCode);
		json.append(",districtName=");
		json.append(districtName);
		json.append("]");
		return json.toString();
	}
}
