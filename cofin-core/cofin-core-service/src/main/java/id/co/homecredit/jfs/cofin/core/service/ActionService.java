package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Role;

/**
 * Service interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface ActionService {

    /**
     * Get all action.
     *
     * included deleted
     *
     * @return list actions
     */
    public List<Action> getAllAction(boolean includedDeleted);

    /**
     * Get action by name.
     *
     * @param name
     * @return action
     */
    public Action getByName(String name);

    /**
     * save action
     *
     * @param action
     * @return action
     */
    public Action getByRoleCode(Action action);

    /**
     * Get action by role_code.
     *
     * @param role_code
     * @return action
     */
    public Action saveAction(Action roleCode);
    
    /**
     * Get Action Like Name
     * 
     * @param name
     * @return action
     */
    public List<Action> getActionLikeName(String name);

}
