package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.AuditLog;

/**
 * Service interface class for {@link AuditLog}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface ActivityLogService {

    /**
     * Delete activity log.
     *
     * @param activityLog
     */
    public void deleteActivityLog(AuditLog activityLog);

    /**
     * Get activity log by activity log id.
     *
     * @param activityLogId
     * @return activityLog
     */
    public AuditLog getActivityLogById(String activityLogId);

    /**
     * Get all activity logs by include deleted.
     *
     * @param includeDeleted
     * @return activityLogs
     */
    public List<AuditLog> getAllActivityLogsByIncludeDeleted(Boolean includeDeleted);

    /**
     * Save activity log.
     *
     * @param activityLog
     * @return activityLog
     */
    public AuditLog saveActivityLog(AuditLog activityLog);
}
