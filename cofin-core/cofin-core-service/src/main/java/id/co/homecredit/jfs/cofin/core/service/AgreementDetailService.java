package id.co.homecredit.jfs.cofin.core.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.AgreementDetail;

/**
 * Service interface class for {@link AgreementDetail}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface AgreementDetailService {

    /**
     * Delete agreement detail by agreement detail id.
     *
     * @param agreementDetailId
     */
    public void deleteAgreementDetailById(String agreementDetailId);

    /**
     * Get active agreement detail by agreement id and date.
     *
     * @param agreementId
     * @param date
     * @return agreementDetail
     */
    public AgreementDetail getActiveAgreementDetailByAgreementIdAndDate(String agreementId,
            Date date);

    /**
     * get agreement detail by agreement detail id.
     *
     * @param agreementDetailId
     */
    public AgreementDetail getAgreementDetailById(String agreementDetailId);

    /**
     * Get all agreement detail by agreement id.
     *
     * @param agreementId
     * @return agreementDetails
     */
    public List<AgreementDetail> getAllAgreementDetailByAgreementId(String agreementId);

    /**
     * Save agreement detail.
     *
     * @param agreementDetail
     * @return agreementDetail
     */
    public AgreementDetail saveAgreementDetail(AgreementDetail agreementDetail);

    /**
     * Save all agreement detail.
     *
     * @param List<agreementDetail>
     * @return List<agreementDetail>
     */
    public List<AgreementDetail> saveAgreementDetails(List<AgreementDetail> agreementDetail);

}
