package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Agreement;

/**
 * Service interface class for {@link Agreement}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface AgreementService {

    /**
     * Delete agreement by id.
     *
     * @param agreementId
     */
    public void deleteAgreement(String agreementId);

    /**
     * ger agreement.
     *
     * @param agreementId
     * @return agreement
     */
    public Agreement getAgreementById(String agreementId);

    /**
     * Get agreement by id alias.
     *
     * @param idAlias
     * @return agreement
     */
    public Agreement getAgreementByIdAlias(String idAlias);

    /**
     * Get agreement by agreement id with fetched all agreement details.
     *
     * @param agreementId
     * @return agreement
     */
    public Agreement getAgreementWithFetchedDetailsById(String agreementId);

    /**
     * Get agreement by agreement id with fetched all producvt mappings.
     *
     * @param agreementId
     * @return agreement
     */
    public Agreement getAgreementWithFetchedProductMappingsById(String agreementId);

    /**
     * Get all active agreements by include deleted.
     *
     * @param includeDeleted
     * @return agreements
     */
    public List<Agreement> getAllAgreements(Boolean includeDeleted);

    /**
     * Get all active agreements by partner code.
     *
     * @param partnerCode
     * @return agreements
     */
    public List<Agreement> getAllAgreementsByPartnerCode(String partnerCode);

    /**
     * Save agreement.
     *
     * @param agreement
     * @return agreement
     */
    public Agreement saveAgreement(Agreement agreement);
    
}
