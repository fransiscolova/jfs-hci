package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.AgreementType;

/**
 * Service Interface For {@link COFIN_AGREEMENT_TYPE}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface AgreementTypeService {

	/**
	 * Get Agreement Type
	 * 
	 * @param ""
	 * @return agreementType
	 */
	public List<AgreementType> getAllAgreementType();
	
	/**
	 * Get Agreement Type By ID
	 * 
	 * @param id
	 * @return id
	 */
	public AgreementType getAgreementTypeById(String id);
	
}
