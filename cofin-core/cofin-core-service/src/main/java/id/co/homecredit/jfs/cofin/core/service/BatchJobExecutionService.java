package id.co.homecredit.jfs.cofin.core.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.BatchJob;
import id.co.homecredit.jfs.cofin.core.model.BatchJobExecution;

/**
 * Service interface class for {@link BatchJob}.
 *
 * @author fransisco.situmroang
 *
 */
@Transactional
public interface BatchJobExecutionService {

    /**
     * Get batch job Execution.
     *
     * @param batchJobInstanceId
     *
     * @return list batchJobExecutionId
     */
    public List<BatchJobExecution> getBatchJobExecutionByJobInstanceId(String jobName);

    /**
     * Get Batch_JOB_EXECUTION
     *
     * @param jobinstanceId,createDateFrom,createDateFrom
     * @return batchJobExecutionId
     */
    public List<BatchJobExecution> getBatchJobExecutionByJobInstanceId(String jobname,
            Date createDateFrom, Date createDateTo);

}
