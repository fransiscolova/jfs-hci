package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.BatchJob;
import id.co.homecredit.jfs.cofin.core.model.BatchJobInstance;

/**
 * Service interface class for {@link BatchJob}.
 *
 * @author fransisco.situmroang
 *
 */
@Transactional
public interface BatchJobInstanceService {

    public BatchJobInstance getAllBatchInstance();

    /**
     * Get batch job.
     *
     * batchJobIntsanceName
     *
     * @return batchJobInstance
     */
    public List<BatchJobInstance> getBatchInstanceByName(String byname);
}
