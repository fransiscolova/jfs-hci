package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.BatchJob;

/**
 * Service interface class for {@link BatchJob}.
 *
 * @author fransisco.situmroang
 *
 */
@Transactional
public interface BatchJobService {

    /**
     * Get all batch job.
     *
     * included deleted
     *
     * @return list batchJob
     */
    public List<BatchJob> getAllBatchJob(boolean includedDeleted);

    /**
     * Get batch job.
     *
     * batchJobId
     *
     * @return batchJob
     */
    public BatchJob getBatchJobById(String id);

    /**
     * save batch job.
     *
     * batchJobId
     *
     * @return batchJob
     */
    public BatchJob save(BatchJob batchJob);

}
