package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.BatchJob;
import id.co.homecredit.jfs.cofin.core.model.BatchStepExecution;

/**
 * Service interface class for {@link BatchJob}.
 *
 * @author fransisco.situmroang
 *
 */
@Transactional
public interface BatchStepExecutionService {

    /**
     * Get batch step Execution.
     *
     * @param jobExecutionId
     *
     * @return list batchStepExecution
     */
    public List<BatchStepExecution> getBatchStepExecutionByJobExecutionId(Long jobExecutionId);

}
