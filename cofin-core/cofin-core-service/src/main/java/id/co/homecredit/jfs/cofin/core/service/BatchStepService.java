package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.BatchStep;

/*
 * Interface to link BatchStep
 * @author fransisco.situmorang
 */
@Transactional
public interface BatchStepService {

    /*
     * get List BatchStep
     * @param jobId
     * @return list BatchStep
     */

    public List<BatchStep> getBatchStepByJobId(String jobsId);

}
