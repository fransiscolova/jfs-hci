package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.common.model.DropDownListItem;
import id.co.homecredit.jfs.cofin.core.model.CofinConfig;
import id.co.homecredit.jfs.cofin.core.model.Role;

/**
 * Service interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CofinConfigService {

    /**
     * Get ConfinConfig
     *
     * @param category
     * @return cofinconfig
     */
    public List<CofinConfig> getByCategory(String category);

    /**
     * Get cofinconfig by category,key1.
     *
     * @param roleCode,key1
     * @return cofinconfig
     */
    public List<CofinConfig> getByCategoryKey1(String roleCode,String key1);
    
    /**
     * Get Config By Category,Key1,Key2
     * 
     * @param category,key1,key2
     * @return cofinConfig
     */
    public String getByCategoryKey2(String category,String key01,String key02);
    
    /**
     * Get JFS Status Value
     * 
     * @param ""
     * @return DropDownListItem
     */
    public List<DropDownListItem> getJfsContractStatus();

    /**
     * Get Config By Value
     *
     * @param value
     * @return value
     */
    public CofinConfig getDescriptionByValue(String value);

}
