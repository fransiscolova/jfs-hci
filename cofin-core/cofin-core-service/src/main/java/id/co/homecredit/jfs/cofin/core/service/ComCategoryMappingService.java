package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.ComCategoryMapping;

/**
 * Service interface class for {@link ComCategoryMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface ComCategoryMappingService {

}
