package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.ComTypeMapping;

/**
 * Service interface class for {@link ComTypeMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface ComTypeMappingService {

}
