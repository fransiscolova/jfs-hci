package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.CommodityCategory;

/**
 * Service interface class for {@link CommodityCategory}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CommodityCategoryService {

    /**
     * Get all commodity category by include deleted.
     *
     * @param includeDeleted
     * @return commodityCategories
     */
    public List<CommodityCategory> getAllCommodityCategoriesByIncludeDeleted(Boolean includeDeleted);

    /**
     * Get commodity category by commodity category code.
     *
     * @param categoryCode
     * @return commodityCategory
     */
    public CommodityCategory getCommodityCategoryByCode(String categoryCode);

    /**
     * Save commodity category.
     *
     * @param commodityCategory
     * @return commodityCategory
     */
    public CommodityCategory saveCommodityCategory(CommodityCategory commodityCategory);
}
