package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.CommodityType;

/**
 * Service interface class for {@link CommodityType}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CommodityTypeService {

    /**
     * Get all commodity types by include deleted.
     *
     * @param includeDeleted
     * @return commodityTypes
     */
    public List<CommodityType> getAllCommodityTypesByIncludeDeleted(Boolean includeDeleted);

    /**
     * Get commodity type by commodity type code.
     *
     * @param typeCode
     * @return commodityType
     */
    public CommodityType getCommodityTypeByCode(String typeCode);

    /**
     * Save commodity type.
     *
     * @param commodityType
     * @return commodityType
     */
    public CommodityType saveCommodityType(CommodityType commodityType);

}
