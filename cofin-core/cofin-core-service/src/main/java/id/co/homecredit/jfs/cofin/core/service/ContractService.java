package id.co.homecredit.jfs.cofin.core.service;

import java.util.Date;
import java.util.List;
import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.common.model.enumeration.ContractStatusEnum;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.Contract;
import id.co.homecredit.jfs.cofin.core.model.Partner;

/**
 * Service interface class for {@link Contract}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface ContractService {

    /**
     * Delete contract.
     *
     * @param contract
     */
    public void deleteContract(Contract contract);

    /**
     * Delete contract by contract number.
     *
     * @param contractNumber
     */
    public void deleteContract(String contractNumber);

    /**
     * Get all contracts by include deleted.
     *
     * @param includeDeleted
     * @return contracts
     */
    public List<Contract> getAllContractsByInlcludeDeleted(Boolean includeDeleted);

    /**
     * Get all contracts .
     *
     * @param contractCode,status,partner,agreement
     * @return contracts
     * @throws Exception
     */
    public List<Contract> getAllContractsInquiry(String contractCode, ContractStatusEnum status,
            Partner partner, Agreement agreement, JSONObject dateFilter) throws Exception;

    /**
     * Get contract by contract number.
     *
     * @param contractNumber
     * @return
     */
    public Contract getContract(String contractNumber);

    /**
     * Save contract.
     *
     * @param contract
     * @return contract
     */
    public Contract saveContract(Contract contract);
    
}
