package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;

/**
 * Service interface class for {@link CriteriaEligibility}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CriteriaEligibilityService {

    /**
     * Get all criteria eligibilities by agreement id.
     *
     * @param agreementId
     * @return criteriaEligibilities
     */
    public List<CriteriaEligibility> getAllCriteriaEligibilitiesByAgreementId(String agreementId);

    /**
     * Save criteria eligibility.
     *
     * @param criteriaEligibility
     * @return criteriaEligibility
     */
    public CriteriaEligibility saveCriteriaEligibility(CriteriaEligibility criteriaEligibility);
}
