package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.dto.FinalCriteriaEligibilityDto;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;

/**
 * Service interface class for {@link CriteriaMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CriteriaMappingService {

    /**
     * Delete criteria mapping by criteria mapping id.
     *
     * @param criteriaMappingId
     */
    public void deleteCriteriaMappingById(String criteriaMappingId);

    public String finalListToSql(FinalCriteriaEligibilityDto finalList);

    public FinalCriteriaEligibilityDto flattingCriteria(List<CriteriaMapping> criteriaMappings);

    /**
     * Get all criteria mappings by agreement id.
     *
     * @param agreementId
     * @return criteriaMappings
     */
    public List<CriteriaMapping> getAllCriteriaMappingsByAgreementId(String agreementId);

    /**
     * Get all criteria mappings by criteria and date.
     *
     * @param criteiraEnum
     * @return criteriaMappings
     */
    public List<CriteriaMapping> getAllCriteriaMappingsByCriteria(CriteriaEnum criteiraEnum);

    /**
     * Get criteria mapping by id.
     *
     * @param criteriaMappingId
     * @return criteriaMapping
     */
    public CriteriaMapping getCriteriaMappingById(String criteriaMappingId);

    /**
     * Save criteria mapping.
     *
     * @param criteriaMapping
     * @return criteriaMapping
     */
    public CriteriaMapping saveCriteriaMapping(CriteriaMapping criteriaMapping);
}
