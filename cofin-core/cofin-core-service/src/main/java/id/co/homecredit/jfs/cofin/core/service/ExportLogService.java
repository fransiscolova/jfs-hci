package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.ExportLog;

/**
 * service interface class for {@link exportlog}
 *
 * @author f.situmorang01
 *
 */

@Transactional
public interface ExportLogService {
    /**
     * Get ExportLog
     *
     * @param stepExecutionId
     *
     * @return list ExportLog
     */

    public List<ExportLog> getExportLogByStepExecutionId(Long stepExecutionId);
    
    public ExportLog get(String id);
}
