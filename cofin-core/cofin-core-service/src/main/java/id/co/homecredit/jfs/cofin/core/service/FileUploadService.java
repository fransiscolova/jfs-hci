package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Role;

/**
 * Service interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface FileUploadService {


    /**
     * save file upload.
     *
     * @param fileUpload
     * @return fileUpload
     */
    public FileUpload saveFileUpload(FileUpload FileUpload);
    
    
    public List<FileUpload> getFileUpload();
    
    
    public FileUpload getFileUpload(String id);
    
    public List<FileUpload> getFileUpload(String processDate,String confirmFile);
    
    
    public Boolean deleteById(String id);
}
