package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.ImportTempFile;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Service interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface ImportTempFileService {

  

    /**
     * save file hasi;l pemeriksaaan
     *
     * @param importTempFile
     * @return importTempFiles
     */
    public List<ImportTempFile> saveHasilPemeriksaan(List<ImportTempFile> importTempFile);
    
    

    /**
     * save hasil konfirmasi
     *
     * @param importTempFile
     * @return importTempFile
     */
    public String saveHasilPemeriksaan(ImportTempFile importTempFile);
    
    
    /**
     * run procedure file
     */
    public void runProcedurePermata(String query);
    
    
}
