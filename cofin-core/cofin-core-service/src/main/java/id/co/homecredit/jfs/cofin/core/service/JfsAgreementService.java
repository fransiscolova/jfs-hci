package id.co.homecredit.jfs.cofin.core.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsAgreement;

/**
 * Service Interface Class For {@link JfsAgreement}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsAgreementService{
	
	/**
	 * Execute Select Query
	 * 
	 * @param query
	 * @return query
	 */
	public List<JfsAgreement> getAllAgreement();
	
	/**
	 * Execute Select Query
	 * 
	 * @param query
	 * @return query
	 */
	public List<Map<String,Object>> getAllAgreementWithQuery();
	
	/**
	 * Get Agreement By Agreement ID
	 * 
	 * @apram id
	 * @return id
	 * @throws ParseException 
	 */
	public JfsAgreement getJfsAgreementById(Long agreementId);
	
	/**
	 * Save Jfs Agreement Value
	 * 
	 * @param object
	 * @return object
	 */
	public Object saveAgreement(Object JfsAgreement);
	
	/**
     * Save JFS Agreement.
     *
     * @param jfsAgreement
     * @return jfsAgreement
     */
    public JfsAgreement agreementSave(JfsAgreement jfsAgreement);
    
    /**
     * Update Value Of JFS Agreement.
     * 
     * @param query
     * @return query
     */
    public boolean updateValueOfJfsAgreementWithQuery(String query);
    
    /**
     * Update Value Of JFS Agreement
     * 
     * @param jfsAgreement
     * @return jfsAgreement
     */
    public JfsAgreement agreementUpdate(JfsAgreement jfsAgreement);
    
    /**
     * Update Value Of JFS Agreement Using Query String
     * 
     * @param jfsAgreement
     * @return JfsAgreement
     */
    public boolean agreementUpdateWithQuery(JfsAgreement jfsAgreement,String validFrom,String validTo);
    
    /**
     * Get JFS Agreement Value By Partner ID
     * 
     * @param partnerId
     * @return jfsAgreement
     */
    public List<JfsAgreement> getJfsAgreementByPartnerId(String partnerId)throws Exception;
    
    /**
     * Get Max Value Of JFS Agreement
     * 
     * @param ""
     * @return agreementId
     */
    public String getMaxIdAgreement();

}
