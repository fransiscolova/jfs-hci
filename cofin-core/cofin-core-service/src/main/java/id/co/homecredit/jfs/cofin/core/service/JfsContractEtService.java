package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsContractEt;

/**
 * Service Interface Class For {@link JfsContractEt}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsContractEtService{
	
	/**
	 * Get Value Of JFS Contract ET By Contract Number
	 * 
	 * @Param contractNumber
	 * @return contractNumber
	 */
	public List<JfsContractEt> getContractEtByContractNumber(String contractNumber);
	
}
