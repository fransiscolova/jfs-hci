package id.co.homecredit.jfs.cofin.core.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.common.model.DropDownListItem;
import id.co.homecredit.jfs.cofin.core.model.JfsContract;

/**
 * Service Interface Class For {@link JfsContract}.
 *
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsContractService{

    /**
     * Get Value Of JFS Contract
     * 
     * @param contractNumber
     * @return jfaContract
     */
    public List<JfsContract> getJfsContractWithoutDate(String contractNumber,String status,Long idAgreement);
    
    /**
     * Get Value Of JFS Contract With Date Criteria
     *
     * @param contractNumber
     * @return jfsContract
     */
    public List<JfsContract> getJfsContractWithDate(String contractNumber,String status,Long idAgreement,Date exportDate);

    /**
     * Get Value Of JFS Agreement Between Export Date
     *
     * @param contractNumber
     * @return jfsAgreement
     */
    public List<JfsContract> getJfsContractBetweenDate(String contractNumber,String status,Long idAgreement,Date dateFrom,Date dateTo);
    
    /**
     * Get Header Value Of JFS Contract
     *
     * @param contractNumber
     * @value jfsContract
     */
    public JfsContract getHeaderJfsContract(String contractNumber);

//    /**
//     * Get Detail JfsContract
//     *
//     * @param incomingPaymentId
//     * @value jfsContract
//     */
//    public JfsContract getDetailJfsContract(String textContractNumber);

}