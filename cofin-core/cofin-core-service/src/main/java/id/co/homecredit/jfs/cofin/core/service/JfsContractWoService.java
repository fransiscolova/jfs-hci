package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsContractWo;

/**
 * Service Interface Class For {@link JfsContractWo}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsContractWoService{

	/**
	 * Get Value Of WO Contract By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 * @throws ParseException 
	 */
	public JfsContractWo getContractWoByContractNumber(String contractNumber);
	
}
