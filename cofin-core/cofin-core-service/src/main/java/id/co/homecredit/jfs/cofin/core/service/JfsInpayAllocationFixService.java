package id.co.homecredit.jfs.cofin.core.service;

import java.util.Date;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsInpayAllocationFix;

/**
 * Service Interface Class For {@link JfsInpayAllocationFix}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsInpayAllocationFixService{

	/**
	 * Get Allocation Amount
	 * 
	 * @param incomingPaymentId
	 * @return incomingPaymentId
	 */
	public JfsInpayAllocationFix getAllocationAmountValue(Long incomingPaymentId,Date dateInstalment);
	
}
