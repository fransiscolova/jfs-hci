package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Service Class For {@link JfsOfiPayment}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsOfiPaymentService{

	/**
	 * Process Normal Payment
	 * 
	 * @param date
	 * @return date
	 * @throws ParseException 
	 */
	public boolean processNormalPayment(String paymentDate,String xpaymentDate)throws Exception;
	
	/**
	 * Save OFI Data
	 * 
	 * @param date
	 * @return date
	 */
	public boolean saveData(Object object);
	
}
