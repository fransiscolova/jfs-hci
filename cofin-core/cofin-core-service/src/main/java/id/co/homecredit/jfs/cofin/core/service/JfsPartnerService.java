package id.co.homecredit.jfs.cofin.core.service;

import java.util.Map;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsPartner;

/**
 * Service Interface Class For {@link JfsPartner}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsPartnerService{
	
	/**
	 * Get Name Of Partner
	 * 
	 * @param partnerId
	 * @return name
	 */
	public String getPartnerName(String partnerId);
	
	/**
	 * Get All Partner
	 * 
	 * @return partner
	 */
	public List<JfsPartner> getAllPartner();
	
	/**
	 * Search JFS Partner Value By Name
	 * 
	 * @param id
	 * @return JfsPartner
	 */
	public JfsPartner getPartnerValueById(String id);
	
	/**
	 * Save Value Of JFS Partner
	 * 
	 * @param jfsPartner
	 * @return jfsPartner
	 */
	public boolean partnerSave(JfsPartner jfsPartner);
	
	/**
	 * Update Value Of JFS Partner Using Query
	 * 
	 * @param query
	 * @return query
	 */
	public boolean updateValueOfJfsPartnerWithQuery(String query);
	
	/**
	 * Update Value Of JFS Partner
	 * 
	 * @param jfsPartner
	 * @return jfsPartner
	 */
	public boolean partnerUpdate(JfsPartner jfsPartner);
	
	/**
	 * Update Partner Using Query
	 * 
	 * @param jfsPartner
	 * @return query
	 */
	public boolean partnerUpdateWithQuery(JfsPartner jfsPartner);
	
}
