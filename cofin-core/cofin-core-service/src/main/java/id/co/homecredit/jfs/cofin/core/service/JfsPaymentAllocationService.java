package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentAllocation;

/**
 * Service Interface Class For {@link JFS_PAYMENT_ALLOCATION}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsPaymentAllocationService {
	
	/**
	 * Get Payment Allocation By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public List<JfsPaymentAllocation> getAllocationPartnerByContractNumber(String contractNumber);
	
}
