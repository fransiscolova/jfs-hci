package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import java.util.Date;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentBsl;

/**
 * Service Interface Class For {@link JfsPaymentBsl}
 * 
 * @author denny.afrizal01
 *
 */

@Transactional
public interface JfsPaymentBslService{
	
	/**
	 * Get BSL Data By Date
	 * 
	 * @param paymentDate
	 * @return paymentDate
	 */
	public List<JfsPaymentBsl> getBslPaymentByDate(Date paymentDate);

	/**
	 * Get BSL Data By Contract Number
	 *
	 * @param textContractNumber
	 * @return textContractNumber
	 */
	public List<JfsPaymentBsl> getBslPaymentByContractNumber(String textContractNumber);

	/**
	 * Get Payment BSL Between Date
	 *
	 * @param dateFrom
	 * @return dateTo
	 */
	public List<JfsPaymentBsl> getBslPaymentBetweenDate(Date dateFrom,Date dateTo);
	
}
