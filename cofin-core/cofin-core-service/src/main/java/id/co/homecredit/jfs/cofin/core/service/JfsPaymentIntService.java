package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentInt;

/**
 * Service Interface Class For {@link JfsPaymentInt}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsPaymentIntService{

	/**
	 * Get JFS Payment Int Value By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public List<JfsPaymentInt> getPaymentIntByContractNumber(String contractNumber);

    /**
     * Get JFS Payment Int By Contract Number And Incoming Payment Id
     *
     * @param incomingPaymentId
     * @return textContractNumber
     */
    public JfsPaymentInt getByContractNumberAndIncomingPayment(String textContractNumber,Long incomingPaymentId);
	
}
