package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsProductHci;


/**
 * Service interface class for {@link JfsProductHci}.
 *
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsProductHciService{
	
	/**
     * Execute Select Query
     * 
     * @param script
     * @return script
     */
	public List<Map<String,Object>> executeSelectQuery(String script);
	
	/**
     * Save JFS Product HCI
     * 
     * @param product
     * @return product
     */
	public JfsProductHci saveJfsProductHci(JfsProductHci product);
	
	/**
     * Update JFS Product HCI
     * 
     * @param product
     * @return product
     */
	public JfsProductHci updateJfsProductHci(JfsProductHci product);
	
	/**
     * Update JFS Product HCI Using Query String
     * 
     * @param product
     * @return product
     */
	public boolean updateJfsProductHciWithQuery(String query);
	
	/**
	 * Get JFS Product HCI Value By ID
	 * 
	 * @param id
	 * @return id
	 */
	public JfsProductHci getJfsProductHciById(String id);
	
	/**
	 * Get All PRoduct
	 * 
	 * @param JfsProductHci
	 * @return JfsProductHci
	 */
	public List<JfsProductHci> getAllProductHci();
}
