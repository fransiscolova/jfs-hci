package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import java.text.ParseException;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsProductMapping;

/**
 * Service Interface Class For {@link JfsProductMapping}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsProductMappingService{
	
	/**
	 * Get All Product Mapping By Agreement ID
	 * 
	 * @param agreementId
	 * @return agreementId
	 * @throws ParseException
	 */
	public List<JfsProductMapping> getProductMappingByAgreementId(Integer agreementId)throws ParseException;
	
	/**
	 * Save Value Of JFS Product Mapping
	 * 
	 * @param jfsProductMapping
	 * @return jfsProductMapping
	 */
	public JfsProductMapping productMappingSave(JfsProductMapping jfsProductMapping);
	
	/**
	 * Get Product Mapping By ID
	 * 
	 * @param id
	 * @return id
	 */
	public JfsProductMapping getProductMappingById(String id)throws ParseException;
	
	/**
	 * Update Status Of JFS Product Mapping
	 *
	 * @param status
	 * @return jfsProductMapping
	 */
	public boolean jfsProductMappingUpdate(String status,String updatedBy, String updatedDate, String id);

}
