package id.co.homecredit.jfs.cofin.core.service;

import java.text.ParseException;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsProduct;

/**
 * Service Interface Class For {@link JfsProduct}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsProductService{

	/**
	 * Get Detail Product By Agreement ID
	 * 
	 * @param id
	 * @return id
	 * @throws ParseException 
	 */
	public List<JfsProduct> getProductByAgreementId(Long agreementId) throws ParseException;
	
	/**
	 * Save Value Of JFS Product
	 * 
	 * @param jfsProduct
	 * @return jfsProduct
	 */
	public JfsProduct productSave(JfsProduct jfsProduct);
	
	/**
	 * Update Valid Date Of JFS Product
	 * 
	 * @param id
	 * @return id
	 */
	public boolean updateValidDate(String id, String validFrom, String validTo);
	
	/**
	 * Get JFS Product By ID
	 * 
	 * @param id
	 * @return id
	 * @throws ParseException 
	 */
	public JfsProduct getProductById(String id) throws ParseException;
	
}
