package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsReconciliation;

/**
 * Service Interface Class For {@link JfsReconciliation}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsReconciliationService{
	
	/**
	 * Get Value Of JFS Reconcilistion By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public List<JfsReconciliation> getReconciliationByContractNumber(String contractNumber);
	
}
