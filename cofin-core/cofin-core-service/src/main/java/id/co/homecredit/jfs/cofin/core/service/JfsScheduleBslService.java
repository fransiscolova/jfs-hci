package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsScheduleBsl;

/**
 * Service Interface Class For {@link JfsScheduleBsl}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsScheduleBslService{

	/**
	 * Get Value Of Schedule From BSL By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public List<JfsScheduleBsl> getScheduleBslByContractNumber(String contractNumber);
	
}
