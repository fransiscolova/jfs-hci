package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.JfsSchedule;

/**
 * Service Interface Class For {@link JfsSchedule}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface JfsScheduleService{

	/**
	 * Get Value Of Jfs Schedule
	 *
	 * @param textContractNumber
	 */
	public JfsSchedule getScheduleJfs(String textContractNumber,Integer partIndex);
	
}
