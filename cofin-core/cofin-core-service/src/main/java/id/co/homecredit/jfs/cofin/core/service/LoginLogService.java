package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.LoginLog;

/**
 * Service interface class for {@link LoginLog}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface LoginLogService {

    /**
     * Get latest login log by username.
     *
     * @param username
     * @return loginLog
     */
    public LoginLog getLatestLoginLogByUsername(String username);

    /**
     * Save login log for login user by username.
     *
     * @param username
     */
    public void saveUserLogin(String username);

    /**
     * Save login log for logout user by username.
     *
     * @param username
     */
    public void saveUserLogout(String username);

}
