package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Partner;

/**
 * Service interface for {@link Partner}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface PartnerService {

    /**
     * Delete partner by partner id.
     *
     * @param partnerId
     */
    public void deletePartnerById(String partnerId);

    /**
     * Get all active partners by include deleted.
     *
     * @param includeDeleted
     * @return partners
     */
    public List<Partner> getAllPartners(Boolean includeDeleted);

    /**
     * Get partner by partner code.
     *
     * @param id
     * @return partner
     */
    public Partner getPartnerByCode(String partnerCode);

    /**
     * Get partner by partner id.
     *
     * @param partnerId
     * @return partner
     */
    public Partner getPartnerById(String partnerId);

    /**
     * Get partner by name.
     *
     * @param partnerName
     * @return partner
     */
    public Partner getPartnerByName(String name);

    /**
     * Save partner.
     *
     * @param partner
     * @return partner
     */
    public Partner savePartner(Partner partner);
}
