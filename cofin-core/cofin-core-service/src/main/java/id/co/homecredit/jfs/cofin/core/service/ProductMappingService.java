package id.co.homecredit.jfs.cofin.core.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.ProductMapping;

/**
 * Service interface class for {@link ProductMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface ProductMappingService {

    /**
     * Delete product mapping by product mapping id.
     *
     * @param productMappingId
     */
    public void deleteProductMappingById(String productMappingId);

    /**
     * Get all product mappings by agreement id and date.
     *
     * @param agreementId
     * @param date
     * @return productMappings
     */
    public List<ProductMapping> getAllProductMappingsByAgreementIdAndDate(String agreementId,
            Date date);

    /**
     * Get all product mappings by product code and date.
     *
     * @param productCode
     * @param date
     * @return productMappings
     */
    public List<ProductMapping> getAllProductMappingsByProductCodeAndDate(String productCode,
            Date date);

    /**
     * get product mapping by product mapping id.
     *
     * @param productMappingId
     */
    public ProductMapping getProductMappingById(String productMappingId);

    /**
     * Save product mapping.
     *
     * @param productMapping
     * @return productMapping
     */
    public ProductMapping saveProductMapping(ProductMapping productMapping);

}
