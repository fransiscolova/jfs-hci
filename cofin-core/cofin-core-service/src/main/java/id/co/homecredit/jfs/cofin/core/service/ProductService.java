package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Product;

/**
 * Service interface class for {@link Product}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface ProductService {

    /**
     * Delete product by code.
     *
     * @param productCode
     */
    public void deleteProductByCode(String productCode);

    /**
     * Get all products by include deleted.
     *
     * @param includeDeleted
     * @return products
     */
    public List<Product> getAllProductsByIncludeDeleted(Boolean includeDeleted);

    /**
     * Get product by product code.
     *
     * @param productCode
     * @return product
     */
    public Product getProductByCode(String productCode);

    /**
     * Save product.
     *
     * @param product
     * @return product
     */
    public Product saveProduct(Product product);

}
