package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.RoleAction;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Service interface class for {@link RoleAction}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface RoleActionService {

    /**
     * Get all role actions.
     *
     * @return roleActions
     */
    public List<RoleAction> getAll();

    /**
     * Get all role actions by role.
     *
     * @param role
     * @return roleActions
     */
    public List<RoleAction> getAllByRole(RoleEnum role);

    /**
     * Get role action by id.
     *
     * @param id
     * @return roleAction
     */
    public RoleAction getRoleActionById(String id);

    /**
     * Get all role actions by action.
     *
     * @param actionUrl
     * @return roleActions
     */
    public List<RoleAction> getRoleActionsByAction(String actionUrl);

    /**
     * Save role action.
     *
     * @param roleAction
     * @return roleAction
     */
    public RoleAction saveRoleAction(RoleAction roleAction);

}
