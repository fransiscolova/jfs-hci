package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import java.util.Date;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;

/**
 * Service interface class for {@link RoleMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface RoleMappingService {

    /**
     * Delete role mapping.
     *
     * @param roleMapping
     */
    public void deleteRoleMapping(RoleMapping roleMapping);

    /**
     * Get all role mapping.
     *
     * @param includeDeleted
     * @return roleMappings
     */
    public List<RoleMapping> getAllRoleMapping(Boolean includeDeleted);

    /**
     * Get all role mapping by username.
     *
     * @param username
     * @return roleMappings
     */
    public List<RoleMapping> getAllRoleMappingByUsername(String username);

    /**
     * get role mapping.
     *
     * @param id
     * @return roleMapping
     */
    public RoleMapping getRoleMappingById(String id);

    /**
     * Save role mapping.
     *
     * @param roleMapping
     * @return roleMapping
     */
    public RoleMapping saveRoleMapping(RoleMapping roleMapping);
    
    /**
     * Get All Valid Role Mapping By Username
     * 
     * @param username
     * @return username
     */
    public List<RoleMapping> getAllValidRoleMappingsByUsername(String username,Date validDate);
}
