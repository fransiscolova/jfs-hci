package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;
import java.text.ParseException;
import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;

/**
 * Service interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface RoleService {

    /**
     * Get all roles by include deleted.
     *
     * @param includeDeleted
     * @return roles
     */
    public List<Role> getAllRoles(Boolean includeDeleted);

    /**
     * Get role not used in mapping role
     *
     * @param colection
     * @return roles
     */
    public List<Role> getAllUnsedRole(List<Enum> col);

    /**
     * Get role
     *
     * @param role
     * @return role
     */
    public Role getRolesByRole(RoleEnum role);
    
    /**
     * Get List Roles
     * 
     * @param role
     * @return role
     */
    public List<Role> getListRoles(List<Enum> col);
    
    /**
     * Save Role.
     *
     * @param role
     * @return role
     */
    public Role saveRoleMaster(Role role);
    
    /**
     * Get Role Value By Role
     * 
     * @param role
     * @return role
     */
    public Role getRoleValueByRole(RoleEnum role)throws ParseException;
    
    /**
     * Status Update Role
     * 
     * @param role
     * @return role
     */
    public boolean statusUpdateRole(String role, String status, String updatedBy, String updatedDate, String version);
}
