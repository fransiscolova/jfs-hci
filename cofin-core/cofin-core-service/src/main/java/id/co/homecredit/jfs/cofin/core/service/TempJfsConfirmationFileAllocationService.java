package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationAllocation;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Service interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface TempJfsConfirmationFileAllocationService {

  

    /**
     * save confirmation file
     *
     * @param ConfirmationFile
     * @return ConfirmationFiles
     */
    public List<TempJfsConfirmationAllocation> saveConfirmation(List<TempJfsConfirmationAllocation> confirmationAllocation);
    
    

    /**
     * save confirmation file
     *
     * @param ConfirmationFile
     * @return ConfirmationFile
     */
    public String saveConfirmation(TempJfsConfirmationAllocation confirmationAllocation);
    
    
    /**
     * run procedure file
     */
    public void runProcedurePermata(String query);
    
    
}
