package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;

/**
 * Service interface class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface TempJfsConfirmationFileService {

  

    /**
     * save confirmation file
     *
     * @param ConfirmationFile
     * @return ConfirmationFiles
     */
    public List<TempJfsConfirmationFile> saveConfirmation(List<TempJfsConfirmationFile> ConfirmationFile);
    
    

    /**
     * save confirmation file
     *
     * @param ConfirmationFile
     * @return ConfirmationFile
     */
    public String saveConfirmation(TempJfsConfirmationFile ConfirmationFile);
    
    
    /**
     * run procedure file
     */
    public void runProcedurePermata(String query);
    
    
}
