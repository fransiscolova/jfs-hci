package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.User;

/**
 * Service interface class for {@link User}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface UserService {

    /**
     * Delete user.
     *
     * @param user
     */
    public void deleteUser(User user);

    /**
     * Get all unmapped users.
     *
     * @return users
     */
    public List<User> getAllUnmappedUsers();

    /**
     * Get all users by include deleted.
     *
     * @param includeDeleted
     * @return users
     */
    public List<User> getAllUsers(Boolean includeDeleted);

    /**
     * Get user
     *
     * @param username
     * @return user
     */
    public User getUserByUserName(String username);

    /**
     * Save user.
     *
     * @param user
     * @return user
     */
    public User saveUser(User user);
}
