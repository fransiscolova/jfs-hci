package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.transaction.annotation.Transactional;
import id.co.homecredit.jfs.cofin.core.model.VwCofinContractDetail;

/**
 * Service Interface Class For {@link VwCofinContractDetail}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface VwCofinContractDetailService{

	/**
	 * Get Contract Detail By Contract Number
	 * 
	 * @param contractNumber
	 * @return contractNumber
	 */
	public VwCofinContractDetail getDetailContractByContractNumber(String contractNumber);
	
}
