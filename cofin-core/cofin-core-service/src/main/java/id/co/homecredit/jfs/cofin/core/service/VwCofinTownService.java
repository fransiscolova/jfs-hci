package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Service Interface Class For {@link VwCofinTown}
 * 
 * @author denny.afrizal01
 *
 */
@Transactional
public interface VwCofinTownService{

	/**
	 * Get Description Of Town
	 * 
	 * @param townCode
	 * @return townCode
	 */
	public String getTownDescription(String townCode);
	
	/**
	 * Get Description Of Subdistrict
	 * 
	 * @param subdistrictCode
	 * @return subdistrictCode
	 */
	public String getSubdistrictDescription(String subdistrictCode);
	
	/**
	 * Get Description Of District
	 * 
	 * @Param districtCode
	 * @return districtCode
	 */
	public String getDistrictDescription(String districtCode);
	
}
