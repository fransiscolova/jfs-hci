package id.co.homecredit.jfs.cofin.core.service.audit;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.persistence.Id;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostInsertEventListener;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PostUpdateEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.common.model.annotation.AuditTransient;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.util.JSONUtil;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.ColumnName;
import id.co.homecredit.jfs.cofin.core.dao.ActivityLogDao;
import id.co.homecredit.jfs.cofin.core.model.AuditLog;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.ActivityEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;
import id.co.homecredit.jfs.cofin.core.service.security.UserLogin;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;
import net.sf.json.JSON;

/**
 * Event listener for every changes in entity, then insert it to {@link AuditLog}.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class AuditEventListener
        implements PostInsertEventListener, PostUpdateEventListener, PostDeleteEventListener {
    private static final Logger log = LogManager.getLogger(AuditEventListener.class);
    private static final long serialVersionUID = -7758068329392606817L;
    private static JSONUtil jsonUtil = new JSONUtil();

    @Autowired
    private ActivityLogDao activityLogDao;
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Convert entity object to JSON.
     *
     * @param object
     * @return json
     */
    @SuppressWarnings("static-access")
    private JSON convertToJSON(Object object) {
        log.debug("Convert To JSON {}", object.toString());
        return jsonUtil.convertToJSON(object);
    }

    /**
     * Get deleted field value from entity.
     *
     * @param clazz
     * @param entity
     * @return yesNoEnum
     */
    private YesNoEnum getDeletedFieldValue(Class<?> clazz, Object entity) {
        YesNoEnum deletedFieldValue = null;
        Class<?> superClass = clazz.getSuperclass();
        try {
            Field deletedField = superClass.getDeclaredField(ColumnName.DELETED.toLowerCase());
            deletedField.setAccessible(true);
            deletedFieldValue = (YesNoEnum) deletedField.get(entity);
        } catch (Exception e) {
        	log.error("Deleted Field Exception : {}", e.getLocalizedMessage());
        	log.error(e.getStackTrace()[0]);
        	try{
        		Field deletedField = superClass.getDeclaredField(ColumnName.IS_DELETE.toLowerCase());
                deletedField.setAccessible(true);
                deletedFieldValue = (YesNoEnum) deletedField.get(entity);
        	}catch(Exception g){
        		log.error("Deleted Field Exception : {}", g.getLocalizedMessage());
                log.error(g.getStackTrace()[0]);
        	}
        }
        if (deletedFieldValue == null) {
            deletedFieldValue = getDeletedFieldValue(superClass, entity);
        }
        log.debug("Returned Deleted Value {}", deletedFieldValue);
        return deletedFieldValue;
    }

    /**
     * Get field value from entity which has {@link Id} annotation on its getter method.
     *
     * @param clazz
     * @param entity
     * @return string
     */
    private String getIdFieldValueFromGetterMethod(Class<?> clazz, Object entity) {
        String idFieldValue = null;
        for (Method dm : clazz.getDeclaredMethods()) {
            for (Annotation annotation : dm.getAnnotations()) {
                if (annotation.annotationType().equals(Id.class)) {
                    String fieldId = dm.getName().substring(3);
                    fieldId = fieldId.substring(0, 1).toLowerCase() + fieldId.substring(1);
                    try {
                        Field field = clazz.getDeclaredField(fieldId);
                        field.setAccessible(true);
                        idFieldValue = (String) field.get(entity);
                        break;
                    } catch (Exception e) {
                        log.error("ID Field Exception : {}", e.getLocalizedMessage());
                        log.error(e.getStackTrace()[0]);
                    }
                }
            }
        }
        if (idFieldValue == null) {
            idFieldValue = getIdFieldValueFromGetterMethod(clazz.getSuperclass(), entity);
        }
        log.debug("Returned ID Value {}", idFieldValue);
        return idFieldValue;
    }

    /**
     * Insert all changes to {@link AuditLog}.
     *
     * @param before
     * @param after
     * @param classPath
     */
    private void insertActivityLog(String before, String after, String classPath,
            ActivityEnum activity) {
        UserLogin userLogin = UserLoginUtil.getCurrentUser();
        String createdBy = userLogin == null ? CoreModelConstant.COFIN_SYSTEM
                : userLogin.getUsername();
        AuditLog activityLog = new AuditLog();
        activityLog.setCreatedBy(createdBy);
        activityLog.setActivity(activity);
        activityLog.setFullClassName(classPath);
        activityLog.setDataBefore(before);
        activityLog.setDataAfter(after);
        activityLog = activityLogDao.saveWithNewPropagation(activityLog);
    }

    /**
     * Return true if the entity is marked as audit transient.
     *
     * @param clazz
     * @return boolean
     */
    private Boolean isAuditTransientEntity(Class<?> clazz) {
        for (Annotation annotation : clazz.getAnnotations()) {
            if (annotation.annotationType().equals(AuditTransient.class)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onPostDelete(PostDeleteEvent event) {
        log.debug("Post Delete Audit");
        Object entity = event.getEntity();
        Class<? extends Object> clazz = entity.getClass();
        if (isAuditTransientEntity(clazz)) {
            log.debug("Skip Audit For Entity With Annotation @AuditTransient");
            return;
        }
        JSON json = convertToJSON(entity);
        insertActivityLog(json.toString(), null, clazz.getCanonicalName(), ActivityEnum.DELETE);
    }

    @Override
    public void onPostInsert(PostInsertEvent event) {
        log.debug("Post Insert Audit");
        Object entity = event.getEntity();
        Class<? extends Object> clazz = entity.getClass();
        if (isAuditTransientEntity(clazz)) {
            log.debug("Skip Audit For Entity With Annotation @AuditTransient");
            return;
        }
        JSON json = convertToJSON(entity);
        insertActivityLog(null, json.toString(), clazz.getCanonicalName(), ActivityEnum.INSERT);
    }

    @Override
    public void onPostUpdate(PostUpdateEvent event) {
        log.debug("Post Update Audit");
        Object entity = event.getEntity();
        Class<?> clazz = entity.getClass();
        if (isAuditTransientEntity(clazz)) {
            log.debug("Skip Audit For Entity With Annotation @AuditTransient");
            return;
        }
        String idFieldValue = getIdFieldValueFromGetterMethod(clazz, entity);
        Object entityBefore = sessionFactory.openSession().get(clazz, idFieldValue);
        JSON jsonBefore = convertToJSON(entityBefore);
        JSON jsonAfter = convertToJSON(entity);
        YesNoEnum deletedAfter = null;
        try{
        	deletedAfter = getDeletedFieldValue(clazz, entity);
        }catch(Exception g){
        	System.out.println(g);
        	deletedAfter = YesNoEnum.N;
        }
        YesNoEnum deletedBefore = null;
        try{
        	deletedBefore = getDeletedFieldValue(clazz, entityBefore);
        }catch(Exception t){
        	System.out.println(t);
        	deletedBefore = YesNoEnum.N;
        }
        if (deletedBefore.equals(YesNoEnum.N) && deletedAfter.equals(YesNoEnum.Y)) {
            insertActivityLog(jsonBefore.toString(), jsonAfter.toString(), clazz.getCanonicalName(),
                    ActivityEnum.SOFT_DELETE);
        } else if (deletedBefore.equals(YesNoEnum.Y) && deletedAfter.equals(YesNoEnum.N)) {
            insertActivityLog(jsonBefore.toString(), jsonAfter.toString(), clazz.getCanonicalName(),
                    ActivityEnum.UNDO_DELETE);
        } else {
            insertActivityLog(jsonBefore.toString(), jsonAfter.toString(), clazz.getCanonicalName(),
                    ActivityEnum.UPDATE);
        }
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister persister) {
        return false;
    }

}
