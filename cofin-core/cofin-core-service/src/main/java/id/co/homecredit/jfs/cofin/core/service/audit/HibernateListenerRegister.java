package id.co.homecredit.jfs.cofin.core.service.audit;

import javax.annotation.PostConstruct;

import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Class to register hibernate listener.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class HibernateListenerRegister {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private AuditEventListener auditEventListener;

    @PostConstruct
    public void registerListeners() {
        EventListenerRegistry registry = ((SessionFactoryImplementor) sessionFactory)
                .getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(auditEventListener);
        registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(auditEventListener);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(auditEventListener);
    }
}
