package id.co.homecredit.jfs.cofin.core.service.batch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * @author muhammad.muflihun
 *
 */
@Service
public class BtpnBatchService {
    private static final Logger log = LogManager.getLogger(BtpnBatchService.class);

    public void basicProcess() {
        log.debug("inside basic process");
    }
}
