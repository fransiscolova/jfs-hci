package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.service.ActionService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class ActionServiceImpl implements ActionService {
    private static final Logger log = LogManager.getLogger(ActionServiceImpl.class);

    @Autowired
    private ActionDao actionDao;

    @Override
    public List<Action> getAllAction(boolean includeDeleted) {
        // TODO Auto-generated method stub
        return actionDao.getAll(includeDeleted);
    }

    @Override
    public Action getByName(String name) {
        // TODO Auto-generated method stub
        return actionDao.getByName(name);
    }

    @Override
    public Action getByRoleCode(Action action) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Action saveAction(Action action) {
        // TODO Auto-generated method stub
        return actionDao.save(action);
    }
    
    @Override
    public List<Action> getActionLikeName(String name){
    	return actionDao.getActionLikeName(name);
    }

}
