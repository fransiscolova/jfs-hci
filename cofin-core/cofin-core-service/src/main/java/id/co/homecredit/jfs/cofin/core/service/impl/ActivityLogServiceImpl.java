package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ActivityLogDao;
import id.co.homecredit.jfs.cofin.core.model.AuditLog;
import id.co.homecredit.jfs.cofin.core.service.ActivityLogService;

/**
 * Service implement class for {@link AuditLog}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class ActivityLogServiceImpl implements ActivityLogService {
    private static final Logger log = LogManager.getLogger(ActivityLogServiceImpl.class);

    @Autowired
    private ActivityLogDao activityLogDao;

    @Override
    public void deleteActivityLog(AuditLog activityLog) {
        log.info("delete activity log {}", activityLog.toString());
        activityLogDao.delete(activityLog);
    }

    @Override
    public AuditLog getActivityLogById(String activityLogId) {
        log.info("get activity log by activity log {}", activityLogId);
        return activityLogDao.get(activityLogId);
    }

    @Override
    public List<AuditLog> getAllActivityLogsByIncludeDeleted(Boolean includeDeleted) {
        log.info("get all activity logs by include deleted {}", includeDeleted);
        return activityLogDao.getAll(includeDeleted);
    }

    @Override
    public AuditLog saveActivityLog(AuditLog activityLog) {
        log.info("save activity log {}", activityLog.toString());
        return activityLogDao.save(activityLog);
    }

}
