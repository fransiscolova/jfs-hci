package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.AgreementDetailDao;
import id.co.homecredit.jfs.cofin.core.model.AgreementDetail;
import id.co.homecredit.jfs.cofin.core.service.AgreementDetailService;

/**
 * Service implement class for {@link AgreementDetail}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class AgreementDetailServiceImpl implements AgreementDetailService {
    private static final Logger log = LogManager.getLogger(AgreementDetailServiceImpl.class);

    @Autowired
    private AgreementDetailDao agreementDetailDao;

    @Override
    public void deleteAgreementDetailById(String agreementDetailId) {
        log.info("delete agreement detail by agreement detail id {}", agreementDetailId);
        agreementDetailDao.deleteById(agreementDetailId);
    }

    @Override
    public AgreementDetail getActiveAgreementDetailByAgreementIdAndDate(String agreementId,
            Date date) {
        log.info("get active agreement detail by agreement id {} and date {}", agreementId, date);
        return agreementDetailDao.getActiveAgreementDetailByAgreementIdAndDate(agreementId, date);
    }

    @Override
    public AgreementDetail getAgreementDetailById(String agreementDetailId) {
        // TODO Auto-generated method stub
        return agreementDetailDao.get(agreementDetailId);
    }

    @Override
    public List<AgreementDetail> getAllAgreementDetailByAgreementId(String agreementId) {
        log.info("get all agreement detail by agreement id {}", agreementId);
        return agreementDetailDao.getAllAgreementDetailByAgreementId(agreementId);
    }

    @Override
    public AgreementDetail saveAgreementDetail(AgreementDetail agreementDetail) {
        log.info("save agreement detail {}", agreementDetail.toString());
        return agreementDetailDao.save(agreementDetail);
    }

    @Override
    public List<AgreementDetail> saveAgreementDetails(List<AgreementDetail> agreementDetail) {
        // TODO Auto-generated method stub
        agreementDetailDao.save(agreementDetail);
        return null;
    }

}
