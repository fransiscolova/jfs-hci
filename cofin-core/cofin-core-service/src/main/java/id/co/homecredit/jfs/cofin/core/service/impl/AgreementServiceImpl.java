package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.AgreementDao;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.service.AgreementService;

/**
 * Service implement class for {@link Agreement}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class AgreementServiceImpl implements AgreementService {
    private static final Logger log = LogManager.getLogger(PartnerServiceImpl.class);

    @Autowired
    private AgreementDao agreementDao;

    @Override
    public void deleteAgreement(String agreementId) {
        log.info("delete agreement by id {}", agreementId);
        agreementDao.deleteById(agreementId);
    }

    @Override
    public Agreement getAgreementById(String agreementId) {
        log.info("get agreement {}", agreementId);
        return agreementDao.get(agreementId);
    }

    @Override
    public Agreement getAgreementByIdAlias(String idAlias) {
        log.info("delete agreement by id {}", idAlias);
        return agreementDao.getAgreementByAliasId(idAlias);
    }

    @Override
    public Agreement getAgreementWithFetchedDetailsById(String agreementId) {
        return agreementDao.getAgreementWithFetchedDetailsById(agreementId);
    }

    @Override
    public Agreement getAgreementWithFetchedProductMappingsById(String agreementId) {
        return agreementDao.getAgreementWithFetchedProductMappingsById(agreementId);
    }

    @Override
    public List<Agreement> getAllAgreements(Boolean includeDeleted) {
        log.info("get all agreements by include deleted {}", includeDeleted);
        return agreementDao.getAll(includeDeleted);
    }

    @Override
    public List<Agreement> getAllAgreementsByPartnerCode(String partnerCode) {
        log.info("Get All Agreements By Partner Code {}", partnerCode);
        return agreementDao.getAllAgreementsByPartnerCode(partnerCode);
    }

    @Override
    public Agreement saveAgreement(Agreement agreement) {
        log.info("save agreement {}", agreement.toString());
        return agreementDao.save(agreement);
    }
    
}
