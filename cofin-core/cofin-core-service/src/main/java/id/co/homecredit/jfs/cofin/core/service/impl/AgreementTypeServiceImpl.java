package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.AgreementTypeDao;
import id.co.homecredit.jfs.cofin.core.model.AgreementType;
import id.co.homecredit.jfs.cofin.core.service.AgreementTypeService;

/**
 * Service Implement For {@link AgreementTypeService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class AgreementTypeServiceImpl implements AgreementTypeService{
	private static final Logger log = LogManager.getLogger(AgreementTypeServiceImpl.class);
	
	@Autowired
	private AgreementTypeDao agreementTypeDao;
	
	@Override
	public List<AgreementType> getAllAgreementType(){
		log.info("Get All Agreement Type");
		return agreementTypeDao.getAll(true);
	}
	
	@Override
	public AgreementType getAgreementTypeById(String id){
		log.info("Get Agreement Type By ID {}",id);
		return agreementTypeDao.get(id);
	}
	
}
