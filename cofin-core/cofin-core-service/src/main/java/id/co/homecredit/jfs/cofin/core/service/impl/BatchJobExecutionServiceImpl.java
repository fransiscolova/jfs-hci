package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.BatchJobExecutionDao;
import id.co.homecredit.jfs.cofin.core.model.BatchJobExecution;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.service.BatchJobExecutionService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class BatchJobExecutionServiceImpl implements BatchJobExecutionService {
    private static final Logger log = LogManager.getLogger(BatchJobExecutionServiceImpl.class);

    @Autowired
    private BatchJobExecutionDao BatchJobExecutionDao;

    @Override
    public List<BatchJobExecution> getBatchJobExecutionByJobInstanceId(String jobName) {
        // TODO Auto-generated method stub
        return BatchJobExecutionDao.getBatchJobExecutionByJobInstanceId(jobName);
    }

    @Override
    public List<BatchJobExecution> getBatchJobExecutionByJobInstanceId(String jobname,
            Date createDateFrom, Date createDateTo) {
        // TODO Auto-generated method stub
        return BatchJobExecutionDao.getBatchJobExecutionByJobInstanceId(jobname, createDateFrom,
                createDateTo);
    }

}
