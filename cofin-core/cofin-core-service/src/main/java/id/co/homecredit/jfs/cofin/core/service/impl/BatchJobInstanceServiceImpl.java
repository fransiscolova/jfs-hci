package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.BatchJobInstanceDao;
import id.co.homecredit.jfs.cofin.core.model.BatchJobInstance;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.service.BatchJobInstanceService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class BatchJobInstanceServiceImpl implements BatchJobInstanceService {
    private static final Logger log = LogManager.getLogger(BatchJobInstanceServiceImpl.class);

    @Autowired
    private BatchJobInstanceDao batchInstanceJobDao;

    @Override
    public BatchJobInstance getAllBatchInstance() {
        // TODO Auto-generated method stub
        return batchInstanceJobDao.getAllBatchInstance();
    }

    @Override
    public List<BatchJobInstance> getBatchInstanceByName(String jobname) {
        // TODO Auto-generated method stub
        return batchInstanceJobDao.getBatchInstanceByName(jobname);
    }

}
