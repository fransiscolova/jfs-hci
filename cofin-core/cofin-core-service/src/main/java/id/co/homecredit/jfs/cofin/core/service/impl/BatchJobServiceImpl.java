package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.BatchJobDao;
import id.co.homecredit.jfs.cofin.core.model.BatchJob;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.service.BatchJobService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class BatchJobServiceImpl implements BatchJobService {
    private static final Logger log = LogManager.getLogger(BatchJobServiceImpl.class);

    @Autowired
    private BatchJobDao batchJobDao;

    @Override
    public List<BatchJob> getAllBatchJob(boolean includedDeleted) {
        // TODO Auto-generated method stub
        return batchJobDao.getAll(true);
    }

    @Override
    public BatchJob getBatchJobById(String id) {
        // TODO Auto-generated method stub
        return batchJobDao.get(id);
    }

    @Override
    public BatchJob save(BatchJob batchJob) {
        // TODO Auto-generated method stub
        return batchJobDao.save(batchJob);
    }

}
