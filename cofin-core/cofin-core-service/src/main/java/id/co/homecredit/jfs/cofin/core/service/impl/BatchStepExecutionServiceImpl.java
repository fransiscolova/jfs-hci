package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.BatchStepExecutionDao;
import id.co.homecredit.jfs.cofin.core.model.BatchStepExecution;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.service.BatchStepExecutionService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class BatchStepExecutionServiceImpl implements BatchStepExecutionService {
    private static final Logger log = LogManager.getLogger(BatchStepExecutionServiceImpl.class);

    @Autowired
    private BatchStepExecutionDao batchStepExecutionDao;

    @Override
    public List<BatchStepExecution> getBatchStepExecutionByJobExecutionId(Long jobExecutionId) {
        // TODO Auto-generated method stub
        return batchStepExecutionDao.getBatchStepExecutionByJobExecutionId(jobExecutionId);
    }

}
