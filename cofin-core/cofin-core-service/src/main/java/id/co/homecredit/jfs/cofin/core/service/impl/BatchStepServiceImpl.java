package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.BatchStepDao;
import id.co.homecredit.jfs.cofin.core.model.BatchStep;
import id.co.homecredit.jfs.cofin.core.service.BatchStepService;

@Service
public class BatchStepServiceImpl implements BatchStepService {

    @Autowired
    BatchStepDao batchStepDao;

    @Override
    public List<BatchStep> getBatchStepByJobId(String jobsId) {
        // TODO Auto-generated method stub
        return batchStepDao.getBatchStepByJobId(jobsId);
    }

}
