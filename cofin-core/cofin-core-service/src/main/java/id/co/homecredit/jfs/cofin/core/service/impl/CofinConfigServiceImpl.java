package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.common.model.DropDownListItem;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.CofinConfigDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.CofinConfig;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.service.CofinConfigService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class CofinConfigServiceImpl implements CofinConfigService {
    private static final Logger log = LogManager.getLogger(CofinConfigServiceImpl.class);

    @Autowired
    private CofinConfigDao cofinConfigDao;

	@Override
	public List<CofinConfig> getByCategory(String category) {
		return cofinConfigDao.getByCategory(category);
	}

	@Override
	public List<CofinConfig> getByCategoryKey1(String roleCode, String key1) {
		return cofinConfigDao.getByCategoryKey1(roleCode, key1);
	}
	
	@Override
	public String getByCategoryKey2(String category,String key01,String key02){
		log.info("Get Configuration By Category = {}, Key01 = {}, key02 = {}",category,key01,key02);
		return cofinConfigDao.getByCategoryKey2(category,key01,key02);
	}
	
	@Override
	public List<DropDownListItem> getJfsContractStatus(){
		log.info("Get Value Of JFS Contract Status");
		return cofinConfigDao.getJfsContractStatus();
	}

	@Override
	public CofinConfig getDescriptionByValue(String value){
		log.info("Get Description By Value {}",value);
		return cofinConfigDao.getDescriptionByValue(value);
	}

}
