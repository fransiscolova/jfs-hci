package id.co.homecredit.jfs.cofin.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ComCategoryMappingDao;
import id.co.homecredit.jfs.cofin.core.model.ComCategoryMapping;
import id.co.homecredit.jfs.cofin.core.service.ComCategoryMappingService;

/**
 * Service implement class for {@link ComCategoryMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class ComCategoryMappingServiceImpl implements ComCategoryMappingService {
    private static final Logger log = LogManager.getLogger(ComCategoryMappingServiceImpl.class);
    
    @Autowired
    private ComCategoryMappingDao comCategoryMappingDao;
    
    
}
