package id.co.homecredit.jfs.cofin.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ComTypeMappingDao;
import id.co.homecredit.jfs.cofin.core.model.ComTypeMapping;
import id.co.homecredit.jfs.cofin.core.service.ComTypeMappingService;

/**
 * Service implement class for {@link ComTypeMapping}.
 * 
 * @author muhammad.muflihun
 *
 */
@Service
public class ComTypeMappingServiceImpl implements ComTypeMappingService {
    private static final Logger log = LogManager.getLogger(ComTypeMappingServiceImpl.class);
    
    @Autowired
    private ComTypeMappingDao comTypeMappingDao;

}
