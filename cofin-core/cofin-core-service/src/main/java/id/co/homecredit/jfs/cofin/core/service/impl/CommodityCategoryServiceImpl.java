package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.CommodityCategoryDao;
import id.co.homecredit.jfs.cofin.core.model.CommodityCategory;
import id.co.homecredit.jfs.cofin.core.service.CommodityCategoryService;

/**
 * Service implement class for {@link CommodityCategory}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class CommodityCategoryServiceImpl implements CommodityCategoryService {
    private static final Logger log = LogManager.getLogger(CommodityCategoryServiceImpl.class);

    @Autowired
    private CommodityCategoryDao commodityCategoryDao;

    @Override
    public List<CommodityCategory> getAllCommodityCategoriesByIncludeDeleted(Boolean includeDeleted) {
        log.info("get all commodity category by include deleted {}", includeDeleted);
        return commodityCategoryDao.getAll(includeDeleted);
    }

    @Override
    public CommodityCategory getCommodityCategoryByCode(String categoryCode) {
        log.info("get commodity category by commodity category code {}", categoryCode);
        return commodityCategoryDao.get(categoryCode);
    }

    @Override
    public CommodityCategory saveCommodityCategory(CommodityCategory commodityCategory) {
        log.info("save commodity category {}", commodityCategory.toString());
        return commodityCategoryDao.save(commodityCategory);
    }

}
