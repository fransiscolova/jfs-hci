package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import id.co.homecredit.jfs.cofin.core.dao.CommodityTypeDao;
import id.co.homecredit.jfs.cofin.core.model.CommodityType;
import id.co.homecredit.jfs.cofin.core.service.CommodityTypeService;

/**
 * Service implement class for {@link CommodityType}.
 *
 * @author muhammad.muflihun
 *
 */
public class CommodityTypeServiceImpl implements CommodityTypeService {
    private static final Logger log = LogManager.getLogger(CommodityTypeServiceImpl.class);

    @Autowired
    private CommodityTypeDao commodityTypeDao;

    @Override
    public List<CommodityType> getAllCommodityTypesByIncludeDeleted(Boolean includeDeleted) {
        log.info("get all commodity type by include deleted {}", includeDeleted);
        return commodityTypeDao.getAll(includeDeleted);
    }

    @Override
    public CommodityType getCommodityTypeByCode(String typeCode) {
        log.info("get commodity type by commodity type code {}", typeCode);
        return commodityTypeDao.get(typeCode);
    }

    @Override
    public CommodityType saveCommodityType(CommodityType commodityType) {
        log.info("save commodity type {}", commodityType.toString());
        return commodityTypeDao.save(commodityType);
    }

}
