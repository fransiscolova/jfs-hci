package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.model.enumeration.ContractStatusEnum;
import id.co.homecredit.jfs.cofin.core.dao.ContractDao;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.Contract;
import id.co.homecredit.jfs.cofin.core.model.JfsContract;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.service.ContractService;

/**
 * Service implement class for {@link Contract}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class ContractServiceImpl implements ContractService {
    private static final Logger log = LogManager.getLogger(ContractServiceImpl.class);

    @Autowired
    private ContractDao contractDao;

    @Override
    public void deleteContract(Contract contract) {
        log.info("delete contract {}", contract);
        contractDao.delete(contract);
    }

    @Override
    public void deleteContract(String contractNumber) {
        log.info("delete contract by contract number {}", contractNumber);
        contractDao.deleteById(contractNumber);
    }

    @Override
    public List<Contract> getAllContractsByInlcludeDeleted(Boolean includeDeleted) {
        log.info("get all contracts by include deleted {}", includeDeleted);
        return contractDao.getAll(includeDeleted);
    }

    @Override
    public List<Contract> getAllContractsInquiry(String contractCode, ContractStatusEnum status,
            Partner partner, Agreement agreement, JSONObject dateFilter) throws Exception {
        return contractDao.getAllContractsInquiry(contractCode, status, partner, agreement,
                dateFilter);
    }

    @Override
    public Contract getContract(String contractNumber) {
        log.info("get contract by contract number {}", contractNumber);
        return contractDao.get(contractNumber);
    }

    @Override
    public Contract saveContract(Contract contract) {
        log.info("save contract {}", contract.toString());
        return contractDao.save(contract);
    }
    
}
