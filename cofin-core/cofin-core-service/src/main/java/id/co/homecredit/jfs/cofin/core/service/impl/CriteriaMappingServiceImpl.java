package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.util.StringUtil;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.Html;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.StringChar;
import id.co.homecredit.jfs.cofin.core.dao.CriteriaMappingDao;
import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;
import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.dto.FinalCriteriaEligibilityDto;
import id.co.homecredit.jfs.cofin.core.model.dto.FlatCriteriaEligibilityDto;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.EligibilityEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.OperatorEnum;
import id.co.homecredit.jfs.cofin.core.service.CriteriaMappingService;

/**
 * Service interface class for {@link CriteriaMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class CriteriaMappingServiceImpl implements CriteriaMappingService {
    private static final Logger log = LogManager.getLogger(CriteriaMappingServiceImpl.class);

    @Autowired
    private CriteriaMappingDao criteriaMappingDao;

    @Override
    public void deleteCriteriaMappingById(String criteriaMappingId) {
        log.info("delete criteria mapping by criteria mapping id {}", criteriaMappingId);
        criteriaMappingDao.deleteById(criteriaMappingId);
    }

    @Override
    public String finalListToSql(FinalCriteriaEligibilityDto finalList) {
        StringBuilder sb = new StringBuilder(1000);
        sb.append(" WHERE 0=0 ");
        List<FlatCriteriaEligibilityDto> flatDtos = finalList.getFlatCriteriaEligibilityDtos();
        for (FlatCriteriaEligibilityDto dto : flatDtos) {
            sb.append(finalSqlGroup(dto));
        }
        return sb.toString();
    }

    private String finalSqlGroup(FlatCriteriaEligibilityDto dto) {
        StringBuilder sb = new StringBuilder(500);
        CriteriaEnum criteria = dto.getCriteria();
        OperatorEnum operator = dto.getOperator() == null ? OperatorEnum.AND : dto.getOperator();
        Integer criteriaLevel = dto.getLevel();
        sb.append(Html.ENTER);
        for (int level = 0; level < criteriaLevel; level++) {
            sb.append(Html.TAB_SPACE);
        }
        sb.append(StringChar.SPACE);
        if (!criteria.equals(CriteriaEnum.GROUP)) {
            for (Map<EligibilityEnum, String> map : dto.getListMap()) {
                for (Entry<EligibilityEnum, String> entry : map.entrySet()) {
                    switch (entry.getKey()) {
                        case MINIMUM:
                            sb.append(operator);
                            sb.append(StringChar.SPACE);
                            sb.append(criteria);
                            sb.append(StringChar.SPACE);
                            sb.append(">=");
                            sb.append(StringChar.SPACE);
                            sb.append(entry.getValue());
                            sb.append(StringChar.SPACE);
                            break;
                        case MAXIMUM:
                            sb.append(operator);
                            sb.append(StringChar.SPACE);
                            sb.append(criteria);
                            sb.append(StringChar.SPACE);
                            sb.append("<=");
                            sb.append(StringChar.SPACE);
                            sb.append(entry.getValue());
                            sb.append(StringChar.SPACE);
                            break;
                        case IN:
                            sb.append(operator);
                            sb.append(StringChar.SPACE);
                            sb.append(criteria);
                            sb.append(StringChar.SPACE);
                            sb.append("IN");
                            sb.append(StringChar.SPACE);
                            sb.append(StringChar.LEFT_PARENTHESES);
                            sb.append(StringUtil.splitAndTrimAndJoinToSql(entry.getValue()));
                            sb.append(StringChar.RIGHT_PARENTHESES);
                            sb.append(StringChar.SPACE);
                            break;
                        case NOT_IN:
                            sb.append(operator);
                            sb.append(StringChar.SPACE);
                            sb.append(criteria);
                            sb.append(StringChar.SPACE);
                            sb.append("NOT IN");
                            sb.append(StringChar.SPACE);
                            sb.append(StringChar.LEFT_PARENTHESES);
                            sb.append(StringUtil.splitAndTrimAndJoinToSql(entry.getValue()));
                            sb.append(StringChar.RIGHT_PARENTHESES);
                            sb.append(StringChar.SPACE);
                            break;
                        case EQUALS:
                            sb.append(operator);
                            sb.append(StringChar.SPACE);
                            sb.append(criteria);
                            sb.append(StringChar.SPACE);
                            sb.append("=");
                            sb.append(StringChar.SPACE);
                            sb.append(entry.getValue());
                            sb.append(StringChar.SPACE);
                            break;
                        case NOT_EQUALS:
                            sb.append(operator);
                            sb.append(StringChar.SPACE);
                            sb.append(criteria);
                            sb.append(StringChar.SPACE);
                            sb.append("!=");
                            sb.append(StringChar.SPACE);
                            sb.append(entry.getValue());
                            sb.append(StringChar.SPACE);
                            break;
                        default:
                            break;
                    }
                }
            }
        } else {
            sb.append(operator + " (");
            List<FlatCriteriaEligibilityDto> childDtos = dto.getFlatDtos();
            OperatorEnum childOperator = childDtos.get(0).getOperator();
            Integer childLevel = dto.getLevel() + 1;
            if (childOperator == null | childOperator.equals(OperatorEnum.AND)) {
                sb.append(childLevel).append("=").append(childLevel);
            } else {
                sb.append("0=").append(childLevel);
            }
            for (FlatCriteriaEligibilityDto childDto : childDtos) {
                sb.append(finalSqlGroup(childDto));
            }
            sb.append(")");
        }
        return sb.toString();
    }

    private FlatCriteriaEligibilityDto flatteningCriteria(CriteriaMapping criteriaMapping) {
        FlatCriteriaEligibilityDto flatDto = new FlatCriteriaEligibilityDto();
        CriteriaEnum criteria = criteriaMapping.getCriteria();
        flatDto.setCriteria(criteria);
        if (!criteria.equals(CriteriaEnum.GROUP)) {
            List<Map<EligibilityEnum, String>> listMap = new ArrayList<>();
            List<CriteriaEligibility> criteriaEligibilities = criteriaMapping
                    .getCriteriaEligibilities();
            for (CriteriaEligibility criteriaEligibility : criteriaEligibilities) {
                Map<EligibilityEnum, String> map = new HashMap<>();
                map.put(criteriaEligibility.getEligibility(), criteriaEligibility.getValue());
                listMap.add(map);
            }
            flatDto.setLevel(criteriaMapping.getCriteriaLevel());
            flatDto.setListMap(listMap);
        } else {
            flatDto.setOperator(criteriaMapping.getGroupOperator());
            flatDto.setLevel(criteriaMapping.getCriteriaLevel());

            List<FlatCriteriaEligibilityDto> listDto = new ArrayList<>();
            List<CriteriaMapping> childs = criteriaMapping.getCriteriaMappingChilds();
            for (CriteriaMapping child : childs) {
                listDto.add(flatteningCriteria(child));
            }
            flatDto.setFlatDtos(listDto);
        }
        return flatDto;
    }

    @Override
    public FinalCriteriaEligibilityDto flattingCriteria(List<CriteriaMapping> criteriaMappings) {
        FinalCriteriaEligibilityDto finalDto = new FinalCriteriaEligibilityDto();
        List<FlatCriteriaEligibilityDto> listDtos = new ArrayList<>();
        for (CriteriaMapping criteriaMapping : criteriaMappings) {
            listDtos.add(flatteningCriteria(criteriaMapping));
        }
        finalDto.setFlatCriteriaEligibilityDtos(listDtos);
        return finalDto;
    }

    @Override
    public List<CriteriaMapping> getAllCriteriaMappingsByAgreementId(String agreementId) {
        log.info("get all criteria mappings by agreement id {}", agreementId);
        return criteriaMappingDao.getAllCriteriaMappingsByAgreementId(agreementId);
    }

    @Override
    public List<CriteriaMapping> getAllCriteriaMappingsByCriteria(CriteriaEnum criteiraEnum) {
        return criteriaMappingDao.getAllCriteriaMappingsByCriteria(criteiraEnum);
    }

    @Override
    public CriteriaMapping getCriteriaMappingById(String criteriaMappingId) {
        log.info("get criteria mapping by id {}", criteriaMappingId);
        return criteriaMappingDao.get(criteriaMappingId);
    }

    @Override
    public CriteriaMapping saveCriteriaMapping(CriteriaMapping criteriaMapping) {
        log.info("save criteria mapping {}", criteriaMapping.toString());
        return criteriaMappingDao.save(criteriaMapping);
    }

}
