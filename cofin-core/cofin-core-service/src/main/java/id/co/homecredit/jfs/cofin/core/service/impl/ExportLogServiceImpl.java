package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ExportLogDao;
import id.co.homecredit.jfs.cofin.core.model.ExportLog;
import id.co.homecredit.jfs.cofin.core.service.ExportLogService;

/*
 * Service implement for ExportLogService
 * @author fransisco.situmorang
 */

@Service
public class ExportLogServiceImpl implements ExportLogService {

    @Autowired
    ExportLogDao exportLogDao;

    @Override
    public List<ExportLog> getExportLogByStepExecutionId(Long stepExecutionId) {
        // TODO Auto-generated method stub
        return exportLogDao.getExportLogByStepExecutionId(stepExecutionId);
    }

	@Override
	public ExportLog get(String id) {
		// TODO Auto-generated method stub
		return exportLogDao.get(id);
	}
    
    

}
