package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.FileUploadDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class FileUploadServiceImpl implements FileUploadService {
    private static final Logger log = LogManager.getLogger(FileUploadServiceImpl.class);

    @Autowired
    private FileUploadDao fileUploadDao;


    
    @Override
    public FileUpload saveFileUpload(FileUpload fileUpload) {
        // TODO Auto-generated method stub
        return fileUploadDao.save(fileUpload);
    }
    
    
    @Override
    public List<FileUpload> getFileUpload() {
        // TODO Auto-generated method stub
        return fileUploadDao.getAll(true);
    }
    
    
    @Override
    public FileUpload getFileUpload(String id) {
        // TODO Auto-generated method stub
        return fileUploadDao.get(id);
    }


	@Override
	public List<FileUpload> getFileUpload(String processDate, String confirmFile) {
		// TODO Auto-generated method stub
		return fileUploadDao.getFileUpload(processDate, confirmFile);
	}


	@Override
	public Boolean deleteById(String id) {
		// TODO Auto-generated method stub
		fileUploadDao.deleteById(id);;
		return true;
				
				
	}
}
