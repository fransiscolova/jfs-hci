package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.ImportTempFileDao;
import id.co.homecredit.jfs.cofin.core.dao.TempJfsConfirmationFileDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.ImportTempFile;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.ImportTempFileService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class ImportTempFileServiceImpl implements ImportTempFileService {
    private static final Logger log = LogManager.getLogger(ImportTempFileServiceImpl.class);

    @Autowired
    private ImportTempFileDao importTempFileDao;

	@Override
	public List<ImportTempFile> saveHasilPemeriksaan(List<ImportTempFile> importTempFile) {
		// TODO Auto-generated method stub
		return importTempFileDao.save(importTempFile);
	}

	@Override
	public String saveHasilPemeriksaan(ImportTempFile importTempFile) {
		// TODO Auto-generated method stub
		return importTempFileDao.save(importTempFile).getId();
		
	}

	@Override
	public void runProcedurePermata(String query) {
		// TODO Auto-generated method stub
		
		importTempFileDao.runProcedurePermata(query);
		 
	}

  

}
