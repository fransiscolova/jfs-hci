package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsAgreementDao;
import id.co.homecredit.jfs.cofin.core.model.JfsAgreement;
import id.co.homecredit.jfs.cofin.core.service.JfsAgreementService;

/**
 * Service Implement Class For {@link JfsAgreement}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsAgreementServiceImpl implements JfsAgreementService{
	private static final Logger log = LogManager.getLogger(JfsAgreementServiceImpl.class);
	
	@Autowired
	protected JfsAgreementDao jfsAgreementDao;
	
	public List<Map<String,Object>> getAllAgreementWithQuery(){
		String query = "select * from JFS_AGREEMENT order by ID_AGREEMENT";
		log.info("Execute {}",query);
		return jfsAgreementDao.executeSelectQuery(query);
	}
	
	@Override
	public List<JfsAgreement> getAllAgreement(){
		log.info("Get All Agreement");
		return jfsAgreementDao.getAll();
	}
	
	@Override
	public JfsAgreement getJfsAgreementById(Long agreementId){
		log.info("Get JFS Agreement By ID {}",agreementId);
		return jfsAgreementDao.getJfsAgreementById(agreementId);
	}
	
	@Override
	public Object saveAgreement(Object jfsAgreement){
		log.info("Save Value Of JFS Agreement");
		return jfsAgreementDao.saveObject(jfsAgreement);
	}
	
	@Override
	public JfsAgreement agreementSave(JfsAgreement jfsAgreement){
		log.info("Save Value Of JFS Agreement");
		return jfsAgreementDao.save(jfsAgreement);
	}
	
	@Override
	public boolean updateValueOfJfsAgreementWithQuery(String query){
		log.info("Update JFS Agreement To Database Using Query String");
		return jfsAgreementDao.updateObjectWithQuery(query);
	}
	
	@Override
	public JfsAgreement agreementUpdate(JfsAgreement jfsAgreement){
		log.info("Update Value Of JFS Agreement");
		return jfsAgreementDao.save(jfsAgreement);
	}
	
	@Override
	public boolean agreementUpdateWithQuery(JfsAgreement jfsAgreement, String validFrom, String validTo){
		log.info("Update Value Of JFS Agreement Using Query");
		validFrom = validFrom.substring(3,5)+"/"+validFrom.substring(0,2)+"/"+validFrom.substring(6,10);
		validTo = validTo.substring(3,5)+"/"+validTo.substring(0,2)+"/"+validTo.substring(6,10);
		String query = "update JFS_AGREEMENT set CODE_AGREEMENT = '"+jfsAgreement.getCodeAgreement()+"', NAME_AGREEMENT = '"+jfsAgreement.getNameAgreement()+"', VALID_FROM = TO_DATE('"+validFrom+"', 'DD/MM/YY'), VALID_TO = TO_DATE('"+validTo+"', 'DD/MM/YY'), DESCRIPTION = '"+jfsAgreement.getDescription()+"' where ID = '"+jfsAgreement.getId()+"'";
		return jfsAgreementDao.updateObjectWithQuery(query);
	}
	
	@Override
	public List<JfsAgreement> getJfsAgreementByPartnerId(String partnerId)throws Exception{
		log.info("Get Value Of JFS Agreement By Partner ID {}",partnerId);
		return jfsAgreementDao.getJfsAgreementByPartnerId(partnerId);
	}
	
	@Override
	public String getMaxIdAgreement(){
		log.info("Get Max Value Of JFS Agreement");
		return jfsAgreementDao.getMaxIdAgreement();
	}

}
