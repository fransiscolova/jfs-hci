package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsContractEtDao;
import id.co.homecredit.jfs.cofin.core.model.JfsContractEt;
import id.co.homecredit.jfs.cofin.core.service.JfsContractEtService;

/**
 * Service Implement Class For {@link JfsContractEtService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsContractEtServiceImpl implements JfsContractEtService{
	private static final Logger log = LogManager.getLogger(JfsContractEtServiceImpl.class);
	
	@Autowired
	protected JfsContractEtDao jfsContractEtDao;
	
	@Override
	public List<JfsContractEt> getContractEtByContractNumber(String contractNumber){
		log.info("Get Value Of JFS Contract ET By Contract Number {}",contractNumber);
		return jfsContractEtDao.getContractEtByContractNumber(contractNumber);
	}
	
}
