package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import java.util.Date;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.service.JfsContractService;
import id.co.homecredit.jfs.cofin.core.model.JfsContract;
import id.co.homecredit.jfs.cofin.core.dao.JfsContractDao;

/**
 * Service implement class for {@link JfsContract}.
 *
 * @author denny.afrizal01
 *
 */
@Service
public class JfsContractServiceImpl implements JfsContractService{
	private static final Logger log = LogManager.getLogger(JfsContractServiceImpl.class);
	
	@Autowired
	private JfsContractDao jfsContractDao;
	
    @Override
    public List<JfsContract> getJfsContractWithoutDate(String contractNumber,String status,Long idAgreement){
    	log.info("Get JFS Contract Value Without Date Criteria");
    	return jfsContractDao.getJfsContractWithoutDate(contractNumber,status,idAgreement);
    }
    
    @Override
    public List<JfsContract> getJfsContractWithDate(String contractNumber,String status,Long idAgreement,Date date){
    	log.info("Get JFS Contract Value With Date Criteria");
    	return jfsContractDao.getJfsContractWithDate(contractNumber,status,idAgreement,date);
    }

    @Override
    public List<JfsContract> getJfsContractBetweenDate(String contractNumber,String status,Long idAgreement,Date dateFrom,Date dateTo){
    	log.info("Get JFS Contract Value With Between Date Criteria");
    	return jfsContractDao.getJfsContractBetweenDate(contractNumber,status,idAgreement,dateFrom,dateTo);
    }
    
    @Override
    public JfsContract getHeaderJfsContract(String contractNumber){
    	log.info("Get Header Of JFS Contract By Contract Number {}",contractNumber);
    	return jfsContractDao.getHeaderJfsContract(contractNumber);
    }

//    @Override
//	public JfsContract getDetailJfsContract(String textContractNumber){
//		log.info("Get Detail Of JFS Contract By Text Contract Number{}",textContractNumber);
//		return jfsContractDao.getDetailJfsContract(textContractNumber);
//	}

}
