package id.co.homecredit.jfs.cofin.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsContractWoDao;
import id.co.homecredit.jfs.cofin.core.model.JfsContractWo;
import id.co.homecredit.jfs.cofin.core.service.JfsContractWoService;

/**
 * Service Implement Class For {@link JfsContractWo}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsContractWoServiceImpl implements JfsContractWoService{
	private static final Logger log = LogManager.getLogger(JfsContractWoServiceImpl.class);
	
	@Autowired
	protected JfsContractWoDao jfsContractWoDao;
	
	@Override
	public JfsContractWo getContractWoByContractNumber(String contractNumber){
		log.info("Get Contract WO Value By Contract Number {}",contractNumber);
		return jfsContractWoDao.getContractWoByContractNumber(contractNumber);
	}
	
}
