package id.co.homecredit.jfs.cofin.core.service.impl;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsCustomerDao;
import id.co.homecredit.jfs.cofin.core.model.JfsCustomer;
import id.co.homecredit.jfs.cofin.core.model.dto.JfsCustomerBsl;
import id.co.homecredit.jfs.cofin.core.service.JfsCustomerService;

/**
 * Service Implement Class For {@link JfsCustomerService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsCustomerServiceImpl implements JfsCustomerService{
	private static final Logger log = LogManager.getLogger(JfsCustomerServiceImpl.class);
	
	@Autowired
	protected JfsCustomerDao jfsCustomerDao;
	
	@Override
	public JfsCustomerBsl getDetailCustomer(String cuid)throws MalformedURLException,RemoteException{
		log.info("Get Detail Customer From BSL By CUID {}",cuid);
		return jfsCustomerDao.getDetailCustomer(cuid);
	}

}
