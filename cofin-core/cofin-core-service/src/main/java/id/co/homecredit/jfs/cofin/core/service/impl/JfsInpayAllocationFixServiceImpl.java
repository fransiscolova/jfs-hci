package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsInpayAllocationFixDao;
import id.co.homecredit.jfs.cofin.core.model.JfsInpayAllocationFix;
import id.co.homecredit.jfs.cofin.core.service.JfsInpayAllocationFixService;

/**
 * Service Implement Class For {@JfsInpayAllocationFixService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsInpayAllocationFixServiceImpl implements JfsInpayAllocationFixService{
	private static final Logger log = LogManager.getLogger(JfsInpayAllocationFixServiceImpl.class);
	
	@Autowired
	private JfsInpayAllocationFixDao jfsInpayAllocationFixDao;
	
	@Override
	public JfsInpayAllocationFix getAllocationAmountValue(Long incomingPaymentId,Date dateInstalment){
		log.info("Get Payment Allocation By Incoming Payment Id {} And Date Instalment {}",incomingPaymentId,dateInstalment);
		return jfsInpayAllocationFixDao.getAllocationAmountValue(incomingPaymentId,dateInstalment);
	}
	
}
