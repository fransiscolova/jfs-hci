package id.co.homecredit.jfs.cofin.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsOfiPaymentDao;
import id.co.homecredit.jfs.cofin.core.service.JfsOfiPaymentService;

/**
 * Service Implement Class For {@link JfsOfiPaymentService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsOfiPaymentServiceImpl implements JfsOfiPaymentService{
	private static final Logger log = LogManager.getLogger(JfsOfiPaymentServiceImpl.class);
	
	@Autowired
	private JfsOfiPaymentDao jfsOfiPaymentDao;
	
	@Override
	public boolean processNormalPayment(String tanggal,String xtanggal)throws Exception{
		log.info("Process Normal Calculation Principal And Interest Payment By Payment Date {}",tanggal);
		String sql = "select a.INCOMING_PAYMENT_ID,a.TEXT_CONTRACT_NUMBER,a.DATE_INSTALMENT as DUE_DATE,a.PAYMENT_DATE,"
					 +"sum(nvl(a.AMT_PRINCIPAL,0)-nvl(b.AMT_PRINCIPAL,0)-nvl(c.AMT_PRINCIPAL,0)) as OFI_PRINCIPAL,"
					 +"sum(nvl(a.AMT_INTEREST,0)-nvl(b.AMT_INTEREST,0)-nvl(c.AMT_INTEREST,0)) as OFI_INTEREST "
					 +"from JFS_PAYMENT_BSL a LEFT JOIN (select INCOMING_PAYMENT_ID,DUE_DATE,sum(AMT_PRINCIPAL) as AMT_PRINCIPAL,sum(amt_INTEREST) as AMT_INTEREST "
					 +"from JFS_INPAY_ALLOCATION_FIX group BY INCOMING_PAYMENT_ID,DUE_DATE) b on a.INCOMING_PAYMENT_ID = b.INCOMING_PAYMENT_ID and a.DATE_INSTALMENT = b.DUE_DATE "
					 +"LEFT JOIN JFS_PAYMENT_HCI_PORTION c on a.INCOMING_PAYMENT_ID = c.INCOMING_PAYMENT_ID and a.DATE_INSTALMENT = c.DATE_INSTALMENT "
					 +"where a.PAYMENT_DATE between DATE '"+tanggal+"' and DATE '"+xtanggal+"' "
					 +"group by a.INCOMING_PAYMENT_ID,a.TEXT_CONTRACT_NUMBER,a.DATE_INSTALMENT,a.PAYMENT_DATE";
		return jfsOfiPaymentDao.saveOfiPayment(sql);
	}
	
	@Override
	public boolean saveData(Object object){
		return (boolean) jfsOfiPaymentDao.saveObject(object);
	}
	
}
