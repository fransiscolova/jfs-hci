package id.co.homecredit.jfs.cofin.core.service.impl;

import id.co.homecredit.jfs.cofin.common.dao.DaoBase;
import id.co.homecredit.jfs.cofin.core.dao.JfsPartnerDao;
import id.co.homecredit.jfs.cofin.core.model.JfsPartner;
import id.co.homecredit.jfs.cofin.core.service.JfsPartnerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implement Class For {@link JfsPartner}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsPartnerServiceImpl implements JfsPartnerService{
	private static final Logger log = LogManager.getLogger(JfsPartnerServiceImpl.class);
	
	@Autowired
	protected JfsPartnerDao jfsPartnerDao;
	
	@Override
	public String getPartnerName(String partnerId){
		log.info("Get Name Of Partner {}",partnerId);
		return jfsPartnerDao.getPartnerName(partnerId);
	}
	
	@Override
	public List<JfsPartner> getAllPartner(){
		log.info("Get All JFS Partner Value");
		return jfsPartnerDao.getAllPartner();
	}
	
	@Override
	public JfsPartner getPartnerValueById(String id){
		log.info("Search JFS Partner By Partner ID {}",id);
		return jfsPartnerDao.getPartnerValueById(id);
	}
	
	@Override
	public boolean partnerSave(JfsPartner jfsPartner){
		log.info("Save Value Of JFS Partner");
		return jfsPartnerDao.saveObject(jfsPartner);
	}
	
	@Override
	public boolean updateValueOfJfsPartnerWithQuery(String query){
		log.info("Update JFS Partner To Database Using Query String");
		return jfsPartnerDao.updateObjectWithQuery(query);
	}
	
	@Override
	public boolean partnerUpdate(JfsPartner jfsPartner){
		log.info("Update Value Of JFS Partner To Database");
		return jfsPartnerDao.saveObject(jfsPartner);
	}
	
	@Override
	public boolean partnerUpdateWithQuery(JfsPartner jfsPartner){
		log.info("Update Value Of JFS Partner Using Query");
		String query = "update JFS_PARTNER set NAME='"+jfsPartner.getName()+"',ADDRESS='"+jfsPartner.getAddress()+"',MAIN_CONTACT='"+jfsPartner.getMainContact()+"',PHONE_NUMBER='"+jfsPartner.getPhoneNumber()+"',EMAIL='"+jfsPartner.getEmail()+"',PRIORITY='"+jfsPartner.getPriority()+"',IS_CHECKING_ELIGIBILITY='"+jfsPartner.getIsCheckingEligibility()+"' where id='"+jfsPartner.getId()+"'";
		return jfsPartnerDao.updateObjectWithQuery(query);
	}
	
}
