package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsPaymentAllocationDao;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentAllocation;
import id.co.homecredit.jfs.cofin.core.service.JfsPaymentAllocationService;

/**
 * Service Implement Class For {@link JfsPaymentAllocationService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsPaymentAllocationServiceImpl implements JfsPaymentAllocationService{
	private static final Logger log = LogManager.getLogger(JfsPaymentAllocationServiceImpl.class);
	
	@Autowired
	protected JfsPaymentAllocationDao jfsPaymentAllocationDao;
	
	@Override
	public List<JfsPaymentAllocation> getAllocationPartnerByContractNumber(String contractNumber){
		log.info("Get Payment Allocation By Contract Number {}",contractNumber);
		return jfsPaymentAllocationDao.getAllocationPartnerByContractNumber(contractNumber);
	}

}
