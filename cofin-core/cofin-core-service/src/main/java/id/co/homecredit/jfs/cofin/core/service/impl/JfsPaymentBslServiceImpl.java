package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsPaymentBslDao;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentBsl;
import id.co.homecredit.jfs.cofin.core.service.JfsPaymentBslService;

/**
 * Service Implement Class For {@link JfsPaymentBslService}
 * 
 * @author denny.afrizal01
 *
 */

@Service
public class JfsPaymentBslServiceImpl implements JfsPaymentBslService{
	private static final Logger log = LogManager.getLogger(JfsPaymentBslServiceImpl.class);

	@Autowired
	private JfsPaymentBslDao jfsPaymentBslDao;
	
	@Override
	public List<JfsPaymentBsl> getBslPaymentByDate(Date paymentDate){
		log.info("Get Payment BSL By Payment Date {}",paymentDate);
		return jfsPaymentBslDao.getBslPaymentByDate(paymentDate);
	}

	@Override
	public List<JfsPaymentBsl> getBslPaymentByContractNumber(String textContractNumber){
		log.info("Get Payment BSL By Contract Number {}",textContractNumber);
		return jfsPaymentBslDao.getBslPaymentByContractNumber(textContractNumber);
	}

	@Override
	public List<JfsPaymentBsl> getBslPaymentBetweenDate(Date dateFrom,Date dateTo){
		log.info("Get Payment BSL Between {} And {}",dateFrom,dateTo);
		return jfsPaymentBslDao.getBslPaymentBetweenDate(dateFrom,dateTo);
	}

}
