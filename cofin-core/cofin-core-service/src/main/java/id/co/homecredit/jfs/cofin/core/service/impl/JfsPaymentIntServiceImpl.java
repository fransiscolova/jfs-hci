package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsPaymentIntDao;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentInt;
import id.co.homecredit.jfs.cofin.core.service.JfsPaymentIntService;

/**
 * Service Implement Class For {@link JfsPaymentIntService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsPaymentIntServiceImpl implements JfsPaymentIntService{
	private static final Logger log = LogManager.getLogger(JfsPaymentIntServiceImpl.class);
	
	@Autowired
	protected JfsPaymentIntDao jfsPaymentIntDao;
	
	@Override
	public List<JfsPaymentInt> getPaymentIntByContractNumber(String contractNumber){
		log.info("Get Value Of JFS Payment Int By Contract Number {}",contractNumber);
		return jfsPaymentIntDao.getPaymentIntByContractNumber(contractNumber);
	}

	@Override
	public JfsPaymentInt getByContractNumberAndIncomingPayment(String textContractNumber,Long incomingPaymentId){
		log.info("Get Value Of JFS Payment Int By Contract Number {} And Incoming Payment {}",textContractNumber,incomingPaymentId);
		return jfsPaymentIntDao.getByContractNumberAndIncomingPayment(textContractNumber,incomingPaymentId);
	}
	
}
