package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsProductHciDao;
import id.co.homecredit.jfs.cofin.core.model.JfsProductHci;
import id.co.homecredit.jfs.cofin.core.service.JfsProductHciService;


/**
 * Service implement class for {@link JfsProduct}.
 *
 * @author denny.afrizal01
 *
 */
@Service
public class JfsProductHciServiceImpl implements JfsProductHciService{
	private static final Logger log = LogManager.getLogger(JfsProductHciServiceImpl.class);
	
	@Autowired
	protected JfsProductHciDao jfsProductHciDao;
	
	public List<Map<String,Object>> executeSelectQuery(String script){
		log.info("Execute {}",script);
		return jfsProductHciDao.executeSelectQuery(script);
	}
	
	@Override
	public JfsProductHci saveJfsProductHci(JfsProductHci product){
		log.info("Save JFS Product HCI To Database");
		return jfsProductHciDao.save(product);
	}
	
	@Override
	public JfsProductHci updateJfsProductHci(JfsProductHci product){
		log.info("Update JFS Product HCI To Database");
		return jfsProductHciDao.save(product);
	}
	
	@Override
	public boolean updateJfsProductHciWithQuery(String query){
		log.info("Update JFS Product HCI To Database Using Query String");
		return jfsProductHciDao.updateObjectWithQuery(query);
	}
	
	@Override
	public JfsProductHci getJfsProductHciById(String id){
		log.info("Get JFS Product HCI Value By ID {}",id);
		return jfsProductHciDao.getJfsProductHciById(id);
	}
	
	@Override
	public List<JfsProductHci> getAllProductHci(){
		log.info("Get All JFS Product HCI");
		return jfsProductHciDao.getAll();
	}

}
