package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import java.text.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsProductMappingDao;
import id.co.homecredit.jfs.cofin.core.model.JfsProductMapping;
import id.co.homecredit.jfs.cofin.core.service.JfsProductMappingService;

/**
 * Service Implement Class For {@link JfsProductMapping}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsProductMappingServiceImpl implements JfsProductMappingService{
	private static final Logger log = LogManager.getLogger(JfsProductMappingServiceImpl.class);
	
	@Autowired
	protected JfsProductMappingDao jfsProductMappingDao;
	
	@Override
	public List<JfsProductMapping> getProductMappingByAgreementId(Integer agreementId)throws ParseException{
		log.info("Get All Product Mapping By Agreement ID {}",agreementId);
		return jfsProductMappingDao.getProductMappingByAgreementId(agreementId);
	}
	
	@Override
	public JfsProductMapping productMappingSave(JfsProductMapping jfsProductMapping){
		log.info("Save Value Of JFS Product Mapping");
		return jfsProductMappingDao.save(jfsProductMapping);
	}
	
	@Override
	public JfsProductMapping getProductMappingById(String id)throws ParseException{
		log.info("Get Product Mapping By ID {}",id);
		return jfsProductMappingDao.getProductMappingById(id);
	}
	
	@Override
	public boolean jfsProductMappingUpdate(String status, String updatedBy, String updatedDate, String id){
		log.info("Update Value Of JFS Product Mapping Using Query String");
		String query = "update JFS_PRODUCT_MAPPING set IS_DELETE = '"+status+"', UPDATED_BY = '"+updatedBy+"', UPDATED_DATE = TO_DATE('"+updatedDate+"', 'DD/MM/YY') where ID = '"+id+"'";
		return jfsProductMappingDao.updateObjectWithQuery(query);
	}
	
}
