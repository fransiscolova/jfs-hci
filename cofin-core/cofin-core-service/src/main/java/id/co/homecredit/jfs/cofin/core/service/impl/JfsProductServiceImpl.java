package id.co.homecredit.jfs.cofin.core.service.impl;

import java.text.ParseException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsProductDao;
import id.co.homecredit.jfs.cofin.core.model.JfsProduct;
import id.co.homecredit.jfs.cofin.core.service.JfsProductService;

/**
 * Service Implement Class For {@link JfsProduct}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsProductServiceImpl implements JfsProductService{
	private static final Logger log = LogManager.getLogger(JfsProductServiceImpl.class);
	
	@Autowired
	protected JfsProductDao jfsProductDao;
	
	@Override
	public List<JfsProduct> getProductByAgreementId(Long agreementId) throws ParseException{
		log.info("Get JFS Product By Agreement ID {}",agreementId);
		return jfsProductDao.getProductByAgreementId(agreementId);
	}
	
	@Override
	public JfsProduct productSave(JfsProduct jfsProduct){
		log.info("Save Value Of JFS Product");
		return jfsProductDao.save(jfsProduct);
	}
	
	@Override
	public boolean updateValidDate(String id, String validFrom, String validTo){
		log.info("Update Valid Date Of JFS Product");
		String query = "update JFS_PRODUCT set VALID_FROM = TO_DATE('"+validFrom+"', 'MM/DD/YY'), VALID_TO = TO_DATE('"+validTo+"', 'MM/DD/YY') where ID = '"+id+"'";
		return jfsProductDao.updateObjectWithQuery(query);
	}
	
	@Override
	public JfsProduct getProductById(String id)throws ParseException{
		log.info("Get JFS Product By ID {}",id);
		return jfsProductDao.getProductById(id);
	}
	
}
