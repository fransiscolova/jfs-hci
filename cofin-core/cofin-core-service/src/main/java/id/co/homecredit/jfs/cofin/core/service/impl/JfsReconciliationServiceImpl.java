package id.co.homecredit.jfs.cofin.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import id.co.homecredit.jfs.cofin.core.model.JfsReconciliation;
import id.co.homecredit.jfs.cofin.core.dao.JfsReconciliationDao;
import id.co.homecredit.jfs.cofin.core.service.JfsReconciliationService;

/**
 * Service Implement Class For {@link JfsReconciliationService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsReconciliationServiceImpl implements JfsReconciliationService{
	private static final Logger log = LogManager.getLogger(JfsReconciliationServiceImpl.class);
	
	@Autowired
	protected JfsReconciliationDao jfsReconciliationDao;
	
	@Override
	public List<JfsReconciliation> getReconciliationByContractNumber(String contractNumber){
		log.info("Get Value Of JFS Reconciliation By Contract Number {}",contractNumber);
		return jfsReconciliationDao.getReconciliationByContractNumber(contractNumber);
	}
	
}
