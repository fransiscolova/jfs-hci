package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsScheduleBslDao;
import id.co.homecredit.jfs.cofin.core.model.JfsScheduleBsl;
import id.co.homecredit.jfs.cofin.core.service.JfsScheduleBslService;

/**
 * Service Implement Class For {@link JfsScheduleBslService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsScheduleBslServiceImpl implements JfsScheduleBslService{
	private static final Logger log = LogManager.getLogger(JfsScheduleBslServiceImpl.class);
	
	@Autowired
	protected JfsScheduleBslDao jfsScheduleBslDao;
	
	@Override
	public List<JfsScheduleBsl> getScheduleBslByContractNumber(String contractNumber){
		log.info("Get Value Of BSL Schedule By Contract Number {}",contractNumber);
		return jfsScheduleBslDao.getScheduleBslByContractNumber(contractNumber);
	}
	
}
