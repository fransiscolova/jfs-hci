package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.JfsScheduleDao;
import id.co.homecredit.jfs.cofin.core.model.JfsSchedule;
import id.co.homecredit.jfs.cofin.core.service.JfsScheduleService;

/**
 * Service Implement Class For {@link JfsScheduleService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class JfsScheduleServiceImpl implements JfsScheduleService{
	private static final Logger log = LogManager.getLogger(JfsScheduleServiceImpl.class);
	
	@Autowired
	protected JfsScheduleDao jfsScheduleDao;
	
	@Override
	public JfsSchedule getScheduleJfs(String contractNumber,Integer partIndex){
		log.info("Get Value Of Bank Schedule By Contract Number {} And Part Index {}",contractNumber,partIndex);
		return jfsScheduleDao.getScheduleJfs(contractNumber,partIndex);
	}
	
}
