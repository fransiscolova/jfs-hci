package id.co.homecredit.jfs.cofin.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.model.enumeration.LoginEnum;
import id.co.homecredit.jfs.cofin.core.dao.LoginLogDao;
import id.co.homecredit.jfs.cofin.core.model.LoginLog;
import id.co.homecredit.jfs.cofin.core.service.LoginLogService;

/**
 * Service implement class for {@link LoginLog}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class LoginLogServiceImpl implements LoginLogService {
    private static final Logger log = LogManager.getLogger(LoginLogServiceImpl.class);

    @Autowired
    private LoginLogDao loginLogDao;

    @Override
    public LoginLog getLatestLoginLogByUsername(String username) {
        log.info("get latest login log by username {}", username);
        return loginLogDao.getLatestLoginLogByUsername(username);
    }

    @Override
    public void saveUserLogin(String username) {
        log.info("save LOGIN login log for username {}", username);
        LoginLog loginLog = new LoginLog();
        loginLog.setCreatedBy(username);
        loginLog.setAction(LoginEnum.LOGIN);
        loginLogDao.save(loginLog);
    }

    @Override
    public void saveUserLogout(String username) {
        log.info("save LOGOUT login log for username {}", username);
        LoginLog loginLog = new LoginLog();
        loginLog.setCreatedBy(username);
        loginLog.setAction(LoginEnum.LOGOUT);
        loginLogDao.save(loginLog);
    }

}
