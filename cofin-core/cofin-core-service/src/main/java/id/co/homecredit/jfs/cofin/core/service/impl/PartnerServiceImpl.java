package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.PartnerDao;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.service.PartnerService;

/**
 * Service implement for {@link Partner}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class PartnerServiceImpl implements PartnerService {
    private static final Logger log = LogManager.getLogger(PartnerServiceImpl.class);

    @Autowired
    private PartnerDao partnerDao;

    @Override
    public void deletePartnerById(String partnerId) {
        log.info("delete by partner id {}", partnerId);
        partnerDao.deleteById(partnerId);
    }

    @Override
    public List<Partner> getAllPartners(Boolean includeDeleted) {
        log.info("get all partners by include deleted {}", includeDeleted);
        return partnerDao.getAll(includeDeleted);
    }

    @Override
    public Partner getPartnerByCode(String partnerCode) {
        log.info("get partner by partner code {}", partnerCode);
        return partnerDao.getPartnerByCode(partnerCode);
    }

    @Override
    public Partner getPartnerById(String partnerId) {
        log.info("get partner by partner id {}", partnerId);
        return partnerDao.get(partnerId);
    }

    @Override
    public Partner getPartnerByName(String name) {
        partnerDao.getPartnerByName(name);
        return null;
    }

    @Override
    public Partner savePartner(Partner partner) {
        log.info("save partner {}", partner.toString());
        return partnerDao.save(partner);
    }

}
