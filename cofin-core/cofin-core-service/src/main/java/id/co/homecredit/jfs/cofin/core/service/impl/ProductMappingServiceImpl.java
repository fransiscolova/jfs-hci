package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ProductMappingDao;
import id.co.homecredit.jfs.cofin.core.model.ProductMapping;
import id.co.homecredit.jfs.cofin.core.service.ProductMappingService;

/**
 * Service implement class for {@link ProductMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class ProductMappingServiceImpl implements ProductMappingService {
    private static final Logger log = LogManager.getLogger(ProductMappingServiceImpl.class);

    @Autowired
    private ProductMappingDao productMappingDao;

    @Override
    public void deleteProductMappingById(String productMappingId) {
        log.info("delete product mapping by product mapping id {}", productMappingId);
        productMappingDao.deleteById(productMappingId);
    }

    @Override
    public List<ProductMapping> getAllProductMappingsByAgreementIdAndDate(String agreementId,
            Date date) {
        log.info("get all product mappings by agreement id {}", agreementId);
        return productMappingDao.getAllProductMappingsByAgreementIdAndDate(agreementId, date);
    }

    @Override
    public List<ProductMapping> getAllProductMappingsByProductCodeAndDate(String productCode,
            Date date) {
        log.info("get all product mappings by product code {}", productCode);
        return productMappingDao.getAllProductMappingsByProductCodeAndDate(productCode, date);
    }

    @Override
    public ProductMapping getProductMappingById(String productMappingId) {
        return productMappingDao.get(productMappingId);

    }

    @Override
    public ProductMapping saveProductMapping(ProductMapping productMapping) {
        log.info("save product mapping {}", productMapping.toString());
        return productMappingDao.save(productMapping);
    }

}
