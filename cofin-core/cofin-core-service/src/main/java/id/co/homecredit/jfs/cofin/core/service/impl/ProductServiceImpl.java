package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ProductDao;
import id.co.homecredit.jfs.cofin.core.model.Product;
import id.co.homecredit.jfs.cofin.core.service.ProductService;

/**
 * Service implement class for {@link Product}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger log = LogManager.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductDao productDao;

    @Override
    public void deleteProductByCode(String productCode) {
        log.info("delete product by product code {}", productCode);
        productDao.deleteById(productCode);
    }

    @Override
    public List<Product> getAllProductsByIncludeDeleted(Boolean includeDeleted) {
        log.info("get all products by include deleted {}", includeDeleted);
        return productDao.getAll(includeDeleted);
    }

    @Override
    public Product getProductByCode(String productCode) {
        log.info("get product by product code {}", productCode);
        return productDao.get(productCode);
    }

    @Override
    public Product saveProduct(Product product) {
        log.info("save product {}", product.toString());
        return productDao.save(product);
    }

}
