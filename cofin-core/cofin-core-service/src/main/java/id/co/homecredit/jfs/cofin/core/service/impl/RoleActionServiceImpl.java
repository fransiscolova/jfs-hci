package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.RoleActionDao;
import id.co.homecredit.jfs.cofin.core.model.RoleAction;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;
import id.co.homecredit.jfs.cofin.core.service.RoleActionService;

/**
 * Service implement class for {@link RoleAction}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class RoleActionServiceImpl implements RoleActionService {
    private static final Logger log = LogManager.getLogger(RoleActionServiceImpl.class);

    @Autowired
    private RoleActionDao roleActionDao;

    @Override
    public RoleAction getRoleActionById(String id) {
      //  log.info("get role action by id {}", id);
        return roleActionDao.get(id);
    }

    @Override
    public List<RoleAction> getAll() {
       // log.info("get all active role actions");
        return roleActionDao.getAll(false);
    }

    @Override
    public List<RoleAction> getAllByRole(RoleEnum role) {
       // log.info("get all role actions by role {}", role);
        return roleActionDao.getAllRoleActionsByRole(role);
    }

    @Override
    public List<RoleAction> getRoleActionsByAction(String actionUrl) {
       // log.info("get all role actions by action {}", actionUrl);
        return roleActionDao.getAllRoleActionsByAction(actionUrl);
    }

    @Override
    public RoleAction saveRoleAction(RoleAction roleAction) {
        log.info("save role action {}", roleAction);
        return roleActionDao.save(roleAction);
    }

}
