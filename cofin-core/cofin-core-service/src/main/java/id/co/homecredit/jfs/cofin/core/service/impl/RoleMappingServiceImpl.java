package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.RoleMappingDao;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;
import id.co.homecredit.jfs.cofin.core.service.RoleMappingService;

/**
 * Service implement class for {@link RoleMapping}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class RoleMappingServiceImpl implements RoleMappingService {
    private static final Logger log = LogManager.getLogger(RoleMappingServiceImpl.class);

    @Autowired
    private RoleMappingDao roleMappingDao;

    @Override
    public void deleteRoleMapping(RoleMapping roleMapping) {
        log.info("delete role mapping {}", roleMapping.toString());
        roleMappingDao.delete(roleMapping);
    }

    @Override
    public List<RoleMapping> getAllRoleMapping(Boolean includeDeleted) {
        log.info("get all role mappings by include deleted {}", includeDeleted);
        return roleMappingDao.getAll(includeDeleted);
    }

    @Override
    public List<RoleMapping> getAllRoleMappingByUsername(String username) {
        log.info("get all role mappings by username {}", username);
        return roleMappingDao.getAllRoleMappingsByUsername(username);
    }

    @Override
    public RoleMapping getRoleMappingById(String id) {
        // TODO Auto-generated method stub
        return roleMappingDao.get(id);

    }

    @Override
    public RoleMapping saveRoleMapping(RoleMapping roleMapping) {
        log.info("save role mapping {}", roleMapping.toString());
        return roleMappingDao.save(roleMapping);
    }
    
    @Override
    public List<RoleMapping> getAllValidRoleMappingsByUsername(String username,Date validDate){
    	log.info("Get Valid Role Maping By Username {}",username);
    	return roleMappingDao.getAllValidRoleMappingsByUsername(username,validDate);
    }

}
