package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;
import java.text.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.RoleDao;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;
import id.co.homecredit.jfs.cofin.core.service.RoleService;

/**
 * Service implement class for {@link Role}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class RoleServiceImpl implements RoleService {
    private static final Logger log = LogManager.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleDao roleDao;

    @Override
    public List<Role> getAllRoles(Boolean includeDeleted) {
        log.info("get all roles by include deleted {}", includeDeleted);
        return roleDao.getAll(includeDeleted);
    }

    @Override
    public List<Role> getAllUnsedRole(List<Enum> col) {
        // TODO Auto-generated method stub
        return roleDao.getAllUnsedRole(col);

    }

    @Override
    public Role getRolesByRole(RoleEnum role) {
        // TODO Auto-generated method stub
        return roleDao.get(role);
    }
    
    @Override
    public Role saveRoleMaster(Role role){
    	log.info("Save Role {}", role);
    	return roleDao.save(role);
    }
    
    @Override
    public List<Role> getListRoles(List<Enum> col){
    	return roleDao.getListRoles(col);
    }
    
    @Override
    public Role getRoleValueByRole(RoleEnum role)throws ParseException{
    	log.info("Get Role Value By Role {}",role);
    	return roleDao.getRoleValueByRole(role);
    }
    
    @Override
    public boolean statusUpdateRole(String role, String status, String updatedBy, String updatedDate, String version){
    	log.info("Update Status Value Of Role {}",role);
    	String query = "update COFIN_ROLE set DELETED = '"+status+"', UPDATED_BY = '"+updatedBy+"', UPDATED_DATE = '"+updatedDate+"', VERSION = '"+version+"' where ROLE = '"+role+"'";
    	return roleDao.updateObjectWithQuery(query);
    }
    
}
