package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.TempJfsConfirmationFileDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class TempJfsConfirmationFileServiceImpl implements TempJfsConfirmationFileService {
    private static final Logger log = LogManager.getLogger(TempJfsConfirmationFileServiceImpl.class);

    @Autowired
    private TempJfsConfirmationFileDao tempJfsConfirmationFileDao;

	@Override
	public List<TempJfsConfirmationFile> saveConfirmation(List<TempJfsConfirmationFile> confirmationFile) {
		// TODO Auto-generated method stub
		return tempJfsConfirmationFileDao.save(confirmationFile);
	}

	@Override
	public String saveConfirmation(TempJfsConfirmationFile confirmationFile) {
		// TODO Auto-generated method stub
		return tempJfsConfirmationFileDao.save(confirmationFile).getId();
		
	}

	@Override
	public void runProcedurePermata(String query) {
		// TODO Auto-generated method stub
		
		tempJfsConfirmationFileDao.runProcedurePermata(query);
		 
	}

  

}
