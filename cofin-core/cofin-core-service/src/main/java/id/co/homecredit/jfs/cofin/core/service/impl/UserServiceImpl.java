package id.co.homecredit.jfs.cofin.core.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.UserDao;
import id.co.homecredit.jfs.cofin.core.model.User;
import id.co.homecredit.jfs.cofin.core.service.UserService;

/**
 * Service implement class for {@link User}.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Override
    public void deleteUser(User user) {
        log.info("delete user {}", user.toString());
        userDao.delete(user);
    }

    @Override
    public List<User> getAllUnmappedUsers() {
        log.info("get all unmapped users");
        return userDao.getAllUnmappedUsers();
    }

    @Override
    public List<User> getAllUsers(Boolean includeDeleted) {
        log.info("get all users by include deleted {}", includeDeleted);
        return userDao.getAll(includeDeleted);
    }

    @Override
    public User getUserByUserName(String userName) {
        log.info("get user by username {}", userName);
        return userDao.getUserByUserName(userName);
    }

    @Override
    public User saveUser(User user) {
        log.info("save user {}", user.toString());
        return userDao.save(user);
    }
}
