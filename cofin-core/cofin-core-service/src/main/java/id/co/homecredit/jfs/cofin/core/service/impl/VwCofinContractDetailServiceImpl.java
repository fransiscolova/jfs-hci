package id.co.homecredit.jfs.cofin.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.homecredit.jfs.cofin.core.dao.VwCofinContractDetailDao;
import id.co.homecredit.jfs.cofin.core.model.VwCofinContractDetail;
import id.co.homecredit.jfs.cofin.core.service.VwCofinContractDetailService;

/**
 * Service Implement Class For {@link VwCofinContractDetailService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class VwCofinContractDetailServiceImpl implements VwCofinContractDetailService{
	private static final Logger log = LogManager.getLogger(VwCofinContractDetailServiceImpl.class);
	
	@Autowired
	protected VwCofinContractDetailDao vwCofinContractDetailDao;
	
	@Override
	public VwCofinContractDetail getDetailContractByContractNumber(String contractNumber){
		log.info("Get Detail Contract By Contract Number {}",contractNumber);
		return vwCofinContractDetailDao.getDetailContractByContractNumber(contractNumber);
	}
	
}
