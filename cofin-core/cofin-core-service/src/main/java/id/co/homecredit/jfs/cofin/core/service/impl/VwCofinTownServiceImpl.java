package id.co.homecredit.jfs.cofin.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.core.dao.VwCofinTownDao;
import id.co.homecredit.jfs.cofin.core.service.VwCofinTownService;

/**
 * Service Implement Class For {@link VwCofinTownService}
 * 
 * @author denny.afrizal01
 *
 */
@Service
public class VwCofinTownServiceImpl implements VwCofinTownService{
	private static final Logger log = LogManager.getLogger(VwCofinTownServiceImpl.class);
	
	@Autowired
	protected VwCofinTownDao vwCofinTownDao;
	
	@Override
	public String getTownDescription(String townCode){
		log.info("Get Town Description By Town Code {}",townCode);
		return vwCofinTownDao.getTownDescription(townCode);
	}
	
	@Override
	public String getSubdistrictDescription(String subdistrictCode){
		log.info("Get Subdistrict Description By Subdistrict Code {}",subdistrictCode);
		return vwCofinTownDao.getSubdistrictDescription(subdistrictCode);
	}
	
	@Override
	public String getDistrictDescription(String districtCode){
		log.info("Get District Description By District Code {}",districtCode);
		return vwCofinTownDao.getDistrictDescription(districtCode);
	}
	
}
