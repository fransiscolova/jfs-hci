package id.co.homecredit.jfs.cofin.core.service.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.dao.RoleMappingDao;
import id.co.homecredit.jfs.cofin.core.dao.UserDao;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;
import id.co.homecredit.jfs.cofin.core.model.User;
import id.co.homecredit.jfs.cofin.core.service.LoginLogService;

/**
 * Modified user detail context mapper impl for cofin.
 *
 * @author Muhammad.Agaputra
 * @author muhammad.muflihun
 */
@Transactional
public class UserDetailsContextMapperImpl implements UserDetailsContextMapper, Serializable {
    private static final long serialVersionUID = 3962976258168853954L;
    private static final Logger log = LogManager.getLogger(UserDetailsContextMapperImpl.class);

    @Autowired
    private RoleMappingDao roleMappingDao;
    @Autowired
    private UserDao userDao;	
    @Autowired
    private LoginLogService loginLogService;

    @Override
    public UserDetails mapUserFromContext(DirContextOperations ctx, String username,
            Collection<? extends GrantedAuthority> authority) {
        List<GrantedAuthority> mappedAuthorities = new ArrayList<>();

        // TODO : Uncomment this if you need default giveaway role
        // // Give default ROLE_USER for authenticated user
        // mappedAuthorities.add(new SimpleGrantedAuthority(RoleEnum.USER.name()));

        // Save user login
        User user = userDao.get(username);
        loginLogService.saveUserLogin(username);

        // Get additional ROLE from database
        List<RoleMapping> roleMappings = roleMappingDao.getAllValidRoleMappingsByUsername(username,new Date());

        // Add to each additional ROLE into mappedAuthorities
        for (RoleMapping roleMapping : roleMappings) {
            mappedAuthorities
                    .add(new SimpleGrantedAuthority(roleMapping.getRole().getRole().name()));
        }

        // Get user info
        return new UserLogin(username, user.getFirstName(), user.getLastName(), "", true, true,
                true, true, mappedAuthorities);
    }

    @Override
    public void mapUserToContext(UserDetails arg0, DirContextAdapter arg1) {
    }
}
