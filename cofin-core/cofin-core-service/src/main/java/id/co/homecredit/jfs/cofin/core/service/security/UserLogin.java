package id.co.homecredit.jfs.cofin.core.service.security;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * Modified for cofin.
 *
 * @author Muhammad.Agaputra
 * @author muhammad.muflihun
 */
public class UserLogin extends User {
    private static final long serialVersionUID = -2585113257594639651L;

    private Date lastAccessDate = new Date();
    private String firstName;
    private String lastName;

    /**
     * Initialize user login by parameters:
     *
     * @param username
     * @param password
     * @param enabled
     * @param accountNonExpired
     * @param credentialsNonExpired
     * @param accountNonLocked
     * @param authorities
     */
    public UserLogin(String username, String firstName, String lastName, String password,
            boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired,
            boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, authorities);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Get user login first name.
     *
     * @return string
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get user login full name.
     *
     * @return string
     */
    public String getFullName() {
        return firstName.concat(" ").concat(lastName);
    }

    /**
     * Get user login last login date.
     *
     * @return date
     */
    public Date getLastAccessDate() {
        return lastAccessDate;
    }

    /**
     * Get user login last name.
     *
     * @return string
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set user login first name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Set user login last access date.
     */
    public void setLastAccessDate(Date lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    /**
     * Set user login last name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
