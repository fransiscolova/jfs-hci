package id.co.homecredit.jfs.cofin.core.service.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant;

/**
 * Helper class related to {@link UserLogin}.
 *
 * @author muhammad.muflihun
 *
 */
public class UserLoginUtil {
    private static final Logger log = LogManager.getLogger(UserLoginUtil.class);

    /**
     * Get current user login.
     *
     * @return userLogin
     */
    public static UserLogin getCurrentUser() {
        UserLogin userLogin = null;
        try {
            userLogin = (UserLogin) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
        } catch (Exception e) {
            log.debug("user login not found : {}", e.getLocalizedMessage());
           // log.debug(e.getStackTrace()[0]);
         
        }
        return userLogin;
    }

    /**
     * Get current user login full name.
     *
     * @return fullname
     */
    public static String getFullname() {
        return getCurrentUser().getFullName();
    }

    /**
     * Get current user login username.
     *
     * @return username
     */
    public static String getUsername() {
        return getCurrentUser().getUsername();
    }

    public UserLoginUtil() {

    }
}
