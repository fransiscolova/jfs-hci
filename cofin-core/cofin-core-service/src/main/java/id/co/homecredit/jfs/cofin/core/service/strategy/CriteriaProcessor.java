package id.co.homecredit.jfs.cofin.core.service.strategy;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;

/**
 * Interface class to be implemented for each criteria.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CriteriaProcessor {

    /**
     * Compare criteria parameter(s) with object(s).
     *
     * @param criteriaMapping
     * @param inputValues
     * @return true if object meets criteria parameters, false if don't or no logic for it yet
     */
    public Boolean compareCriteriaWithObjects(CriteriaMapping criteriaMapping,
            Object... inputValues);

}
