package id.co.homecredit.jfs.cofin.core.service.strategy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;
import id.co.homecredit.jfs.cofin.core.service.strategy.impl.AgeCriteriaProcessor;
import id.co.homecredit.jfs.cofin.core.service.strategy.impl.DpCriteriaProcessor;
import id.co.homecredit.jfs.cofin.core.service.strategy.impl.TermsCriteriaProcessor;

/**
 * Strategy class to encapsulate logic for each criteria checking.
 *
 * @author muhammad.muflihun
 *
 */
public class CriteriaStrategy {
    private static final Logger log = LogManager.getLogger(CriteriaStrategy.class);

    /**
     * Create criteria processor for specified criteria.
     *
     * @param criteria
     * @return criteriaProcessor
     */
    public CriteriaProcessor createInstance(CriteriaEnum criteria) {
        CriteriaProcessor criteriaProcessor = null;
        switch (criteria) {
            case AGE:
                criteriaProcessor = new AgeCriteriaProcessor();
                break;
            case DOWN_PAYMENT:
                criteriaProcessor = new DpCriteriaProcessor();
                break;
            case TERMS:
                criteriaProcessor = new TermsCriteriaProcessor();
                break;
            case TOTAL_OUTSTANDING_PRINCIPAL:
                break;
            case COMMODITY_TYPE:
                break;
            case COMMODITY_CATEGORY:
                break;
            case KTP_STATUS:
                break;
            default:
                break;
        }
        log.debug("result processorr is {}", criteriaProcessor.getClass());
        return criteriaProcessor;
    }
}
