package id.co.homecredit.jfs.cofin.core.service.strategy.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;
import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.EligibilityEnum;
import id.co.homecredit.jfs.cofin.core.service.strategy.CriteriaProcessor;

/**
 * Class implement for {@link CriteriaProcessor} with {@link CriteriaEnum} age.
 *
 * @author muhammad.muflihun
 *
 */
public class AgeCriteriaProcessor implements CriteriaProcessor {
    private static final Logger log = LogManager.getLogger(AgeCriteriaProcessor.class);

    /**
     * Compare criteria parameter(s) with object(s).
     *
     * <p>
     *
     * First object is date of birth in {@link Calendar}. Second object is contract first due date
     * in {@link Calendar}.
     *
     * @param criteriaMapping
     * @param inputValues
     * @return true if object meets criteria parameters, false if doesn't or no logic for it yet.
     */
    @Override
    public Boolean compareCriteriaWithObjects(CriteriaMapping criteriaMapping,
            Object... inputValues) {
        log.debug("input values {}", inputValues);
        Calendar dob = (Calendar) inputValues[0];
        Calendar firstDueDate = (Calendar) inputValues[1];

        // minus 1 month to get signing contract date
        firstDueDate.add(Calendar.MONTH, -1);
        LocalDate signingContract = LocalDate.of(firstDueDate.get(Calendar.YEAR),
                firstDueDate.get(Calendar.MONTH) + 1, firstDueDate.get(Calendar.DAY_OF_MONTH));
        LocalDate dateOfBirth = LocalDate.of(dob.get(Calendar.YEAR), dob.get(Calendar.MONTH) + 1,
                dob.get(Calendar.DAY_OF_MONTH));

        // compare date of birth with customer signing date
        Period period = Period.between(dateOfBirth, signingContract);
        Integer age = period.getYears();

        Boolean result = Boolean.FALSE;
        for (CriteriaEligibility criteriaMappingEligibility : criteriaMapping
                .getCriteriaEligibilities()) {
            String parameter = criteriaMappingEligibility.getValue();
            log.debug("parameter {}", parameter);
            Integer ageParameter = Integer.parseInt(parameter);
            log.debug("age parameter {}", ageParameter);
            EligibilityEnum eligibility = criteriaMappingEligibility.getEligibility();
            switch (eligibility) {
                case MINIMUM:
                    result = age >= ageParameter;
                    log.debug("result for eligiblity {} is {}", eligibility, result);
                    break;
                case MAXIMUM:
                    result = age <= ageParameter;
                    log.debug("result for eligiblity {} is {}", eligibility, result);
                default:
                    log.debug("eligibility is not configured yet for {}", eligibility);
                    break;
            }
        }
        log.debug("final result is {}", result);
        return result;
    }

}
