package id.co.homecredit.jfs.cofin.core.service.strategy.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import id.co.homecredit.jfs.cofin.common.util.StringUtil;
import id.co.homecredit.jfs.cofin.core.model.CommodityType;
import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;
import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.EligibilityEnum;
import id.co.homecredit.jfs.cofin.core.service.strategy.CriteriaProcessor;

/**
 * Class implement for {@link CriteriaProcessor} with {@link CriteriaEnum} commodity type.
 *
 * @author muhammad.muflihun
 *
 */
public class CommodityTypeCriteriaProcessor implements CriteriaProcessor {
    private static final Logger log = LogManager.getLogger(CommodityTypeCriteriaProcessor.class);

    /**
     * Compare criteria parameter(s) with object(s).
     *
     * <p>
     *
     * First object is commodity type code in {@link CommodityType}.
     *
     * @param criteriaMapping
     * @param inputValues
     * @return true if object meets criteria parameters, false if doesn't or no logic for it yet.
     */
    @Override
    public Boolean compareCriteriaWithObjects(CriteriaMapping criteriaMapping,
            Object... inputValues) {
        log.debug("input values {}", inputValues);
        String comType = (String) inputValues[0];
        Boolean result = Boolean.FALSE;
        for (CriteriaEligibility criteriaMappingEligibility : criteriaMapping
                .getCriteriaEligibilities()) {
            String parameter = criteriaMappingEligibility.getValue();
            log.debug("parameter {}", parameter);
            List<String> comTypeParameters = StringUtil.splitAndTrimAsList(parameter);
            log.debug("commodity type parameters {}", comTypeParameters);
            EligibilityEnum eligibility = criteriaMappingEligibility.getEligibility();
            switch (eligibility) {
                case IN:
                    result = comTypeParameters.contains(comType);
                    log.debug("result for eligiblity {} is {}", eligibility, result);
                    break;
                case NOT_IN:
                    result = !comTypeParameters.contains(comType);
                    log.debug("result for eligiblity {} is {}", eligibility, result);
                    break;
                default:
                    log.debug("eligibility is not configured yet for {}", eligibility);
                    break;
            }
        }
        log.debug("final result is {}", result);
        return result;
    }

}
