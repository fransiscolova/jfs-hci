package id.co.homecredit.jfs.cofin.core.service.strategy.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;
import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.EligibilityEnum;
import id.co.homecredit.jfs.cofin.core.service.strategy.CriteriaProcessor;

/**
 * Class implement for {@link CriteriaProcessor} with {@link CriteriaEnum} down payment.
 *
 * @author muhammad.muflihun
 *
 */
public class DpCriteriaProcessor implements CriteriaProcessor {
    private static final Logger log = LogManager.getLogger(DpCriteriaProcessor.class);

    /**
     * Compare criteria parameter(s) with object(s).
     *
     * <p>
     *
     * First object is down-payment amount in {@link BigDecimal}. Second object is contract amount
     * in {@link BigDecimal}.
     *
     * @param criteriaMapping
     * @param inputValues
     * @return true if object meets criteria parameters, false if doesn't or no logic for it yet.
     */
    @Override
    public Boolean compareCriteriaWithObjects(CriteriaMapping criteriaMapping,
            Object... inputValues) {
        log.debug("input values {}", inputValues);
        BigDecimal dpAmount = (BigDecimal) inputValues[0];
        BigDecimal contractAmount = (BigDecimal) inputValues[1];
        Boolean result = Boolean.FALSE;
        for (CriteriaEligibility criteriaMappingEligibility : criteriaMapping
                .getCriteriaEligibilities()) {
            String parameter = criteriaMappingEligibility.getValue();
            log.debug("parameter {}", parameter);
            BigDecimal dpPercentageParameter = new BigDecimal(parameter);
            log.debug("down payment percentage parameter {}", dpPercentageParameter);
            BigDecimal dpAmountParameter = contractAmount.multiply(dpPercentageParameter)
                    .divide(new BigDecimal("100"));
            log.debug("down payment amount parameter {}", dpAmountParameter);
            EligibilityEnum eligibility = criteriaMappingEligibility.getEligibility();
            switch (eligibility) {
                case MINIMUM:
                    result = dpAmount.compareTo(dpAmountParameter) < 1;
                    log.debug("result for eligiblity {} is {}", eligibility, result);
                    break;
                default:
                    log.debug("eligibility is not configured yet for {}", eligibility);
                    break;
            }
        }
        log.debug("final result is {}", result);
        return result;
    }

}
