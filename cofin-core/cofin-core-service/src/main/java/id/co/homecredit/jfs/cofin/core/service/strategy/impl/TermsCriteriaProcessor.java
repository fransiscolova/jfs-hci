package id.co.homecredit.jfs.cofin.core.service.strategy.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import id.co.homecredit.jfs.cofin.core.model.CriteriaEligibility;
import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.enumeration.CriteriaEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.EligibilityEnum;
import id.co.homecredit.jfs.cofin.core.service.strategy.CriteriaProcessor;

/**
 * Class implement for {@link CriteriaProcessor} with {@link CriteriaEnum} terms.
 *
 * @author muhammad.muflihun
 *
 */
public class TermsCriteriaProcessor implements CriteriaProcessor {
    private static final Logger log = LogManager.getLogger(TermsCriteriaProcessor.class);

    /**
     * Compare criteria parameter(s) with object(s).
     *
     * <p>
     *
     * First object is terms in {@link Integer}.
     *
     * @param criteriaMapping
     * @param inputValues
     * @return true if object meets criteria parameters, false if doesn't or no logic for it yet.
     */
    @Override
    public Boolean compareCriteriaWithObjects(CriteriaMapping criteriaMapping,
            Object... inputValues) {
        log.debug("input values {}", inputValues);
        Integer terms = (Integer) inputValues[0];
        Boolean result = Boolean.FALSE;
        for (CriteriaEligibility criteriaMappingEligibility : criteriaMapping
                .getCriteriaEligibilities()) {
            String parameter = criteriaMappingEligibility.getValue();
            log.debug("parameter {}", parameter);
            Integer termsParameter = Integer.parseInt(parameter);
            log.debug("terms percentage parameter {}", termsParameter);
            EligibilityEnum eligibility = criteriaMappingEligibility.getEligibility();
            switch (eligibility) {
                case MINIMUM:
                    result = terms >= termsParameter;
                    log.debug("result for eligiblity {} is {}", eligibility, result);
                    break;
                case MAXIMUM:
                    result = terms <= termsParameter;
                    log.debug("result for eligiblity {} is {}", eligibility, result);
                    break;
                default:
                    log.debug("eligibility is not configured yet for {}", eligibility);
                    break;
            }
        }
        log.debug("final result is {}", result);
        return result;
    }

}
