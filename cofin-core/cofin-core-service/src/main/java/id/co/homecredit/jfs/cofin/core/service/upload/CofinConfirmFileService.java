package id.co.homecredit.jfs.cofin.core.service.upload;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmProcess;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;

/**
 * Service interface class for {@link disbursement permata}.
 *
 * @author fransisco.situmorang
 *
 */
@Transactional
public interface CofinConfirmFileService {

    /**
     * get all
     *
     * @return List<CofinProcessType>
     */
	
   public List<CofinConfirmFile> getAll();
   
   /**
    * get by id
    *
    *@param id
    *
    * @return CofinProcessType
    */
	
   public CofinConfirmFile getById(String id);
   
   
   
   /*
    * get by confirmProcessID
    * 
    * 
    * @param processId
    * 
    * 
    * return list<CofinConfirmFile>
    */
   
   public List<CofinConfirmFile> getFile(String processId);
   
}
