package id.co.homecredit.jfs.cofin.core.service.upload;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFileSetting;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmProcess;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;

/**
 * Service interface class for {@link disbursement permata}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CofinConfirmFileSettingService {

    /**
     * get all
     *
     * @return List<CofinProcessType>
     */
	
   public List<CofinConfirmFileSetting> getAll();
   
   /**
    * get by id
    *
    *@param id
    *
    * @return CofinProcessType
    */
	
   public CofinConfirmFileSetting getById(String id);
   
   
   /**
    * get CofinConfirmFileSetting 
    * 
    * @param fk,recordType
    * 
    * return List<CofinConfirmFileSetting>
    */
   
   public List<CofinConfirmFileSetting> getByIdRecordType(String fk,String recordType);
   
}
