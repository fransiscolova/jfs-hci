package id.co.homecredit.jfs.cofin.core.service.upload;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmProcess;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;

/**
 * Service interface class for {@link disbursement permata}.
 *
 * @author fransisco.situmorang
 *
 */
@Transactional
public interface CofinConfirmProcessService {

    /**
     * get all
     *
     * @return List<CofinProcessType>
     */
	
   public List<CofinConfirmProcess> getAll();
   
   /**
    * get by id
    *
    *@param id
    *
    * @return CofinProcessType
    */
	
   public CofinConfirmProcess getById(String id);
   
   
   /*
    * get byId Partner
    * 
    * @param idPartner
    * 
    * @return Lis<CofinConfirmProcess>
    * 
    */
   
   public List<CofinConfirmProcess> getProcessByPartner(String partnerId);
   
   
   
   
   /*
    * get byId Partner,processId
    * 
    * @param idPartner
    * 
    * @return Lis<CofinConfirmProcess>
    * 
    */
   
   public List<CofinConfirmProcess> getProcessByPartnerAndIdType(String partnerId,String processTypeId);
   
}
