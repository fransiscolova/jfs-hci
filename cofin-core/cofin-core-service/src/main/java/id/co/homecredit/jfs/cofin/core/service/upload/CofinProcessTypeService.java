package id.co.homecredit.jfs.cofin.core.service.upload;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;

/**
 * Service interface class for {@link disbursement permata}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CofinProcessTypeService {

    /**
     * get all
     *
     * @return List<CofinProcessType>
     */
	
   public List<CofinProcessType> getAll();
   
   /**
    * get by id
    *
    *@param id
    *
    * @return CofinProcessType
    */
	
   public CofinProcessType getById(String id);
   
   
   
   /* get by FK_PROCESS_CATEGORY
   *
   *@param catgeoryID
   *
   * @return List<CofinProcessType>
   */
	
   public List<CofinProcessType> getByCategoryId(String categoryId);
   
   
   public void runQuery(String query);
   
   
   void executeProcedure(String query);
   
   
   
   
}
