package id.co.homecredit.jfs.cofin.core.service.upload;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Role;

/**
 * Service interface class for {@link disbursement permata}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface PermataFileUploadService {

    /**
     * read file disbursemnt to upload.
     *
     * @param path,fileUpload
     *
     * @return path,fileUpload
     */
    public BigDecimal readDisbursementFile(String path,FileUpload fileUpload);
    
    
    /**
     * read file payment to upload.
     *
     * @param path,fileUpload
     *
     * @return path,fileUpload
     */
    public BigDecimal readFilePayement(String path,FileUpload fileUpload);

    

}
