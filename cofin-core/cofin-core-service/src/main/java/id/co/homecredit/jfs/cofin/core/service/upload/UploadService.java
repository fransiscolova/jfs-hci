package id.co.homecredit.jfs.cofin.core.service.upload;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;

/**
 * Service interface class for {@link disbursement permata}.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface UploadService {

    /**
     * read file disbursemnt to upload.
     *
     * @param path
     *
     * @return string
     */
	
     
    
    public BigDecimal convertorFile(String path,FileUpload file,String idConfirmFile);
    
    
    
    public String checkFile(String path,FileUpload file,String idConfirmFile);
    
    
}
