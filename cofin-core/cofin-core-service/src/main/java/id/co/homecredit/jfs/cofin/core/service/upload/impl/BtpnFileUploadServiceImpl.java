package id.co.homecredit.jfs.cofin.core.service.upload.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.JSONUtil;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.ImportTempFile;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.ImportTempFileService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.BtpnFileUploadService;
import id.co.homecredit.jfs.cofin.core.service.upload.UploadService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class BtpnFileUploadServiceImpl implements BtpnFileUploadService {
	private static final Logger log = LogManager.getLogger(BtpnFileUploadServiceImpl.class);

	@Autowired
	TempJfsConfirmationFileService tempJfsConfirmationFileService;

	@Autowired
	ImportTempFileService importTempFileService;

	@Autowired
	UploadService uploadService;

	@Autowired
	FileUploadService fileUploadService;

	@Override
	public BigDecimal readDisbursementFile(String path, FileUpload fileUpload) {
		try {
			Boolean content = false;
			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			String taggalCetak = "";
			String noBatch = "";
			String perTanggal = "";
			String splitRate = "";
			int validPos = 0;
			ArrayList<TempJfsConfirmationFile> arrayConfirmationFile = new ArrayList<TempJfsConfirmationFile>();

			while ((readLine = b.readLine()) != null) {
				if (validPos == 0 && !readLine.contains("KONFIRMASI PENCAIRAN KONTRAK JOINT FINANCE")) {
					log.info("Wrong format BTPN");
					return BigDecimal.ZERO;
				}

				if (validPos == 1) {
					// get batch number
					noBatch = readLine.trim().split(":")[1];
					log.info(noBatch);
				}

				if (validPos == 2) {
					// get tanggal
					perTanggal = readLine.trim().split(":")[1];
					log.info(perTanggal);
				}

				if (validPos == 5) {
					// get split rate
					splitRate = readLine.trim().replace("%", "").split(":")[1];
					log.info(splitRate);
				}

				if (validPos == 6) {
					// get tanggal cetak
					taggalCetak = readLine.trim().split(":")[1].split(" ")[1];
					log.info("pertanggal " + taggalCetak);
				}

				// content
				try {
					TempJfsConfirmationFile confirmationFile = new TempJfsConfirmationFile();
					String[] splitRow = readLine.trim().split("\\s{2,}");					
					if (splitRow.length > 8 && !readLine.contains("PROVISI") ) {
						String[] getTenor = splitRow[9].split(" ");
						confirmationFile.setRecord("C");
						confirmationFile.setChecksum(0.0);
						confirmationFile.setBatchNumber(noBatch.replace("\\s{2,}", ""));
						confirmationFile.setTextContractNumber(splitRow[0].split(" ")[1]);
						confirmationFile.setClientName(splitRow[2]);
						confirmationFile.setBankTenor(Double.parseDouble(getTenor[0]));
						confirmationFile.setBatchDate(DateUtil.stringToDateWithFormat(perTanggal.trim(), "dd-MM-yyyy"));
						confirmationFile.setExportTime(DateUtil.stringToDateWithFormat(taggalCetak.trim(), "dd-MM-yyyy"));
						confirmationFile.setSplitRate(Double.parseDouble(splitRate));
						confirmationFile.setAmtBankPrincipal(new BigDecimal(splitRow[3].replace(",", "")));
						confirmationFile.setAmtBankInterest(new BigDecimal(splitRow[4].replace(",", "")));
						confirmationFile.setAmtPrincipal(new BigDecimal(splitRow[5].replace(",", "")));
						confirmationFile.setBankRate(new BigDecimal(splitRow[6].replace(",", "")));
						confirmationFile.setAmtBankProvision(Double.parseDouble(splitRow[9]));
						confirmationFile.setAmtBankAdminFee(Double.parseDouble(splitRow[10].replace(",", "")));
						confirmationFile.setFileUpload(fileUpload);

						
						arrayConfirmationFile.add(confirmationFile);

					}

					validPos++;

				} catch (Exception e) {
					e.printStackTrace();
					// return;

				}

			}
			
			tempJfsConfirmationFileService.saveConfirmation(arrayConfirmationFile);

			return new BigDecimal(arrayConfirmationFile.size());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return BigDecimal.ZERO;
	}

	@Override
	public BigDecimal readHasilPemeriksanaan(String path, FileUpload fileUpload) {
		// TODO Auto-generated method stub

		try {
			Boolean content = false;
			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			String noSession = "";
			String agreement = "";
			String agreementNo = "";
			String noSurat = "";
			String tanggalSurat = "";

			int validPos = 0;

			ArrayList<ImportTempFile> arrayConfirmationFile = new ArrayList<ImportTempFile>();

			while ((readLine = b.readLine()) != null) {
				if (validPos == 0 && !readLine.contains("LAPORAN HASIL PEMERIKSAAN (LHP)")) {
					log.info("Wrong format BTPN");
					return BigDecimal.ZERO;
				}

				if (validPos == 1) {
					noSession = readLine.split("\\|")[1];
				}

				if (validPos == 3) {
					agreement = readLine.split("\\|")[1];
				}

				if (validPos == 4) {
					agreementNo = readLine.split("\\|")[1];
				}

				if (validPos == 5) {
					noSurat = readLine.split("\\|")[1];
				}

				if (validPos == 6) {
					noSurat = readLine.split("\\|")[1];
				}

				
				// content
				try {
					ImportTempFile hasilPemeriksaan = new ImportTempFile();
					String[] splitLine = readLine.split("\\|");
					if (validPos > 8) {
						hasilPemeriksaan.setCol1(splitLine[0]);
						hasilPemeriksaan.setCol2(splitLine[1]);
						hasilPemeriksaan.setCol3(splitLine[2]);
						hasilPemeriksaan.setCol4(splitLine[3]);
						hasilPemeriksaan.setCol5(splitLine[4]);
						hasilPemeriksaan.setCol6(splitLine[5]);
						hasilPemeriksaan.setCol7(splitLine[6]);
						hasilPemeriksaan.setCol8(splitLine[7]);
						if (splitLine.length == 9) {
							hasilPemeriksaan.setCol9(splitLine[8]);
						} else {
							hasilPemeriksaan.setCol9("");
						}
						hasilPemeriksaan.setCol10(agreement);
						hasilPemeriksaan.setCol11(agreementNo);
						hasilPemeriksaan.setCol12(noSurat);
						hasilPemeriksaan.setCol13(tanggalSurat);
						hasilPemeriksaan.setFileUpload(fileUpload);

						arrayConfirmationFile.add(hasilPemeriksaan);

					}

				} catch (Exception e) {
					e.printStackTrace();
					// return;

				}
				
				validPos++;

			}

			log.info("save Hasil Pemeriksaan ");
			importTempFileService.saveHasilPemeriksaan(arrayConfirmationFile);
			

			return new BigDecimal(arrayConfirmationFile.size());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return BigDecimal.ZERO;
	}

}
