package id.co.homecredit.jfs.cofin.core.service.upload.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmFileDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinConfirmFileSettingDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinProcessTypeDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFileSetting;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.BtpnFileUploadService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinConfirmFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinConfirmFileSettingService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinProcessTypeService;
import id.co.homecredit.jfs.cofin.core.service.upload.PermataFileUploadService;
import id.co.homecredit.jfs.cofin.core.service.upload.UploadService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class CofinConfirmFileServiceImpl implements CofinConfirmFileService {
	private static final Logger log = LogManager.getLogger(CofinConfirmFileServiceImpl.class);
	
	
	
	@Autowired
	CofinConfirmFileDao cofinConfirmFileDao;

	@Override
	public List<CofinConfirmFile> getAll() {
		// TODO Auto-generated method stub
		return cofinConfirmFileDao.getAll(false);
	}

	@Override
	public CofinConfirmFile getById(String id) {
		// TODO Auto-generated method stub
		return cofinConfirmFileDao.getByID(id);
	}

	@Override
	public List<CofinConfirmFile> getFile(String processId) {
		// TODO Auto-generated method stub
		return cofinConfirmFileDao.getFile(processId);
	}
	
	
}
