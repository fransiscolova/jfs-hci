package id.co.homecredit.jfs.cofin.core.service.upload.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.dao.upload.CofinProcessTypeDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.BtpnFileUploadService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinProcessTypeService;
import id.co.homecredit.jfs.cofin.core.service.upload.PermataFileUploadService;
import id.co.homecredit.jfs.cofin.core.service.upload.UploadService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class CofinProcessTypeServiceImpl implements CofinProcessTypeService {
	private static final Logger log = LogManager.getLogger(CofinProcessTypeServiceImpl.class);
	
	
	@Autowired
	CofinProcessTypeDao cofinProcessTypeDao;

	@Override
	public List<CofinProcessType> getAll() {
		// TODO Auto-generated method stub
		return cofinProcessTypeDao.getAll(false);
	}

	@Override
	public CofinProcessType getById(String id) {
		// TODO Auto-generated method stub
		return cofinProcessTypeDao.get(id);
	}

	@Override
	public void runQuery(String query) {
		// TODO Auto-generated method stub
		
		cofinProcessTypeDao.runProcedurePermata(query);
	}

	@Override
	public List<CofinProcessType> getByCategoryId(String categoryId) {
		// TODO Auto-generated method stub
		return cofinProcessTypeDao.getByCategoryId(categoryId);
	}

	@Override
	public void executeProcedure(String query) {
		// TODO Auto-generated method stub
		cofinProcessTypeDao.executeProcedure(query);
	}

	

}
