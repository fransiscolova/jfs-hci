package id.co.homecredit.jfs.cofin.core.service.upload.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.JSONUtil;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationAllocation;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileAllocationService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.PermataFileUploadService;
import id.co.homecredit.jfs.cofin.core.service.upload.UploadService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class PermataFileUploadServiceImpl implements PermataFileUploadService {
	private static final Logger log = LogManager.getLogger(PermataFileUploadServiceImpl.class);

	@Autowired
	TempJfsConfirmationFileService tempJfsConfirmationFileService;
	
	@Autowired
	TempJfsConfirmationFileAllocationService tempJfsConfirmationFileAllocationService;
	
	@Autowired
	UploadService uploadService;

	@Autowired
	FileUploadService fileUploadService;

	@Override
	public BigDecimal readDisbursementFile(String path, FileUpload fileUpload) {
		try {
			Boolean content = false;
			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			Boolean isHeader = true;
			String taggalCetak = "";
			String noSurat = "";
			String perTanggal = "";
			ArrayList<TempJfsConfirmationFile> arrayConfirmationFile = new ArrayList<TempJfsConfirmationFile>();
			int validPos = 0;

			while ((readLine = b.readLine()) != null) {
				if (validPos == 1 && !readLine.contains("PT BANK PERMATA, tbk.")) {
					log.info("Wrong format PERMATA");
					return BigDecimal.ZERO;
				}
				
				// header

				if (readLine.contains("PT BANK PERMATA,")) {
					// get tanggal cetak
					taggalCetak = readLine.trim().split(":")[1];
				}

				if (readLine.trim().contains("NOMOR SURAT")) {
					// get no surat
					noSurat = readLine.trim().split(":")[1];
				}

				if (readLine.contains("PER TANGGAL")) {
					// get pertanggal
					perTanggal = readLine.trim().split(":")[1];
				}

				// content
					try {
						TempJfsConfirmationFile confirmationFile = new TempJfsConfirmationFile();
						String[] splitRow = readLine.split("\\s{2,}");
						
						if (splitRow.length>8 && !readLine.contains("NO.PINJAMAN")) {
							
							log.info(readLine);
							String[] getTenor = splitRow[5].split(" ");
							confirmationFile.setRecord("C");
							confirmationFile.setChecksum(32768.0);
							confirmationFile.setBatchNumber(noSurat.replace("\\s{2,}", ""));
							confirmationFile.setTextContractNumber(splitRow[1]);
							confirmationFile.setClientName(splitRow[2]);
							confirmationFile.setBankTenor(Double.parseDouble(getTenor[1]));
							confirmationFile.setBatchDate(DateUtil.stringToDateWithFormat(perTanggal, "dd/MM/yyyy"));
							confirmationFile.setExportTime(DateUtil.stringToDateWithFormat(taggalCetak, "dd/MM/yyyy"));
							confirmationFile.setSplitRate(0.9);
							confirmationFile.setAmtBankPrincipal(new BigDecimal(splitRow[8].replace(",", "")));
							confirmationFile.setAmtBankInterest(BigDecimal.ZERO);
							confirmationFile.setAmtPrincipal(new BigDecimal(splitRow[7].replace(",", "")));
							confirmationFile.setBankRate(new BigDecimal(splitRow[11].replace(",", "")));
							confirmationFile.setAmtBankProvision(0.0);
							confirmationFile.setAmtBankAdminFee(0.0);
							confirmationFile.setFileUpload(fileUpload);
							arrayConfirmationFile.add(confirmationFile);

						}

					} catch (Exception e) {
						e.printStackTrace();
						// return;

					}

				
					validPos++;
			}

			tempJfsConfirmationFileService.saveConfirmation(arrayConfirmationFile);
			tempJfsConfirmationFileAllocationService.runProcedurePermata("update cofin_file_upload set created_by='frans' where id='CCB8C85CDDFC4BB8BA64CBEAF2EA32FE'");
			
			return new BigDecimal(arrayConfirmationFile.size());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return BigDecimal.ZERO;
	}

	@Override
	public BigDecimal readFilePayement(String path, FileUpload fileUpload) {
		try {
			Boolean content = false;
			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			Boolean isHeader = true;
			String taggalCetak = "";
			String noSurat = "";
			String perTanggal = "";
			ArrayList<TempJfsConfirmationAllocation> arrayConfirmationFile = new ArrayList<TempJfsConfirmationAllocation>();
			int validPos = 0;
			int counter = 0;

			while ((readLine = b.readLine()) != null) {
				if (validPos == 3 && !readLine.contains("LAPORAN REALISASI PEMBAYARAN ANGSURAN")) {
					log.info("Wrong format PERMATA");
					return BigDecimal.ZERO;
				}
				
				// header

				if (validPos==0) {
					// get no surat
					taggalCetak = readLine.trim().split(":")[1];
				}
				
				if (validPos==4) {
					// get pertanggal					
					perTanggal =  readLine.trim().split("\\s{2,}")[1];
				}
				
				if (validPos==6) {
					// get no surat
					noSurat = readLine.trim().split(":")[1];
				}

				

				// content
					try {
						TempJfsConfirmationAllocation confirmationAllocation = new TempJfsConfirmationAllocation();

						String[] splitRow = readLine.split("\\s{2,}");
						if (splitRow.length>8 && !readLine.contains("TGL BAYAR") && !readLine.contains("SUB TOTAL")
								&& !readLine.contains("GRAND TOTAL") && !readLine.contains("BUNGA PL") ) {
							log.info(readLine);
							confirmationAllocation.setRecordNo(splitRow[0].split(" ")[1]);
							confirmationAllocation.setBatchNumber(noSurat);
							confirmationAllocation.setBatchDate(DateUtil.stringToDateWithFormat(perTanggal, "dd/MM/yyyy"));
							confirmationAllocation.setExportTime(DateUtil.stringToDateWithFormat(taggalCetak, "dd/MM/yyyy"));
							confirmationAllocation.setContractNumber(splitRow[0].split(" ")[2]);
							confirmationAllocation.setClientName(splitRow[1]);
							confirmationAllocation.setDueDate(DateUtil.stringToDateWithFormat(splitRow[3], "dd/MM/yyyy"));
							confirmationAllocation.setPayemntDate(DateUtil.stringToDateWithFormat(splitRow[4], "dd/MM/yyyy"));
							confirmationAllocation.setInstalment(new BigDecimal(splitRow[2]));
							confirmationAllocation.setAmtPrincipal(new BigDecimal(splitRow[5].replaceAll(",", "")));
							confirmationAllocation.setAmtBankInterest(new BigDecimal(splitRow[6].replaceAll(",", "")));
							confirmationAllocation.setInterestAdvance(new BigDecimal(splitRow[8].replaceAll(",", "")));
							confirmationAllocation.setAmtPenalty(new BigDecimal(splitRow[9].replaceAll(",", "")));					
							confirmationAllocation.setFileUpload(fileUpload);
							
							log.info(JSONUtil.convertToJSON(confirmationAllocation));
							arrayConfirmationFile.add(confirmationAllocation);

						}

					} catch (Exception e) {
							e.printStackTrace();
						// return;

					}

					validPos++;

			}

			tempJfsConfirmationFileAllocationService.saveConfirmation(arrayConfirmationFile);
		
			return new BigDecimal(arrayConfirmationFile.size());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return BigDecimal.ZERO;
	}

}
