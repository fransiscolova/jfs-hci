package id.co.homecredit.jfs.cofin.core.service.upload.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.security.auth.callback.ConfirmationCallback;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.JSONUtil;
import id.co.homecredit.jfs.cofin.core.dao.ActionDao;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFileSetting;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.BtpnFileUploadService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinConfirmFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinConfirmFileSettingService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinProcessTypeService;
import id.co.homecredit.jfs.cofin.core.service.upload.PermataFileUploadService;
import id.co.homecredit.jfs.cofin.core.service.upload.UploadService;

/**
 * Service implement class for {@link Role}.
 *
 * @author fransisco.situmorang
 *
 */
@Service
public class UploadServiceImpl implements UploadService {
	private static final Logger log = LogManager.getLogger(UploadServiceImpl.class);

	@Autowired
	PermataFileUploadService permataFileUploadService;

	@Autowired
	BtpnFileUploadService btpnFileUploadService;

	@Autowired
	FileUploadService fileUploadService;

	@Autowired
	CofinConfirmFileService cofinConfirmFileService;

	@Autowired
	CofinConfirmFileSettingService cofinConfirmFileSettingService;

	@Autowired
	CofinProcessTypeService cofinProcessTypeService;

	@Override
	public BigDecimal convertorFile(String path, FileUpload file, String idConfirmFile) {
		// TODO Auto-generated method stub
		Map<Integer, String> mapContent = new HashMap<Integer, String>();
		try {

			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));
			String readLine = "";
			String queryContent = "";
			String queryColumn = "";

			String title = "";
			String header = "";
			String table = "";
			String splitBy = "";
			String reference = file.getId();

			int row = 0;
			int countColumn = 0;
			int counter = 0;

			CofinConfirmFile cofinConfirmFile = cofinConfirmFileService.getById(idConfirmFile);

			title = cofinConfirmFile.getTitle().trim();
			header = cofinConfirmFile.getHeader().trim();
			row = cofinConfirmFile.getRow();
			countColumn = cofinConfirmFile.getTotalColumn();
			table = cofinConfirmFile.getTableName().trim();
			splitBy = cofinConfirmFile.getSplitBy();
			boolean continueProcess = true;

			if (validateFormat(row, title, path) == false) {
				log.info("Wrong title");
				return BigDecimal.ZERO;
			}

			Map<String, String> QueryHeader = getHeader(path, file, idConfirmFile);

			if (QueryHeader.size() == 0) {
				log.info("Date not match");
				return BigDecimal.ZERO;
			}

			if (QueryHeader.get("NA") != null) {
				QueryHeader.clear();
			}

			Map<String, String> QueryFooter = getFooter(path, file, idConfirmFile);

			if (QueryFooter.size() == 0) {
				log.info("Date not match");
				return BigDecimal.ZERO;
			}

			if (QueryFooter.get("NA") != null) {
				QueryFooter.clear();
			}

			log.info(" validate format header");

			Map<String, String> QueryParameter = getParameter(path, file, idConfirmFile);

			if (QueryParameter.size() == 0) {
				log.info("Date not match");
				return BigDecimal.ZERO;
			}

			if (QueryParameter.get("NA") != null) {
				QueryParameter.clear();
			}

			List<CofinConfirmFileSetting> cofinConfirmFileSetting = cofinConfirmFileSettingService
					.getByIdRecordType(idConfirmFile, "CONTENT");
			while ((readLine = b.readLine()) != null) {

				// content
				try {

					if (continueProcess) {

						String[] splitRow = readLine.trim().split(splitBy, -1);
						if (splitRow.length == countColumn && !readLine.contains(header.trim())) {
							queryContent = "";

							for (Map.Entry m : QueryHeader.entrySet()) {
								queryContent = queryContent + m.getValue() + "|";

								if (counter == 0) {
									queryColumn = queryColumn + m.getKey() + "|";
								}

							}

							for (Map.Entry m : QueryFooter.entrySet()) {
								queryContent = queryContent + m.getValue() + "|";

								if (counter == 0) {
									queryColumn = queryColumn + m.getKey() + "|";
								}

							}

							for (Map.Entry m : QueryParameter.entrySet()) {
								queryContent = queryContent + m.getValue() + "|";
								if (counter == 0) {
									queryColumn = queryColumn + m.getKey() + "|";
								}

							}

							for (CofinConfirmFileSetting fileSetting : cofinConfirmFileSetting) {
								// log.info("Index " + fileSetting.getDataIndex());
								String content = splitRow[fileSetting.getDataIndex()].replaceAll("\'", "")
										.replaceAll(",", "").replaceAll("\\.", "");

								/// substring

								if (!fileSetting.getSubString().equalsIgnoreCase("0")) {
									String subString = substrings(content, fileSetting.getSubString());
									content = subString;
								}

								if (!fileSetting.getCompareValue().equals("0")) {
									// compare ammount
									if (compareAmount(content, fileSetting.getCompareValue()) == false) {
										continueProcess = false;
										log.info(continueProcess + "compare value not found");
										return BigDecimal.ZERO;
									}
								}

								if (!fileSetting.getValuePattern().equalsIgnoreCase("0")) {
									// check pattern
									if (checkValuePattern(content, fileSetting.getValuePattern()) == false) {
										log.info(continueProcess + "pattern file wrong");
										continueProcess = false;
										return BigDecimal.ZERO;
									}
								}

								if (!fileSetting.getReplace().equalsIgnoreCase("0")) {
									// check pattern
									if (fileSetting.getReplace().startsWith("-")) {
										content = "-" + content;
									} else {
										content = fileSetting.getReplace();
									}

								}

								if (fileSetting.getDataType().contains("Date :")) {

									log.info("Data Type " + fileSetting.getDataType());
									content = "TO_DATE('" + content + "','"
											+ fileSetting.getDataType().split("\\:")[1].trim() + "')";

								}

								if (content.trim().isEmpty()) {
									content = "NA";
								}

								String column = fileSetting.getColumnDestination();

								queryContent = queryContent + content + "|";

								// get column first loop
								if (counter == 0) {
									queryColumn = queryColumn + column + "|";

									if (queryColumn.isEmpty()) {
										log.info("column not found");
										return BigDecimal.ZERO;
									}
								}

							}

							queryContent = queryContent.substring(0, queryContent.length() - 1) + "|"
									+ java.util.UUID.randomUUID();
							mapContent.put(counter, queryContent);

							log.info(mapContent.get(counter));

							counter++;

						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					return BigDecimal.ZERO;

				}

			}

			counter = 0;
			queryColumn = queryColumn.substring(0, queryColumn.length() - 1) + "|ID";
			if (continueProcess == true) {
				for (Map.Entry m : mapContent.entrySet()) {

					String query_String1 = "insert into " + table + "(" + queryColumn.replaceAll("\\|", ",")
							+ ") values('";

					String query_String2 = m.getValue().toString();
					query_String2 = query_String2.replaceAll(",", "");
					query_String2 = query_String2.replaceAll("\\|", "','");
					query_String2 = query_String2.replaceAll("BULAN", "");
					query_String2 = query_String2.replaceAll("'TO_DATE", "TO_DATE");
					query_String2 = query_String2.replaceAll("'\\)'", "'\\)");
					query_String2 = query_String2.replaceAll("\\'\\'", "\\',\\'");
					query_String2 = query_String2.replaceAll("'NA'", "''");
					query_String2 = query_String2.replaceAll("'SYSDATE'", "SYSDATE");

					String finalQuery = query_String1 + query_String2 + "')";
					finalQuery = finalQuery.replaceAll("'TO_DATE", "TO_DATE");

					try {
						cofinProcessTypeService.executeProcedure(finalQuery);
						log.info("Success Execute query " + finalQuery);
					} catch (Exception e) {
						log.info(e.toString());
						log.info("Failed Execute query " + finalQuery);
					}

					counter++;
				}

			}

			return new BigDecimal(counter);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return BigDecimal.ZERO;
	}

	public Map<String, String> getHeader(String path, FileUpload file, String idConfirmFile) {

		List<CofinConfirmFileSetting> cofinConfirmFileSetting = cofinConfirmFileSettingService
				.getByIdRecordType(idConfirmFile, "HEADER");

		String value = "";

		Map<String, String> map = new HashMap<String, String>();

		if (cofinConfirmFileSetting.size() == 0) {
			map.put("NA", "NA");
			return map;
		}

		try {
			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));
			String readLine = "";
			int counter = 1;

			while ((readLine = b.readLine()) != null) {

				// validate title
				try {
					for (CofinConfirmFileSetting fileSetting : cofinConfirmFileSetting) {

						if (counter == fileSetting.getDataIndex()) {

							value = readLine.split(fileSetting.getSplitType())[fileSetting.getSplitIndex()].trim();

							if (!fileSetting.getSubString().equalsIgnoreCase("0")) {
								String subString = substrings(value, fileSetting.getSubString());

								if (!subString.equals(value)) {
									value = subString;
								}
							}

							if (fileSetting.getDataSpource().contains("Validation Date")) {
								FileUpload fileUpload = file;
								try {
									String format = fileSetting.getDataSpource().split(":")[1];

									Date dateFromFile = DateUtil.stringToDateWithFormat(value, format.trim());
									String processDateFromFile = DateUtil.dateToStringWithFormat(dateFromFile,
											"yyyy-MM-dd");
									String fileUploadDate = DateUtil.dateToStringWithFormat(fileUpload.getProcessDate(),
											"yyyy-MM-dd");

									if (processDateFromFile.trim().equals(fileUploadDate.trim())) {
										log.info("date same " + processDateFromFile + "  -- " + fileUploadDate.trim());
									} else {
										log.info("date not same " + processDateFromFile + "  -- "
												+ fileUploadDate.trim());
										map.clear();
										return map;

									}

									fileUpload.setProcessDate(DateUtil.stringToDateWithFormat(value, format.trim()));

								} catch (Exception e) {
									e.printStackTrace();
									map.clear();
									return map;
								}

							}

							if (fileSetting.getDataType().contains("Date :")) {
								log.info("Data Type " + fileSetting.getDataType());
								value = "TO_DATE('" + value + "','" + fileSetting.getDataType().split("\\:")[1].trim()
										+ "')";

							}

							map.put(fileSetting.getColumnDestination(), value);

						}

					}

				} catch (Exception e) {
					e.printStackTrace();
					log.info(e.toString());
					// return;

				}

				counter++;

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return map;
	}

	public Map<String, String> getFooter(String path, FileUpload file, String idConfirmFile) {

		List<CofinConfirmFileSetting> cofinConfirmFileSetting = cofinConfirmFileSettingService
				.getByIdRecordType(idConfirmFile, "FOOTER");

		String value = "";

		Map<String, String> map = new HashMap<String, String>();

		if (cofinConfirmFileSetting.size() == 0) {
			map.put("NA", "NA");
			return map;
		}

		try {
			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));
			String readLine = "";
			int counter = 1;

			while ((readLine = b.readLine()) != null) {

				try {
					for (CofinConfirmFileSetting fileSetting : cofinConfirmFileSetting) {
						if (readLine.contains(fileSetting.getContain())) {
							value = readLine.split(fileSetting.getSplitType())[fileSetting.getSplitIndex()].trim();
							if (!fileSetting.getContain().equalsIgnoreCase("0")) {
								String subString = substrings(value, fileSetting.getSubString());

								if (!subString.equals(value)) {
									value = subString;
								}

							}

							if (fileSetting.getDataType().contains("Date :")) {
								log.info("Data Type " + fileSetting.getDataType());
								value = "TO_DATE('" + value + "','" + fileSetting.getDataType().split("\\:")[1].trim()
										+ "')";

							}

							map.put(fileSetting.getColumnDestination(), value);

						}

					}

				} catch (Exception e) {
					e.printStackTrace();
					// return;

				}

				counter++;

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return map;
	}

	public Map<String, String> getParameter(String path, FileUpload file, String idConfirmFile) {

		List<CofinConfirmFileSetting> cofinConfirmFileSetting = cofinConfirmFileSettingService
				.getByIdRecordType(idConfirmFile, "PARAMETER");

		String value = "";

		Map<String, String> map = new HashMap<String, String>();

		if (cofinConfirmFileSetting.size() == 0) {
			map.put("NA", "NA");
			return map;
		}

		try {
			for (CofinConfirmFileSetting fileSetting : cofinConfirmFileSetting) {

				if (fileSetting.getDefaultData().equalsIgnoreCase("@REF_FILE_UPLOAD_ID")) {
					value = file.getId();
				} else if (fileSetting.getDefaultData().equalsIgnoreCase("@SYSDATE")) {
					String dates = DateUtil.dateToStringWithFormat(new Date(), "dd/MM/YYYY");
					value = "SYSDATE";
				}				
				else {
					value = fileSetting.getDefaultData();

					if (fileSetting.getDataType().contains("Date :")) {
						log.info("Data Type " + fileSetting.getDataType());
						value = "TO_DATE('" + value + "','" + fileSetting.getDataType().split("\\:")[1].trim() + "')";
					}

				}

				map.put(fileSetting.getColumnDestination(), value);

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error getParameter");

		}

		return map;
	}

	public boolean validateFormat(int row, String contain, String path) {

		try {
			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));
			String readLine = "";
			int counter = 0;

			while ((readLine = b.readLine()) != null) {

				// validate title
				try {

					if (counter == row) {
						if (readLine.contains(contain)) {

							return true;
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
					// return;

				}
				counter++;

				if (counter > row) {
					return false;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	private boolean compareAmount(String content, String compareMark) {

		BigDecimal amount2 = BigDecimal.ZERO;
		Integer startGetAmount = 0;
		BigDecimal amount1 = BigDecimal.ZERO;
		compareMark = compareMark.replaceAll(",", "");
		content = content.replaceAll(",", "");

		if (compareMark.contains("<=") || compareMark.contains(">=")) {
			startGetAmount = 2;
		} else {
			startGetAmount = 1;
		}

		try {

			amount2 = new BigDecimal(compareMark.substring(startGetAmount, compareMark.length()));
			amount1 = new BigDecimal(Integer.parseInt(content));
		} catch (Exception e) {
			// e.printStackTrace();
			return true;
		}

		if (compareMark.startsWith(">=")) {
			if (amount1.intValue() >= amount2.intValue()) {
				log.info(amount1.intValue() + ">=" + amount2.intValue());
				return true;
			} else {
				log.info(amount1.intValue() + " false >=" + amount2.intValue());
				return false;
			}

		}

		else if (compareMark.startsWith("<=")) {
			if (amount1.intValue() <= amount2.intValue()) {
				log.info(amount1.intValue() + "<=" + amount2.intValue());
				return true;
			} else {
				log.info(amount1.intValue() + " false <=" + amount2.intValue());
				return false;
			}

		}

		else if (compareMark.startsWith(">")) {

			log.info("Mark >");
			if (amount1.intValue() > amount2.intValue()) {
				log.info(amount1.intValue() + ">" + amount2.intValue());
				return true;
			} else {
				log.info(amount1.intValue() + " false >" + amount2.intValue());
				return false;
			}

		}

		else if (compareMark.startsWith("<")) {
			if (amount1.intValue() < amount2.intValue()) {
				log.info(amount1.intValue() + "<" + amount2.intValue());
				return true;
			} else {
				log.info(amount1.intValue() + " false <" + amount2.intValue());
				return false;
			}

		} else if (compareMark.startsWith("=")) {
			if (amount1.intValue() == amount2.intValue()) {
				log.info(amount1.intValue() + "==" + amount2.intValue());
				return true;
			} else {
				log.info(amount1.intValue() + " false ==" + amount2.intValue());
				return false;
			}

		}

		else {
			log.info(amount1.intValue() + " Compare Mark not found >" + amount2.intValue());
			return false;

		}

	}

	private boolean checkValuePattern(String value, String valuePattern) {
		if (valuePattern.equalsIgnoreCase(value)) {
			return true;
		} else {
			log.info("Wrong pattern value " + value + " " + valuePattern);
			return false;
		}

	}

	private String substrings(String value, String substring) {

		if (substring.contains(",")) {
			String[] substr = substring.split(",");
			log.info(value + " | " + substring);
			Integer start = 0;
			Integer end = 0;
			try {
				start = Integer.parseInt(substr[0]);
				end = Integer.parseInt(substr[1]);
				value = value.substring(start, end);
			} catch (Exception e) {

				log.info(value + " | " + substring);
				e.printStackTrace();
				return value;
			}

		} else {
			int startFromBack = Integer.parseInt(substring);
			if (startFromBack > 0) {
				value = value.substring(value.length() - startFromBack, value.length());

				log.info("substring " + value);
				return value;
			}

		}

		return value;
	}

	@Override
	public String checkFile(String path, FileUpload file, String idConfirmFile) {
		// TODO Auto-generated method stub
		Map<Integer, String> mapContent = new HashMap<Integer, String>();
		try {

			File f = new File(path);
			BufferedReader b = new BufferedReader(new FileReader(f));
			String readLine = "";
			String queryContent = "";
			String queryColumn = "";

			String title = "";
			String header = "";
			String table = "";
			String splitBy = "";
			String reference = file.getId();

			int row = 0;
			int countColumn = 0;
			int counter = 0;

			CofinConfirmFile cofinConfirmFile = cofinConfirmFileService.getById(idConfirmFile);

			title = cofinConfirmFile.getTitle().trim();
			header = cofinConfirmFile.getHeader().trim();
			row = cofinConfirmFile.getRow();
			countColumn = cofinConfirmFile.getTotalColumn();
			table = cofinConfirmFile.getTableName().trim();
			splitBy = cofinConfirmFile.getSplitBy();
			boolean continueProcess = true;

			log.info(" validate format");
			if (validateFormat(row, title, path) == false) {
				log.info("Wrong title");
				return "Title file not found";
			}

			log.info(" validate format header");
			Map<String, String> QueryHeader = getHeader(path, file, idConfirmFile);

			if (QueryHeader.size() == 0) {
				log.info("Date not match");
				return "Date Process format not match";
			}

			if (QueryHeader.get("NA") != null) {
				QueryHeader.clear();
			}

			Map<String, String> QueryFooter = getFooter(path, file, idConfirmFile);
			log.info("processsss");
			if (QueryFooter.size() == 0) {
				log.info("Date not match");
				return "Date Process format not match";
			}

			if (QueryFooter.get("NA") != null) {

				QueryFooter.clear();
			}

			List<CofinConfirmFileSetting> cofinConfirmFileSetting = cofinConfirmFileSettingService
					.getByIdRecordType(idConfirmFile, "CONTENT");
			while ((readLine = b.readLine()) != null) {

				// content
				try {

					if (continueProcess) {
						String[] splitRow = readLine.trim().split(splitBy, -1);
						if (splitRow.length == countColumn && !readLine.contains(header.trim())) {

							for (CofinConfirmFileSetting fileSetting : cofinConfirmFileSetting) {
								// log.info("Index " + fileSetting.getDataIndex());
								String content = splitRow[fileSetting.getDataIndex()].replaceAll("\'", "")
										.replaceAll(",", "").replaceAll("\\.", "");

								/// substring

								if (!fileSetting.getSubString().equalsIgnoreCase("0")) {
									String subString = substrings(content, fileSetting.getSubString());
									content = subString;
								}

								if (!fileSetting.getCompareValue().equals("0")) {
									// compare ammount
									if (compareAmount(content, fileSetting.getCompareValue()) == false) {
										continueProcess = false;
										log.info(continueProcess + "compare value not found");
										return "Value amount not " + fileSetting.getCompareValue();
									}
								}

								if (!fileSetting.getValuePattern().equalsIgnoreCase("0")) {
									// check pattern
									if (checkValuePattern(content, fileSetting.getValuePattern()) == false) {
										log.info(continueProcess + "pattern file wrong");
										continueProcess = false;
										return "Pattern file wrong " + fileSetting.getValuePattern();
									}
								}

								if (!fileSetting.getReplace().equalsIgnoreCase("0")) {
									// check pattern
									if (fileSetting.getReplace().startsWith("-")) {
										content = "-" + content;
									} else {
										content = fileSetting.getReplace();
									}
								}

								if (fileSetting.getDataType().contains("Date :")) {
									log.info("Data Type " + fileSetting.getDataType());
									content = "TO_DATE('" + content + "','"
											+ fileSetting.getDataType().split("\\:")[1].trim() + "')";

								}

								if (content.trim().isEmpty()) {
									content = "NA";
								}

								String column = fileSetting.getColumnDestination();

								queryContent = queryContent + content + "|";

								// get column first loop
								if (counter == 0) {
									queryColumn = queryColumn + column + "|";

									if (queryColumn.isEmpty()) {
										log.info("column not found");
										return "column not found";
									}
								}

							}

							counter++;

						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					return "Erorr";

				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return "success";
	}

}
