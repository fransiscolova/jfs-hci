package id.co.homecredit.jfs.cofin.core.service.util;

import java.util.Properties;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Helper class to get static text from properties file.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class CofinPropertyUtil implements InitializingBean {
    private final static String source = "file:/cofin/data/jfs/properties/cofin.properties";
    private static CofinPropertyUtil singleInst;

    /**
     * Get string property by key.
     *
     * @param key
     * @return string
     */
    public static String get(String key) {
        if (key == null || key.isEmpty()) {
            return "";
        }
        return singleInst.properties.getProperty(key);
    }

    @Autowired
    protected ApplicationContext context;

    private Properties properties;

    protected CofinPropertyUtil() {
        properties = new Properties();
        singleInst = this;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        properties.load(context.getResource(source).getInputStream());
    }

}
