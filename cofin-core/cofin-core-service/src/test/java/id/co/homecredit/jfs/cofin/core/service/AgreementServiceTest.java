package id.co.homecredit.jfs.cofin.core.service;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PartnerEnum;

/**
 * Test class for {@link AgreementService}.
 *
 * @author muhammad.muflihun
 *
 */
public class AgreementServiceTest extends BaseServiceTest {

    @Autowired
    private AgreementService agreementService;
    @Autowired
    private PartnerService partnerService;

    @Test
    public void getAllAgreements() {
        List<Agreement> agreements = agreementService.getAllAgreements(false);
        Assert.assertFalse(agreements.isEmpty());
        System.out.println(agreements.size());
        for (Agreement agreement : agreements) {
            System.out.println(agreement.toString());
        }
    }

    @Test
    public void getAllAgreementsByPartnerCode() {
        List<Agreement> agreements = agreementService.getAllAgreementsByPartnerCode("PERMATA");
        for (Agreement agreement : agreements) {
            Assert.assertNotNull(agreement);
            System.out.println(agreement.toString());
        }
    }

    @Test
    public void save() throws ParseException {
        Agreement agreement = new Agreement();
        agreement.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        agreement.setCode("PERMATA_FF");
        agreement.setAgreementType(AgreementTypeEnum.FF);
        agreement.setAliasId("52");
        agreement.setPartner(partnerService.getPartnerByCode(PartnerEnum.PERMATA.name()));
        agreement.setDescription("test");
        agreement.setValidFrom(DateUtil.stringToDate("2017-11-01"));
        agreement.setValidTo(DateUtil.stringToDate("2017-11-01"));
        agreement = agreementService.saveAgreement(agreement);
    }
}
