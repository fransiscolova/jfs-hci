package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;

/**
 * Abstract test class for core service.
 *
 * @author muhammad.muflihun
 *
 */
@ContextConfiguration(locations = { "classpath*:cofin-core-service-test-context.xml" })
public abstract class BaseServiceTest extends AbstractTransactionalTestNGSpringContextTests {

}
