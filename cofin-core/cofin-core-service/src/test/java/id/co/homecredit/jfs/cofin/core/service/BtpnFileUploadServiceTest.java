package id.co.homecredit.jfs.cofin.core.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.JSONUtil;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PartnerEnum;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.PartnerService;
import id.co.homecredit.jfs.cofin.core.service.upload.BtpnFileUploadService;

/**
 * Test class for {@link AgreementService}.
 *
 * @author muhammad.muflihun
 *
 */
public class BtpnFileUploadServiceTest extends BaseServiceTest {

    @Autowired
    private BtpnFileUploadService btpnFileUploadService;
    
    @Autowired
    private PartnerService partnerService;
    
	@Autowired
	FileUploadService fileUploadService;

	
	private String mainFolder="D:\\JFS DATA\\BTPN\\";

    @Test
    @Rollback(false)
    public void uploadDisbursement() {
    	
		
		 FileUpload file=upload();
    	 String path= mainFolder + "Disbursement\\DISB_HCI_1_15-01-18.txt";
         BigDecimal count= btpnFileUploadService.readDisbursementFile(path, file);
         System.out.println("Result =>" + JSONUtil.convertToJSON(file));
         System.out.println("Result =>" + count);
         
         
         
    }
    
    
    @Test
    @Rollback(false)
    public void uploadHasilPemeriksaan() {
    			
		 FileUpload file=upload();
    	 String path= mainFolder + "Disbursement\\J138084_1_LAPORAN_HASIL_PEMERIKSAAN_(LHP)_JF_HCI_TGL_12012018.txt";
         BigDecimal count= btpnFileUploadService.readHasilPemeriksanaan(path, file);
         System.out.println("Result =>" + JSONUtil.convertToJSON(file));
         System.out.println("Result =>" + count);
         
         
         
    }
    
    
    private FileUpload upload() {
    	Partner partner = partnerService.getPartnerById("DD623E14AE644A408E52F495A756EAC0");
		FileUpload fileUpload = new FileUpload();
		fileUpload.setPartner(partner);
		fileUpload.setFileName("fileTest");
		
		fileUpload.setMemo("test memo");
		fileUpload.setCreatedBy("user");
		fileUpload.setProcessDate(new Date());
		fileUpload.setDescription("");
		fileUpload.setProcessType("Disbursement test" + "_" + "daity");
		fileUpload.setStatus("On Progress");	
		fileUpload.setTotalRow(BigDecimal.ZERO);			
		fileUpload=fileUploadService.saveFileUpload(fileUpload);
		return fileUpload;
    }

}
