package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.dao.CriteriaMappingDao;
import id.co.homecredit.jfs.cofin.core.model.CriteriaMapping;
import id.co.homecredit.jfs.cofin.core.model.dto.FinalCriteriaEligibilityDto;

/**
 * @author muhammad.muflihun
 *
 */
public class CriteriaMappingServiceTest extends BaseServiceTest {

    @Autowired
    private CriteriaMappingDao criteriaMappingDao;
    @Autowired
    private CriteriaMappingService criteriaMappingService;

    @Test
    public void getAllCriteriaMappingsByAgreementId() {
        String agreementId = "245E17CFDEE447E9842DB960CA29D668";
        List<CriteriaMapping> criteriaMappings = criteriaMappingDao
                .getAllCriteriaMappingsByAgreementId(agreementId);
        Assert.assertFalse(criteriaMappings.isEmpty());
        FinalCriteriaEligibilityDto finalDto = criteriaMappingService
                .flattingCriteria(criteriaMappings);
        System.out.println(finalDto);
        String sql = criteriaMappingService.finalListToSql(finalDto);
        System.out.println(sql);
    }

}
