package id.co.homecredit.jfs.cofin.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.core.model.Partner;

/**
 * Test class for {@link PartnerService}.
 *
 * @author muhammad.muflihun
 *
 */
public class PartnerServiceTest extends BaseServiceTest {

    @Autowired
    private PartnerService partnerService;

    @Test
    public void deletePartner() {
        partnerService.deletePartnerById(partnerService.getAllPartners(false).get(0).getId());
    }

    @Test
    public void getAllPartners() {
        List<Partner> partners = partnerService.getAllPartners(false);
        for (Partner partner : partners) {
            Assert.assertNotNull(partner);
            System.out.println(partner.toString());
        }
    }
}
