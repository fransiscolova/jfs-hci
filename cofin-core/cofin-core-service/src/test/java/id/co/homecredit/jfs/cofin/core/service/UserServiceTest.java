package id.co.homecredit.jfs.cofin.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.model.User;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;

/**
 * Test class for {@link UserService}.
 *
 * @author muhammad.muflihun
 *
 */
public class UserServiceTest extends BaseServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void getUser() {
        String username = "muhammad.muflihun";
        System.out.println(userService.getUserByUserName(username));
    }

    @Test
    public void save() {
        User user = new User();
        user.setCreatedBy(CoreModelConstant.COFIN_SYSTEM);
        user.setUsername("service.audit");
        user = userService.saveUser(user);
        System.out.println(user);
    }

    @Test
    public void update() {
        String username = "muhammad.muflihun";
        User user = userService.getUserByUserName(username);
        user.setUpdatedBy(CoreModelConstant.COFIN_SYSTEM);
        user.setFirstName("Mufti");
        user.setLastName("Muflihun");
        user = userService.saveUser(user);
        System.out.println(user);
    }

    @Rollback(false)
    @Test
    public void update2() {
        String username = "service.audit";
        User user = userService.getUserByUserName(username);
        user.setUpdatedBy(CoreModelConstant.COFIN_SYSTEM);
        user.setFirstName(null);
        user.setLastName(null);
        user.setDeleted(YesNoEnum.Y);
        user = userService.saveUser(user);
        System.out.println(user);
    }

}
