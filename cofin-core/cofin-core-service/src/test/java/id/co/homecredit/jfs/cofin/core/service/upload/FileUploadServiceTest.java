package id.co.homecredit.jfs.cofin.core.service.upload;

import org.testng.annotations.Test;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.JSONUtil;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PartnerEnum;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.service.BaseServiceTest;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.PartnerService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.PermataFileUploadService;

/**
 * Test class for {@link AgreementService}.
 *
 * @author muhammad.muflihun
 *
 */
public class FileUploadServiceTest extends BaseServiceTest {

   @Autowired
   UploadService uploadService;
   

   @Autowired
   private PartnerService partnerService;
   
	@Autowired
	FileUploadService fileUploadService;

	
	@Autowired
	CofinConfirmFileService cofinConfirmFileService;
	
	

	
    
    
    
  //disburement btpn pemeriksaan
    @Test   
    @Rollback(false)
    public void uploadDisbursementBTPNPemeriksaaan() {    	    	
    	FileUpload file=upload("637EBEC68A90B46FE0531107380A4974","DD623E14AE644A408E52F495A756EAC0","02/21/2018");
    	 String path="D:\\JFS DATA\\BTPN\\Disbursement\\J138084_1_LAPORAN_HASIL_PEMERIKSAAN_(LHP)_JF_HCI_TGL_12012018.txt";
         BigDecimal count= uploadService.convertorFile(path, file, file.getCofinConfirmFile().getId());     
         System.out.println("Result =>" + count);
         
        
		 
         
    }
    
    
    
    
    
    
    //disburement btpn pemeriksaan
    @Test   
    @Rollback(false)
    public void uploadDisbursementBTPN() {  	
    	FileUpload file=upload("637EBEC68A91B46FE0531107380A4974","DD623E14AE644A408E52F495A756EAC0","");
    	 String path="D:\\JFS DATA\\BTPN\\Disbursement\\DISB_HCI_1_15-01-18.txt";
         BigDecimal count= uploadService.convertorFile(path, file, file.getCofinConfirmFile().getId());     
         System.out.println("Result =>" + count);
         
    }
    
    
    //upload dummy disbursment
    @Rollback(false)
    private FileUpload upload(String idconfirmFile,String idPartner,String date) {

    	CofinConfirmFile confirmFile=cofinConfirmFileService.getById(idconfirmFile);
    	Partner partner = partnerService.getPartnerById(idPartner);
    	System.out.println(partner.getName());
    	    	
		FileUpload fileUpload = new FileUpload();
		fileUpload.setPartner(partner);
		fileUpload.setFileName("fileTest");		
		fileUpload.setMemo("test memo");
		fileUpload.setCreatedBy("user");
		try {
			fileUpload.setProcessDate(DateUtil.stringToDateWithFormat(date, "MM/dd/yyyy"));
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		fileUpload.setDescription("");
		fileUpload.setProcessType("Disbursement test" + "_" + "daily");
		fileUpload.setStatus("On Progress");	
		fileUpload.setTotalRow(BigDecimal.ZERO);			
		fileUpload.setCofinConfirmFile(confirmFile);
		
		
		fileUploadService.saveFileUpload(fileUpload);
		 
		return fileUpload;
    }
    
    
    
    

}
