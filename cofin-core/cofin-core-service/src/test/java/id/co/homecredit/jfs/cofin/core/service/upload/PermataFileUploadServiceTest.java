package id.co.homecredit.jfs.cofin.core.service.upload;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.Assert;
import org.testng.annotations.Test;

import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.util.JSONUtil;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.constant.CoreModelConstant;
import id.co.homecredit.jfs.cofin.core.model.enumeration.AgreementTypeEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.PartnerEnum;
import id.co.homecredit.jfs.cofin.core.service.BaseServiceTest;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.PartnerService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.PermataFileUploadService;

/**
 * Test class for {@link AgreementService}.
 *
 * @author muhammad.muflihun
 *
 */
public class PermataFileUploadServiceTest extends BaseServiceTest {

    @Autowired
    private PermataFileUploadService permataFileUploadService;
    
    @Autowired
    private PartnerService partnerService;
    
	@Autowired
	FileUploadService fileUploadService;

	@Autowired
	TempJfsConfirmationFileService tempJfsConfirmationFileService;

    @Test
    @Rollback(false)
    public void uploadDisbursement() {
    	
		
		 FileUpload file=upload();
    	 String path="D:\\JFS DATA\\Permata\\REAL_CAIR_HCI_09012018.txt";
         BigDecimal count= permataFileUploadService.readDisbursementFile(path, file);
         System.out.println("Result =>" + JSONUtil.convertToJSON(file));
         System.out.println("Result =>" + count);
         
         
         
    }
    
    
    

    @Test
    @Rollback(false)
    public void uploadPayment() {
    	
		 
		 FileUpload file=upload();
    	 String path="D:\\JFS DATA\\Permata\\payment\\REAL_PMT_HCI_20171218.txt";
         BigDecimal count= permataFileUploadService.readFilePayement(path, file);
         System.out.println("Result =>" + JSONUtil.convertToJSON(file));
         System.out.println("Result =>" + count);
         
         
         
    }
    
    
    private FileUpload upload() {
    	Partner partner = partnerService.getPartnerById("DB57E85359684F13A2B8DAA1493FDF1F");
		FileUpload fileUpload = new FileUpload();
		fileUpload.setPartner(partner);
		fileUpload.setFileName("fileTest");
		
		fileUpload.setMemo("test memo");
		fileUpload.setCreatedBy("user");
		fileUpload.setProcessDate(new Date());
		fileUpload.setDescription("");
		fileUpload.setProcessType("Disbursement test" + "_" + "daity");
		fileUpload.setStatus("On Progress");	
		fileUpload.setTotalRow(BigDecimal.ZERO);			
		fileUpload=fileUploadService.saveFileUpload(fileUpload);
		return fileUpload;
    }

}
