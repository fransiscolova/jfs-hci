package id.co.homecredit.jfs.cofin.ui.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;
import id.co.homecredit.jfs.cofin.common.model.DropDownListItem;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.dao.ActivityLogDao;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.AgreementType;
import id.co.homecredit.jfs.cofin.core.model.AuditLog;
import id.co.homecredit.jfs.cofin.core.model.JfsAgreement;
import id.co.homecredit.jfs.cofin.core.model.JfsPartner;
import id.co.homecredit.jfs.cofin.core.model.JfsProduct;
import id.co.homecredit.jfs.cofin.core.model.JfsProductHci;
import id.co.homecredit.jfs.cofin.core.model.JfsProductMapping;
import id.co.homecredit.jfs.cofin.core.model.enumeration.ActivityEnum;
import id.co.homecredit.jfs.cofin.core.service.AgreementTypeService;
import id.co.homecredit.jfs.cofin.core.service.JfsAgreementService;
import id.co.homecredit.jfs.cofin.core.service.JfsPartnerService;
import id.co.homecredit.jfs.cofin.core.service.JfsProductHciService;
import id.co.homecredit.jfs.cofin.core.service.JfsProductMappingService;
import id.co.homecredit.jfs.cofin.core.service.JfsProductService;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;

/**
 * {@link Agreement} controller for implementing Spring MVC.
 *
 * @author Fransisco.situmorang
 *
 */

//@Controller
//@RequestMapping(value = "/agreement")
//public class AgreementController {
//    private static final Logger log = LogManager.getLogger(AgreementController.class);
//
//    @Autowired
//    private ProductMappingService productMappingService;
//
//    @Autowired
//    private ProductService productService;
//
//    @Autowired
//    private AgreementService agreementService;
//
//    @Autowired
//    private AgreementDetailService agreementDetailService;
//
//    @Autowired
//    private PartnerService partnerService;
//
//    @RequestMapping(value = "/activation", method = RequestMethod.POST)
//    public @ResponseBody String activationAgreement(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException {
//        JSONObject json = new JSONObject(js);
//        Agreement agreement = agreementService.getAgreementById(json.getString("id"));
//        if (agreement.getDeleted().equals(YesNoEnum.Y)) {
//            agreement.setDeleted(YesNoEnum.N);
//            log.info("Reactive");
//        } else {
//            agreement.setDeleted(YesNoEnum.Y);
//            log.info("Deactive");
//        }
//        agreement.setUpdatedBy(UserLoginUtil.getUsername());
//        agreementService.saveAgreement(agreement);
//        return js;
//    }
//
//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public ModelAndView allAgreement() throws Exception {
//        ModelAndView model = new ModelAndView();
//        model.setViewName("agreement");
//        List<Agreement> agreement = agreementService.getAllAgreements(true);
//        model.addObject("agreement", agreement);
//        model.addObject("partners", partnerService.getAllPartners(false));
//        model.addObject("sizes", agreement.size() + 1);
//        return model;
//
//    }
//
//    @RequestMapping(value = "/product/delete", method = RequestMethod.POST)
//    public @ResponseBody String deleteProductMapping(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException, ParseException {
//        JSONObject json = new JSONObject(js);
//        ProductMapping productMapping = productMappingService
//                .getProductMappingById(json.getString("id"));
//        if (productMapping.getDeleted().equals(YesNoEnum.Y)) {
//            productMapping.setDeleted(YesNoEnum.N);
//        } else {
//            productMapping.setDeleted(YesNoEnum.Y);
//        }
//        productMapping.setUpdatedBy(UserLoginUtil.getUsername());
//        productMappingService.saveProductMapping(productMapping);
//        return js;
//    }
//
//    @RequestMapping(value = "/detail/{agreementId}", method = RequestMethod.GET)
//    public ModelAndView getAgreementById(@PathVariable String agreementId) throws Exception {
//        ModelAndView model = new ModelAndView();
//        model.setViewName("agreementDetail");
//
//        Agreement agreement = agreementService.getAgreementWithFetchedDetailsById(agreementId);
//        model.addObject("agreement", agreement);
//        String status = "Deactive";
//        if (agreement.getDeleted().equals(YesNoEnum.Y)) {
//            model.addObject("status", "Deactive");
//            status = "Reactive";
//        }
//        model.addObject("status", status);
//
//        for (AgreementDetail agreementDetail : agreement.getAgreementDetails()) {
//            if (agreementDetail.getDeleted().equals(YesNoEnum.N)) {
//                model.addObject("interestRate", agreementDetail.getInterestRate().toString());
//                model.addObject("principalSplitRate",
//                        agreementDetail.getPrincipalSplitRate().toString());
//                model.addObject("adminFeeRate", agreementDetail.getAdminFeeRate().toString());
//                model.addObject("penaltySplitRate",
//                        agreementDetail.getPenaltySplitRate().toString());
//
//                model.addObject("agreementId", agreementId);
//            }
//        }
//
//        return model;
//
//    }
//
//    @RequestMapping(value = "/product/{agreementId}", method = RequestMethod.GET)
//    public ModelAndView getProductMapping(@PathVariable String agreementId) throws Exception {
//        ModelAndView model = new ModelAndView();
//        model.setViewName("agreementProductMapping");
//        model.addObject("products", productService.getAllProductsByIncludeDeleted(true));
//
//        Agreement agreement = agreementService
//                .getAgreementWithFetchedProductMappingsById(agreementId);
//        model.addObject("agreement", agreement);
//
//        Agreement agreementDetails = agreementService
//                .getAgreementWithFetchedDetailsById(agreementId);
//        for (AgreementDetail agreementDetail : agreementDetails.getAgreementDetails()) {
//            if (agreementDetail.getDeleted().equals(YesNoEnum.N)) {
//                model.addObject("interestRate", agreementDetail.getInterestRate().toString());
//                model.addObject("principalSplitRate",
//                        agreementDetail.getPrincipalSplitRate().toString());
//                model.addObject("adminFeeRate", agreementDetail.getAdminFeeRate().toString());
//                model.addObject("penaltySplitRate",
//                        agreementDetail.getPenaltySplitRate().toString());
//                model.addObject("agreementId", agreementId);
//            }
//        }
//
//        return model;
//
//    }
//
//    @RequestMapping(value = "/save", method = RequestMethod.POST)
//    public @ResponseBody String saveAgreement(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException, ParseException {
//        JSONObject json = new JSONObject(js);
//        Agreement agreement = new Agreement();
//        agreement.setCode(json.getString("code"));
//        agreement.setName(json.getString("name"));
//        Partner partner = partnerService.getPartnerById(json.getString("partnerId"));
//        agreement.setPartner(partner);
//        agreement.setDescription(json.getString("description"));
//        log.debug(json.getString("validFrom"));
//        agreement.setValidFrom(
//                DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
//        agreement.setValidTo(
//                DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
//        agreement.setCreatedBy(UserLoginUtil.getUsername());
//        String xtype = json.getString("agreementType");
//        if(xtype.equals("REG")){
//        	agreement.setAgreementType(AgreementTypeEnum.REG);
//        }else if(xtype.equals("EZ10")){
//        	agreement.setAgreementType(AgreementTypeEnum.EZ10);
//        }else if(xtype.equals("FF")){
//        	agreement.setAgreementType(AgreementTypeEnum.FF);
//        }else if(xtype.equals("ZERO")){
//        	agreement.setAgreementType(AgreementTypeEnum.ZERO);
//        }else{
//        	agreement.setAgreementType(AgreementTypeEnum.REG);
//        }
//        agreement.setAliasId(json.getString("idAlias"));
//        agreementService.saveAgreement(agreement);
//
//        return js;
//    }
//
//    @RequestMapping(value = "/detail/save", method = RequestMethod.POST)
//    public @ResponseBody String saveAgreementDetail(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException, ParseException {
//        JSONObject json = new JSONObject(js);
//        List<AgreementDetail> deactiveAgreementDetail = new ArrayList<>();
//        List<AgreementDetail> AgreementDetails = agreementDetailService
//                .getAllAgreementDetailByAgreementId(json.getString("agreementId"));
//
//        for (AgreementDetail agreementDetail : AgreementDetails) {
//            AgreementDetail detail = agreementDetailService
//                    .getAgreementDetailById(agreementDetail.getId());
//            detail.setDeleted(YesNoEnum.Y);
//            deactiveAgreementDetail.add(detail);
//        }
//
//        AgreementDetail agreementsDetail = new AgreementDetail();
//        agreementsDetail.setInterestRate(new BigDecimal(json.getLong("interestRate")));
//        agreementsDetail.setPrincipalSplitRate(new BigDecimal(json.getLong("principalSplitRate")));
//        agreementsDetail.setAdminFeeRate(new BigDecimal(json.getLong("adminFeeRate")));
//        agreementsDetail.setPenaltySplitRate(new BigDecimal(json.getLong("penaltySplitRate")));
//        agreementsDetail
//                .setAgreement(agreementService.getAgreementById(json.getString("agreementId")));
//        agreementsDetail.setValidFrom(
//                DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
//        agreementsDetail.setValidTo(
//                DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
//        agreementsDetail.setCreatedBy(UserLoginUtil.getUsername());
//
//        agreementDetailService.saveAgreementDetails(deactiveAgreementDetail);
//        agreementDetailService.saveAgreementDetail(agreementsDetail);
//
//        return js;
//    }
//
//    @RequestMapping(value = "/product/save", method = RequestMethod.POST)
//    public @ResponseBody String saveAgreementProductMapping(
//            @RequestBody(required = false) String js, HttpServletResponse response)
//            throws JSONException, ParseException {
//        JSONObject json = new JSONObject(js);
//        Product product = productService.getProductByCode(json.getString("productCode"));
//        ProductMapping productMapping = new ProductMapping();
//        productMapping.setProduct(product);
//        Agreement agreement = agreementService.getAgreementById(json.getString("agreementId"));
//        productMapping.setAgreement(agreement);
//        productMapping.setCreatedBy(UserLoginUtil.getUsername());
//        productMapping.setValidFrom(
//                DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
//        productMapping.setValidTo(
//                DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
//
//        productMappingService.saveProductMapping(productMapping);
//        return js;
//    }
//
//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public @ResponseBody String updateAgreement(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException, ParseException {
//        JSONObject json = new JSONObject(js);
//        Agreement agreement = agreementService.getAgreementById(json.getString("agreementId"));
//        agreement.setCode(json.getString("code"));
//        agreement.setName(json.getString("name"));
//        agreement.setValidFrom(
//                DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
//        agreement.setValidTo(
//                DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
//        agreement.setUpdatedBy(UserLoginUtil.getUsername());
//        agreement.setAgreementType(AgreementTypeEnum.REG);
//        agreement.setDescription(json.getString("desc"));
//
//        agreementService.saveAgreement(agreement);
//        return js;
//    }
//    
//    @RequestMapping(value = "/detail/update", method = RequestMethod.POST)
//    public @ResponseBody String updateDetailAgreement(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException, ParseException {
//    	JSONObject json = new JSONObject(js);
//    	AgreementDetail soft = agreementDetailService.getAgreementDetailById(json.getString("id"));
//    	soft.setValidFrom(DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
//    	soft.setValidTo(DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
//    	soft.setUpdatedDate(new Date());
//    	soft.setUpdatedBy(UserLoginUtil.getUsername());
//    	agreementDetailService.saveAgreementDetail(soft);
//    	return js;
//    }
//
//}


@Controller
@RequestMapping(value="/agreement")
public class AgreementController{
	private static final Logger log = LogManager.getLogger(AgreementController.class);
	
	@Autowired
	private JfsAgreementService jfsAgreementService;
	
	@Autowired
	private JfsPartnerService jfsPartnerService;
	
	@Autowired
	private JfsProductService jfsProductService;
	
	@Autowired
	private JfsProductMappingService jfsProductMappingService;
	
	@Autowired
	private JfsProductHciService jfsProductHciService;
	
	@Autowired
	private AgreementTypeService agreementTypeService;
	
	@Autowired
	private ActivityLogDao activityLogDao;
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public ModelAndView allAgreement()throws Exception{
		log.info("To Agreement Page");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		ModelAndView view = new ModelAndView();
		view.setViewName("agreement");
		List<JfsAgreement> xagree = new ArrayList<JfsAgreement>();
		List<Map<String,Object>> agree = jfsAgreementService.getAllAgreementWithQuery();
		for(int x=0;x<agree.size();x++){
			String xname = jfsPartnerService.getPartnerName(agree.get(x).get("PARTNER_ID").toString());
			JfsAgreement agr = new JfsAgreement();
			agr.setId(String.valueOf(x+1));
			agr.setIdAgreement(Long.parseLong(agree.get(x).get("ID_AGREEMENT").toString()));
			agr.setCodeAgreement(agree.get(x).get("CODE_AGREEMENT").toString());
			agr.setNameAgreement(agree.get(x).get("NAME_AGREEMENT").toString());
			agr.setCodePartner(xname);
			agr.setValidFrom(format.parse(agree.get(x).get("VALID_FROM").toString().substring(0,10)));
			agr.setValidTo(format.parse(agree.get(x).get("VALID_TO").toString().substring(0,10)));
			agr.setIsDelete(agree.get(x).get("IS_DELETE").toString());
			agr.setCreatedBy(agree.get(x).get("VALID_FROM").toString().substring(0,10));
			agr.setUpdatedBy(agree.get(x).get("VALID_TO").toString().substring(0,10));
			xagree.add(agr);
		}
		List<JfsPartner> xpart = jfsPartnerService.getAllPartner();
		List<DropDownListItem> zpart = new ArrayList<DropDownListItem>();
		for(int x=0;x<xpart.size();x++){
			zpart.add(new DropDownListItem(xpart.get(x).getName(),xpart.get(x).getId()));
		}
		view.addObject("partner",zpart);
		view.addObject("agreement",xagree);
		String maxId = jfsAgreementService.getMaxIdAgreement();
		maxId = String.valueOf(Integer.parseInt(maxId)+1);
		view.addObject("sizes",maxId);
		List<AgreementType> xtype = agreementTypeService.getAllAgreementType();
		List<DropDownListItem> xlist = new ArrayList<DropDownListItem>();
		for(int x=0;x<xtype.size();x++){
			xlist.add(new DropDownListItem(xtype.get(x).getCode()+" - "+xtype.get(x).getName(),xtype.get(x).getId()));
		}
		view.addObject("xtype",xlist);
		return view;
	}
	
	@RequestMapping(value="/detail/{agreementId}",method=RequestMethod.GET)
	public ModelAndView getAgreementDetail(@PathVariable String agreementId)throws Exception{
		log.info("To Agreement Detail Page {}",agreementId);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		ModelAndView model = new ModelAndView();
		model.setViewName("agreementDetail");
		JfsAgreement resultHead = jfsAgreementService.getJfsAgreementById(Long.parseLong(agreementId));
		String xname = jfsPartnerService.getPartnerName(resultHead.getPartnerId());
		model.addObject("agreement",resultHead);
		if(resultHead.getIsDelete().equals(YesNoEnum.Y)){
			model.addObject("status","Deactive");
		}else{
			model.addObject("status","Reactive");
		}
		List<JfsProduct> resultDet = jfsProductService.getProductByAgreementId(Long.parseLong(agreementId));
		if(resultDet.size()==0){
			model.addObject("interestRate","0.0");
			model.addObject("principalSplitRate","0.0");
			model.addObject("adminFeeRate","0.0");
			model.addObject("penaltySplitRate","0.0");
			model.addObject("principalSplitRateHci","0.0");
		}else{
			if(resultDet.get(0).getInterestRate()==null){
				model.addObject("interestRate","0.0");
			}else{
				model.addObject("interestRate",resultDet.get(0).getInterestRate().toString());
			}
			if(resultDet.get(0).getPrincipalSplitRate()==null){
				model.addObject("principalSplitRate","0.0");
			}else{
				model.addObject("principalSplitRate",resultDet.get(0).getPrincipalSplitRate().toString());
			}
			if(resultDet.get(0).getAdminFeeRate()==null){
				model.addObject("adminFeeRate","0.0");
			}else{
				model.addObject("adminFeeRate",resultDet.get(0).getAdminFeeRate().toString());
			}
			if(resultDet.get(0).getPenaltySplitRate()==null){
				model.addObject("penaltySplitRate","0.0");
			}else{
				model.addObject("penaltySplitRate",resultDet.get(0).getPenaltySplitRate().toString());
			}
			if(resultDet.get(0).getPrincipalSplitRateHci()==null){
				model.addObject("principalSplitRateHci","0.0");
			}else{
				model.addObject("principalSplitRateHci",resultDet.get(0).getPrincipalSplitRateHci().toString());
			}
		}
		if(resultHead.getIsDelete().equals("N")){
			model.addObject("status","Deactive");
		}else{
			model.addObject("status","Reactive");
		}
		model.addObject("partnerName",xname);
		model.addObject("agreementId",agreementId);
		model.addObject("agreementDetail",resultDet);
		model.addObject("validFrom",format.format(resultHead.getValidFrom()));
		model.addObject("validTo",format.format(resultHead.getValidTo()));
		return model;
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public @ResponseBody String saveAgreement(	@RequestBody(required=false)String js,
												HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		JfsPartner vpartner = jfsPartnerService.getPartnerValueById(json.getString("partnerId"));
		String partnerId= "";
		String partnerName = "";
		if(vpartner == null){
			partnerId = "";
			partnerName = "";
		}else{
			partnerId = vpartner.getId();
			partnerName = vpartner.getName();
		}
		JfsAgreement xjfs = new JfsAgreement();
		xjfs.setCodePartner("HCI");
		xjfs.setCodeAgreement(json.getString("code"));
		xjfs.setNameAgreement(json.getString("name"));
		xjfs.setValidFrom(DateUtil.stringToDateWithFormat(json.getString("validFrom"),"MM/dd/yyyy"));
		xjfs.setValidTo(DateUtil.stringToDateWithFormat(json.getString("validTo"),"MM/dd/yyyy"));
		xjfs.setBankCode(partnerName);
		xjfs.setIdAgreement(Long.parseLong(json.getString("idAlias")));
		xjfs.setPartnerId(partnerId);
		xjfs.setIsDelete("N");
		xjfs.setDescription(json.getString("description"));
		xjfs.setCreatedBy(UserLoginUtil.getUsername());
		xjfs.setCreatedDate(new Date());
		xjfs.setFkAgreementType(json.getString("agreementType"));
		jfsAgreementService.agreementSave(xjfs);
		String query = "update JFS_AGREEMENT set IS_DELETE = 'N', ID_AGREEMENT = '"+json.getString("idAlias")+"' where IS_DELETE = '1'";
		jfsAgreementService.updateValueOfJfsAgreementWithQuery(query);
		return js;
	}
	
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public @ResponseBody String updateAgreement(@RequestBody(required=false) String js,
												HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		JfsAgreement agreement = jfsAgreementService.getJfsAgreementById(Long.parseLong(json.getString("agreementId")));
		String jsonBefore = new Gson().toJson(agreement);
		agreement.setCodeAgreement(json.getString("code"));
		agreement.setNameAgreement(json.getString("name"));
		agreement.setValidFrom(DateUtil.stringToDateWithFormat(json.getString("validFrom"),"MM/dd/yyyy"));
		agreement.setValidTo(DateUtil.stringToDateWithFormat(json.getString("validTo"),"MM/dd/yyyy"));
		agreement.setDescription(json.getString("desc"));
		jfsAgreementService.agreementUpdateWithQuery(agreement,json.getString("validFrom"),json.getString("validTo"));
		String jsonAfter = new Gson().toJson(agreement);
		AuditLog audit = new AuditLog();
		audit.setCreatedBy(UserLoginUtil.getUsername());	
		audit.setActivity(ActivityEnum.UPDATE);
		audit.setFullClassName("id.co.homecredit.jfs.cofin.core.model.JfsAgreement");
		audit.setDataBefore(jsonBefore);
		audit.setDataAfter(jsonAfter);
		audit = activityLogDao.saveWithNewPropagation(audit);
		return js;
	}
	
	@RequestMapping(value="/detail/save",method=RequestMethod.POST)
	public @ResponseBody String saveAgreementDetail(@RequestBody(required=false)String js,
													HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		JfsProduct jfsp = new JfsProduct();
		JfsAgreement agreement = jfsAgreementService.getJfsAgreementById(Long.parseLong(json.getString("agreementId")));
		JfsPartner partner = jfsPartnerService.getPartnerValueById(agreement.getPartnerId());
		jfsp.setBankProductCode(partner.getName().replace(" ","_")+"_PRODUCT");
		jfsp.setInterestRate(Double.parseDouble(json.getString("interestRate")));
		jfsp.setPrincipalSplitRate(Double.parseDouble(json.getString("principalSplitRate")));
		jfsp.setValidFrom(DateUtil.stringToDateWithFormat(json.getString("validFrom"),"MM/dd/yyyy"));
		jfsp.setValidTo(DateUtil.stringToDateWithFormat(json.getString("validTo"),"MM/dd/yyyy"));
		jfsp.setIdAgreement(Long.parseLong(json.getString("agreementId")));
		jfsp.setAdminFeeRate(Double.parseDouble(json.getString("adminFeeRate")));
		jfsp.setPenaltySplitRate(Double.parseDouble(json.getString("penaltySplitRate")));
		jfsp.setPrincipalSplitRateHci(Double.parseDouble(json.getString("principalSplitRateHci")));
		jfsProductService.productSave(jfsp);
		return js;
	}
	
	@RequestMapping(value="/activation",method=RequestMethod.POST)
	public @ResponseBody String activateAgreement(	@RequestBody(required=false)String js,
													HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		JfsAgreement agree = jfsAgreementService.getJfsAgreementById(Long.parseLong(json.getString("id")));
		String jsonBefore = new Gson().toJson(agree);
		String deleted = "";
		ActivityEnum activity = null;
		if(agree.getIsDelete().equals("Y")){
			deleted = "N";
			activity = ActivityEnum.UNDO_DELETE;
			agree.setIsDelete("N");
		}else{
			deleted = "Y";
			activity = ActivityEnum.SOFT_DELETE;
			agree.setIsDelete("N");
		}
		String jsonAfter = new Gson().toJson(agree);
		String query = "update JFS_AGREEMENT set IS_DELETE = '"+deleted+"' where ID_AGREEMENT = '"+json.getString("id")+"'";
		jfsAgreementService.updateValueOfJfsAgreementWithQuery(query);
		AuditLog audit = new AuditLog();
		audit.setCreatedBy(UserLoginUtil.getUsername());
		audit.setActivity(activity);
		audit.setFullClassName("id.co.homecredit.jfs.cofin.core.model.JfsAgreement");
		audit.setDataBefore(jsonBefore);
		audit.setDataAfter(jsonAfter);
		audit = activityLogDao.saveWithNewPropagation(audit);
		return js;
	}
	
	@RequestMapping(value="/detail/update",method=RequestMethod.POST)
	public @ResponseBody String updateDetail(	@RequestBody(required=false)String js,
												HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		String id = json.getString("id");
		String validFrom = json.getString("validFrom");
		String validTo = json.getString("validTo");
		JfsProduct product = jfsProductService.getProductById(id);
		String jsonBefore = new Gson().toJson(product);
		product.setValidFrom(DateUtil.stringToDateWithFormat(validFrom,"MM/dd/yyyy"));
		product.setValidTo(DateUtil.stringToDateWithFormat(validTo,"MM/dd/yyyy"));
		validFrom = validFrom.substring(3,5)+"/"+validFrom.substring(0,2)+"/"+validFrom.substring(6,10);
		validTo = validTo.substring(3,5)+"/"+validTo.substring(0,2)+"/"+validTo.substring(6,10);
		jfsProductService.updateValidDate(id,validFrom,validTo);
		String jsonAfter = new Gson().toJson(product);
		AuditLog audit = new AuditLog();
		audit.setCreatedBy(UserLoginUtil.getUsername());
		audit.setActivity(ActivityEnum.UPDATE);
		audit.setFullClassName("id.co.homecredit.jfs.cofin.core.model.JfsProduct");
		audit.setDataBefore(jsonBefore);
		audit.setDataAfter(jsonAfter);
		audit = activityLogDao.saveWithNewPropagation(audit);
		return js;
	}
	
	@RequestMapping(value="/product/{agreementId}",method=RequestMethod.GET)
	public ModelAndView getProductMapping(@PathVariable String agreementId)throws Exception{
		ModelAndView views = new ModelAndView();
		views.setViewName("agreementProductMapping");
		JfsAgreement agree = jfsAgreementService.getJfsAgreementById(Long.parseLong(agreementId));
		views.addObject("agreement",agree);
		List<JfsProductMapping> mapping = jfsProductMappingService.getProductMappingByAgreementId(Integer.parseInt(agreementId));
		views.addObject("productMapping",mapping);
		List<JfsProductHci> product = jfsProductHciService.getAllProductHci();
		views.addObject("product",product);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		views.addObject("validFrom",format.format(agree.getValidFrom()));
		views.addObject("validTo"	,format.format(agree.getValidTo()));
		String xname = jfsPartnerService.getPartnerName(agree.getPartnerId());
		views.addObject("partnerName",xname);
		views.addObject("agreementId",agreementId);
		return views;
	}
	
	@RequestMapping(value="/product/save",method=RequestMethod.POST)
	public @ResponseBody String saveProductMaping(	@RequestBody(required=false)String js,
													HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		String productCode = json.getString("productCode");
		String validFrom = json.getString("validFrom");
		String validTo = json.getString("validTo");
		JfsAgreement agree = jfsAgreementService.getJfsAgreementById(Long.parseLong(json.getString("agreementId")));
		JfsPartner partner = jfsPartnerService.getPartnerValueById(agree.getPartnerId());
		JfsProductMapping map = new JfsProductMapping();
		map.setCodeProduct(productCode);
		map.setBankProductCode(partner.getName()+"_PRODUCT");
		map.setValidFrom(DateUtil.stringToDateWithFormat(validFrom,"MM/dd/yyyy"));
		map.setValidTo(DateUtil.stringToDateWithFormat(validTo,"MM/dd/yyyy"));
		map.setIdAgreement(Long.parseLong(json.getString("agreementId")));
		map.setDtimeInserted(new Date());
		map.setIsDelete(YesNoEnum.N);
		map.setCreatedBy(UserLoginUtil.getUsername());
		map.setCreatedDate(new Date());
		jfsProductMappingService.productMappingSave(map);
		String query = "update JFS_PRODUCT_MAPPING set IS_DELETE = 'N' where IS_DELETE = '1'";
		jfsAgreementService.updateValueOfJfsAgreementWithQuery(query);
		return js;
	}
	
	@RequestMapping(value="/product/status",method=RequestMethod.POST)
	public @ResponseBody String updateStatusMapping(@RequestBody(required=false)String js,
													HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		SimpleDateFormat xformat = new SimpleDateFormat("dd/MM/yyyy");
		String status = json.getString("status");
		String id = json.getString("id");
		JfsProductMapping pmap = jfsProductMappingService.getProductMappingById(id);
		String jsonBefore = new Gson().toJson(pmap);
		ActivityEnum act = null;
		if(status.equals("Deactive")){
			status = "Y";
			pmap.setIsDelete(YesNoEnum.Y);
			act = ActivityEnum.SOFT_DELETE;
		}else{
			status = "N";
			pmap.setIsDelete(YesNoEnum.N);
			act = ActivityEnum.UNDO_DELETE;
		}
		String updatedBy = UserLoginUtil.getUsername();
		Date updatedDate = new Date();
		pmap.setUpdatedBy(updatedBy);
		pmap.setUpdatedDate(updatedDate);	
		String jsonAfter = new Gson().toJson(pmap);
		jfsProductMappingService.jfsProductMappingUpdate(status,updatedBy,xformat.format(updatedDate),id);
		AuditLog audit = new AuditLog();
		audit.setCreatedBy(updatedBy);
		audit.setActivity(act);
		audit.setFullClassName("id.co.homecredit.jfs.cofin.core.service.JfsProductMappingService");
		audit.setDataBefore(jsonBefore);
		audit.setDataAfter(jsonAfter);
		audit = activityLogDao.saveWithNewPropagation(audit);
		return js;
	}
	
}
