package id.co.homecredit.jfs.cofin.ui.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.engine.jdbc.batch.spi.Batch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.BatchJob;
import id.co.homecredit.jfs.cofin.core.model.BatchJobExecution;
import id.co.homecredit.jfs.cofin.core.model.BatchStep;
import id.co.homecredit.jfs.cofin.core.model.BatchStepExecution;
import id.co.homecredit.jfs.cofin.core.model.ExportLog;
import id.co.homecredit.jfs.cofin.core.service.BatchJobExecutionService;
import id.co.homecredit.jfs.cofin.core.service.BatchJobService;
import id.co.homecredit.jfs.cofin.core.service.BatchStepExecutionService;
import id.co.homecredit.jfs.cofin.core.service.BatchStepService;
import id.co.homecredit.jfs.cofin.core.service.ExportLogService;

/**
 * {@link Batch} controller for implementing Spring MVC.
 *
 * @author fransisco.situmorang
 *
 */

@Controller
@RequestMapping(value = "/batch")
public class BatchController {

	private static final Logger log = LogManager.getLogger(BatchController.class);

	@Autowired
	private BatchJobService batchJobService;

	@Autowired
	BatchJobExecutionService batchJobExecutionService;

	@Autowired
	BatchStepExecutionService batchStepExecutionService;

	@Autowired
	ExportLogService exportLogService;

	@Autowired
	BatchStepService batchStepService;

	@RequestMapping(value = "/stop-generate", method = RequestMethod.POST)
	public @ResponseBody String activationAgreement(@RequestBody(required = false) String js,
			HttpServletResponse response) throws JSONException {
		JSONObject json = new JSONObject(js);
		BatchJob batchJob = batchJobService.getBatchJobById(json.getString("id"));
		batchJob.setFlagGenerate(YesNoEnum.valueOf(json.getString("flag")));
		batchJobService.save(batchJob);
		return js;
	}

	@RequestMapping(value = "process/job/{jobsname}", method = RequestMethod.GET)
	public ModelAndView batchJobExecution(@PathVariable String jobsname, HttpServletResponse response)
			throws JSONException, ParseException {
		List<BatchJobExecution> jobExecution = batchJobExecutionService.getBatchJobExecutionByJobInstanceId(jobsname);
		log.debug("job execution size {}", jobExecution.size());

		ModelAndView mav = new ModelAndView("batchJobExecution");
		mav.addObject("batchJobExecution", jobExecution);
		mav.addObject("jobsName", jobsname);
		return mav;
	}

	@RequestMapping(value = "process/job/{jobsname}/{start}/{end}", method = RequestMethod.GET)
	public ModelAndView batchJobExecution(@PathVariable String jobsname, @PathVariable String start,
			@PathVariable String end, HttpServletResponse response) throws JSONException, ParseException {
		Date startDate = DateUtil.stringToDateWithFormat(start, "MM-dd-yyyy");
		Date endDate = DateUtil.stringToDateWithFormat(end, "MM-dd-yyyy");

		List<BatchJobExecution> jobExecution = batchJobExecutionService.getBatchJobExecutionByJobInstanceId(jobsname,
				startDate, endDate);
		log.debug("job execution size {}", jobExecution.size());

		ModelAndView mav = new ModelAndView("batchJobExecution");
		mav.addObject("batchJobExecution", jobExecution);
		mav.addObject("startDate", start.replaceAll("-", "/"));
		mav.addObject("endDate", end.replaceAll("-", "/"));
		mav.addObject("jobsName", jobsname);
		return mav;
	}

	@RequestMapping(value = "/management", method = RequestMethod.GET)
	public ModelAndView batchManagement(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException, ParseException {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("batchManagement");
		List<BatchJob> batchJob = batchJobService.getAllBatchJob(true);
		mav.addObject("batchJob", batchJob);
		return mav;
	}

	@RequestMapping(value = "/management/detail/{batchJobId}", method = RequestMethod.GET)
	public ModelAndView batchManagementDetail(@PathVariable String batchJobId, HttpServletResponse response)
			throws JSONException, ParseException {
		log.debug("batch job id {}", batchJobId);
		List<BatchStep> batchStep = batchStepService.getBatchStepByJobId(batchJobId);
		log.debug("batch step size {}", batchStep.size());

		ModelAndView mav = new ModelAndView("batchManagementDetail");
		mav.addObject("batchStep", batchStep);
		return mav;
	}

	@RequestMapping(value = "/process", method = RequestMethod.GET)
	public ModelAndView batchProcess(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException, ParseException {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("batchProcess");
		List<BatchJob> batchJob = batchJobService.getAllBatchJob(true);

		mav.addObject("batchJob", batchJob);
		return mav;
	}

	@RequestMapping(value = "process/job/step/{jobExecutionId}", method = RequestMethod.GET)
	public ModelAndView batchStepExecution(@PathVariable String jobExecutionId, HttpServletResponse response)
			throws JSONException, ParseException {
		List<BatchStepExecution> stepExecution = batchStepExecutionService
				.getBatchStepExecutionByJobExecutionId(Long.parseLong(jobExecutionId));
		log.debug("step execution size {}", stepExecution.size() + " JOBExecutionID" + Long.parseLong(jobExecutionId));

		ModelAndView mav = new ModelAndView("batchStepExecution");
		mav.addObject("batchStepExecution", stepExecution);
		return mav;
	}

	@RequestMapping(value = "process/job/step/file/{stepExecutionId}", method = RequestMethod.GET)
	public ModelAndView fileGenerateStepExecution(@PathVariable String stepExecutionId, HttpServletResponse response)
			throws JSONException, ParseException {
		List<ExportLog> ExportLog = exportLogService.getExportLogByStepExecutionId(Long.parseLong(stepExecutionId));
		log.debug("export log size {}", ExportLog.size());

		ModelAndView mav = new ModelAndView("fileBatch");
		mav.addObject("fileBatch", ExportLog);
		return mav;
	}
	
	

	@RequestMapping(value = "process/job/step/file/download/{exportLogId}", method = RequestMethod.GET)
	public void downloadFile(@PathVariable String exportLogId, HttpServletResponse response)
			throws JSONException, ParseException,IOException {
		ExportLog exportLog = exportLogService.get(exportLogId);
		try {
		
		File file = new File(exportLog.getFilename());
		 InputStream is = new FileInputStream(file);
		 
	        // MIME type of the file
	        response.setContentType("application/octet-stream");
	        // Response header
	        response.setHeader("Content-Disposition", "attachment; filename=\""
	                + file.getName() + "\"");
	        // Read from the file and write into the response
	        OutputStream os = response.getOutputStream();
	        byte[] buffer = new byte[1024];
	        int len;
	        while ((len = is.read(buffer)) != -1) {
	            os.write(buffer, 0, len);
	        }
	        os.flush();
	        os.close();
	        is.close(); 
       
		}catch (IOException e) {
			// TODO: handle exception
		
		}
		
	}
	
	
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String generateCron(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException {
		JSONObject json = new JSONObject(js);
		String cronExpression = json.getString("expresion");
		log.debug("new cron expression {}", cronExpression);
		BatchJob batchJob = batchJobService.getBatchJobById(json.getString("id"));
		batchJob.setCronExpression(cronExpression);
		batchJobService.save(batchJob);
		return js;
	}

	@RequestMapping(value = "/runSSHCommand", method = RequestMethod.POST)
	public @ResponseBody String runBatch(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException, JSchException {
		JSONObject json = new JSONObject(js);
		JSch jsch = new JSch();

		String privateKeyPath = "/cofin/private_key";
		jsch.addIdentity(privateKeyPath, "passphrase");
		Session session = jsch.getSession("weblogic", "wls01-idcofin1.id.nonprod", 22);
		new Thread(() -> {

			Runnable runnable = new Runnable() {
				@Override
				public void run() {

					try {

						UserInfo ui = new MyUserInfo();
						session.setUserInfo(ui);
						session.connect();

						// Channel channel=session.openChannel("shell");
						// channel.setInputStream(System.in);
						// channel.setOutputStream(System.out);
						//
						// channel.connect();
						//

						ChannelExec channelExec = (ChannelExec) session.openChannel("exec");

						try {
							InputStream in = channelExec.getInputStream();

							channelExec.setCommand(json.getString("command"));

							channelExec.connect();

							BufferedReader reader = new BufferedReader(new InputStreamReader(in));
							String line;
							int index = 0;

							while ((line = reader.readLine()) != null) {
								System.out.println(++index + " : " + line);
								log.info(++index + " : " + line);
							}

							int exitStatus = channelExec.getExitStatus();
							channelExec.disconnect();
							session.disconnect();
							if (exitStatus < 0) {
								System.out.println("Done, but exit status not set!");
								log.info("Done, but exit status not set!");
							} else if (exitStatus > 0) {
								System.out.println("Done, but with error!");
								log.info("Done, but with error!");
							} else {
								System.out.println("Done!");
								log.info("Done!");
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} catch (JSchException e) {
						throw new RuntimeException("Failed to create Jsch Session object.", e);
					}
				}
			};

			Thread thread = new Thread(runnable);
			thread.start();

		}).start();

		return "Batch Run in Background";

	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public @ResponseBody String test() throws JSchException {
		log.info("D");
		JSch jsch = new JSch();
		log.info("C");
		String privateKeyPath = "/cofin/private_key";
		jsch.addIdentity(privateKeyPath, "passphrase");
		Session session = jsch.getSession("weblogic", "wls01-idcofin1.id.nonprod", 22);
		log.info("B");
		new Thread(() -> {

			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					// BatchProcessBtpn.main(param);

					try {
						log.info("A");
						UserInfo ui = new MyUserInfo();
						session.setUserInfo(ui);
						session.connect();

						// Channel channel=session.openChannel("shell");
						// channel.setInputStream(System.in);
						// channel.setOutputStream(System.out);
						//
						// channel.connect();
						//

						ChannelExec channelExec = (ChannelExec) session.openChannel("exec");

						try {
							InputStream in = channelExec.getInputStream();

							channelExec.setCommand("java -jar /cofin/cofin-batch.jar d");

							channelExec.connect();
							log.info("1");
							BufferedReader reader = new BufferedReader(new InputStreamReader(in));
							String line;
							int index = 0;

							while ((line = reader.readLine()) != null) {
								System.out.println(++index + " : " + line);
								log.info(++index + " : " + line);
							}
							log.info("2");
							int exitStatus = channelExec.getExitStatus();
							// channelExec.disconnect();
							// session.disconnect();
							log.info("3");
							if (exitStatus < 0) {
								System.out.println("Done, but exit status not set!");
								log.info("Done, but exit status not set!");
							} else if (exitStatus > 0) {
								System.out.println("Done, but with error!");
								log.info("Done, but with error!");
							} else {
								System.out.println("Done!");
								log.info("Done!");
							}
						} catch (Exception e) {
							// TODO: handle exception
							log.info(e.toString());
							e.printStackTrace();

						}

					} catch (JSchException e) {
						log.info(e.toString());
						throw new RuntimeException("Failed to create Jsch Session object.", e);
					}
				}
			};

			Thread thread = new Thread(runnable);
			thread.start();

		}).start();

		return "Success";

	}

	@RequestMapping(value = "/runBatchJar", method = RequestMethod.POST)
	public @ResponseBody String runBatchJar(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException, JSchException, JSONException {
		JSONObject json = new JSONObject(js);
		JSONArray JsonArray = new JSONArray();
		JSONObject jsData = new JSONObject();
		new Thread(() -> {

			Runnable runnable = new Runnable() {
				@Override
				public void run() {

					try {
						String s = null;

						try {
							Process p = Runtime.getRuntime().exec(json.getString("command"));

							BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

							BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

							// read the output from the command
							System.out.println("Here is the standard output of the command:\n");
							while ((s = stdInput.readLine()) != null) {
								System.out.println(s);
								log.info(s);
							}

							// read any errors from the attempted command
							System.out.println("Here is the standard error of the command (if any):\n");
							while ((s = stdError.readLine()) != null) {
								System.out.println(s);
								log.error(s);
							}

						} catch (Exception e) {

							log.error(e.getMessage());

						}

						
					} catch (Exception e) {

						// TODO: handle exception
						log.error(e.getMessage());

					}
				}
			};

			Thread thread = new Thread(runnable);
			thread.start();

		}).start();
		
		jsData.put("Success", "Batch run in background");
		JsonArray.put(jsData);
		
		return JsonArray.toString();

	}

	public static class MyUserInfo implements UserInfo {
		public String getPassword() {
			return "p@ssw0rd";
		}

		public boolean promptYesNo(String str) {
			return true;
		}

		public String getPassphrase() {
			return "p@ssw0rd";
		}

		public boolean promptPassphrase(String message) {

			return true;

		}

		public boolean promptPassword(String message) {
			return false;
		}

		public void showMessage(String message) {
			System.out.println(message);
		}
	}
}
