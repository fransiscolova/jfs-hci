package id.co.homecredit.jfs.cofin.ui.controller;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentBsl;
import id.co.homecredit.jfs.cofin.core.model.JfsInpayAllocationFix;
import id.co.homecredit.jfs.cofin.core.model.JfsOfiPayment;
import id.co.homecredit.jfs.cofin.core.model.JfsPaymentBankSim;
import id.co.homecredit.jfs.cofin.core.model.JfsContract;
import id.co.homecredit.jfs.cofin.core.model.JfsSchedule;
import id.co.homecredit.jfs.cofin.core.service.JfsInpayAllocationFixService;
import id.co.homecredit.jfs.cofin.core.service.JfsOfiPaymentService;
import id.co.homecredit.jfs.cofin.core.service.JfsPaymentBslService;
import id.co.homecredit.jfs.cofin.core.service.JfsContractService;
import id.co.homecredit.jfs.cofin.core.service.JfsScheduleService;

/**
 * controller for handler OFI Calculation.
 * 
 * @author denny.afrizal01
 *
 */

@Controller
@RequestMapping(value="/ofi")
public class CalculateOFIController{
	private static final Logger log = LogManager.getLogger(CalculateOFIController.class);
	
	@Autowired
	private JfsPaymentBslService jfsPaymentBslService;
	
	@Autowired
	private JfsOfiPaymentService jfsOfiPaymentService;

	@Autowired
	private JfsContractService jfsContractService;
	
	@Autowired
	private JfsInpayAllocationFixService jfsInpayAllocationFixService;

	@Autowired
    private JfsScheduleService jfsScheduleService;

	@RequestMapping(value="/JfsOfiPayment",method={RequestMethod.GET,RequestMethod.POST})
	public void getOfiPaymentJfs(){
		log.info("Get Normal Payment Calculation");
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date valfr = sdf.parse("2018-01-01");
			Date valto = sdf.parse("2018-01-10");
			Date sekarang = sdf.parse("2018-01-11");
//			List<JfsPaymentBsl> bsl = jfsPaymentBslService.getBslPaymentBetweenDate(valfr,valto);
			List<JfsPaymentBsl> bsl = jfsPaymentBslService.getBslPaymentByDate(sekarang);
			if(bsl.size()>0){
				for(int x=0;x<bsl.size();x++){
					System.out.println(bsl.get(x).toString());
					Double principal = 0.0;
					Double interest = 0.0;
					Double prnhci = 0.1*bsl.get(x).getAmtPrincipal();
					Double inthci = 0.1*bsl.get(x).getAmtInterest();
					JfsInpayAllocationFix alloc = jfsInpayAllocationFixService.getAllocationAmountValue(bsl.get(x).getIncomingPaymentId(),bsl.get(x).getDateInstalment());
					if(alloc==null){
						principal = bsl.get(x).getAmtPrincipal()-prnhci;
						interest = bsl.get(x).getAmtInterest()-inthci;
					}else{
						principal = bsl.get(x).getAmtPrincipal()-Double.parseDouble(alloc.getAmtPrincipal())-prnhci;
						interest = bsl.get(x).getAmtInterest()-Double.parseDouble(alloc.getAmtInterest())-inthci;
					}
					principal = (double) Math.round(principal*100)/100;
					interest = (double) Math.round(interest*100)/100;
					JfsOfiPayment pay = new JfsOfiPayment();
					pay.setIncomingPaymentId(bsl.get(x).getIncomingPaymentId());
					pay.setTextContractNumber(bsl.get(x).getTextContractNumber());
					pay.setDueDate(bsl.get(x).getDateInstalment());
					pay.setPaymentDate(bsl.get(x).getPaymentDate());
					pay.setOfiPrincipal(principal);
					pay.setOfiInterest(interest);
					pay.setDtimeCreated(new Date());
					pay.setCreatedBy("JFS_SYSTEM");
					pay.setVer(1);
					pay.setIsActive("Y");
					System.out.println(pay.toString());
					jfsOfiPaymentService.saveData(pay);
					Integer receord = x+1;
					System.out.println("Success Save Record "+receord);
				}
				System.out.println("Complete To Save Data");
			}
		}catch(Exception d){
			d.printStackTrace();
		}
	}

//	@RequestMapping(value="/JfsPaymentBankSim",method={RequestMethod.GET,RequestMethod.POST})
//	public void getSimulate(HttpServletRequest request,
//							HttpServletResponse response)throws Exception{
//		String[] array = {"3611135011","3613674273","3613937294","3614274785","3614347682","3614374071","3614374258","3614385742","3614415090","3614423989"};
//		Integer rec = 0;
//		try{
//			for(int x=0;x<array.length;x++){
//				List<JfsPaymentBsl> bsl = jfsPaymentBslService.getBslPaymentByContractNumber(array[x]);
//				if(bsl.size()>0){
//					for(int z=0;z<bsl.size();z++){
//						JfsSchedule sch = jfsScheduleService.getScheduleJfs(bsl.get(z).getTextContractNumber(),String.valueOf(bsl.get(z).getInstalmentNumber()));
//						if(sch==null){
//							System.out.println("Null Value From JfsSchedule");
//						}else{
//							JfsContract kontrak = jfsContractService.getDetailJfsContract(bsl.get(z).getTextContractNumber());
//							if(kontrak==null){
//								System.out.println("Null Value From JfsContract");
//							}else{
//								Double amtTotal = bsl.get(z).getAmtPrincipal() + bsl.get(z).getAmtInterest();
//								Double amtPrincipal = 0.0;
//								Double amtInterest = sch.getInterest();
//								amtTotal = amtTotal * (Double.parseDouble(kontrak.getBankInstallment()) / Double.parseDouble(kontrak.getAmtInstallment()));
//								amtPrincipal = amtTotal - amtInterest;
//								amtPrincipal = (double) Math.round(amtPrincipal*100)/100;
//								JfsPaymentBankSim pay = new JfsPaymentBankSim();
//								pay.setIncomingPaymentId(bsl.get(z).getIncomingPaymentId());
//								pay.setTextContractNumber(bsl.get(z).getTextContractNumber());
//								pay.setDateInstalment(bsl.get(z).getDateInstalment());
//								pay.setInstalmentNumber(bsl.get(z).getInstalmentNumber());
//								pay.setPaymentDate(bsl.get(z).getPaymentDate());
//								pay.setAmtPrincipal(amtPrincipal);
//								pay.setAmtInterest(amtInterest);
//								pay.setAmtPenalty(0.0);
//								pay.setAmtOverpayment(0.0);
//								pay.setDtimeCreated(new Date());
//								pay.setCreatedBy("JFS_SYSTEM");
//								pay.setVersion(1);
//								jfsOfiPaymentService.saveData(pay);
//								rec = rec+1;
//								System.out.println("Success Save Record "+rec);
//							}
//						}
//					}
//				}else{
//					System.out.println("Null Value From JfsPaymentBsl");
//				}
//				System.out.println("Complete To Save Data");
//			}
//		}catch(Exception g){
//			g.printStackTrace();
//		}
//	}

//	@RequestMapping(value="/JfsPaymentBankSim",method={RequestMethod.GET,RequestMethod.POST})
//	public void getSimulate(HttpServletRequest request,
//							HttpServletResponse response)throws Exception{
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		String sekarang = "2017-11-20";
//		Integer rec = 0;
//		try{
//			List<JfsPaymentBsl> bsl = jfsPaymentBslService.getBslPaymentByDate(sdf.parse(sekarang));
//			if(bsl.size()>0){
//				for(int x=0;x<bsl.size();x++){
//                    JfsSchedule sch = jfsScheduleService.getScheduleJfs(bsl.get(x).getTextContractNumber(),String.valueOf(bsl.get(x).getInstalmentNumber()));
//                    if(sch==null){
//                        System.out.println("Null Value From JFS_SCHEDULE");
//                    }else{
//                        Double amtTotal = bsl.get(x).getAmtPrincipal() + bsl.get(x).getAmtInterest();
//                        Double amtInterest = 0.0;
//                        Double amtPrincipal = 0.0;
//                        JfsContract kontrak = jfsContractService.getDetailJfsContract(bsl.get(x).getTextContractNumber());
//                        if(kontrak==null){
//                            System.out.println("Null Value From JFS_CONTRACT");
//                        }else{
//                            amtTotal = amtTotal * (Double.parseDouble(kontrak.getBankInstallment()) / Double.parseDouble(kontrak.getAmtInstallment()));
//                            amtInterest = Double.parseDouble(sch.getInterest());
//                            amtPrincipal = amtTotal - Double.parseDouble(sch.getInterest());
//                            amtPrincipal = (double) Math.round(amtPrincipal*100)/100;
//                            JfsPaymentBankSim pay = new JfsPaymentBankSim();
//                            pay.setIncomingPaymentId(bsl.get(x).getIncomingPaymentId());
//                            pay.setTextContractNumber(bsl.get(x).getTextContractNumber());
//                            pay.setDateInstalment(bsl.get(x).getDateInstalment());
//                            pay.setInstalmentNumber(bsl.get(x).getInstalmentNumber());
//                            pay.setPaymentDate(bsl.get(x).getPaymentDate());
//                            pay.setAmtPrincipal(amtPrincipal);
//                            pay.setAmtInterest(amtInterest);
//                            pay.setAmtPenalty(0.0);
//                            pay.setAmtOverpayment(0.0);
//                            pay.setDtimeCreated(new Date());
//                            pay.setCreatedBy("JFS_SYSTEM");
//                            pay.setVersion(1);
//                            jfsOfiPaymentService.saveData(pay);
//                            rec = rec+1;
//                            System.out.println("Success Save Record "+rec);
//                        }
//                    }
//				}
//				System.out.println("Complete Save Data");
//			}
//		}catch(Exception h){
//			h.printStackTrace();
//		}
//	}
	
}
