package id.co.homecredit.jfs.cofin.ui.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.common.variable.constant.CommonConstant.StringChar;
import id.co.homecredit.jfs.cofin.core.model.*;
import id.co.homecredit.jfs.cofin.core.service.*;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;
import id.co.homecredit.jfs.cofin.core.model.dto.JfsCustomerBsl;
import id.co.homecredit.jfs.cofin.common.model.DropDownListItem;
import id.co.homecredit.jfs.cofin.ui.model.InsertMessage;

/**
 * Controller for handling login-related request.
 *
 * @author muhammad.muflihun
 *
 */
@Controller
@RequestMapping(value = "/contract")
public class ContractController {
    private static final Logger log = LogManager.getLogger(ContractController.class);
    
    @Autowired
    JfsContractService jfsContractService;
    
    @Autowired
    JfsAgreementService jfsAgreementService;
    
    @Autowired
    JfsPartnerService jfsPartnerService;
    
    @Autowired
    JfsContractWoService jfsContractWoService;
    
    @Autowired
    JfsPaymentIntService jfsPaymentIntService;
    
    @Autowired
    JfsReconciliationService jfsReconciliationService;
    
    @Autowired
    JfsContractEtService jfsContractEtService;
    
    @Autowired
    AgreementTypeService agreementTypeService;
    
    @Autowired
    JfsCustomerService jfsCustomerService;
    
    @Autowired
    JfsPaymentAllocationService jfsPaymentAllocationService;
    
    @Autowired
    JfsScheduleBslService jfsScheduleBslService;
    
    @Autowired
    VwCofinTownService vwCofinTownService;
    
    @Autowired
    VwCofinContractDetailService vwCofinContractDetailService;
    
    @Autowired
    CofinConfigService cofinConfigService;

    @Autowired
    JfsScheduleService jfsScheduleService;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView redirectToHome() {
        log.info("redirect to contract page");
        ModelAndView mav = new ModelAndView();
        mav.setViewName("showContract");
        List<DropDownListItem> drop = cofinConfigService.getJfsContractStatus();
        mav.addObject("enumStatusContracts", drop);
        List<JfsPartner> partner = jfsPartnerService.getAllPartner();
        mav.addObject("partners",partner);
        return mav;
    }
    
    @RequestMapping(value="/getAgreement",method = RequestMethod.GET)
    public @ResponseBody List<DropDownListItem> getAgreement(HttpServletRequest request)throws IOException,Exception{
    	List<DropDownListItem> drop = new ArrayList<DropDownListItem>();
    	String partner = request.getParameter("partner");
    	List<JfsAgreement> agreement = jfsAgreementService.getJfsAgreementByPartnerId(partner);
    	for(int x=0;x<agreement.size();x++){
    		drop.add(new DropDownListItem(agreement.get(x).getNameAgreement(),String.valueOf(agreement.get(x).getIdAgreement())));
    	}
    	return drop;
    }
    
    @RequestMapping(value = "/search", method = {RequestMethod.GET,RequestMethod.POST})
    public void searctContract(	HttpServletRequest request,
    							HttpSession session,
    							HttpServletResponse response)throws IOException,Exception{
    	InsertMessage msg = new InsertMessage();
    	List<InsertMessage> lmsg = new ArrayList<InsertMessage>();
    	List<JfsContract> contract = new ArrayList<JfsContract>();
    	String contractCode = request.getParameter("contractCode");
    	String status = request.getParameter("status");
    	String idAgreement = StringChar.EMPTY_STRING;
    	if(request.getParameter("agreement").equals(StringChar.EMPTY_STRING) || request.getParameter("agreement")==null){
    	    idAgreement = "0";
        }else{
    	    idAgreement = request.getParameter("agreement");
        }
    	String dateFrom = request.getParameter("from");
    	String dateTo = request.getParameter("to");
    	if(status.equals("W")){
    		status = "";
    	}
    	if(dateFrom.equals(StringChar.EMPTY_STRING) && dateTo.equals(StringChar.EMPTY_STRING)){
            contract = jfsContractService.getJfsContractWithoutDate(contractCode,status,Long.parseLong(idAgreement));
        }else if(!dateFrom.equals(StringChar.EMPTY_STRING) && dateTo.equals(StringChar.EMPTY_STRING)){
            dateFrom = dateFrom.substring(6,10)+"-"+dateFrom.substring(0,2)+"-"+dateFrom.substring(3,5);
            contract = jfsContractService.getJfsContractWithDate(contractCode,status,Long.parseLong(idAgreement),DateUtil.stringToDate(dateFrom));
        }else if(dateFrom.equals(StringChar.EMPTY_STRING) && !dateTo.equals(StringChar.EMPTY_STRING)){
            dateTo = dateTo.substring(6,10)+"-"+dateTo.substring(0,2)+"-"+dateTo.substring(3,5);
            contract = jfsContractService.getJfsContractWithDate(contractCode,status,Long.parseLong(idAgreement),DateUtil.stringToDate(dateTo));
        }else{
            dateFrom = dateFrom.substring(6,10)+"-"+dateFrom.substring(0,2)+"-"+dateFrom.substring(3,5);
            dateTo = dateTo.substring(6,10)+"-"+dateTo.substring(0,2)+"-"+dateTo.substring(3,5);
            contract = jfsContractService.getJfsContractBetweenDate(contractCode,status,Long.parseLong(idAgreement),DateUtil.stringToDate(dateFrom),DateUtil.stringToDate(dateTo));
        }
    	if(contract.size()==0){
    		msg.setResult(0);
    		msg.setMessage("Contract Not Found");
    		lmsg.add(msg);
    	}else{
    		session.setAttribute("data",contract);
    		msg.setResult(1);
    		msg.setData1(new Gson().toJson(contract));
    		lmsg.add(msg);
    	}
    	response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(lmsg));
    }
    
    @RequestMapping(value="/detail/{contractNumber}",method=RequestMethod.GET)
    public ModelAndView getContractDetail(@PathVariable String contractNumber)throws Exception{
    	ModelAndView views = new ModelAndView();
    	JfsContract cont = jfsContractService.getHeaderJfsContract(contractNumber);
    	if(cont==null){
            views.setViewName("showContract");
            views.addObject("message","Contract Number "+contractNumber+" Not Found");
            return views;
        }else{
    	    JfsAgreement agree = jfsAgreementService.getJfsAgreementById(cont.getIdAgreement());
    	    if(agree==null){
                views.setViewName("showContract");
                views.addObject("message","Agreement Number For Contract Number "+contractNumber+" Not Found");
                return views;
            }else{
                CofinConfig config = cofinConfigService.getDescriptionByValue(cont.getStatus());
                if(config==null){
                    views.setViewName("showContract");
                    views.addObject("message","Status Description For Contract Number "+contractNumber+" Not Found");
                    return views;
                }else{
                    JfsSchedule sch = jfsScheduleService.getScheduleJfs(cont.getTextContractNumber(),cont.getSendTenor());
                    if(sch==null){
                        views.setViewName("showContract");
                        views.addObject("message","Payment Schedule For Contract Number "+contractNumber+" Not Found");
                        return views;
                    }else{
                        views.setViewName("showContractDetail");
                        String stat = StringChar.EMPTY_STRING;
                        if(cont.getStatus().equals("A")){
                            stat = "Active";
                        }else{
                            stat = "Inactive";
                        }
                        views.addObject("status",stat);
                        views.addObject("contractNumber",contractNumber);
                        views.addObject("contract",cont);
                        views.addObject("config",config);
                        views.addObject("schedule",sch);
                        views.addObject("agreement",agree);
                        return views;
                    }
                }
            }
        }
    }

    @RequestMapping(value="/detail/jfsStatus",method={RequestMethod.GET,RequestMethod.POST})
    public void detailJfsStatus(HttpServletRequest request,
    							HttpServletResponse response)throws IOException,Exception{
    	InsertMessage pesan = new InsertMessage();
    	List<InsertMessage> listPesan = new ArrayList<InsertMessage>();
    	String contractNumber = request.getParameter("contractNumber");
    	pesan.setResult(1);
    	JfsContract cont = jfsContractService.getHeaderJfsContract(contractNumber);
    	if(cont==null){
            pesan.setData1(new Gson().toJson(new JfsContract()));
        }else{
            pesan.setData1(new Gson().toJson(cont));
        }
    	JfsAgreement agree = jfsAgreementService.getJfsAgreementById(cont.getIdAgreement());
    	if(agree==null){
            pesan.setData2(new Gson().toJson(new JfsAgreement()));
            pesan.setMessage(StringChar.STRIP);
        }else{
            pesan.setData2(new Gson().toJson(agree));
            if(cont==null){
                pesan.setMessage(StringChar.STRIP);
            }else{
                if(cont.getBankClawbackDate().equals(StringChar.STRIP) || cont.getBankClawbackDate()==null){
                    pesan.setMessage(StringChar.STRIP);
                }else{
                    AgreementType xtyp = agreementTypeService.getAgreementTypeById(agree.getFkAgreementType());
                    pesan.setMessage(xtyp.getAgreementCategory()+" Clawback");
                }
            }
        }
    	JfsPartner partner = jfsPartnerService.getPartnerValueById(agree.getPartnerId());
    	if(partner==null){
            pesan.setData3(new Gson().toJson(new JfsPartner()));
        }else{
            pesan.setData3(new Gson().toJson(partner));
        }
    	JfsContractWo xcont = jfsContractWoService.getContractWoByContractNumber(contractNumber);
    	if(xcont==null){
            pesan.setData4(new Gson().toJson(new JfsContractWo()));
        }else{
            pesan.setData4(new Gson().toJson(xcont));
        }
    	List<JfsPaymentInt> payment = jfsPaymentIntService.getPaymentIntByContractNumber(contractNumber);
    	if(payment.size()==0){
            pesan.setData5(new Gson().toJson(new ArrayList<JfsPaymentInt>()));
        }else{
            pesan.setData5(new Gson().toJson(payment));
        }
    	List<JfsReconciliation> recon = jfsReconciliationService.getReconciliationByContractNumber(contractNumber);
    	if(recon.size()==0){
            pesan.setData6(new Gson().toJson(new ArrayList<JfsReconciliation>()));
        }else{
            pesan.setData6(new Gson().toJson(recon));
        }
    	List<JfsContractEt> contractEt = jfsContractEtService.getContractEtByContractNumber(contractNumber);
    	String etRequest = StringChar.EMPTY_STRING;
    	String etReqDate = StringChar.EMPTY_STRING;
    	String etReqType = StringChar.EMPTY_STRING;
    	String etReqRegDate = StringChar.EMPTY_STRING;
    	if(contractEt.size()==0){
            pesan.setData7(new Gson().toJson(new ArrayList<JfsContractEt>()));
    		etRequest = "No";
    		etReqDate = StringChar.STRIP;
    		etReqType = StringChar.STRIP;
    		etReqRegDate = StringChar.STRIP;
    	}else{
    	    for(int x=0;x<contractEt.size();x++){
                JfsPaymentInt zint = jfsPaymentIntService.getByContractNumberAndIncomingPayment(contractEt.get(x).getTextContractNumber(),contractEt.get(x).getIncomingPaymentId());
                if(zint==null){
                    contractEt.get(x).setUpdatedBy(StringChar.STRIP);
                }else{
                    CofinConfig cfg = cofinConfigService.getDescriptionByValue(zint.getStatus());
                    if(cfg==null){
                        contractEt.get(x).setUpdatedBy(StringChar.STRIP);
                    }else{
                        contractEt.get(x).setUpdatedBy(cfg.getDesc01());
                    }
                }
            }
            pesan.setData7(new Gson().toJson(contractEt));
    		etRequest = "Yes";
    		etReqDate = DateUtil.dateToString(contractEt.get(0).getDateSigned());
    		if(contractEt.get(0).getCntEtDays()<=15){
                etReqType = "Within 15";
            }else{
    		    etReqType = "After";
            }
    		etReqRegDate = DateUtil.dateToString(contractEt.get(0).getDateEt());
    	}
    	pesan.setData8(etRequest);
    	pesan.setData9(etReqDate);
    	pesan.setData10(etReqType);
    	pesan.setData11(etReqRegDate);
    	listPesan.add(pesan);
    	response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(listPesan));
    }

//    @RequestMapping(value="/detail/client",method={RequestMethod.GET,RequestMethod.POST})
//    public void getClientContract(	HttpServletRequest request,
//    								HttpServletResponse response)throws IOException{
//    	InsertMessage pesan = new InsertMessage();
//    	List<InsertMessage> listPesan = new ArrayList<InsertMessage>();
//    	String cuid = request.getParameter("cuid");
//    	if(cuid.equals("-")){
//    		pesan.setResult(0);
//    		pesan.setMessage("Client Not Found Because CUID Is Empty Or Not Found");
//    		listPesan.add(pesan);
//    	}else{
//    		JfsCustomerBsl bsl = jfsCustomerService.getDetailCustomer(cuid);
//    		if(bsl==null){
//        		pesan.setResult(0);
//            	pesan.setMessage("Client Data With CUID "+cuid+" Not Found");
//            	listPesan.add(pesan);
//        	}else{
//        		String town = vwCofinTownService.getTownDescription(bsl.getTown());
//        		String subdistrict = vwCofinTownService.getSubdistrictDescription(bsl.getSubdistrict());
//        		String district = vwCofinTownService.getDistrictDescription(bsl.getDistrict());
//        		bsl.setTown(town);
//        		bsl.setSubdistrict(subdistrict);
//        		bsl.setDistrict(district);
//        		pesan.setResult(1);
//            	pesan.setData1(new Gson().toJson(bsl));
//            	listPesan.add(pesan);
//        	}
//    	}
//    	response.setContentType("application/json");
//		response.setCharacterEncoding("UTF-8");
//		response.getWriter().write(new Gson().toJson(listPesan));
//    }
//
//    @RequestMapping(value="/detail/product",method={RequestMethod.GET,RequestMethod.POST})
//    public void getProductContract(	HttpServletRequest request,
//    								HttpServletResponse response)throws IOException{
//    	InsertMessage pesan = new InsertMessage();
//    	List<InsertMessage> listPesan = new ArrayList<InsertMessage>();
//    	String contractNumber = request.getParameter("contractNumber");
//    	VwCofinContractDetail det = vwCofinContractDetailService.getDetailContractByContractNumber(contractNumber);
//    	if(det==null){
//    		pesan.setResult(0);
//    		pesan.setMessage("Product Detail With Contract Number "+contractNumber+" Not Found");
//    		listPesan.add(pesan);
//    	}else{
//    		pesan.setResult(1);
//    		pesan.setData1(new Gson().toJson(det));
//    		listPesan.add(pesan);
//    	}
//    	response.setContentType("application/json");
//    	response.setCharacterEncoding("UTF-8");
//    	response.getWriter().write(new Gson().toJson(listPesan));
//    }
//
//    @RequestMapping(value="/detail/bslAllocation",method={RequestMethod.GET,RequestMethod.POST})
//    public void allocBsl(	HttpServletRequest request,
//    						HttpServletResponse response)throws IOException{
//    	InsertMessage pesan = new InsertMessage();
//    	List<InsertMessage> listPesan = new ArrayList<InsertMessage>();
//    	String contractNumber = request.getParameter("contractNumber");
//    	List<JfsScheduleBsl> sch = jfsScheduleBslService.getScheduleBslByContractNumber(contractNumber);
//    	pesan.setResult(1);
//    	pesan.setData1(new Gson().toJson(sch));
//    	listPesan.add(pesan);
//    	response.setContentType("application/json");
//    	response.setCharacterEncoding("UTF-8");
//    	response.getWriter().write(new Gson().toJson(listPesan));
//    }
//
//    @RequestMapping(value="/detail/partnerAllocation",method={RequestMethod.GET,RequestMethod.POST})
//    public void allocationPartner(	HttpServletRequest request,
//    								HttpServletResponse response)throws IOException{
//    	InsertMessage pesan = new InsertMessage();
//    	List<InsertMessage> listPesan = new ArrayList<InsertMessage>();
//    	String contractNumber = request.getParameter("contractNumber");
//    	List<JfsPaymentAllocation> alloc = jfsPaymentAllocationService.getAllocationPartnerByContractNumber(contractNumber);
//    	pesan.setResult(1);
//    	pesan.setData1(new Gson().toJson(alloc));
//    	listPesan.add(pesan);
//    	response.setContentType("application/json");
//    	response.setCharacterEncoding("UTF-8");
//    	response.getWriter().write(new Gson().toJson(listPesan));
//    }
//
//    @SuppressWarnings("unchecked")
//    @RequestMapping(value="/ImportXls",method={RequestMethod.GET,RequestMethod.POST})
//    public void getXls(	HttpServletRequest request,
//    					HttpSession session,
//    					HttpServletResponse response)throws IOException{
//    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//    	List<JfsContract> kontrak = (List<JfsContract>) session.getAttribute("data");
//    	try{
//    		String pathname = "JfsContract"+sdf.format(new Date()).replace(" ","")+".xls";
//    		pathname = pathname.replace(":","");
//    		File file = new File(pathname.replace("-",""));
//    		WorkbookSettings workbookSettings = new WorkbookSettings();
//    		workbookSettings.setLocale(new Locale("en","EN"));
//    		WritableWorkbook writableWorkbook = Workbook.createWorkbook(file,workbookSettings);
//    		writableWorkbook.createSheet("JfsContract",0);
//    		WritableSheet writableSheet = writableWorkbook.getSheet(0);
//    		WritableFont title = new WritableFont(WritableFont.ARIAL,14);
//    		title.setBoldStyle(WritableFont.BOLD);
//    		WritableFont xjudul = new WritableFont(WritableFont.ARIAL,9);
//    		xjudul.setBoldStyle(WritableFont.BOLD);
//    		WritableCellFormat titel = new WritableCellFormat(title);
//    		WritableCellFormat defCell = new WritableCellFormat();
//    		WritableCellFormat headCell = new WritableCellFormat(xjudul);
//    		headCell.setAlignment(Alignment.CENTRE);
//    		headCell.setBorder(Border.ALL,BorderLineStyle.THIN);
//    		headCell.setBackground(Colour.RED);
//    		WritableCellFormat detCell = new WritableCellFormat();
//    		detCell.setAlignment(Alignment.CENTRE);
//    		detCell.setBorder(Border.ALL,BorderLineStyle.THIN);
//    		Label label;
//    		List<String> judul = new ArrayList<String>();
//    		judul.add(" ");
//    		judul.add("No.");
//    		judul.add("Contract Code");
//    		judul.add("JFS Status");
//    		judul.add("Agreement");
//    		judul.add("Reg. Date");
//    		CellView cellView;
//    		WritableSheet[] sheetValue = writableWorkbook.getSheets();
//    		for(int x=0;x<5;x++){
//    			cellView = sheetValue[0].getColumnView(x);
//    			cellView.setAutosize(true);
//    			sheetValue[0].setColumnView(x,cellView);
//    		}
//    		for(int x=0;x<judul.size();x++){
//    			if(judul.get(x).equals(" ")){
//    				label = new Label(x,4,judul.get(x),defCell);
//    			}else{
//    				label = new Label(x,4,judul.get(x),headCell);
//    			}
//    			writableSheet.addCell(label);
//    		}
//    		Label xtitle = new Label(4,1,"JFS Contract",titel);
//    		writableSheet.addCell(xtitle);
//    		Label tanggal = new Label(4,2,sdf.format(new Date()),defCell);
//    		writableSheet.addCell(tanggal);
//    		int x=4;
//    		for(int s=0;s<kontrak.size();s++){
//    			x=x+1;
//    			try{
//    				label = new Label(0,x,"    ",defCell);
//    				writableSheet.addCell(label);
//    			}catch(Exception j){
//    				label = new Label(0,x,j.toString(),defCell);
//    				writableSheet.addCell(label);
//    			}
//    			try{
//    				label = new Label(1,x,String.valueOf(s+1),detCell);
//    				writableSheet.addCell(label);
//    			}catch(Exception j){
//    				label = new Label(1,x,j.toString(),detCell);
//    				writableSheet.addCell(label);
//    			}
//    			try{
//    				label = new Label(2,x,kontrak.get(s).getTextContractNumber(),detCell);
//    				writableSheet.addCell(label);
//    			}catch(Exception j){
//    				label = new Label(2,x,j.toString(),detCell);
//    				writableSheet.addCell(label);
//    			}
//    			try{
//    				label = new Label(3,x,kontrak.get(s).getStatus(),detCell);
//    				writableSheet.addCell(label);
//    			}catch(Exception j){
//    				label = new Label(3,x,j.toString(),detCell);
//    				writableSheet.addCell(label);
//    			}
//    			try{
//    				label = new Label(4,x,kontrak.get(s).getIdAgreement(),detCell);
//    				writableSheet.addCell(label);
//    			}catch(Exception j){
//    				label = new Label(4,x,j.toString(),detCell);
//    				writableSheet.addCell(label);
//    			}
//    			try{
//    				label = new Label(5,x,kontrak.get(s).getExportDate(),detCell);
//    				writableSheet.addCell(label);
//    			}catch(Exception j){
//    				label = new Label(5,x,j.toString(),detCell);
//    				writableSheet.addCell(label);
//    			}
//    		}
//    		writableSheet.getSettings().setVerticalFreeze(5);
//    		writableWorkbook.write();
//    		writableWorkbook.close();
//    		response.setContentType("application/octet-stream");
//    		response.setHeader("Content-Disposition","attachment;filename="+file.getName());
//    		FileInputStream input = new FileInputStream(file);
//    		ServletOutputStream output = response.getOutputStream();
//    		byte[] xbyte = new byte[4096];
//    		while(input.read(xbyte,0,4096) != -1){
//    			output.write(xbyte,0,4096);
//    		}
//    		input.close();
//    		output.flush();
//    		output.close();
//    		file.delete();
//    	}catch(Exception g){
//    		log.info(g.toString());
//    	}
//    }

}
