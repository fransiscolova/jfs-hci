package id.co.homecredit.jfs.cofin.ui.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.spi.BreakIteratorProvider;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;


import id.co.homecredit.jfs.cofin.common.model.IdCreate;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.Agreement;
import id.co.homecredit.jfs.cofin.core.model.Contract;
import id.co.homecredit.jfs.cofin.core.model.FileUpload;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.temp.TempJfsConfirmationFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmFile;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinConfirmProcess;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessCategory;
import id.co.homecredit.jfs.cofin.core.model.upload.CofinProcessType;
import id.co.homecredit.jfs.cofin.core.service.CofinConfigService;
import id.co.homecredit.jfs.cofin.core.service.FileUploadService;
import id.co.homecredit.jfs.cofin.core.service.PartnerService;
import id.co.homecredit.jfs.cofin.core.service.TempJfsConfirmationFileService;
import id.co.homecredit.jfs.cofin.core.service.batch.BtpnBatchService;
import id.co.homecredit.jfs.cofin.core.service.impl.FileUploadServiceImpl;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinConfirmFileService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinConfirmProcessService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinProcessCategoryService;
import id.co.homecredit.jfs.cofin.core.service.upload.CofinProcessTypeService;
import id.co.homecredit.jfs.cofin.core.service.upload.UploadService;
import id.co.homecredit.jfs.cofin.ui.model.FileModel;
import id.co.homecredit.jfs.cofin.ui.model.HeaderPermata;

@Controller
@RequestMapping(value = "/upload")
public class FileUploadController extends IdCreate<Serializable> {
	private static final Logger log = LogManager.getLogger(FileUploadController.class);

	@Autowired
	PartnerService partnerService;

	@Autowired
	CofinConfigService cofinConfigService;

	@Autowired
	UploadService uploadService;

	@Autowired
	FileUploadService fileUploadService;

	@Autowired
	CofinConfirmProcessService cofinConfirmProcessService;

	@Autowired
	CofinProcessCategoryService cofinProcessCategoryService;

	@Autowired
	CofinProcessTypeService cofinProcessTypeService;

	@Autowired
	CofinConfirmFileService cofinConfirmFileService;

	@RequestMapping(value = "/file", method = RequestMethod.GET)
	public ModelAndView fileUploadPage() {
		FileModel file = new FileModel();
		ModelAndView modelAndView = new ModelAndView("uploadFile", "command", file);
		modelAndView.addObject("partners", partnerService.getAllPartners(false));
		modelAndView.addObject("processCategory", cofinProcessCategoryService.getAll());
		modelAndView.addObject("configs", cofinConfigService.getByCategoryKey1("PROCESS_TYPE", "BANK_PARTNER"));
		modelAndView.addObject("fileUpload", fileUploadService.getFileUpload());

		return modelAndView;
	}

	@RequestMapping(value = "/getProcessType", method = RequestMethod.POST)
	public @ResponseBody String getProcessType(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException {
		JSONObject json = new JSONObject(js);

		log.info(json.getString("categoryId"));
		log.info(json.getString("partnerId"));

		List<CofinProcessType> cofinProcessType = cofinProcessTypeService.getByCategoryId(json.getString("categoryId"));

		log.info(cofinProcessType.size());
		JSONObject jsonResponse = new JSONObject(cofinProcessType);

		JSONArray JsonArray = new JSONArray();
		for (int i = 0; i < cofinProcessType.size(); i++) {

			CofinProcessType cofinProcessTypes = cofinProcessType.get(i);

			List<CofinConfirmProcess> confinConfirmProcess = cofinConfirmProcessService
					.getProcessByPartnerAndIdType(json.getString("partnerId"), cofinProcessTypes.getId());
			log.info(confinConfirmProcess.size());
			for (int y = 0; y < confinConfirmProcess.size(); y++) {

				CofinConfirmProcess res = confinConfirmProcess.get(y);
				JSONObject jsData = new JSONObject();
				jsData.put("id", res.getId());
				jsData.put("processCode", res.getProcessCode());
				jsData.put("name", res.getName());
				JsonArray.put(jsData);

			}

		}

		return JsonArray.toString();
	}

	@RequestMapping(value = "/getProcess", method = RequestMethod.POST)
	public @ResponseBody String getProcess(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException {
		JSONObject json = new JSONObject(js);
		List<CofinConfirmProcess> confinConfirmProcess = cofinConfirmProcessService
				.getProcessByPartner(json.getString("partnerId"));

		log.info(confinConfirmProcess.size());

		JSONObject jsonResponse = new JSONObject(confinConfirmProcess);

		JSONArray JsonArray = new JSONArray();
		for (int i = 0; i < confinConfirmProcess.size(); i++) {
			CofinConfirmProcess res = confinConfirmProcess.get(i);
			JSONObject jsData = new JSONObject();
			jsData.put("id", res.getId());
			jsData.put("processCode", res.getProcessCode());
			jsData.put("name", res.getName());
			JsonArray.put(jsData);
		}

		return JsonArray.toString();
	}

	@RequestMapping(value = "/getFile", method = RequestMethod.POST)
	public @ResponseBody String getFile(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException {
		JSONObject json = new JSONObject(js);
		List<CofinConfirmFile> confinConfirmFile = cofinConfirmFileService.getFile(json.getString("processId"));
		JSONArray JsonArray = new JSONArray();
		for (int i = 0; i < confinConfirmFile.size(); i++) {
			CofinConfirmFile res = confinConfirmFile.get(i);
			JSONObject jsData = new JSONObject();
			jsData.put("id", res.getId());
			jsData.put("fileDesc", res.getFileDesc());
			jsData.put("title", res.getFileName());
			JsonArray.put(jsData);
		}
		return JsonArray.toString();
	}

	@RequestMapping(value = "/checkDate", method = RequestMethod.POST)
	public @ResponseBody String checkFileUpload(@RequestBody(required = false) String js, HttpServletResponse response)
			throws JSONException {
		JSONObject json = new JSONObject(js);

		List<FileUpload> fileUpload = fileUploadService.getFileUpload(json.getString("processDate"),
				json.getString("confirmFileId"));
		JSONArray JsonArray = new JSONArray();
		for (int i = 0; i < fileUpload.size(); i++) {
			FileUpload res = fileUpload.get(i);
			JSONObject jsData = new JSONObject();
			jsData.put("id", res.getId());
			jsData.put("createdDate", res.getCreatedDate());
			jsData.put("createdBy", res.getCreatedBy());
			JsonArray.put(jsData);
		}
		return JsonArray.toString();
	}

	// Handling file upload request
	@PostMapping("/fileUpload")
	public ResponseEntity<Object> fileUpload(@Validated FileModel file, BindingResult result) throws IOException {

		log.info(file.getProcessDate());

		System.out.println("Fetching file");
		MultipartFile multipartFile = file.getFile();
		String uploadPath = "/cofin/data/jfs/";
		String backupPath = uploadPath + "procced/";
		String response = "File Already Upload";
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
		String user = file.getUser();
		String validationString = "";
		log.info("initial process");
		// Save file on system
		if (!file.getFile().getOriginalFilename().isEmpty()) {
			BufferedOutputStream outputStream = new BufferedOutputStream(
					new FileOutputStream(new File(uploadPath, file.getFile().getOriginalFilename())));
			outputStream.write(file.getFile().getBytes());
			outputStream.flush();
			outputStream.close();

			File originaldFile = new File(uploadPath + file.getFile().getOriginalFilename());
			log.info("start process");

			try {
				FileCopyUtils.copy(file.getFile().getBytes(), originaldFile);

			} catch (Exception e) {
				// TODO: handle exception
				log.info("Erorr copy  file " + e.toString());
				e.printStackTrace();
			}

			String fileName = multipartFile.getOriginalFilename();

			CofinConfirmFile confirmFile = cofinConfirmFileService.getById(file.getConfirmationFile());
			Partner partner = partnerService.getPartnerById(file.getPartnerId());
			CofinConfirmProcess cofinConfirmProcess = cofinConfirmProcessService.getById(file.getProcessType());

			FileUpload fileUpload = new FileUpload();
			fileUpload.setPartner(partner);
			fileUpload.setFileName(fileName);
			fileUpload.setMemo(file.getMemo());
			fileUpload.setCreatedBy(user);
			fileUpload.setFilePath(uploadPath + "/" + file.getFile().getOriginalFilename());
			fileUpload.setDescription("");
			fileUpload.setProcessType(cofinConfirmProcess.getName() + "_" + file.getPeriod());
			fileUpload.setStatus("On Progress");
			fileUpload.setTotalRow(BigDecimal.ZERO);
			fileUpload.setCofinConfirmFile(confirmFile);

			try {
				fileUpload.setProcessDate(DateUtil.stringToDateWithFormat(file.getProcessDate(), "MM/dd/yyyy"));

			} catch (Exception e) {
				// TODO: handle exception
				log.info("Erorr parsing date " + e.toString());
				e.printStackTrace();
			}

			fileUploadService.saveFileUpload(fileUpload);
			String fileUploadId = fileUpload.getId();

			try {
				validationString = uploadService.checkFile(uploadPath + fileName, fileUpload,
						file.getConfirmationFile());
			} catch (Exception e) {
				// TODO: handle exception
				validationString = "Please check the file ";
				return new ResponseEntity<>(validationString, HttpStatus.BAD_REQUEST);
			}

			if (validationString.equalsIgnoreCase("success")) {

				new Thread(() -> {

					if (result.hasErrors()) {
						System.out.println("validation errors");
					} else {
						// save info if it exist
						if (originaldFile.exists()) {
							log.info("next process");
							Runnable runnable = new Runnable() {
								@Override
								public void run() {
									try {

										BigDecimal totalRow = uploadService.convertorFile(uploadPath + fileName,
												fileUpload, file.getConfirmationFile());

										if (totalRow.intValue() > 0) {
											File backupFile = new File(
													backupPath
															+ file.getFile().getOriginalFilename().substring(0,
																	file.getFile().getOriginalFilename()
																			.lastIndexOf("."))
															+ "-" + fileUploadId + ".txt");
											FileUtils.copyFile(originaldFile, backupFile);
											FileUpload fileUpload = fileUploadService.getFileUpload(fileUploadId);
											fileUpload.setBackupFileName(backupFile.getPath().toString());
											fileUpload.setStatus("Done");
											fileUpload.setTotalRow(totalRow);
											fileUploadService.saveFileUpload(fileUpload);

											if (!confirmFile.getProcedureName().equalsIgnoreCase("NA")) {

												try {
													cofinProcessTypeService
															.executeProcedure("CALL " + confirmFile.getProcedureName()
																	+ "('" + fileUpload.getId() + "')");

												} catch (Exception e) {
													log.info("Erorrr " + e.toString());
													e.printStackTrace();
												}

											}

										} else {
											FileUpload fileUpload = fileUploadService.getFileUpload(fileUploadId);
											fileUpload.setStatus("Failed");
											fileUploadService.saveFileUpload(fileUpload);
											cofinProcessTypeService.runQuery(
													"delete from cofin_file_upload where id='" + fileUploadId + "'");

										}

									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							};

							Thread thread = new Thread(runnable);
							thread.start();

						}
					}

				}).start();

			} else {

				cofinProcessTypeService.runQuery("delete from cofin_file_upload where id='" + fileUploadId + "'");
				return new ResponseEntity<>(validationString + " ", HttpStatus.BAD_REQUEST);

			}

		} else {
			return new ResponseEntity<>("Invalid file. ", HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(
				"File Uploaded Successfully and data is being processed in the background but you can still upload other file ",
				HttpStatus.OK);
	}



}
