package id.co.homecredit.jfs.cofin.ui.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import id.co.homecredit.jfs.cofin.core.model.LoginLog;
import id.co.homecredit.jfs.cofin.core.service.LoginLogService;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;
import id.co.homecredit.jfs.cofin.core.service.util.CofinPropertyUtil;

import java.io.BufferedReader;

import java.io.IOException;

import java.io.InputStreamReader;

import java.util.Hashtable;

import javax.jms.*;

import javax.naming.Context;

import javax.naming.InitialContext;

import javax.naming.NamingException;

/**
 * Controller for handling login-related request.
 *
 * @author muhammad.muflihun
 *
 */
@Controller
@RequestMapping(value = "jms/")
public class JMSController {
	private static final Logger log = LogManager.getLogger(JMSController.class);

	@Autowired
	private LoginLogService loginLogService;
	@Autowired
	private HttpServletRequest httpServletRequest;

	public final static String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";

	public final static String JMS_FACTORY = "JMSConnectionFactory";

	public final static String QUEUE = "JMSCofinQueue";

	private QueueConnectionFactory qconFactory;

	private QueueConnection qcon;

	private QueueSession qsession;

	private QueueSender qsender;

	private Queue queue;

	private TextMessage msg;

	@RequestMapping(value = "sendMessage", method = RequestMethod.GET)
	public String sendMessage() {
		log.info("Success login");
		// loginLogService.saveUserLogin(UserLoginUtil.getUsername());

		String messg = "t3://wls01-idcofin1.id.nonprod:15001";
		try {
			InitialContext ic = getInitialContext(messg);

			init(ic, QUEUE);
			String msg="Hai broo from spring";
			send(msg);
			log.info("send message " + msg);
			close();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.toString());
		}
		return "Message already sent";
	}

	// private static void readAndSend(JMSController qs) throws IOException,
	// JMSException
	//
	// {
	//
	// BufferedReader msgStream = new BufferedReader(new
	// InputStreamReader(System.in));
	//
	// String line=null;
	//
	// boolean quitNow = false;
	//
	// do {
	//
	// System.out.print("Enter message (\"quit\" to quit): \n");
	//
	// // line = msgStream.readLine();
	//
	// // if (line != null && line.trim().length() != 0) {
	//
	// qs.send("Hai broooo");
	//
	// System.out.println("JMS Message Sent: "+line+"\n");
	//
	// quitNow = line.equalsIgnoreCase("quit");
	//
	// //}
	//
	// } while (! quitNow);
	//
	// }

	private static InitialContext getInitialContext(String url)

			throws NamingException

	{

		Hashtable<String, String> env = new Hashtable<String, String>();

		env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);

		env.put(Context.PROVIDER_URL, url);
		env.put(Context.SECURITY_PRINCIPAL, "weblogic");
		env.put(Context.SECURITY_CREDENTIALS, "Big9dromAc");
		
		
		return new InitialContext(env);

	}

	public void init(Context ctx, String queueName)

			throws NamingException, JMSException

	{

		qconFactory = (QueueConnectionFactory) ctx.lookup(JMS_FACTORY);

		qcon = qconFactory.createQueueConnection();

		qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

		queue = (Queue) ctx.lookup(queueName);

		qsender = qsession.createSender(queue);

		msg = qsession.createTextMessage();

		qcon.start();

	}

	public void send(String message) throws JMSException {

		msg.setText(message);

		qsender.send(msg);

	}

	public void close() throws JMSException {

		qsender.close();

		qsession.close();

		qcon.close();

	}

}
