package id.co.homecredit.jfs.cofin.ui.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import id.co.homecredit.jfs.cofin.core.model.LoginLog;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;
import id.co.homecredit.jfs.cofin.core.service.LoginLogService;
import id.co.homecredit.jfs.cofin.core.service.RoleMappingService;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;
import id.co.homecredit.jfs.cofin.core.service.util.CofinPropertyUtil;

/**
 * Controller for handling login-related request.
 *
 * @author muhammad.muflihun
 *
 */
@Controller
@RequestMapping(value = "/")
public class LoginController {
    private static final Logger log = LogManager.getLogger(LoginController.class);

    @Autowired
    private LoginLogService loginLogService;
    @Autowired
    private HttpServletRequest httpServletRequest;
    @Autowired
    private RoleMappingService roleMappingService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView redirectToHome() {
//    	List<RoleMapping> roleMappings = roleMappingService.getAllValidRoleMappingsByUsername(UserLoginUtil.getUsername(),new Date());
//    	if(roleMappings.size()==0){
//    		log.info("Failed Login Because Username {} Is Expired {}",UserLoginUtil.getUsername());
//    		return new ModelAndView("redirect:login/expire");
//    	}else{
//    		log.info("Success Login");
//            return new ModelAndView("redirect:home");
//    	}
        return new ModelAndView("redirect:home");
    }
    
    @RequestMapping(value = "login/expire", method = RequestMethod.GET)
    public ModelAndView redirectToLogin() {
    	ModelAndView mav = new ModelAndView("login");
    	List<RoleMapping> map = roleMappingService.getAllValidRoleMappingsByUsername(UserLoginUtil.getUsername(),new Date());
    	if(map.size()==0){
    		mav.addObject("error","Your Username Was Expired, Please Contact Administrator.");
    	}
		return mav;
    }

    @RequestMapping(value = "savelogout", method = RequestMethod.GET)
    public ModelAndView saveBeforeLogout() {
        log.info("save logout info");
        log.info("user ip address {}", httpServletRequest.getRemoteAddr());
        loginLogService.saveUserLogout(UserLoginUtil.getUsername());
        return new ModelAndView("redirect:logout");
    }

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public ModelAndView showHomePage() {
        log.info("showing home page");
//        String username = UserLoginUtil.getUsername();
//        LoginLog loginLog = loginLogService.getLatestLoginLogByUsername(username);
//        log.info("user {} latest login {}", username, loginLog.getCreatedDate());
        return new ModelAndView("home");
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView showLoginPage(	@RequestParam(value = "error", required = false) String error,
            							@RequestParam(value = "logout", required = false) String logout){
        log.info("showing login page");
        ModelAndView mav = new ModelAndView("login");
        if(error != null){
            log.info("error {}", error);
            mav.addObject("error", CofinPropertyUtil.get("login.error"));
        }
        if(logout != null){
            log.info("logout success {}", logout);
            mav.addObject("error", CofinPropertyUtil.get("logout.success"));
        }
        return mav;
    }

}
