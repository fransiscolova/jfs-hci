package id.co.homecredit.jfs.cofin.ui.controller;

import java.awt.Menu;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.RoleAction;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.core.service.RoleActionService;
import id.co.homecredit.jfs.cofin.core.service.RoleMappingService;
import id.co.homecredit.jfs.cofin.core.service.RoleService;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;

/**
 * {@link Menu} controller for implementing Spring MVC.
 *
 * @author fransisco.situmorang
 *
 */
@Controller
@RequestMapping(value = "/menu")
public class MenuController {
    private static final Logger log = LogManager.getLogger(MenuController.class);

    @Autowired
    private ActionService actionService;

    @Autowired
    RoleService roleService;

    @Autowired
    private RoleActionService roleActionService;

    @Autowired
    RoleMappingService roleMappingService;

    @RequestMapping(value = "/roleAction/activation", method = RequestMethod.POST)
    public @ResponseBody String activationAgreement(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException {
        JSONObject json = new JSONObject(js);
        RoleAction roleAction = roleActionService.getRoleActionById(json.getString("id"));
        if (roleAction.getDeleted().equals(YesNoEnum.Y)) {
            roleAction.setDeleted(YesNoEnum.N);
        } else {
            roleAction.setDeleted(YesNoEnum.Y);
        }
        roleAction.setUpdatedBy(UserLoginUtil.getUsername());
        roleAction.setUpdatedDate(new Date());
        roleActionService.saveRoleAction(roleAction);
        return js;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView activationRole(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("menuManagement");
        List<Action> actions = actionService.getAllAction(true);
        mav.addObject("actions", actions);
        return mav;
    }

    @RequestMapping(value = "/getAllAction", method = RequestMethod.POST)
    public @ResponseBody String getAllAction(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        JSONObject json = new JSONObject(js);
        RoleEnum roleEnum = RoleEnum.valueOf(json.getString("role"));
        log.debug("role emun {}", roleEnum);
        List<RoleAction> actions = roleActionService.getAllByRole(roleEnum);
        JSONArray JsonArray = new JSONArray();
        for (int i = 0; i < actions.size(); i++) {
            Action action = actions.get(i).getAction();
          //  log.debug("Data {}", action.getAction());
            JSONObject jsonData = new JSONObject();
            jsonData.put("action", action.getAction());
            jsonData.put("name", action.getName());
            JsonArray.put(jsonData);
        }
        return JsonArray.toString();
    }

    @RequestMapping(value = "/save/roleAction", method = RequestMethod.POST)
    public @ResponseBody String saveRoleAction(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        JSONObject json = new JSONObject(js);
        if (json.getString("typeProcess").equalsIgnoreCase("save")) {
            RoleAction roleAction = new RoleAction();
            roleAction.setCreatedBy(UserLoginUtil.getUsername());
            Action action = actionService.getByName(json.getString("name"));
         //   log.info("PARAM " + json.getString("name"));
         //   log.info("ACTION " + action.getName());
            
            roleAction.setAction(action);
            Role role = roleService.getRolesByRole(RoleEnum.valueOf(json.getString("role")));
           // log.info("PARAM " + json.getString("role"));
          //  log.info("ROLE " + role.getRole());
            
            
            
            roleAction.setRole(role);
            roleAction.setValidFrom(DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
            roleAction.setValidTo(DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
            roleActionService.saveRoleAction(roleAction);
        } else {
            RoleAction roleAction = roleActionService.getRoleActionById(json.getString("id"));
            roleAction.setCreatedBy(UserLoginUtil.getUsername());
            Action action = actionService.getByName(json.getString("name"));
            roleAction.setAction(action);
            Role role = roleService.getRolesByRole(RoleEnum.valueOf(json.getString("role")));
            roleAction.setRole(role);
            roleAction.setValidFrom(DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
            roleAction.setValidTo(DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
            roleActionService.saveRoleAction(roleAction);
        }
        return js;
    }
    
//    @RequestMapping(value = "/save/roleAction", method = RequestMethod.POST)
//    public @ResponseBody String saveRoleAction(	@RequestBody(required = false) String js,
//    											HttpServletResponse response)throws JSONException,ParseException{
//    	JSONObject xjson = new JSONObject(js);
//    	String blart = xjson.getString("typeProcess");
//    	RoleAction xrole = null;
//    	String usnam = UserLoginUtil.getUsername();
//    	List<Action> act = actionService.getActionLikeName(xjson.getString("name"));
//		log.info(act.size());
//		Role vrole = roleService.getRolesByRole(RoleEnum.valueOf(xjson.getString("role")));
//		log.info(vrole);
//    	if(blart.equals("Save")){
//    		xrole = new RoleAction();
//    		xrole.setCreatedBy(usnam);
//    		log.info("Created By = "+usnam);
//    		xrole.setValidFrom(DateUtil.stringToDateWithFormat(xjson.getString("validFrom"),"MM/dd/yyyy"));
//    		log.info("Valid From = "+DateUtil.stringToDateWithFormat(xjson.getString("validFrom"),"MM/dd/yyyy"));
//    		xrole.setValidTo(DateUtil.stringToDateWithFormat(xjson.getString("validTo"),"MM/dd/yyyy"));
//    		log.info("Valid To = "+DateUtil.stringToDateWithFormat(xjson.getString("validTo"),"MM/dd/yyyy"));
//    		xrole.setRole(vrole);
//    		log.info("Role = "+vrole);
//    		xrole.setAction(act.get(0));
//    		log.info("Action = "+act.get(0));
//    		roleActionService.saveRoleAction(xrole);
//    	}else{
//    		xrole = roleActionService.getRoleActionById(xjson.getString("id"));
//    		xrole.setAction(act.get(0));
//    		xrole.setCreatedBy(usnam);
//    		xrole.setRole(vrole);
//    		xrole.setValidFrom(DateUtil.stringToDateWithFormat(xjson.getString("validFrom"),"MM/dd/yyyy"));
//    		xrole.setValidTo(DateUtil.stringToDateWithFormat(xjson.getString("validTo"),"MM/dd/yyyy"));
//    		roleActionService.saveRoleAction(xrole);
//    	}
//    	return js;
//    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody String saveUser(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        JSONObject json = new JSONObject(js);
        Action action = new Action();
        action.setCreatedBy(UserLoginUtil.getUsername());
        action.setAction(json.getString("action"));
        action.setName(json.getString("name"));
        actionService.saveAction(action);
        return js;
    }

    @RequestMapping(value = "/{name:.+}", method = RequestMethod.GET)
    public ModelAndView view(@PathVariable String name) throws Exception {
        Action action = actionService.getByName(name);
        log.debug("menu");
        List<RoleAction> roleActions = roleActionService.getRoleActionsByAction(action.getAction());
        List<Enum> list = new ArrayList<>();
        for (RoleAction roleAction : roleActions) {
            list.add(roleAction.getRole().getRole());
        }
        List<Role> role = roleService.getAllUnsedRole(list);
        List<Role> xrole = roleService.getListRoles(list);
        for(int x=0;x<xrole.size();x++){
        	Integer cari = 0;
        	for(int c=0;c<role.size();c++){
        		if(role.get(c).getRole().equals(xrole.get(x).getRole())){
        			cari = cari + 1;
        		}
        	}
        	if(cari == 0){
        		Role zrole = new Role();
            	zrole.setRole(xrole.get(x).getRole());
            	zrole.setDescription(xrole.get(x).getDescription());
            	role.add(zrole);
        	}else{
        		cari = 0;
        	}
        }
        ModelAndView model = new ModelAndView("menuManagementDetail");
        model.addObject("usedRole", list);
        model.addObject("roleUser", role);
        model.addObject("action", action);
        model.addObject("roleActions", roleActions);
        return model;
    }

}
