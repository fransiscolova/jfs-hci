package id.co.homecredit.jfs.cofin.ui.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.dao.ActivityLogDao;
import id.co.homecredit.jfs.cofin.core.model.AuditLog;
import id.co.homecredit.jfs.cofin.core.model.JfsPartner;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.model.enumeration.ActivityEnum;
import id.co.homecredit.jfs.cofin.core.service.JfsPartnerService;
import id.co.homecredit.jfs.cofin.core.service.PartnerService;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;

/**
 * {@link Partner} controller for implementing Spring MVC.
 *
 * @author Fransisco.situmorang
 *
 */

//@Controller
//@RequestMapping(value = "/partner")
//public class PartnerController {
//    private static final Logger log = LogManager.getLogger(PartnerController.class);
//
//    @Autowired
//    private PartnerService partnerService;
//
//    @RequestMapping(value = "/activation", method = RequestMethod.POST)
//    public @ResponseBody String activationPartner(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException {
//        JSONObject json = new JSONObject(js);
//
//        Partner partners = partnerService.getPartnerById(json.getString("id"));
//        if (partners.getDeleted().equals(YesNoEnum.Y)) {
//            partners.setDeleted(YesNoEnum.N);
//        } else {
//            partners.setDeleted(YesNoEnum.Y);
//        }
//        partners.setUpdatedBy(UserLoginUtil.getUsername());
//        partners.setUpdatedDate(new Date());
//        partnerService.savePartner(partners);
//
//        return js;
//    }
//
//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public ModelAndView allPartner() throws Exception {
//        ModelAndView model = new ModelAndView();
//        List<Partner> xpart = partnerService.getAllPartners(true);
//        Integer prio = 0;
//        if(xpart.size()==0){
//        	prio = 1;
//        }else{
//        	List<Integer> zpart = new ArrayList<Integer>();
//        	for(int x=0;x<xpart.size();x++){
//        		zpart.add(xpart.get(x).getPriority());
//        		log.info(x+" "+xpart.get(x).getPriority());
//        	}
//        	Collections.sort(zpart, Collections.reverseOrder());
//        	prio = zpart.get(0) + 1;
//        }
//        model.addObject("prio", prio);
//        model.setViewName("partner");
//        model.addObject("partners", xpart);
//        return model;
//    }
//
//    @RequestMapping(value = "/save", method = RequestMethod.POST)
//    public @ResponseBody String savePartner(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException {
//        JSONObject json = new JSONObject(js);
//        Partner partners = new Partner();
//        partners.setName(json.getString("name"));
//        partners.setCode(json.getString("code"));
//        partners.setAddress(json.getString("address"));
//        partners.setContactName(json.getString("contactName"));
//        partners.setContactPhone(json.getString("contactPhone"));
//        partners.setContactEmail(json.getString("contactEmail"));
//        partners.setCreatedBy(UserLoginUtil.getUsername());
//        partners.setPriority(Integer.parseInt(json.getString("priority")));
//        if(json.getString("isEligibilityChecked").equals("N")){
//        	partners.setIsEligibilityChecked(YesNoEnum.N);
//        }else{
//        	partners.setIsEligibilityChecked(YesNoEnum.Y);
//        }
//        partnerService.savePartner(partners);
//        return js;
//    }
//
//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public @ResponseBody String updatePartner(@RequestBody(required = false) String js,
//            HttpServletResponse response) throws JSONException {
//        JSONObject json = new JSONObject(js);
//        Partner partners = partnerService.getPartnerById(json.getString("id"));
//        partners.setName(json.getString("name"));
//        partners.setCode(json.getString("code"));
//        partners.setAddress(json.getString("address"));
//        partners.setContactName(json.getString("contactName"));
//        partners.setContactPhone(json.getString("contactPhone"));
//        partners.setContactEmail(json.getString("contactEmail"));
//        partners.setUpdatedBy(UserLoginUtil.getUsername());
//        partners.setUpdatedDate(new Date());
//        partners.setPriority(Integer.parseInt(json.getString("priority")));
//        if(json.getString("isEligibilityChecked").equals("N")){
//        	partners.setIsEligibilityChecked(YesNoEnum.N);
//        }else{
//        	partners.setIsEligibilityChecked(YesNoEnum.Y);
//        }
//        partnerService.savePartner(partners);
//        return js;
//    }
//
//}

@Controller
@RequestMapping(value="/partner")
public class PartnerController{
	private static final Logger log = LogManager.getLogger(PartnerController.class);
	
	@Autowired
	private JfsPartnerService jfsPartnerService;
	
	@Autowired ActivityLogDao activityLogDao;
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public ModelAndView allPartner()throws Exception{
		ModelAndView model = new ModelAndView();
		List<JfsPartner> xpart = jfsPartnerService.getAllPartner();
		Long prio = Long.parseLong("0");
		if(xpart.size()==0){
			prio = Long.parseLong("1");
		}else{
			List<Long> zpart = new ArrayList<Long>();
			for(int x=0;x<xpart.size();x++){
				zpart.add(xpart.get(x).getPriority());
			}
			Collections.sort(zpart,Collections.reverseOrder());
			prio = zpart.get(0)+Long.parseLong("1");
		}
		model.addObject("prio",prio);
		model.addObject("partners",xpart);
		model.setViewName("partner");
		return model;
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public @ResponseBody String savePartner(@RequestBody(required=false)String js,
											HttpServletResponse response)throws JSONException{
		JSONObject json = new JSONObject(js);
		JfsPartner part = new JfsPartner();
		part.setName(json.getString("name"));
		part.setAddress(json.getString("address"));
		part.setMainContact(json.getString("contactName"));
		part.setPhoneNumber(json.getString("contactPhone"));
		part.setEmail(json.getString("contactEmail"));
		part.setIsDelete("N");
		part.setPriority(Long.parseLong(json.getString("priority")));
		if(json.getString("isEligibilityChecked").equals("N")){
			part.setIsCheckingEligibility("N");
		}else{
			part.setIsCheckingEligibility("Y");
		}
		jfsPartnerService.partnerSave(part);
		String query = "update JFS_PARTNER set IS_DELETE = 'N', IS_CHECKING_ELIGIBILITY = '"+json.getString("isEligibilityChecked")+"' where IS_DELETE = '1'";
		jfsPartnerService.updateValueOfJfsPartnerWithQuery(query);
		return js;
	}
	
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public @ResponseBody String updatePartner(	@RequestBody(required=false)String js,
												HttpServletResponse response)throws JSONException{
		JSONObject json = new JSONObject(js);
		JfsPartner part = jfsPartnerService.getPartnerValueById(json.getString("id"));
		String jsonBefore = new Gson().toJson(part);
		part.setName(json.getString("name"));
		part.setAddress(json.getString("address"));
		part.setMainContact(json.getString("contactName"));
		part.setPhoneNumber(json.getString("contactPhone"));
		part.setEmail(json.getString("contactEmail"));
		part.setPriority(Long.parseLong(json.getString("priority")));
		part.setIsCheckingEligibility(json.getString("isEligibilityChecked"));
		jfsPartnerService.partnerUpdateWithQuery(part);
		String jsonAfter = new Gson().toJson(part);
		AuditLog logger = new AuditLog();
		logger.setCreatedBy(UserLoginUtil.getUsername());
		logger.setActivity(ActivityEnum.UPDATE);
		logger.setFullClassName("id.co.homecredit.jfs.cofin.core.model.JfsPartner");
		logger.setDataBefore(jsonBefore);
		logger.setDataAfter(jsonAfter);
		logger = activityLogDao.saveWithNewPropagation(logger);
		return js;
	}
	
	@RequestMapping(value="/activation",method=RequestMethod.POST)
	public @ResponseBody String activationPartner(	@RequestBody(required=false)String js,
													HttpServletResponse response)throws JSONException{
		JSONObject json = new JSONObject(js);
		JfsPartner xpartner = jfsPartnerService.getPartnerValueById(json.getString("id"));
		String jsonBefore = new Gson().toJson(xpartner);
		String deleted = "";
		ActivityEnum act = null;
		if(xpartner.getIsDelete().equals("N")){
			deleted = "Y";
			act = ActivityEnum.SOFT_DELETE;
			xpartner.setIsDelete("Y");
		}else{
			deleted = "N";
			act = ActivityEnum.UNDO_DELETE;
			xpartner.setIsDelete("Y");
		}
		String jsonAfter = new Gson().toJson(xpartner);
		String query = "update JFS_PARTNER set IS_DELETE = '"+deleted+"' where ID = '"+json.getString("id")+"'";
		jfsPartnerService.updateValueOfJfsPartnerWithQuery(query);
		AuditLog logger = new AuditLog();
		logger.setCreatedBy(UserLoginUtil.getUsername());
		logger.setActivity(act);
		logger.setFullClassName("id.co.homecredit.jfs.cofin.core.model.JfsPartner");
		logger.setDataBefore(jsonBefore);
		logger.setDataAfter(jsonAfter);
		logger = activityLogDao.saveWithNewPropagation(logger);
		return js;
	}
	
}