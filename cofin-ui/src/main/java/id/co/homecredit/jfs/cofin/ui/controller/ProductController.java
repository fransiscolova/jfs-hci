package id.co.homecredit.jfs.cofin.ui.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;
import id.co.homecredit.jfs.cofin.core.model.JfsProductHci;
import id.co.homecredit.jfs.cofin.core.model.Partner;
import id.co.homecredit.jfs.cofin.core.service.JfsProductHciService;

/**
 * {@link Partner} controller for implementing Spring MVC.
 *
 * @author Fransisco.situmorang
 *
 */

//@Controller
//@RequestMapping(value = "/product")
//public class ProductController {
//    private static final Logger log = LogManager.getLogger(ProductController.class);
//
//    @Autowired
//    private ProductService productService;
//
//    @RequestMapping(value = "", method = RequestMethod.GET)
//    public ModelAndView allProduct() throws Exception {
//        ModelAndView model = new ModelAndView();
//        model.setViewName("product");
//        model.addObject("product", productService.getAllProductsByIncludeDeleted(true));
//        return model;
//    }
//
//    @RequestMapping(value = "/save", method = RequestMethod.POST)
//    public @ResponseBody String saveProduct(@RequestBody(required = true) String js,
//            HttpServletResponse response) throws JSONException, ParseException {
//        JSONObject json = new JSONObject(js);
//        log.debug("json {}", json.toString());
//        Product product;
//        if (json.getString("type").equalsIgnoreCase("Save")) {
//            product = new Product();
//            product.setCode(json.getString("code"));
//            product.setName(json.getString("name"));
//            product.setCreatedBy(UserLoginUtil.getUsername());
//        } else {
//            product = productService.getProductByCode(json.getString("code"));
//            product.setName(json.getString("name"));
//            product.setUpdatedBy(UserLoginUtil.getUsername());
//        }
//        productService.saveProduct(product);
//        return js;
//    }
//
//}

@Controller
@RequestMapping(value="/product")
public class ProductController{
	private static final Logger log = LogManager.getLogger(ProductController.class);
	
	@Autowired
	protected JfsProductHciService jfsProductHciService;
	
	@RequestMapping(value="",method=RequestMethod.GET)
	public ModelAndView allJfsProduct()throws Exception{
		ModelAndView model = new ModelAndView();
		List<JfsProductHci> xprod = jfsProductHciService.getAllProductHci();
		model.addObject("sizes",xprod.size());
		model.setViewName("product");
		model.addObject("product",xprod);
		return model;
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public @ResponseBody String saveJfsProduct(	@RequestBody(required=true) String js,
												HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		JfsProductHci produk = new JfsProductHci();
		produk.setCodeProduct(json.getString("code"));
		produk.setNameProduct(json.getString("name"));
		produk.setDtimeInserted(new Date());
		jfsProductHciService.saveJfsProductHci(produk);
		return js;
	}
	
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public @ResponseBody String updateJfsProductHci(@RequestBody(required=true) String js,
													HttpServletResponse response)throws JSONException,ParseException{
		JSONObject json = new JSONObject(js);
		JfsProductHci prod = jfsProductHciService.getJfsProductHciById(json.getString("id"));
		prod.setCodeProduct(json.getString("code"));
		prod.setNameProduct(json.getString("name"));
		jfsProductHciService.updateJfsProductHci(prod);
		return js;
	}
	
}
