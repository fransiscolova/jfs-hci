package id.co.homecredit.jfs.cofin.ui.controller;

import java.awt.Menu;
import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.core.dao.ActivityLogDao;
import id.co.homecredit.jfs.cofin.core.model.AuditLog;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.enumeration.ActivityEnum;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;
import id.co.homecredit.jfs.cofin.core.service.RoleMappingService;
import id.co.homecredit.jfs.cofin.core.service.RoleService;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;

/**
 * {@link Menu} Controller for Setting Role Master.
 *
 * @author denny.afrizal01
 *
 */
@Controller
@RequestMapping(value="/role")
public class RoleController {
	private static final Logger log = LogManager.getLogger(RoleController.class);
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	RoleMappingService roleMappingService;
	
	@Autowired
	private ActivityLogDao activityLogDao;
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public ModelAndView roleMapingHome()throws Exception{
		ModelAndView views = new ModelAndView();
		views.setViewName("role");
		List<Role> xrole = roleService.getAllRoles(true);
		views.addObject("xrole",xrole);
		return views;
	}
	
	@RequestMapping(value="/status",method=RequestMethod.POST)
	public @ResponseBody String RoleActivation(	@RequestBody(required=false) String js,
												HttpServletResponse response)throws JSONException,ParseException{
		JSONObject xjson = new JSONObject(js);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Role xrole = roleService.getRoleValueByRole(RoleEnum.valueOf(xjson.getString("role")));
		String jsonBefore = new Gson().toJson(xrole);
		ActivityEnum act = null;
		String status = "";
		if(xjson.getString("status").equals("Reactive")){
			xrole.setDeleted(YesNoEnum.N);
			act = ActivityEnum.UNDO_DELETE;
			status = "N";
		}else{
			xrole.setDeleted(YesNoEnum.Y);
			act = ActivityEnum.SOFT_DELETE;
			status = "Y";
		}
		xrole.setUpdatedBy(UserLoginUtil.getUsername());
		xrole.setUpdatedDate(new Date());
		xrole.setVersion(xrole.getVersion()+1);
		String jsonAfter = new Gson().toJson(xrole);
		log.info(xrole);
		roleService.statusUpdateRole(xjson.getString("role"),status,UserLoginUtil.getUsername(),sdf.format(new Date()),String.valueOf(xrole.getVersion()+1));
		AuditLog audit = new AuditLog();
		audit.setCreatedBy(UserLoginUtil.getUsername());
		audit.setActivity(act);
		audit.setFullClassName("id.co.homecredit.jfs.cofin.core.model.Role");
		audit.setDataBefore(jsonBefore);
		audit.setDataAfter(jsonAfter);
		audit = activityLogDao.saveWithNewPropagation(audit);
		return js;
	}

}
