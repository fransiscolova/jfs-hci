package id.co.homecredit.jfs.cofin.ui.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import id.co.homecredit.jfs.cofin.common.model.enumeration.YesNoEnum;
import id.co.homecredit.jfs.cofin.common.util.DateUtil;
import id.co.homecredit.jfs.cofin.core.model.Role;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;
import id.co.homecredit.jfs.cofin.core.model.User;
import id.co.homecredit.jfs.cofin.core.model.enumeration.RoleEnum;
import id.co.homecredit.jfs.cofin.core.service.RoleMappingService;
import id.co.homecredit.jfs.cofin.core.service.RoleService;
import id.co.homecredit.jfs.cofin.core.service.UserService;
import id.co.homecredit.jfs.cofin.core.service.security.UserLoginUtil;

/**
 * {@link User} controller for implementing Spring MVC.
 *
 * @author muhammad.muflihun
 *
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {
    private static final Logger log = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private RoleMappingService roleMappingService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/role/activation", method = RequestMethod.POST)
    public @ResponseBody String activationRole(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        JSONObject json = new JSONObject(js);
        RoleMapping roleMap = roleMappingService.getRoleMappingById(json.getString("id"));
        if (roleMap.getDeleted().equals(YesNoEnum.Y)) {
            roleMap.setDeleted(YesNoEnum.N);
        } else {
            roleMap.setDeleted(YesNoEnum.Y);
        }

        roleMap.setUpdatedBy(UserLoginUtil.getUsername());
        roleMap.setUpdatedDate(new Date());

        roleMappingService.saveRoleMapping(roleMap);
        return js;
    }

    @RequestMapping(value = "/activation", method = RequestMethod.POST)
    public @ResponseBody String activationUser(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        JSONObject json = new JSONObject(js);
        User user = userService.getUserByUserName(json.getString("username"));
        if (user.getDeleted().equals(YesNoEnum.Y)) {
            user.setDeleted(YesNoEnum.N);
        } else {
            user.setDeleted(YesNoEnum.Y);
        }
        user.setUpdatedBy(UserLoginUtil.getUsername());
        user.setUpdatedDate(new Date());
        userService.saveUser(user);
        return js;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody String saveUser(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        JSONObject json = new JSONObject(js);
        User user = new User();
        user.setCreatedBy(UserLoginUtil.getUsername());
        user.setUsername(json.getString("username"));
        user.setFirstName(json.getString("firstName"));
        user.setLastName(json.getString("lastName"));
        userService.saveUser(user);
        return js;
    }

    @RequestMapping(value = "/role/save", method = RequestMethod.POST)
    public @ResponseBody String saveUserRoles(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        JSONObject json = new JSONObject(js);
        RoleMapping roleMap = null;
        Role role = roleService.getRolesByRole(RoleEnum.valueOf(json.getString("role")));
        User user = userService.getUserByUserName(json.getString("username"));

        if (json.getString("typeProcess").equalsIgnoreCase("save")) {
            roleMap = new RoleMapping();
            roleMap.setUser(user);
            roleMap.setRole(role);
            roleMap.setCreatedBy(UserLoginUtil.getUsername());
            roleMap.setCreatedDate(new Date());
            roleMap.setValidFrom(
                    DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
            roleMap.setValidTo(
                    DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
        } else {
            roleMap = roleMappingService.getRoleMappingById(json.getString("id"));
            roleMap.setRole(role);
            roleMap.setUpdatedBy(UserLoginUtil.getUsername());
            roleMap.setUpdatedDate(new Date());
            roleMap.setValidFrom(
                    DateUtil.stringToDateWithFormat(json.getString("validFrom"), "MM/dd/yyyy"));
            roleMap.setValidTo(
                    DateUtil.stringToDateWithFormat(json.getString("validTo"), "MM/dd/yyyy"));
        }

        roleMappingService.saveRoleMapping(roleMap);
        return js;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public @ResponseBody String updateUser(@RequestBody(required = false) String js,
            HttpServletResponse response) throws JSONException, ParseException {
        JSONObject json = new JSONObject(js);
        User user = userService.getUserByUserName(json.getString("username"));
        user.setCreatedBy(UserLoginUtil.getUsername());
        user.setUsername(json.getString("username"));
        user.setFirstName(json.getString("firstName"));
        user.setLastName(json.getString("lastName"));
        userService.saveUser(user);
        return js;
    }

    @RequestMapping(value = "/role/{username:.+}", method = RequestMethod.GET)
    public ModelAndView view(@PathVariable String username) throws Exception {
        log.info("get username role view" + username);
        ModelAndView model = new ModelAndView();
        model.setViewName("userDetail");
        List<RoleMapping> roleMapping = roleMappingService.getAllRoleMappingByUsername(username);
        List<Enum> list = new ArrayList<>();
        for (RoleMapping roleMap : roleMapping) {
            list.add(roleMap.getRole().getRole());
        }
        List<Role> role = roleService.getAllUnsedRole(list);
        List<Role> xrole = roleService.getListRoles(list);
        for(int x=0;x<xrole.size();x++){
        	Integer cari = 0;
        	for(int c=0;c<role.size();c++){
        		if(role.get(c).getRole().equals(xrole.get(x).getRole())){
        			cari = cari + 1;
        		}
        	}
        	if(cari == 0){
        		Role zrole = new Role();
            	zrole.setRole(xrole.get(x).getRole());
            	zrole.setDescription(xrole.get(x).getDescription());
            	role.add(zrole);
        	}else{
        		cari = 0;
        	}
        }
        model.addObject("usedRole", list);
        model.addObject("roleUser", role);
        model.addObject("users", userService.getUserByUserName(username));
        model.addObject("roles", roleMapping);
        return model;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView viewAllUsers() throws Exception {
        log.info("get user view");
        ModelAndView model = new ModelAndView();
        model.setViewName("user");
        model.addObject("users", userService.getAllUsers(true));
        return model;
    }

}
