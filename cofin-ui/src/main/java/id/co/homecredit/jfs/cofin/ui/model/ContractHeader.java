package id.co.homecredit.jfs.cofin.ui.model;

/**
 * Model For Show Contract Header
 *
 * @author denny.afrizal01
 *
 */
public class ContractHeader {
	private Integer no;
	private String contractNumber;
	private String status;
	private String jfsStatus;
	private String createDate;
	private String agreement;
	private String regDate;
	public Integer getNo() {
		return no;
	}
	public void setNo(Integer no) {
		this.no = no;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJfsStatus() {
		return jfsStatus;
	}
	public void setJfsStatus(String jfsStatus) {
		this.jfsStatus = jfsStatus;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getAgreement() {
		return agreement;
	}
	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
}
