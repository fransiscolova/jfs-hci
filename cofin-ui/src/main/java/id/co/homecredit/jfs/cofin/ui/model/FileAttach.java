package id.co.homecredit.jfs.cofin.ui.model;

public class FileAttach{
	
	private byte[] fileContent;
	private String fileName;
	private String id;
	private String mimeType;
	public byte[] getFileContent(){
		return fileContent;
	}
	public void setFileContent(byte[] fileContent){
		this.fileContent=fileContent;
	}
	public String getFileName(){
		return fileName;
	}
	public void setFileName(String fileName){
		this.fileName=fileName;
	}
	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id=id;
	}
	public String getMimeType(){
		return mimeType;
	}
	public void setMimeType(String mimeType){
		this.mimeType=mimeType;
	}

}
