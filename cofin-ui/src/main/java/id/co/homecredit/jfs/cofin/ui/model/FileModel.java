package id.co.homecredit.jfs.cofin.ui.model;

import org.springframework.web.multipart.MultipartFile;

public class FileModel {


	private MultipartFile file;
	private String partnerId;
	private String processType;
	private String period;
	private String memo;
	private String status;
	private String user;
	private String confirmationFile;
	private String processCategory;
	private String processDate;
	
	
	
	
	public String getProcessDate() {
		return processDate;
	}


	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}


	public String getProcessCategory() {
		return processCategory;
	}

	
	public String getConfirmationFile() {
		return confirmationFile;
	}

	public void setConfirmationFile(String confirmationFile) {
		this.confirmationFile = confirmationFile;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public String getPeriod() {
		return period;
	}

	public String getMemo() {
		return memo;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public String getProcessType() {
		return processType;
	}
	
	public void setProcessCategory(String processCategory) {
		this.processCategory = processCategory;
	}


	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
}