package id.co.homecredit.jfs.cofin.ui.model;

import org.springframework.web.multipart.MultipartFile;

public class HeaderPermata {


	private String tglCetak;
	private String cabangBank;
	private String noSurat;
	private String perTanggal;
	
	
	public String getTglCetak() {
		return tglCetak;
	}
	public String getCabangBank() {
		return cabangBank;
	}
	public String getNoSurat() {
		return noSurat;
	}
	public String getPerTanggal() {
		return perTanggal;
	}
	public void setTglCetak(String tglCetak) {
		this.tglCetak = tglCetak;
	}
	public void setCabangBank(String cabangBank) {
		this.cabangBank = cabangBank;
	}
	public void setNoSurat(String noSurat) {
		this.noSurat = noSurat;
	}
	public void setPerTanggal(String perTanggal) {
		this.perTanggal = perTanggal;
	}
	
	
	
	
	
}