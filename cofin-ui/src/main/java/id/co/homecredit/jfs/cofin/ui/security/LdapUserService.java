package id.co.homecredit.jfs.cofin.ui.security;

import id.co.homecredit.jfs.cofin.core.dao.RoleMappingDao;
import id.co.homecredit.jfs.cofin.core.dao.UserDao;
import id.co.homecredit.jfs.cofin.core.model.RoleMapping;
import id.co.homecredit.jfs.cofin.core.model.User;
import id.co.homecredit.jfs.cofin.core.service.LoginLogService;
import id.co.homecredit.jfs.cofin.core.service.security.UserLogin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.DefaultDirObjectFactory;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Auth user with ldap
 * @author ahmad.sodiqin
 * @version 1.0.0
 */
@Transactional
public class LdapUserService implements AuthenticationProvider {

    private static final Logger LOG = LogManager.getLogger(LdapUserService.class);

    @Autowired
    private RoleMappingDao roleMappingDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private LoginLogService loginLogService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        //initiliaze auth
        Authentication auth = null;

        //get username and password
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        //do ldap
        if(this.springLdap(username+"@homecredit.co.id", password)){
            //save user
            User user = this.getUser(username);

            //get roles
            List<GrantedAuthority> mappedAuthorities = this.getAuthorize(username);

            //set user info to context
            UserLogin userLogin = new UserLogin(username, user.getFirstName(), user.getLastName(), password, true, true,
                    true, true, mappedAuthorities);

            //set user info to auth
            auth = new UsernamePasswordAuthenticationToken(userLogin, password, mappedAuthorities);
        }

        //return auth
        return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    /**
     * Auth with Spring Ldap
     * @param username
     * @param password
     * @return true if auth success
     */
    private boolean springLdap(String username, String password){
        try {
            //prepare ldap
            LdapContextSource sourceLdapCtx = new LdapContextSource();
            sourceLdapCtx.setUrl("ldap://id-vw01.hcg.homecredit.net:3268/");
            sourceLdapCtx.setUserDn(username);
            sourceLdapCtx.setPassword(password);
            sourceLdapCtx.setDirObjectFactory(DefaultDirObjectFactory.class);
            sourceLdapCtx.afterPropertiesSet();
            LdapTemplate sourceLdapTemplate = new LdapTemplate(sourceLdapCtx);

            // Authenticate:
            sourceLdapTemplate.getContextSource().getContext(username, password);

            //user auth successful
            return true;
        } catch (AuthenticationException e) {
            //user auth failed
            return false;
        } catch (Exception e){
            LOG.error("LdapUserService.springLdap("+username+") : " + e.getMessage(), e);
            return false;
        }
    }

    /**
     * get roles based on username
     * @param username
     * @return List<GrantedAuthority>
     */
    private List<GrantedAuthority> getAuthorize(String username){
        //initialize authorize
        List<GrantedAuthority> mappedAuthorities = new ArrayList<>();

        try {
            // Get additional roles from database
            List<RoleMapping> roles = roleMappingDao.getAllValidRoleMappingsByUsername(username,new Date());

            // Add to each additional ROLE into mappedAuthorities
            for (RoleMapping role : roles) {
                mappedAuthorities.add(new SimpleGrantedAuthority(role.getRole().getRole().name()));
            }
        } catch (Exception e) {
            LOG.error("LdapUserService.getAuthorize("+username+") : " + e.getMessage(), e);
        }

        return mappedAuthorities;
    }

    /**
     * Find and log login user
     * @param username
     * @return user
     */
    private User getUser(String username){
        //find user
        User user = userDao.getUserByUserName(username);

        //log login
        loginLogService.saveUserLogin(username);

        //return user
        return user;
    }

}//end class
