package id.co.homecredit.jfs.cofin.ui.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

import id.co.homecredit.jfs.cofin.core.model.Action;
import id.co.homecredit.jfs.cofin.core.model.RoleAction;
import id.co.homecredit.jfs.cofin.core.service.RoleActionService;
import id.co.homecredit.jfs.cofin.core.service.ActionService;
import id.co.homecredit.jfs.cofin.ui.security.tools.AntUrlPathMatcher;
import id.co.homecredit.jfs.cofin.ui.security.tools.UrlMatcher;

public class SecurityMetadataSource implements FilterInvocationSecurityMetadataSource {
    private static final Logger log = LogManager.getLogger(SecurityMetadataSource.class);

    private Map<String, Collection<ConfigAttribute>> resourceMap = new HashMap<>();
    private UrlMatcher urlMatcher = new AntUrlPathMatcher();

    @Autowired
    private RoleActionService roleActionService;
    @Autowired
    private ActionService actionService;

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object)
            throws IllegalArgumentException {
        String url = ((FilterInvocation) object).getRequestUrl();
        if (url.startsWith("/lib/")) {
            return null;
        }

        List<Action> action = actionService.getAllAction(true);
        for (Action act : action) {
            Collection<ConfigAttribute> role = new ArrayList<>();
            List<RoleAction> roleActions = roleActionService
                    .getRoleActionsByAction(act.getAction());
            for (RoleAction roleAction : roleActions) {
                SecurityConfig sec = new SecurityConfig(
                        roleAction.getRole().getRole().toString());
                role.add(sec);
            }
            resourceMap.put(act.getAction(), role);
        }
        //log.debug("resource map {}", resourceMap.toString());
        for (String resURL : resourceMap.keySet()) {
            if (urlMatcher.pathMatchesUrl(resURL, url)) {
                return resourceMap.get(resURL);
            }
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
