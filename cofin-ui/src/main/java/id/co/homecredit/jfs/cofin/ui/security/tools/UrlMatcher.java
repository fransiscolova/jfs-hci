package id.co.homecredit.jfs.cofin.ui.security.tools;

public interface UrlMatcher {
    Object compile(String paramString);

    String getUniversalMatchPattern();

    boolean pathMatchesUrl(Object paramObject, String paramString);

    boolean requiresLowerCaseUrl();
}
