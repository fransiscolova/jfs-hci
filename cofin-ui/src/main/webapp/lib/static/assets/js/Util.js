function goBack() {
	window.history.back();
}

$("#addDwh").hide();

function postData(data, url, msgRes) {
	var response = "";
	$
			.ajax({

				xhr : function() {
					var xhr = new window.XMLHttpRequest();
					// Upload progress
					xhr.upload.addEventListener("progress", function(evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							showPleaseWait();

						}
					}, false);
					// Download progress
					xhr.addEventListener("progress", function(evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							showPleaseWait();

						}
					}, false);
					return xhr;
				},
				type : "POST",
				contentType : "application/json",
				url : url,
				data : JSON.stringify(data),
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(data) {

					$("#info").html("Success " + msgRes);
					setTimeout(function() {
						$("#pleaseWaitDialog").modal("hide");
						$('#myModal').modal('show');
					}, 3000);
				},
				error : function(e) {
					var json = "<h4> Failed to "
							+ msgRes
							+ ",please try again</h4><pre><button onClick='window.location.reload()'> OK </button>"
							+ e.responseText + "</pre>";
					// $('#feedback').html(json);

					$("#info").html(
							"Failed " + msgRes + "\<br>" + e.responseText);
					setTimeout(function() {
						$("#pleaseWaitDialog").modal("hide");
						$('#myModal').modal('show');

					}, 3000);

				}

			});

	return response;

}

function search(data, url, msgRes) {
	var response = "";
	$
			.ajax({

				xhr : function() {
					var xhr = new window.XMLHttpRequest();
					// Upload progress
					xhr.upload.addEventListener("progress", function(evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							console.log(percentComplete);
							// showPleaseWait();

						}
					}, false);
					// Download progress
					xhr.addEventListener("progress", function(evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							// showPleaseWait();

						}
					}, false);
					return xhr;
				},
				type : "POST",
				contentType : "application/json",
				url : url,
				data : JSON.stringify(data),
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(data) {
					t.clear().draw();

					$("#info").html("Success ");

					if (data.length > 0) {
						for (key in data) {
							var tmp = data[key];
							console.log(tmp.agreement);
							t.row.add([ tmp.contractNumber, tmp.status,
									tmp.status, tmp.creationDate,
									tmp.agreement, tmp.registrationDate,
									tmp.status, '<button>view</button>' ]);

						}
					} else {
						console.log("no result");
					}

					t.draw();

					$("#pleaseWaitDialog").modal("hide");
				},
				error : function(e) {
					var json = "<h4> Failed to "
							+ msgRes
							+ ",please try again</h4><pre><button onClick='window.location.reload()'> OK </button>"
							+ e.responseText + "</pre>";
					$("#info").html(
							"Failed " + msgRes + "\<br>" + e.responseText);
					setTimeout(function() {
						$("#pleaseWaitDialog").modal("hide");
						$('#myModal').modal('show');

					}, 3000);

				}

			});

	return response;

}

function loadMenu(datas, url, msgRes) {
	var response = "";
	$.ajax({

		xhr : function() {
			var xhr = new window.XMLHttpRequest();
			// Upload progress
			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					// showPleaseWait();

				}
			}, false);
			// Download progress
			xhr.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					// showPleaseWait();

				}
			}, false);
			return xhr;
		},
		type : "POST",
		contentType : "application/json",
		url : url,
		data : JSON.stringify(datas),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(data) {
			if (data.length > 0) {
				for (key in data) {
					var tmp = data[key];
					console.log(tmp.name);
					var att = tmp.name;
					document.getElementById(att).style.display = 'block';

					if (tmp.name == "Agreement") {

						if (datas['agreementId']) {
							$('#Agreement ul li').show();
						} else {
							$('#Agreement ul li').hide();
						}

					}

				}
			} else {
				console.log("no result");
			}

			if (datas['role'] == "DEV") {
				$("#Menu_Management").show();
			}

		},
		error : function(e) {
			// alert("Error To Load Menu");
		}

	});

	return response;

}

function getProcess(data, url, msgRes) {
	var response = "";
	$.ajax({

		xhr : function() {
			var xhr = new window.XMLHttpRequest();
			// Upload progress
			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					// showPleaseWait();

				}
			}, false);
			// Download progress
			xhr.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					// showPleaseWait();

				}
			}, false);
			return xhr;
		},
		type : "POST",
		contentType : "application/json",
		url : url,
		data : JSON.stringify(data),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(data) {
			
			select = document.getElementById('processType');
			select.options.length = 0;
			
			var opt = document.createElement('option');
			
			opt.value = "--";
			opt.innerHTML = "--Select Process--";
			select.appendChild(opt);
			
			if (data.length > 0) {
				for (key in data) {
					var tmp = data[key];					
					var opt = document.createElement('option');
					opt.value = tmp.id;
					opt.innerHTML = tmp.name;
					select.appendChild(opt);
				}
			} else {
				console.log("no result");
			}
			console.log(JSON.stringify(data));
		},
		error : function(e) {
			console.log("errrorr");

		}

	});

	return response;

}





function getProcessType(data, url, msgRes) {
	var response = "";
	$.ajax({

		xhr : function() {
			var xhr = new window.XMLHttpRequest();
			// Upload progress
			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					// showPleaseWait();

				}
			}, false);
			// Download progress
			xhr.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					// showPleaseWait();

				}
			}, false);
			return xhr;
		},
		type : "POST",
		contentType : "application/json",
		url : url,
		data : JSON.stringify(data),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(data) {
			
			select = document.getElementById('processType');
			select.options.length = 0;
			
			var opt = document.createElement('option');
			
			opt.value = "--";
			opt.innerHTML = "--Select Process--";
			select.appendChild(opt);
			
			if (data.length > 0) {
				for (key in data) {
					var tmp = data[key];					
					var opt = document.createElement('option');
					opt.value = tmp.id;
					opt.innerHTML = tmp.name;
					select.appendChild(opt);
				}
			} else {
				console.log("no result");
			}
			console.log(JSON.stringify(data));
		},
		error : function(e) {
			console.log("errrorr");

		}

	});

	return response;

}


function getFile(data, url, msgRes) {
	var response = "";
	$.ajax({

		xhr : function() {
			var xhr = new window.XMLHttpRequest();
			// Upload progress
			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					// showPleaseWait();

				}
			}, false);
			// Download progress
			xhr.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					// showPleaseWait();

				}
			}, false);
			return xhr;
		},
		type : "POST",
		contentType : "application/json",
		url : url,
		data : JSON.stringify(data),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(data) {
			console.log(" result" + JSON.stringify(data) );
			select = document.getElementById('confirmationFile');
			select.options.length = 0;
			
			var opt = document.createElement('option');
			
			opt.value = "--";
			opt.innerHTML = "--Select File--";
			select.appendChild(opt);
			
			if (data.length > 0) {
				for (key in data) {
					
					
					var tmp = data[key];					
					var opt = document.createElement('option');
					console.log( tmp.title);
					opt.value = tmp.id;
					opt.innerHTML = tmp.title;
					select.appendChild(opt);
				}
			} else {
				console.log("no result");
			}

		},
		error : function(e) {
			console.log("errrorr");

		}

	});

	return response;

}



function getProcessData(data, url, msgRes) {
	var response = "";
	$.ajax({

		xhr : function() {
			var xhr = new window.XMLHttpRequest();
			// Upload progress
			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					 showPleaseWait();

				}
			}, false);
			// Download progress
			xhr.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					 showPleaseWait();

				}
			}, false);
			return xhr;
		},
		type : "POST",
		contentType : "application/json",
		url : url,
		data : JSON.stringify(data),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(data) {
			console.log(" result" + JSON.stringify(data) );
		
			if (data.length > 0) {
				for (key in data) {					
					var tmp = data[key];					
					console.log(tmp);
				}
				
				var json = "<h4> Data already upload "
					+ msgRes
					+ ",please try again</h4><pre><button onClick='window.location.reload()'> OK </button>"
					"</pre>";
			$("#info").html(
					"Data already upload");
			setTimeout(function() {
				$("#pleaseWaitDialog").modal("hide");
				$('#myModal').modal('show');

			}, 3000);
				
			} else {
				console.log("no result");
				$("#pleaseWaitDialog").modal("hide");
			}

		},
		error : function(e) {
			console.log("errrorr");

		}

	});

	return response;

}