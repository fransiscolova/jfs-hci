<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="lib/static/assets/images/favicon.png" type="image/png"
	sizes="32x32">

<title>JFS HCI</title>
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700"
	rel="stylesheet">
<!-- Bootstrap -->
<link href="lib/static/assets/vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="lib/static/assets/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- iCheck -->
<link href="lib/static/assets/vendors/iCheck/skins/flat/green.css"
	rel="stylesheet">

<!-- Custom Theme Style -->
<link href="lib/static/assets/build/css/custom.min.css" rel="stylesheet">
<link href="lib/static/assets/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="lib/static/assets/css/style.css" rel="stylesheet">



</head>

<body class="nav-md body-jfs">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="index.html" class="site_title"><img
							src="lib/static/assets/images/small_logo.png" class="img-responsive"><span>JFS
								HCI</span></a>
					</div>
					<div class="clearfix"></div>
					<!-- menu profile quick info -->
					<div class="side-profile clearfix">
						<div class="profile_pic">
							<img src="lib/static/assets/images/img.jpg" alt="..." class="profile_img">
						</div>
						<div class="profile profile_info">
							<h2>Ilham Jayakesuma</h2>
							<span>Admin</span>
						</div>
					</div>
					<div id="sidebar-menu"
						class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<ul class="nav side-menu">
								<li><a href="dashboard_main-menu.html" class="home">Home</a></li>
								<li><a href="partner" class="partner">Partner</a></li>
								<li><a href="agreement" class="agreement">Agreement
										and Criteria</a></li>
								<li><a href="contract" class="contract">Contract</a></li>
								<li><a href="batch" class="batch">Batch Process</a></li>
								<li><a href="usermanagement" class="usermanagement">User
										Management</a></li>


							</ul>
						</div>
					</div>
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>
						<ul class="nav navbar-nav navbar-right">
							<li role="presentation" class="dropdown"><a
								href="javascript:;" class="info-number"> <i
									class="fa fa-sign-out"></i>
							</a></li>
							<li><a href="javascript:;" class="info-number"> <i
									class="fa fa-envelope-o"></i> <span class="badge bg-green">6</span>
							</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col main-content" role="main">
				<div class="row">
					<ol class="breadcrumb">
						<li class="active">Partner</li>
					</ol>



					<div class="table-responsive">

						<table class="table-jfs display" cellspacing="0" width="100%">
							<div id="feedback"></div>
							<thead>
								<tr>
									<th>Code</th>
									<th>Isdeleted</th>
									<th>UpdateBy</th>
								</tr>
							</thead>
							<tbody>							
							<c:forEach var="o" items="${subject}">
								<tr>
									<td>${o.code}</td>
									<td>${o.deleted}</td>
									<td>${o.updatedBy}</td>
								</tr>
							</c:forEach>
								</tbody>
						</table>

					</div>
				</div>
			</div>
			<!-- /page content -->
		</div>
	</div>

	<!-- jQuery -->
	<script src="lib/static/assets/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="lib/static/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="lib/static/assets/vendors/iCheck/icheck.min.js"></script>

	<!-- Custom Theme Scripts -->
	<script src="lib/static/assets/build/js/custom.min.js"></script>
	<script src="lib/static/assets/js/jquery.dataTables.min.js"></script>


	<script>
		var t = $("table").DataTable({
			"bPaginate" : true,
			"bLengthChange" : false,
			"bFilter" : true,
			"bInfo" : false,
			"order" : [ [ 2, "asc" ] ]
		});

		function sentData() {
			var data = {}
			data["fieldName"] = $("#fieldname").val();
			data["fieldLabel"] = $("#fieldname").val();
			data["type"] = $("#type").val();
			data["verificationFunction"] = $("#verifications").val();
			data["restrictions"] = $("#restrictions").val();

			$("#save").prop("disabled", true);
			$("#add").prop("disabled", true);
			$
					.ajax({

						xhr : function() {
							var xhr = new window.XMLHttpRequest();
							//Upload progress
							xhr.upload
									.addEventListener(
											"progress",
											function(evt) {
												if (evt.lengthComputable) {
													var percentComplete = evt.loaded
															/ evt.total;
													//Do something with upload progress
													console
															.log(percentComplete);
													var json = "<h4>Onprogress to save data</h4>";
													$('#feedback').html(json);

												}
											}, false);
							//Download progress
							xhr
									.addEventListener(
											"progress",
											function(evt) {
												if (evt.lengthComputable) {
													var percentComplete = evt.loaded
															/ evt.total;
													//Do something with download progress
													console
															.log(percentComplete);
													var json = "<h4>Onprogress to show data</h4>";
													$('#feedback').html(json);
												}
											}, false);
							return xhr;
						},
						type : "POST",
						contentType : "application/json",
						url : "/savefield",
						data : JSON.stringify(data),
						dataType : 'json',
						cache : false,
						timeout : 600000,
						success : function(data) {
							var json = "<h4>Response save</h4><pre>"
									+ JSON.stringify(data, null, 4)
									+ "</pre><button onClick='window.location.reload()'> OK </button>";
							$('#feedback').html(json);
							//console.log("SUCCESS : ", data);
							if (data.hasOwnProperty('ErrorCode')) {
								$("#save").prop("disabled", false);
								$("#fieldname").prop("disabled", false);
								$("#fieldname").prop("disabled", false);
								$("#type").prop("disabled", false);
								$("#verifications").prop("disabled", false);
								$("#restrictions").prop("disabled", false);
							} else {
								$("#fieldname").prop("disabled", true);
								$("#fieldname").prop("disabled", true);
								$("#type").prop("disabled", true);
								$("#verifications").prop("disabled", true);
								$("#restrictions").prop("disabled", true);

							}
						},
						error : function(e) {

							var json = "<h4>Failed to save data,please try again</h4><pre><button onClick='window.location.reload()'> OK </button>"
									+ e.responseText + "</pre>";
							$('#feedback').html(json);

							//console.log("ERROR : ", e);
							//$("#btn-search").prop("disabled", false);

						}
					});

		}

		function update() {
			var data = {}
			data["fieldName"] = $("#fieldname").val();
			data["fieldLabel"] = $("#fieldname").val();
			data["type"] = $("#type").val();
			data["verificationFunction"] = $("#verifications").val();
			data["restrictions"] = $("#restrictions").val();

			$("#save").prop("disabled", true);
			$("#add").prop("disabled", true);
			$
					.ajax({

						xhr : function() {
							var xhr = new window.XMLHttpRequest();
							//Upload progress
							xhr.upload
									.addEventListener(
											"progress",
											function(evt) {
												if (evt.lengthComputable) {
													var percentComplete = evt.loaded
															/ evt.total;
													//Do something with upload progress
													console
															.log(percentComplete);
													var json = "<h4>Onprogress to save data</h4>";
													$('#feedback').html(json);

												}
											}, false);
							//Download progress
							xhr
									.addEventListener(
											"progress",
											function(evt) {
												if (evt.lengthComputable) {
													var percentComplete = evt.loaded
															/ evt.total;
													//Do something with download progress
													console
															.log(percentComplete);
													var json = "<h4>Onprogress to show data</h4>";
													$('#feedback').html(json);
												}
											}, false);
							return xhr;
						},
						type : "POST",
						contentType : "application/json",
						url : "/updatefield",
						data : JSON.stringify(data),
						dataType : 'json',
						cache : false,
						timeout : 600000,
						success : function(data) {
							var json = "<h4>Response save</h4><pre>"
									+ JSON.stringify(data, null, 4)
									+ "</pre><button onClick='window.location.reload()'> OK </button>";
							$('#feedback').html(json);
							//console.log("SUCCESS : ", data);
							if (data.hasOwnProperty('ErrorCode')) {
								$("#save").prop("disabled", false);
								$("#fieldname").prop("disabled", false);
								$("#fieldname").prop("disabled", false);
								$("#type").prop("disabled", false);
								$("#verifications").prop("disabled", false);
								$("#restrictions").prop("disabled", false);
							} else {
								$("#fieldname").prop("disabled", true);
								$("#fieldname").prop("disabled", true);
								$("#type").prop("disabled", true);
								$("#verifications").prop("disabled", true);
								$("#restrictions").prop("disabled", true);

							}
						},
						error : function(e) {

							var json = "<h4>Failed to save data,please try again</h4><pre><button onClick='window.location.reload()'> OK </button>"
									+ e.responseText + "</pre>";
							$('#feedback').html(json);

							//console.log("ERROR : ", e);
							//$("#btn-search").prop("disabled", false);

						}
					});

		}

		$('#add')
				.on(
						'click',
						function() {
							$("#add").prop("disabled", true);
							t.row
									.add(
											[
													'-',
													'<input type="text" id="fieldname" name="fieldname" placeholder="field name" >',
													'<input type="text" id="type" name="type" placeholder="type" >',
													'<input type="text" id="verifications" name="verifaction" placeholder="verifaction" >',
													'<input type="text" id="restrictions" name="restrictions" placeholder="restrictions" ></form>',
													'<button class="btn btn-approve" onClick="sentData()">Save</button> <button class="btn btn-reject" id="save" onClick="window.location.reload()">Delete</button>'

											]).draw(false);

						});

		function updateData(fieldname, type, ver, res) {

			t.row
					.add(
							[
									'-',
									'<input type="text" id="fieldname" value="' + fieldname + '" name="fieldname" placeholder="field name" disabled>',
									'<input type="text" id="type" name="type" placeholder="type" value="' + type + '" >',
									'<input type="text" id="verifications" name="verifaction" placeholder="verifaction" value="' + ver + '">',
									'<input type="text" id="restrictions" name="restrictions" placeholder="restrictions" value="' + res + '"></form>',
									'<button class="btn btn-approve" onClick="update()">Save</button> <button class="btn btn-reject" id="save" onClick="window.location.reload()">Delete</button>'

							]).draw(false);

		}

		function deleteData(val) {
			var data = {}
			data["fieldId"] = val;

			$("#save").prop("disabled", true);
			$("#add").prop("disabled", true);
			$
					.ajax({

						xhr : function() {
							var xhr = new window.XMLHttpRequest();
							//Upload progress
							xhr.upload
									.addEventListener(
											"progress",
											function(evt) {
												if (evt.lengthComputable) {
													var percentComplete = evt.loaded
															/ evt.total;
													//Do something with upload progress
													console
															.log(percentComplete);
													var json = "<h4>Onprogress to delete data</h4>";
													$('#feedback').html(json);

												}
											}, false);
							//Download progress
							xhr
									.addEventListener(
											"progress",
											function(evt) {
												if (evt.lengthComputable) {
													var percentComplete = evt.loaded
															/ evt.total;
													//Do something with download progress
													console
															.log(percentComplete);
													var json = "<h4>Onprogress to show respon</h4>";
													$('#feedback').html(json);
												}
											}, false);
							return xhr;
						},
						type : "POST",
						contentType : "application/json",
						url : "/deletefield",
						data : JSON.stringify(data),
						dataType : 'json',
						cache : false,
						timeout : 600000,
						success : function(data) {
							var json = "<h4>Response delete</h4><pre>"
									+ JSON.stringify(data, null, 4)
									+ "</pre><button onClick='window.location.reload()'> OK </button>";
							$('#feedback').html(json);
							//console.log("SUCCESS : ", data);

						},
						error : function(e) {

							var json = "<h4>Failed to delete data,please try again</h4><pre><button onClick='window.location.reload()'> OK </button>"
									+ e.responseText + "</pre>";
							$('#feedback').html(json);

							//console.log("ERROR : ", e);
							//$("#btn-search").prop("disabled", false);

						}
					});

		}
	</script>
</body>
</html>
