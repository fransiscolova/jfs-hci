<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="breadcrumb" scope="session" value="Agreement"/>
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<jsp:include page="fragment/topnavigation.jsp"/>
<div class="table-responsive">
 	<fieldset class="the-fieldset">
    <legend class="the-legend">AGREEMENT</legend>
    <input type="hidden" name="buzei" id="buzei" value="${sizes}"/>
	<table id="xtable" class="table-jfs display" cellspacing="0" width="100%">
		<div id="feedback"></div>
		<thead>
			<tr>
				<th>No</th>
				<th>Agreement Id</th>
				<th>Agreement Code</th>
				<th>Agreement Name</th>
				<th>Partner Name</th>
				<th>Valid From</th>
				<th>Valid To</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="agreement" items="${agreement}">
				<tr>
					<td>${agreement.id}</td>
					<td>${agreement.idAgreement}</td>
					<td>${agreement.codeAgreement}</td>
					<td>${agreement.nameAgreement}</td>
					<td>${agreement.codePartner}</td>
					<td>${agreement.createdBy}</td>
					<td>${agreement.updatedBy}</td>
					<td>
						<c:set var="deleted" scope="session" value="${agreement.isDelete}"/> 
						<c:if test="${deleted=='N'}">
							<c:out value="ACTIVE"/>
						</c:if> 
						<c:if test="${deleted=='Y'}">
							<c:out value="INACTIVE"/>
						</c:if>
					</td>
					<td>
						<button class="btn btn-approve" onClick="toDetail('${agreement.idAgreement}')">View</button>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</fieldset>
	<div class="modal fade" id="addAgreement" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">X</button>
					<h4 id="title" class="modal-title">Add Agreement</h4>
				</div>
				<div class="modal-body">
					<fieldset class="the-fieldset">
    				<legend class="the-legend">Form Add User</legend>
					<form id="agreement" name="agreement">
						<div class="form-group">
							<label class="col-md-4">Agreement Code</label> 
							<input type="text" id="code" name="code" class="form-control" placeholder="Agreement Code">
						</div>
						<div class="form-group">
							<label class="col-md-4">Agreement Name</label> 
							<input type="text" name="name" id="name" class="form-control" placeholder="Agreement Name">
						</div>
						<div class="form-group">
							<label class="col-md-4">Agreement Type</label>
							<select class="form-control" id="type" name="type" style="border-radius:6px;">
								<option value="">Select Agreement Type</option>
								<c:forEach items="${xtype}" var="xtype">
									<option value="${xtype.value}">${xtype.text}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label class="col-md-4">Partner Name</label> 
							<select style="border-radius:6px;" class="form-control" id="partner" name="partner">
							<option value="">Select Partner</option>
								<c:forEach items="${partner}" var="partner">
									<option value="${partner.value}">${partner.text}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label class="col-md-4">Description</label>
							<input type="text" id="desc" name="desc" class="form-control" placeholder="Description">
						</div>
						<div class="form-group">
							<label class="col-md-4">Valid From</label> 
							<input type="text" name="validFrom" id="validFrom" class="form-control" placeholder="Valid From">
						</div>
						<div>
							<label class="col-md-4">Valid To</label> 
							<input type="text" name="validTo" id="validTo" class="form-control" placeholder="Valid To">
						</div>
						<div class="text-right">
							<br>
								<button type="button" id="cancelBtn" name="cancelBtn" class="btn btn-red">Cancel</button>
								<button type="button" onClick="save()" id="saveBtn" name="saveBtn" class="btn btn-blue">Save</button>
							</br>
						</div>
					</form>
					</fieldset>
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="fragment/footer.jsp" />

	<script type="text/javascript">
		$("#partnerModal").on("shown.bs.modal", function() {
			$('#addAgreement').modal('hide');
		});
		$("#partnerModal").on("hidden.bs.modal", function() {
			$('#addAgreement').modal('show');
		});
		t.order([ 5, 'asc' ]).draw();
		function save() {
			$("#add").prop("disabled", true);
			if ($("#code").val() == '' || $("#name").val() == '' || $("#partner").val() == '' || $("#type").val() == '' || $("#validFrom").val() == '' || $("#validTo").val() == '' || $("#desc").val() == '') {
				$("#info").html("CompleteThe Form Before Save!");
				$('#myModal').modal('show');
			} else {
				if ($('#saveBtn').text() == 'Save Now') {
					var data = getData();
					postData(data, '/cofin-ui/agreement/save', "save data");
				}
				$("#agreement :input:text").attr("disabled", true);
				$("#partner").attr("disabled", true);
				$("#type").attr("disabled", true);
				$('#title').text('Are you sure want to save?');
				$('#saveBtn').text('Save Now');
				$('#cancelBtn').show();
			}
		}
		$('#add').on('click', function() {
			$("form")[0].reset();
			$('#addAgreement').modal('show');
			$("#validFrom").datepicker();
			$("#validTo").datepicker();
			$("#cancelBtn").hide();
		});
		$('#cancelBtn').on('click', function() {
			$('#addAgreement').modal('show');
			$("#cancelBtn").hide();
			$('#title').text('Cancel Save!');
			$("#agreement :input:text").attr("disabled", false);
			$("#partner").attr("disabled", false);
			$("#type").attr("disabled", false);
			$('#saveBtn').text('Save');
		});
		function getData() {
			var data = {};
			data["code"] = $("#code").val();
			data["name"] = $("#name").val();
			data["partnerId"] = $("#partner").val();
			data["validFrom"] = $("#validFrom").val();
			data["validTo"] = $("#validTo").val();
			data["idAlias"] = $("#buzei").val();
			data["agreementType"] = $("#type").val();
			data["description"] = $("#desc").val();
			return data;
		}
		function toDetail(id){
			window.location.replace('detail/'+id);
		}
	</script>
	</body>
</html>
