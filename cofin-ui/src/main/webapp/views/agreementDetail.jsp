<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="breadcrumb" scope="session" value="Detail"/>
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<div class="top_nav">
	<div style="background-color: #CB0D31" class="nav_menu">
		<nav>
			<ul class="nav navbar-nav navbar-right">
				<li role="presentation" class="dropdown">
				<a href="javascript:;" class="info-number"> 
				<i class="fa fa-sign-out"></i>
				</a></li>
			</ul>
		</nav>
	</div>
</div>
<div class="right_col main-content" role="main">
	<div class="row">
		<ol class="breadcrumb">
			<li class="active"><a href="/cofin-ui/agreement/" class="agreement"><< Agreement</a></li>
		</ol>
		<div class="form-creation">
		<form class="form-inline">
		  <fieldset class="the-fieldset">
          <legend class="the-legend">Agreement</legend>
				<div class="form-group">
					<label class="col-md-2">Partner Name</label>
					<div class="col-md-3">
						<input size="30" type="text" id="partner" name="partner" value="${partnerName}" class="form-control" disabled>
					</div>
					<label class="col-md-1">Valid From</label>
					<div class="col-md-2">
						<input value="${validFrom}" type="text" id="validFromAgreement" name="validFrom" class="form-control" disabled>
					</div>
					<div class="col-md-3" style="text-align:center">
						<button id="updateAgreement" type="button" style="width:135px;" class="btn btn-blue">Update</button>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Agreement Code</label>
					<div class="col-md-3">
						<input size="30" type="text" id="agreementCode"	name="agreementCode" value="${agreement.codeAgreement}" class="form-control" disabled>
					</div>
					<label class="col-md-1">Valid To</label>
					<div class="col-md-2">
						<input value="${validTo}" type="text" id="validToAgreement" name="validTo" class="form-control" disabled>
					</div>
					<div class="col-md-3" style="text-align:center">
						<c:if test="${status=='Deactive'}">
							<button id="deactive" type="button" class="btn btn-red">Deactive</button>
						</c:if>
						<c:if test="${status=='Reactive'}">
							<button id="deactive" type="button" class="btn btn-blue">Reactive</button>
						</c:if>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Agreement Name</label>
					<div class="col-md-3">
						<input size="30" type="text" id="agreementName" id="agreementName" name="agreementName" value="${agreement.nameAgreement}" class="form-control" disabled>
					</div>
					<label class="col-md-1">Description</label>
					<div class="col-md-5">
						<input type="text" id="desc" name="desc" value="${agreement.description}" class="form-control" disabled>
						<input type="hidden" id="bstat" name="bstat" value="${status}"/>
					</div>
				</div>
				</fieldset>
				<br>
            <fieldset class="the-fieldset">
            <legend class="the-legend">Active Agreement Detail</legend>
				<div class="form-group">
					<label class="col-md-2">Interest Rate</label>
					<div class="col-md-1">
						<input size="5" value="${interestRate}" type="text"	id="interestRates" name="interestRates" class="form-control" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Principal Split Rate</label>
					<div class="col-md-1">
						<input size="5" value="${principalSplitRate}" type="text" id="principleSplitRates" name="principleSplitRates" class="form-control" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Admin Fee Rate</label>
					<div class="col-md-9">
						<input size="5" value="${adminFeeRate}" type="text"	id="adminFeeRates" name="adminFeeRates" class="form-control" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Penalty Split Rate</label>
					<div class="col-md-9">
						<input size="5" value="${penaltySplitRate}" type="text" id="principleSplitRates" name="principleSplitRates" class="form-control" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Principal Split Rate HCI</label>
					<div class="col-md-9">
						<input size="5" value="${principalSplitRateHci}" type="text" id="principalSplitRatesHci" name="principalSplitRatesHci" class="form-control" disabled>
					</div>
				</div>
			</fieldset>
			<br>
			</form>
		</div>
		<div class="table-responsive">
			<fieldset class="the-fieldset">
  		  <legend class="the-legend">List Agreement Detail</legend>
			<table id="xtable" class="table-jfs display" cellspacing="0"	width="100%">
				<div id="feedback"></div>
				<thead>
					<tr>
						<th style="display:none;">X</th>
						<th style="display:none;">X</th>
						<th>Interest Rate</th>
						<th>Principal Split Rate</th>
						<th>Admin Fee Rate</th>
						<th>Penalty Split Rate</th>
						<th>Principal Split Rate HCI</th>
						<th>Valid From</th>
						<th>Valid To</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="agreementDetail" items="${agreementDetail}">
						<tr>
							<td style="display:none;">${agreementDetail.idAgreement}</td>
							<td style="display:none;">${agreementDetail.id}</td>
							<td>${agreementDetail.interestRate}</td>
							<td>${agreementDetail.principalSplitRate}</td>
							<td>${agreementDetail.adminFeeRate}</td>
							<td>${agreementDetail.penaltySplitRate}</td>
							<td>${agreementDetail.principalSplitRateHci}</td>
							<td>
								<c:set var="validFrom" value="${agreementDetail.bankProductCode}"/>	
								<c:set var="dateFrom" value="${fn:substring(validFrom,0,10)}"/>
								<c:out value="${dateFrom}"/>
							</td>
							<td>
								<c:set var="validTo" value="${agreementDetail.bankProductCode}"/>
								<c:set var="dateTo" value="${fn:substring(validTo,11,21)}"/>
								<c:out value="${dateTo}"/>
							</td>
							<td>
								<c:set var="status" value="${agreementDetail.bankProductCode}"/>
								<c:set var="stats" value="${fn:substring(status,22,30)}"/>
								<c:out value="${stats}"/>
							</td>
							<td>
								<button class="btn btn-approve" onClick="updateData('${agreementDetail.id}','${agreementDetail.interestRate}','${agreementDetail.principalSplitRate}','${agreementDetail.adminFeeRate}','${agreementDetail.penaltySplitRate}','${agreementDetail.principalSplitRateHci}','${agreementDetail.validFrom}','${agreementDetail.validTo}','${agreementDetail.bankProductCode}')">View</button>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</fieldset>
			<div class="modal fade" id="addAgreementDetail" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">X</button>
							<h4 id="title" class="modal-title">Add Agreement Detail</h4>
						</div>
						<div class="modal-body">
							<fieldset class="the-fieldset">
    						<legend class="the-legend">Form Agreement Detail</legend>
							<form id="agreementDetail" name="agreementDetail">
								<div class="form-group">
									<label class="col-md-4" id="lblId">ID</label>
									<input type="text" id="id" name="id" class="form-control" placeholder="ID">
								</div>
								<div class="form-group">
									<label class="col-md-4">Interest Rate</label> 
									<input type="text" id="interestRate" name="interestRate" class="form-control" placeholder="Interest Rate" maxlength='2' pattern='^[0-9]$' onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>
								<div class="form-group">
									<label class="col-md-4">Principal Split Rate</label> 
									<input type="text" id="principalSplitRate" name="principalSplitRate" class="form-control" placeholder="Principal split rate" maxlength='2' pattern='^[0-9]$' onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>
								<div class="form-group">
									<label class="col-md-4">Admin Fee Rate</label> 
									<input type="text" name="adminFeeRate" id="adminFeeRate" class="form-control" placeholder="Admin Fee rate" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>
								<div class="form-group">
									<label class="col-md-4">Penalty Split Rate</label> 
									<input type="text" name="penaltySplitRate" id="penaltySplitRate" class="form-control" placeholder="Penalty Split Rate" maxlength='2' pattern='^[0-9]$' onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>
								<div class="form-group">
									<label class="col-md-4">Principal Split Rate HCI</label>
									<input type="text" name="principalSplitRateHci" id="principalSplitRateHci" class="form-control" placeholder="Principal Split Rate HCI" pattern='^[0-9]$' onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>
								<div class="form-group">
									<label class="col-md-4">Valid From</label> 
									<input type="text" name="validFrom" id="validFrom" class="form-control" placeholder="Valid From">
								</div>
								<div>
									<label class="col-md-4">Valid To</label> 
									<input type="text" name="validTo" id="validTo" class="form-control" placeholder="Valid To">
								</div>
								<div class="text-right">
									<br>
									<button type="button" id="cancelBtn" name="cancelBtn" class="btn btn-red">Cancel</button>
									<button type="button" onClick="save()" id="saveBtn" name="saveBtn" class="btn btn-blue">Save</button>
								</div>
							</form>
							</fieldset>
						</div>
						<div class="modal-footer">
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="fragment/footer.jsp"/>
			<script type="text/javascript">
				t.order([ 5, 'asc' ]).draw();
				var typeProcess = '';
				function save() {
					var data = getData()
					$("#add").prop("disabled", true);
					if ($("#interestRate").val() == '' || $("#principalSplitRate").val() == '' || $("#adminFeeRate").val() == '' || $("#penaltySplitRate").val() == '' || $("#validFrom").val() == '' || $("#validTo").val() == ''){
						$("#info").html("Complete the form before save!");
						$('#myModal').modal('show');
					} else {
						if($('#saveBtn').text() == 'Save Now'){
							var data = getData();
							postData(data, '/cofin-ui/agreement/detail/save', "Save Data");
						}else if($('#saveBtn').text() == 'Update Now'){
							var data = getData();
							postData(data, '/cofin-ui/agreement/detail/update', "Update Data");
						}
						$("#agreementDetail :input:text").attr("disabled", true);
						$('#title').text('Are you sure want to save?');
						if(typeProcess == 'Update'){
							$('#saveBtn').text('Update Now');
						}else{
							$('#saveBtn').text('Save Now');
						}
						$("#cancelBtn").show();
					}
				}
				$(function(){
					$("#validFromAgreement").datepicker();
				});
				$(function(){
					$("#validToAgreement").datepicker();
				});
				$('#add').on('click',function(){
					typeProcess = '';
					$('#addAgreementDetail').modal('show');
					$("form")[0].reset();
					$("#validFrom").datepicker();
					$("#validTo").datepicker();
					$("#cancelBtn").hide();
					$('#title').text("Add Agreement Detail");
					document.getElementById("id").style.display="none";
					document.getElementById("lblId").style.display="none";
					$('#interestRate').val('');
					$('#principalSplitRate').val('');
					$('#adminFeeRate').val('');
					$('#penaltySplitRate').val('');
					$('#validFrom').val('');
					$('#validTo').val('');
					$('#interestRate').removeAttr("disabled");
					$('#principalSplitRate').removeAttr("disabled");
					$('#adminFeeRate').removeAttr("disabled");
					$('#penaltySplitRate').removeAttr("disabled");
					$('#validFrom').removeAttr("disabled");
					$('#validTo').removeAttr("disabled");
					$("#saveBtn").text("Save");
				});
				$('#cancelBtn').on('click',function(){
					$("#cancelBtn").hide();
					$("#agreementDetail :input:text").attr("disabled", false);
					if(typeProcess == 'Update'){
						$("#saveBtn").text("Update");
						$("#interestRate").prop('disabled','disabled');
						$("#principalSplitRate").prop('disabled','disabled');
						$("#adminFeeRate").prop('disabled','disabled');
						$("#penaltySplitRate").prop('disabled','disabled');
					}else{
						$("#saveBtn").text("Save");
					}
					$('#title').text('Cancel Save');
				});
				$('#updateAgreement').on('click',function(){
					if($("#updateAgreement").text().includes('Update')){
						$("#updateAgreement").text('Save');
						$("#partner").prop('disabled', true);
						$("#validFromAgreement").prop('disabled', false);
						$("#validToAgreement").prop('disabled', false);
						$("#agreementName").prop('disabled', false);
						$("#agreementCode").prop('disabled', false);
						$("#desc").prop('disabled', false);
					}else{
						var d1 = Date.parse($("#validFromAgreement").val());
						var d2 = Date.parse($("#validToAgreement").val());
						if(d1>d2){
							$("#info").html("Valid To Must Be Greather Than Valid From!");
							$('#myModal').modal('show');
						}else if($("#partner").val() == '' || $("#agreementCode").val() == '' || $("#agreementCode").val() == '' || $("#validFromAgreement").val() == '' || $("#validToAgreement").val() == ''){
							$("#info").html("Complete The Form Before Save!");
							$('#myModal').modal('show');
						}else{
							$('#infoConfirmation').text('Are You Sure Want To Update?');
							$('#myModalConfirmation').modal('show');
							postData(data,'/cofin-ui/agreement/update',"Update Data");
						}
					}
				});
				$('#deactive').on('click',function(){
					var status = "";
					var xtext = $("#bstat").val();
					if(xtext == "Reactive"){
						typeProcess = "reactive";
						status = "Reactive";
					}else{
						typeProcess = "deactive";
						status = "Deactive";
					}
					$('#infoConfirmation').text('Are you sure want to '+status+'?');
					$('#myModalConfirmation').modal('show');
				});
				$('#ok').on('click', function() {
					var data = {};
					if(typeProcess == "deactive"){
						data["id"] = '${agreementId}';
						postData(data, '/cofin-ui/agreement/activation', "Deactive Data");
					}else if(typeProcess == "reactive"){
						data["id"] = '${agreementId}';
						postData(data, '/cofin-ui/agreement/activation', "Reactive Data");
					}else{
						data["partner"] = $("#partner").val();
						data["code"] = $("#agreementCode").val();
						data["name"] = $("#agreementName").val();
						data["validFrom"] = $("#validFromAgreement").val();
						data["validTo"] = $("#validToAgreement").val();
						data["agreementId"] = '${agreementId}';
						data["desc"] = $("#desc").val();
						postData(data, '/cofin-ui/agreement/update', "update data");
					}
					$('#myModalConfirmation').modal('hide');
				})
				function getData() {
					var data = {}
					data["agreementId"] = '${agreementId}';
					data["validFrom"] = $("#validFrom").val();
					data["validTo"] = $("#validTo").val();
					data["id"] = $("#id").val();
					data["interestRate"] = $("#interestRate").val();
					data["principalSplitRate"] = $("#principalSplitRate").val();
					data["principalSplitRateHci"] = $("#principalSplitRateHci").val();
					data["adminFeeRate"] = $("#adminFeeRate").val();
					data["penaltySplitRate"] = $("#penaltySplitRate").val()
					return data;
				}
				function updateData(id,interestRate,principalSplitRate,adminFeeRate,penaltySplitRate,principalSplitRateHci,validFrom,validTo,deleted){
					typeProcess = 'Update';
					var status = deleted.substring(22,30);
					$('#addAgreementDetail').modal('show');
					$("#id").val(id);
					$("#interestRate").val(interestRate);
					$("#principalSplitRate").val(principalSplitRate);
					$("#adminFeeRate").val(adminFeeRate);
					$("#penaltySplitRate").val(penaltySplitRate);
					$("#principalSplitRateHci").val(principalSplitRateHci);
					$("#interestRate").prop('disabled','disabled');
					$("#principalSplitRate").prop('disabled','disabled');
					$("#adminFeeRate").prop('disabled','disabled');
					$("#penaltySplitRate").prop('disabled','disabled');
					$("#principalSplitRateHci").prop('disabled','disabled');
					$("#validFrom").val(validFrom);
					$("#validTo").val(validTo);
					$("#validFrom").datepicker();
					$("#validTo").datepicker();
					$("#cancelBtn").hide();
					$("#saveBtn").text("Update");
					formatDateFrom($('#validFrom').val());
					formatDateTo($('#validTo').val());
					$('#title').text(typeProcess+" Agreement Detail");
					document.getElementById("lblId").style.display="none";
					document.getElementById("id").style.display="none";
					if(status=='INACTIVE'){
						$("#validFrom").prop('disabled','disabled');
						$("#validTo").prop('disabled','disabled');
						$("#saveBtn").hide();
					}else{
						$("#validFrom").removeAttr("disabled");
						$("#validTo").removeAttr("disabled");
						$("#saveBtn").show();
					}
				}
				function formatDateFrom(tanggal){
					var hari = tanggal.substring(8,10);
					var bulan = tanggal.substring(4,7);
					var tahun = tanggal.substring(24,28);
					if(bulan=='Jan'){
						bulan='01';
					}else if(bulan=='Feb'){
						bulan='02';
					}else if(bulan=='Mar'){
						bulan='03';
					}else if(bulan=='Apr'){
						bulan='04';
					}else if(bulan=='May'){
						bulan='05';
					}else if(bulan=='Jun'){
						bulan='06';
					}else if(bulan=='Jul'){
						bulan='07';
					}else if(bulan=='Aug'){
						bulan='08';
					}else if(bulan=='Sep'){
						bulan='09';
					}else if(bulan=='Oct'){
						bulan='10';
					}else if(bulan=='Nov'){
						bulan='11';
					}else if(bulan=='Dec'){
						bulan='12';
					}
					var tgl = hari+"/"+bulan+"/"+tahun;
					$("#validFrom").val(tgl);
					$("#validFrom").datepicker({
						format : 'dd/MM/yyyy'
					});
				}
				function formatDateTo(tanggal){
					var hari = tanggal.substring(8,10);
					var bulan = tanggal.substring(4,7);
					var tahun = tanggal.substring(24,28);
					if(bulan=='Jan'){
						bulan='01';
					}else if(bulan=='Feb'){
						bulan='02';
					}else if(bulan=='Mar'){
						bulan='03';
					}else if(bulan=='Apr'){
						bulan='04';
					}else if(bulan=='May'){
						bulan='05';
					}else if(bulan=='Jun'){
						bulan='06';
					}else if(bulan=='Jul'){
						bulan='07';
					}else if(bulan=='Aug'){
						bulan='08';
					}else if(bulan=='Sep'){
						bulan='09';
					}else if(bulan=='Oct'){
						bulan='10';
					}else if(bulan=='Nov'){
						bulan='11';
					}else if(bulan=='Dec'){
						bulan='12';
					}
					var tgl = hari+"/"+bulan+"/"+tahun;
					$("#validTo").val(tgl);
					$("#validTo").datepicker({
						format : 'dd/MM/yyyy'
					});
				}
			</script>
		</body>
</html>
