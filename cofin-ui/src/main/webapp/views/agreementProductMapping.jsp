<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="breadcrumb" scope="session" value="Product"/>
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<div class="top_nav">
	<div style="background-color:#CB0D31" class="nav_menu">
		<nav>
			<ul class="nav navbar-nav navbar-right">
				<li role="presentation" class="dropdown">
				<a href="javascript:;" class="info-number"> 
				<i class="fa fa-sign-out"></i>
				</a></li>
			</ul>
		</nav>
	</div>
</div>
<div class="right_col main-content" role="main">
	<div class="row">
		<ol class="breadcrumb">
			<li class="active"><a href="/cofin-ui/agreement/" class="agreement"><< Agreement</a></li>
		</ol>
		<div class="form-creation">
		<fieldset class="the-fieldset">
   		 <legend class="the-legend">AGREEMENT</legend>
			<form class="form-inline">
				<div class="form-group">
					<label class="col-md-2">Partner Name</label>
					<div class="col-md-3">
						<input size="30" type="text" id="partner" name="partner" value="${partnerName}" class="form-control" disabled>
					</div>
					<label class="col-md-1">Valid From</label>
					<div class="col-md-2">
						<input value="${validFrom}" type="text" id="validFromAgreement" name="validFrom" class="form-control" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Agreement Code</label>
					<div class="col-md-3">
						<input size="30" type="text" id="agreementCode" name="agreementCode" value="${agreement.codeAgreement}" class="form-control" disabled>
					</div>
					<label class="col-md-1">Valid To</label>
					<div class="col-md-2">
						<input value="${validTo}" type="text" id="validToAgreement" name="validTo" class="form-control" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Agreement Name</label>
					<div class="col-md-3">
						<input size="30" type="text" id="agreementName" id=name="agreementName" value="${agreement.nameAgreement}" class="form-control" disabled>
					</div>
					<label class="col-md-1">Description</label>
					<div class="col-md-5">
						<input type="text" id="desc" name="desc" value="${agreement.description}" class="form-control" disabled>
					</div>
				</div>
			</form>
			</fieldset>
			<br>
		</div>
		<div class="table-responsive">
		<fieldset class="the-fieldset">
   		 <legend class="the-legend">Product Mapping</legend>
			<table id="table" class="table-jfs display" cellspacing="0" width="100%">
				<div id="feedback"></div>
				<thead>
					<tr>
						<th style="display:none;">X</th>
						<th style="display:none;">X</th>
						<th>Product Name</th>
						<th>HCID Product Code</th>
						<th>Valid From</th>
						<th>Valid To</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="agreementProduct" items="${productMapping}">
						<tr>
							<td style="display:none;">${agreementProduct.idAgreement}</td>
							<td style="display:none;">${agreementProduct.id}</td>
							<td>${agreementProduct.bankProductCode}</td>
							<td>${agreementProduct.codeProduct}</td>
							<td>
								<c:set var="validFrom" value="${agreementProduct.createdBy}"/>	
								<c:set var="dateFrom" value="${fn:substring(validFrom,0,10)}"/>
								<c:out value="${dateFrom}"/>
							</td>
							<td>
								<c:set var="validTo" value="${agreementProduct.createdBy}"/>
								<c:set var="dateTo" value="${fn:substring(validTo,11,21)}"/>
								<c:out value="${dateTo}"/>
							</td>
							<td>${agreementProduct.updatedBy}</td>
							<td>
								<c:set var="status" scope="session" value="${agreementProduct.updatedBy}"/>
								<c:if test="${status=='ACTIVE'}">
									<button class="btn btn-red" onClick="deactivation('${agreementProduct.id}')">Deactive</button>
								</c:if>
								<c:if test="${status=='INACTIVE'}">
									<button class="btn btn-blue" onClick="reactivation('${agreementProduct.id}')">Reactive</button>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</fieldset>
			<div class="modal fade" id="productModal" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Select Partner</h4>
						</div>
						<div class="modal-body">
							<table id="tabless" class="table-jfs display" width="100%">
								<thead>
									<tr>
										<th>Product Code</th>
										<th>Product Name</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="product" items="${product}">
										<tr>
											<td>${product.codeProduct}</td>
											<td>${product.nameProduct}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="addAgreementProductMapping" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">X</button>
							<h4 id="title" class="modal-title">Add Product Mapping</h4>
						</div>
						<div class="modal-body">
							<fieldset class="the-fieldset">
							<legend class="the-legend">Form Product Mapping</legend>
							<form id="agreementProductMapping" name="agreementProductMapping">
								<div class="form-group">
									<label class="col-md-4">Product Name</label>
									<input type="text" id="productName" name="productName" class="form-control" placeholder="Product Name" data-toggle="modal" data-target="#productModal"/>
								</div>
								<div class="form-group">
									<label class="col-md-4">HCI Product Code</label>
									<input type="text" id="productCode" name="productCode" class="form-control" placeholder="Product Code"/>
								</div>
								<div class="form-group">
									<label class="col-md-4">Valid From</label>
									<input type="text" id="validFrom" name="validFrom" class="form-control" placeholder="Valid From"/>
								</div>
								<div class="form-group">
									<label class="col-md-4">Valid To</label>
									<input type="text" id="validTo" name="validTo" class="form-control" placeholder="Valid To"/>
								</div>
								<div class="text-right">
									<br>
									<button type="button" id="cancelBtn" name="cancelBtn" class="btn btn-red">Cancel</button>
									<button type="button" id="saveBtn" name="saveBtn" onClick="save()" class="btn btn-blue">Save</button>
								</div>
							</form>
							</fieldset>
						</div>
						<div class="modal-footer"></div>
					</div>
				</div>
			</div>
		<jsp:include page="fragment/footer.jsp"/>
		<script type="text/javascript">
			t.order([4,'asc']).draw();
			var data = {};
			var typeProcess = "";
			$("#productModal").on("shown.bs.modal",function(){
				$('#addAgreementProductMapping').modal('hide');
			});
			$("#productModal").on("hidden.bs.modal",function(){
				$('#addAgreementProductMapping').modal('show');
			});
			$('#add').on('click',function(){
				$("form")[0].reset();
				$('#addAgreementProductMapping').modal('show');	
				$("#validFrom").datepicker();
				$("#validTo").datepicker();
				$('#cancelBtn').hide();
				$('#productName').keypress(function(event){
					event.preventDefault();
					return false;
				});
			});
			$('#cancelBtn').on('click',function(){
				$('#cancelBtn').hide();
				$("#agreementProductMapping :input:text").attr("disabled",false);
				$('#saveBtn').text('Save');
			});
			function save(){
				$("#add").prop("disabled",true);
				var productCode = $("#productCode").val();
				var productName = $("#productName").val();
				var validFrom = $("#validFrom").val();
				var validTo = $("#validTo").val();
				if(productCode=="" || productCode==null){
					$("#info").html("Please Insert Product Code");
					$('#myModal').modal('show');
				}else if(productName=="" || productName==null){
					$("#info").html("Please Insert Product Name");
					$('#myModal').modal('show');
				}else if(validFrom=="" || validFrom==null){
					$("#info").html("Please Insert Date Valid From");
					$('#myModal').modal('show');
				}else if(validTo=="" || validTo==null){
					$("#info").html("Please Insert Date Valid To");
					$('#myModal').modal('show');
				}else{
					if($('#saveBtn').text()=='Save Now'){
						var data = getData();
						postData(data,'/cofin-ui/agreement/product/save',"Save Data");
					}
					$("#agreementProductMapping :input:text").attr("disabled",true);
					$('#title').text('Are You Sure Want To Save ?');
					$('#saveBtn').text('Save Now');
					$('#cancelBtn').show();
				}
			}
			function deactivation(id){
				data["id"] = id;
				typeProcess = "Deactive";
				$("#infoConfirmation").html("Are You Sure Want To Deactive Data?");
				$('#myModalConfirmation').modal('show');
			}
			function reactivation(id){
				data["id"] = id;
				typeProcess = "Reactive";
				$("#infoConfirmation").html("Are You Sure Want To Reactive Data?");
				$('#myModalConfirmation').modal('show');
			}
			$('#ok').on('click',function(){
				$('#myModalConfirmation').modal('hide');
				data["status"] = typeProcess;
				postData(data,'/cofin-ui/agreement/product/status',typeProcess+" Data");
			});
			function getData(){
				var data = {};
				data["productCode"] = $("#productCode").val();
				data["validFrom"] = $("#validFrom").val();
				data["validTo"] = $("#validTo").val();
				data["agreementId"] ='${agreementId}';
				return data;
			}
		</script>
	</body>
</html>
