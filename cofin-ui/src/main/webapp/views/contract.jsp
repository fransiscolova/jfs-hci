<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="breadcrumb" scope="session" value="Detail" />
<jsp:include page="fragment/header.jsp" />
<jsp:include page="fragment/menu.jsp" />
<!-- top navigation -->
<div class="top_nav">
	<div style="background-color: #CB0D31" class="nav_menu">
		<nav>
			<!--  
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>-->
			<ul class="nav navbar-nav navbar-right">
				<li role="presentation" class="dropdown"><a href="javascript:;"
					class="info-number"> <i class="fa fa-sign-out"></i>
				</a></li>

			</ul>
		</nav>
	</div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col main-content" role="main">
	<div class="row">
		<ol class="breadcrumb">
			<li class="active"><a href="/cofin-ui/Contract/"
				class="Contract"><< Contract</a></li>
		</ol>
		<div class="form-creation">
			<fieldset class="the-fieldset">
				<legend class="the-legend">Search Contract</legend>
				<form class="form-inline">
					<div class="form-group">
						<div class="form-group">
							<label class="col-md-2">Contract Code</label>
							<div class="col-md-3">
								<input size="33" type="text" id="contractCode" maxlength="10"
									name="contractCode" value="" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2">Status</label>
							<div class="col-md-3">
								<select style="width:150px;border-radius:6px;" class="form-control" id="status"
									name="status">
									<option value="W">Select Status</option>
									<c:forEach items="${enumStatusContracts}" var="statusContract">
										<option value="${statusContract.key}">${statusContract.value}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<!--
				<div class="form-group">
					<label class="col-md-2">JFS Status</label>
					<div class="col-md-3">
						<select style="width:150px;border-radius:6px;" class="form-control" id="jfsStatus" name="jfsStatus">
						<option value="">Select JFS Status</option>
							<option value="JFS">JFS</option>
							<option value="JFS">Non-JFS</option>
						</select>
					</div>
				</div>
-->
					<div class="form-group">
						<label class="col-md-2">Partner</label>

						<div class="col-md-3">
							<select style="width:292px;border-radius:6px;" class="form-control" id="partner"
								name="partner">
								<option value="">Select partner</option>
								<c:forEach items="${partners}" var="partner">
									<option value="${partner.id}">${partner.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-2">Agreement</label>
						<div class="col-md-9">
							<select class="form-control" id="agreement" style="border-radius:6px;" name="agreement">
								<option value="">Select Agreement</option>
								<c:forEach items="${agreements}" var="agreement">
									<option value="${agreement.id}">${agreement.name}</option>
								</c:forEach>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="col-md-2">Creation Date From</label>
						<div class="col-md-1">
							<input size="7" value="" style="width:100px;" type="text" id="creationDateFrom"
								name="creationDateFrom" class="form-control">
						</div>


						<label style="text-align: center" class="col-md-1">Creation Date To</label>
						<div class="col-md-1">
							<input size="7" value="" style="width:100px;" type="text" id="creationDateTo"
								name="creationDateTo" class="form-control">
						</div>
					</div>
					<!--
				<div class="form-group">
					<label class="col-md-2">JFS Registration Date</label>
					<div class="col-md-1">
						<input size="7" value="" style="width:100px;" type="text" id="registrationDateFrom"
							name="" id="registrationDateFrom" class="form-control">
					</div>


					<label style="text-align: center" class="col-md-1">To</label>
					<div class="col-md-1">
						<input size="7" value="" style="width:100px;" type="text" id="registrationDateTo"
							name="registrationDateTo" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2">JFS Disbursement Date</label>
					<div class="col-md-1">
						<input size="7" value="" type="text" style="width:100px;" id="disbursementDateFrom"
							name="disbursementDateFrom" class="form-control">
					</div>


					<label style="text-align: center" class="col-md-1">To</label>
					<div class="col-md-1">
						<input size="7" value="" style="width:100px;" type="text" id="disbursementDateTo"
							name="disbursementDateTo" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2">JFS Write-off Date</label>
					<div class="col-md-1">
						<input size="7" value="" style="width:100px;" type="text" id="writeOffDateFrom"
							name="writeOffDateFrom" class="form-control">
					</div>

					<label style="text-align: center" class="col-md-1">To</label>
					<div class="col-md-1">
						<input size="7" value="" type="text" style="width:100px;" id="writeOffDateTo"
							name="writeOffDateTo" class="form-control">
					</div>
				</div>
-->
					<div class="form-group">
						<label class="col-md-2">JFS Clawback Date</label>
						<div class="col-md-1">
							<input size="7" value="" type="text" id="clawBackDateFrom"
								name="clawBackDateFrom" style="width:100px;" class="form-control">
						</div>
						<label style="text-align: center" class="col-md-1">FS Clawback To</label>
						<div class="col-md-1">
							<input size="7" value="" style="width:100px;" type="text" id="clawBackDateTo"
								name="clawBackDateTo" class="form-control">
						</div>
					</div>
					<!--
				<div class="form-group">
					<label class="col-md-2">ET Request Date</label>
					<div class="col-md-1">
						<input size="7" value="" type="text" id="requestDateFrom"
							name="requestDateFrom" style="width:100px;" class="form-control">
					</div>
					<label style="text-align: center" class="col-md-1">To</label>
					<div class="col-md-1">
						<input size="7" value="" type="text" style="width:100px;" id="requestDateTo"
							name="requestDateTo" class="form-control">
					</div>
				</div>
				-->
					<div class="text-right">
						<br>
						<button type="button" id="resetBtn" name="resetBtn"
							onclick="javascript:window.location.replace('/cofin-ui/contract');"
							class="btn btn-red">Reset</button>

						<button type="button" id="searchBtn" name="searchBtn"
							class="btn btn-blue">Search</button>

					</div>

				</form>
			</fieldset>
			<br>
		</div>
		<div class="table-responsive">

			<table id="table" class="table-jfs display" cellspacing="0"
				width="100%">
				<div id="feedback"></div>
				<thead width="100%">
					<tr>
						<th>Contract Code</th>
						<th>Status</th>
						<th>JFS Status</th>
						<th>Creation Date</th>
						<th>Agreement</th>
						<th>Registration Date</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ContractDetail" items="${Contract.ContractDetails}">
						<tr>
							<td>${ContractDetail.interestRate}</td>
							<td>${ContractDetail.principalSplitRate}</td>
							<td>${ContractDetail.adminFeeRate}</td>
							<td>${ContractDetail.penaltySplitRate}</td>
							<td>${ContractDetail.validFrom}</td>
							<td>${ContractDetail.validTo}</td>
							<td><c:set var="deleted" scope="session"
									value="${ContractDetail.deleted}" /> <c:if
									test="${deleted=='N'}">
									<c:out value="ACTIVE" />
								</c:if> <c:if test="${deleted=='Y'}">
									<c:out value="INACTIVE" />
								</c:if></td>

							<td>
								<input type="button" class="btn btn-blue">View</button>

							</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>




			<div class="modal fade" id="addContractDetail" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">X</button>
							<h4 id="title" class="modal-title">Add Contract Detail</h4>
						</div>
						<div class="modal-body">



							<form id="ContractDetail" name="ContractDetail">

								<div class="form-group">
									<label class="col-md-4">Interest Rate</label> <input
										type="text" id="interestRate" name="interestRate"
										class="form-control" placeholder="Interest Rate" maxlength='2'
										pattern='^[0-9]$'
										onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>

								<div class="form-group">
									<label class="col-md-4">Principal Split Rate</label> <input
										type="text" id="principalSplitRate" name="principalSplitRate"
										class="form-control" placeholder="Principal split rate"
										maxlength='2' pattern='^[0-9]$'
										onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>
								<div class="form-group">
									<label class="col-md-4">Admin Fee Rate</label> <input
										type="text" name="adminFeeRate" id="adminFeeRate"
										class="form-control" placeholder="Admin Fee rate"
										onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>


								<div class="form-group">
									<label class="col-md-4">Penalty Split Rate</label> <input
										type="text" data-toggle="modal" data-target="#partnerModal"
										name="penaltySplitRate" id="penaltySplitRate"
										class="form-control" placeholder="Penalty split rate"
										maxlength='2' pattern='^[0-9]$'
										onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								</div>


								<div class="form-group">
									<label class="col-md-4">Valid From</label> <input type="text"
										name="validFrom" id="validFrom" class="form-control"
										placeholder="Valid From">
								</div>

								<div>
									<label class="col-md-4">Valid To</label> <input type="text"
										name="validTo" id="validTo" class="form-control"
										placeholder="Valid To">
								</div>

								<div class="text-right">
									<br>
									<button type="button" id="cancelBtn" name="cancelBtn"
										class="btn btn-red">Cancel</button>

									<button type="button" onClick="save()" id="saveBtn"
										name="saveBtn" class="btn btn-blue">Save</button>


								</div>


							</form>




						</div>
						<div class="modal-footer"></div>
					</div>

				</div>
			</div>


			<jsp:include page="fragment/footer.jsp" />




			<script type="text/javascript">
				t.order([ 5, 'asc' ]).draw();
				var typeProcess = '';
				$('#add').hide();
				$("#creationDateFrom").datepicker();
				$("#creationDateTo").datepicker();

				$("#registrationDateFrom").datepicker();
				$("#registrationDateTo").datepicker();

				$("#disbursementDateFrom").datepicker();
				$("#disbursementDateTo").datepicker();

				$("#writeOffDateFrom").datepicker();
				$("#writeOffDateTo").datepicker();

				$("#clawBackDateFrom").datepicker();
				$("#clawBackDateTo").datepicker();

				$("#requestDateFrom").datepicker();
				$("#requestDateTo").datepicker();

				$('#searchBtn').on('click', function() {

					search(getData(), '/cofin-ui/contract/inquiry', ' search');

				});

				function getData() {
					var dateFilter={};
					dateFilter['clawBackDateFrom'] = $('#clawBackDateFrom').val();
					dateFilter['clawBackDateTo'] = $('#clawBackDateTo').val();
					dateFilter['creationDateFrom']=$('#creationDateFrom').val();
				    dateFilter['creationDateTo']=$('#creationDateTo').val();
					
					var data = {};
					data['agreementId'] = $('#agreement').val();
					data['partnerId'] = $('#partner').val();
					data['contractCode'] = $('#contractCode').val();
					data['status'] = $('#status').val();				
					data['dateFilter']=dateFilter;
					return data;
				}
			</script>
			</body>
</html>
