<%@ page language="java" isErrorPage="true"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML>
<html>
<c:set var="baseUrl" scope="session" value="${pageContext.request.contextPath}" />
<head>
<title>Error-404</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="${baseUrl}/lib/static/assets/css/error/css/style.css">
</head>
<body>
	<div class="wrap">
	<div id="erorrBanner">
		<h1>Error : <b>${pageContext.errorData.statusCode}</b> </h1>
	</div>	
		<div class="banner">
			<img src="${baseUrl}/lib/static/assets/css/error/images/banner.png" alt="" />
		</div>
		<div class="page">
			<div class="msg">
				<c:set var="statusCode" scope="session" value="${pageContext.errorData.statusCode}" />
				<c:if test="${statusCode=='400'}">
					<c:out value="Dude,Bad Request !" />
				</c:if>
				<c:if test="${statusCode=='401'}">
					<c:out value="Dude, Unauthorized Request !" />
				</c:if>
				<c:if test="${statusCode=='403'}">
					<c:out value="The Request Was Valid, But The Server Is Refusing Action !" />
				</c:if>
				<c:if test="${statusCode=='404'}">
					<c:out value="We Can't Find That Page !" />
				</c:if>
				<c:if test="${statusCode=='500'}">
					<c:out value="Ooops, There Is Internal Server Error! If This Happens Again Please Contact Our Admin." />
				</c:if>
				<c:if test="${statusCode=='503'}">
					<c:out value="Our Server Is Currently Available (Because It Is Overloaded Or Down For Maintenance)! Please Try Again Later." />
				</c:if>
			</div>
			<h3>
				<a href="#" onclick="history.back()"><< Back To Previous Page</a>
			</h3>
		</div>
		<div class="footer">
			<p>Home Credit Indonesia</a></p>
		</div>
	</div>
</body>
</html>

