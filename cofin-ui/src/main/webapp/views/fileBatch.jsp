<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="baseUrl" scope="session" value=".." />
<c:set var="breadcrumb" scope="session" value="Batch" />

<jsp:include page="fragment/header.jsp" />
<jsp:include page="fragment/menu.jsp" />
<jsp:include page="fragment/topnavigation.jsp" />


<div class="table-responsive">


	<fieldset class="the-fieldset">
		<legend class="the-legend"><< <a href="#" onClick="goBack()">Back </a>| Generate File</legend>
		<table id="table" class="table-jfs display" cellspacing="0"
			width="100%">
			<div id="feedback"></div>
			<thead>
				<tr>
					<th>Created By</th>
					<th>Created Date</th>
					<th>File Path</th>
					<th>Download</th>

				</tr>
			</thead>

			<tbody>
				<c:forEach var="fileBatch" items="${fileBatch}">
					<tr>
						<td>${fileBatch.createdBy}</td>
						<td>${fileBatch.createdDate}</td>
						<td>${fileBatch.filename}</td>
						<td>
				<button class="btn btn-approve"
								onClick="location.href = 'download/${fileBatch.id}';">Download</button>
								
						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</fieldset>
	<div class="modal fade" id="addBatch" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">X</button>
					<h4 id="title" class="modal-title">Update Cron Time</h4>
				</div>

				<div class="modal-body">
					<fieldset class="the-fieldset">
						<legend class="the-legend">Form Cron Time</legend>
						<form id="Batch" name="Batch">
							<div class="form-group">
								<label class="col-md-4">Partner Name</label> <input type="text"
									id="partner" name="partner" class="form-control"
									placeholder="partner name" disabled>
							</div>

							<div class="form-group">
								<label class="col-md-4">Cron Expresion</label> <input
									type="text" id="expresion" name="expresion"
									class="form-control" placeholder="expresion" disabled>
							</div>





							<div class="form-group">
								<input type="checkbox" class="chooseCron" name="chooseCron"
									id="dayCount" value="dayCount">Every <input
									style="width: 30px" type="text" id="daily"> days<br>
								<input type="checkbox" class="chooseCron" name="chooseCron"
									value="weekday" id="weekday"> Every Week Day<br>
							</div>


							<div class="form-group">
								<label class="col-md-2">Start TIme</label> <select id="hours">
									<option>00</option>
								</select> <select id="minute">
									<option>00</option>
								</select>
							</div>


							<div class="form-group">
								<button type="button" id="generateCron" name="generateCron"
									class="btn btn-approve" onClick="everyDay()">Generate
									Cron</button>
							</div>



							<div class="text-right">
								<button type="button" id="cancelBtn" name="cancelBtn"
									class="btn btn-red">Cancel</button>

								<button type="button" onClick="save()" id="saveBtn"
									name="saveBtn" class="btn btn-blue">Save</button>
							</div>


						</form>

					</fieldset>

				</div>

				<div class="modal-footer"></div>
			</div>
		</div>
	</div>

	<jsp:include page="fragment/footer.jsp" />

	<script type="text/javascript">
		var Id = "";
		var select = document.getElementById("hours");

		for (var i = 0; i < 24; i++) {
			var opt = "";

			if (i < 10) {
				opt = "0" + i;
			} else {
				opt = i;
			}

			var el = document.createElement("option");
			el.textContent = opt;
			el.value = opt;
			select.appendChild(el);

		}

		var selectOp = document.getElementById("minute");

		for (var i = 0; i <= 60; i++) {
			var opt = "";

			if (i < 10) {
				opt = "0" + i;
			} else {
				opt = i;
			}

			var el = document.createElement("option");
			el.textContent = opt;
			el.value = opt;
			selectOp.appendChild(el);
			select.value = "12";
		}

		$('#add').hide();
		$('input.chooseCron').on('change', function() {
			$('input.chooseCron').not(this).prop('checked', false);
		});

		t.order([ 3, 'asc' ]).draw();
		var typeProcess = '';

		$('#add').on('click', function() {
			typeProcess = 'Save';
			$("form")[0].reset();
			$('#title').text(typeProcess + " Menu");
			$('#addMenu').modal('show');
			$('#cancelBtn').hide();

		});

		$('#ok').on(
				'click',
				function() {
					$('#myModalConfirmation').modal('hide');
					postData(getData(), '/cofin-ui/Batch/activation',
							typeProcess + " data");
				});

		$('#cancelBtn').on('click', function() {
			$("#cancelBtn").hide();
			$('#title').text('Cancel ' + typeProcess + '!');
			$("#Batch :input:text").attr("disabled", false);
			$('#saveBtn').text('Save');

		});

		function save() {

			$("#add").prop("disabled", true);
			if ($("#url").val() == '' || $("#name").val() == '') {
				$("#info").html("Complete the form before save!");
			} else {
				if ($('#saveBtn').text() == 'Save Now'
						|| $('#saveBtn').text() == 'Update Now') {
					var data = getData();

					if (typeProcess == 'Update') {

						postData(data, '/cofin-ui/batch/update', typeProcess
								+ " data");
					} else {

						postData(data, '/cofin-ui/menu/save', typeProcess
								+ " data");

					}
				}

				$("#menu :input:text").attr("disabled", true);
				$('#title').text('Are you sure want to ' + typeProcess + '?');
				$('#saveBtn').text(typeProcess + " Now");
				$('#cancelBtn').show();

			}
		}

		function updateCron(idRow, partner, expresion, status) {

			Id = idRow;
			typeProcess = 'Update';
			$('#partner').val(partner);
			$('#expresion').val(expresion);
			$('#addBatch').modal('show');
			$('#cancelBtn').hide();
			$('#saveBtn').text(typeProcess);
			$('#title').text(typeProcess + " Batch");

			if ($('#expresion').val().indexOf("MON-FRI") !== -1) {

				document.getElementById("weekday").checked = true;
				document.getElementById("dayCount").checked = false;
			}

			if ($('#expresion').val().indexOf("* ?") !== -1) {

				document.getElementById("dayCount").checked = true;
				document.getElementById("weekday").checked = false;
			}

		}

		function update(idRow, flag) {

			var data = {};
			Id = idRow;
			data["id"] = Id;
			if (flag == 'N') {
				data["flag"] = 'Y';
				typeProcess = 'Rerun batch';
			} else {
				data["flag"] = 'N';
				typeProcess = 'Stop batch';
			}
			postData(data, '/cofin-ui/batch/stop-generate', typeProcess);
		}

		function getData() {
			var data = {};
			data["expresion"] = $("#expresion").val();
			data["id"] = Id;
			return data;
		}

		function everyDay() {
			tmp = document.getElementById("expresion").value;

			var hour = document.getElementById("hours").value;
			var minute = document.getElementById("minute").value;
			var days = document.getElementById("daily").value;
			//x.charAt(0)

			if (minute.charAt(0) == 0) {
				minute = minute.replace("0", "");
			}

			if (hour.charAt(0) == 0) {
				hour = hour.replace("0", "");
			}

			var daily = document.getElementById("dayCount").checked;

			if (daily) {
				document.getElementById("expresion").value = "0 " + minute
						+ " " + hour + " 1/" + days + " * ? *";
			} else {

				document.getElementById("expresion").value = "0 " + minute
						+ " " + hour + " ? * MON-FRI *";
			}

		}
	</script>
	</body>
</html>
