<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="button-creation text-right">
	<button id="add" class="btn btn-blue">Add New ${breadcrumb}</button>
	<button id="addDwh" style="display:none;" class="btn btn-blue">Add ${breadcrumb} DWH</button>
</div>
</div>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Information</h4>
			</div>
			<div class="modal-body">
				<p id="info">.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModalConfirmation" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Information</h4>
			</div>
			<div class="modal-body">
				<p id="infoConfirmation"></p>
				<div class="text-right">
					<button type="button" id="no" name="no" class="btn btn-red">No</button>
					<button type="button" id="ok" name="ok" class="btn btn-blue">Yes</button>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="LoadingDialog" style="width:500px">
	<img src="${baseUrl}/lib/static/assets/images/PlaneLoading.gif" style="position:relative;width:250px"/>
</div>
<script src="${baseUrl}/lib/static/assets/vendors/jquery/dist/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${baseUrl}/lib/static/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${baseUrl}/lib/static/assets/vendors/iCheck/icheck.min.js"></script>
<script src="${baseUrl}/lib/static/assets/build/js/custom.min.js"></script>
<script src="${baseUrl}/lib/static/assets/js/jquery.dataTables.min.js"></script>
<script src="${baseUrl}/lib/static/assets/js/Util.js"></script>
<script>
var parnerId="";
var ts = $("#tables").DataTable({
	"bPaginate" : false,
	"bLengthChange" : false,
	"bFilter" : true,
	"bInfo" : false,
	 "columnDefs": [
         {
             "targets": [ 0],
             "visible": false
         }
     ]
});
var tss = $("#tabless").DataTable({
	"bPaginate" : true,
	"bLengthChange" : true,
	"bFilter" : true,
	"bInfo" : false
});
var t = $("#table").DataTable({
	"bPaginate" : true,
	"bLengthChange" : true,
	"bFilter" : true,
	"bInfo" : true,
	scrollY : 400,
	scrollX : true
});
var r = $("#xtable").DataTable({
	"bPaginate" : true,
	"bLengthChange" : true,
	"bFilter" : true,
	"bInfo" : true,
	scrollY : 500,
	scrollX : true,
	"order": [[ 0, "asc" ]]
});
var r1 = $("#xtable1").DataTable({
	"bPaginate" : true,
	"bLengthChange" : true,
	"bFilter" : true,
	"bInfo" : true,
	scrollY : 400,
	scrollX : true,
	"order": [[ 0, "asc" ]]
});
var r2 = $("#xtable2").DataTable({
	"bPaginate" : true,
	"bLengthChange" : true,
	"bFilter" : true,
	"bInfo" : true,
	scrollY : 400,
	scrollX : true,
	"order": [[ 0, "asc" ]]
});
var r3 = $("#xtable3").DataTable({
	"bPaginate" : true,
	"bLengthChange" : true,
	"bFilter" : true,
	"bInfo" : true,
	scrollY : 400,
	scrollX : true,
	"order": [[ 0, "asc" ]]
});
var r4 = $("#xtable4").DataTable({
	"bPaginate" : true,
	"bLengthChange" : true,
	"bFilter" : true,
	"bInfo" : true,
	scrollY : 400,
	scrollX : true,
	"order": [[ 0, "asc" ]]
});
var r5 = $("#xtable5").DataTable({
	"bPaginate" : true,
	"bLengthChange" : true,
	"bFilter" : true,
	"bInfo" : true,
	scrollY : 400,
	scrollX : true,
	"order": [[ 0, "asc" ]]
});
	
$(document).ready(function(){
	var data = {};
	var role='${pageContext.request.userPrincipal.authorities}'.replace("[", "");		
	var agId='${agreementId}';
	data["role"] =role.replace("]", "");
	data['agreementId']=agId;
	loadMenu(data,"/cofin-ui/menu/getAllAction","test");
	$('#LoadingDialog').dialog({
    	autoOpen	: false,
    	title		: 'Loading...',
    	draggable	: false,
    	resizable	: false,
    	modal		: true,
    	open		: function(event,ui){
        	$(".ui-dialog-titlebar-close").hide();
    	}
	});
});
$(function() {
	var table = ts;
	$('#tables tbody').on('click', 'tr', function() {
		var d = table.row( this ).data();
		$('#partnerName').val(d[1]);
		partnerId=d[0];
		$('#partnerModal').modal('hide');
	});
});
$(function() {		
	var table = tss;
	$('#tabless tbody').on('click', 'tr', function() {
		var d = table.row( this ).data();
		$('#productCode').val(d[0]);
		$('#productName').val(d[1]);
		$('#productModal').modal('hide');
	});
});
function showPleaseWait() {
    var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false role="dialog">\
        <div class="modal-dialog">\
            <div class="modal-content">\
                <div class="modal-header">\
                    <h4 class="modal-title">Please wait...</h4>\
                </div>\
                <div class="modal-body">\
                    <div class="progress">\
                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                      aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                      </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>';
    $(document.body).append(modalLoading);
    $("#pleaseWaitDialog").modal("show");
}
$('#no').on('click', function() {
	$('#myModalConfirmation').modal('hide');
});
$('#myModal').on('hidden.bs.modal',function(){
	var msg = $("#info").html();
	if(msg.substring(0,7) == "Success"){
		location.reload();
	}else{
		$('#myModal').modal('hide');
	}
});
</script>