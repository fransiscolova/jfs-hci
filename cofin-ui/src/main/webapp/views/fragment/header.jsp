<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="baseUrl" scope="session" value="${pageContext.request.contextPath}" />
<c:set var="roleUserMap" scope="session" value="${pageContext.request.userPrincipal.authorities}" />
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="${baseUrl}/lib/static/assets/images/icon_hci.jpg" type="image/png" sizes="64x64">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<title>JFS Cofin</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- Bootstrap -->
<link href="${baseUrl}/lib/static/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="${baseUrl}/lib/static/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- iCheck -->
<link href="${baseUrl}/lib/static/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="${baseUrl}/lib/static/assets/build/css/custom.min.css" rel="stylesheet">
<link href="${baseUrl}/lib/static/assets/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="${baseUrl}/lib/static/assets/css/style.css" rel="stylesheet">
<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



</head>
<body class="nav-md body-jfs">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border:0;">
						<a href="/cofin-ui/home/" class="site_title"><img src="${baseUrl}/lib/static/assets/images/small_logo.jpg" class="img-responsive"></a>
					</div>
					<div class="clearfix"></div>
					<div class="side-profile">
						<div class="profile profile_info">
							<h2><div style="font-size:30px;color:yellow;">JFS Cofin</div></h2>
							<span style="color:white;">${pageContext.request.userPrincipal.name}</span><br>
							<span style="color:white;">${pageContext.request.userPrincipal.authorities}</span><br>
						</div>
					</div>