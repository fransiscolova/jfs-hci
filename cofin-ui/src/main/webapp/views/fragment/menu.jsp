<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<ul class="nav side-menu">
			<li><a href="${baseUrl}/home" class="home">Home</a></li>
			<li id="Partner" style="display:block"><a href="${baseUrl}/partner/" class="partner">Partner</a></li>
			<li id="Agreement" style="display:block"><a href="${baseUrl}/agreement/">Agreement and Mapping<span class="fa fa-chevron-right"></span></a>
				<ul id="Agreement_Detail" class="nav child_menu">
					<li><a href="/cofin-ui/agreement/detail/${agreementId}" class="agreement">Agreement Detail</a></li>
					<li><a href="/cofin-ui/agreement/product/${agreementId}" class="agreement">Product Mapping</a></li>
				</ul>
			</li>
			<li id="Contract" style="display:block"><a href="${baseUrl}/contract" class="contract">Contract</a></li>
			<li id="Product" style="display:block"><a href="${baseUrl}/product" class="product">Product</a></li	>
			<li id="Batch_Process" style="display:block"><a href="${baseUrl}/batch/process" class="batch_Process">Batch Process</a></li>
			<li id="Batch_Management" style="display:block"><a href="${baseUrl}/batch/management" class="batch_Management">Batch Management</a></li>
			<li id="User_Management" style="display:block"><a href="${baseUrl}/user/" class="usermanagement">User Management</a></li>
			<li id="Menu_Management" style="display:block"><a href="${baseUrl}/menu" class="menumanagement">Menu Management</a></li>
			<li id="Confirmation" style="display:block"><a href="${baseUrl}/upload/file" class="confirmation">Upload File Confirmation</a></li>
			<li id="Role_Management" style="display:block"><a href="${baseUrl}/role/" class="rolemanagement">Role Management</a></li>
			<li style="display:none;"><a href="${baseUrl}/savelogout" class="logout">Logout</a></li>
		</ul>
	</div>
</div>
</div>
</div>
</html>