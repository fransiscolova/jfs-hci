<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- top navigation -->
<div class="top_nav">
	<div style="background-color:#CB0D31" class="nav_menu">
		<nav>
			<div class="nav toggle">
			<!--
				<a id="menu_toggle">
				<i class="fa fa-bars"></i>
				</a>
				-->
			</div>
			<ul  class="nav navbar-nav navbar-right">
				<li role="presentation" class="dropdown"><a href="javascript:;" class="info-number"> 
				<i class="fa fa-sign-out"></i>
				</a></li>
			</ul>
		</nav>
	</div>
</div>
<!-- /top navigation -->
		<!-- page content -->
			<div class="right_col main-content"  role="main">
				<div  class="row">
				<!--
					<ol class="breadcrumb">
						<li class="active">${breadcrumb}</li>
					</ol> -->