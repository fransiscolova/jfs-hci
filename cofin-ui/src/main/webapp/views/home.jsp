<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="breadcrumb" scope="session" value="Partner"/> 
<script>
window.onload = function(){
var chart = new CanvasJS.Chart("chartContainer",{
	animationEnabled: true,
	title: {
		text: "Home Credit Indonesia - 2017"
	},
	data: [{
		type: "pie",
		startAngle: 240,
		yValueFormatString: "##0.00\"%\"",
		indexLabel: "{label} {y}",
		dataPoints: [
			{y: 50, label: "Credit"},
			{y: 25, label: "Flexy Fast"},
			{y: 25, label: "Others"}
		]
	}]
});
chart.render();
}
</script>
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<jsp:include page="fragment/topnavigation.jsp"/>
<div class="table-responsive" style="text-align:center;height:100%">
<h1 class="w3-container w3-center w3-animate-top">Welcome To JFS- Cofin</h1>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="${baseUrl}/lib/static/assets/js/canvasjs.min.js"></script>
<jsp:include page="fragment/footer.jsp"/>
<script type="text/javascript">
$('#add').hide();
</script>
</body>
</html>
