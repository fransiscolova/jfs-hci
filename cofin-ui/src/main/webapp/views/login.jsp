<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="${baseUrl}/lib/static/assets/images/icon_hci.jpg" type="image/png" sizes="32x32">
<c:set var="baseUrl" scope="session" value="/cofin-ui" />
<title>JFS Cofin Login</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="${baseUrl}/lib/static/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="${baseUrl}/lib/static/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="${baseUrl}/lib/static/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<link href="${baseUrl}/lib/static/assets/build/css/custom.min.css" rel="stylesheet">
<link href="${baseUrl}/lib/static/assets/css/jquery.dataTables.min.css"	rel="stylesheet">
<link href="${baseUrl}/lib/static/assets/css/style.css" rel="stylesheet">
</head>
<body class="body-login">
	<div class="login-area clearfix">
		<div class="box-login clear-float">
			<div class="grid-size">
				<div class="product-logo"></div>
				<div class="box-content clear-float">
					<p style="color:#363636;font-style:bold;font-size:40px;">COFIN Login</p>
					<form action="<c:url value='j_spring_security_check'/>" method="POST">
						<div class="form-group">
							<i class="fa fa-user"></i> 
							<input type="text" id="username" name="username" class="form-control" placeholder="User ID">
						</div>
						<div class="form-group">
							<i class="fa fa-lock"></i> 
							<input type="password" id="password" name="password" class="form-control" placeholder="Password">
						</div>
						<div class="text-right">
							<button type="submit" class="button-primary">Sign In</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="loginModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div style="background-color:#CB0D31;font-color:white" class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h2 class="modal-title">Login Information</h2>
					</div>
					<div class="modal-body">
						<p id="info" style="font-size:15px">.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="${baseUrl}/lib/static/assets/vendors/jquery/dist/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="${baseUrl}/lib/static/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="${baseUrl}/lib/static/assets/vendors/iCheck/icheck.min.js"></script>
	<script src="${baseUrl}/lib/static/assets/build/js/custom.min.js"></script>
	<script src="${baseUrl}/lib/static/assets/js/jquery.dataTables.min.js"></script>
	<script src="${baseUrl}/lib/static/assets/js/Util.js"></script>
	<script type="text/javascript"></script>
	<c:choose>
		<c:when test="${not empty error}">
			<script>
				$("#info").html('${error}');
				$('#loginModal').modal('show');
			</script>
		</c:when>
		<c:when test="${not empty msg}">
			<script>
				$("#info").html('${msg}');
				$('#loginModal').modal('show');
			</script>
		</c:when>
	</c:choose>
</body>
</html>
