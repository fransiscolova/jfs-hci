<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="baseUrl" scope="session" value=".." />
<c:set var="breadcrumb" scope="session" value="Menu" />
<jsp:include page="fragment/header.jsp" />
<jsp:include page="fragment/menu.jsp" />
<jsp:include page="fragment/topnavigation.jsp" />
<div class="table-responsive">
<fieldset class="the-fieldset">
	<legend class="the-legend">Menu</legend>
	<table id="table" class="table-jfs display" cellspacing="0"
		width="100%">
		<div id="feedback"></div>
		<thead>
			<tr>
				<th>Menu Url</th>
				<th>Name</th>				
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="action" items="${actions}">
				<tr>
					<td>${action.action}</td>
					<td>${action.name}</td>
					<td><c:set var="deleted" scope="session" value="${action.deleted}"/> 
						<c:if test="${deleted=='N'}">
							<c:out value="ACTIVE"/>
						</c:if> 
						<c:if test="${deleted=='Y'}">
							<c:out value="INACTIVE"/>
						</c:if>
					</td>
					<td>
						<button class="btn btn-approve"	onClick="location.href = 'menu/${action.name}';">View</button>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</fieldset>
	<div class="modal fade" id="addMenu" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">X</button>
					<h4 id="title" class="modal-title">Add Menu</h4>
				</div>
				<div class="modal-body">
					<fieldset class="the-fieldset">
						<legend class="the-legend">Form Add Menu</legend>
						<form id="user" name="user">
							<div class="form-group">
								<label class="col-md-4">URL</label> <input type="text"
									id="url" name="url" class="form-control"
									placeholder="name">
							</div>
							<div class="form-group">
								<label class="col-md-4">Name</label> <input type="text"
									id="name" name="Name" class="form-control"
									placeholder="name">
							</div>
							<div class="text-right">
								<button type="button" id="cancelBtn" name="cancelBtn"
									class="btn btn-red">Cancel</button>
								<button type="button" id="deactiveBtn" name="deactiveBtn"
									class="btn btn-red">Deactive</button>
								<button type="button" onClick="save()" id="saveBtn"
									name="saveBtn" class="btn btn-blue">Save</button>
							</div>


						</form>

					</fieldset>



				</div>

				<div class="modal-footer"></div>
			</div>
		</div>
	</div>

	<jsp:include page="fragment/footer.jsp" />

	<script type="text/javascript">
		t.order([ 3, 'asc' ]).draw();
		var typeProcess = '';
		$('#add').hide();

		$('#deactiveBtn').on('click', function() {
			$('#infoConfirmation').text("Are you sure want to deactive?");
			$('#myModalConfirmation').modal('show');
		});

		$('#ok').on(
				'click',
				function() {
					$('#myModalConfirmation').modal('hide');
					postData(getData(), '/cofin-ui/user/activation',
							typeProcess + " data");
				});

		$('#cancelBtn').on('click', function() {
			$("#cancelBtn").hide();
			$('#title').text('Cancel ' + typeProcess + '!');
			$("#user :input:text").attr("disabled", false);
			$('#saveBtn').text('Save');
			if (typeProcess == 'Save') {
				$('#deactiveBtn').hide();
			} else {
				$('#deactiveBtn').show();
			}
			$("#url").removeAttr("disabled");
			$("#name").removeAttr("disabled");
		});

		function save() {

			$("#add").prop("disabled", true);
			if ($("#url").val() == '' || $("#name").val() == '') {
				$("#info").html("Complete the form before save!");
			} else {
				if ($('#saveBtn').text() == 'Save Now' || $('#saveBtn').text() == 'Update Now') {
					var data = getData();
					if (typeProcess == 'Update') {
						postData(data, '/cofin-ui/user/update', typeProcess + " data");
					} else {
						postData(data, '/cofin-ui/menu/save', typeProcess + " data");
					}
				}
				$("#url").prop('disabled','disabled');
				$("#name").prop('disabled','disabled');
				$("#menu :input:text").attr("disabled", true);
				$('#title').text('Are you sure want to ' + typeProcess + '?');
				$('#saveBtn').text(typeProcess + " Now");
				$('#cancelBtn').show();
				$('#deactiveBtn').hide();
			}
		}

		function update(userName, firstName, lastName) {
			typeProcess = 'Update';
			$('#username').val(userName);
			$('#firstName').val(firstName);
			$('#lastName').val(lastName);
			$('#addUser').modal('show');
			$('#cancelBtn').hide();
			$('#saveBtn').text(typeProcess);
			$('#title').text(typeProcess + " User");
			$('#deactiveBtn').show();
		}

		function getData() {
			var data = {};
			data["action"] = $("#url").val();
			data["name"] = $("#name").val();
			return data;
		}
	</script>
	</body>
</html>
