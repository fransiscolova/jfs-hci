<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="baseUrl" scope="session" value=".."/>
<c:set var="breadcrumb" scope="session" value="Role"/>
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<jsp:include page="fragment/topnavigation.jsp"/>
<fieldset class="the-fieldset">
<legend class="the-legend">Menu</legend>
<form id="user" name="user">
	<div class="form-group">
		<label class="col-md-2">Name</label>
		<div class="col-md-3">
			<input type="text" value="${action.name}" id="name" name="name" class="form-control" disabled/>
			<input type="hidden" id="bstat" name="bstat" value="${status}"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2">URL</label>
		<div class="col-md-3">
			<input type="text" value="${action.action}" id="url" name="url" class="form-control" disabled/>
			<input type="hidden" id="zid" name="zid"/>
		</div>
	</div>
</form>
</fieldset>
<br>
<div class="table-responsive">
<table id="xtable" class="table-jfs display" cellspacing="0" width="100%">
	<div id="feedback"></div>
	<thead>
		<tr>
			<th>Role</th>
			<th>Valid From</th>
			<th>Valid To</th>
			<th>Status</th>
			<th>Update</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="roleAction" items="${roleActions}">
			<tr>
				<td>${roleAction.role.role}</td>
				<td>${roleAction.validFrom}</td>
				<td>${roleAction.validTo}</td>
				<td>
					<c:set var="deleted" scope="session" value="${roleAction.deleted}"/>
					<c:if test="${deleted=='N'}">
						<c:out value="ACTIVE"/>
					</c:if>
					<c:if test="${deleted=='Y'}">
						<c:out value="INACTIVE"/>
					</c:if>
				</td>
				<td>
					<button class="btn btn-approve" onclick="update('${roleAction.role.role}','${roleAction.validFrom}','${roleAction.validTo}','${roleAction.id}')">Update</button>
				</td>
				<td>
					<c:set var="deleted" scope="session" value="${roleAction.deleted}"/>
					<c:if test="${deleted=='N'}">
						<button class="btn btn-red" style="padding: 5px 25px;margin: 0;" onclick="deactive('${roleAction.id}')">Deactive</button>
					</c:if>
					<c:if test="${deleted=='Y'}">
						<button class="btn btn-blue" style="padding: 5px 25px;margin: 0;" onclick="reactive('${roleAction.id}')">Reactive</button>
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<div class="modal fade" id="addUser" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">X</button>
				<h4 id="title" class="modal-title">Add Role To '${action.name}'</h4>
			</div>
			<div class="modal-body">
				<fieldset class="the-fieldset">
					<legend class="the-legend">Role</legend>
					<form id="userAdd" name="userAdd">
						<div class="form-group">
							<label class="col-md-4">Role</label>
							<select style="border-radius:6px;" class="form-control" id="role" name="role">
								<c:forEach items="${roleUser}" var="role">
									<option value="${role.role}">${role.role}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label class="col-md-4">Valid From</label>
							<input type="text" id="validFrom" name="validFrom" class="form-control" placeholder="Valid From"/>
						</div>
						<div class="form-group">
							<label class="col-md-4">Valid To</label>
							<input type="text" id="validTo" name="validTo" class="form-control" placeholder="Valid To"/>
						</div>
						<div class="text-right">
							<button type="button" id="cancelBtn" name="cancelBtn" class="btn btn-red" onclick="cancel()">Cancel</button>
							<button type="button" id="deactiveBtn" name="deactiveBtn" class="btn btn-red" onclick="deactive()">Deactive</button>
							<button type="button" id="saveBtn" name="saveBtn" class="btn btn-blue" onclick="save()">Save</button>
						</div>
					</form>
				</fieldset>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="updMenu" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">X</button>
				<h4 id="title" class="modal-title">Update Menu</h4>
			</div>
			<div class="modal-body">
				<fieldset class="the-fieldset">
					<legend class="the-legend">Update Menu</legend>
					<form name="updateMenu" id="updateMenu">
						<div class="form-group">
							<label class="col-md-4">Name</label>
							<input type="text" name="xname" id="xname" placeholder="Name" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="col-md-4">URL</label>
							<input type="text" name="xurl" id="xurl" placeholder="URL" class="form-control"/>
						</div>
						<div class="text-right">
							<button type="button" id="btnCancel" name="btnCancel" class="btn btn-red" onclick="batal()">Cancel</button>
							<button type="button" id="btnSave" name="btnSave" class="btn btn-blue" onclick="simpan()">Save</button>
						</div>
					</form>
				</fieldset>
			</div>
		</div>
	</div>
</div>
</div>
<jsp:include page="fragment/footer.jsp"/>
<script type="text/javascript">
	t.order([ 3, 'asc' ]).draw();
	var typeProcess = '';
	var id = "";
	$('#add').on('click',function(){
		typeProcess = 'Save';
		$("form")[0].reset();
		$('#title').text(typeProcess + " Role");
		$('#addUser').modal('show');
		$('#cancelBtn').hide();
		$('#deactiveBtn').hide();
		$('#role').val("");
		$('#validTo').val("");
		$('#validFrom').val("");
		$("#validFrom").datepicker();
		$("#validTo").datepicker();
		$('#saveBtn').text('Save');
	});
	function updMenu(){
		$('#updMenu').modal('show');
		$("#xname").val($("#name").val());
		$("#xurl").val($("#url").val());
		$("#btnCancel").hide();
	}
	function update(role,validFrom,validTo,idRoleMap){
		typeProcess='Update';
		id=idRoleMap;
		$('#role').val(role);
		$('#validTo').val(validTo);
		$('#validFrom').val(validFrom);
		$('#addUser').modal('show');
		$('#cancelBtn').hide();
		$('#saveBtn').text(typeProcess);
		$('#title').text(typeProcess + " User Role");
		$('#deactiveBtn').show();
		formatDate($('#validFrom').val(), $('#validTo').val());
	}
	function formatDate(validFrom,validTo){
		var valFrom = validFrom.split("-");
		var newFormatFrom = valFrom[2]+"/"+valFrom[1]+"/"+valFrom[0];
		$("#validFrom").val(newFormatFrom);
		$("#validFrom").datepicker({
			format : 'dd/MM/yyyy'
		});
		var valTo = validTo.split("-");
		var newFormatTo = valTo[2]+"/"+valTo[1]+"/"+valTo[0];
		$("#validTo").val(newFormatTo);
		$("#validTo").datepicker({
			format : 'dd/MM/yyyy'
		});
	}
	function save() {
		$("#add").prop("disabled", true);
		if ($("#role").val() == '' || $("#validFrom").val() == '' || $("#validTo").val() == '') {
			$("#info").html("Complete the form before save!");
		} else {
			if ($('#saveBtn').text() == 'Save Now' || $('#saveBtn').text() == 'Update Now') {
				var data = getData();
				data["typeProcess"] = typeProcess;
				data["id"] = id;
				postData(data, '/cofin-ui/menu/save/roleAction', typeProcess + " data");
			}
			$("#role").prop('disabled','disabled');
			$("#validFrom").prop('disabled','disabled');
			$("#validTo").prop('disabled','disabled');
			$("#user :input:text").attr("disabled", true);
			$('#title').text('Are you sure want to ' + typeProcess + '?');
			$('#saveBtn').text(typeProcess + " Now");
			$('#cancelBtn').show();
			$('#deactiveBtn').hide();
		}
	}
	function getData() {
		var data = {};
		data["id"]=$("#zid").val();
		data["name"]='${action.name}';
		data["role"] = $("#role").val();
		data["validFrom"] = $("#validFrom").val();
		data["validTo"] = $("#validTo").val();
		return data;
	}
	function deactive(id){
		$("#zid").val(id);
		typeProcess = "Deactive";
		$('#infoConfirmation').text("Are You Sure Want To Deactive?");
		$('#myModalConfirmation').modal('show');
	}
	function reactive(id){
		$("#zid").val(id);
		typeProcess = "Reactive";
		$('#infoConfirmation').text("Are You Sure Want To Reactive?");
		$('#myModalConfirmation').modal('show');
	}
	function cancel(){
		$("#cancelBtn").hide();
		$('#title').text('Cancel '+typeProcess+'!');
		$("#user:input:text").attr("disabled", false);
		$('#saveBtn').text('Save');
		if(typeProcess=='Save'){
			$('#deactiveBtn').hide();
		}else{
			$('#deactiveBtn').show();
		}
		$("#role").removeAttr("disabled");
		$("#validFrom").removeAttr("disabled");
		$("#validTo").removeAttr("disabled");
	}
	$('#ok').on('click',function(){
		var data = getData();
		$('#myModalConfirmation').modal('hide');
		postData(data,'/cofin-ui/menu/roleAction/activation',typeProcess+" Data");
	});
	function simpan(){
		alert("Simpan");
	}
</script>
</html>