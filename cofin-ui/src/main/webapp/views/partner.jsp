<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="breadcrumb" scope="session" value="Partner" />
<jsp:include page="fragment/header.jsp" />
<jsp:include page="fragment/menu.jsp" />
<jsp:include page="fragment/topnavigation.jsp" />
<div class="table-responsive">
<fieldset class="the-fieldset">
<legend class="the-legend">PARTNER</legend>
 	<input type="hidden" name="prio" id="prio" value="${prio}"/>
	<table id="xtable" class="table-jfs display" cellspacing="0" width="100%">
		<div id="feedback"></div>
		<thead>
			<tr>	
				<th>Priority</th>
				<th>Partner Name</th>
				<th>Address</th>
				<th>Contact Person</th>
				<th>Phone Number</th>
				<th>E-Mail</th>
				<th>Status</th>
				<th>Action</th>
				<th style="display:none;">X</th>
				<th style="display:none;">X</th>
				<th style="display:none;">X</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="partner" items="${partners}">
				<tr>
					<td>${partner.priority}</td>
					<td>${partner.name}</td>
					<td>${partner.address}</td>
					<td>${partner.mainContact}</td>
					<td>${partner.phoneNumber}</td>	
					<td>${partner.email}</td>	
					<td>
						<c:set var="deleted" scope="session" value="${partner.isDelete}"/> 
						<c:if test="${deleted=='N'}">
							<c:out value="ACTIVE"/>
						</c:if> 
						<c:if test="${deleted=='Y'}">
							<c:out value="INACTIVE"/>
						</c:if>
					</td>
					<td>
						<button class="btn btn-approve"	onClick="updateData('${partner.name}', '${partner.address}','${partner.mainContact}','${partner.phoneNumber}','${partner.email}','${partner.id}','${partner.isDelete}','${partner.priority}','${partner.isCheckingEligibility}')">Update</button>
					</td>
					<td style="display:none;">${partner.isCheckingEligibility}</td>
					<td style="display:none;"><input type="text" name="xprio_${partner.priority}" id="xprio_${partner.priority}" value="${partner.priority}"></td>
					<td style="display:none;"><input type="text" name="xname_${partner.priority}" id="xname_${partner.priority}" value="${partner.name}"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</fieldset>
<div class="modal fade" id="updatePartner" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">X</button>
				<h4 id="title" class="modal-title">Update Partner</h4>
			</div>
			<div class="modal-body">
			<fieldset class="the-fieldset">
            <legend class="the-legend">Form Partner</legend>
				<form id="partner" name="partner">
					<div class="form-group">
						<label class="col-md-4">Partner Name</label>
						<input type="hidden" id="id" name="id"> 
						<input type="hidden" id="deleted" name="deleted"> 
						<input type="text" id="partnerName" name="partnerName" class="form-control" placeholder="Partner Name">
					</div>
						<div class="form-group">
						<label class="col-md-4">Address</label> 
						<input type="text" name="address" id="address" class="form-control" placeholder="Address">
					</div>
					<div>
						<label class="col-md-4">Contact Person</label> 
						<input type="text" name="contactPerson" id="contactPerson" class="form-control"	placeholder="Contact Person">
					</div>
						<div class="form-group">
						<label class="col-md-4">Phone Number</label> 
						<input type="text" name="contactPhone" id="contactPhone" class="form-control" placeholder="Phone Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength='16' pattern='^[0-9]$'>
					</div>
						<div class="form-group">
						<label class="col-md-4">Email</label> 
						<input type="text" name="email" id="email" class="form-control" placeholder="Email">
					</div>
					<div class="form-group">
						<label class="col-md-4">Priority</label>
						<input type="text" name="priority" id="priority" class="form-control" placeholder="Priority" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength='7' pattern='^[0-9]$'>
						<input type="hidden" name="zprio" id="zprio"/>
					</div>
					<div class="form-group">
						<input type="checkbox" name="eligibility" id="eligibility">&nbsp;Eligibility&nbsp;Check
					</div>
					<div class="text-right">
						<button type="button" id="cancelBtn" name="cancelBtn" class="btn btn-red">Cancel</button>
						<button type="button" onClick="activationData()" id="deactiveBtn" name="deactiveBtn" class="btn btn-red">Deactive</button>
						<button type="button" onClick="update()" id="updateBtn" name="updateBtn" class="btn btn-blue">Update</button>
						<button type="button" onClick="save()" id="saveBtn" name="saveBtn" class="btn btn-blue">Save</button>
					</div>
				</form>
			</fieldset>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<jsp:include page="fragment/footer.jsp"/>
<script type="text/javascript">
	var typeProcess = '';
	t.order([ 5, 'asc' ]).draw();
	var data = {};
	function save(){
		if ($("#partnerName").val() == '' || $("#address").val() == '' || $("#contactPerson").val() == '' || $("#email").val() == ''){
			$("#info").html("Complete the form before save!");
			$('#myModal').modal('show');
		}else{
			$('#title').text("Are you sure want to save?");
			$("#partner :input:text").attr("disabled", true);
			if($('#saveBtn').text() == 'Save Now'){
				var data = getdata();
				postData(data, '/cofin-ui/partner/save', "Save Data");
			}
			$("#cancelBtn").show();
			$('#saveBtn').text("Save Now");
			$('#deactiveBtn').hide();
		}
	}
	function update(){		
		var data = getdata();
		$("#save").prop("disabled", true);
		$("#add").prop("disabled", true);
		$('#title').text("Are you sure want to update?");
		$("#partner :input:text").attr("disabled", true);
		$('#saveBtn').hide();
		document.getElementById("eligibility").disabled = true;
		if($('#updateBtn').text() == 'Update Now'){
			var salah = 0;
			var pesan = "";
			var hitung = $("#prio").val();
			var prio = $("#priority").val();
			var xprio = 0;
			var zprio = $("#zprio").val();
			for(var x=1;x<hitung;x++){
				xprio = $("#xprio_"+x).val();
				if(xprio == prio){
					if(zprio == xprio){
						salah = salah + 0;
					}else{
						salah = salah + 1;
						pesan = "Priority "+prio+" Was Use For "+$("#xname_"+x).val()+" Partner";	
					}
				}
			}
			if(salah > 0){
				alert(pesan);
			}else{
				data["id"] = $("#id").val();
				postData(data, '/cofin-ui/partner/update', "Update Data");				
			}
		}
		$('#updateBtn').text('Update Now');
		$("#cancelBtn").show();
		$('#deactiveBtn').hide();
	}
	function activationData(){
		data["id"] = $("#id").val();
		$("#save").prop("disabled", true);
		$("#add").prop("disabled", true);
		$("#infoConfirmation").html("Are You Sure Want To Update Data?");
		$('#myModalConfirmation').modal('show');
	}
	$('#ok').on('click',function(){
		$('#myModalConfirmation').modal('hide');
		var msg = "";
		var deleted = $('#deleted').val();
		if(deleted == "N"){
			msg = "Deactive "
		}else{
			msg = "Reactive "
		}
		postData(data, '/cofin-ui/partner/activation', msg+" Data");
	});
	$('#add').on('click',function(){
		$("form")[0].reset();
		typeProcess="save";
		$('#updatePartner').modal('show');
		$("#title").text("Add New Partner");
		$("#deactiveBtn").hide();
		$("#updateBtn").hide();
		$("#cancelBtn").hide();
		$('#saveBtn').show();
		$("#priority").val($("#prio").val());
		$("#priority").prop('disabled', 'disabled');
	});
	$('#cancelBtn').on('click',function(){
		$("#partner:input:text").attr("disabled", false);
		$("#cancelBtn").hide();
		$("#deactiveBtn").show();
		document.getElementById("eligibility").disabled = false;
		if(typeProcess=="update"){
			$('#updateBtn').text('Update');
		}else{
			$('#saveBtn').text('Update');
		}
	});
	function updateData(name,address,contactName,contactPhone,contactEmail,id,deleted,priority,eligibility){
		typeProcess="update";
		$('#updatePartner').modal('show');
		$('#partnerName').val(name);
		$('#address').val(address);
		$('#contactPerson').val(contactName);
		$('#contactPhone').val(contactPhone);
		$('#email').val(contactEmail);
		$('#id').val(id);
		$('#deleted').val(deleted);
		$('#saveBtn').hide();
		$('#updateBtn').show();
		$('#deactiveBtn').show();
		$('#cancelBtn').hide();
		$('#priority').val(priority);
		$('#priority').removeAttr("disabled");
		$("#zprio").val(priority);
		if(eligibility=='N'){
			$('#eligibility').attr('checked',false);
		}else{
			$('#eligibility').attr('checked',true);
		}
		if (deleted == 'N') {
			$('#deactiveBtn').text("Deactive");
		} else {
			$('#deactiveBtn').text("Reactive");
		}
	}
	function getdata(){
		var dataForm = {};
		dataForm["name"] = $('#partnerName').val();
		dataForm["address"] = $("#address").val();
		dataForm["contactName"] = $("#contactPerson").val();
		dataForm["contactPhone"] = $("#contactPhone").val();
		dataForm["contactEmail"] = $("#email").val();
		dataForm["priority"] = $("#priority").val();
		if(document.getElementById("eligibility").checked = true){
			dataForm["isEligibilityChecked"] = 'Y';
		}else{
			dataForm["isEligibilityChecked"] = 'N';
		}
		return dataForm;
	}
</script>
</html>
