<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="breadcrumb" scope="session" value="Product"/>
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<jsp:include page="fragment/topnavigation.jsp"/>
<div class="table-responsive">
	<input type="hidden" name="sizes" id="sizes" value="${sizes}"/>
	<fieldset class="the-fieldset">
		<legend class="the-legend">Product</legend>
		<table id="xtable" class="table-jfs display" cellspacing="0"	width="100%">
			<div id="feedback"></div>
			<thead>
				<tr>
					<th>Product Code</th>
					<th>Product Name</th>
					<th>Action</th>
					<th style="display:none;">X</th>
					<th style="display:none;">X</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="product" items="${product}">
					<tr>
						<td>${product.codeProduct}</td>
						<td>${product.nameProduct}</td>
						<td>
							<button class="btn btn-approve"	onClick="update('${product.codeProduct}','${product.nameProduct}');">Update</button>
						</td>
						<td style="display:none;"><input type="text" name="xcode_${product.id}" id="xcode_${product.id}" value="${product.codeProduct}"></td>
						<td style="display:none;"><input type="text" name="xname_${product.id}" id="xname_${product.id}" value="${product.nameProduct}"></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</fieldset>
	<div class="modal fade" id="addProduct" role="dialog">
		<div class="modal-dialog ">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">X</button>
					<h4 id="title" class="modal-title">Add Agreement</h4>
				</div>
				<div class="modal-body">
					<fieldset class="the-fieldset">
						<legend class="the-legend">Form Add Product</legend>
						<form id="product" name="product">
							<div class="form-group">
								<input type="hidden" id="zcode" name="zcode" class="form-control" placeholder="ID">
							</div>
							<div class="form-group">
								<label class="col-md-4">Product Code</label> 
								<input type="text" id="code" name="code" class="form-control" placeholder="ID">
							</div>
							<div class="form-group">
								<label class="col-md-4">Product Name</label> 
								<input type="text" id="name" name="name" class="form-control" placeholder="Product Name">
							</div>
							<div class="text-right">
								<br>
								<button type="button" id="cancelBtn" name="cancelBtn" class="btn btn-red">Cancel</button>
								<button type="button" onClick="save()" id="saveBtn"	name="saveBtn" class="btn btn-blue">Save</button>
							</div>
						</form>
					</fieldset>
				</div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<jsp:include page="fragment/footer.jsp"/>
	<script type="text/javascript">
		$("#addDwh").hide();
		var typeProcess = '';
		t.order([ 3, 'asc' ]).draw();
		function save() {
			if ($("#code").val() == '' || $("#name").val() == '') {
				$("#info").html("Complete The Form Before Save!");
				$('#myModal').modal('show');
			} else {
				if ($('#saveBtn').text() == 'Save Now') {
					var data = getData();
					data['type'] = typeProcess;
					var sizes = $("#sizes").val();
					var check = 0;
					var pesan = "";
					var code = $("#code").val();
					for(var x=1;x<=sizes;x++){
						var xcode = $("#xcode_"+x).val();
						var xname = $("#xname_"+x).val();
						if(code == xcode){
							check = check+1;
							pesan = "Product Code "+code+" Was Used With Product Name "+xname;
						}else{
							check = check+0;
						}
					}
					var zcode = $("#zcode").val();
					if(check>0){
						if(zcode==code){
							postData(data, '/cofin-ui/product/update', typeProcess+" Data");
						}else{
							alert(pesan);
						}
					}else{
						if(typeProcess=='Update'){
							postData(data, '/cofin-ui/product/update', typeProcess+" Data");
						}else{
							postData(data, '/cofin-ui/product/save', typeProcess+" Data");
						}
					}
				}
				$("#product :input:text").attr("disabled", true);
				$('#title').text('Are you sure want to save?');
				$('#saveBtn').text('Save Now');
				$('#cancelBtn').show();
			}
		}
		$('#add').on('click', function() {
			typeProcess = 'Save';
			$("form")[0].reset();
			$('#addProduct').modal('show');
			$("#cancelBtn").hide();
			$('#title').text('Add New Product');
			$("#product :input:text").attr("disabled", false);
		});
		function update(code,name) {
			typeProcess = 'Update';
			$('#title').text('Update Product');
			$('#addProduct').modal('show');
			$("#zcode").val(code);
			$("#code").val(code);
			$("#name").val(name);
			$('#cancelBtn').hide();
		}
		$('#cancelBtn').on('click', function() {
			$('#addProduct').modal('show');
			$("#cancelBtn").hide();
			$('#title').text('Cancel Save!');
			$("#product :input:text").attr("disabled", false);
			$('#saveBtn').text('Save');
		});
		function getData() {
			var data = {};
			data["id"] = $("#zcode").val();
			data["code"] = $("#code").val();
			data["name"] = $("#name").val();
			return data;
		}
	</script>
	</body>
</html>
