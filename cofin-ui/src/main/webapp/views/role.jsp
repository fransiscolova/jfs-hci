<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="breadcrumb" scope="session" value="Role"/>
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<jsp:include page="fragment/topnavigation.jsp"/>
<div class="table-responsive">
<fieldset class="the-fieldset">
<legend class="the-legend">ROLE</legend>
	<input type="hidden" name="xdata" id="xdata"/>
	<table id="table" class="table-jfs display" cellspacing="0" width="100%">
		<div id="feedback"></div>
		<thead>
			<th>Role</th>
			<th>Description</th>
			<th>Status</th>
			<th>Action</th>
		</thead>
		<tbody>
			<c:forEach var="role" items="${xrole}">
				<tr>
					<td>${role.role}</td>
					<td>${role.description}</td>
					<td>
						<c:set var="deleted" scope="session" value="${role.deleted}"/>
							<c:if test="${deleted=='N'}">
								<c:out value="ACTIVE"/>
							</c:if>
							<c:if test="${deleted=='Y'}">
								<c:out value="INACTIVE"/>
							</c:if>
					</td>
					<td>
						<c:set var="deleted" scope="session" value="${role.deleted}"/>
							<c:if test="${deleted=='N'}">
								<button class="btn btn-red" onClick="statusUpdate('${role.role}','Deactive')">Deactive</button>
							</c:if>
							<c:if test="${deleted=='Y'}">
								<button class="btn btn-blue" onClick="statusUpdate('${role.role}','Reactive')">Reactive</button>
							</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</fieldset>
</div>
<jsp:include page="fragment/footer.jsp"/>
<script type="text/javascript">
	t.order([0,'asc']).draw();
	$('#add').hide();
	var xstatus = "";
	var data = {};
	function statusUpdate(role,status){
		data["role"] = role;
		data["status"] = status;
		xstatus = status;
		$('#infoConfirmation').text('Are you sure want to '+status+'?');
		$('#myModalConfirmation').modal('show');
	}
	$('#ok').on('click',function(){
		$('#myModalConfirmation').modal('hide');
		postData(data,'/cofin-ui/role/status',xstatus+" Data");
	});
</script>
</html>