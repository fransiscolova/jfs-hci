<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<div class="top_nav">
	<div style="background-color: #CB0D31" class="nav_menu">
		<nav>
			<ul class="nav navbar-nav navbar-right">
				<li role="presentation" class="dropdown">
				<a href="javascript:;"	class="info-number"> 
				<i class="fa fa-sign-out"></i>
			</ul>
		</nav>
	</div>
</div>
<div class="right_col main-content" role="main">
	<div class="row">
		<ol class="breadcrumb">
			<li class="active"><a href="/cofin-ui/contract/"><< Contract</a></li>
		</ol>	
		<div class="form-creation">
			<fieldset class="the-fieldset">
			<legend class="the-legend">Search Contract</legend>
			<form class="form-inline" id="testForm">
				<div class="form-group">
					<label class="col-md-2">Contract Number</label>
					<div class="col-md-3">
						<input type="text" name="contractCode" id="contractCode" maxlength="10" placeholder="Contract Number" class="form-control"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Status</label>
					<div class="col-md-3">
						<select style="width:150px;border-radius:6px;" class="form-control" id="status" name="status">
						<option value="W">Select Status</option>
							<c:forEach items="${enumStatusContracts}" var="statusContract">
								<option value="${statusContract.value}">${statusContract.text}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Partner</label>
					<div class="col-md-3">
						<select style="width:290px;border-radius:6px;" onchange="cariAgreement()" class="form-control" id="partner" name="partner">
							<option value="">Select Partner</option>
								<c:forEach items="${partners}" var="partner">
									<option value="${partner.id}">${partner.name}</option>
								</c:forEach>
						</select>
					</div>
					<div class="text-right">
						<button type="button" class="btn btn-approve" id="xls" name="xls" style="width:150px;height:38px;" onclick="toXls()">Export To Xls</button>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">Agreement</label>
					<div class="col-md-3">
						<select class="form-control" style="width:290px;border-radius:6px;" id="agreement" name="agreement">
							<option value="">Select Agreement</option>
						</select>
					</div>
					<div class="text-right">
						<button type="button" id="reset" name="reset" class="btn btn-red" style="width:150px;" onClick="bersih()">Reset</button>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2">JFS Registration Date</label>
					<div class="col-md-1">
						<input type="text" style="width:100px;" id="regFrom" name="regFrom" class="form-control"/>
					</div>
					<label style="text-align:center" class="col-md-1">To</label>
					<div class="col-md-1">
						<input type="text" style="width:100px;" id="regTo" name="regTo" class="form-control"/>
					</div>
					<div class="text-right">
						<button type="button" id="search" name="search" style="width:150px;" class="btn btn-blue" onClick="cari()">Search</button>
					</div>
				</div>
			</form>
			</fieldset>
			<br/>
		</div>
		<div class="table-responsive">
			<table id="xtable" class="table-jfs display" cellspacing="0" width="100%">
				<div id="feedback"></div>
				<thead width="100%">
					<tr>
						<th>No</th>
						<th>Contract Code</th>
						<th>JFS Status</th>
						<th>Agreement</th>
						<th>Registration Date</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<jsp:include page="fragment/footer.jsp"/>
<c:choose>
	<c:when test="${not empty message}">
		<script>
            $("#info").html('${message}');
            $('#loginModal').modal('show');
		</script>
	</c:when>
</c:choose>
<script type="text/javascript">
$('#add').hide();
$('#xls').hide();
var typeProcess = '';
$("#regFrom").datepicker();
$("#regTo").datepicker();
function bersih(){
	$('#xls').hide();
	$("#agreement").empty();
	var agree = document.getElementById("agreement");
	var option = document.createElement("option");
	option.text = "Select Agreement";
	option.value = "";
	agree.add(option);
	$("#contractCode").val("");
	$("#status").val("W");
	$("#partner").val("");
	$("#agreement").val("");
	$("#regFrom").val("");
	$("#regTo").val("");
	r.clear().draw();
}
function cariAgreement(){
	var lifnr = $("#partner").val();
	if(lifnr==""||lifnr==null){
		alert("Please Choose Partner");
		$("#agreement").data("comboBox").value("");
	}else{
		$.ajax({
			url			: "/cofin-ui/contract/getAgreement/?partner="+lifnr,
			type		: "GET",
			dataType	: "json",
			beforeSend	: function(){
				$('#LoadingDialog').dialog('open');
			},
			success		: function(data){
				$('#LoadingDialog').dialog('close');
				$("#agreement").empty();
				for(var x=0;x<data.length;x++){
					var agree = document.getElementById("agreement");
					var option = document.createElement("option");
					option.text = data[x].text;
					option.value = data[x].value;
					agree.add(option);
				}
			},
			error		: function(){
				$('#LoadingDialog').dialog('close');
				alert("Failed To Process Data");
			}
		});
	}
}
function cari(){
	$('#xls').hide();
	var contractCode = $("#contractCode").val();
	var status = $("#status").val();
	var partner = $("#partner").val();
	var agreement = $("#agreement").val();
	var regFrom = $("#regFrom").val();
	var regTo = $("#regTo").val();
	var forms = $('#testForm');
	if(contractCode == "" && status == "W" && partner == "" && agreement == "" && regFrom == "" && regTo == ""){
		$('#xls').hide();
		$("#info").html("Please Enter At Least One Of Criteria Column");
		$('#myModal').modal('show');
	}else{
		$.ajax({
			url			: "/cofin-ui/contract/search/?contractCode="+contractCode+"&status="+status+"&agreement="+agreement+"&from="+regFrom+"&to="+regTo,
			type		: "POST",
			dataType	: "json",
			data		: forms.serialize(),
			beforeSend	: function(){
				$('#LoadingDialog').dialog('open');
			},
			success		: function(data){
				$.each(data,function(key,value){
					$('#LoadingDialog').dialog('close');
					var result = this['result'];
					if(result==0){
						$('#xls').hide();
						$("#info").html(this['message']);
						$('#myModal').modal('show');
					}else{
						$('#xls').show();
						var json = this['data1'];
						var gson = JSON.parse(json);
						var table = document.getElementById("xtable");
						r.clear().draw();
						for(var x=0;x<gson.length;x++){
							var no = x+1;
							var contractCode = gson[x].textContractNumber;
							var jfsStatus = gson[x].status;
							var agreement = gson[x].idAgreement;
							var regDate = gson[x].exportDate;
							var zdate = new Date(regDate);
							zdate.setDate(zdate.getDate()+1);
							r.row.add([no,contractCode,jfsStatus,agreement,zdate.toISOString().substring(0,10),'<button class="btn btn-approve" onClick="toDetail('+contractCode+')">View</button>']);
						}
						r.draw();
					}
				});
			},
			error		: function(){
				$('#LoadingDialog').dialog('close');
				$("#info").html("Failed To Process Data");
				$('#myModal').modal('show');
			}
		});
	}
}
function toDetail(id){
	window.location.replace('/cofin-ui/contract/detail/'+id);
}
function toXls(){
	var forms = $('#testForm');
	var contractNumber = $("#contractCode").val();
	var status = $("#status").val();
	var partner = $("#partner").val();
	var agreement = $("#agreement").val();
	var regFrom = $("#regFrom").val();
	var regTo = $("#regTo").val();
	$.ajax({
		url			: "/cofin-ui/contract/search/?contractCode="+contractNumber+"&status="+status+"&agreement="+agreement+"&from="+regFrom+"&to="+regTo,
		type		: "POST",
		dataType	: "json",
		data		: forms.serialize,
		beforeSend	: function(){
			$('#LoadingDialog').dialog('open');
		},
		success		: function(data){
			$.each(data,function(key,value){
				if(this['result']==0){
					$('#LoadingDialog').dialog('close');
					$("#info").html(this['message']);
					$('#myModal').modal('show');
				}else{
					setTimeout(function(){
						window.location = "/cofin-ui/contract/ImportXls/";
						$('#LoadingDialog').dialog('close');
					});
				}
			});
		},
		error		: function(){
			$('#LoadingDialog').dialog('close');
			$("#info").html("Failed To Process Data");
			$('#myModal').modal('show');
		}
	});
}
</script>
</html>