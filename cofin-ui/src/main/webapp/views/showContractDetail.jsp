<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="baseUrl" scope="session" value=".."/>
<c:set var="breadcrumb" scope="session" value="Role"/>
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<div class="top_nav">
	<div style="background-color: #CB0D31" class="nav_menu">
		<nav>
			<ul class="nav navbar-nav navbar-right">
				<li role="presentation" class="dropdown">
				<a href="javascript:;"	class="info-number"> 
				<i class="fa fa-sign-out"></i>
			</ul>
		</nav>
	</div>
</div>
<div class="right_col main-content" role="main">
	<div class="row">
		<ol class="breadcrumb">
			<li class="active"><a href="/cofin-ui/contract/"><< Contract</a></li>
		</ol>
		<div class="form-creation">
			<form class="form-inline" id="testForm">
			<fieldset class="the-fieldset">
			<legend class="the-legend">View Contract Detail</legend>
				<div class="form-group">
					<label class="col-md-3">Contract Number</label>
					<div class="col-md-3">
						<input type="text" value="${contract.textContractNumber}" style="width:250px;" class="form-control" disabled/>
					</div>
					<label class="col-md-3">Credit Amount</label>
					<div class="col-md-3">
						<input type="text" value="" style="width:250px;" class="form-control" disabled/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">Agreement Code</label>
					<div class="col-md-3">
						<input type="text" value="${agreement.codeAgreement}" style="width:250px;" class="form-control" disabled/>
					</div>
					<label class="col-md-3">Additional Protection Premium</label>
					<div class="col-md-3">
						<input type="text" value="" style="width:250px;" class="form-control" disabled/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">Client Name</label>
					<div class="col-md-3">
						<input type="text" value="${contract.clientName}" style="width:250px;" class="form-control" disabled/>
					</div>
					<label class="col-md-3">Tenor</label>
					<div class="col-md-3">
						<input type="text" value="${contract.sendTenor}" style="width:250px;" class="form-control" disabled/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">Status</label>
					<div class="col-md-3">
						<input type="text" value="${status}" style="width:250px;" class="form-control" disabled/>
					</div>
					<label class="col-md-3">Annuity</label>
					<div class="col-md-3">
						<input type="text" value="" style="width:250px;" class="form-control" disabled/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">Creation Date</label>
					<div class="col-md-3">
						<input type="text" id="exportDate" name="exportDate" value="${contract.exportDate}" style="width:250px;" class="form-control" disabled/>
					</div>
					<label class="col-md-3">Monthly Installment</label>
					<div class="col-md-3">
						<input type="text" id="xamtInstalment" name="xamtInstalment" value="${contract.amtInstallment}" style="width:250px;" class="form-control" disabled/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">JFS Status</label>
					<div class="col-md-3">
						<input type="text" value="${config.desc01}" style="width:250px;" class="form-control" disabled/>
					</div>
					<label class="col-md-3">First Due Date</label>
					<div class="col-md-3">
						<input type="text" id="fDueDate" name="fDueDate" value="${contract.dateFirstDue}" style="width:250px;" class="form-control" disabled/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3"></label>
					<div class="col-md-3">
						<label></label>
						<div class="col-md-3">
							<input type="hidden" value="${cuid}"/>
						</div>
					</div>
					<label class="col-md-3">Last Due Date</label>
					<div class="col-md-3">
						<input type="text" id="xDueDate" name="xDueDate" value="${schedule.dueDate}" style="width:250px;" class="form-control" disabled/>
					</div>
				</div>
				</fieldset>
				<br>
				<div class="container">
					<ul class="nav nav-tabs">
    					<li class="tablink" onclick="jfsStatus('${contract.textContractNumber}')"><a data-toggle="tab" href="#jfsStatus">JFS Status</a></li>
    					<li class="tablink" onclick="client('${cuid}')"><a data-toggle="tab" href="#client">Client</a></li>
    					<li class="tablink" onclick="product('${contract.textContractNumber}')"><a data-toggle="tab" href="#product">Product</a></li>
    					<li class="tablink" onclick="bslAlloc('${contract.textContractNumber}')"><a data-toggle="tab" href="#bslAllocation">Full Installment Schedule</a></li>
    					<li style="display:none;" class="tablink" onclick="hcidAlloc('${contract.textContractNumber}')"><a data-toggle="tab" href="#hcidAllocation">HCID Allocation</a></li>
    					<li style="display:none;" class="tablink" onclick="partnerAlloc('${contract.textContractNumber}')"><a data-toggle="tab" href="#partnerAllocation">Partner Allocation</a></li>
  					</ul>
  					<div class="tab-content">
  						<div id="jfsStatus" class="tab-pane fade in">
  						<br>
  						<fieldset class="the-fieldset">
  						<legend class="the-legend">Agreement Detail</legend>
  							<div class="form-group">
  								<label class="col-md-3">Agreement Code</label>
  								<div class="col-md-3">
  									<input type="text" id="agreementCode1" name="agreementCode1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Agreement Name</label>
  								<div class="col-md-3">
  									<input type="text" id="agreementName1" name="agreementName1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Partner Name</label>
  								<div class="col-md-3">
  									<input type="text" id="partnerName1" name="partnerName1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">JFS Status</label>
  								<div class="col-md-3">
  									<input type="text" id="jfsStatus1" name="jfsStatus1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Registration Date</label>
  								<div class="col-md-3">
  									<input type="text" id="regDate1" name="regDate1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Disbursement Status</label>
  								<div class="col-md-3">
  									<input type="text" id="disbStatus1" name="disbStatus1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Disbursement Date</label>
  								<div class="col-md-3">
  									<input type="text" id="disbDate1" name="disbDate1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Write-Off Date</label>
  								<div class="col-md-3">
  									<input type="text" id="woDate1" name="woDate1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Write-Off Registration Date</label>
  								<div class="col-md-3">
  									<input type="text" id="woReg1" name="woReg1" class="form-control"  style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Clawback Type</label>
  								<div class="col-md-3">
  									<input type="text" id="clawType1" name="clawType1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Clawback Date</label>
  								<div class="col-md-3">
  									<input type="text" id="clawDate1" name="clawDate1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  							<div class="form-group">
  								<label class="col-md-3">Clawback Amount</label>
  								<div class="col-md-3">
  									<input type="text" name="clawAmt1" id="clawAmt1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  						</fieldset>
  						<br>
  						<fieldset class="the-fieldset">
  						<legend class="the-legend">Payment Information Forwarding Detail</legend>
  						<div class="table-responsive">
  							<table id="xtable1" class="table-jfs display" cellspacing="0" width="100%">
  								<div id="feedback"></div>
  								<thead width="100%">
  									<tr>
  										<th>No</th>
  										<th>Amount</th>
  										<th>Payment Type</th>
  										<th>Payment Date</th>
  										<th>Payment Forwarding Date</th>
  										<th>Forwarding Status</th>
  									</tr>
  								</thead>
  							</table>
  						</div>
  						</fieldset>
  						<br>
  						<fieldset class="the-fieldset">
  						<legend class="the-legend">Early Termination Detail</legend>
  						<div class="form-group">
  							<div class="form-group">
  								<label class="col-md-3">ET Request</label>
  								<div class="col-md-3">
  									<input type="text" id="etReq1" name="etReq1" class="form-control" style="width:250px;" disabled/>
  								</div>
  								<label class="col-md-3">ET Request Date</label>
  								<div class="col-md-3">
  									<input type="text" id="etReqDate1" name="etReqDate1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  						</div>
  						<div class="form-group">
  							<div class="form-group">
  								<label class="col-md-3">ET Request Type</label>
  								<div class="col-md-3">
  									<input type="text" id="etReqType1" name="etReqType1" class="form-control" style="width:250px;" disabled/>
  								</div>
  								<label class="col-md-3">ET Request Registration Date</label>
  								<div class="col-md-3">
  									<input type="text" id="etReqRegDate1" name="etReqRegDate1" class="form-control" style="width:250px;" disabled/>
  								</div>
  							</div>
  						</div>
  						<div class="table-responsive">
  							<table id="xtable2" class="table-jfs display" cellspacing="0" width="100%">
  								<div id="feedback"></div>
  								<thead width="100%">
  									<tr>
  										<th>No</th>
  										<th>Type</th>
  										<th>Amount</th>
  										<th>Payment Date</th>
  										<th>Payment Forwarding Date</th>
  										<th>Forwarding Status</th>
  									</tr>
  								</thead>
  							</table>
  						</div>
  						</fieldset>
  						<br>
  						<fieldset class="the-fieldset">
  						<legend class="the-legend">Reconciliation Status</legend>
  						<div class="table-responsive">
  							<table id="xtable3" class="table-jfs display" cellspacing="0" width="100%">
  								<div id="feedback"></div>
  								<thead width="100%">
  									<tr>
  										<th>No</th>
  										<th>Reconciliation Date</th>
  										<th>Outstanding Principal</th>
  									</tr>
  								</thead>
  							</table>
  						</div>
  						</fieldset>
  						</div>
  						<div id="client" class="tab-pane fade">
  						<br>
  						<label id="message" name="message" style="color:#e60000"></label>
  						<br>
  						<fieldset class="the-fieldset">
						<legend class="the-legend">Personal Data</legend>
						<div class="form-group">
							<label class="col-md-3">First Name</label>
							<div class="col-md-3">
								<input type="text" id="firstName2" name="firstName2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">Birth Place</label>
							<div class="col-md-3">
								<input type="text" id="birthPlace2" name="birthPlace2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Last Name</label>
							<div class="col-md-3">
								<input type="text" id="lastName2" name="lastName2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">Birth Date</label>
							<div class="col-md-3">
								<input type="text" id="birthDate2" name="birthDate2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Legal Name</label>
							<div class="col-md-3">
								<input type="text" id="legalName2" name="legalName2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">Education</label>
							<div class="col-md-3">
								<input type="text" id="education2" name="education2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Gender</label>
							<div class="col-md-3">
								<input type="text" id="gender2" name="gender2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">Marital Status</label>
							<div class="col-md-3">
								<input type="text" id="maritalStatus2" name="maritalStatus2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">CUID</label>
							<div class="col-md-3">
								<input type="text" id="cuid2" name="cuid2" class="form-control" style="width:250px;" disabled/>
							</div>
  							<label class="col-md-3">Number Of Dependents</label>
							<div class="col-md-3">
								<input type="text" id="numberOfDependents2" name="numberOfDependents2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Mother's Maiden Name</label>
							<div class="col-md-3">
								<input type="text" id="mothersName2" name="mothersName2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">Religion</label>
							<div class="col-md-3">
								<input type="text" id="religion2" name="religion2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						</fieldset>
						<br>
						<fieldset class="the-fieldset">
						<legend class="the-legend">Employment</legend>
						<div class="form-group">
							<label class="col-md-3">Occupation</label>
							<div class="col-md-3">
								<input type="text" id="occupation2" name="occupation2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">Field Of Business</label>
							<div class="col-md-3">
								<input type="text" id="fieldOfBusiness2" name="fieldOfBusiness2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
 						<div class="form-group">
							<label class="col-md-3">Place Of Work</label>
							<div class="col-md-3">
								<input type="text" id="placeOfWork2" name="placeOfWork2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">Monthly Income</label>
							<div class="col-md-3">
								<input type="text" id="monthlyIncome2" name="monthlyIncome2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						</fieldset>
						<br>
						<fieldset class="the-fieldset">
						<legend class="the-legend">ID Card</legend>
						<div class="form-group">
							<label class="col-md-3">ID Card Type</label>
							<div class="col-md-3">
								<input type="text" id="idCardType2" name="idCardType2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">ID Card Number</label>
							<div class="col-md-3">
								<input type="text" id="idCard2" name="idCard2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">ID Card Expiration Date</label>
							<div class="col-md-3">
								<input type="text" id="idCardExpire2" name="idCardExpire2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						</fieldset>
						<br>
						<fieldset class="the-fieldset">
						<legend class="the-legend">Address</legend>
						<div class="form-group">
							<label class="col-md-3">Street Name</label>
							<div class="col-md-3">
								<input type="text" id="street2" name="street2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">RT</label>
							<div class="col-md-3">
								<input type="text" id="rt2" name="rt2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Town</label>
							<div class="col-md-3">
								<input type="text" id="town2" name="town2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">RW</label>
							<div class="col-md-3">
								<input type="text" id="rw2" name="rw2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">District</label>
							<div class="col-md-3">
								<input type="text" id="district2" name="district2" class="form-control" style="width:250px;" disabled/>
							</div>
							<label class="col-md-3">Zip Code</label>
							<div class="col-md-3">
								<input type="text" id="zipCode2" name="zipCode2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Subdistrict</label>
							<div class="col-md-3">
								<input type="text" id="subdistrict2" name="subdistrict2" class="form-control" style="width:250px;" disabled/>
							</div>
						</div>
						</fieldset>
						</div>
  						<div id="product" class="tab-pane fade">
  						<br>
  						<label id="xpesan" name="xpesan"></label>
  						<input type="hidden" id="xresult" name="xresult"/>
  						<br>
  						<fieldset class="the-fieldset">
  						<legend class="the-legend">Product</legend>
  						<div class="form-group">
  							<label class="col-md-3">Product Code</label>
  							<div class="col-md-3">
  								<input type="text" id="productCode3" name="productCode3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3">Manufacturer</label>
  							<div class="col-md-3">
  								<input type="text" id="manufacturer3" name="manufacturer3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Product Name</label>
  							<div class="col-md-3">
  								<input type="text" id="productName3" name="productName3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3">Model Number</label>
  							<div class="col-md-3">
  								<input type="text" id="modelNumber3" name="modelNumber3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Product Description</label>
  							<div class="col-md-3">
  								<input type="text" id="productDescription3" name="productDescription3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3">Goods Price</label>
  							<div class="col-md-3">
  								<input type="text" id="goodsPrice3" name="goodsPrice3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">HCID Commodity Category</label>
  							<div class="col-md-3">
  								<input type="text" id="hcidComCategory3" name="hcidComCategory3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3">Service Type Code</label>
  							<div class="col-md-3">
  								<input type="text" id="serviceTypeCode3" name="serviceTypeCode3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Partner Commodity Category</label>
  							<div class="col-md-3">
  								<input type="text" id="partnerComCategory3" name="partnerComCategory3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3" style="display:none;">Service Code</label>
  							<div class="col-md-3">
  								<input type="text" id="serviceCode3" name="serviceCode3" class="form-control" style="width:250px;display:none;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">HCID Commodity Type</label>
  							<div class="col-md-3">
  								<input type="text" id="hcidComType3" name="hcidComType3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3" style="display:none;">Service Name</label>
  							<div class="col-md-3">
  								<input type="text" id="serviceName3" name="serviceName3" class="form-control" style="width:250px;display:none;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Partner Commodity Type</label>
  							<div class="col-md-3">
  								<input type="text" id="partnerComType3" name="partnerComType3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3" style="display:none;">Service Description</label>
  							<div class="col-md-3">
  								<input type="text" id="serviceDescription3" name="serviceDescription3" class="form-control" style="width:250px;display:none;" disabled/>
  							</div>
  						</div>
  						</fieldset>
  						<br>
  						<fieldset class="the-fieldset">
  						<legend class="the-legend">Salesroom</legend>
  						<div class="form-group">
  							<label class="col-md-3">Salesroom Code</label>
  							<div class="col-md-3">
  								<input type="text" id="salesroomCode3" name="salesroomCode3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Salesroom Name</label>
  							<div class="col-md-3">
  								<input type="text" id="salesroomName3" name="salesroomName3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						</fieldset>
  						<br>
  						<fieldset class="the-fieldset" style="display:none;">
  						<legend class="the-legend">Additional Protection</legend>
  						<div class="form-group">
  							<label class="col-md-3">Premium Amount</label>
  							<div class="col-md-3">
  								<input type="text" id="premiumAmount3" name="premiumAmount3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Premium Payment</label>
  							<div class="col-md-3">
  								<input type="text" id="premiumPayment3" name="premiumPayment3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						</fieldset>
  						<br>
  						<fieldset class="the-fieldset">
  						<legend class="the-legend">Early Termination</legend>
  						<div class="form-group">
  							<label class="col-md-3">Request Date</label>
  							<div class="col-md-3">
  								<input type="text" id="requestDate3" name="requestDate3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3">ET Due Date</label>
  							<div class="col-md-3">
  								<input type="text" id="etDueDate3" name="etDueDate3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Total Amount</label>
  							<div class="col-md-3">
  								<input type="text" id="totalAmount3" name="totalAmount3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3">ET Compilation Date</label>
  							<div class="col-md-3">
  								<input type="text" id="etComDate3" name="etComDate3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Paid Amount</label>
  							<div class="col-md-3">
  								<input type="text" id="paidAmount3" name="paidAmount3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Remaining Amount</label>
  							<div class="col-md-3">
  								<input type="text" id="remainingAmount3" name="remainingAmount3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						</fieldset>
  						<br>
  						<fieldset class="the-fieldset">
  						<legend class="the-legend">Gift Payment</legend>
  						<div class="form-group">
  							<label class="col-md-3">Number Of Gift Payment</label>
  							<div class="col-md-3">
  								<input type="text" id="numberGiftPayment3" name="numberGiftPayment3" class="form-control" style="width:250px;" disabled/>
  							</div>
  							<label class="col-md-3">Eligibility Type</label>
  							<div class="col-md-3">
  								<input type="text" id="eligibilityType3" name="eligibilityType3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-3">Date Of Evaluation</label>
  							<div class="col-md-3">
  								<input type="text" id="dateEvaluation3" name="dateEvaluation3" class="form-control" style="width:250px;" disabled/>
  							</div>
  						</div>
  						</fieldset>
  						</div>
  						<div id="bslAllocation" class="tab-pane fade">
  						<br>
  							<input type="hidden" id="result3" name="result3"/>
  							<table id="xtable" class="table-jfs display" cellspacing="0" width="100%">
  								<div id="feedback"></div>
  								<thead width="100%">
  									<th>Term</th>
  									<th>Due Date</th>
  									<th>Principal Amount</th>
  									<th>Interest Amount</th>
  								</thead>
  							</table>
  						</div>
  						<div id="hcidAllocation" class="tab-pane fade">
  						<br>
  							<div class="table-responsive">
  								<table id="xtable4" class="table-jfs display" cellspacing="0" width="100%">
  									<div id="feedback"></div>
  									<thead width="100%">
  										<tr>
  											<th>Term</th>
  											<th>Due Date</th>
  											<th>Principal Amount</th>
  											<th>Payment Number</th>
  											<th>Interest Amount</th>
  											<th>Paid Date</th>
  											<th>Date Bank Process</th>
  										</tr>
  									</thead>
  								</table>
  							</div>
  						</div>
  						<div id="partnerAllocation" class="tab-pane fade">
  						<br>
  							<input type="hidden" id="result5" name="result5"/>
  							<div class="table-responsive">
  								<table id="xtable5" class="table-jfs display" cellspacing="0" width="100%">
  									<div id="feedback"></div>
  									<thead width="100%">
  										<tr>
  											<th>Term</th>
  											<th>Due Date</th>
  											<th>Principal Amount</th>
  											<th>Payment Number</th>
  											<th>Interest Amount</th>
  											<th>Paid Date</th>
  											<th>Date Bank Process</th>
  										</tr>
  									</thead>
  								</table>
  							</div>
  						</div>
  					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<jsp:include page="fragment/footer.jsp"/>
<script type="text/javascript">
	var exportDate = $("#exportDate").val();
	$("#exportDate").val(exportDate.substring(0,10));
    var fDueDate = $("#fDueDate").val();
    $("#fDueDate").val(fDueDate.substring(0,10));
    var xDueDate = $("#xDueDate").val();
    $("#xDueDate").val(xDueDate.substring(0,10));
    var xamtInstalment = $("#xamtInstalment").val();
    xamtInstalment = parseFloat(xamtInstalment);
    $("#xamtInstalment").val(xamtInstalment.toLocaleString());
	$('#add').hide();
	function jfsStatus(id){
		var agreement = $("#agreementCode1").val();
		var forms = $('#testForm');
		if(agreement=="" || agreement==null){
			$.ajax({
				url			: "/cofin-ui/contract/detail/jfsStatus/?contractNumber="+id,
				type		: "POST",
				dataType	: "json",
				data		: forms.serialize(),
				beforeSend	: function(){
					$('#LoadingDialog').dialog('open');
				},
				success		: function(data){
					$.each(data,function(key,value){
						$('#LoadingDialog').dialog('close');
						var json5 = this['data5'];
						var gson5 = JSON.parse(json5);
						r1.clear().draw();
						for(var x=0;x<gson5.length;x++){
							var no = gson5[x].parentId;
							var type = gson5[x].paymentType;
							var amount = gson5[x].amtPayment;
							var pdate = gson5[x].datePayment;
							var pfdate = gson5[x].dateBankProcess;
							var forwst = gson5[x].reason;
							r1.row.add([no,amount,type,pdate,pfdate,forwst]);
						}
						r1.draw();
						var json7 = this['data7'];
						var gson7 = JSON.parse(json7);
						r2.clear().draw();
						for(var y=0;y<gson7.length;y++){
							var no = gson7[y].textContractNumber;
							var type = gson7[y].clientName;
							var amount = gson7[y].amtEt;
							var pdate = gson7[y].dateBankDisbursed;
							var pfdate = gson7[y].dateBankPrinted;
							var forwst = gson7[y].updatedBy;
							r2.row.add([no,type,amount,pdate,pfdate,forwst]);
						}
						r2.draw();
						var json6 = this['data6'];
						var gson6 = JSON.parse(json6);
						r3.clear().draw();
						for(var z=0;z<gson6.length;z++){
							var no = gson6[z].reconId;
							var reconDate = gson6[z].reconDate;
							var outsPrin = gson6[z].outstandingPrincipal;
							r3.row.add([no,reconDate,outsPrin]);
						}
						r3.draw();
						var message = this['message'];
						var data1 = this['data1'];
						var data2 = this['data2'];
						var data3 = this['data3'];
						var data4 = this['data4'];
						var data8 = this['data8'];
						var data9 = this['data9'];
						var data10 = this['data10'];
						var data11 = this['data11'];
						var gson1 = JSON.parse(data1);
						var gson2 = JSON.parse(data2);
						var gson3 = JSON.parse(data3);
						var gson4 = JSON.parse(data4);
						$("#agreementCode1").val(gson2.codeAgreement);
						$("#agreementName1").val(gson2.nameAgreement);
						$("#partnerName1").val(gson3.name);
						$("#jfsStatus1").val(gson1.status);
						$("#regDate1").val(gson1.exportDate);
						$("#disbStatus1").val(gson1.sendPrincipal);
						$("#disbDate1").val(gson1.bankDecisionDate);
						$("#woDate1").val(gson4.woDate);
						$("#woReg1").val(gson4.exportDate);
						$("#clawType1").val(message);
						$("#clawDate1").val(gson1.bankClawbackDate);
						$("#clawAmt1").val(gson1.bankClawbackAmount);
						$("#etReq1").val(data8);
						$("#etReqDate1").val(data9);
						$("#etReqType1").val(data10);
						$("#etReqRegDate1").val(data11);
					});				
				},
				error		: function(){
					$('#LoadingDialog').dialog('close');
					$("#info").html("Failed To Process Data");
					$('#myModal').modal('show');
				}
			});
		}
	}
	function client(id){
		var forms = $('#testForm');
		var firstName = $("#firstName2").val();
		if(firstName=="" || firstName==null){
			$.ajax({
				url			: "/cofin-ui/contract/detail/client/?cuid="+id,
				type		: "POST",
				dataType	: "json",
				data		: forms.serialize(),
				beforeSend	: function(){
					$('#LoadingDialog').dialog('open');
				},
				success		: function(data){
					$('#LoadingDialog').dialog('close');
					$.each(data,function(key,value){
						var result = this['result'];
						if(result==1){
							$("#message").html("");
							var data1 = this['data1'];
							var gson1 = JSON.parse(data1);
							$("#firstName2").val(gson1.firstName);
							$("#lastName2").val(gson1.lastName);
							$("#birthPlace2").val(gson1.birthPlace);
							$("#birthDate2").val(gson1.birthDate);
							$("#legalName2").val(gson1.legalName);
							$("#education2").val(gson1.education);
							$("#gender2").val(gson1.gender);
							$("#maritalStatus2").val(gson1.maritalStatus);
							$("#cuid2").val(gson1.cuid);
							$("#numberOfDependents2").val(gson1.noDependents);
							$("#mothersName2").val(gson1.mothersName);
							$("#religion2").val(gson1.religion);
							$("#occupation2").val(gson1.occupation);
							$("#fieldOfBusiness2").val(gson1.fieldBusiness);
							$("#placeOfWork2").val(gson1.placeWork);
							$("#monthlyIncome2").val(gson1.monthlyIncome);
							$("#idCardType2").val(gson1.typeIdCard);
							$("#idCard2").val(gson1.noIdCard);
							$("#idCardExpire2").val(gson1.exDateIdCard);
							$("#street2").val(gson1.streetName);
							$("#rt2").val(gson1.rt);
							$("#rw2").val(gson1.rw);
							$("#town2").val(gson1.district);
							$("#district2").val(gson1.subdistrict);
							$("#subdistrict2").val(gson1.town);
							$("#zipCode2").val(gson1.zipCode);
						}else{
							$("#message").html(this['message']);
						}
					});
				},
				error		: function(){
					$('#LoadingDialog').dialog('close');
					$("#info").html("Failed To Process Data");
					$('#myModal').modal('show');
				}
			});
		}
	}
	function product(id){
		var xresult = $("#xresult").val();
		if(xresult=="" || xresult==null){
			$.ajax({
				url			: "/cofin-ui/contract/detail/product/?contractNumber="+id,
				type		: "GET",
				dataType	: "json",
				beforeSend	: function(){
					$('#LoadingDialog').dialog('open');
				},
				success		: function(data){
					$('#LoadingDialog').dialog('close');
					$.each(data,function(key,value){
						var result = this['result'];
						$("#xresult").val(result);
						if(result==1){
							var json1 = this['data1'];
							var gson1 = JSON.parse(json1);
							$("#xpesan").html("");
							$("#productCode3").val(gson1.productCode);
							$("#manufacturer3").val(gson1.producer);
							$("#productName3").val(gson1.productName);
							$("#modelNumber3").val(gson1.modelNumber);
							$("#productDescription3").val(gson1.productDescription);
							$("#goodsPrice3").val(gson1.goodsPrice);
							$("#hcidComCategory3").val(gson1.commodityCategoryCode);
							$("#partnerComCategory3").val(gson1.bankCommodityCategoryCode);
							$("#hcidComType3").val(gson1.hciCommodityTypeCode);
							$("#partnerComType3").val(gson1.bankCommodityTypeCode);
							$("#salesroomCode3").val(gson1.salesroomId);
							$("#salesroomName3").val(gson1.salesroomName);
							$("#premiumAmount3").val("");
							$("#premiumPayment3").val("");
							$("#requestDate3").val("");
							$("#etDueDate3").val("");
							$("#totalAmount3").val("");
							$("#etComDate3").val("");
							$("#paidAmount3").val("");
							$("#remainingAmount3").val("");
							$("#numberGiftPayment3").val("");
							$("#eligibilityType3").val("");
							$("#serviceTypeCode3").val(gson1.serviceTypeCode);
							$("#serviceCode3").val(gson1.serviceCode);
							$("#serviceName3").val(gson1.serviceName);
							$("#serviceDescription3").val(gson1.serviceDescription);
						}else{
							$("#xpesan").html(this['message']);
						}
					});
				},
				error		: function(){
					$('#LoadingDialog').dialog('close');
					$("#info").html("Failed To Process Data");
					$('#myModal').modal('show');
				}
			});
		}		
	}
	function bslAlloc(id){
		var forms = $('#testForm');
		var xresult = $("#result3").val();
		if(xresult=="" || result==null){
			$.ajax({
				url			: "/cofin-ui/contract/detail/bslAllocation/?contractNumber="+id,
				type		: "POST",
				dataType	: "json",
				data		: forms.serialize(),
				beforeSend	: function(){
					$('#LoadingDialog').dialog('open');
				},
				success		: function(data){
					$.each(data,function(key,value){
						$('#LoadingDialog').dialog('close');
						var result = this['result'];
						$("#result3").val(result);
						var json1 = this['data1'];
						var gson1 = JSON.parse(json1);
						r.clear().draw();
						for(var x=0;x<gson1.length;x++){
							var term = gson1[x].partIndex;
							var dueDate = gson1[x].dueDate;
							var principal = gson1[x].principal;
							var interest = gson1[x].interest;
							r.row.add([term,dueDate,principal,interest]);
						}
						r.draw();
					});				
				},
				error		: function(){
					$('#LoadingDialog').dialog('close');
					$("#info").html("Failed To Process Data");
					$('#myModal').modal('show');
				}
			});
		}
	}
	function hcidAlloc(id){
		$.ajax({
			url			: "/cofin-ui/contract/detail/hcidAllocation/?contractNumber="+id,
			type		: "GET",
			dataType	: "json",
			beforeSend	: function(){
				$('#LoadingDialog').dialog('open');
			},
			success		: function(data){
				$('#LoadingDialog').dialog('close');
				
			},
			error		: function(){
				$('#LoadingDialog').dialog('close');
				$("#info").html("Failed To Process Data");
				$('#myModal').modal('show');
			}
		});
	}
	function partnerAlloc(id){
		var forms = $('#testForm');
		var result = $("#result5").val();
		if(result=="" || result==null){
			$.ajax({
				url			: "/cofin-ui/contract/detail/partnerAllocation/?contractNumber="+id,
				type		: "POST",
				dataType	: "json",
				data		: forms.serialize(),
				beforeSend	: function(){
					$('#LoadingDialog').dialog('open');
				},
				success		: function(data){
					$.each(data,function(key,value){
						$('#LoadingDialog').dialog('close');
						var result = this['result'];
						$("#result5").val(result);
						var json1 = this['data1'];
						var gson1 = JSON.parse(json1);
						r5.clear().draw();
						for(var x=0;x<gson1.length;x++){
							var term = gson1[x].partIndex;
							var dueDate = gson1[x].dueDate;
							var principalAmount = gson1[x].amtPrincipal;
							var paymentNumber = gson1[x].archieved;
							var interestAmount = gson1[x].amtInterest;
							var paidDate = gson1[x].dtimeUpdated;
							var dateBankProcess = gson1[x].dtimeCreated;
							r5.row.add([term,dueDate,principalAmount,paymentNumber,interestAmount,paidDate,dateBankProcess]);
						}
						r5.draw();
					});
				},
				error		: function(){
					$('#LoadingDialog').dialog('close');
					$("#info").html("Failed To Process Data");
					$('#myModal').modal('show');
				}
			});
		}
	}
</script>
</html>