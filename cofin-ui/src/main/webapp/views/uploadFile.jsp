<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="en">

<c:set var="breadcrumb" scope="session" value="File" />

<jsp:include page="fragment/header.jsp" />
<jsp:include page="fragment/menu.jsp" />



<script type="text/javascript">
	$(function() {
		$('button[type=submit]').click(function(e) {
			e.preventDefault();
			//Disable submit button
			$(this).prop('disabled',true);
			
			
			var form = document.forms[0];
			var formData = new FormData(form);
			
			console.log(formData);
				
			// Ajax call for file uploaling
			var ajaxReq = $.ajax({
				url : 'fileUpload',
				type : 'POST',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				xhr: function(){
					//Get XmlHttpRequest object
					 var xhr = $.ajaxSettings.xhr() ;
					
					//Set onprogress event handler 
					 xhr.upload.onprogress = function(event){
						var perc = Math.round((event.loaded / event.total) * 100);
						$('#progressBar').text(perc + '%');
						$('#progressBar').css('width',perc + '%');
					 };
					 return xhr ;
				},
				beforeSend: function( xhr ) {
					//Reset alert message and progress bar
					$('#alertMsg').text("Please wait during upload file");
					$('#progressBar').text('');
					$('#progressBar').css('width','0%');
                }
			});

			// Called on success of file upload
			ajaxReq.done(function(msg) {
				$('#alertMsg').text(msg);
				$('input[type=file]').val('');
				$('button[type=submit]').prop('disabled',false);
			});
			
			// Called on failure of file upload
			ajaxReq.fail(function(jqXHR) {
				$('#alertMsg').text(jqXHR.responseText+'('+jqXHR.status+
						' - '+jqXHR.statusText+')');
				$('button[type=submit]').prop('disabled',false);
			});
		});
	});
</script>

<!-- top navigation -->
<div class="top_nav">
	<div style="background-color: #CB0D31" class="nav_menu">
		<nav>
			<!--  
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>-->
			<ul class="nav navbar-nav navbar-right">
				<li role="presentation" class="dropdown"><a href="javascript:;"
					class="info-number"> <i class="fa fa-sign-out"></i>
				</a></li>

			</ul>
		</nav>
	</div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col main-content" role="main">
	<div class="row">
		<ol class="breadcrumb">

			<li class="active"><a href="/cofin-ui/Contract/"
				class="Contract"><< Back</a></li>
		</ol>



		<div class="form-creation">


			<fieldset class="the-fieldset">
				<legend class="the-legend">Upload File Confirmation</legend>

				<form class="form-inline" method="POST" modelAttribute="fileUpload"
					enctype="multipart/form-data">
					<div class="form-group">


						<!-- -

						<div class="form-group">
							<label class="col-md-2">Contract Code</label>
							<div class="col-md-3">
								<input size="33" type="text" id="contractCode"
									name="contractCode" value="" class="form-control">

							</div>
						</div>
				 -->



						<div class="form-group">
							<label class="col-md-2">Partner</label>

							<div class="col-md-3">
								<input type="hidden" id="user" name="user"
									value="${pageContext.request.userPrincipal.name}"> <select
									style="width: 292px;" class="form-control" id="partnerId"
									name="partnerId">
									<option value="">--Select partner--</option>
									<c:forEach items="${partners}" var="partner">
										<option value="${partner.id}">${partner.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>



						<div class="form-group">
							<label class="col-md-2">Processing Period</label>

							<div class="col-md-3">
								<select style="width: 292px;" class="form-control" id="period"
									name="period">
									<option value="">--Select Period--</option>
									<option value="Daily">Daily</option>
									<option value="Monthly">Monthly</option>
								</select>
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-2">Process Category</label>
							<div class="col-md-3">
								<select style="width: 292px;" class="form-control"
									id="processCategory" name="processCategory"
									onChange="onChangeProcessType()">
									<option value="">--Select Category--</option>
									<c:forEach items="${processCategory}" var="processCategory">
										<option value="${processCategory.id}">${processCategory.name}</option>
									</c:forEach>

								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2">Confirmation Process</label>
							<div class="col-md-3">
								<select style="width: 292px;" class="form-control"
									id="processType" name="processType" onChange="onChangeFile()">
									<option value="">--Select Period--</option>

								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2">Confirmation File</label>
							<div class="col-md-3">
								<select style="width: 292px;" class="form-control"
									id=confirmationFile " name="confirmationFile">
									<option value="">--Select File--</option>

								</select>
							</div>
						</div>



						<div class="form-group">
							<label class="col-md-2">Process Date</label> 
							<div class="col-md-3">
							<input type="text"
								name="processDate" id="processDate" class="form-control"
								placeholder="Process Date" onChange="onProcessDateChange()">
								</div>
						</div>


						<div class="form-group">
							<label class="col-md-2">Note</label>

							<div class="col-md-3">
								<textarea name="memo" rows="4" cols="50"></textarea>
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-2">Select File</label>

							<div class="col-md-3">

								<input type="file" name="file" />

							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2"></label>

							<div class="col-md-3">

								
								 <button class="btn btn-blue" id="uploadButton" type="submit">Upload</button>
								 
								  <button class="btn btn-red"  onClick="location.reload()" >Refresh</button>
							</div>
						</div>
				</form>
				
				  <br />

    <!-- Bootstrap Progress bar -->
    <div class="progress">
      <div id="progressBar" class="progress-bar progress-bar-success" role="progressbar"
        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
    </div>

    <!-- Alert -->
    <div id="alertMsg" style="color: red;font-size: 18px;"></div>
			</fieldset>
			<br>
		</div>
		<div class="table-responsive">

			<fieldset class="the-fieldset">
				<legend class="the-legend">Upload File History</legend>
				<table id="table" class="table-jfs display" cellspacing="0"
					width="100%">
					<div id="feedback"></div>
					<thead>
						<tr>
							<th>Uploaded By</th>
							<th>Upload Date</th>
							<th>Process Type</th>
							<th>Partner Name</th>
							<th>Process Date</th>
							<th>File Name</th>
							<th>Status</th>
							<th>Total Row</th>
							<!--
							<th>Action</th>
							-->
						</tr>
					</thead>
					<tbody>
						<c:forEach var="upload" items="${fileUpload}">
							<tr>
								<td>${upload.createdBy}</td>
								<td>${upload.createdDate}</td>
								<td>${upload.processType}</td>
								<td>${upload.partner.name}</td>
								<td>${upload.processDate}</td>
								<td>${upload.fileName}</td>
								<td>${upload.status}</td>
								<td>${upload.totalRow}</td>
								

							</tr>
						</c:forEach>
					</tbody>
				</table>
			</fieldset>


			<jsp:include page="fragment/footer.jsp" />
			<script type="text/javascript">
				$('#add').hide();
				$("#processDate").datepicker();
				
				
				function onChangeProcess() {
					var data = {};
					var e = document.getElementById("partnerId");
					var id = e.options[e.selectedIndex].value;
					console.log("idpartner" + id);
					data["partnerId"] = id;
					getProcess(data, '/cofin-ui/upload/getProcess',
							"get Process");

				}

				function onChangeProcessType() {
					var data = {};

					var id = document.getElementById("processCategory");
					var categoryId = id.options[id.selectedIndex].value;
					
					
					var partner = document.getElementById("partnerId");
					var partnerId = partner.options[partner.selectedIndex].value;
					
					console.log("ProcessCategory" + categoryId);
					console.log("PartnerId" + partnerId);
					
					
					data["categoryId"] = categoryId;
					data["partnerId"] = partnerId;

					getProcessType(data, '/cofin-ui/upload/getProcessType',
							"get Process");

				}                                 

				function onChangeFile() {
					var data = {};

					var e = document.getElementById("processType");
					var id = e.options[e.selectedIndex].value;
					console.log("id process" + id);
					data["processId"] = id;
					console.log(id);
					getFile(data, '/cofin-ui/upload/getFile', "get file");

				}
				
				
				function onProcessDateChange() {
					var data = {};					
					data["processDate"] = $("#processDate").val();
					data["confirmFileId"] = $("#confirmationFile").val();
					console.log(data["processDate"]);
					console.log(data["confirmFileId"] );
					
					getProcessData(data, '/cofin-ui/upload/checkDate', "get date");

				}

				
				
				
				
				
			</script>
			</body>
</html>
