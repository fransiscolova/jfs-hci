<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<c:set var="baseUrl" scope="session" value=".." />
<c:set var="breadcrumb" scope="session" value="Role" />
<jsp:include page="fragment/header.jsp"/>
<jsp:include page="fragment/menu.jsp"/>
<jsp:include page="fragment/topnavigation.jsp"/>
<fieldset class="the-fieldset">
	<legend class="the-legend">User Detail</legend>
	<form id="user" name="user">
		<div class="form-group">
			<label class="col-md-2">Username</label>
			<input type="text" value="${users.username}" id="username" name="username" class="form-control" placeholder="Username" disabled>
		</div>
		<div class="form-group">
			<label class="col-md-2">First Name</label> 
			<input type="text" value="${users.firstName}" id="firstName" name="firstName" class="form-control" placeholder="First Name" disabled>
		</div>
		<div class="form-group">
			<label class="col-md-2">Last Name</label> 
			<input type="text" value="${users.lastName}" name="lastName" id="lastName" class="form-control" placeholder="Last Name" disabled>
		</div>
	</form>
</fieldset>
<br>
<div class="table-responsive">
	<table id="table" class="table-jfs display" cellspacing="0"
		width="100%">
		<div id="feedback"></div>
		<thead>
			<tr>
				<th>Role</th>
				<th>Valid From</th>
				<th>Valid To</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="role" items="${roles}">
				<tr>
					<td>${role.role.role}</td>
					<td>${role.validFrom}</td>
					<td>${role.validTo}</td>
					<td><c:set var="deleted" scope="session"
							value="${role.deleted}" /> <c:if test="${deleted=='N'}">
							<c:out value="ACTIVE" />
						</c:if> <c:if test="${deleted=='Y'}">
							<c:out value="INACTIVE" />
						</c:if></td>
					<td>
						<button class="btn btn-approve" onClick="update('${role.role.role}','${role.validFrom}','${role.validTo}','${role.id}')">Update</button>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="modal fade" id="addUser" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">X</button>
					<h4 id="title" class="modal-title">Add Role To '${users.username}'</h4>
				</div>
				<div class="modal-body">
					<fieldset class="the-fieldset">
						<legend class="the-legend">Role</legend>
						<form id="user" name="user">
							<div class="form-group">
								<label class="col-md-4">Role</label> <select style="border-radius:6px;"
									class="form-control" id="role" name="role">
									<c:forEach items="${roleUser}" var="role">
										<option value="${role.role}">${role.role}</option>
									</c:forEach>
								</select>
							</div>
							<div class="form-group">
								<label class="col-md-4">Valid From</label> 
								<input type="text" id="validFrom" name="validFrom" class="form-control" placeholder="Valid From">
							</div>
							<div class="form-group">
								<label class="col-md-4">Valid To</label> 
								<input type="text" name="validTo" id="validTo" class="form-control" placeholder="Valid To">
							</div>
							<div class="text-right">
								<button type="button" id="cancelBtn" name="cancelBtn" class="btn btn-red">Cancel</button>
								<button type="button" id="deactiveBtn" name="deactiveBtn" class="btn btn-red">Deactive</button>
								<button type="button" onClick="save()" id="saveBtn" name="saveBtn" class="btn btn-blue">Save</button>
							</div>
						</form>
					</fieldset>
				</div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<jsp:include page="fragment/footer.jsp"/>
	<script type="text/javascript">
		t.order([ 3, 'asc' ]).draw();
		var typeProcess = '';
		var id = "";
		$('#add').on('click', function() {
			typeProcess = 'Save';
			$("form")[0].reset();
			$('#title').text(typeProcess + " Role");
			$('#addUser').modal('show');
			$('#cancelBtn').hide();
			$('#deactiveBtn').hide();
			$('#role').val("");
			$('#validTo').val("");
			$('#validFrom').val("");
			$("#validFrom").datepicker();
			$("#validTo").datepicker();
		});
		$('#deactiveBtn').on('click', function() {
			$('#infoConfirmation').text("Are you Sure Want To Deactive?");
			$('#myModalConfirmation').modal('show');
		});
		$('#ok').on('click',function() {
			var data = getData();
			data["id"] = id;
			$('#myModalConfirmation').modal('hide');
			postData(data, '/cofin-ui/user/role/activation', typeProcess + " data");
		});
		$('#cancelBtn').on('click', function() {
			$("#cancelBtn").hide();
			$('#title').text('Cancel ' + typeProcess + '!');
			$("#role").removeAttr("disabled");
			$("#validFrom").removeAttr("disabled");
			$("#validTo").removeAttr("disabled");
			$('#saveBtn').text('Save');
			if (typeProcess == 'Save') {
				$('#deactiveBtn').hide();
			} else {
				$('#deactiveBtn').show();
			}
		});
		function save() {
			$("#add").prop("disabled", true);
			if ($("#role").val() == '' || $("#validFrom").val() == '' || $("#validTo").val() == '') {
				$("#info").html("Complete the form before save!");
			} else {
				if ($('#saveBtn').text() == 'Save Now' || $('#saveBtn').text() == 'Update Now') {
					var data = getData();
					data["typeProcess"] = typeProcess;
					data["id"] = id;
					postData(data, '/cofin-ui/user/role/save', typeProcess + " data");
				}
				$("#user :input:text").attr("disabled", true);
				$("#role").prop('disabled','disabled');
				$("#validFrom").prop('disabled','disabled');
				$("#validTo").prop('disabled','disabled');
				$('#title').text('Are you sure want to ' + typeProcess + '?');
				$('#saveBtn').text(typeProcess + " Now");
				$('#cancelBtn').show();
				$('#deactiveBtn').hide();
			}
		}
		function update(role, validFrom, validTo, idRoleMap) {
			typeProcess = 'Update';
			id = idRoleMap;
			$('#role').val(role);
			$('#validTo').val(validTo);
			$('#validFrom').val(validFrom);
			$('#addUser').modal('show');
			$('#cancelBtn').hide();
			$('#saveBtn').text(typeProcess);
			$('#title').text(typeProcess + " User Role");
			$('#deactiveBtn').show();
			formatDate($('#validFrom').val(), $('#validTo').val());
		}
		function formatDate(validFrom, validTo) {
			var valFrom = validFrom.split("-");
			var newFormatFrom = valFrom[2] + "/" + valFrom[1] + "/"
					+ valFrom[0];
			$("#validFrom").val(newFormatFrom);
			$("#validFrom").datepicker({
				format : 'dd/MM/yyyy'
			});
			var valTo = validTo.split("-");
			var newFormatTo = valTo[2] + "/" + valTo[1] + "/" + valTo[0];
			$("#validTo").val(newFormatTo);
			$("#validTo").datepicker({
				format : 'dd/MM/yyyy'
			});

		}
		function getData() {
			var data = {};
			data["role"] = $("#role").val();
			data["username"] = '${users.username}';
			data["validFrom"] = $("#validFrom").val();
			data["validTo"] = $("#validTo").val();
			return data;
		}
		$('#ubah').on('click',function(){
			alert("Testing");
		});
	</script>
	</body>
</html>
