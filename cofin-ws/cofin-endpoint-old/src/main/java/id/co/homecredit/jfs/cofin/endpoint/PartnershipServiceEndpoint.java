package id.co.homecredit.jfs.cofin.endpoint;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import id.co.homecredit.jfs.cofin.endpoint.service.PartnerService;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipResponse;

/**
 * The Class AccountService.
 */
@Endpoint
public class PartnershipServiceEndpoint {
    private static final Logger LOG = LogManager.getLogger(PartnershipServiceEndpoint.class);
    private static final String TARGET_NAMESPACE = "http://homecredit.net/homerselect/ws/financialpartnership/partnership/v2";

    @Autowired
    private PartnerService partnershipService;

    @PayloadRoot(localPart = "GetContractJFSPartnershipRequest", namespace = TARGET_NAMESPACE)
    public @ResponsePayload GetContractJFSPartnershipResponse getAccountDetails(
            @RequestPayload GetContractJFSPartnershipRequest request) throws Exception {
        printRequest(request);
        GetContractJFSPartnershipResponse response = partnershipService
                .getContractJFSPartnershipResponse(request);
        printResponse(response);
        return response;
    }

    /**
     * Print request to log.
     *
     * @param request
     * @return
     * @throws JAXBException
     */
    private void printRequest(GetContractJFSPartnershipRequest request) throws JAXBException {
        StringWriter sw = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(GetContractJFSPartnershipRequest.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(request, sw);
        LOG.debug("incoming request : " + sw.toString());
    }

    /**
     * Print response to log.
     *
     * @param request
     * @return
     * @throws JAXBException
     */
    private void printResponse(GetContractJFSPartnershipResponse response) throws JAXBException {
        StringWriter sw = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(GetContractJFSPartnershipResponse.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(response, sw);
        LOG.debug("outgoing response : " + sw.toString());
    }
}