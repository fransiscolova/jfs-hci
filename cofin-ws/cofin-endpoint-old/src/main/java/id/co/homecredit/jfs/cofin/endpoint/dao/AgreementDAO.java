package id.co.homecredit.jfs.cofin.endpoint.dao;

import id.co.homecredit.jfs.cofin.endpoint.model.Agreement;

public interface AgreementDAO extends DAO<Agreement> {

    /**
     * Get active agreement by code.
     *
     * @param agreementCode
     * @return agreement
     * @throws Exception
     */
    public Agreement getActiveAgreementByCode(String agreementCode);
}
