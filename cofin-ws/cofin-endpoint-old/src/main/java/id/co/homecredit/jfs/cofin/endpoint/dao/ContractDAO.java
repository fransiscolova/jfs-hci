package id.co.homecredit.jfs.cofin.endpoint.dao;

import java.math.BigDecimal;

import id.co.homecredit.jfs.cofin.endpoint.model.Contract;

public interface ContractDAO extends DAO<Contract> {

    /**
     * Get sum of proposed loan by contract status and cuid.
     *
     * @param status
     * @param cuid
     * @return sum
     */
    public BigDecimal getTotalProposedLoanByStatusAndCuid(String status, String cuid);

    /**
     * Will return sum of {@code SEND_PRINCIPAL} amount from {@link Contract} by its {@code STATUS}
     * and {@link Partner} {@code NAME}.
     *
     * @param status
     * @param partnerName
     * @return
     */
    public BigDecimal getTotalProposedLoanByStatusAndPartnerName(String status, String partnerName);
}
