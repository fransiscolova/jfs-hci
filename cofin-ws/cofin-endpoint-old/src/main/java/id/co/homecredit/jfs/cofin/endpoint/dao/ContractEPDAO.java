package id.co.homecredit.jfs.cofin.endpoint.dao;

import java.math.BigDecimal;

import id.co.homecredit.jfs.cofin.endpoint.model.ContractEP;

public interface ContractEPDAO extends DAO<ContractEP> {

    /**
     * Get contract by contract number.
     *
     * @param contractNumber
     * @return contractEP
     */
    public ContractEP getContractByContractNumber(String contractNumber);

    /**
     * Get sum of proposed loan by contract status and cuid.
     *
     * @param status
     * @param cuid
     * @return sum
     */
    public BigDecimal getTotalProposedLoanByStatusAndCuid(String status, String cuid);
}
