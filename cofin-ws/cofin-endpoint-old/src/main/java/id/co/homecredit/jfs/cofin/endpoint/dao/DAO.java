package id.co.homecredit.jfs.cofin.endpoint.dao;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author muhammad.muflihun
 *
 */
@Transactional
public abstract interface DAO<ET> {

    /**
     * Save entity.
     *
     * @param entity
     * @return entity
     */
    public ET save(ET entity);
}
