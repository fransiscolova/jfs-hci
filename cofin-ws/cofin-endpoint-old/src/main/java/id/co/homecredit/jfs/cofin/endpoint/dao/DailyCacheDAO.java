package id.co.homecredit.jfs.cofin.endpoint.dao;

import id.co.homecredit.jfs.cofin.endpoint.model.DailyCache;

public interface DailyCacheDAO extends DAO<DailyCache> {

    /**
     * Get daily cache by partner name.
     *
     * @param partnerName
     * @return dailyCache
     */
    public DailyCache getDailyCacheByPartnerName(String partnerName);

}
