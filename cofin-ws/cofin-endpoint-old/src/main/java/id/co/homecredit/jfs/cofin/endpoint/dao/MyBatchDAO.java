package id.co.homecredit.jfs.cofin.endpoint.dao;

import id.co.homecredit.jfs.cofin.endpoint.model.MyBatch;

/**
 * @author muhammad.muflihun
 *
 */
public interface MyBatchDAO extends DAO<MyBatch> {

    /**
     * Get my batch by my batch id.
     *
     * @param jobId
     * @return myBatch
     */
    public MyBatch getMyBatchById(String jobId);
}
