package id.co.homecredit.jfs.cofin.endpoint.dao;

import id.co.homecredit.jfs.cofin.endpoint.model.MyBatchLog;

public interface MyBatchLogDAO extends DAO<MyBatchLog> {
}
