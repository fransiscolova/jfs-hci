package id.co.homecredit.jfs.cofin.endpoint.dao;

import java.util.Date;

import id.co.homecredit.jfs.cofin.endpoint.model.PaperlessPos;

public interface PaperlessPosDAO extends DAO<PaperlessPos> {

    /**
     * Get paperless pos by salesroom code, product code, and date.
     *
     * @param salesroomCode
     * @param productCode
     * @param date
     * @return paperlessPos
     */
    public PaperlessPos getPaperlessPosBySalesroomCodeAndProductCode(String salesroomCode,
            String productCode, Date date);
}
