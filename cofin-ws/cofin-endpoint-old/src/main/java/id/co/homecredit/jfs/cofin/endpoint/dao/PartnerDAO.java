package id.co.homecredit.jfs.cofin.endpoint.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.endpoint.model.Partner;

public interface PartnerDAO extends DAO<Partner> {

    /**
     * Get all active {@link Partner} where {@code IS_DELETE} is 'N'.
     *
     * @return partners
     */
    public List<Partner> getAllActivePartner();

    /**
     * Get all active partner where {@code IS_DELETE} is 'N', {@code IS_CHECKING_ELIGIBILITY} is 'Y'
     * and ordered by priority.
     *
     * @return partners
     * @throws Exception
     */
    public List<Partner> getAllActivePartnerOrderedByPriority();

    /**
     * Get partner by partner code.
     *
     * @param partnerCode
     * @return partner
     * @throws Exception
     */
    public Partner getPartnerByCode(String partnerCode);
}
