package id.co.homecredit.jfs.cofin.endpoint.dao;

import java.math.BigDecimal;

import id.co.homecredit.jfs.cofin.endpoint.model.PaymentAllocation;

/**
 * Payment allocation DAO interface.
 *
 * @author muhammad.muflihun
 *
 */
public interface PaymentAllocationDAO extends DAO<PaymentAllocation> {

    /**
     * Get sum of {@code AMTPRINCIPAL} by {@link Partner} {@code NAME} and {@code Contract}
     * {@code STATUS}.
     *
     * @param partnerName
     * @param status
     * @return
     */
    public BigDecimal getSumAmtPrincipalByPartnerNameAndStatus(String partnerName, String status);
}
