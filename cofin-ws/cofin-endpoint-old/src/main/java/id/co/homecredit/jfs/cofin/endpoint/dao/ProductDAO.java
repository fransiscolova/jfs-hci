package id.co.homecredit.jfs.cofin.endpoint.dao;

import java.math.BigDecimal;

import id.co.homecredit.jfs.cofin.endpoint.model.Product;

public interface ProductDAO extends DAO<Product> {

    /**
     * Get active product by agreement id and bank product code.
     *
     * @param agreementId
     * @param productCode
     * @return product
     * @throws Exception
     */
    public Product getActiveProductByAgreementIdAndBankProductCode(BigDecimal agreementId,
            String productCode);

}
