package id.co.homecredit.jfs.cofin.endpoint.dao;

import id.co.homecredit.jfs.cofin.endpoint.model.ProductHci;

public interface ProductHciDAO extends DAO<ProductHci> {

    /**
     * Get product hci by product code.
     *
     * @param productCode
     * @return productHci
     * @throws Exception
     */
    public ProductHci getProductHciByCode(String productCode);
}
