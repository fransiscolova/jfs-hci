package id.co.homecredit.jfs.cofin.endpoint.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.endpoint.model.ProductMapping;

public interface ProductMappingDAO extends DAO<ProductMapping> {

    /**
     * Get all active {@link ProductMapping} by product code and bank product code.
     *
     * @param productCode
     * @param bankProductCode
     * @return
     * @throws Exception
     */
    public ProductMapping getActiveMappingByProductCodeAndBankProductCode(String productCode,
            String bankProductCode);

    /**
     * Get all active {@link ProductMapping} entity by its {@code product code}.
     *
     * @param productCode
     * @return
     * @throws Exception
     */
    public List<ProductMapping> getAllActiveProductMappingByProductCode(String productCode);
}
