package id.co.homecredit.jfs.cofin.endpoint.dao;

import java.util.List;

import id.co.homecredit.jfs.cofin.endpoint.model.Tac;

public interface TacDAO extends DAO<Tac> {

    /**
     * Get all TAC by TAC number.
     *
     * @param tacNumber
     * @return
     */
    public List<Tac> getTacByTacNumber(String tacNumber);

    /**
     * Find TAC by TAC number and today's date.
     *
     * @param tacNumber
     * @return
     */
    public Tac getTacByTacNumberAndTodaysDate(String tacNumber);
}
