package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.AgreementDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.Agreement;

@Repository
public class AgreementDAOImpl extends DAOImpl<Agreement> implements AgreementDAO {
    private static final Logger LOG = LogManager.getLogger(AgreementDAOImpl.class);

    @Override
    public Agreement getActiveAgreementByCode(String agreementCode) {
        Date today = new Date();
        LOG.debug("get agreement by code " + agreementCode + " and date " + today);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("code", agreementCode));
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        return (Agreement) criteria.uniqueResult();
    }

}
