package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.ContractDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.Contract;

@Repository
public class ContractDAOImpl extends DAOImpl<Contract> implements ContractDAO {
    private static final Logger LOG = LogManager.getLogger(ContractDAOImpl.class);

    @Override
    public BigDecimal getTotalProposedLoanByStatusAndCuid(String status, String cuid) {
        LOG.debug("get sum proposed loan by status " + status + " and cuid " + cuid);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("cuid", cuid));
        criteria.add(Restrictions.eq("status", status));
        criteria.setProjection(Projections.sum("sendPrincipal"));
        BigDecimal total = (BigDecimal) criteria.uniqueResult();
        return total == null ? BigDecimal.ZERO : total;
    }

    @Override
    public BigDecimal getTotalProposedLoanByStatusAndPartnerName(String status,
            String partnerName) {
        LOG.debug("get sum proposed loan by status " + status + " and partner name " + partnerName);
        Criteria criteria = createCriteria();
        criteria.createAlias("idAgreement", "agreement");
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("partner.name", partnerName));
        criteria.add(Restrictions.eq("status", status));
        criteria.setProjection(Projections.sum("sendPrincipal"));
        BigDecimal total = (BigDecimal) criteria.uniqueResult();
        return total == null ? BigDecimal.ZERO : total;
    }

}
