package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.ContractEPDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.ContractEP;

@Repository
public class ContractEPDAOImpl extends DAOImpl<ContractEP> implements ContractEPDAO {
    private static final Logger LOG = LogManager.getLogger(ContractEPDAOImpl.class);

    @Override
    public ContractEP getContractByContractNumber(String contractNumber) {
        LOG.debug("get contract by contract number " + contractNumber);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("textContractNumber", contractNumber));
        return (ContractEP) criteria.uniqueResult();
    }

    @Override
    public BigDecimal getTotalProposedLoanByStatusAndCuid(String status, String cuid) {
        LOG.debug("get sum proposed loan by status " + status + " and cuid " + cuid);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("cuid", cuid));
        criteria.add(Restrictions.eq("status", status));
        criteria.setProjection(Projections.sum("sendPrincipal"));
        BigDecimal total = (BigDecimal) criteria.uniqueResult();
        return total == null ? BigDecimal.ZERO : total;
    }
}
