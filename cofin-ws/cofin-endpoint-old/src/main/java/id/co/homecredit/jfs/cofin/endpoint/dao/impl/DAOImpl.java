package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import id.co.homecredit.jfs.cofin.endpoint.dao.DAO;

/**
 * @author muhammad.muflihun
 *
 */
public class DAOImpl<ET> implements DAO<ET> {

    private Class<ET> entity;
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Constructor to define the entity class.
     */
    @SuppressWarnings("unchecked")
    public DAOImpl() {
        Type[] actualTypeArg = ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments();
        entity = (Class<ET>) actualTypeArg[0];
    }

    /**
     * Create criteria.
     *
     * @return criteria
     */
    protected Criteria createCriteria() {
        return getSession().createCriteria(entity);
    }

    /**
     * Get current session.
     *
     * @return session
     */
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Save or update entity.
     *
     * @param entity
     * @return entity
     */
    public ET save(ET entity) {
        getSession().saveOrUpdate(entity);
        return entity;
    }
}
