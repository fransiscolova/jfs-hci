package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.DailyCacheDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.DailyCache;

@Repository
public class DailyCacheDAOImpl extends DAOImpl<DailyCache> implements DailyCacheDAO {
    private static final Logger LOG = LogManager.getLogger(DailyCacheDAOImpl.class);

    @Override
    public DailyCache getDailyCacheByPartnerName(String partnerName) {
        LOG.debug("get daily cache by partner name {}", partnerName);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("partnerName", partnerName));
        return (DailyCache) criteria.uniqueResult();
    }

}
