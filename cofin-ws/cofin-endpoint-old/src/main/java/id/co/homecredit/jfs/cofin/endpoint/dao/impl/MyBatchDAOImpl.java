package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.MyBatchDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.MyBatch;

/**
 * @author muhammad.muflihun
 *
 */
@Repository
public class MyBatchDAOImpl extends DAOImpl<MyBatch> implements MyBatchDAO {

    @Override
    public MyBatch getMyBatchById(String jobId) {
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("isActive", "Y"));
        criteria.add(Restrictions.eq("jobId", jobId));
        return (MyBatch) criteria.uniqueResult();
    }
}
