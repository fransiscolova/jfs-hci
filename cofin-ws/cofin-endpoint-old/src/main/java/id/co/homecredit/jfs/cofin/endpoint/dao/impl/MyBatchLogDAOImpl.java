package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.MyBatchLogDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.MyBatchLog;

@Repository
public class MyBatchLogDAOImpl extends DAOImpl<MyBatchLog> implements MyBatchLogDAO {

}
