package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.PaperlessPosDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.PaperlessPos;

/**
 * Dao implement class for {@link PaperlessPos}.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class PaperlessPosDAOImpl extends DAOImpl<PaperlessPos> implements PaperlessPosDAO {
    private static final Logger LOG = LogManager.getLogger(PaperlessPosDAOImpl.class);

    @Override
    public PaperlessPos getPaperlessPosBySalesroomCodeAndProductCode(String salesroomCode,
            String productCode, Date date) {
        LOG.debug("get paperless pos by salesroom code " + salesroomCode + ", product code "
                + productCode + ", and date " + date);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("salesroomCode", salesroomCode));
        criteria.add(Restrictions.eq("productCode", productCode));
        criteria.add(Restrictions.lt("validFrom", date));
        criteria.add(Restrictions.gt("validTo", date));
        return (PaperlessPos) criteria.uniqueResult();
    }

}
