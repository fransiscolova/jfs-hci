package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.PartnerDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.Partner;

@Repository
public class PartnerDAOImpl extends DAOImpl<Partner> implements PartnerDAO {
    private static final Logger LOG = LogManager.getLogger(PartnerDAOImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<Partner> getAllActivePartner() {
        LOG.debug("get all active partners");
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("isDelete", "N"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Partner> getAllActivePartnerOrderedByPriority() {
        LOG.debug("get all active partners");
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("isDelete", "N"));
        criteria.add(Restrictions.eq("isCheckingEligibility", "Y"));
        criteria.addOrder(Order.asc("priority"));
        return criteria.list();
    }

    @Override
    public Partner getPartnerByCode(String partnerCode) {
        LOG.debug("get partner by code " + partnerCode);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("id", partnerCode));
        return (Partner) criteria.uniqueResult();
    }

}
