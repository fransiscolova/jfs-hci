package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.PaymentAllocationDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.PaymentAllocation;

/**
 * Payment Allocation DAO implement class.
 *
 * @author muhammad.muflihun
 *
 */
@Repository
public class PaymentAllocationDAOImpl extends DAOImpl<PaymentAllocation>
        implements PaymentAllocationDAO {
    private static final Logger LOG = LogManager.getLogger(PaymentAllocationDAOImpl.class);

    @Override
    public BigDecimal getSumAmtPrincipalByPartnerNameAndStatus(String partnerName, String status) {
        LOG.debug("get sum total payment allocation by status " + status + " and partner name "
                + partnerName);
        Criteria criteria = createCriteria();
        criteria.createAlias("contract", "contract");
        criteria.createAlias("contract.idAgreement", "agreement");
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("partner.name", partnerName));
        criteria.add(Restrictions.eq("contract.status", status));
        criteria.setProjection(Projections.sum("amtPrincipal"));
        BigDecimal total = (BigDecimal) criteria.uniqueResult();
        return total == null ? BigDecimal.ZERO : total;
    }

}
