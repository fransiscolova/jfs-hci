package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.ProductDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.Product;

@Repository
public class ProductDAOImpl extends DAOImpl<Product> implements ProductDAO {
    private static final Logger LOG = LogManager.getLogger(ProductDAOImpl.class);

    public Product getActiveProductByAgreementIdAndBankProductCode(BigDecimal agreementId,
            String bankProductCode) {
        Date today = new Date();
        LOG.debug("get active product by agreement id " + agreementId + ", bank product code "
                + bankProductCode + ", and date " + today);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("bankProductCode", bankProductCode));
        criteria.add(Restrictions.eq("idAgreement", agreementId));
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        return (Product) criteria.uniqueResult();
    }

}