package id.co.homecredit.jfs.cofin.endpoint.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import id.co.homecredit.jfs.cofin.endpoint.dao.ProductHciDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.ProductHci;

@Repository
public class ProductHciDAOImpl extends DAOImpl<ProductHci> implements ProductHciDAO {
    private static final Logger LOG = LogManager.getLogger(ProductHciDAOImpl.class);

    public ProductHci getProductHciByCode(String productCode) {
        LOG.debug("get product by code " + productCode);
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq("codeProduct", productCode));
        return (ProductHci) criteria.uniqueResult();
    }
}