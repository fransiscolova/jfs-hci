package id.co.homecredit.jfs.cofin.endpoint.enumeration;

/**
 * Reflect code from entity {@link CommodityCategory} for partner Permata. <br>
 * Each commodity category code has maximum tenor allowed and minimum down payment.
 *
 * @author muhammad.muflihun
 *
 */
public enum CommodityCategoryPermata {
    CAT_CD("18", "10"), CAT_MP("12", "15"), CAT_CD_FUR("18", "10"), CAT_TV("18", "10"), CAT_GG(
            "12", "10"), CAT_LAP("12", "10"), CAT_SP("12", "15");

    private String tenor;
    private String downPayment;

    CommodityCategoryPermata(String tenor, String downPayment) {
        this.tenor = tenor;
        this.downPayment = downPayment;
    }

    /**
     * Get minimum down payment.
     *
     * @return
     */
    public String getDownPayment() {
        return downPayment;
    }

    /**
     * Get maximum tenor.
     *
     * @return
     */
    public String getTenor() {
        return tenor;
    }
}
