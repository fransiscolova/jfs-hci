package id.co.homecredit.jfs.cofin.endpoint.enumeration;

/**
 * Enum for product profile code.
 *
 * @author muhammad.muflihun
 *
 */
public enum ProductProfileCode {
    PP_ONLINE, PP_UNSEC
}
