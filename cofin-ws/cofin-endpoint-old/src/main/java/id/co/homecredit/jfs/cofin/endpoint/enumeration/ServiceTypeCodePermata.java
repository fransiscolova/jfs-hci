package id.co.homecredit.jfs.cofin.endpoint.enumeration;

/**
 * Service Type code enumeration that will be excluded for Permata.
 *
 * @author muhammad.muflihun
 *
 */
public enum ServiceTypeCodePermata {
    GIFTP
}
