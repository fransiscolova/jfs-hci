package id.co.homecredit.jfs.cofin.endpoint.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_daily_cache")
public class DailyCache implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String partnerName;
    private BigDecimal dailyLimit;
    private BigDecimal dailyLoan;
    private BigDecimal totalLoan;
    private String sendEmailFlag;

    @Column(name = "DAILY_LIMIT")
    public BigDecimal getDailyLimit() {
        return dailyLimit;
    }

    @Column(name = "DAILY_LOAN")
    public BigDecimal getDailyLoan() {
        return dailyLoan;
    }

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    @Column(name = "PARTNER_NAME")
    public String getPartnerName() {
        return partnerName;
    }

    @Column(name = "SEND_EMAIL")
    public String getSendEmailFlag() {
        return sendEmailFlag;
    }

    @Column(name = "TOTAL_LOAN")
    public BigDecimal getTotalLoan() {
        return totalLoan;
    }

    public void setDailyLimit(BigDecimal dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public void setDailyLoan(BigDecimal dailyLoan) {
        this.dailyLoan = dailyLoan;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public void setSendEmailFlag(String sendEmailFlag) {
        this.sendEmailFlag = sendEmailFlag;
    }

    public void setTotalLoan(BigDecimal totalLoan) {
        this.totalLoan = totalLoan;
    }

    @Override
    public String toString() {
        return "DailyCache [id=" + id + ", partnerName=" + partnerName + ", dailyLimit="
                + dailyLimit + ", dailyLoan=" + dailyLoan + ", totalLoan=" + totalLoan + "]";
    }

}
