package id.co.homecredit.jfs.cofin.endpoint.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "my_batch_log")
public class MyBatchLog {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", unique = true)
    private String id;

    @Column(name = "job_id", length = 200)
    private String jobId;

    @Column(name = "execute_by", length = 200)
    private String executeBy;

    @Column(name = "execute_date")
    private Date executeDate;

    @Column(name = "is_error", length = 1)
    private String isError;

    @Column(name = "error_log", length = 200)
    private String errorLog;

    public String getErrorLog() {
        return errorLog;
    }

    public String getExecuteBy() {
        return executeBy;
    }

    public Date getExecuteDate() {
        return executeDate;
    }

    public String getId() {
        return id;
    }

    public String getIsError() {
        return isError;
    }

    public String getJobId() {
        return jobId;
    }

    public void setErrorLog(String errorLog) {
        this.errorLog = errorLog;
    }

    public void setExecuteBy(String executeBy) {
        this.executeBy = executeBy;
    }

    public void setExecuteDate(Date executeDate) {
        this.executeDate = executeDate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    @Override
    public String toString() {
        return "MyBatchLog [id=" + id + ", jobId=" + jobId + ", executeBy=" + executeBy
                + ", executeDate=" + executeDate + ", isError=" + isError + ", errorLog=" + errorLog
                + "]";
    }
}
