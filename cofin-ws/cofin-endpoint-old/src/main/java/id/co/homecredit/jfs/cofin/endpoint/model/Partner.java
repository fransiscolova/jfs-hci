package id.co.homecredit.jfs.cofin.endpoint.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_partner")
public class Partner implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", length = 10)
    private String id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "address", length = 200)
    private String address;

    @Column(name = "main_contact", length = 100)
    private String mainContact;

    @Column(name = "phone_number", length = 20)
    private String phoneNumber;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "is_delete", length = 1)
    private String isDelete;

    @Column(name = "is_checking_eligibility", length = 1)
    private String isCheckingEligibility;

    @Column(name = "priority", length = 3)
    private Integer priority;

    public Partner() {
    }

    public Partner(String id, String name, String address, String mainContact, String phoneNumber,
            String email, String isDelete) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.mainContact = mainContact;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.isDelete = isDelete;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }

    public String getIsCheckingEligibility() {
        return isCheckingEligibility;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public String getMainContact() {
        return mainContact;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIsCheckingEligibility(String isCheckingEligibility) {
        this.isCheckingEligibility = isCheckingEligibility;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public void setMainContact(String mainContact) {
        this.mainContact = mainContact;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Partner [id=" + id + ", name=" + name + ", address=" + address + ", mainContact="
                + mainContact + ", phoneNumber=" + phoneNumber + ", email=" + email + ", isDelete="
                + isDelete + "]";
    }

}
