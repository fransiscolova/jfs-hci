package id.co.homecredit.jfs.cofin.endpoint.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "JFS_PAYMENT_ALLOCATION")
public class PaymentAllocation implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "ID")
    private String id;

    @ManyToOne
    @JoinColumn(name = "TEXT_CONTRACT_NUMBER")
    private Contract contract;

    @Column(name = "DUE_DATE")
    private Date dueDate;

    @Column(name = "PART_INDEX")
    private Integer partIndex;

    @Column(name = "AMT_PRINCIPAL")
    private BigDecimal amtPrincipal;

    @Column(name = "AMT_INTEREST")
    private BigDecimal amtInterest;

    @Column(name = "ARCHIVED")
    private Integer archieved;

    @Column(name = "DTIME_CREATED")
    private Date dtimeCreated;

    @Column(name = "DTIME_UPDATED")
    private Date dtimeUpdated;

    @Column(name = "PAYMENT_TYPE")
    private String paymentType;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "DATE_UPLOAD")
    private Date dateUpload;

    @Column(name = "FILENAME")
    private String filename;

    public BigDecimal getAmtInterest() {
        return amtInterest;
    }

    public BigDecimal getAmtPrincipal() {
        return amtPrincipal;
    }

    public Integer getArchieved() {
        return archieved;
    }

    public Contract getContract() {
        return contract;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getDateUpload() {
        return dateUpload;
    }

    public Date getDtimeCreated() {
        return dtimeCreated;
    }

    public Date getDtimeUpdated() {
        return dtimeUpdated;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public String getFilename() {
        return filename;
    }

    public String getId() {
        return id;
    }

    public Integer getPartIndex() {
        return partIndex;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setAmtInterest(BigDecimal amtInterest) {
        this.amtInterest = amtInterest;
    }

    public void setAmtPrincipal(BigDecimal amtPrincipal) {
        this.amtPrincipal = amtPrincipal;
    }

    public void setArchieved(Integer archieved) {
        this.archieved = archieved;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setDateUpload(Date dateUpload) {
        this.dateUpload = dateUpload;
    }

    public void setDtimeCreated(Date dtimeCreated) {
        this.dtimeCreated = dtimeCreated;
    }

    public void setDtimeUpdated(Date dtimeUpdated) {
        this.dtimeUpdated = dtimeUpdated;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPartIndex(Integer partIndex) {
        this.partIndex = partIndex;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public String toString() {
        return "PaymentAllocation [id=" + id + ", contract=" + contract + ", dueDate=" + dueDate
                + ", partIndex=" + partIndex + ", amtPrincipal=" + amtPrincipal + ", amtInterest="
                + amtInterest + ", archieved=" + archieved + ", dtimeCreated=" + dtimeCreated
                + ", dtimeUpdated=" + dtimeUpdated + ", paymentType=" + paymentType + ", createdBy="
                + createdBy + ", dateUpload=" + dateUpload + ", filename=" + filename + "]";
    }

}
