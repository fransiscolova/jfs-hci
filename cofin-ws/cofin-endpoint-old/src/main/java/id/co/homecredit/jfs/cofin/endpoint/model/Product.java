package id.co.homecredit.jfs.cofin.endpoint.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_product")
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", length = 40)
    private String id;

    @Column(name = "BANK_PRODUCT_CODE", length = 30)
    private String bankProductCode;

    @Column(name = "INTEREST_RATE", precision = 5, scale = 2)
    private BigDecimal interestRate;

    @Column(name = "PRINCIPAL_SPLIT_RATE", precision = 5, scale = 2)
    private BigDecimal principalSplitRate;

    @Column(name = "VALID_FROM")
    private Timestamp validFrom;

    @Column(name = "VALID_TO")
    private Timestamp validTo;

    @Column(name = "ID_AGREEMENT", precision = 5, scale = 2)
    private BigDecimal idAgreement;

    @Column(name = "ADMIN_FEE_RATE", precision = 5, scale = 2)
    private BigDecimal adminFeeRate;

    public BigDecimal getAdminFeeRate() {
        return adminFeeRate;
    }

    public String getBankProductCode() {
        return bankProductCode;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getIdAgreement() {
        return idAgreement;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public BigDecimal getPrincipalSplitRate() {
        return principalSplitRate;
    }

    public Timestamp getValidFrom() {
        return validFrom;
    }

    public Timestamp getValidTo() {
        return validTo;
    }

    public void setAdminFeeRate(BigDecimal adminFeeRate) {
        this.adminFeeRate = adminFeeRate;
    }

    public void setBankProductCode(String bankProductCode) {
        this.bankProductCode = bankProductCode;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdAgreement(BigDecimal idAgreement) {
        this.idAgreement = idAgreement;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public void setPrincipalSplitRate(BigDecimal principalSplitRate) {
        this.principalSplitRate = principalSplitRate;
    }

    public void setValidFrom(Timestamp validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTo(Timestamp validTo) {
        this.validTo = validTo;
    }

    @Override
    public String toString() {
        return "Product [ID " + id + ", INTEREST_RATE " + interestRate + ", PRINCIPAL_SPLIT_RATE "
                + principalSplitRate + ", VALID_FROM " + validFrom + ", VALID_TO " + validTo
                + ", ID_AGREEMENT " + idAgreement + ", ADMIN_FEE_RATE " + adminFeeRate + "]";
    }

}
