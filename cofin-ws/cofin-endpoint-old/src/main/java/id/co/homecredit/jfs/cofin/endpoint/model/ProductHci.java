package id.co.homecredit.jfs.cofin.endpoint.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_product_hci")
public class ProductHci implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", length = 20)
    private String id;

    @Column(name = "CODE_PRODUCT", length = 30)
    private String codeProduct;

    @Column(name = "NAME_PRODUCT", length = 255)
    private String nameProduct;

    @Column(name = "DTIME_INSERTED")
    private Timestamp dtimeInserted;

    public String getCodeProduct() {
        return codeProduct;
    }

    public Timestamp getDtimeInserted() {
        return dtimeInserted;
    }

    public String getId() {
        return id;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setCodeProduct(String codeProduct) {
        this.codeProduct = codeProduct;
    }

    public void setDtimeInserted(Timestamp dtimeInserted) {
        this.dtimeInserted = dtimeInserted;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    @Override
    public String toString() {
        return "Product HCI [ID " + id + ", CODE_PRODUCT " + codeProduct + ", NAME_PRODUCT "
                + nameProduct + ", DTIME_INSERTED " + dtimeInserted + "]";
    }

}
