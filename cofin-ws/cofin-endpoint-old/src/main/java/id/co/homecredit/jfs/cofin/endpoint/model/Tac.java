package id.co.homecredit.jfs.cofin.endpoint.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Type Allocation Code(TAC) is the first 6 or 8 digits <br>
 * from 15 digits International Mobile Equipment Identity(IMEI). <br>
 * TAC identifies a particular model and revision of wireless telephone.
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "TAC")
public class Tac implements Serializable {
    private static final long serialVersionUID = 8205797817641017509L;
    private String id;
    private String tac;
    private String manufacturer;
    private String modelName;
    private Date validFrom;
    private Date validTo;

    @Id
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    @Column(name = "MANUFACTURER", nullable = false)
    public String getManufacturer() {
        return manufacturer;
    }

    @Column(name = "MODEL_NAME", nullable = false)
    public String getModelName() {
        return modelName;
    }

    @Column(name = "TAC", length = 6, nullable = false)
    public String getTac() {
        return tac;
    }

    @Column(name = "VALID_FROM", nullable = false)
    public Date getValidFrom() {
        return validFrom;
    }

    @Column(name = "VALID_TO", nullable = false)
    public Date getValidTo() {
        return validTo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public void setTac(String tac) {
        this.tac = tac;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    @Override
    public String toString() {
        return "Tac [tac=" + tac + ", manufacturer=" + manufacturer + ", modelName=" + modelName
                + ", validFrom=" + validFrom + ", validTo=" + validTo + "]";
    }

}
