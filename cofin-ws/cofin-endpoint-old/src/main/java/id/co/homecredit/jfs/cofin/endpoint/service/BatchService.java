package id.co.homecredit.jfs.cofin.endpoint.service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Service interface to handle business logic related to batch.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface BatchService {

    /**
     * Start scheduler from configuration in database.
     */
    public void startScheduler();
}
