package id.co.homecredit.jfs.cofin.endpoint.service;

import java.math.BigDecimal;

import org.springframework.transaction.annotation.Transactional;

import id.co.homecredit.jfs.cofin.endpoint.model.Partner;

/**
 * Service interface to handle business logic related to cache.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface CacheService {

    /**
     * Recalculate add {@link Partner} loan limit cache.
     *
     * @param partnerName
     * @param isLoan
     * @param amount
     * @return
     */
    public BigDecimal addPartnerLimitCache(String partnerName, BigDecimal amount);

    /**
     * Calculate partner loan limit cache.
     */
    public void calculatePartnerLoanLimitCache();

    /**
     * Get {@link Partner} limit.
     *
     * @param partnerName
     * @param amount
     * @throws exeption
     * @return
     */
    public BigDecimal getLimitCacheByPartnerName(String partnerName);

    /**
     * Send warning email to user when JFS amount capacity over certain capacity.
     *
     * @param partnerName
     * @param partnerLimit
     */
    public void sendWarningEmail(String partnerName, BigDecimal partnerLimit);

    /**
     * Recalculate subtract {@link Partner} loan limit cache.
     *
     * @param partnerName
     * @param isLoan
     * @param amount
     * @return
     */
    public BigDecimal subtractPartnerLimitCache(String partnerName, BigDecimal amount);

}
