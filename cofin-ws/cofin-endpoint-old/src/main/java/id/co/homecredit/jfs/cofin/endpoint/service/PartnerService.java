package id.co.homecredit.jfs.cofin.endpoint.service;

import org.springframework.transaction.annotation.Transactional;

import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipResponse;

/**
 * Component class for implement business logic of partnership service endpoint.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
public interface PartnerService {

    /**
     * Process contract data to be determined which bank partner will we use as financing partner by
     * eligibility criteria.
     *
     * @param request
     * @return response
     * @throws Exception
     */
    public GetContractJFSPartnershipResponse getContractJFSPartnershipResponse(
            GetContractJFSPartnershipRequest request) throws Exception;
}
