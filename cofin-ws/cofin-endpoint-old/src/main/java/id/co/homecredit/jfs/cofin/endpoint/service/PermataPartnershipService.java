package id.co.homecredit.jfs.cofin.endpoint.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;

@Transactional
public interface PermataPartnershipService {

    /**
     * Business eligibility for Permata. Return null value if one of eligibility criteria doesn't
     * match.
     *
     * @param request
     * @return
     * @throws Exception
     */
    @Async
    public CompletableFuture<String> permataValidation(GetContractJFSPartnershipRequest request)
            throws Exception;

}
