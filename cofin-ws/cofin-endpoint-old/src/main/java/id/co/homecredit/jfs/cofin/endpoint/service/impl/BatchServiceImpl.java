package id.co.homecredit.jfs.cofin.endpoint.service.impl;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.endpoint.dao.MyBatchDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.MyBatch;
import id.co.homecredit.jfs.cofin.endpoint.service.BatchService;
import id.co.homecredit.jfs.cofin.endpoint.service.util.MyBatchJob;

/**
 * Service implement to handle business logic related to batch.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class BatchServiceImpl implements BatchService {
    private static final Logger LOG = LogManager.getLogger(BatchServiceImpl.class);

    private static final String SYNC_LOAN_JOB_NAME = "BATCH_DAILY_SYNCHRONIZE_LOAN_CACHE";

    @Autowired
    private MyBatchDAO myBatchDAO;

    @Override
    public void startScheduler() {
        LOG.info("starting scheduler for name " + SYNC_LOAN_JOB_NAME);
        try {
            MyBatch batch = myBatchDAO.getMyBatchById(SYNC_LOAN_JOB_NAME);
            JobDetail job = new JobDetail();
            job.setName(batch.getJobName());
            job.setJobClass(MyBatchJob.class);

            HashMap<String, Object> map = new HashMap<>();
            map.put("jobId", batch.getJobId());
            map.put("service", batch.getService());
            map.put("method", batch.getMethod());

            JobDataMap jobDataMap = new JobDataMap(map);
            job.setJobDataMap(jobDataMap);

            CronTrigger trigger = new CronTrigger();
            trigger.setName(batch.getJobName().concat(" ").concat("Trigger"));
            trigger.setCronExpression(batch.getCronTime());

            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job, trigger);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
