package id.co.homecredit.jfs.cofin.endpoint.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.endpoint.dao.ProductDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ProductHciDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ProductMappingDAO;
import id.co.homecredit.jfs.cofin.endpoint.enumeration.CommodityCategoryBtpn;
import id.co.homecredit.jfs.cofin.endpoint.enumeration.CommodityTypeBtpn;
import id.co.homecredit.jfs.cofin.endpoint.enumeration.ServiceTypeCodeBtpn;
import id.co.homecredit.jfs.cofin.endpoint.model.Product;
import id.co.homecredit.jfs.cofin.endpoint.model.ProductHci;
import id.co.homecredit.jfs.cofin.endpoint.model.ProductMapping;
import id.co.homecredit.jfs.cofin.endpoint.service.BtpnPartnershipService;
import id.co.homecredit.jfs.cofin.endpoint.service.util.CofinEPPropertyUtil;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Commodity;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;

/**
 * @author muhammad.muflihun
 *
 */
@Service
public class BtpnPartnershipServiceImpl implements BtpnPartnershipService {
    private static final Logger LOG = LogManager.getLogger(BtpnPartnershipServiceImpl.class);

    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private ProductHciDAO productHciDAO;
    @Autowired
    private ProductMappingDAO productMappingDAO;

    public CompletableFuture<String> btpnValidation(GetContractJFSPartnershipRequest request)
            throws Exception {
        String salesroomCode = request.getContract().getSalesroomCode();
        LOG.debug("salesroom code " + salesroomCode);
        String mpfVirtual = CofinEPPropertyUtil.get("mpf.virtual.salesroom.code");
        String mpfPaperless = CofinEPPropertyUtil.get("mpf.paperless.salesroom.code");
        String result = null;
        if (salesroomCode.equals(mpfVirtual) || salesroomCode.equals(mpfPaperless)) {
            result = btpnValidationMPF(request);
        } else {
            result = btpnValidationRegular(request);
        }
        return CompletableFuture.completedFuture(result);
    }

    /**
     * Filter eligibility specifically for FlexiFast / MPF.
     *
     * @param request
     * @return
     * @throws Exception
     */
    private String btpnValidationMPF(GetContractJFSPartnershipRequest request) throws Exception {
        String contractNumber = request.getContract().getContractCode();
        LOG.info("btpn mpf validation for contract " + contractNumber);
        BigDecimal reqAmount = request.getContract().getOfferFinancialParameters().getCreditAmount()
                .getAmount();

        // Check product code is active
        ProductHci productHci = productHciDAO
                .getProductHciByCode(request.getProduct().getProductCode());
        Product product = null;
        try {
            ProductMapping productMapping = productMappingDAO
                    .getActiveMappingByProductCodeAndBankProductCode(productHci.getCodeProduct(),
                            CofinEPPropertyUtil.get("btpn.name").concat("_PRODUCT").toUpperCase());
            product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
                    productMapping.getIdAgreement(), productMapping.getBankProductCode());
            if (product == null) {
                throw new Exception("Product is not mapped");
            }
        } catch (Exception e) {
            LOG.error("btpn mpf exception " + e);
            return null;
        }

        // Check max loan portion value
        BigDecimal partnerAmount = reqAmount.multiply(product.getPrincipalSplitRate()).setScale(0,
                RoundingMode.HALF_UP);
        BigDecimal mpfLoanTop = new BigDecimal(CofinEPPropertyUtil.get("btpn.mpf.loan.top"));
        BigDecimal mpfLoanBot = new BigDecimal(CofinEPPropertyUtil.get("btpn.mpf.loan.bot"));
        if (partnerAmount.compareTo(mpfLoanTop) > 0 || partnerAmount.compareTo(mpfLoanBot) < 0) {
            LOG.info("btpn amount " + partnerAmount + " not inside range " + mpfLoanBot + " and "
                    + mpfLoanTop);
            return null;
        }

        // Check max tenor
        Integer term = request.getContract().getOfferFinancialParameters().getTerms();
        Integer mpfTenorTop = Integer.parseInt(CofinEPPropertyUtil.get("btpn.mpf.tenor.top"));
        Integer mpfTenorBot = Integer.parseInt(CofinEPPropertyUtil.get("btpn.mpf.tenor.bot"));
        if (term > mpfTenorTop || term < mpfTenorBot) {
            LOG.info("btpn terms " + term + " not inside range " + mpfTenorBot + " and "
                    + mpfTenorTop);
            return null;
        }

        // Check service type code
        List<net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service> services = request
                .getProduct().getServices();
        for (net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service service : services) {
            for (ServiceTypeCodeBtpn enu : ServiceTypeCodeBtpn.values()) {
                if (enu.name().equalsIgnoreCase(service.getServiceTypeCode())) {
                    LOG.info("btpn service " + enu.name());
                    return null;
                }
            }
        }

        // Check age between 21 and 65 when contract signed
        Calendar firstDueDate = request.getContract().getOfferFinancialParameters()
                .getFirstDueDate().toGregorianCalendar();
        // minus 1 month
        firstDueDate.add(Calendar.MONTH, -1);
        LocalDate signingContract = LocalDate.of(firstDueDate.get(Calendar.YEAR),
                firstDueDate.get(Calendar.MONTH) + 1, firstDueDate.get(Calendar.DAY_OF_MONTH));
        Calendar dob = request.getCustomer().getBirthDate().toGregorianCalendar();
        LocalDate dateOfBirth = LocalDate.of(dob.get(Calendar.YEAR), dob.get(Calendar.MONTH) + 1,
                dob.get(Calendar.DAY_OF_MONTH));
        Period period = Period.between(dateOfBirth, signingContract);
        if (period.getYears() < 21 || period.getYears() > 65) {
            LOG.info("btpn dob " + dateOfBirth + " compared to " + signingContract);
            return null;
        }

        // TODO : check KTP valid date
        // // Check KTP active more than 15 days
        // // Calendar ktp =
        // request.getCustomer().getPrimaryIDDocuments().get(0).getDocumentAttributes().get(0);
        // Calendar ktp = new GregorianCalendar();
        // LocalDate ktpActiveDate = LocalDate.of(ktp.get(Calendar.YEAR), ktp.get(Calendar.MONTH) +
        // 1,
        // ktp.get(Calendar.DAY_OF_MONTH));
        // period = Period.between(ktpActiveDate, today.plusDays(15));
        // if (period.getDays() < 0) {
        // return null;
        // }

        // TODO: check loan purpose

        return CofinEPPropertyUtil.get("btpn.name");
    }

    /**
     * Filter eligibility specifically for regular.
     *
     * @param request
     * @return
     * @throws Exception
     */
    private String btpnValidationRegular(GetContractJFSPartnershipRequest request)
            throws Exception {
        String contractNumber = request.getContract().getContractCode();
        LOG.info("btpn regular validation for contract " + contractNumber);
        BigDecimal reqAmount = request.getContract().getOfferFinancialParameters().getCreditAmount()
                .getAmount();

        // Check product code is active
        ProductHci productHci = productHciDAO
                .getProductHciByCode(request.getProduct().getProductCode());
        Product product = null;
        try {
            ProductMapping productMapping = productMappingDAO
                    .getActiveMappingByProductCodeAndBankProductCode(productHci.getCodeProduct(),
                            CofinEPPropertyUtil.get("btpn.name").concat("_PRODUCT").toUpperCase());
            product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
                    productMapping.getIdAgreement(), productMapping.getBankProductCode());
            if (product == null) {
                throw new Exception("Product is not mapped");
            }
        } catch (Exception e) {
            LOG.error("btpn exception " + e);
            return null;
        }

        // Check commodity
        List<Commodity> commodities = request.getCommodity();
        if (commodities.size() > 1) {
            LOG.info("request has commodity size of " + commodities.size());
            return null;
        }

        // Check excluded commodity
        String commodityType = commodities.get(0).getCommodityTypeCode();
        for (String excludedCommodity : CofinEPPropertyUtil.get("btpn.exclude.commodity.types")
                .split(",")) {
            if (excludedCommodity.equals(commodityType)) {
                LOG.info("commodity type excluded for btpn " + commodityType);
                return null;
            }
        }

        // Check commodity to find terms and amount limit
        String commodityCategory = commodities.get(0).getCommodityCategoryCode();
        BigDecimal btpnAmountMin = BigDecimal.ZERO;
        BigDecimal btpnAmountMax = BigDecimal.ZERO;
        Integer btpnTenorMin = 0;
        Integer btpnTenorMax = 0;
        Boolean isSearching = true;
        if (isSearching) {
            for (CommodityCategoryBtpn enu : CommodityCategoryBtpn.values()) {
                if (enu.name().equalsIgnoreCase(commodityCategory)) {
                    LOG.info("commodity category matched " + enu.name());
                    isSearching = false;
                    btpnTenorMin = Integer.parseInt(enu.getTenorMin());
                    btpnTenorMax = Integer.parseInt(enu.getTenorMax());
                    btpnAmountMin = new BigDecimal(enu.getAmountMin());
                    btpnAmountMax = new BigDecimal(enu.getAmountMax());
                    LOG.info("tenor min " + btpnTenorMin);
                    LOG.info("tenor max " + btpnTenorMax);
                    LOG.info("amoun min " + btpnAmountMin);
                    LOG.info("amount max " + btpnAmountMax);
                    break;
                }
            }
        }
        if (isSearching) {
            for (CommodityTypeBtpn enu : CommodityTypeBtpn.values()) {
                if (enu.name().equalsIgnoreCase(commodityType)) {
                    LOG.info("commodity type matched " + enu.name());
                    isSearching = false;
                    btpnTenorMin = Integer.parseInt(enu.getTenorMin());
                    btpnTenorMax = Integer.parseInt(enu.getTenorMax());
                    btpnAmountMin = new BigDecimal(enu.getAmountMin());
                    btpnAmountMax = new BigDecimal(enu.getAmountMax());
                    LOG.info("tenor min " + btpnTenorMin);
                    LOG.info("tenor max " + btpnTenorMax);
                    LOG.info("amoun min " + btpnAmountMin);
                    LOG.info("amount max " + btpnAmountMax);
                    break;
                }
            }
        }
        if (isSearching) {
            LOG.info("type or category didn't match");
            return null;
        }

        // Check max loan portion value
        BigDecimal partnerAmount = reqAmount.multiply(product.getPrincipalSplitRate()).setScale(0,
                RoundingMode.HALF_UP);
        if (partnerAmount.compareTo(btpnAmountMax) > 0
                || partnerAmount.compareTo(btpnAmountMin) < 0) {
            LOG.info("btpn amount " + partnerAmount + " not inside range " + btpnAmountMin + " and "
                    + btpnAmountMax);
            return null;
        }

        // Check max tenor
        Integer term = request.getContract().getOfferFinancialParameters().getTerms();
        if (term > btpnTenorMax || term < btpnTenorMin) {
            LOG.info("btpn terms " + term + " not inside range " + btpnTenorMin + " and "
                    + btpnTenorMax);
            return null;
        }

        // Check service
        List<net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service> services = request
                .getProduct().getServices();
        for (net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service service : services) {
            if (service.getServiceCode()
                    .equalsIgnoreCase(CofinEPPropertyUtil.get("btpn.exclude.service.code"))) {
                LOG.info("btpn exclude service " + service.getServiceCode());
                return null;
            }
            for (ServiceTypeCodeBtpn enu : ServiceTypeCodeBtpn.values()) {
                if (enu.name().equalsIgnoreCase(service.getServiceTypeCode())) {
                    LOG.info("btpn service type " + enu.name());
                    return null;
                }
            }
        }

        // Check age between 21 and 65 when contract signed
        Calendar firstDueDate = request.getContract().getOfferFinancialParameters()
                .getFirstDueDate().toGregorianCalendar();
        // minus 1 month
        firstDueDate.add(Calendar.MONTH, -1);
        LocalDate signingContract = LocalDate.of(firstDueDate.get(Calendar.YEAR),
                firstDueDate.get(Calendar.MONTH) + 1, firstDueDate.get(Calendar.DAY_OF_MONTH));
        Calendar dob = request.getCustomer().getBirthDate().toGregorianCalendar();
        LocalDate dateOfBirth = LocalDate.of(dob.get(Calendar.YEAR), dob.get(Calendar.MONTH) + 1,
                dob.get(Calendar.DAY_OF_MONTH));
        Period period = Period.between(dateOfBirth, signingContract);
        if (period.getYears() < 21 || period.getYears() > 65) {
            LOG.info("btpn dob " + dateOfBirth + " compared to " + signingContract);
            return null;
        }

        request.getContract().getOfferFinancialParameters().getDownPayment().getAmount();

        // TODO : check KTP valid date
        // // Check KTP active more than 15 days
        // // Calendar ktp =
        // request.getCustomer().getPrimaryIDDocuments().get(0).getDocumentAttributes().get(0);
        // Calendar ktp = new GregorianCalendar();
        // LocalDate ktpActiveDate = LocalDate.of(ktp.get(Calendar.YEAR), ktp.get(Calendar.MONTH) +
        // 1,
        // ktp.get(Calendar.DAY_OF_MONTH));
        // period = Period.between(ktpActiveDate, today.plusDays(15));
        // if (period.getDays() < 0) {
        // return null;
        // }
        return CofinEPPropertyUtil.get("btpn.name");
    }

}
