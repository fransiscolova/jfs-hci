package id.co.homecredit.jfs.cofin.endpoint.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.common.util.EmailUtil;
import id.co.homecredit.jfs.cofin.endpoint.dao.ContractDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.DailyCacheDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.PartnerDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.PaymentAllocationDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.DailyCache;
import id.co.homecredit.jfs.cofin.endpoint.model.Partner;
import id.co.homecredit.jfs.cofin.endpoint.service.CacheService;
import id.co.homecredit.jfs.cofin.endpoint.service.util.CofinEPPropertyUtil;

/**
 * Service implement to handle business logic related to cache.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class CacheServiceImpl implements CacheService {
    private static final Logger LOG = LogManager.getLogger(CacheServiceImpl.class);

    @Autowired
    private PartnerDAO partnerDAO;
    @Autowired
    private DailyCacheDAO dailyCacheDAO;
    @Autowired
    private ContractDAO contractDAO;
    @Autowired
    private PaymentAllocationDAO paymentAllocationDAO;

    @Override
    public BigDecimal addPartnerLimitCache(String partnerName, BigDecimal amount) {
        return recalculateLimitCacheByPartnerNameAndAmount(partnerName, amount);
    }

    @Override
    public void calculatePartnerLoanLimitCache() {
        List<Partner> partners = partnerDAO.getAllActivePartner();
        LOG.debug(partners.toString());
        calculatePartnerLoanLimitCache(partners);
    }

    /**
     * Calculate {@link Partner} total loan limit.
     */
    private void calculatePartnerLoanLimitCache(List<Partner> partners) {
        for (Partner partner : partners) {
            String partnerName = partner.getName();
            String statusActive = CofinEPPropertyUtil.get("status.active");
            BigDecimal totalPartnerLoan = contractDAO
                    .getTotalProposedLoanByStatusAndPartnerName(statusActive, partnerName);
            BigDecimal totalPartnerPaymentAllocation = paymentAllocationDAO
                    .getSumAmtPrincipalByPartnerNameAndStatus(statusActive, partnerName);
            BigDecimal partnerLimit = totalPartnerLoan.subtract(totalPartnerPaymentAllocation);
            LOG.debug("partner " + partnerName + " has limit " + partnerLimit);
            updateDailyCacheData(partnerName, partnerLimit);
            sendWarningEmail(partnerName, partnerLimit);
        }
    }

    /**
     * Get cache from DB.
     *
     * @param partnerName
     * @return dailyCache
     */
    private DailyCache getDailyCacheData(String partnerName) {
        return dailyCacheDAO.getDailyCacheByPartnerName(partnerName);
    }

    @Override
    public BigDecimal getLimitCacheByPartnerName(String partnerName) {
        DailyCache dailyCache = getDailyCacheData(partnerName);
        return dailyCache.getTotalLoan();
    }

    /**
     * Recalculate {@link Partner} limit without save the data to cache.
     *
     * @param partnerName
     * @param amount
     * @return bigDecimal
     */
    private BigDecimal recalculateLimitCacheByPartnerNameAndAmount(String partnerName,
            BigDecimal amount) {
        BigDecimal partnerLimit = getLimitCacheByPartnerName(partnerName).add(amount);
        LOG.debug("limit after " + partnerLimit);
        return partnerLimit;
    }

    @Override
    public void sendWarningEmail(String partnerName, BigDecimal partnerLimit) {
        // if reached its limit, send an email
        String permataName = CofinEPPropertyUtil.get("permata.name");
        String btpnName = CofinEPPropertyUtil.get("btpn.name");
        DailyCache dailyCache = dailyCacheDAO.getDailyCacheByPartnerName(partnerName);
        if (partnerName.equalsIgnoreCase(permataName)) {
            BigDecimal permataLoanWarning = new BigDecimal(
                    CofinEPPropertyUtil.get("permata.loan.warning"));
            if (partnerLimit.compareTo(permataLoanWarning) >= 0) {
                if (dailyCache.getSendEmailFlag().equalsIgnoreCase("Y")) {
                    try {
                        String hostname = CofinEPPropertyUtil.get("smtp.host.name");
                        Integer port = Integer.parseInt(CofinEPPropertyUtil.get("smtp.port"));
                        String username = CofinEPPropertyUtil.get("smtp.username");
                        String password = CofinEPPropertyUtil.get("smtp.password");
                        String fromEmail = CofinEPPropertyUtil.get("smtp.from.email");
                        String fromName = CofinEPPropertyUtil.get("smtp.from.name");
                        String bounceEmail = CofinEPPropertyUtil.get("smtp.bounce.email");
                        String textMessage = CofinEPPropertyUtil.get("smtp.text");
                        EmailUtil emailUtil = new EmailUtil(hostname, port, username, password,
                                fromEmail, fromName, bounceEmail, textMessage);

                        String subject = "JFS Cofin - Permata loan reached 90% of max capacity";
                        String message = "As per subject.\nThank You";
                        String to = "elsa.destiana@homecredit.co.id";
                        String cc = "kwee.ilona@homecredit.co.id";
                        String bcc = "it.baudevelopment@hcg.homecredit.net";
                        emailUtil.sendSimpleEmail(subject, message, to, cc, bcc);
                        LOG.info("Email sent");
                        dailyCache.setSendEmailFlag("N");
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOG.error("Error sending warning email!");
                        LOG.error(e);
                    }
                }
            } else {
                dailyCache.setSendEmailFlag("Y");
                dailyCacheDAO.save(dailyCache);
            }
        } else if (partnerName.equalsIgnoreCase(btpnName)) {

        }
    }

    @Override
    public BigDecimal subtractPartnerLimitCache(String partnerName, BigDecimal amount) {
        BigDecimal minus = BigDecimal.ONE.negate();
        return recalculateLimitCacheByPartnerNameAndAmount(partnerName, amount.multiply(minus));
    }

    /**
     * Update partner cache data to DB.
     *
     * @param partnerName
     * @param partnerLimit
     */
    private void updateDailyCacheData(String partnerName, BigDecimal partnerLimit) {
        DailyCache dailyCache = getDailyCacheData(partnerName);
        dailyCache.setTotalLoan(partnerLimit);
        dailyCacheDAO.save(dailyCache);
    }

    /**
     * Private method to reset all daily loan for each partner to zero.
     */
    public void voidDailyLimit() {
        List<Partner> partners = partnerDAO.getAllActivePartner();
        for (Partner partner : partners) {
            DailyCache dailyCache = dailyCacheDAO.getDailyCacheByPartnerName(partner.getName());
            dailyCache.setDailyLoan(BigDecimal.ZERO);
            dailyCacheDAO.save(dailyCache);
        }
    }

}
