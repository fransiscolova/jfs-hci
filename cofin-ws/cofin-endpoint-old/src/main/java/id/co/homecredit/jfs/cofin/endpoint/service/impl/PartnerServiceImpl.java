package id.co.homecredit.jfs.cofin.endpoint.service.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.endpoint.dao.AgreementDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ContractEPDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.DailyCacheDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.PaperlessPosDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.PartnerDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ProductDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ProductHciDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ProductMappingDAO;
import id.co.homecredit.jfs.cofin.endpoint.enumeration.ProductProfileCode;
import id.co.homecredit.jfs.cofin.endpoint.model.Agreement;
import id.co.homecredit.jfs.cofin.endpoint.model.ContractEP;
import id.co.homecredit.jfs.cofin.endpoint.model.DailyCache;
import id.co.homecredit.jfs.cofin.endpoint.model.Partner;
import id.co.homecredit.jfs.cofin.endpoint.model.Product;
import id.co.homecredit.jfs.cofin.endpoint.model.ProductHci;
import id.co.homecredit.jfs.cofin.endpoint.model.ProductMapping;
import id.co.homecredit.jfs.cofin.endpoint.service.BtpnPartnershipService;
import id.co.homecredit.jfs.cofin.endpoint.service.CacheService;
import id.co.homecredit.jfs.cofin.endpoint.service.PartnerService;
import id.co.homecredit.jfs.cofin.endpoint.service.PermataPartnershipService;
import id.co.homecredit.jfs.cofin.endpoint.service.util.CofinEPPropertyUtil;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.ContractJFSPartnership;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipResponse;

/**
 * Component class for implement business logic of partnership service endpoint.
 *
 * @author muhammad.muflihun
 *
 */
@Service
public class PartnerServiceImpl implements PartnerService {
    private static final Logger LOG = LogManager.getLogger(PartnerServiceImpl.class);

    @Autowired
    private PartnerDAO partnerDAO;
    @Autowired
    private ContractEPDAO contractEPDAO;
    @Autowired
    private AgreementDAO agreementDAO;
    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private ProductHciDAO productHciDAO;
    @Autowired
    private ProductMappingDAO productMappingDAO;
    @Autowired
    private DailyCacheDAO dailyCacheDAO;
    @Autowired
    private PaperlessPosDAO paperlessPosDAO;
    @Autowired
    private PermataPartnershipService permataService;
    @Autowired
    private BtpnPartnershipService btpnService;
    @Autowired
    private CacheService cacheService;

    /**
     * Convert date to XMLGregorian date.
     *
     * @param date
     * @return
     * @throws DatatypeConfigurationException
     */
    private XMLGregorianCalendar dateToXmlDate(Date date) throws DatatypeConfigurationException {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
    }

    /**
     * Generate response. Use BTPN by default if null.
     *
     * @param partnerCode
     * @param agreementCode
     * @param partnershipShare
     * @param validFrom
     * @param validTo
     * @return response
     */
    private GetContractJFSPartnershipResponse generateResponse(String partnerCode,
            String agreementCode, BigDecimal partnershipShare, XMLGregorianCalendar validFrom,
            XMLGregorianCalendar validTo) {
        // TODO : temporary solution
        partnerCode = partnerCode == null ? CofinEPPropertyUtil.get("btpn.name") : partnerCode;
        partnershipShare = partnershipShare == null ? BigDecimal.ZERO : partnershipShare;
        GetContractJFSPartnershipResponse response = new GetContractJFSPartnershipResponse();
        ContractJFSPartnership contractJFSPartnership = new ContractJFSPartnership();
        contractJFSPartnership.setPartnerCode(partnerCode);
        contractJFSPartnership.setPartnershipAgreementCode(agreementCode);
        contractJFSPartnership.setPartnershipShare(partnershipShare);
        contractJFSPartnership.setValidFrom(validFrom);
        contractJFSPartnership.setValidTo(validTo);
        response.setContractFinancialPartnership(contractJFSPartnership);
        return response;
    }

    public GetContractJFSPartnershipResponse getContractJFSPartnershipResponse(
            GetContractJFSPartnershipRequest request) throws Exception {
        try {
            // check if contract is already exist
            String contractCode = request.getContract().getContractCode();
            ContractEP existContract = contractEPDAO.getContractByContractNumber(contractCode);
            if (existContract != null) {
                LOG.debug("contract " + contractCode + " is already in jfs contract ep");
                Agreement agreement = existContract.getIdAgreement();
                Product product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
                        new BigDecimal(agreement.getCode()), existContract.getBankProductCode());
                return generateResponse(agreement.getPartner().getName(),
                        agreement.getCodeAgreement(), product.getPrincipalSplitRate(),
                        dateToXmlDate(new Date(agreement.getValidFrom().getTime())),
                        dateToXmlDate(new Date(agreement.getValidTo().getTime())));
            }

            // check if contract come from online profile
            String productProfile = request.getProduct().getProductProfileCode();
            if (productProfile.equalsIgnoreCase(ProductProfileCode.PP_ONLINE.name())) {
                LOG.debug("product profile is from online " + productProfile);
                return generateResponse(null, null, null, null, null);
            }

            // check if salesroom code and product is for paperless
            String salesroomCode = request.getContract().getSalesroomCode();
            String productCode = request.getProduct().getProductCode();
            if (paperlessPosDAO.getPaperlessPosBySalesroomCodeAndProductCode(salesroomCode,
                    productCode, new Date()) != null) {
                LOG.debug("salesroom and product for paperless " + salesroomCode + " and "
                        + productCode);
                return generateResponse(null, null, null, null, null);
            }

            String partnerName = splittingContractValidation(request);
            LOG.info("partner validation result is " + partnerName);

            if (partnerName.equalsIgnoreCase(CofinEPPropertyUtil.get("non.jfs.name"))) {
                return generateResponse(null, null, null, null, null);
            }

            ProductHci productHci = productHciDAO
                    .getProductHciByCode(request.getProduct().getProductCode());
            ProductMapping productMapping = productMappingDAO
                    .getActiveMappingByProductCodeAndBankProductCode(productHci.getCodeProduct(),
                            partnerName.concat("_PRODUCT").toUpperCase());
            Product product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
                    productMapping.getIdAgreement(), productMapping.getBankProductCode());
            Agreement agreement = agreementDAO
                    .getActiveAgreementByCode(product.getIdAgreement().toString());

            ContractEP contractEP = new ContractEP();
            // ContractStatus contractStatus = new ContractStatus();
            BigDecimal amtCredit = request.getContract().getOfferFinancialParameters()
                    .getCreditAmount().getAmount();
            int trm = request.getContract().getOfferFinancialParameters().getTerms();
            BigDecimal split = product.getPrincipalSplitRate();
            BigDecimal interest = product.getInterestRate();
            BigDecimal partnerAmount = amtCredit.multiply(split).setScale(0, RoundingMode.HALF_UP);

            MathContext mathContext = new MathContext(50);
            BigDecimal term = new BigDecimal(BigDecimal.ONE
                    .add(interest.divide(new BigDecimal("1200"), mathContext)).pow(trm).toString());
            contractEP.setTextContractNumber(request.getContract().getContractCode());
            contractEP.setStatus("X");
            // set export date to be tomorrow
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DATE, 1);
            contractEP.setExportDate(cal.getTime());
            contractEP.setSkpContract(BigDecimal.ZERO);
            contractEP.setSendPrincipal(partnerAmount);
            // change formula if term equals one
            if (term.equals(BigDecimal.ONE)) {
                contractEP.setSendInstalment(partnerAmount
                        .multiply(
                                interest.divide(new BigDecimal("1200"), mathContext).multiply(term))
                        .divide(BigDecimal.ONE).setScale(0, 4));
            } else {
                contractEP.setSendInstalment(partnerAmount
                        .multiply(
                                interest.divide(new BigDecimal("1200"), mathContext).multiply(term))
                        .divide(term.subtract(BigDecimal.ONE), mathContext).setScale(0, 4));
            }
            contractEP.setSendTenor(new BigDecimal(trm));
            contractEP.setSendRate(interest);
            contractEP.setDateFirstDue(request.getContract().getOfferFinancialParameters()
                    .getFirstDueDate().toGregorianCalendar().getTime());
            contractEP.setAmtInstalment(
                    request.getContract().getOfferFinancialParameters().getAnnuity().getAmount());
            contractEP.setAmtMonthlyfee(BigDecimal.ZERO);
            contractEP.setIdAgreement(agreement);
            contractEP.setBankProductCode(productMapping.getBankProductCode());
            contractEP.setCuid(request.getCustomer().getClientCuid());
            contractEPDAO.save(contractEP);

            // update cache for JFS contract
            if (!partnerName.equals(CofinEPPropertyUtil.get("non.jfs.name"))) {
                DailyCache dailyCache = dailyCacheDAO.getDailyCacheByPartnerName(partnerName);
                BigDecimal updatedLimit = dailyCache.getTotalLoan().add(partnerAmount);
                dailyCache.setTotalLoan(updatedLimit);
                dailyCache.setDailyLoan(dailyCache.getDailyLoan().add(partnerAmount));
                dailyCacheDAO.save(dailyCache);
                cacheService.sendWarningEmail(partnerName, updatedLimit);
            }
            return generateResponse(agreement.getPartner().getName(), agreement.getCodeAgreement(),
                    product.getPrincipalSplitRate(),
                    dateToXmlDate(new Date(agreement.getValidFrom().getTime())),
                    dateToXmlDate(new Date(agreement.getValidTo().getTime())));
        } catch (Exception e) {
            LOG.error("main exception " + e);
            e.printStackTrace();
        }
        return generateResponse(null, null, null, null, null);
    }

    /**
     * Validate business criteria to determine {@link Contract} will using certain {@link Partner}
     * or not.
     * <p>
     * Priority of checking partner is stored in {@link Partner} and a flag about whether if system
     * must check the partner criteria.
     *
     * @param request
     * @return partnerName
     * @throws Exception
     */
    private String splittingContractValidation(GetContractJFSPartnershipRequest request)
            throws Exception {
        List<Partner> partners = partnerDAO.getAllActivePartnerOrderedByPriority();
        LOG.debug("partners " + partners);

        Long start = System.currentTimeMillis();
        LOG.debug("start async process " + start);
        // do async process for each active partner
        CompletableFuture<String> permata = null;
        CompletableFuture<String> btpn = null;
        for (Partner partner : partners) {
            if (partner.getName().equalsIgnoreCase(CofinEPPropertyUtil.get("permata.name"))) {
                permata = permataService.permataValidation(request);
            } else if (partner.getName().equalsIgnoreCase(CofinEPPropertyUtil.get("btpn.name"))) {
                btpn = btpnService.btpnValidation(request);
            }
        }

        // wait to all async process done
        CompletableFuture.allOf(permata, btpn).join();
        Long finish = System.currentTimeMillis() - start;
        LOG.debug("finish async process " + finish);

        String permataName = permata.get();
        String btpnName = btpn.get();
        for (Partner partner : partners) {
            if (partner.getName().equalsIgnoreCase(CofinEPPropertyUtil.get("permata.name"))
                    && permataName != null) {
                return permataName;
            } else if (partner.getName().equalsIgnoreCase(CofinEPPropertyUtil.get("btpn.name"))
                    && btpnName != null) {
                return btpnName;
            }
        }
        return CofinEPPropertyUtil.get("non.jfs.name");
    }
}
