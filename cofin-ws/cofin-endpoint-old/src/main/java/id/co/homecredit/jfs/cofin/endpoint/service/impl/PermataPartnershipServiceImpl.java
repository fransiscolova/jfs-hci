package id.co.homecredit.jfs.cofin.endpoint.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.homecredit.jfs.cofin.endpoint.dao.ContractDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.DailyCacheDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ProductDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ProductHciDAO;
import id.co.homecredit.jfs.cofin.endpoint.dao.ProductMappingDAO;
import id.co.homecredit.jfs.cofin.endpoint.enumeration.CommodityCategoryPermata;
import id.co.homecredit.jfs.cofin.endpoint.enumeration.ServiceTypeCodePermata;
import id.co.homecredit.jfs.cofin.endpoint.model.DailyCache;
import id.co.homecredit.jfs.cofin.endpoint.model.Product;
import id.co.homecredit.jfs.cofin.endpoint.model.ProductHci;
import id.co.homecredit.jfs.cofin.endpoint.model.ProductMapping;
import id.co.homecredit.jfs.cofin.endpoint.service.PermataPartnershipService;
import id.co.homecredit.jfs.cofin.endpoint.service.util.CofinEPPropertyUtil;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;

/**
 * @author muhammad.muflihun
 *
 */
@Service
public class PermataPartnershipServiceImpl implements PermataPartnershipService {
    private static final Logger LOG = LogManager.getLogger(PermataPartnershipServiceImpl.class);

    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private ContractDAO contractDAO;
    @Autowired
    private ProductHciDAO productHciDAO;
    @Autowired
    private ProductMappingDAO productMappingDAO;
    @Autowired
    private DailyCacheDAO dailyCacheDAO;

    /**
     * Calculate dp amount from request amount and dp percentage.
     *
     * @param reqAmount
     * @param percentage
     * @return dpAmount
     */
    private BigDecimal percentCalculation(BigDecimal reqAmount, BigDecimal percentage) {
        return reqAmount.multiply(percentage).divide(new BigDecimal("100"));
    }

    @Override
    public CompletableFuture<String> permataValidation(GetContractJFSPartnershipRequest request)
            throws Exception {
        String result = permataValidationReg(request);
        return CompletableFuture.completedFuture(result);
    }

    /**
     * Permata validation for regular agreement.
     *
     * @param request
     * @return
     * @throws Exception
     */
    private String permataValidationReg(GetContractJFSPartnershipRequest request) throws Exception {
        String contractNumber = request.getContract().getContractCode();
        LOG.info("permata validation for contract " + contractNumber);
        BigDecimal reqAmount = request.getContract().getOfferFinancialParameters().getCreditAmount()
                .getAmount();

        // Check salesroom code
        String salesroomCode = request.getContract().getSalesroomCode();
        String mpfVirtual = CofinEPPropertyUtil.get("mpf.virtual.salesroom.code");
        String mpfPaperless = CofinEPPropertyUtil.get("mpf.paperless.salesroom.code");
        if (salesroomCode.equals(mpfVirtual) || salesroomCode.equals(mpfPaperless)) {
            LOG.info("salesroom code for mpf " + salesroomCode);
            return null;
        }

        // Check daily limit
        DailyCache dailyCache = dailyCacheDAO
                .getDailyCacheByPartnerName(CofinEPPropertyUtil.get("permata.name"));
        if (dailyCache.getDailyLoan().add(reqAmount).compareTo(dailyCache.getDailyLimit()) > 0) {
            LOG.info("permata daily limit is reached");
            return null;
        }

        // Check product is active
        ProductHci productHci = productHciDAO
                .getProductHciByCode(request.getProduct().getProductCode());
        Product product = null;
        try {
            ProductMapping productMapping = productMappingDAO
                    .getActiveMappingByProductCodeAndBankProductCode(productHci.getCodeProduct(),
                            CofinEPPropertyUtil.get("permata.name").concat("_PRODUCT")
                                    .toUpperCase());
            product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
                    productMapping.getIdAgreement(), productMapping.getBankProductCode());
            if (product == null) {
                throw new Exception("Product is not mapped");
            }
        } catch (Exception e) {
            LOG.error("permata exception " + e);
            return null;
        }

        // Check max loan portion value
        String cuid = request.getCustomer().getClientCuid();
        BigDecimal partnerAmount = reqAmount.multiply(product.getPrincipalSplitRate()).setScale(0,
                RoundingMode.HALF_UP);
        BigDecimal existingLoan = contractDAO
                .getTotalProposedLoanByStatusAndCuid(CofinEPPropertyUtil.get("status.active"), cuid)
                .add(partnerAmount);
        BigDecimal loadTop = new BigDecimal(CofinEPPropertyUtil.get("permata.loan.top"));
        if (existingLoan.compareTo(loadTop) > 0) {
            LOG.info("customer with " + cuid + " total amount is " + existingLoan + " more than "
                    + loadTop);
            return null;
        }

        // Check max total loan
        // BigDecimal partnerLimit = CacheBatch.addPartnerLimitCache(permataName, partnerAmount);
        BigDecimal partnerLimit = dailyCacheDAO
                .getDailyCacheByPartnerName(CofinEPPropertyUtil.get("permata.name")).getTotalLoan()
                .add(partnerAmount);
        BigDecimal loanMax = new BigDecimal(CofinEPPropertyUtil.get("permata.loan.max"));
        if (partnerLimit.compareTo(loanMax) == 1) {
            LOG.info("permata total loan " + partnerLimit + " more than " + loanMax);
            return null;
        }

        // TODO : refractor this to read multiple commodities.
        // Check commodity
        String commodityCategoryCode = request.getCommodity().get(0).getCommodityCategoryCode();
        CommodityCategoryPermata category = CommodityCategoryPermata.valueOf(commodityCategoryCode);

        // Check min down payment per category
        BigDecimal downPayment = request.getContract().getOfferFinancialParameters()
                .getDownPayment().getAmount();
        String categoryDP = category.getDownPayment();
        BigDecimal dpAmount = percentCalculation(reqAmount, new BigDecimal(categoryDP));
        if (categoryDP != null && !categoryDP.isEmpty() && downPayment.compareTo(dpAmount) < 0) {
            LOG.info("permata dp " + downPayment + " lesser than " + dpAmount);
            return null;
        }

        // Check max tenor per category
        String categoryTenor = category.getTenor();
        BigDecimal tenor = BigDecimal
                .valueOf(request.getContract().getOfferFinancialParameters().getTerms());
        if (categoryTenor != null && !categoryTenor.isEmpty()
                && tenor.compareTo(new BigDecimal(categoryTenor)) > 0) {
            LOG.info("permata terms " + tenor + " more than " + categoryTenor);
            return null;
        }

        // Check service type code
        List<net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service> services = request
                .getProduct().getServices();
        for (net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service service : services) {
            for (ServiceTypeCodePermata enu : ServiceTypeCodePermata.values()) {
                if (enu.name().equalsIgnoreCase(service.getServiceTypeCode())) {
                    LOG.info("permata service " + enu.name());
                    return null;
                }
            }
        }

        // Check age between 21 and 65 when contract signed
        Calendar firstDueDate = request.getContract().getOfferFinancialParameters()
                .getFirstDueDate().toGregorianCalendar();
        // minus 1 month
        firstDueDate.add(Calendar.MONTH, -1);
        LocalDate signingContract = LocalDate.of(firstDueDate.get(Calendar.YEAR),
                firstDueDate.get(Calendar.MONTH) + 1, firstDueDate.get(Calendar.DAY_OF_MONTH));
        Calendar dob = request.getCustomer().getBirthDate().toGregorianCalendar();
        LocalDate dateOfBirth = LocalDate.of(dob.get(Calendar.YEAR), dob.get(Calendar.MONTH) + 1,
                dob.get(Calendar.DAY_OF_MONTH));
        Period period = Period.between(dateOfBirth, signingContract);
        if (period.getYears() < 21 || period.getYears() > 65) {
            LOG.info("permata dob " + dateOfBirth + " compared to " + signingContract);
            return null;
        }

        // TODO : check KTP valid date
        // // Check KTP active more than 15 days
        // // Calendar ktp =
        // request.getCustomer().getPrimaryIDDocuments().get(0).getDocumentAttributes().get(0);
        // Calendar ktp = new GregorianCalendar();
        // LocalDate ktpActiveDate = LocalDate.of(ktp.get(Calendar.YEAR), ktp.get(Calendar.MONTH) +
        // 1,
        // ktp.get(Calendar.DAY_OF_MONTH));
        // period = Period.between(ktpActiveDate, today.plusDays(15));
        // if (period.getDays() < 0) {
        // LOG.debug("permata ktp " +partnerAmount+ " more than " +permataLoanTop);
        // return null;
        // }

        return CofinEPPropertyUtil.get("permata.name");
    }

}
