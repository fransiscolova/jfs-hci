package id.co.homecredit.jfs.cofin.endpoint.service.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import id.co.homecredit.jfs.cofin.endpoint.service.BatchService;
import id.co.homecredit.jfs.cofin.endpoint.service.CacheService;

/**
 * Listener class to doing action when application context is fully loaded or refreshed.
 *
 * @author muhammad.muflihun
 *
 */
@Component
public class ApplicationContextListener implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger LOG = LogManager.getLogger(ApplicationContextListener.class);
    private static Boolean firstTime = true;

    @Autowired
    private CacheService cacheService;
    @Autowired
    private BatchService batchService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
        LOG.debug("after loaded");
        if (firstTime) {
            LOG.info("after loaded first time");
            cacheService.calculatePartnerLoanLimitCache();
            batchService.startScheduler();
            firstTime = false;
        }
    }

}
