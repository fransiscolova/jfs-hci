package id.co.homecredit.jfs.cofin.endpoint.service.util;

import java.lang.reflect.Method;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import id.co.homecredit.jfs.cofin.endpoint.dao.MyBatchLogDAO;
import id.co.homecredit.jfs.cofin.endpoint.model.MyBatchLog;

public class MyBatchJob implements Job {

    @SuppressWarnings("resource")
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                    "cofin-endpoint-dao-context.xml");
            MyBatchLogDAO batchLogDAO = (MyBatchLogDAO) applicationContext.getBean("myBatchLogDAO");

            JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();

            MyBatchLog myBatchLog = new MyBatchLog();
            myBatchLog.setJobId((String) dataMap.get("jobId"));
            myBatchLog.setExecuteBy("SYSTEM");
            myBatchLog.setExecuteDate(new Date());
            myBatchLog.setIsError("P");
            batchLogDAO.save(myBatchLog);

            try {
                Object o = applicationContext.getBean((String) dataMap.get("service"));
                Method m = o.getClass().getDeclaredMethod((String) dataMap.get("method"),
                        new Class[] {});
                m.invoke(o);

                myBatchLog.setIsError("N");
                batchLogDAO.save(myBatchLog);

            } catch (Exception e) {
                e.printStackTrace();
                myBatchLog.setIsError("Y");
                myBatchLog.setErrorLog(e.getMessage());
                batchLogDAO.save(myBatchLog);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
