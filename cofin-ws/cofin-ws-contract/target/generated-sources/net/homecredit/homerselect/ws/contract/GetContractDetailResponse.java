
package net.homecredit.homerselect.ws.contract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.homecredit.homerselect.ws.contract.dto.ContractDetail;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contractDetail" type="{http://homecredit.net/homerselect/ws/contract/dto}ContractDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "contractDetail"
})
@XmlRootElement(name = "GetContractDetailResponse")
public class GetContractDetailResponse {

    protected List<ContractDetail> contractDetail;

    /**
     * Gets the value of the contractDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractDetail }
     * 
     * 
     */
    public List<ContractDetail> getContractDetail() {
        if (contractDetail == null) {
            contractDetail = new ArrayList<ContractDetail>();
        }
        return this.contractDetail;
    }

}
