
package net.homecredit.homerselect.ws.contract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.homecredit.homerselect.ws.contract.dto.ContractHistoryDto;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contractHistory" type="{http://homecredit.net/homerselect/ws/contract/dto}ContractHistoryDto" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "contractHistory"
})
@XmlRootElement(name = "GetContractHistoryResponse")
public class GetContractHistoryResponse {

    protected List<ContractHistoryDto> contractHistory;

    /**
     * Gets the value of the contractHistory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractHistory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractHistory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractHistoryDto }
     * 
     * 
     */
    public List<ContractHistoryDto> getContractHistory() {
        if (contractHistory == null) {
            contractHistory = new ArrayList<ContractHistoryDto>();
        }
        return this.contractHistory;
    }

}
