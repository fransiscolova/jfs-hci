
package net.homecredit.homerselect.ws.contract;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.homecredit.homerselect.ws.contract.dto.ContractFilter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="filter" type="{http://homecredit.net/homerselect/ws/contract/dto}ContractFilter"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cuid",
    "filter"
})
@XmlRootElement(name = "GetContractListRequest")
public class GetContractListRequest {

    protected long cuid;
    @XmlElement(required = true)
    protected ContractFilter filter;

    /**
     * Gets the value of the cuid property.
     * 
     */
    public long getCuid() {
        return cuid;
    }

    /**
     * Sets the value of the cuid property.
     * 
     */
    public void setCuid(long value) {
        this.cuid = value;
    }

    /**
     * Gets the value of the filter property.
     * 
     * @return
     *     possible object is
     *     {@link ContractFilter }
     *     
     */
    public ContractFilter getFilter() {
        return filter;
    }

    /**
     * Sets the value of the filter property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractFilter }
     *     
     */
    public void setFilter(ContractFilter value) {
        this.filter = value;
    }

}
