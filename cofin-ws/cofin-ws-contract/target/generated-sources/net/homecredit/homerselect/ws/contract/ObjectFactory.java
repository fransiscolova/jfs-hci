
package net.homecredit.homerselect.ws.contract;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.homecredit.homerselect.ws.contract package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.homecredit.homerselect.ws.contract
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetContractHistoryResponse }
     * 
     */
    public GetContractHistoryResponse createGetContractHistoryResponse() {
        return new GetContractHistoryResponse();
    }

    /**
     * Create an instance of {@link GetContractListRequest }
     * 
     */
    public GetContractListRequest createGetContractListRequest() {
        return new GetContractListRequest();
    }

    /**
     * Create an instance of {@link GetContractsForCrossChecksResponse }
     * 
     */
    public GetContractsForCrossChecksResponse createGetContractsForCrossChecksResponse() {
        return new GetContractsForCrossChecksResponse();
    }

    /**
     * Create an instance of {@link GetContractsResponse }
     * 
     */
    public GetContractsResponse createGetContractsResponse() {
        return new GetContractsResponse();
    }

    /**
     * Create an instance of {@link GetPersonCUIDByContractResponse }
     * 
     */
    public GetPersonCUIDByContractResponse createGetPersonCUIDByContractResponse() {
        return new GetPersonCUIDByContractResponse();
    }

    /**
     * Create an instance of {@link GetContractDetailResponse }
     * 
     */
    public GetContractDetailResponse createGetContractDetailResponse() {
        return new GetContractDetailResponse();
    }

    /**
     * Create an instance of {@link GetContractListResponse }
     * 
     */
    public GetContractListResponse createGetContractListResponse() {
        return new GetContractListResponse();
    }

    /**
     * Create an instance of {@link GetContractsRequest }
     * 
     */
    public GetContractsRequest createGetContractsRequest() {
        return new GetContractsRequest();
    }

    /**
     * Create an instance of {@link GetContractsForCrossChecksRequest }
     * 
     */
    public GetContractsForCrossChecksRequest createGetContractsForCrossChecksRequest() {
        return new GetContractsForCrossChecksRequest();
    }

    /**
     * Create an instance of {@link GetContractHistoryRequest }
     * 
     */
    public GetContractHistoryRequest createGetContractHistoryRequest() {
        return new GetContractHistoryRequest();
    }

    /**
     * Create an instance of {@link GetPersonCUIDByContractRequest }
     * 
     */
    public GetPersonCUIDByContractRequest createGetPersonCUIDByContractRequest() {
        return new GetPersonCUIDByContractRequest();
    }

    /**
     * Create an instance of {@link GetContractDetailRequest }
     * 
     */
    public GetContractDetailRequest createGetContractDetailRequest() {
        return new GetContractDetailRequest();
    }

}
