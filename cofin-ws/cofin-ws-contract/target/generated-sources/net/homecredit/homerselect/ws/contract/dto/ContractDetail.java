
package net.homecredit.homerselect.ws.contract.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContractDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contractCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contractType" type="{http://homecredit.net/homerselect/ws/contract/dto}CreditType"/>
 *         &lt;element name="contractStatus" type="{http://homecredit.net/homerselect/ws/contract/dto}ContractStatusType"/>
 *         &lt;element name="initTransactionType" type="{http://homecredit.net/homerselect/ws/contract/dto}InitialTransactionType"/>
 *         &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productVersion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="productBusinessDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="providedCreditAmount" type="{http://homecredit.net/homerselect/ws/contract/dto}MoneyDto"/>
 *         &lt;element name="lastContractStatusChange" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="signatureDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="installmentDueDay" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
 *         &lt;element name="firstInstallmentDueDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="installmentDueDayShift" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
 *         &lt;element name="presentedInterestRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;choice>
 *           &lt;element name="contractParameterCEL" type="{http://homecredit.net/homerselect/ws/contract/dto}ClosedEndParameter"/>
 *           &lt;element name="contractParameterREL" type="{http://homecredit.net/homerselect/ws/contract/dto}RevlovingParameter"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractDetail", propOrder = {
    "contractCode",
    "contractType",
    "contractStatus",
    "initTransactionType",
    "productCode",
    "productVersion",
    "productBusinessDescription",
    "providedCreditAmount",
    "lastContractStatusChange",
    "signatureDate",
    "installmentDueDay",
    "firstInstallmentDueDate",
    "installmentDueDayShift",
    "presentedInterestRate",
    "contractParameterCEL",
    "contractParameterREL"
})
public class ContractDetail {

    @XmlElement(required = true)
    protected String contractCode;
    @XmlElement(required = true)
    protected CreditType contractType;
    @XmlElement(required = true)
    protected ContractStatusType contractStatus;
    @XmlElement(required = true)
    protected InitialTransactionType initTransactionType;
    @XmlElement(required = true)
    protected String productCode;
    protected int productVersion;
    @XmlElement(required = true)
    protected String productBusinessDescription;
    @XmlElement(required = true)
    protected MoneyDto providedCreditAmount;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastContractStatusChange;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar signatureDate;
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger installmentDueDay;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar firstInstallmentDueDate;
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger installmentDueDayShift;
    @XmlElement(required = true)
    protected BigDecimal presentedInterestRate;
    protected ClosedEndParameter contractParameterCEL;
    protected RevlovingParameter contractParameterREL;

    /**
     * Gets the value of the contractCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractCode() {
        return contractCode;
    }

    /**
     * Sets the value of the contractCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractCode(String value) {
        this.contractCode = value;
    }

    /**
     * Gets the value of the contractType property.
     * 
     * @return
     *     possible object is
     *     {@link CreditType }
     *     
     */
    public CreditType getContractType() {
        return contractType;
    }

    /**
     * Sets the value of the contractType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditType }
     *     
     */
    public void setContractType(CreditType value) {
        this.contractType = value;
    }

    /**
     * Gets the value of the contractStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ContractStatusType }
     *     
     */
    public ContractStatusType getContractStatus() {
        return contractStatus;
    }

    /**
     * Sets the value of the contractStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractStatusType }
     *     
     */
    public void setContractStatus(ContractStatusType value) {
        this.contractStatus = value;
    }

    /**
     * Gets the value of the initTransactionType property.
     * 
     * @return
     *     possible object is
     *     {@link InitialTransactionType }
     *     
     */
    public InitialTransactionType getInitTransactionType() {
        return initTransactionType;
    }

    /**
     * Sets the value of the initTransactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link InitialTransactionType }
     *     
     */
    public void setInitTransactionType(InitialTransactionType value) {
        this.initTransactionType = value;
    }

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the productVersion property.
     * 
     */
    public int getProductVersion() {
        return productVersion;
    }

    /**
     * Sets the value of the productVersion property.
     * 
     */
    public void setProductVersion(int value) {
        this.productVersion = value;
    }

    /**
     * Gets the value of the productBusinessDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductBusinessDescription() {
        return productBusinessDescription;
    }

    /**
     * Sets the value of the productBusinessDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductBusinessDescription(String value) {
        this.productBusinessDescription = value;
    }

    /**
     * Gets the value of the providedCreditAmount property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyDto }
     *     
     */
    public MoneyDto getProvidedCreditAmount() {
        return providedCreditAmount;
    }

    /**
     * Sets the value of the providedCreditAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyDto }
     *     
     */
    public void setProvidedCreditAmount(MoneyDto value) {
        this.providedCreditAmount = value;
    }

    /**
     * Gets the value of the lastContractStatusChange property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastContractStatusChange() {
        return lastContractStatusChange;
    }

    /**
     * Sets the value of the lastContractStatusChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastContractStatusChange(XMLGregorianCalendar value) {
        this.lastContractStatusChange = value;
    }

    /**
     * Gets the value of the signatureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSignatureDate() {
        return signatureDate;
    }

    /**
     * Sets the value of the signatureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSignatureDate(XMLGregorianCalendar value) {
        this.signatureDate = value;
    }

    /**
     * Gets the value of the installmentDueDay property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInstallmentDueDay() {
        return installmentDueDay;
    }

    /**
     * Sets the value of the installmentDueDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInstallmentDueDay(BigInteger value) {
        this.installmentDueDay = value;
    }

    /**
     * Gets the value of the firstInstallmentDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstInstallmentDueDate() {
        return firstInstallmentDueDate;
    }

    /**
     * Sets the value of the firstInstallmentDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstInstallmentDueDate(XMLGregorianCalendar value) {
        this.firstInstallmentDueDate = value;
    }

    /**
     * Gets the value of the installmentDueDayShift property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInstallmentDueDayShift() {
        return installmentDueDayShift;
    }

    /**
     * Sets the value of the installmentDueDayShift property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInstallmentDueDayShift(BigInteger value) {
        this.installmentDueDayShift = value;
    }

    /**
     * Gets the value of the presentedInterestRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPresentedInterestRate() {
        return presentedInterestRate;
    }

    /**
     * Sets the value of the presentedInterestRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPresentedInterestRate(BigDecimal value) {
        this.presentedInterestRate = value;
    }

    /**
     * Gets the value of the contractParameterCEL property.
     * 
     * @return
     *     possible object is
     *     {@link ClosedEndParameter }
     *     
     */
    public ClosedEndParameter getContractParameterCEL() {
        return contractParameterCEL;
    }

    /**
     * Sets the value of the contractParameterCEL property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClosedEndParameter }
     *     
     */
    public void setContractParameterCEL(ClosedEndParameter value) {
        this.contractParameterCEL = value;
    }

    /**
     * Gets the value of the contractParameterREL property.
     * 
     * @return
     *     possible object is
     *     {@link RevlovingParameter }
     *     
     */
    public RevlovingParameter getContractParameterREL() {
        return contractParameterREL;
    }

    /**
     * Sets the value of the contractParameterREL property.
     * 
     * @param value
     *     allowed object is
     *     {@link RevlovingParameter }
     *     
     */
    public void setContractParameterREL(RevlovingParameter value) {
        this.contractParameterREL = value;
    }

}
