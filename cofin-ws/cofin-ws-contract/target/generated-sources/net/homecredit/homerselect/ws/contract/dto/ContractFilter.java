
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContractFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contractType" type="{http://homecredit.net/homerselect/ws/contract/dto}CreditType"/>
 *         &lt;element name="initialTransactionType" type="{http://homecredit.net/homerselect/ws/contract/dto}InitialTransactionType"/>
 *         &lt;element name="contractStatus" type="{http://homecredit.net/homerselect/ws/contract/dto}ContractStatusType"/>
 *         &lt;element name="signatureDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractFilter", propOrder = {
    "contractType",
    "initialTransactionType",
    "contractStatus",
    "signatureDate"
})
public class ContractFilter {

    @XmlElement(required = true)
    protected CreditType contractType;
    @XmlElement(required = true)
    protected InitialTransactionType initialTransactionType;
    @XmlElement(required = true)
    protected ContractStatusType contractStatus;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar signatureDate;

    /**
     * Gets the value of the contractType property.
     * 
     * @return
     *     possible object is
     *     {@link CreditType }
     *     
     */
    public CreditType getContractType() {
        return contractType;
    }

    /**
     * Sets the value of the contractType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditType }
     *     
     */
    public void setContractType(CreditType value) {
        this.contractType = value;
    }

    /**
     * Gets the value of the initialTransactionType property.
     * 
     * @return
     *     possible object is
     *     {@link InitialTransactionType }
     *     
     */
    public InitialTransactionType getInitialTransactionType() {
        return initialTransactionType;
    }

    /**
     * Sets the value of the initialTransactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link InitialTransactionType }
     *     
     */
    public void setInitialTransactionType(InitialTransactionType value) {
        this.initialTransactionType = value;
    }

    /**
     * Gets the value of the contractStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ContractStatusType }
     *     
     */
    public ContractStatusType getContractStatus() {
        return contractStatus;
    }

    /**
     * Sets the value of the contractStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractStatusType }
     *     
     */
    public void setContractStatus(ContractStatusType value) {
        this.contractStatus = value;
    }

    /**
     * Gets the value of the signatureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSignatureDate() {
        return signatureDate;
    }

    /**
     * Sets the value of the signatureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSignatureDate(XMLGregorianCalendar value) {
        this.signatureDate = value;
    }

}
