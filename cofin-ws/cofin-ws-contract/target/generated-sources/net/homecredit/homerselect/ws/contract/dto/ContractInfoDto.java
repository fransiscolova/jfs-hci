
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContractInfoDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractInfoDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contractNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="signatureDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="productCategory" type="{http://homecredit.net/homerselect/ws/contract/dto}ProductCategoryDto"/>
 *         &lt;element name="creditAmount" type="{http://homecredit.net/homerselect/ws/contract/dto}MoneyDto"/>
 *         &lt;element name="firstInstalment" type="{http://homecredit.net/homerselect/ws/contract/dto}MoneyDto" minOccurs="0"/>
 *         &lt;element name="downpayment" type="{http://homecredit.net/homerselect/ws/contract/dto}MoneyDto" minOccurs="0"/>
 *         &lt;element name="advancepayment" type="{http://homecredit.net/homerselect/ws/contract/dto}MoneyDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractInfoDto", propOrder = {
    "status",
    "contractNumber",
    "signatureDate",
    "productCode",
    "productCategory",
    "creditAmount",
    "firstInstalment",
    "downpayment",
    "advancepayment"
})
public class ContractInfoDto {

    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    protected String contractNumber;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar signatureDate;
    protected String productCode;
    @XmlElement(required = true)
    protected ProductCategoryDto productCategory;
    @XmlElement(required = true)
    protected MoneyDto creditAmount;
    protected MoneyDto firstInstalment;
    protected MoneyDto downpayment;
    protected MoneyDto advancepayment;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the contractNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractNumber() {
        return contractNumber;
    }

    /**
     * Sets the value of the contractNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractNumber(String value) {
        this.contractNumber = value;
    }

    /**
     * Gets the value of the signatureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSignatureDate() {
        return signatureDate;
    }

    /**
     * Sets the value of the signatureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSignatureDate(XMLGregorianCalendar value) {
        this.signatureDate = value;
    }

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the productCategory property.
     * 
     * @return
     *     possible object is
     *     {@link ProductCategoryDto }
     *     
     */
    public ProductCategoryDto getProductCategory() {
        return productCategory;
    }

    /**
     * Sets the value of the productCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductCategoryDto }
     *     
     */
    public void setProductCategory(ProductCategoryDto value) {
        this.productCategory = value;
    }

    /**
     * Gets the value of the creditAmount property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyDto }
     *     
     */
    public MoneyDto getCreditAmount() {
        return creditAmount;
    }

    /**
     * Sets the value of the creditAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyDto }
     *     
     */
    public void setCreditAmount(MoneyDto value) {
        this.creditAmount = value;
    }

    /**
     * Gets the value of the firstInstalment property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyDto }
     *     
     */
    public MoneyDto getFirstInstalment() {
        return firstInstalment;
    }

    /**
     * Sets the value of the firstInstalment property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyDto }
     *     
     */
    public void setFirstInstalment(MoneyDto value) {
        this.firstInstalment = value;
    }

    /**
     * Gets the value of the downpayment property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyDto }
     *     
     */
    public MoneyDto getDownpayment() {
        return downpayment;
    }

    /**
     * Sets the value of the downpayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyDto }
     *     
     */
    public void setDownpayment(MoneyDto value) {
        this.downpayment = value;
    }

    /**
     * Gets the value of the advancepayment property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyDto }
     *     
     */
    public MoneyDto getAdvancepayment() {
        return advancepayment;
    }

    /**
     * Sets the value of the advancepayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyDto }
     *     
     */
    public void setAdvancepayment(MoneyDto value) {
        this.advancepayment = value;
    }

}
