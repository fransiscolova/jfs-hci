
package net.homecredit.homerselect.ws.contract.dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContractInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contractCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contractStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dpdCurrent" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dpdMaximal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="clientFullName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientMobilePhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientHomePhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientAddress" type="{http://homecredit.net/homerselect/ws/contract/dto}AddressInformation"/>
 *         &lt;element name="employerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="employerPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="employerAddress" type="{http://homecredit.net/homerselect/ws/contract/dto}AddressInformation" minOccurs="0"/>
 *         &lt;element name="clientProfession" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="additionalPersons" type="{http://homecredit.net/homerselect/ws/contract/dto}PersonInformation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="applicationCreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="applicationCreationUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="applicationCreationSalesroom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="applicationDetailUrlSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractInformation", propOrder = {
    "contractCode",
    "contractStatus",
    "dpdCurrent",
    "dpdMaximal",
    "clientFullName",
    "clientMobilePhone",
    "clientHomePhone",
    "clientAddress",
    "employerName",
    "employerPhone",
    "employerAddress",
    "clientProfession",
    "additionalPersons",
    "applicationCreationDate",
    "applicationCreationUser",
    "applicationCreationSalesroom",
    "applicationDetailUrlSuffix"
})
public class ContractInformation {

    @XmlElement(required = true)
    protected String contractCode;
    @XmlElement(required = true)
    protected String contractStatus;
    protected Integer dpdCurrent;
    protected Integer dpdMaximal;
    @XmlElement(required = true)
    protected String clientFullName;
    protected String clientMobilePhone;
    protected String clientHomePhone;
    @XmlElement(required = true)
    protected AddressInformation clientAddress;
    protected String employerName;
    protected String employerPhone;
    protected AddressInformation employerAddress;
    protected String clientProfession;
    protected List<PersonInformation> additionalPersons;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar applicationCreationDate;
    @XmlElement(required = true)
    protected String applicationCreationUser;
    @XmlElement(required = true)
    protected String applicationCreationSalesroom;
    @XmlElement(required = true)
    protected String applicationDetailUrlSuffix;

    /**
     * Gets the value of the contractCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractCode() {
        return contractCode;
    }

    /**
     * Sets the value of the contractCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractCode(String value) {
        this.contractCode = value;
    }

    /**
     * Gets the value of the contractStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractStatus() {
        return contractStatus;
    }

    /**
     * Sets the value of the contractStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractStatus(String value) {
        this.contractStatus = value;
    }

    /**
     * Gets the value of the dpdCurrent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDpdCurrent() {
        return dpdCurrent;
    }

    /**
     * Sets the value of the dpdCurrent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDpdCurrent(Integer value) {
        this.dpdCurrent = value;
    }

    /**
     * Gets the value of the dpdMaximal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDpdMaximal() {
        return dpdMaximal;
    }

    /**
     * Sets the value of the dpdMaximal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDpdMaximal(Integer value) {
        this.dpdMaximal = value;
    }

    /**
     * Gets the value of the clientFullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientFullName() {
        return clientFullName;
    }

    /**
     * Sets the value of the clientFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientFullName(String value) {
        this.clientFullName = value;
    }

    /**
     * Gets the value of the clientMobilePhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientMobilePhone() {
        return clientMobilePhone;
    }

    /**
     * Sets the value of the clientMobilePhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientMobilePhone(String value) {
        this.clientMobilePhone = value;
    }

    /**
     * Gets the value of the clientHomePhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientHomePhone() {
        return clientHomePhone;
    }

    /**
     * Sets the value of the clientHomePhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientHomePhone(String value) {
        this.clientHomePhone = value;
    }

    /**
     * Gets the value of the clientAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressInformation }
     *     
     */
    public AddressInformation getClientAddress() {
        return clientAddress;
    }

    /**
     * Sets the value of the clientAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInformation }
     *     
     */
    public void setClientAddress(AddressInformation value) {
        this.clientAddress = value;
    }

    /**
     * Gets the value of the employerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployerName() {
        return employerName;
    }

    /**
     * Sets the value of the employerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployerName(String value) {
        this.employerName = value;
    }

    /**
     * Gets the value of the employerPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployerPhone() {
        return employerPhone;
    }

    /**
     * Sets the value of the employerPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployerPhone(String value) {
        this.employerPhone = value;
    }

    /**
     * Gets the value of the employerAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressInformation }
     *     
     */
    public AddressInformation getEmployerAddress() {
        return employerAddress;
    }

    /**
     * Sets the value of the employerAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInformation }
     *     
     */
    public void setEmployerAddress(AddressInformation value) {
        this.employerAddress = value;
    }

    /**
     * Gets the value of the clientProfession property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientProfession() {
        return clientProfession;
    }

    /**
     * Sets the value of the clientProfession property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientProfession(String value) {
        this.clientProfession = value;
    }

    /**
     * Gets the value of the additionalPersons property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalPersons property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalPersons().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PersonInformation }
     * 
     * 
     */
    public List<PersonInformation> getAdditionalPersons() {
        if (additionalPersons == null) {
            additionalPersons = new ArrayList<PersonInformation>();
        }
        return this.additionalPersons;
    }

    /**
     * Gets the value of the applicationCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getApplicationCreationDate() {
        return applicationCreationDate;
    }

    /**
     * Sets the value of the applicationCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setApplicationCreationDate(XMLGregorianCalendar value) {
        this.applicationCreationDate = value;
    }

    /**
     * Gets the value of the applicationCreationUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationCreationUser() {
        return applicationCreationUser;
    }

    /**
     * Sets the value of the applicationCreationUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationCreationUser(String value) {
        this.applicationCreationUser = value;
    }

    /**
     * Gets the value of the applicationCreationSalesroom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationCreationSalesroom() {
        return applicationCreationSalesroom;
    }

    /**
     * Sets the value of the applicationCreationSalesroom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationCreationSalesroom(String value) {
        this.applicationCreationSalesroom = value;
    }

    /**
     * Gets the value of the applicationDetailUrlSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationDetailUrlSuffix() {
        return applicationDetailUrlSuffix;
    }

    /**
     * Sets the value of the applicationDetailUrlSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationDetailUrlSuffix(String value) {
        this.applicationDetailUrlSuffix = value;
    }

}
