
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractPerson complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractPerson">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="personRole" type="{http://homecredit.net/homerselect/ws/contract/dto}PersonRoleType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractPerson", propOrder = {
    "cuid",
    "personRole"
})
public class ContractPerson {

    protected long cuid;
    @XmlElement(required = true)
    protected PersonRoleType personRole;

    /**
     * Gets the value of the cuid property.
     * 
     */
    public long getCuid() {
        return cuid;
    }

    /**
     * Sets the value of the cuid property.
     * 
     */
    public void setCuid(long value) {
        this.cuid = value;
    }

    /**
     * Gets the value of the personRole property.
     * 
     * @return
     *     possible object is
     *     {@link PersonRoleType }
     *     
     */
    public PersonRoleType getPersonRole() {
        return personRole;
    }

    /**
     * Sets the value of the personRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonRoleType }
     *     
     */
    public void setPersonRole(PersonRoleType value) {
        this.personRole = value;
    }

}
