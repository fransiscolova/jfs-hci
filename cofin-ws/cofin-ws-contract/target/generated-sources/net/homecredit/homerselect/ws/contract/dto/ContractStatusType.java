
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContractStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="A"/>
 *     &lt;enumeration value="H"/>
 *     &lt;enumeration value="L"/>
 *     &lt;enumeration value="N"/>
 *     &lt;enumeration value="Q"/>
 *     &lt;enumeration value="S"/>
 *     &lt;enumeration value="R"/>
 *     &lt;enumeration value="P"/>
 *     &lt;enumeration value="T"/>
 *     &lt;enumeration value="D"/>
 *     &lt;enumeration value="K"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContractStatusType")
@XmlEnum
public enum ContractStatusType {


    /**
     * Active
     * 
     */
    A,

    /**
     * Written-off
     * 
     */
    H,

    /**
     * Paid-off
     * 
     */
    L,

    /**
     * Signed
     * 
     */
    N,

    /**
     * Sold
     * 
     */
    Q,

    /**
     * Approved
     * 
     */
    S,

    /**
     * In process
     * 
     */
    R,

    /**
     * Pre-approved
     * 
     */
    P,

    /**
     * Cancelled
     * 
     */
    T,

    /**
     * Rejected
     * 
     */
    D,

    /**
     * Finished
     * 
     */
    K;

    public String value() {
        return name();
    }

    public static ContractStatusType fromValue(String v) {
        return valueOf(v);
    }

}
