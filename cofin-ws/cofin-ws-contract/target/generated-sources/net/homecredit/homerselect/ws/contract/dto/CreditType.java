
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CreditType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CEL"/>
 *     &lt;enumeration value="REL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CreditType")
@XmlEnum
public enum CreditType {


    /**
     * Closed-end Loan
     * 
     */
    CEL,

    /**
     * Revolving Loan
     * 
     */
    REL;

    public String value() {
        return name();
    }

    public static CreditType fromValue(String v) {
        return valueOf(v);
    }

}
