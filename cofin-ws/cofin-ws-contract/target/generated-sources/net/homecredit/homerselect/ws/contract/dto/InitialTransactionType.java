
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InitialTransactionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InitialTransactionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CASH"/>
 *     &lt;enumeration value="NDF"/>
 *     &lt;enumeration value="POS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InitialTransactionType")
@XmlEnum
public enum InitialTransactionType {


    /**
     * Cash transaction
     * 
     */
    CASH,

    /**
     * Not defined (no initial transaction is done)
     * 
     */
    NDF,

    /**
     * Purchase of consumer goods
     * 
     */
    POS;

    public String value() {
        return name();
    }

    public static InitialTransactionType fromValue(String v) {
        return valueOf(v);
    }

}
