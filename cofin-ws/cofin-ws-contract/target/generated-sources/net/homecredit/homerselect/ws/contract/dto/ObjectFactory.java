
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.homecredit.homerselect.ws.contract.dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.homecredit.homerselect.ws.contract.dto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ContractHistoryDto }
     * 
     */
    public ContractHistoryDto createContractHistoryDto() {
        return new ContractHistoryDto();
    }

    /**
     * Create an instance of {@link ProductCategoryDto }
     * 
     */
    public ProductCategoryDto createProductCategoryDto() {
        return new ProductCategoryDto();
    }

    /**
     * Create an instance of {@link PersonInformation }
     * 
     */
    public PersonInformation createPersonInformation() {
        return new PersonInformation();
    }

    /**
     * Create an instance of {@link MoneyDto }
     * 
     */
    public MoneyDto createMoneyDto() {
        return new MoneyDto();
    }

    /**
     * Create an instance of {@link AddressInformation }
     * 
     */
    public AddressInformation createAddressInformation() {
        return new AddressInformation();
    }

    /**
     * Create an instance of {@link ContractPerson }
     * 
     */
    public ContractPerson createContractPerson() {
        return new ContractPerson();
    }

    /**
     * Create an instance of {@link ContractDetail }
     * 
     */
    public ContractDetail createContractDetail() {
        return new ContractDetail();
    }

    /**
     * Create an instance of {@link ClosedEndParameter }
     * 
     */
    public ClosedEndParameter createClosedEndParameter() {
        return new ClosedEndParameter();
    }

    /**
     * Create an instance of {@link ContractInfoDto }
     * 
     */
    public ContractInfoDto createContractInfoDto() {
        return new ContractInfoDto();
    }

    /**
     * Create an instance of {@link ContractRequired }
     * 
     */
    public ContractRequired createContractRequired() {
        return new ContractRequired();
    }

    /**
     * Create an instance of {@link RevlovingParameter }
     * 
     */
    public RevlovingParameter createRevlovingParameter() {
        return new RevlovingParameter();
    }

    /**
     * Create an instance of {@link ContractInformation }
     * 
     */
    public ContractInformation createContractInformation() {
        return new ContractInformation();
    }

    /**
     * Create an instance of {@link ContractFilter }
     * 
     */
    public ContractFilter createContractFilter() {
        return new ContractFilter();
    }

    /**
     * Create an instance of {@link KeyValueType }
     * 
     */
    public KeyValueType createKeyValueType() {
        return new KeyValueType();
    }

    /**
     * Create an instance of {@link ContractFound }
     * 
     */
    public ContractFound createContractFound() {
        return new ContractFound();
    }

}
