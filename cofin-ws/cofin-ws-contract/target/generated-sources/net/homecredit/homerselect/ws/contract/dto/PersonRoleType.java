
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonRoleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PersonRoleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CARD_HOLDER"/>
 *     &lt;enumeration value="CLIENT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PersonRoleType")
@XmlEnum
public enum PersonRoleType {


    /**
     * Card holder
     * 
     */
    CARD_HOLDER,

    /**
     * Client
     * 
     */
    CLIENT;

    public String value() {
        return name();
    }

    public static PersonRoleType fromValue(String v) {
        return valueOf(v);
    }

}
