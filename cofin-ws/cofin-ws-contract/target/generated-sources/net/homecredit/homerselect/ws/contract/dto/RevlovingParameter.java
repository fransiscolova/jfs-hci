
package net.homecredit.homerselect.ws.contract.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RevlovingParameter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RevlovingParameter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="creditLimit" type="{http://homecredit.net/homerselect/ws/contract/dto}MoneyDto"/>
 *         &lt;element name="minInsatllmentAmount" type="{http://homecredit.net/homerselect/ws/contract/dto}MoneyDto"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RevlovingParameter", propOrder = {
    "creditLimit",
    "minInsatllmentAmount"
})
public class RevlovingParameter {

    @XmlElement(required = true)
    protected MoneyDto creditLimit;
    @XmlElement(required = true)
    protected MoneyDto minInsatllmentAmount;

    /**
     * Gets the value of the creditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyDto }
     *     
     */
    public MoneyDto getCreditLimit() {
        return creditLimit;
    }

    /**
     * Sets the value of the creditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyDto }
     *     
     */
    public void setCreditLimit(MoneyDto value) {
        this.creditLimit = value;
    }

    /**
     * Gets the value of the minInsatllmentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyDto }
     *     
     */
    public MoneyDto getMinInsatllmentAmount() {
        return minInsatllmentAmount;
    }

    /**
     * Sets the value of the minInsatllmentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyDto }
     *     
     */
    public void setMinInsatllmentAmount(MoneyDto value) {
        this.minInsatllmentAmount = value;
    }

}
