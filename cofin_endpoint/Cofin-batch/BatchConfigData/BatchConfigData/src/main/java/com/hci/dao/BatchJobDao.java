package com.hci.dao;

import java.util.List;

import com.hci.entity.BatchJob;

public interface BatchJobDao {
    public List<BatchJob> getAllJob();
}
