package com.hci.dao.impl;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.constant.BatchConstant;
import com.hci.dao.BatchJobDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.entity.BatchJob;
import com.hci.mapper.BatchJobMapper;

public class BatchJobDaoImpl implements BatchJobDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public BatchJobDaoImpl() {
        dataSource = new DatabaseConnection().getContextConfigurationDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public List<BatchJob> getAllJob() {
        StringBuilder SQL = new StringBuilder(100);
        SQL.append("select * from batch_job ");
        SQL.append(" where is_active = ").append(BatchConstant.Q_YES);
        List<BatchJob> bjs = jdbcTemplateObject.query(SQL.toString(), new BatchJobMapper());
        return bjs;
    }
}
