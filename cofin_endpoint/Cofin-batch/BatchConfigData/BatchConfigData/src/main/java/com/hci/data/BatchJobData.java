package com.hci.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import com.hci.dao.impl.BatchJobDaoImpl;
import com.hci.entity.BatchJob;

public class BatchJobData {
	public Map<String, String> sourceFName;
	public Map<String, String> sourceFolName;
	public Map<String, String> targetFName;
	public Map<String, String> tmpFName;
	public Map<String, String> sch;
	public Map<String, String> jobClass;
	public Map<String, String> processName;
	public List<String> jobName;	
	public Map<String, BatchJob> batchJobs;
	public static Map<String, List<String>> fName;
	public static Map<String, List<String>> tName;
	public static Map<String, List<String>> aName;
	public Map<String, String> jobClassParent;

    @Autowired
	private DataSource dataSource;
	
	public Map<String, String> getJobClassParent() {
		return jobClassParent;
	}

	public void setJobClassParent(Map<String, String> jobClassParent) {
		this.jobClassParent = jobClassParent;
	}
	
	public Map<String, String> getProcessName() {
		return processName;
	}

	public void setProcessName(Map<String, String> processName) {
		this.processName = processName;
	}
	
	public static Map<String, List<String>> getfName() {
		return fName;
	}

	public static void setfName(Map<String, List<String>> fName) {
		BatchJobData.fName = fName;
	}

	public static Map<String, List<String>> gettName() {
		return tName;
	}

	public static void settName(Map<String, List<String>> tName) {
		BatchJobData.tName = tName;
	}

	public Map<String, String> getJobClass() {
		return jobClass;
	}

	public void setJobClass(Map<String, String> jobClass) {
		this.jobClass = jobClass;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public Map<String, String> getSourceFName() {
		return sourceFName;
	}

	public void setSourceFName(Map<String, String> sourceFName) {
		this.sourceFName = sourceFName;
	}

	public Map<String, String> getTargetFName() {
		return targetFName;
	}

	public void setTargetFName(Map<String, String> targetFName) {
		this.targetFName = targetFName;
	}
	
	public Map<String, String> getSch() {
		return sch;
	}

	public void setSch(Map<String, String> sch) {
		this.sch = sch;
	}
	
	public List<String> getJobName() {
		return jobName;
	}

	public void setJobName(List<String> jobName) {
		this.jobName = jobName;
	}
	
	public Map<String, String> getTmpFName() {
		return tmpFName;
	}

	public void setTmpFName(Map<String, String> tmpFName) {
		this.tmpFName = tmpFName;
	}
	
	public Map<String, String> getSourceFolName() {
		return sourceFolName;
	}

	public void setSourceFolName(Map<String, String> sourceFolName) {
		this.sourceFolName = sourceFolName;
	}
	
	public Map<String, BatchJob> getBatchJobs() {
		return batchJobs;
	}

	public void setBatchJobs(Map<String, BatchJob> batchJobs) {
		this.batchJobs = batchJobs;
	}
	
	public static Map<String, List<String>> getaName() {
		return aName;
	}

	public static void setaName(Map<String, List<String>> aName) {
		BatchJobData.aName = aName;
	}
	
	public BatchJobData() {
	}
	
	public void setAllVariable() {
		BatchJobDaoImpl bjdi = new BatchJobDaoImpl();
//		bjdi.setDataSource(dataSource);
		List<BatchJob> bjs = bjdi.getAllJob();
		List<String> jn = new ArrayList<String>();
		Map<String, String> fn = new HashMap<String, String>();
		Map<String, String> sFolName = new HashMap<String, String>();
		Map<String, String> tfn = new HashMap<String, String>();
		Map<String, String> mp = new HashMap<String, String>();
		Map<String, String> jc = new HashMap<String, String>();
		Map<String, String> tmpfn = new HashMap<String, String>();
		Map<String, String> prcsName = new HashMap<String, String>();
		//processName
		Map<String, BatchJob> bJob = new HashMap<String, BatchJob>();
		Map<String, List<String>> fName = new HashMap<String, List<String>>();
		Map<String, List<String>> tName = new HashMap<String,  List<String>>();
		Map<String, List<String>> aName = new HashMap<String,  List<String>>();
		Map<String, String> parent = new HashMap<String, String>();
		
		if(fName.size() == 0) {
			for(BatchJob bj : bjs) {
				jn.add(bj.getJobName());
				sFolName.put(bj.getJobName(), bj.getSourceFolderName());
				fn.put(bj.getJobName(), bj.getSourceFileName());
				tfn.put(bj.getJobName(), bj.getTargetFolder());
				mp.put(bj.getJobName(), bj.getCronExpression());
				jc.put(bj.getJobName(), bj.getJobClass());
				tmpfn.put(bj.getJobName(), bj.getTmpFolder());
				prcsName.put(bj.getJobName(), bj.getProcessName());
				bJob.put(bj.getJobName(), bj);
				fName.put(bj.getJobName(), new ArrayList<String>());
				tName.put(bj.getJobName(), new ArrayList<String>());
				aName.put(bj.getJobName(), new ArrayList<String>());
				if(bj.getParentJob().equals("-")) {
					parent.put(bj.getJobName(), bj.getJobClass());
				}
			}
			setJobName(jn);
			setSourceFolName(sFolName);
			setSourceFName(fn);
			setTargetFName(tfn);
			setSch(mp);
			setJobClass(jc);
			setTmpFName(tmpfn);
			setBatchJobs(bJob);
			setfName(fName);
			settName(tName);
			setaName(aName);
			setProcessName(prcsName);
			setJobClassParent(parent);
		}
	}
}
