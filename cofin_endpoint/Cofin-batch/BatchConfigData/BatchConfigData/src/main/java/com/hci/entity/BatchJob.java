package com.hci.entity;

public class BatchJob {
	private String jobId;
	private String parentJob;
	private String jobName;
	private String sourceFolderName;
	private String sourceFileName;
	private String targetFolder;
	private String cronExpression;
	private String forceRun;
	private String jobStatus;
	private String isActive;
	private String jobClass;
	private String tmpFolder;
	private String processName;
	private String configJob;
	
	public String getConfigJob() {
		return configJob;
	}
	public void setConfigJob(String configJob) {
		this.configJob = configJob;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getParentJob() {
		return parentJob;
	}
	public void setParentJob(String parentJob) {
		this.parentJob = parentJob;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getSourceFolderName() {
		return sourceFolderName;
	}
	public void setSourceFolderName(String sourceFolderName) {
		this.sourceFolderName = sourceFolderName;
	}
	public String getSourceFileName() {
		return sourceFileName;
	}
	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}
	public String getTargetFolder() {
		return targetFolder;
	}
	public void setTargetFolder(String targetFolder) {
		this.targetFolder = targetFolder;
	}
	public String getCronExpression() {
		return cronExpression;
	}
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	public String getForceRun() {
		return forceRun;
	}
	public void setForceRun(String forceRun) {
		this.forceRun = forceRun;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getJobClass() {
		return jobClass;
	}
	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}
	public String getTmpFolder() {
		return tmpFolder;
	}
	public void setTmpFolder(String tmpFolder) {
		this.tmpFolder = tmpFolder;
	}
}
