package com.hci.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.entity.BatchJob;

public class BatchJobMapper implements RowMapper<BatchJob>{

	@Override
	public BatchJob mapRow(ResultSet rs, int rowNum) throws SQLException {
		BatchJob batchJob = new BatchJob();
		batchJob.setJobId(rs.getString("JOB_ID"));
		batchJob.setParentJob(rs.getString("PARENT_JOB"));
		batchJob.setJobName(rs.getString("JOB_NAME"));
		batchJob.setSourceFolderName(rs.getString("SOURCE_FOLDER_NAME"));
		batchJob.setSourceFileName(rs.getString("SOURCE_FILE_NAME"));
		batchJob.setTargetFolder(rs.getString("TARGET_FOLDER_NAME"));
		batchJob.setCronExpression(rs.getString("CRON_EXPRESSION"));
		batchJob.setForceRun(rs.getString("FORCE_RUN"));
		batchJob.setJobStatus(rs.getString("JOB_STATUS"));
		batchJob.setIsActive(rs.getString("IS_ACTIVE"));
		batchJob.setJobClass(rs.getString("JOB_CLASS"));
		batchJob.setTmpFolder(rs.getString("TMP_FOLDER"));
		batchJob.setProcessName(rs.getString("PROCESS_NAME"));
		batchJob.setConfigJob(rs.getString("CONFIG_JOB"));
		return batchJob;
	}

}
