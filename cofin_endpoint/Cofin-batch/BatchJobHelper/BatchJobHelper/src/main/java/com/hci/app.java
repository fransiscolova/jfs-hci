package com.hci;

import java.util.List;

import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;

public class app {
	public static void main(String[] args) {
		SSHUtil sshUtil = new SSHUtil();
		ShellConnectionObject shellCObj = new ShellConnectionObject();
		
		PropertyUtil prop = new PropertyUtil();
		
		shellCObj.setIp(prop.getProperty("linux.ip"));
		shellCObj.setPort(Integer.parseInt(prop.getProperty("linux.port")));
		shellCObj.setUsername(prop.getProperty("linux.username"));
		shellCObj.setPassword(prop.getProperty("linux.password"));
		
		String command = "less /WS/convertor/test_michael/testing.txt";
		List<String> mes = sshUtil.sendCommandStdAlone(command, shellCObj);
		for (String me : mes) {
			if(me.startsWith("testing")) {
				System.out.println(me.split("=")[1]);
			}
		}
	}
}
