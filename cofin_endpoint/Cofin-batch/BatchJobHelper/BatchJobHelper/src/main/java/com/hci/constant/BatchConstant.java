/**
 *
 */
package com.hci.constant;

/**
 * Class to centralize all constants in JFS Batch.
 *
 * @author muhammad.muflihun
 *
 */
public final class BatchConstant {

    /**
     * Common constants.
     *
     * @author muhammad.muflihun
     *
     */
    public final class Common {
        public static final String YES = "Y";
        public static final String NO = "N";
        public static final String SUCCESS = "Success";
        public static final String ERROR = "Error";
        public static final String LOCALHOST = "localhost";
        public static final String EMAIL_HCID_WA = "hcid.workaround@homecredit.co.id";
        public static final String USER_HCID_WA = "workarounds";
        public static final String PASS_HCID_WA = "hc1dwrk4r0und";
        public static final String EMAIL_JFS = "jfs@homecredit.co.id";
    }

    /**
     * Query parameter constants.
     *
     * @author muhammad.muflihun
     *
     */
    public final class QueryParameters {
        public static final String YES = "'Y'";
        public static final String NO = "'N'";
        public static final String SUCCESS = "'Success'";
        public static final String ERROR = "'Error'";
        public static final String DATE_FORMAT_DD_MON_YYYY = "'dd-MON-yyyy'";
    }
}
