package com.hci.data.dao;

import java.util.List;
import java.util.Map;

import com.hci.data.entity.ReExecuteJob;


public interface ParentJobDao {
	public int createDBLog(String uuid, String date, String time, String partner, String agreement, String processName, String status, String reason, String fileName);
	public List<ReExecuteJob> getRerunJob(String jobName);
	public int updateDBLog(String uuid, String jobStatus, String reason, String lastRun, int numOfRerun, String rerunStatus);
	public boolean checkHoliday();
	public boolean checkHoliday(String date);
	public boolean getParentJobStatus(String processName, String date);
	public boolean checkTodayRunJob(String processName, String date);
	public boolean setForceRunChildJob(String processName);
	public boolean setForceRunNChildJob(String processName);
	public boolean checkTodayJob(String todayDate);
	public boolean updateForceRunStatusN(String jobClass);
	public void runProcedure(String procedureName);
	public void runProcedure(String procedureName, Map<String, Object> param);
}
