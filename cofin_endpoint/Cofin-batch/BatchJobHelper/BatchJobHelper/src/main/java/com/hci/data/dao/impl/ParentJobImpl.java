package com.hci.data.dao.impl;

import java.sql.Types;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.hci.constant.BatchConstant;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.ReExecuteJob;
import com.hci.data.mapper.ReExecuteJobMapper;
import com.hci.util.LoggerUtil;

public class ParentJobImpl implements ParentJobDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public ParentJobImpl() {
        dataSource = new DatabaseConnection().getContextConfigurationDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public boolean checkHoliday() {
        StringBuilder sb = new StringBuilder(200);
        sb.append(" SELECT count(*) ");
        sb.append(" FROM jfs_public_holiday_id ");
        sb.append(" WHERE to_date(sysdate, '")
                .append(BatchConstant.QueryParameters.DATE_FORMAT_DD_MON_YYYY)
                .append("') BETWEEN date_from ");
        sb.append("         AND date_to ");
        int holidayCount = jdbcTemplateObject.queryForObject(sb.toString(), Integer.class);
        return holidayCount > 0;
    }

    @Override
    public boolean checkHoliday(String date) {
        StringBuilder sb = new StringBuilder(200);
        sb.append(" SELECT count(*) ");
        sb.append(" FROM jfs_public_holiday_id ");
        sb.append(" WHERE to_date('").append(date).append("', '")
        .append(BatchConstant.QueryParameters.DATE_FORMAT_DD_MON_YYYY)
        .append("') BETWEEN date_from ");
        sb.append("         AND date_to ");
        int holidayCount = jdbcTemplateObject.queryForObject(sb.toString(), Integer.class);
        return holidayCount > 0;
    }

    @Override
    public boolean checkTodayJob(String todayDate) {
        StringBuilder sb = new StringBuilder(250);
        sb.append(" SELECT count(DISTINCT (process_name)) ");
        sb.append(" FROM batch_job bj ");
        sb.append(" WHERE bj.config_job = ").append(BatchConstant.QueryParameters.NO);
        sb.append("     AND process_name NOT IN ( ");
        sb.append("         SELECT DISTINCT (process_code) ");
        sb.append("         FROM batch_job_log bjl ");
        sb.append("         WHERE bjl.run_date = '").append(todayDate).append("' ");
        sb.append("         ) ");
        int result = jdbcTemplateObject.queryForObject(sb.toString(), Integer.class);
        LoggerUtil log = new LoggerUtil();
        log.info("this", String.valueOf(result));
        return result > 0;
    }

    @Override
    public boolean checkTodayRunJob(String processName, String date) {
        StringBuilder sb = new StringBuilder(200);
        sb.append(" SELECT count(*) ");
        sb.append(" FROM batch_job_log ");
        sb.append(" WHERE process_code = '").append(processName).append("' ");
        sb.append("     AND run_date = '").append(date).append("' ");
        int result = jdbcTemplateObject.queryForObject(sb.toString(), Integer.class);
        return result > 0;
    }

    @Override
    public int createDBLog(String uuid, String date, String time, String partner, String agreement,
            String processName, String status, String reason, String fileName) {
        StringBuilder sb = new StringBuilder(200);
        sb.append(" INSERT INTO BATCH_JOB_LOG ( ");
        sb.append("     RUN_ID ");
        sb.append("     ,RUN_DATE ");
        sb.append("     ,RUN_TIME ");
        sb.append("     ,FILE_NAME ");
        sb.append("     ,STATUS ");
        sb.append("     ,REASON ");
        sb.append("     ,PARTNER_CODE ");
        sb.append("     ,AGREEMENT_CODE ");
        sb.append("     ,PROCESS_CODE ");
        sb.append("     ) ");
        sb.append(" VALUES ( ");
        sb.append("     ? ");
        sb.append("     ,? ");
        sb.append("     ,? ");
        sb.append("     ,? ");
        sb.append("     ,? ");
        sb.append("     ,? ");
        sb.append("     ,? ");
        sb.append("     ,? ");
        sb.append("     ,? ");
        sb.append("     ) ");
        Object[] params = new Object[] { uuid, date, time, fileName, status, reason, partner,
                agreement, processName };
        int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
                Types.VARCHAR, Types.VARCHAR, Types.NVARCHAR, Types.NVARCHAR, Types.NVARCHAR };
        return jdbcTemplateObject.update(sb.toString(), params, types);
    }

    @Override
    public boolean getParentJobStatus(String processName, String date) {
        StringBuilder sb = new StringBuilder(100);
        sb.append(" SELECT parent_job ");
        sb.append(" FROM batch_job ");
        sb.append(" WHERE process_name = '").append(processName).append("' ");
        String queryParentRes = jdbcTemplateObject.queryForObject(sb.toString(), String.class);
        if (!queryParentRes.equals("-")) {
            StringBuilder sb2 = new StringBuilder(250);
            sb2.append(" SELECT count(*) ");
            sb2.append(" FROM batch_job_log bjl ");
            sb2.append(" WHERE bjl.process_code = ( ");
            sb2.append("         SELECT process_name ");
            sb2.append("         FROM batch_job ");
            sb2.append("         WHERE job_id = ( ");
            sb2.append("                 SELECT parent_job ");
            sb2.append("                 FROM batch_job ");
            sb2.append("                 WHERE process_name = '").append(processName).append("' ");
            sb2.append("                 ) ");
            sb2.append("         ) + ");
            sb2.append("     AND STATUS = ").append(BatchConstant.QueryParameters.SUCCESS);
            sb2.append("     AND run_date = '").append(date).append("' ");
            int result = jdbcTemplateObject.queryForObject(sb2.toString(), Integer.class);
            if (result == 0) {
                StringBuilder sb3 = new StringBuilder(100);
                sb3.append(" SELECT force_run ");
                sb3.append(" FROM batch_job ");
                sb3.append(" WHERE process_name = '").append(processName).append("' ");
                String forceRunRes = jdbcTemplateObject
                        .queryForObject(sb3.toString(), String.class);
                if (!forceRunRes.equals(BatchConstant.Common.YES)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public List<ReExecuteJob> getRerunJob(String jobName) {
        StringBuilder sb = new StringBuilder(350);
        sb.append(" SELECT DISTINCT bjl.run_id ");
        sb.append("     ,bjl.run_date ");
        sb.append("     ,bjl.num_of_rerun ");
        sb.append(" FROM batch_job_log bjl ");
        sb.append(" LEFT JOIN batch_job bj ON bjl.process_code = bj.process_name ");
        sb.append(" WHERE 1 = 1 ");
        sb.append("     AND bjl.re_execute = '").append(BatchConstant.QueryParameters.YES)
                .append("' ");
        sb.append("     AND bj.job_name = '").append(jobName).append("' ");
        sb.append("     AND bjl.num_of_rerun < 3 ");
        return jdbcTemplateObject.query(sb.toString(), new ReExecuteJobMapper());
    }

    @Override
    public void runProcedure(String procedureName) {
        new SimpleJdbcCall(dataSource).withProcedureName(procedureName).executeObject(null);
    }

    @Override
    public void runProcedure(String procedureName, Map<String, Object> param) {
        SqlParameterSource in = new MapSqlParameterSource(param);
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName(procedureName);
        jdbcCall.execute(in);
    }

    @Override
    public boolean setForceRunChildJob(String processName) {
        StringBuilder sb = new StringBuilder(250);
        sb.append(" UPDATE batch_job ");
        sb.append(" SET force_run = ").append(BatchConstant.QueryParameters.YES);
        sb.append(" WHERE job_id IN ( ");
        sb.append("         SELECT job_id ");
        sb.append("         FROM batch_job ");
        sb.append("         WHERE parent_job = ( ");
        sb.append("                 SELECT job_id ");
        sb.append("                 FROM batch_job ");
        sb.append("                 WHERE process_name = '").append(processName).append("' ");
        sb.append("                 ) ");
        sb.append("         ) ");
        return jdbcTemplateObject.update(sb.toString()) > 0;
    }

    @Override
    public boolean setForceRunNChildJob(String processName) {
        StringBuilder sb = new StringBuilder(100);
        sb.append(" UPDATE batch_job ");
        sb.append(" SET force_run = ").append(BatchConstant.QueryParameters.NO);
        sb.append(" WHERE process_name = '").append(processName).append("' ");
        jdbcTemplateObject.update(sb.toString());
        return true;
    }

    @Override
    public int updateDBLog(String uuid, String jobStatus, String reason, String lastRun,
            int numOfRerun, String rerunStatus) {
        StringBuilder sb = new StringBuilder(200);
        sb.append(" UPDATE BATCH_JOB_LOG ");
        sb.append(" SET NUM_OF_RERUN = ? ");
        sb.append("     ,STATUS = ? ");
        sb.append("     ,REASON = ? ");
        sb.append("     ,LAST_RUN = ? ");
        sb.append("     ,RE_EXECUTE = ? ");
        sb.append(" WHERE RUN_ID = ? ");
        Object[] params = new Object[] { numOfRerun, jobStatus, reason, lastRun, rerunStatus, uuid };
        int[] types = new int[] { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
                Types.VARCHAR, Types.VARCHAR };

        return jdbcTemplateObject.update(sb.toString(), params, types);
    }

    @Override
    public boolean updateForceRunStatusN(String jobClass) {
        StringBuilder sb = new StringBuilder(100);
        sb.append(" UPDATE BATCH_JOB ");
        sb.append(" SET FORCE_RUN = ").append(BatchConstant.QueryParameters.NO);
        sb.append(" WHERE JOB_CLASS = ? ");
        Object[] params = new Object[] { jobClass };
        int[] types = new int[] { Types.VARCHAR };
        return jdbcTemplateObject.update(sb.toString(), params, types) > 0;
    }
}
