package com.hci.data.database;

import java.sql.SQLException;
import javax.sql.DataSource;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class DatabaseConnection {
    CompositeConfiguration config = new CompositeConfiguration();

    public DatabaseConnection() {
        try {
//            config.addConfiguration(new PropertiesConfiguration("D:/Properties/Cofin/db.properties"));
            config.addConfiguration(new PropertiesConfiguration("/WS/convertor/batch/properties/db.properties"));
            // config.addConfiguration(new
            // PropertiesConfiguration("/cofin/batch/app/db.properties"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public DataSource getContextConfigurationDatasource() {
        OracleDataSource oracleDS = null;
        try {
            oracleDS = new OracleDataSource();
            oracleDS.setURL((String) config.getProperty("oracle.url"));
            oracleDS.setUser((String) config.getProperty("oracle.user"));
            oracleDS.setPassword((String) config.getProperty("oracle.password"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oracleDS;
    }

    public DataSource getContextDatasource() {
        OracleDataSource oracleDS = null;
        try {
            oracleDS = new OracleDataSource();
            oracleDS.setURL((String) config.getProperty("oracle.url"));
            oracleDS.setUser((String) config.getProperty("oracle.user"));
            oracleDS.setPassword((String) config.getProperty("oracle.password"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oracleDS;
    }
}
