package com.hci.data.entity;

public class ReExecuteJob {
	public String runId;
	public String runDate;
	public int numOfRerun;
	
	public String getRunId() {
		return runId;
	}
	public void setRunId(String runId) {
		this.runId = runId;
	}
	public String getRunDate() {
		return runDate;
	}
	public void setRunDate(String runDate) {
		this.runDate = runDate;
	}
	public int getNumOfRerun() {
		return numOfRerun;
	}
	public void setNumOfRerun(int numOfRerun) {
		this.numOfRerun = numOfRerun;
	}
}
