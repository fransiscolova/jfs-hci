package com.hci.data.entity;

public class ShellConnectionObject {
	private String username;
	private String password;
	private String ip;
	private String host;
	private int port;
	
	public ShellConnectionObject() {
	}
	
	public ShellConnectionObject(String username, String password, String ip, String host, int port) {
		this.username = username;
		this.password = password;
		this.ip = ip;
		this.host = host;
		this.port = port;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
}