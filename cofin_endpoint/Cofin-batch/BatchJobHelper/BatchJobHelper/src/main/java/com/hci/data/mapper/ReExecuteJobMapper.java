package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.ReExecuteJob;

public class ReExecuteJobMapper implements RowMapper<ReExecuteJob> {

	@Override
	public ReExecuteJob mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReExecuteJob rej = new ReExecuteJob();
		rej.setRunId(rs.getString("RUN_ID"));
		rej.setRunDate(rs.getString("RUN_DATE"));
		rej.setNumOfRerun(rs.getInt("NUM_OF_RERUN"));
		return rej;
	}
	
}
