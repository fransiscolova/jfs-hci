package com.hci.job.sub;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class SubJobParent {
	public String aescrypt(ShellConnectionObject shellCObj, String sourceFilename, String targetFilename) {
		SSHUtil sshUtil = new SSHUtil(shellCObj.getUsername(), shellCObj.getPassword(), shellCObj.getIp(), shellCObj.getHost(), shellCObj.getPort());
		String status = "Success";
		try {
			sshUtil.connect();
			List<String> messages = sshUtil.sendCommand(". /WS/convertor/java_sh/__aescrypt.sh " + sourceFilename + " " + targetFilename);
			sshUtil.close();
		} catch (Exception e) {
			status = e.getMessage();
		}
		
		return status;
	}
	
	public String csv2xls(ShellConnectionObject shellCObj, String sourceFilename, String targetFilename, String delimiter) {
		SSHUtil sshUtil = new SSHUtil(shellCObj.getUsername(), shellCObj.getPassword(), shellCObj.getIp(), shellCObj.getHost(), shellCObj.getPort());
		String jobStatus = "Success";
		try {
			sshUtil.connect();
			List<String> messages = sshUtil.sendCommand(". /WS/convertor/java_sh/__csv2xls.sh " + sourceFilename + " " + targetFilename + " " + delimiter);
			sshUtil.close();
		} catch (Exception e) {
			jobStatus = e.getMessage();
		}
		
		return jobStatus;
	}
	
	public boolean saveLogToDb(String uuid, Map<String, Object> data, Date startJobTime, String jobName, String status, String filename) {
		ParentJobDao jfsPaymentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		
		Map<String, String> agreementPartner = (Map<String, String>) data.get("agreementPartner");
		
		for (Entry<String, String> e : agreementPartner.entrySet()) {
			jfsPaymentDao.createDBLog(uuid, new SimpleDateFormat("yyyy-MM-dd").format(startJobTime), 
					new SimpleDateFormat("hh:mm:ss").format(startJobTime), (String) e.getValue(), e.getKey(), jobName, 
					(status.equals("Success") ? "Success" : "Fail"), (status.equals("Success") ? "-" : status), filename);
		}
		//(String uuid, String date, String time, String partner, String agreement, String processName, String status, String reason);
		return true;
	}
	
	public boolean updateDBLog(String uuid, String status, int numOfRerun, String rerunStatus) {
		ParentJobDao jfsPaymentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		jfsPaymentDao.updateDBLog(uuid, (status.equals("Success") ? "Success" : "Fail"), (status.equals("Success") ? "-" : status), 
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), numOfRerun, rerunStatus);
		return true;
	}
	
	public boolean setChildForceRunY(String processName) {
		ParentJobDao parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		return parentDao.setForceRunChildJob(processName);
	}
	
	public boolean saveXNAToDbLog(String uuid, Date startJobTime, String jobName, String status, String filename) {
		ParentJobDao parentJobDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		parentJobDao.createDBLog(uuid, new SimpleDateFormat("yyyy-MM-dd").format(startJobTime), 
				new SimpleDateFormat("hh:mm:ss").format(startJobTime), "XNA", "XNA", jobName, status, "-", filename);
		return true;
	}
	
	public boolean removeAndWriteToFile(ShellConnectionObject shellCObj, String filename, List<String> texts) {
		String result = new SSHUtil().removeAndWriteToFile(shellCObj, filename, texts);
		if(result.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
