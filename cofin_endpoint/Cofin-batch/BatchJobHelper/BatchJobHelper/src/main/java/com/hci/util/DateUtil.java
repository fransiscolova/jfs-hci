package com.hci.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public String dateToString(Date date, String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(date);
	}
}
