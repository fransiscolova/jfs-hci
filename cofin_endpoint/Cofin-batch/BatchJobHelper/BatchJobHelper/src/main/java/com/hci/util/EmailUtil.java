package com.hci.util;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import com.hci.constant.BatchConstant;
	
public class EmailUtil {
	public void sendEmail(String subject, StringBuffer content) throws EmailException {
		Email email = new SimpleEmail();
		email.setSmtpPort(25);
//		email.setHostName("smtp-int.homecredit.id");
		email.setHostName(BatchConstant.Common.LOCALHOST);
		email.setAuthenticator(new DefaultAuthenticator(BatchConstant.Common.USER_HCID_WA, BatchConstant.Common.PASS_HCID_WA));
		email.setFrom(BatchConstant.Common.EMAIL_HCID_WA, "Workaround");
		email.setSubject(subject);
		email.setMsg(content.toString());
		/*email.setSubject("JFS - HCID Payment");
		email.setMsg("There are new HCID Payment that have to be registered to BTPN \n\n " +
						"You can find the file: \n" +
						"/WS/Data/JFS/Export/JFS_PAYMENT_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes \n " +
						"/WS/Data/JFS/Export/JFS_PAYMENT_DETAIL" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt");*/ 
		email.addTo(BatchConstant.Common.EMAIL_JFS);
		email.send();
	}
}
