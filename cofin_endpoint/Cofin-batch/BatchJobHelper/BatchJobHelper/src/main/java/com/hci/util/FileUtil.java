package com.hci.util;

import java.util.List;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.hci.constant.BatchConstant;

public class FileUtil {
	public String createFile(List<String> paymentData, String fileName) {
		File fileToWrite = new File(fileName);
		try {
			for(String payment : paymentData) {
				FileUtils.writeStringToFile(fileToWrite, payment + "\n", true);
			}
			return BatchConstant.Common.SUCCESS;
		} catch (IOException e) {
			e.printStackTrace();
			return BatchConstant.Common.ERROR;
		}
	}
}
