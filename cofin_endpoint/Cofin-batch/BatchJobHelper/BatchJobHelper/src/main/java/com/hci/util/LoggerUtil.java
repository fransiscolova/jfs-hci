package com.hci.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerUtil {
	
	private static final Logger logger = LogManager.getLogger(LoggerUtil.class);
	
	public void debug(String cls, String message) {
		logger.debug(cls + " : " + message);
	}
	
	public void info(String cls, String message) {
		logger.info(cls + " : " + message);
	}
	
	public void error(String cls, String message) {
		logger.error(cls + " : " + message);
	}
	
	public void fatal(String cls, String message) {
		logger.fatal(cls + " : " + message);
	}
	
	public void warn(String cls, String message) {
		logger.warn(cls + " : " + message);
	}
}
