package com.hci.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class OtherUtil {

    public boolean checkJFSRun() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date startTime = sdf.parse(new PropertyUtil().getStartJFSJob());
        Date endTime = sdf.parse(new PropertyUtil().getEndJFSJob());
        String currentTime = sdf.format(new Date());
        if ((startTime.compareTo(sdf.parse(currentTime)) == 1 && endTime.compareTo(sdf
                .parse(currentTime)) == -1)
                || startTime.compareTo(sdf.parse(currentTime)) == 0
                || endTime.compareTo(sdf.parse(currentTime)) == 0) {
            // This is within JFS Run period, prioritize today job, intead of rerun

            // But if all job has been runed, then, they can run the rerun job
        }
        return true;
    }

    /**
     * Pad parameter {@code str} with white-space on the left until match {@code length} characters. <br>
     * If {@code str} is longer than {@code length}, no padding will be done. <br>
     * ex: <br>
     * str = "123" and n = 5 then "  123". <br>
     * str = "abcdef" and n = 3 then "abcdef" <br>
     * str = "" and n = 10 then "          " <br>
     *
     * @param str
     * @param length
     * @return paddedString
     */
    public String leftPaddingSpace(String str, Integer length) {
        return String.format("%1$" + length + "s", str == null ? "" : str);
    }

    /**
     * Pad parameter {@code str} with white-space on the right until match {@code length}
     * characters. <br>
     * If {@code str} is longer than {@code length}, no padding will be done. <br>
     * ex: <br>
     * str = "123" and n = 5 then "123  ". <br>
     * str = "abcdef" and n = 3 then "abcdef" <br>
     * str = "" and n = 10 then "          " <br>
     *
     * @param str
     * @param length
     * @return paddedString
     */
    public String rightPaddingSpace(String str, Integer length) {
        return String.format("%1$-" + length + "s", str == null ? "" : str);
    }

    public String uuidGenerator() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
