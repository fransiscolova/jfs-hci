package com.hci.util;

import java.sql.SQLException;
import javax.sql.DataSource;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.hci.data.entity.ShellConnectionObject;

public class PropertyUtil {
    LoggerUtil log = new LoggerUtil();
    CompositeConfiguration prop = new CompositeConfiguration();
    // String propFile = "C:/Users/Michael.Stefen/Desktop/db.properties";
    String propFile = "/WS/convertor/batch/properties/db.properties";

    // String propFile = "/cofin/batch/app/db.properties";

    public PropertyUtil() {
        try {
            prop.addConfiguration(new PropertiesConfiguration(propFile));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public DataSource getContextConfigurationDatasource() {
        OracleDataSource oracleDS = null;
        try {
            oracleDS = new OracleDataSource();
            oracleDS.setURL((String) prop.getProperty("oracle.conf.url"));
            oracleDS.setUser((String) prop.getProperty("oracle.conf.user"));
            oracleDS.setPassword((String) prop.getProperty("oracle.conf.password"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oracleDS;
    }

    public DataSource getContextDatasource() {
        OracleDataSource oracleDS = null;
        try {
            oracleDS = new OracleDataSource();
            oracleDS.setURL((String) prop.getProperty("oracle.url"));
            oracleDS.setUser((String) prop.getProperty("oracle.user"));
            oracleDS.setPassword((String) prop.getProperty("oracle.password"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oracleDS;
    }

    public String getEndJFSJob() {
        return (String) prop.getProperty("jfs.end");
    }

    public String getProperty(String key) {
        return (String) prop.getProperty(key);
    }

    public ShellConnectionObject getShellConnectionObject() {
        ShellConnectionObject sco = new ShellConnectionObject();
        sco.setUsername((String) prop.getProperty("ssh.username"));
        sco.setPassword((String) prop.getProperty("ssh.password"));
        sco.setIp((String) prop.getProperty("ssh.ip"));
        sco.setPort(Integer.valueOf((String) prop.getProperty("ssh.port")));

        return sco;
    }

    public String getStartJFSJob() {
        return (String) prop.getProperty("jfs.start");
    }

    public void setProperty(String key, String value) {
        try {
            PropertiesConfiguration config = new PropertiesConfiguration(propFile);
            config.setProperty(key, value);
            config.save();
        } catch (ConfigurationException e) {
            log.info("PropertyUtil", "ConfigurationException");
        }
    }
}
