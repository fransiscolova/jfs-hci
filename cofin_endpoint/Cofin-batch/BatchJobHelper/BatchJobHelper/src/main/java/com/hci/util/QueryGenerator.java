/**
 *
 */
package com.hci.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Query generator for GTT_JSF_003 from txt delimited by "|" to sql.
 *
 * @author muhammad.muflihun
 *
 */
public class QueryGenerator {

    private static final String readPath = "C:\\Users\\muhammad.muflihun\\Documents\\EXPORT SQL\\test.dsv";
    private static final String writePath = "C:\\Users\\muhammad.muflihun\\Documents\\EXPORT SQL\\query.sql";

    /**
     * @param args
     */
    public static void main(String[] args) {
        File readFile = new File(readPath);
        FileReader fr = null;
        BufferedReader br = null;
        FileWriter fw = null;

        try {
            fr = new FileReader(readFile);
            br = new BufferedReader(fr);
            fw = new FileWriter(writePath);

            String line;
            Boolean firstRow = true;
            StringBuilder queryPrefix = new StringBuilder("insert into GTT_JSF_003 (");
            while ((line = br.readLine()) != null) {
                // System.out.println(line);
                String writeLine = queryPrefix.toString();
                // System.out.println("pre " + queryPrefix.toString());
                if (firstRow) {
                    firstRow = false;
                    String[] lineElement = line.split("\\|");
                    for (int i = 0; i < lineElement.length; i++) {
                        String element = lineElement[i];
                        element = element.replaceAll("'", "");
                        if (i == lineElement.length - 1) {
                            queryPrefix.append(element).append(") values (");
                        } else {
                            queryPrefix.append(element).append(",");
                        }
                    }
                    continue;
                } else {
                    String[] lineElement = line.split("\\|");
                    for (int i = 0; i < lineElement.length; i++) {
                        String element = lineElement[i];
                        // System.out.println(element);
                        if (i == lineElement.length - 1) {
                            writeLine = writeLine + element + ");";
                        } else {
                            if (element == null || element.isEmpty()) {
                                writeLine = writeLine + "null,";
                            } else if (element.contains("'")) {
                                if (element.replaceAll("'", "").isEmpty()) {
                                    writeLine = writeLine + "null,";
                                } else {
                                    writeLine = writeLine + element + ",";
                                }
                            } else if (element.contains("-")) {
                                writeLine = writeLine + "to_date('" + element + "', 'DD-MM-RRRR'),";
                            } else if (element.contains(",")) {
                                writeLine = writeLine + "'" + element + "'" + ",";
                            } else {
                                writeLine = writeLine + element + ",";
                            }
                        }
                    }
                }

                fw.write(writeLine);
                fw.write("\r\n");
            }
            br.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
