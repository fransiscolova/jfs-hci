/**
 *
 */
package com.hci.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Query generator for DAO impl using string builder.
 *
 * @author muhammad.muflihun
 *
 */
public class QueryGenerator2 {

    private static final String readPath = "C:\\Users\\muhammad.muflihun\\Documents\\EXPORT SQL\\string builder\\raw.txt";
    private static final String writePath = "C:\\Users\\muhammad.muflihun\\Documents\\EXPORT SQL\\string builder\\result.txt";

    /**
     * @param args
     */
    public static void main(String[] args) {
        File readFile = new File(readPath);
        FileReader fr = null;
        BufferedReader br = null;
        FileWriter fw = null;

        try {
            fr = new FileReader(readFile);
            br = new BufferedReader(fr);
            fw = new FileWriter(writePath);

            String line;
            // StringBuilder queryPrefix = new StringBuilder("sb.append(\" ");
            while ((line = br.readLine()) != null) {
                System.out.println(line.toUpperCase());
                // String writeLine = queryPrefix.toString();
                // writeLine = writeLine + line + " \");";
                // System.out.println(writeLine);
                //
                // fw.write(writeLine);
                // fw.write("\r\n");
            }
            br.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
