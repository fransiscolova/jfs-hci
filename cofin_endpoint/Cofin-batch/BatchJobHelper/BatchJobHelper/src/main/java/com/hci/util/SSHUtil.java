package com.hci.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.hci.data.entity.ShellConnectionObject;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SSHUtil {
	private JSch jschSSHChannel;
	private String strUserName;
	private String strConnectionIP;
	private int intConnectionPort;
	private String strPassword;
	private Session sesConnection;
	private int intTimeOut;
	
	private void doCommonConstructorActions(String userName, String password,String connectionIP, String knownHostsFileName) {
		
		jschSSHChannel = new JSch();

		/*try {
			jschSSHChannel.setKnownHosts(knownHostsFileName);
		} catch (JSchException jschX) {
			jschX.printStackTrace();
		}*/

		strUserName = userName;
		strPassword = password;
		strConnectionIP = connectionIP;
	}
	
	public SSHUtil(String userName, String password, String connectionIP, String knownHostsFileName, int connectionPort) {
		doCommonConstructorActions(userName, password, connectionIP, knownHostsFileName);
		intConnectionPort = connectionPort;
		intTimeOut = 60000;
	}
	
	public SSHUtil() {
	}
	
	public String connect() {
		String errorMessage = null;

		try {
			sesConnection = jschSSHChannel.getSession(strUserName, strConnectionIP, intConnectionPort);
			sesConnection.setPassword(strPassword);
			
			// UNCOMMENT THIS FOR TESTING PURPOSES, BUT DO NOT USE IN PRODUCTION
			sesConnection.setConfig("StrictHostKeyChecking", "no");
			
			sesConnection.connect(intTimeOut);
		} catch (JSchException jschX) {
			errorMessage = jschX.getMessage();
		}

		return errorMessage;
	}
	
	public List<String> sendCommand(String command) {
		List<String> message = new ArrayList<String>();
		
		try {
			Channel channel = sesConnection.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			InputStream inputStream = channel.getInputStream();
			channel.connect();

			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));			  
			
			String line;			  
			while ((line = reader.readLine()) != null) {
				message.add(line);
			}
  
			channel.disconnect();
		} catch (IOException ioX) {
			ioX.printStackTrace();
			return null;
		} catch (JSchException jschX) {
			jschX.printStackTrace();
			return null;
		}

		return message;
	}
	
	public String removeAndWriteToFile(ShellConnectionObject shellCObj, String filename, List<String> texts) {
		String writeStatus = "";
		
		try {
			SSHUtil sshUtil = new SSHUtil(shellCObj.getUsername(), shellCObj.getPassword(), shellCObj.getIp(), shellCObj.getHost(), shellCObj.getPort());
			sshUtil.connect();
			
			sshUtil.sendCommand("rm -f " + filename);
			sshUtil.sendCommand("touch " + filename);
		
			for (String text : texts) {
				sshUtil.sendCommand("printf '" + text + "\n' >> " + filename);
			}
			writeStatus = "Success";
		} catch (Exception e) {
			e.printStackTrace();
			writeStatus = e.getMessage();
		}
		
		return writeStatus;
	}
	
	public void close() {
		sesConnection.disconnect();
	}	
	
	public List<String> sendCommandStdAlone(String command, ShellConnectionObject shellCObj) {
		List<String> message = new ArrayList<String>();
		
		SSHUtil sshUtil = new SSHUtil(shellCObj.getUsername(), shellCObj.getPassword(), shellCObj.getIp(), shellCObj.getHost(), shellCObj.getPort());
		sshUtil.connect();
		
		message = sshUtil.sendCommand(command);
		
		sshUtil.close();

		return message;
	}
}
