package com.hci;

import org.quartz.JobExecutionContext;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;

import com.hci.util.SpringContextHolder;

public class JobLauncherJFS2Export implements org.quartz.Job {
    private Job job;
    @SuppressWarnings("unused")
    private JobLocator jobLocator;
    private JobLauncher jobLauncher;

    public void execute(JobExecutionContext context) {
        ApplicationContext applicationContext = SpringContextHolder.getApplicationContext();
        job = (Job) applicationContext.getBean("jfs2ExportJob");
        jobLocator = ((JobLocator) applicationContext.getBean("jfs2ExportJobRegistry"));
        jobLauncher = (JobLauncher) applicationContext.getBean("jobLauncherJFS2Export");
        try {
            JobParameters jobParameters = new JobParametersBuilder().addLong("time",
                    System.currentTimeMillis()).toJobParameters();
            jobLauncher.run(job, jobParameters);
        } catch (JobExecutionException e) {
            e.printStackTrace();
        }
    }
}
