package com.hci.data.dao;

public interface Jfs2ExportBarangDao {

    /**
     * Return mapped barang entity class for specified implements.
     *
     * @return
     */
    public Object getJfs2ExporBarangData();

    /**
     * Return mapped barang entity class for specified implements by string date.
     *
     * @param runDate
     * @return
     */
    public Object getJfs2ExporBarangData(String runDate);
}
