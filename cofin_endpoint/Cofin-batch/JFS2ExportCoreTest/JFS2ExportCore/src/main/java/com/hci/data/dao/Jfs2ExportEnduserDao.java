package com.hci.data.dao;

public interface Jfs2ExportEnduserDao {

    /**
     * Return mapped end user entity class for specified implements by string date.
     *
     * @return
     */
    public Object getJfs2ExporEndusertData();

    /**
     * Return mapped end user entity class for specified implements by string date.
     *
     * @param runDate
     * @return
     */
    public Object getJfs2ExporEndusertData(String runDate);
}
