package com.hci.data.dao;


public interface Jfs2ExportKontrakDao {

    /**
     * Return mapped kontrak entity class for specified implements by string date.
     *
     * @return
     */
    public Object getJfs2ExporKontrakData();

    /**
     * Return mapped kontrak entity class for specified implements by string date.
     *
     * @param runDate
     * @return
     */
    public Object getJfs2ExporKontrakData(String runDate);

    /**
     * Return mapped kontrak footer entity class for specified implements by string date.
     *
     * @return
     */
    public Object getJfs2ExportKontrakFooterData();

    /**
     * Return mapped kontrak footer entity class for specified implements by string date.
     *
     * @param runDate
     * @return
     */
    public Object getJfs2ExportKontrakFooterData(String runDate);
}
