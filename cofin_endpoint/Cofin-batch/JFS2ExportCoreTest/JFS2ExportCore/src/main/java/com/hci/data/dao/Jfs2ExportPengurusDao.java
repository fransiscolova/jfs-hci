package com.hci.data.dao;

public interface Jfs2ExportPengurusDao {

    /**
     * Return mapped pengurus entity class for specified implements by string date.
     *
     * @param runDate
     * @return
     */
    public Object getJfs2ExporPengurusData();

    /**
     * Return mapped pengurus entity class for specified implements by string date.
     *
     * @param runDate
     * @return
     */
    public Object getJfs2ExporPengurusData(String runDate);
}
