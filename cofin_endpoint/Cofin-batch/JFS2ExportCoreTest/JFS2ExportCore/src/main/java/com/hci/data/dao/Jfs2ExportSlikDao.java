package com.hci.data.dao;


public interface Jfs2ExportSlikDao {

    /**
     * Return mapped slik entity class for specified implements by string date.
     *
     * @param runDate
     * @return
     */
    public Object getJfs2ExporSlikData();

    /**
     * Return mapped slik entity class for specified implements by string date.
     *
     * @param runDate
     * @return
     */
    public Object getJfs2ExporSlikData(String runDate);
}
