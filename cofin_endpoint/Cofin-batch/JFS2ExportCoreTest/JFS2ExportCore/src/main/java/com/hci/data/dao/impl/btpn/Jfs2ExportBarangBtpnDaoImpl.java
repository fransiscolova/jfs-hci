package com.hci.data.dao.impl.btpn;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportBarangDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.btpn.Jfs2ExportBarangBtpn;
import com.hci.data.mapper.btpn.Jfs2ExportMapperBarangBtpn;

public class Jfs2ExportBarangBtpnDaoImpl implements Jfs2ExportBarangDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public Jfs2ExportBarangBtpnDaoImpl() {
        dataSource = new DatabaseConnection().getContextDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Jfs2ExportBarangBtpn> getJfs2ExporBarangData() {
        StringBuilder sb = new StringBuilder(3000);
        sb.append(" SELECT DISTINCT * ");
        sb.append(" FROM ( ");
        sb.append("     SELECT c.contract_code ");
        sb.append("         ,( ");
        sb.append("             CASE  ");
        sb.append("                 WHEN upper(trim(d.name1)) = upper(trim(d.name2)) ");
        sb.append("                     THEN d.name1 ");
        sb.append("                 ELSE d.full_name ");
        sb.append("                 END ");
        sb.append("             ) AS NAME ");
        sb.append("         ,hc.engine_number ");
        sb.append("         ,substr(hc.model_number || ' ' || hc.serial_number, 1, 22) AS MODEL_SERIAL_NUMBER ");
        sb.append("         ,ct.btpn_code_commodity_category AS BTPN_CODE_COMMODITY_CATEGORY ");
        sb.append("         ,ct.btpn_code_commodity_type AS BTPN_CODE_COMMODITY_TYPE ");
        sb.append("         ,to_char(sysdate, 'YYYY') AS SISDATE ");
        sb.append("         ,CASE  ");
        sb.append("             WHEN ct.btpn_code_commodity_type IN ( ");
        sb.append("                     'CT_CT_PDA' ");
        sb.append("                     ,'CT_CT_MTAB' ");
        sb.append("                     ) ");
        sb.append("                 THEN 'DGN2' ");
        sb.append("             ELSE cg.btpn_commodity_group ");
        sb.append("             END AS COMMODITY ");
        sb.append("         ,hc.producer ");
        sb.append("         ,jc.id_agreement AS AGREEMENT_ID ");
        sb.append("         ,ja.partner_id ");
        sb.append("     FROM jfs_contract jc ");
        sb.append("     LEFT JOIN ( ");
        sb.append("         SELECT DISTINCT contract_code ");
        sb.append("             ,id ");
        sb.append("             ,deal_code ");
        sb.append("         FROM GTT_JSF_002 ");
        sb.append("         ) c ON jc.text_contract_number = c.contract_code ");
        sb.append("     LEFT JOIN ( ");
        sb.append("         SELECT DISTINCT name1 ");
        sb.append("             ,name2 ");
        sb.append("             ,full_name ");
        sb.append("             ,code ");
        sb.append("         FROM GTT_JSF_005 lm ");
        sb.append("         LEFT JOIN gtt_jsf_012 dbl ON lm.cuid = dbl.cuid ");
        sb.append("         LEFT JOIN gtt_jsf_011 sbs ON dbl.id_document = sbs.document_id ");
        sb.append("         WHERE sbs.DOCUMENT_TYPE = 'KTP' ");
        sb.append("             AND sbs.ID_CARD_STATUS = 'a' ");
        sb.append("         ) d ON c.deal_code = d.code ");
        sb.append("     LEFT JOIN GTT_JSF_004 hc ON c.id = hc.contract_id ");
        sb.append("     LEFT JOIN jfs_commodity_type ct ON hc.commodity_category_code = ct.hci_code_commodity_category ");
        sb.append("         AND hc.COMMODITY_TYPE_CODE = ct.hci_code_commodity_type ");
        sb.append("     LEFT JOIN JFS_COMMODITY_GROUP cg ON hc.commodity_category_code = cg.hci_code_commodity_category ");
        sb.append("         AND jc.id_agreement = cg.id_agreement ");
        sb.append("     LEFT JOIN ( ");
        sb.append("         SELECT * ");
        sb.append("         FROM jfs_agreement ");
        sb.append("         WHERE REGEXP_LIKE(code, '^[[:digit:]]+$') ");
        sb.append("         ) ja ON to_number(ja.code, '99') = jc.id_agreement ");
        sb.append("     WHERE trunc(jc.export_date) = trunc(sysdate) ");
        sb.append("         AND jc.id_agreement < 4 ");
        sb.append("     ORDER BY jc.id_agreement ");
        sb.append("         ,c.contract_code ");
        sb.append("     ) ");
        List<Jfs2ExportBarangBtpn> jfsPayment = jdbcTemplateObject.query(sb.toString(),
                new Jfs2ExportMapperBarangBtpn());
        return jfsPayment;
    }

    @Override
    public List<Jfs2ExportBarangBtpn> getJfs2ExporBarangData(String runDate) {
        StringBuilder sb = new StringBuilder(3000);
        sb.append(" SELECT DISTINCT * ");
        sb.append(" FROM ( ");
        sb.append("     SELECT c.contract_code ");
        sb.append("         ,( ");
        sb.append("             CASE  ");
        sb.append("                 WHEN upper(trim(d.name1)) = upper(trim(d.name2)) ");
        sb.append("                     THEN d.name1 ");
        sb.append("                 ELSE d.full_name ");
        sb.append("                 END ");
        sb.append("             ) AS NAME ");
        sb.append("         ,hc.engine_number ");
        sb.append("         ,substr(hc.model_number || ' ' || hc.serial_number, 1, 22) AS MODEL_SERIAL_NUMBER ");
        sb.append("         ,ct.btpn_code_commodity_category AS BTPN_CODE_COMMODITY_CATEGORY ");
        sb.append("         ,ct.btpn_code_commodity_type AS BTPN_CODE_COMMODITY_TYPE ");
        sb.append("         ,to_char(sysdate, 'YYYY') AS SISDATE ");
        sb.append("         ,CASE  ");
        sb.append("             WHEN ct.btpn_code_commodity_type IN ( ");
        sb.append("                     'CT_CT_PDA' ");
        sb.append("                     ,'CT_CT_MTAB' ");
        sb.append("                     ) ");
        sb.append("                 THEN 'DGN2' ");
        sb.append("             ELSE cg.btpn_commodity_group ");
        sb.append("             END AS COMMODITY ");
        sb.append("         ,hc.producer ");
        sb.append("         ,jc.id_agreement AS AGREEMENT_ID ");
        sb.append("         ,ja.partner_id ");
        sb.append("     FROM jfs_contract jc ");
        sb.append("     LEFT JOIN ( ");
        sb.append("         SELECT DISTINCT contract_code ");
        sb.append("             ,id ");
        sb.append("             ,deal_code ");
        sb.append("         FROM GTT_JSF_002 ");
        sb.append("         ) c ON jc.text_contract_number = c.contract_code ");
        sb.append("     LEFT JOIN ( ");
        sb.append("         SELECT DISTINCT name1 ");
        sb.append("             ,name2 ");
        sb.append("             ,full_name ");
        sb.append("             ,code ");
        sb.append("         FROM GTT_JSF_005 lm ");
        sb.append("         LEFT JOIN gtt_jsf_012 dbl ON lm.cuid = dbl.cuid ");
        sb.append("         LEFT JOIN gtt_jsf_011 sbs ON dbl.id_document = sbs.document_id ");
        sb.append("         WHERE sbs.DOCUMENT_TYPE = 'KTP' ");
        sb.append("             AND sbs.ID_CARD_STATUS = 'a' ");
        sb.append("         ) d ON c.deal_code = d.code ");
        sb.append("     LEFT JOIN GTT_JSF_004 hc ON c.id = hc.contract_id ");
        sb.append("     LEFT JOIN jfs_commodity_type ct ON hc.commodity_category_code = ct.hci_code_commodity_category ");
        sb.append("         AND hc.COMMODITY_TYPE_CODE = ct.hci_code_commodity_type ");
        sb.append("     LEFT JOIN JFS_COMMODITY_GROUP cg ON hc.commodity_category_code = cg.hci_code_commodity_category ");
        sb.append("         AND jc.id_agreement = cg.id_agreement ");
        sb.append("     LEFT JOIN ( ");
        sb.append("         SELECT * ");
        sb.append("         FROM jfs_agreement ");
        sb.append("         WHERE REGEXP_LIKE(code, '^[[:digit:]]+$') ");
        sb.append("         ) ja ON to_number(ja.code, '99') = jc.id_agreement ");
        sb.append("     WHERE trunc(jc.export_date) = ");
        sb.append("           trunc(to_date('").append(runDate).append("', 'yyyy-mm-dd')) ");
        sb.append("         AND jc.id_agreement < 4 ");
        sb.append("     ORDER BY jc.id_agreement ");
        sb.append("         ,c.contract_code ");
        sb.append("     ) ");
        List<Jfs2ExportBarangBtpn> jfsPayment = jdbcTemplateObject.query(sb.toString(),
                new Jfs2ExportMapperBarangBtpn());
        return jfsPayment;
    }
}