package com.hci.data.dao.impl.btpn;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportPengurusDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.btpn.Jfs2ExportPengurusBtpn;
import com.hci.data.mapper.btpn.Jfs2ExportMapperPengurusBtpn;

public class Jfs2ExportPengurusBtpnDaoImpl implements Jfs2ExportPengurusDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public Jfs2ExportPengurusBtpnDaoImpl() {
        dataSource = new DatabaseConnection().getContextDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Jfs2ExportPengurusBtpn> getJfs2ExporPengurusData() {
        StringBuilder sb = new StringBuilder(400);
        sb.append(" SELECT c.contract_code ");
        sb.append(" FROM jfs_contract jc ");
        sb.append(" INNER JOIN ( ");
        sb.append("     SELECT DISTINCT contract_code ");
        sb.append("     FROM GTT_JSF_002 ");
        sb.append("     ) c ON jc.text_contract_number = c.contract_code ");
        sb.append(" WHERE trunc(jc.export_date) = trunc(sysdate) ");
        sb.append("     AND jc.id_agreement < 4 ");
        sb.append(" ORDER BY jc.id_agreement ");
        sb.append("     ,c.contract_code ");
        List<Jfs2ExportPengurusBtpn> jfsPayment = jdbcTemplateObject.query(sb.toString(),
                new Jfs2ExportMapperPengurusBtpn());
        return jfsPayment;
    }

    @Override
    public List<Jfs2ExportPengurusBtpn> getJfs2ExporPengurusData(String runDate) {
        StringBuilder sb = new StringBuilder(6000);
        sb.append(" SELECT c.contract_code ");
        sb.append(" FROM jfs_contract jc ");
        sb.append(" INNER JOIN ( ");
        sb.append("     SELECT DISTINCT contract_code ");
        sb.append("     FROM GTT_JSF_002 ");
        sb.append("     ) c ON jc.text_contract_number = c.contract_code ");
        sb.append(" WHERE trunc(jc.export_date) = ");
        sb.append("       trunc(to_date('").append(runDate).append("', 'yyyy-mm-dd')) ");
        sb.append("     AND jc.id_agreement < 4 + ");
        sb.append(" ORDER BY jc.id_agreement ");
        sb.append("     ,c.contract_code ");
        List<Jfs2ExportPengurusBtpn> jfsPayment = jdbcTemplateObject.query(sb.toString(),
                new Jfs2ExportMapperPengurusBtpn());
        return jfsPayment;
    }
}
