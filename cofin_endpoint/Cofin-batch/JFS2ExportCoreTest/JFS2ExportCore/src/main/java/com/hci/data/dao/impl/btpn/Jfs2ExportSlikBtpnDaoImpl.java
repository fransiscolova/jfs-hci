package com.hci.data.dao.impl.btpn;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportSlikDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.btpn.Jfs2ExportSlikBtpn;
import com.hci.data.mapper.btpn.Jfs2ExportMapperSlikBtpn;

public class Jfs2ExportSlikBtpnDaoImpl implements Jfs2ExportSlikDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public Jfs2ExportSlikBtpnDaoImpl() {
        dataSource = new DatabaseConnection().getContextDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Jfs2ExportSlikBtpn> getJfs2ExporSlikData() {
        StringBuilder sb = new StringBuilder(2000);
        sb.append(" SELECT jc.text_contract_number ");
        sb.append("     ,s.KODE_SIFAT_KREDIT_PEMBIAYAAN ");
        sb.append("     ,s.KODE_JENIS_KREDIT ");
        sb.append("     ,s.KODE_SKIM ");
        sb.append("     ,s.BARU_PERPANJANGAN ");
        sb.append("     ,s.KODE_JENIS_PENGGUNAAN ");
        sb.append("     ,s.KODE_ORIENTASI_PENGGUNAAN ");
        sb.append("     ,s.TAKEOVER_DARI ");
        sb.append("     ,s.ALAMAT_EMAIL ");
        sb.append("     ,s.NO_TELEPON_SELULAR ");
        sb.append("     ,s.ALAMAT_TEMPAT_BEKERJA ");
        sb.append("     ,s.PENGHASILAN_KOTOR_PER_TAHUN ");
        sb.append("     ,s.KODE_SUMBER_PENGHASILAN ");
        sb.append("     ,s.JUMLAH_TANGGUNGAN ");
        sb.append("     ,s.NAMA_PASANGAN ");
        sb.append("     ,s.NO_IDENTITAS_PASANGAN ");
        sb.append("     ,s.TANGGAL_IDENTITAS_PASANGAN ");
        sb.append("     ,s.PERJANJIAN_PISAH_HARTA ");
        sb.append("     ,s.KODE_BIDANG_USAHA ");
        sb.append("     ,s.KODE_GOLONGAN_DEBITUR ");
        sb.append("     ,s.KODE_BENTUK_BADAN_USAHA ");
        sb.append("     ,s.GO_PUBLIC ");
        sb.append("     ,s.PERINGKAT_RATING_DEBITUR ");
        sb.append("     ,s.LEMBAGA_PEMERINGKAT_RATING ");
        sb.append("     ,s.TANGGAL_PEMERINGKAT ");
        sb.append("     ,s.NAMA_GRUP_DEBITUR ");
        sb.append("     ,s.KODE_JENIS_AGUNAN ");
        sb.append("     ,s.KODE_JENIS_PENGIKATAN ");
        sb.append("     ,s.TANGGAL_PENGIKATAN ");
        sb.append("     ,s.NAMA_PEMILIK_AGUNAN ");
        sb.append("     ,s.ALAMAT_AGUNAN ");
        sb.append("     ,s.DATI2 ");
        sb.append("     ,s.STATUS_KREDIT_JOIN ");
        sb.append("     ,s.DIASURANSIKAN ");
        sb.append(" FROM JFS_CONTRACT jc ");
        sb.append(" JOIN jfs_slik_tmp_dwh s ON jc.text_contract_number = s.text_contract_number ");
        sb.append(" WHERE JC.ID_AGREEMENT < 4 ");
        sb.append("     AND trunc(jc.export_date) = trunc(sysdate) ");
        List<Jfs2ExportSlikBtpn> jfsPayment = jdbcTemplateObject.query(sb.toString(),
                new Jfs2ExportMapperSlikBtpn());
        return jfsPayment;
    }

    @Override
    public List<Jfs2ExportSlikBtpn> getJfs2ExporSlikData(String runDate) {
        StringBuilder sb = new StringBuilder(2000);
        sb.append(" SELECT jc.text_contract_number ");
        sb.append("     ,s.KODE_SIFAT_KREDIT_PEMBIAYAAN ");
        sb.append("     ,s.KODE_JENIS_KREDIT ");
        sb.append("     ,s.KODE_SKIM ");
        sb.append("     ,s.BARU_PERPANJANGAN ");
        sb.append("     ,s.KODE_JENIS_PENGGUNAAN ");
        sb.append("     ,s.KODE_ORIENTASI_PENGGUNAAN ");
        sb.append("     ,s.TAKEOVER_DARI ");
        sb.append("     ,s.ALAMAT_EMAIL ");
        sb.append("     ,s.NO_TELEPON_SELULAR ");
        sb.append("     ,s.ALAMAT_TEMPAT_BEKERJA ");
        sb.append("     ,s.PENGHASILAN_KOTOR_PER_TAHUN ");
        sb.append("     ,s.KODE_SUMBER_PENGHASILAN ");
        sb.append("     ,s.JUMLAH_TANGGUNGAN ");
        sb.append("     ,s.NAMA_PASANGAN ");
        sb.append("     ,s.NO_IDENTITAS_PASANGAN ");
        sb.append("     ,s.TANGGAL_IDENTITAS_PASANGAN ");
        sb.append("     ,s.PERJANJIAN_PISAH_HARTA ");
        sb.append("     ,s.KODE_BIDANG_USAHA ");
        sb.append("     ,s.KODE_GOLONGAN_DEBITUR ");
        sb.append("     ,s.KODE_BENTUK_BADAN_USAHA ");
        sb.append("     ,s.GO_PUBLIC ");
        sb.append("     ,s.PERINGKAT_RATING_DEBITUR ");
        sb.append("     ,s.LEMBAGA_PEMERINGKAT_RATING ");
        sb.append("     ,s.TANGGAL_PEMERINGKAT ");
        sb.append("     ,s.NAMA_GRUP_DEBITUR ");
        sb.append("     ,s.KODE_JENIS_AGUNAN ");
        sb.append("     ,s.KODE_JENIS_PENGIKATAN ");
        sb.append("     ,s.TANGGAL_PENGIKATAN ");
        sb.append("     ,s.NAMA_PEMILIK_AGUNAN ");
        sb.append("     ,s.ALAMAT_AGUNAN ");
        sb.append("     ,s.DATI2 ");
        sb.append("     ,s.STATUS_KREDIT_JOIN ");
        sb.append("     ,s.DIASURANSIKAN ");
        sb.append(" FROM JFS_CONTRACT jc ");
        sb.append(" JOIN jfs_slik_tmp_dwh s ON jc.text_contract_number = s.text_contract_number ");
        sb.append(" WHERE JC.ID_AGREEMENT < 4 ");
        sb.append("     AND trunc(jc.export_date) = ");
        sb.append("         trunc(to_date('").append(runDate).append("', 'yyyy-mm-dd')) ");
        List<Jfs2ExportSlikBtpn> jfsPayment = jdbcTemplateObject.query(sb.toString(),
                new Jfs2ExportMapperSlikBtpn());
        return jfsPayment;
    }
}
