/**
 *
 */
package com.hci.data.dao.impl.permata;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportBarangDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.permata.Jfs2ExportBarangPermata;

/**
 * Provide data for barang permata.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportBarangPermataDaoImpl implements Jfs2ExportBarangDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public Jfs2ExportBarangPermataDaoImpl() {
        dataSource = new DatabaseConnection().getContextDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    private List<Jfs2ExportBarangPermata> dummyData(Integer rows) {
        List<Jfs2ExportBarangPermata> list = new ArrayList<Jfs2ExportBarangPermata>();
        list.add(new Jfs2ExportBarangPermata());
        for (Integer row = 1; row <= rows; row++) {
            Jfs2ExportBarangPermata model = new Jfs2ExportBarangPermata();
            model.setNO_PIN("3700250883");
            model.setCAR_REGISTRATION_NO("");
            model.setCAR_REGISTRATION_DATE("");
            model.setNAME_ON_CAR_REGISTRATION("Yosep Krismono");
            model.setPLACE_OF_CAR_REGISTRATION("");
            model.setENGINE_NUMBER("Redmi 4A");
            model.setCHASIS_NUMBER("");
            model.setLISENCE_PLATE_NUMBER("");
            model.setBRAND("CAT_MP");
            model.setTYPE("CT_TE_MP");
            model.setCONDITION_OF_GOODS("N");
            model.setASSEMBLY_YEAR("2017");
            model.setTYPE_PF_GOODS("DGN2");
            model.setTYPE_OF_GOODS("Xiomi");
            model.setVALUE_OF_GOODS("1500000");
            list.add(model);
        }
        return list;
    }

    @Override
    public List<Jfs2ExportBarangPermata> getJfs2ExporBarangData() {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperBarangPermata());
        return dummyData(5);
    }

    @Override
    public List<Jfs2ExportBarangPermata> getJfs2ExporBarangData(String rowDate) {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperBarangPermata());
        return dummyData(5);
    }
}
