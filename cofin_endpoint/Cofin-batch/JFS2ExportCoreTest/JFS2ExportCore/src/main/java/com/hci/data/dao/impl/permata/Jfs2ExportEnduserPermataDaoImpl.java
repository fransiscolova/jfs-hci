/**
 *
 */
package com.hci.data.dao.impl.permata;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportEnduserDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.permata.Jfs2ExportEnduserPermata;

/**
 * Provide data for end user permata.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportEnduserPermataDaoImpl implements Jfs2ExportEnduserDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public Jfs2ExportEnduserPermataDaoImpl() {
        dataSource = new DatabaseConnection().getContextDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    private List<Jfs2ExportEnduserPermata> dummyData(Integer rows) {
        List<Jfs2ExportEnduserPermata> list = new ArrayList<Jfs2ExportEnduserPermata>();
        list.add(new Jfs2ExportEnduserPermata());
        for (Integer row = 1; row <= rows; row++) {
            Jfs2ExportEnduserPermata model = new Jfs2ExportEnduserPermata();
            model.setNO_PIN("3700252136");
            model.setEND_USER_NAME("Mudiyanto");
            model.setALIAS("Mudiyanto");
            model.setSTATUS("0199");
            model.setSTATUS_DESCRIPTION("SMP(Junior High school)");
            model.setDEBTOR_CLASS("907");
            model.setSEX("1");
            model.setID_CARD_NUMBER("3173010511950010");
            model.setPASSPORT_NO("");
            model.setBIRTH_CERTIFICATE_NO("");
            model.setPLACE_OF_BIRTH("Jakarta");
            model.setDATE_OF_BIRTH("05/11/1995");
            model.setDATE_OF_FINAL_DEED("");
            model.setTAX_REGISTRATION("");
            model.setDEBTORS_ADDRESS("Jl Pedongkelan RT02 RW13");
            model.setSUB_DISTRICT("KAPUK");
            model.setKECAMATAN("CENGKARENG");
            model.setDISTRICT_OF_DEBTOR("0393");
            model.setPOSTAL_CODE("11720");
            model.setAREA_CODE("");
            model.setTELEPHONE("");
            model.setCOUNTRY_OF_DOMICILE("ID");
            model.setMAIDEN_NAME_OF_BIOLOGICAL_MOTHER("Kholifah");
            model.setOCCUPATION_CODE("099");
            model.setPLACE_OF_WORK("PT Elektronik");
            model.setFIELD_OF_BUSINESS("6000");
            model.setID_TYPE("1");
            model.setMARITAL_STATUS("1");
            model.setKEWARGANEGARAAN("ID");
            model.setRELIGION("7");
            model.setINCOME("2100000");
            model.setID_EXPIRED("05/11/2018");
            list.add(model);
        }
        return list;
    }

    @Override
    public List<Jfs2ExportEnduserPermata> getJfs2ExporEndusertData() {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperEnduserPermata());
        return dummyData(5);
    }

    @Override
    public List<Jfs2ExportEnduserPermata> getJfs2ExporEndusertData(String runDate) {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperEnduserPermata());
        return dummyData(5);
    }

}
