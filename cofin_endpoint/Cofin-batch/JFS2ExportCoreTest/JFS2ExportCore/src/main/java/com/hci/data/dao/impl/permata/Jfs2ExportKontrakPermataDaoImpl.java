/**
 *
 */
package com.hci.data.dao.impl.permata;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportKontrakDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.permata.Jfs2ExportKontrakPermata;

/**
 * Provide data for kontrak permata.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportKontrakPermataDaoImpl implements Jfs2ExportKontrakDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public Jfs2ExportKontrakPermataDaoImpl() {
        dataSource = new DatabaseConnection().getContextDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    private List<Jfs2ExportKontrakPermata> dummyData(Integer rows) {
        List<Jfs2ExportKontrakPermata> list = new ArrayList<Jfs2ExportKontrakPermata>();
        list.add(new Jfs2ExportKontrakPermata());
        for (Integer row = 1; row <= 1; row++) {
            Jfs2ExportKontrakPermata model = new Jfs2ExportKontrakPermata();
            model.setNO_PIN("3700240420");
            model.setNAME("Maya Widiana Komsani");
            model.setMF_BRANCH_CODE("0650");
            model.setNO_PK_ENDURSER("3700240420");
            model.setEFFECTIVE_DATE_END_USER("02/03/2017");
            model.setINITIAL_PRINCIPAL_AT_MF("3524600");
            model.setGOODS_VALUE("3899000");
            model.setMF_INITIAL_TENOR("10");
            model.setAMOUNT_OF_INSTALLMENT_TO_MF("440600");
            model.setMF_INTEREST_TO_END_USER("51.30");
            model.setPRINCIPLE_UPON_ASSIGNMENT("3524600");
            model.setBANK_FINANCED_PRINCIPAL("3172140");
            model.setTENOR_WITH_BANK("10");
            model.setINSTALLMENT_PAYMENT_DATE_1("02/04/2017");
            model.setFINAL_INSTALLMENT_DATE("02/01/2018");
            model.setINTEREST_AT_THE_BANK("34.08");
            model.setAMOUNT_OF_INSTALLMENT_TO_BANK("368841");
            model.setINSTALLMENT_PERIOD("1");
            model.setVEHICLE_CONDITION("N");
            model.setTYPE_OF_CLOSING("T");
            model.setINSURANCE_PREMIUM_AMOUNT("0");
            model.setPREMIUM_PAYMENT("E");
            model.setVEHICLE_CLASS("DG");
            model.setCREDIT_SCORING("0");
            model.setDEBT_BURDEN_RATIO("");
            model.setEND_USER_LOAN_AGREEMENT_DATE("02/03/2017");
            model.setAMOUNT_PREMIUM_PAID("0");
            model.setAMOUNT_DP("781000");
            model.setINSURANCE_COMPANY("003300");
            model.setAREA_OF_BUSINESS("K");
            model.setPURPOSE_OF_USAGE("");
            model.setDOCUMENT_PROOF_OF_INCOME("");
            model.setPBB("");
            model.setSPOUSE_KTP("");
            model.setLICENCING("");
            model.setDEED("");
            model.setNPWP("");
            model.setFINANCING_APPLICATION_FORM("");
            model.setFIDUCIARY("");
            model.setCODE_IN_REAR("0");
            list.add(model);
        }
        return list;
    }

    @Override
    public List<Jfs2ExportKontrakPermata> getJfs2ExporKontrakData() {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperKontrakPermata());
        return dummyData(5);
    }

    @Override
    public List<Jfs2ExportKontrakPermata> getJfs2ExporKontrakData(String runDate) {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperKontrakPermata());
        return dummyData(5);
    }

    @Override
    public Object getJfs2ExportKontrakFooterData() {
        return null;
    }

    @Override
    public Object getJfs2ExportKontrakFooterData(String runDate) {
        return null;
    }

}
