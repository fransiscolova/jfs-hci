/**
 *
 */
package com.hci.data.dao.impl.permata;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportPengurusDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.permata.Jfs2ExportPengurusPermata;

/**
 * Provide data for pengurus permata.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportPengurusPermataDaoImpl implements Jfs2ExportPengurusDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public Jfs2ExportPengurusPermataDaoImpl() {
        dataSource = new DatabaseConnection().getContextDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    private List<Jfs2ExportPengurusPermata> dummyData(Integer rows) {
        List<Jfs2ExportPengurusPermata> list = new ArrayList<Jfs2ExportPengurusPermata>();
        list.add(new Jfs2ExportPengurusPermata());
        for (Integer row = 1; row <= rows; row++) {
            Jfs2ExportPengurusPermata model = new Jfs2ExportPengurusPermata();
            model.setNO_PIN("3700250883");
            model.setNAME_OF_MANAGER("");
            model.setID_CARD_OF_MANAGER("");
            model.setMANAGERS_TAX_REGISTRATION_NO("");
            model.setSEX("");
            model.setOWNERSHIP_PORTION("");
            model.setPOSITION_CODE("");
            model.setADDRESS("");
            model.setSUB_DISTRICT("");
            model.setKECAMATAN("");
            model.setDISTRICT_OF_DEBTOR("");
            model.setPOSTAL_CODE("");
            list.add(model);
        }
        return list;
    }

    @Override
    public List<Jfs2ExportPengurusPermata> getJfs2ExporPengurusData() {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperPengurusPermata());
        return dummyData(5);
    }

    @Override
    public List<Jfs2ExportPengurusPermata> getJfs2ExporPengurusData(String runDate) {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperPengurusPermata());
        return dummyData(5);
    }

}
