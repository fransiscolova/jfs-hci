/**
 *
 */
package com.hci.data.dao.impl.permata;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportSlikDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.permata.Jfs2ExportSlikPermata;

/**
 * Provide data for slik permata.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportSlikPermataDaoImpl implements Jfs2ExportSlikDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public Jfs2ExportSlikPermataDaoImpl() {
        dataSource = new DatabaseConnection().getContextDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    private List<Jfs2ExportSlikPermata> dummyData(Integer rows) {
        List<Jfs2ExportSlikPermata> list = new ArrayList<Jfs2ExportSlikPermata>();
        list.add(new Jfs2ExportSlikPermata());
        for (Integer row = 1; row <= rows; row++) {
            Jfs2ExportSlikPermata model = new Jfs2ExportSlikPermata();
            model.setNO_PIN("3700250883");
            model.setFINANCING_CREDIT_CODE("9");
            model.setTYPE_OF_CREDIT("10");
            model.setSKIM_CODE("00");
            model.setNEW("0");
            model.setTYPE_OF_USE_CODE_("3");
            model.setORIENTATION_OF_USE_CODE("3");
            model.setTAKE_OVER_FROM("");
            model.setEMAIL_ADDRESS("");
            model.setMOBILE_PHONE_NUMBER("082233956153");
            model.setWORKING_ADDRESS("Jl Kepuharjo, RTXNA, RWXNA");
            model.setGROSS_INCOME_PER_YEAR("26400000");
            model.setSOURCE_OF_INCOME_("3");
            model.setNUMBER_OF_DEPENDENTS("1");
            model.setSPOUSE_NAME("JANE");
            model.setSPOUSE_ID("3573020603840002");
            model.setSPOUSE_DATE_OF_BIRTH("06/03/1994");
            model.setPERJANJIAN_PISAH_HARTA("T");
            model.setKODE_BIDANG_USAHA("111010");
            model.setBUSINESS_ENTITY_CODE("");
            model.setDEBTOR_GROUP_CODE("9000");
            model.setGO_PUBLIC("");
            model.setDEBTOR_RATING("");
            model.setRATING_AGENCIES("");
            model.setDATE_OF_RATING("");
            model.setDEBTOR_GROUP_NAME("");
            model.setTYPES_OF_COLLATERAL_CODE("");
            model.setTYPE_OF_BONDING_CODE("");
            model.setBINDING_DATE("");
            model.setCOLLATERAL_NAME("");
            model.setCOLLATERAL_ADDRESS("");
            model.setJOINT_CREDIT_STATUS("");
            model.setREGENCY_CODE("1293");
            model.setINSURANCE("T");
            list.add(model);
        }
        return list;
    }

    @Override
    public List<Jfs2ExportSlikPermata> getJfs2ExporSlikData() {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperSlikPermata());
        return dummyData(5);
    }

    @Override
    public List<Jfs2ExportSlikPermata> getJfs2ExporSlikData(String runDate) {
        // StringBuilder sb = new StringBuilder(3000);
        // return jdbcTemplateObject.query(sb.toString(),
        // new Jfs2ExportMapperSlikPermata());
        return dummyData(5);
    }

}
