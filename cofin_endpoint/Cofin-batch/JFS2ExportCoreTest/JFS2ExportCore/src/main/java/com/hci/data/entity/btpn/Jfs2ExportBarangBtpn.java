package com.hci.data.entity.btpn;

public class Jfs2ExportBarangBtpn {
	private String contractCode;
	private String name;
	private String engineNumber;
	private String modelSerialNumber;
	private String btpnCodeCommodityCategory;
	private String btpnCodeCommodityType;
	private String sisdate;
	private String commodity;
	private String producer;
	private String agreementId;
	private String partnerId;
	
	
	public String getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getContractCode() {
		return contractCode;
	}
	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEngineNumber() {
		return engineNumber;
	}
	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}
	public String getModelSerialNumber() {
		return modelSerialNumber;
	}
	public void setModelSerialNumber(String modelSerialNumber) {
		this.modelSerialNumber = modelSerialNumber;
	}
	public String getBtpnCodeCommodityCategory() {
		return btpnCodeCommodityCategory;
	}
	public void setBtpnCodeCommodityCategory(String btpnCodeCommodityCategory) {
		this.btpnCodeCommodityCategory = btpnCodeCommodityCategory;
	}
	public String getBtpnCodeCommodityType() {
		return btpnCodeCommodityType;
	}
	public void setBtpnCodeCommodityType(String btpnCodeCommodityType) {
		this.btpnCodeCommodityType = btpnCodeCommodityType;
	}
	public String getSisdate() {
		return sisdate;
	}
	public void setSisdate(String sisdate) {
		this.sisdate = sisdate;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	public String getProducer() {
		return producer;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
}
