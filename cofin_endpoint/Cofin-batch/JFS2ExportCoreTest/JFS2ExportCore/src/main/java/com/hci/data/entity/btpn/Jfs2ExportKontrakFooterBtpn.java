package com.hci.data.entity.btpn;

public class Jfs2ExportKontrakFooterBtpn {
	private String numOfRows;
	private String creditAmount;
	
	public String getNumOfRows() {
		return numOfRows;
	}
	public void setNumOfRows(String numOfRows) {
		this.numOfRows = numOfRows;
	}
	public String getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}
}
