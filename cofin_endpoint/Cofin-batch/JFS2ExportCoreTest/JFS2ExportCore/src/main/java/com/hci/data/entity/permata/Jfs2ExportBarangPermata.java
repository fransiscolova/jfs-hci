/**
 *
 */
package com.hci.data.entity.permata;

import com.hci.data.helper.permata.BarangPermata;
import com.hci.util.OtherUtil;

/**
 * Entity to cater query result for barang Permata.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportBarangPermata {
    private String NO_PIN;
    private String CAR_REGISTRATION_NO;
    private String CAR_REGISTRATION_DATE;
    private String NAME_ON_CAR_REGISTRATION;
    private String PLACE_OF_CAR_REGISTRATION;
    private String ENGINE_NUMBER;
    private String CHASIS_NUMBER;
    private String LISENCE_PLATE_NUMBER;
    private String BRAND;
    private String TYPE;
    private String CONDITION_OF_GOODS;
    private String ASSEMBLY_YEAR;
    private String TYPE_PF_GOODS;
    private String TYPE_OF_GOODS;
    private String VALUE_OF_GOODS;

    public String getASSEMBLY_YEAR() {
        return ASSEMBLY_YEAR;
    }

    public String getBRAND() {
        return BRAND;
    }

    public String getCAR_REGISTRATION_DATE() {
        return CAR_REGISTRATION_DATE;
    }

    public String getCAR_REGISTRATION_NO() {
        return CAR_REGISTRATION_NO;
    }

    public String getCHASIS_NUMBER() {
        return CHASIS_NUMBER;
    }

    public String getCONDITION_OF_GOODS() {
        return CONDITION_OF_GOODS;
    }

    public String getENGINE_NUMBER() {
        return ENGINE_NUMBER;
    }

    public String getLISENCE_PLATE_NUMBER() {
        return LISENCE_PLATE_NUMBER;
    }

    public String getNAME_ON_CAR_REGISTRATION() {
        return NAME_ON_CAR_REGISTRATION;
    }

    public String getNO_PIN() {
        return NO_PIN;
    }

    public String getPLACE_OF_CAR_REGISTRATION() {
        return PLACE_OF_CAR_REGISTRATION;
    }

    public String getTYPE() {
        return TYPE;
    }

    public String getTYPE_OF_GOODS() {
        return TYPE_OF_GOODS;
    }

    public String getTYPE_PF_GOODS() {
        return TYPE_PF_GOODS;
    }

    public String getVALUE_OF_GOODS() {
        return VALUE_OF_GOODS;
    }

    public void setASSEMBLY_YEAR(String aSSEMBLY_YEAR) {
        ASSEMBLY_YEAR = aSSEMBLY_YEAR;
    }

    public void setBRAND(String bRAND) {
        BRAND = bRAND;
    }

    public void setCAR_REGISTRATION_DATE(String cAR_REGISTRATION_DATE) {
        CAR_REGISTRATION_DATE = cAR_REGISTRATION_DATE;
    }

    public void setCAR_REGISTRATION_NO(String cAR_REGISTRATION_NO) {
        CAR_REGISTRATION_NO = cAR_REGISTRATION_NO;
    }

    public void setCHASIS_NUMBER(String cHASIS_NUMBER) {
        CHASIS_NUMBER = cHASIS_NUMBER;
    }

    public void setCONDITION_OF_GOODS(String cONDITION_OF_GOODS) {
        CONDITION_OF_GOODS = cONDITION_OF_GOODS;
    }

    public void setENGINE_NUMBER(String eNGINE_NUMBER) {
        ENGINE_NUMBER = eNGINE_NUMBER;
    }

    public void setLISENCE_PLATE_NUMBER(String lISENCE_PLATE_NUMBER) {
        LISENCE_PLATE_NUMBER = lISENCE_PLATE_NUMBER;
    }

    public void setNAME_ON_CAR_REGISTRATION(String nAME_ON_CAR_REGISTRATION) {
        NAME_ON_CAR_REGISTRATION = nAME_ON_CAR_REGISTRATION;
    }

    public void setNO_PIN(String nO_PIN) {
        NO_PIN = nO_PIN;
    }

    public void setPLACE_OF_CAR_REGISTRATION(String pLACE_OF_CAR_REGISTRATION) {
        PLACE_OF_CAR_REGISTRATION = pLACE_OF_CAR_REGISTRATION;
    }

    public void setTYPE(String tYPE) {
        TYPE = tYPE;
    }

    public void setTYPE_OF_GOODS(String tYPE_OF_GOODS) {
        TYPE_OF_GOODS = tYPE_OF_GOODS;
    }

    public void setTYPE_PF_GOODS(String tYPE_PF_GOODS) {
        TYPE_PF_GOODS = tYPE_PF_GOODS;
    }

    public void setVALUE_OF_GOODS(String vALUE_OF_GOODS) {
        VALUE_OF_GOODS = vALUE_OF_GOODS;
    }

    /**
     * Return data from this entity into one line of string using 
     * fixed length for each field defined in {@link BarangPermata}.
     */
    @Override
    public String toString() {
        OtherUtil util = new OtherUtil();
        StringBuilder sb = new StringBuilder(287);
        sb.append(util.rightPaddingSpace(NO_PIN, BarangPermata.NO_PIN.getMaxLength()));
        sb.append(util.rightPaddingSpace(CAR_REGISTRATION_NO, BarangPermata.CAR_REGISTRATION_NO.getMaxLength()));
        sb.append(util.rightPaddingSpace(CAR_REGISTRATION_DATE, BarangPermata.CAR_REGISTRATION_DATE.getMaxLength()));
        sb.append(util.rightPaddingSpace(NAME_ON_CAR_REGISTRATION, BarangPermata.NAME_ON_CAR_REGISTRATION.getMaxLength()));
        sb.append(util.rightPaddingSpace(PLACE_OF_CAR_REGISTRATION, BarangPermata.PLACE_OF_CAR_REGISTRATION.getMaxLength()));
        sb.append(util.rightPaddingSpace(ENGINE_NUMBER, BarangPermata.ENGINE_NUMBER.getMaxLength()));
        sb.append(util.rightPaddingSpace(CHASIS_NUMBER, BarangPermata.CHASIS_NUMBER.getMaxLength()));
        sb.append(util.rightPaddingSpace(LISENCE_PLATE_NUMBER, BarangPermata.LISENCE_PLATE_NUMBER.getMaxLength()));
        sb.append(util.rightPaddingSpace(BRAND, BarangPermata.BRAND.getMaxLength()));
        sb.append(util.rightPaddingSpace(TYPE, BarangPermata.TYPE.getMaxLength()));
        sb.append(util.rightPaddingSpace(CONDITION_OF_GOODS, BarangPermata.CONDITION_OF_GOODS.getMaxLength()));
        sb.append(util.rightPaddingSpace(ASSEMBLY_YEAR, BarangPermata.ASSEMBLY_YEAR.getMaxLength()));
        sb.append(util.rightPaddingSpace(TYPE_PF_GOODS, BarangPermata.TYPE_PF_GOODS.getMaxLength()));
        sb.append(util.rightPaddingSpace(TYPE_OF_GOODS, BarangPermata.TYPE_OF_GOODS.getMaxLength()));
        sb.append(util.rightPaddingSpace(VALUE_OF_GOODS, BarangPermata.VALUE_OF_GOODS.getMaxLength()));
        return sb.toString();
    }

}
