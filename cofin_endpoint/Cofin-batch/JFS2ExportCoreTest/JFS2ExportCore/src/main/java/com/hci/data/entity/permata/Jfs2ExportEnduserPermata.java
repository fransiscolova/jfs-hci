/**
 *
 */
package com.hci.data.entity.permata;

import com.hci.data.helper.permata.EnduserPermata;
import com.hci.util.OtherUtil;

/**
 * Entity to cater query result for end user Permata.
 * 
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportEnduserPermata {
    private String NO_PIN;
    private String END_USER_NAME;
    private String ALIAS;
    private String STATUS;
    private String STATUS_DESCRIPTION;
    private String DEBTOR_CLASS;
    private String SEX;
    private String ID_CARD_NUMBER;
    private String PASSPORT_NO;
    private String BIRTH_CERTIFICATE_NO;
    private String PLACE_OF_BIRTH;
    private String DATE_OF_BIRTH;
    private String DATE_OF_FINAL_DEED;
    private String TAX_REGISTRATION;
    private String DEBTORS_ADDRESS;
    private String SUB_DISTRICT;
    private String KECAMATAN;
    private String DISTRICT_OF_DEBTOR;
    private String POSTAL_CODE;
    private String AREA_CODE;
    private String TELEPHONE;
    private String COUNTRY_OF_DOMICILE;
    private String MAIDEN_NAME_OF_BIOLOGICAL_MOTHER;
    private String OCCUPATION_CODE;
    private String PLACE_OF_WORK;
    private String FIELD_OF_BUSINESS;
    private String ID_TYPE;
    private String MARITAL_STATUS;
    private String KEWARGANEGARAAN;
    private String RELIGION;
    private String INCOME;
    private String ID_EXPIRED;

    public String getALIAS() {
        return ALIAS;
    }

    public String getAREA_CODE() {
        return AREA_CODE;
    }

    public String getBIRTH_CERTIFICATE_NO() {
        return BIRTH_CERTIFICATE_NO;
    }

    public String getCOUNTRY_OF_DOMICILE() {
        return COUNTRY_OF_DOMICILE;
    }

    public String getDATE_OF_BIRTH() {
        return DATE_OF_BIRTH;
    }

    public String getDATE_OF_FINAL_DEED() {
        return DATE_OF_FINAL_DEED;
    }

    public String getDEBTOR_CLASS() {
        return DEBTOR_CLASS;
    }

    public String getDEBTORS_ADDRESS() {
        return DEBTORS_ADDRESS;
    }

    public String getDISTRICT_OF_DEBTOR() {
        return DISTRICT_OF_DEBTOR;
    }

    public String getEND_USER_NAME() {
        return END_USER_NAME;
    }

    public String getFIELD_OF_BUSINESS() {
        return FIELD_OF_BUSINESS;
    }

    public String getID_CARD_NUMBER() {
        return ID_CARD_NUMBER;
    }

    public String getID_EXPIRED() {
        return ID_EXPIRED;
    }

    public String getID_TYPE() {
        return ID_TYPE;
    }

    public String getINCOME() {
        return INCOME;
    }

    public String getKECAMATAN() {
        return KECAMATAN;
    }

    public String getKEWARGANEGARAAN() {
        return KEWARGANEGARAAN;
    }

    public String getMAIDEN_NAME_OF_BIOLOGICAL_MOTHER() {
        return MAIDEN_NAME_OF_BIOLOGICAL_MOTHER;
    }

    public String getMARITAL_STATUS() {
        return MARITAL_STATUS;
    }

    public String getNO_PIN() {
        return NO_PIN;
    }

    public String getOCCUPATION_CODE() {
        return OCCUPATION_CODE;
    }

    public String getPASSPORT_NO() {
        return PASSPORT_NO;
    }

    public String getPLACE_OF_BIRTH() {
        return PLACE_OF_BIRTH;
    }

    public String getPLACE_OF_WORK() {
        return PLACE_OF_WORK;
    }

    public String getPOSTAL_CODE() {
        return POSTAL_CODE;
    }

    public String getRELIGION() {
        return RELIGION;
    }

    public String getSEX() {
        return SEX;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public String getSTATUS_DESCRIPTION() {
        return STATUS_DESCRIPTION;
    }

    public String getSUB_DISTRICT() {
        return SUB_DISTRICT;
    }

    public String getTAX_REGISTRATION() {
        return TAX_REGISTRATION;
    }

    public String getTELEPHONE() {
        return TELEPHONE;
    }

    public void setALIAS(String aLIAS) {
        ALIAS = aLIAS;
    }

    public void setAREA_CODE(String aREA_CODE) {
        AREA_CODE = aREA_CODE;
    }

    public void setBIRTH_CERTIFICATE_NO(String bIRTH_CERTIFICATE_NO) {
        BIRTH_CERTIFICATE_NO = bIRTH_CERTIFICATE_NO;
    }

    public void setCOUNTRY_OF_DOMICILE(String cOUNTRY_OF_DOMICILE) {
        COUNTRY_OF_DOMICILE = cOUNTRY_OF_DOMICILE;
    }

    public void setDATE_OF_BIRTH(String dATE_OF_BIRTH) {
        DATE_OF_BIRTH = dATE_OF_BIRTH;
    }

    public void setDATE_OF_FINAL_DEED(String dATE_OF_FINAL_DEED) {
        DATE_OF_FINAL_DEED = dATE_OF_FINAL_DEED;
    }

    public void setDEBTOR_CLASS(String dEBTOR_CLASS) {
        DEBTOR_CLASS = dEBTOR_CLASS;
    }

    public void setDEBTORS_ADDRESS(String dEBTORS_ADDRESS) {
        DEBTORS_ADDRESS = dEBTORS_ADDRESS;
    }

    public void setDISTRICT_OF_DEBTOR(String dISTRICT_OF_DEBTOR) {
        DISTRICT_OF_DEBTOR = dISTRICT_OF_DEBTOR;
    }

    public void setEND_USER_NAME(String eND_USER_NAME) {
        END_USER_NAME = eND_USER_NAME;
    }

    public void setFIELD_OF_BUSINESS(String fIELD_OF_BUSINESS) {
        FIELD_OF_BUSINESS = fIELD_OF_BUSINESS;
    }

    public void setID_CARD_NUMBER(String iD_CARD_NUMBER) {
        ID_CARD_NUMBER = iD_CARD_NUMBER;
    }

    public void setID_EXPIRED(String iD_EXPIRED) {
        ID_EXPIRED = iD_EXPIRED;
    }

    public void setID_TYPE(String iD_TYPE) {
        ID_TYPE = iD_TYPE;
    }

    public void setINCOME(String iNCOME) {
        INCOME = iNCOME;
    }

    public void setKECAMATAN(String kECAMATAN) {
        KECAMATAN = kECAMATAN;
    }

    public void setKEWARGANEGARAAN(String kEWARGANEGARAAN) {
        KEWARGANEGARAAN = kEWARGANEGARAAN;
    }

    public void setMAIDEN_NAME_OF_BIOLOGICAL_MOTHER(String mAIDEN_NAME_OF_BIOLOGICAL_MOTHER) {
        MAIDEN_NAME_OF_BIOLOGICAL_MOTHER = mAIDEN_NAME_OF_BIOLOGICAL_MOTHER;
    }

    public void setMARITAL_STATUS(String mARITAL_STATUS) {
        MARITAL_STATUS = mARITAL_STATUS;
    }

    public void setNO_PIN(String nO_PIN) {
        NO_PIN = nO_PIN;
    }

    public void setOCCUPATION_CODE(String oCCUPATION_CODE) {
        OCCUPATION_CODE = oCCUPATION_CODE;
    }

    public void setPASSPORT_NO(String pASSPORT_NO) {
        PASSPORT_NO = pASSPORT_NO;
    }

    public void setPLACE_OF_BIRTH(String pLACE_OF_BIRTH) {
        PLACE_OF_BIRTH = pLACE_OF_BIRTH;
    }

    public void setPLACE_OF_WORK(String pLACE_OF_WORK) {
        PLACE_OF_WORK = pLACE_OF_WORK;
    }

    public void setPOSTAL_CODE(String pOSTAL_CODE) {
        POSTAL_CODE = pOSTAL_CODE;
    }

    public void setRELIGION(String rELIGION) {
        RELIGION = rELIGION;
    }

    public void setSEX(String sEX) {
        SEX = sEX;
    }

    public void setSTATUS(String sTATUS) {
        STATUS = sTATUS;
    }

    public void setSTATUS_DESCRIPTION(String sTATUS_DESCRIPTION) {
        STATUS_DESCRIPTION = sTATUS_DESCRIPTION;
    }

    public void setSUB_DISTRICT(String sUB_DISTRICT) {
        SUB_DISTRICT = sUB_DISTRICT;
    }

    public void setTAX_REGISTRATION(String tAX_REGISTRATION) {
        TAX_REGISTRATION = tAX_REGISTRATION;
    }

    public void setTELEPHONE(String tELEPHONE) {
        TELEPHONE = tELEPHONE;
    }

    /**
     * Return data from this entity into one line of string using 
     * fixed length for each field defined in {@link EnduserPermata}.
     */
    @Override
    public String toString() {
        OtherUtil util = new OtherUtil();
        StringBuilder sb = new StringBuilder(719);
        sb.append(util.rightPaddingSpace(NO_PIN, EnduserPermata.NO_PIN.getMaxLength()));
        sb.append(util.rightPaddingSpace(END_USER_NAME, EnduserPermata.END_USER_NAME.getMaxLength()));
        sb.append(util.rightPaddingSpace(ALIAS, EnduserPermata.ALIAS.getMaxLength()));
        sb.append(util.rightPaddingSpace(STATUS, EnduserPermata.STATUS.getMaxLength()));
        sb.append(util.rightPaddingSpace(STATUS_DESCRIPTION, EnduserPermata.STATUS_DESCRIPTION.getMaxLength()));
        sb.append(util.rightPaddingSpace(DEBTOR_CLASS, EnduserPermata.DEBTOR_CLASS.getMaxLength()));
        sb.append(util.rightPaddingSpace(SEX, EnduserPermata.SEX.getMaxLength()));
        sb.append(util.rightPaddingSpace(ID_CARD_NUMBER, EnduserPermata.ID_CARD_NUMBER.getMaxLength()));
        sb.append(util.rightPaddingSpace(PASSPORT_NO, EnduserPermata.PASSPORT_NO.getMaxLength()));
        sb.append(util.rightPaddingSpace(BIRTH_CERTIFICATE_NO, EnduserPermata.BIRTH_CERTIFICATE_NO.getMaxLength()));
        sb.append(util.rightPaddingSpace(PLACE_OF_BIRTH, EnduserPermata.PLACE_OF_BIRTH.getMaxLength()));
        sb.append(util.rightPaddingSpace(DATE_OF_BIRTH, EnduserPermata.DATE_OF_BIRTH.getMaxLength()));
        sb.append(util.rightPaddingSpace(DATE_OF_FINAL_DEED, EnduserPermata.DATE_OF_FINAL_DEED.getMaxLength()));
        sb.append(util.rightPaddingSpace(TAX_REGISTRATION, EnduserPermata.TAX_REGISTRATION.getMaxLength()));
        sb.append(util.rightPaddingSpace(DEBTORS_ADDRESS, EnduserPermata.DEBTORS_ADDRESS.getMaxLength()));
        sb.append(util.rightPaddingSpace(SUB_DISTRICT, EnduserPermata.SUB_DISTRICT.getMaxLength()));
        sb.append(util.rightPaddingSpace(KECAMATAN, EnduserPermata.KECAMATAN.getMaxLength()));
        sb.append(util.rightPaddingSpace(DISTRICT_OF_DEBTOR, EnduserPermata.DISTRICT_OF_DEBTOR.getMaxLength()));
        sb.append(util.rightPaddingSpace(POSTAL_CODE, EnduserPermata.POSTAL_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(AREA_CODE, EnduserPermata.AREA_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(TELEPHONE, EnduserPermata.TELEPHONE.getMaxLength()));
        sb.append(util.rightPaddingSpace(COUNTRY_OF_DOMICILE, EnduserPermata.COUNTRY_OF_DOMICILE.getMaxLength()));
        sb.append(util.rightPaddingSpace(MAIDEN_NAME_OF_BIOLOGICAL_MOTHER, EnduserPermata.MAIDEN_NAME_OF_BIOLOGICAL_MOTHER.getMaxLength()));
        sb.append(util.rightPaddingSpace(OCCUPATION_CODE, EnduserPermata.OCCUPATION_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(PLACE_OF_WORK, EnduserPermata.PLACE_OF_WORK.getMaxLength()));
        sb.append(util.rightPaddingSpace(FIELD_OF_BUSINESS, EnduserPermata.FIELD_OF_BUSINESS.getMaxLength()));
        sb.append(util.rightPaddingSpace(ID_TYPE, EnduserPermata.ID_TYPE.getMaxLength()));
        sb.append(util.rightPaddingSpace(MARITAL_STATUS, EnduserPermata.MARITAL_STATUS.getMaxLength()));
        sb.append(util.rightPaddingSpace(KEWARGANEGARAAN, EnduserPermata.KEWARGANEGARAAN.getMaxLength()));
        sb.append(util.rightPaddingSpace(RELIGION, EnduserPermata.RELIGION.getMaxLength()));
        sb.append(util.rightPaddingSpace(INCOME, EnduserPermata.INCOME.getMaxLength()));
        sb.append(util.rightPaddingSpace(ID_EXPIRED, EnduserPermata.ID_EXPIRED.getMaxLength()));
        return sb.toString();
    }

    
}
