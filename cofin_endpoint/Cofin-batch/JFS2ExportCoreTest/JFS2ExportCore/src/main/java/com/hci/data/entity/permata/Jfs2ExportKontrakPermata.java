/**
 *
 */
package com.hci.data.entity.permata;

import com.hci.data.helper.permata.KontrakPermata;
import com.hci.util.OtherUtil;


/**
 * Entity to cater query result for kontrak Permata.
 * 
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportKontrakPermata {
    private String NO_PIN;
    private String NAME;
    private String MF_BRANCH_CODE;
    private String NO_PK_ENDURSER;
    private String EFFECTIVE_DATE_END_USER;
    private String INITIAL_PRINCIPAL_AT_MF;
    private String GOODS_VALUE;
    private String MF_INITIAL_TENOR;
    private String AMOUNT_OF_INSTALLMENT_TO_MF;
    private String MF_INTEREST_TO_END_USER;
    private String PRINCIPLE_UPON_ASSIGNMENT;
    private String BANK_FINANCED_PRINCIPAL;
    private String TENOR_WITH_BANK;
    private String INSTALLMENT_PAYMENT_DATE_1;
    private String FINAL_INSTALLMENT_DATE;
    private String INTEREST_AT_THE_BANK;
    private String AMOUNT_OF_INSTALLMENT_TO_BANK;
    private String INSTALLMENT_PERIOD;
    private String VEHICLE_CONDITION;
    private String TYPE_OF_CLOSING;
    private String INSURANCE_PREMIUM_AMOUNT;
    private String PREMIUM_PAYMENT;
    private String VEHICLE_CLASS;
    private String CREDIT_SCORING;
    private String DEBT_BURDEN_RATIO;
    private String END_USER_LOAN_AGREEMENT_DATE;
    private String AMOUNT_PREMIUM_PAID;
    private String AMOUNT_DP;
    private String INSURANCE_COMPANY;
    private String AREA_OF_BUSINESS;
    private String PURPOSE_OF_USAGE;
    private String DOCUMENT_PROOF_OF_INCOME;
    private String PBB;
    private String SPOUSE_KTP;
    private String LICENCING;
    private String DEED;
    private String NPWP;
    private String FINANCING_APPLICATION_FORM;
    private String FIDUCIARY;
    private String CODE_IN_REAR;

    public String getAMOUNT_DP() {
        return AMOUNT_DP;
    }

    public String getAMOUNT_OF_INSTALLMENT_TO_BANK() {
        return AMOUNT_OF_INSTALLMENT_TO_BANK;
    }

    public String getAMOUNT_OF_INSTALLMENT_TO_MF() {
        return AMOUNT_OF_INSTALLMENT_TO_MF;
    }

    public String getAMOUNT_PREMIUM_PAID() {
        return AMOUNT_PREMIUM_PAID;
    }

    public String getAREA_OF_BUSINESS() {
        return AREA_OF_BUSINESS;
    }

    public String getBANK_FINANCED_PRINCIPAL() {
        return BANK_FINANCED_PRINCIPAL;
    }

    public String getCODE_IN_REAR() {
        return CODE_IN_REAR;
    }

    public String getCREDIT_SCORING() {
        return CREDIT_SCORING;
    }

    public String getDEBT_BURDEN_RATIO() {
        return DEBT_BURDEN_RATIO;
    }

    public String getDEED() {
        return DEED;
    }

    public String getDOCUMENT_PROOF_OF_INCOME() {
        return DOCUMENT_PROOF_OF_INCOME;
    }

    public String getEFFECTIVE_DATE_END_USER() {
        return EFFECTIVE_DATE_END_USER;
    }

    public String getEND_USER_LOAN_AGREEMENT_DATE() {
        return END_USER_LOAN_AGREEMENT_DATE;
    }

    public String getFIDUCIARY() {
        return FIDUCIARY;
    }

    public String getFINAL_INSTALLMENT_DATE() {
        return FINAL_INSTALLMENT_DATE;
    }

    public String getFINANCING_APPLICATION_FORM() {
        return FINANCING_APPLICATION_FORM;
    }

    public String getGOODS_VALUE() {
        return GOODS_VALUE;
    }

    public String getINITIAL_PRINCIPAL_AT_MF() {
        return INITIAL_PRINCIPAL_AT_MF;
    }

    public String getINSTALLMENT_PAYMENT_DATE_1() {
        return INSTALLMENT_PAYMENT_DATE_1;
    }

    public String getINSTALLMENT_PERIOD() {
        return INSTALLMENT_PERIOD;
    }

    public String getINSURANCE_COMPANY() {
        return INSURANCE_COMPANY;
    }

    public String getINSURANCE_PREMIUM_AMOUNT() {
        return INSURANCE_PREMIUM_AMOUNT;
    }

    public String getINTEREST_AT_THE_BANK() {
        return INTEREST_AT_THE_BANK;
    }

    public String getLICENCING() {
        return LICENCING;
    }

    public String getMF_BRANCH_CODE() {
        return MF_BRANCH_CODE;
    }

    public String getMF_INITIAL_TENOR() {
        return MF_INITIAL_TENOR;
    }

    public String getMF_INTEREST_TO_END_USER() {
        return MF_INTEREST_TO_END_USER;
    }

    public String getNAME() {
        return NAME;
    }

    public String getNO_PIN() {
        return NO_PIN;
    }

    public String getNO_PK_ENDURSER() {
        return NO_PK_ENDURSER;
    }

    public String getNPWP() {
        return NPWP;
    }

    public String getPBB() {
        return PBB;
    }

    public String getPREMIUM_PAYMENT() {
        return PREMIUM_PAYMENT;
    }

    public String getPRINCIPLE_UPON_ASSIGNMENT() {
        return PRINCIPLE_UPON_ASSIGNMENT;
    }

    public String getPURPOSE_OF_USAGE() {
        return PURPOSE_OF_USAGE;
    }

    public String getSPOUSE_KTP() {
        return SPOUSE_KTP;
    }

    public String getTENOR_WITH_BANK() {
        return TENOR_WITH_BANK;
    }

    public String getTYPE_OF_CLOSING() {
        return TYPE_OF_CLOSING;
    }

    public String getVEHICLE_CLASS() {
        return VEHICLE_CLASS;
    }

    public String getVEHICLE_CONDITION() {
        return VEHICLE_CONDITION;
    }

    public void setAMOUNT_DP(String aMOUNT_DP) {
        AMOUNT_DP = aMOUNT_DP;
    }

    public void setAMOUNT_OF_INSTALLMENT_TO_BANK(String aMOUNT_OF_INSTALLMENT_TO_BANK) {
        AMOUNT_OF_INSTALLMENT_TO_BANK = aMOUNT_OF_INSTALLMENT_TO_BANK;
    }

    public void setAMOUNT_OF_INSTALLMENT_TO_MF(String aMOUNT_OF_INSTALLMENT_TO_MF) {
        AMOUNT_OF_INSTALLMENT_TO_MF = aMOUNT_OF_INSTALLMENT_TO_MF;
    }

    public void setAMOUNT_PREMIUM_PAID(String aMOUNT_PREMIUM_PAID) {
        AMOUNT_PREMIUM_PAID = aMOUNT_PREMIUM_PAID;
    }

    public void setAREA_OF_BUSINESS(String aREA_OF_BUSINESS) {
        AREA_OF_BUSINESS = aREA_OF_BUSINESS;
    }

    public void setBANK_FINANCED_PRINCIPAL(String bANK_FINANCED_PRINCIPAL) {
        BANK_FINANCED_PRINCIPAL = bANK_FINANCED_PRINCIPAL;
    }

    public void setCODE_IN_REAR(String cODE_IN_REAR) {
        CODE_IN_REAR = cODE_IN_REAR;
    }

    public void setCREDIT_SCORING(String cREDIT_SCORING) {
        CREDIT_SCORING = cREDIT_SCORING;
    }

    public void setDEBT_BURDEN_RATIO(String dEBT_BURDEN_RATIO) {
        DEBT_BURDEN_RATIO = dEBT_BURDEN_RATIO;
    }

    public void setDEED(String dEED) {
        DEED = dEED;
    }

    public void setDOCUMENT_PROOF_OF_INCOME(String dOCUMENT_PROOF_OF_INCOME) {
        DOCUMENT_PROOF_OF_INCOME = dOCUMENT_PROOF_OF_INCOME;
    }

    public void setEFFECTIVE_DATE_END_USER(String eFFECTIVE_DATE_END_USER) {
        EFFECTIVE_DATE_END_USER = eFFECTIVE_DATE_END_USER;
    }

    public void setEND_USER_LOAN_AGREEMENT_DATE(String eND_USER_LOAN_AGREEMENT_DATE) {
        END_USER_LOAN_AGREEMENT_DATE = eND_USER_LOAN_AGREEMENT_DATE;
    }

    public void setFIDUCIARY(String fIDUCIARY) {
        FIDUCIARY = fIDUCIARY;
    }

    public void setFINAL_INSTALLMENT_DATE(String fINAL_INSTALLMENT_DATE) {
        FINAL_INSTALLMENT_DATE = fINAL_INSTALLMENT_DATE;
    }

    public void setFINANCING_APPLICATION_FORM(String fINANCING_APPLICATION_FORM) {
        FINANCING_APPLICATION_FORM = fINANCING_APPLICATION_FORM;
    }

    public void setGOODS_VALUE(String gOODS_VALUE) {
        GOODS_VALUE = gOODS_VALUE;
    }

    public void setINITIAL_PRINCIPAL_AT_MF(String iNITIAL_PRINCIPAL_AT_MF) {
        INITIAL_PRINCIPAL_AT_MF = iNITIAL_PRINCIPAL_AT_MF;
    }

    public void setINSTALLMENT_PAYMENT_DATE_1(String iNSTALLMENT_PAYMENT_DATE_1) {
        INSTALLMENT_PAYMENT_DATE_1 = iNSTALLMENT_PAYMENT_DATE_1;
    }

    public void setINSTALLMENT_PERIOD(String iNSTALLMENT_PERIOD) {
        INSTALLMENT_PERIOD = iNSTALLMENT_PERIOD;
    }

    public void setINSURANCE_COMPANY(String iNSURANCE_COMPANY) {
        INSURANCE_COMPANY = iNSURANCE_COMPANY;
    }

    public void setINSURANCE_PREMIUM_AMOUNT(String iNSURANCE_PREMIUM_AMOUNT) {
        INSURANCE_PREMIUM_AMOUNT = iNSURANCE_PREMIUM_AMOUNT;
    }

    public void setINTEREST_AT_THE_BANK(String iNTEREST_AT_THE_BANK) {
        INTEREST_AT_THE_BANK = iNTEREST_AT_THE_BANK;
    }

    public void setLICENCING(String lICENCING) {
        LICENCING = lICENCING;
    }

    public void setMF_BRANCH_CODE(String mF_BRANCH_CODE) {
        MF_BRANCH_CODE = mF_BRANCH_CODE;
    }

    public void setMF_INITIAL_TENOR(String mF_INITIAL_TENOR) {
        MF_INITIAL_TENOR = mF_INITIAL_TENOR;
    }

    public void setMF_INTEREST_TO_END_USER(String mF_INTEREST_TO_END_USER) {
        MF_INTEREST_TO_END_USER = mF_INTEREST_TO_END_USER;
    }

    public void setNAME(String nAME) {
        NAME = nAME;
    }

    public void setNO_PIN(String nO_PIN) {
        NO_PIN = nO_PIN;
    }

    public void setNO_PK_ENDURSER(String nO_PK_ENDURSER) {
        NO_PK_ENDURSER = nO_PK_ENDURSER;
    }

    public void setNPWP(String nPWP) {
        NPWP = nPWP;
    }

    public void setPBB(String pBB) {
        PBB = pBB;
    }

    public void setPREMIUM_PAYMENT(String pREMIUM_PAYMENT) {
        PREMIUM_PAYMENT = pREMIUM_PAYMENT;
    }

    public void setPRINCIPLE_UPON_ASSIGNMENT(String pRINCIPLE_UPON_ASSIGNMENT) {
        PRINCIPLE_UPON_ASSIGNMENT = pRINCIPLE_UPON_ASSIGNMENT;
    }

    public void setPURPOSE_OF_USAGE(String pURPOSE_OF_USAGE) {
        PURPOSE_OF_USAGE = pURPOSE_OF_USAGE;
    }

    public void setSPOUSE_KTP(String sPOUSE_KTP) {
        SPOUSE_KTP = sPOUSE_KTP;
    }

    public void setTENOR_WITH_BANK(String tENOR_WITH_BANK) {
        TENOR_WITH_BANK = tENOR_WITH_BANK;
    }

    public void setTYPE_OF_CLOSING(String tYPE_OF_CLOSING) {
        TYPE_OF_CLOSING = tYPE_OF_CLOSING;
    }

    public void setVEHICLE_CLASS(String vEHICLE_CLASS) {
        VEHICLE_CLASS = vEHICLE_CLASS;
    }

    public void setVEHICLE_CONDITION(String vEHICLE_CONDITION) {
        VEHICLE_CONDITION = vEHICLE_CONDITION;
    }

    /**
     * Return data from this entity into one line of string using 
     * fixed length for each field defined in {@link KontrakPermata}. 
     */
    @Override
    public String toString() {
        OtherUtil util = new OtherUtil();
        StringBuilder sb = new StringBuilder(305);
        sb.append(util.rightPaddingSpace(NO_PIN, KontrakPermata.NO_PIN.getMaxLength()));
        sb.append(util.rightPaddingSpace(NAME, KontrakPermata.NAME.getMaxLength()));
        sb.append(util.rightPaddingSpace(MF_BRANCH_CODE, KontrakPermata.MF_BRANCH_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(NO_PK_ENDURSER, KontrakPermata.NO_PK_ENDURSER.getMaxLength()));
        sb.append(util.rightPaddingSpace(EFFECTIVE_DATE_END_USER, KontrakPermata.EFFECTIVE_DATE_END_USER.getMaxLength()));
        sb.append(util.rightPaddingSpace(INITIAL_PRINCIPAL_AT_MF, KontrakPermata.INITIAL_PRINCIPAL_AT_MF.getMaxLength()));
        sb.append(util.rightPaddingSpace(GOODS_VALUE, KontrakPermata.GOODS_VALUE.getMaxLength()));
        sb.append(util.rightPaddingSpace(MF_INITIAL_TENOR, KontrakPermata.MF_INITIAL_TENOR.getMaxLength()));
        sb.append(util.rightPaddingSpace(AMOUNT_OF_INSTALLMENT_TO_MF, KontrakPermata.AMOUNT_OF_INSTALLMENT_TO_MF.getMaxLength()));
        sb.append(util.rightPaddingSpace(MF_INTEREST_TO_END_USER, KontrakPermata.MF_INTEREST_TO_END_USER.getMaxLength()));
        sb.append(util.rightPaddingSpace(PRINCIPLE_UPON_ASSIGNMENT, KontrakPermata.PRINCIPLE_UPON_ASSIGNMENT.getMaxLength()));
        sb.append(util.rightPaddingSpace(BANK_FINANCED_PRINCIPAL, KontrakPermata.BANK_FINANCED_PRINCIPAL.getMaxLength()));
        sb.append(util.rightPaddingSpace(TENOR_WITH_BANK, KontrakPermata.TENOR_WITH_BANK.getMaxLength()));
        sb.append(util.rightPaddingSpace(INSTALLMENT_PAYMENT_DATE_1, KontrakPermata.INSTALLMENT_PAYMENT_DATE_1.getMaxLength()));
        sb.append(util.rightPaddingSpace(FINAL_INSTALLMENT_DATE, KontrakPermata.FINAL_INSTALLMENT_DATE.getMaxLength()));
        sb.append(util.rightPaddingSpace(INTEREST_AT_THE_BANK, KontrakPermata.INTEREST_AT_THE_BANK.getMaxLength()));
        sb.append(util.rightPaddingSpace(AMOUNT_OF_INSTALLMENT_TO_BANK, KontrakPermata.AMOUNT_OF_INSTALLMENT_TO_BANK.getMaxLength()));
        sb.append(util.rightPaddingSpace(INSTALLMENT_PERIOD, KontrakPermata.INSTALLMENT_PERIOD.getMaxLength()));
        sb.append(util.rightPaddingSpace(VEHICLE_CONDITION, KontrakPermata.VEHICLE_CONDITION.getMaxLength()));
        sb.append(util.rightPaddingSpace(TYPE_OF_CLOSING, KontrakPermata.TYPE_OF_CLOSING.getMaxLength()));
        sb.append(util.rightPaddingSpace(INSURANCE_PREMIUM_AMOUNT, KontrakPermata.INSURANCE_PREMIUM_AMOUNT.getMaxLength()));
        sb.append(util.rightPaddingSpace(PREMIUM_PAYMENT, KontrakPermata.PREMIUM_PAYMENT.getMaxLength()));
        sb.append(util.rightPaddingSpace(VEHICLE_CLASS, KontrakPermata.VEHICLE_CLASS.getMaxLength()));
        sb.append(util.rightPaddingSpace(CREDIT_SCORING, KontrakPermata.CREDIT_SCORING.getMaxLength()));
        sb.append(util.rightPaddingSpace(DEBT_BURDEN_RATIO, KontrakPermata.DEBT_BURDEN_RATIO.getMaxLength()));
        sb.append(util.rightPaddingSpace(END_USER_LOAN_AGREEMENT_DATE, KontrakPermata.END_USER_LOAN_AGREEMENT_DATE.getMaxLength()));
        sb.append(util.rightPaddingSpace(AMOUNT_PREMIUM_PAID, KontrakPermata.AMOUNT_PREMIUM_PAID.getMaxLength()));
        sb.append(util.rightPaddingSpace(AMOUNT_DP, KontrakPermata.AMOUNT_DP.getMaxLength()));
        sb.append(util.rightPaddingSpace(INSURANCE_COMPANY, KontrakPermata.INSURANCE_COMPANY.getMaxLength()));
        sb.append(util.rightPaddingSpace(AREA_OF_BUSINESS, KontrakPermata.AREA_OF_BUSINESS.getMaxLength()));
        sb.append(util.rightPaddingSpace(PURPOSE_OF_USAGE, KontrakPermata.PURPOSE_OF_USAGE.getMaxLength()));
        sb.append(util.rightPaddingSpace(DOCUMENT_PROOF_OF_INCOME, KontrakPermata.DOCUMENT_PROOF_OF_INCOME.getMaxLength()));
        sb.append(util.rightPaddingSpace(PBB, KontrakPermata.PBB.getMaxLength()));
        sb.append(util.rightPaddingSpace(SPOUSE_KTP, KontrakPermata.SPOUSE_KTP.getMaxLength()));
        sb.append(util.rightPaddingSpace(LICENCING, KontrakPermata.LICENCING.getMaxLength()));
        sb.append(util.rightPaddingSpace(DEED, KontrakPermata.DEED.getMaxLength()));
        sb.append(util.rightPaddingSpace(NPWP, KontrakPermata.NPWP.getMaxLength()));
        sb.append(util.rightPaddingSpace(FINANCING_APPLICATION_FORM, KontrakPermata.FINANCING_APPLICATION_FORM.getMaxLength()));
        sb.append(util.rightPaddingSpace(FIDUCIARY, KontrakPermata.FIDUCIARY.getMaxLength()));
        sb.append(util.rightPaddingSpace(CODE_IN_REAR, KontrakPermata.CODE_IN_REAR.getMaxLength()));
        return sb.toString();
    }

}
