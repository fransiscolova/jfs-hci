/**
 *
 */
package com.hci.data.entity.permata;

import com.hci.data.helper.permata.PengurusPermata;
import com.hci.util.OtherUtil;

/**
 * Entity to cater query result for pengurus Permata.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportPengurusPermata {
    private String NO_PIN;
    private String NAME_OF_MANAGER;
    private String ID_CARD_OF_MANAGER;
    private String MANAGERS_TAX_REGISTRATION_NO;
    private String SEX;
    private String OWNERSHIP_PORTION;
    private String POSITION_CODE;
    private String ADDRESS;
    private String SUB_DISTRICT;
    private String KECAMATAN;
    private String DISTRICT_OF_DEBTOR;
    private String POSTAL_CODE;

    public String getADDRESS() {
        return ADDRESS;
    }

    public String getDISTRICT_OF_DEBTOR() {
        return DISTRICT_OF_DEBTOR;
    }

    public String getID_CARD_OF_MANAGER() {
        return ID_CARD_OF_MANAGER;
    }

    public String getKECAMATAN() {
        return KECAMATAN;
    }

    public String getMANAGERS_TAX_REGISTRATION_NO() {
        return MANAGERS_TAX_REGISTRATION_NO;
    }

    public String getNAME_OF_MANAGER() {
        return NAME_OF_MANAGER;
    }

    public String getNO_PIN() {
        return NO_PIN;
    }

    public String getOWNERSHIP_PORTION() {
        return OWNERSHIP_PORTION;
    }

    public String getPOSITION_CODE() {
        return POSITION_CODE;
    }

    public String getPOSTAL_CODE() {
        return POSTAL_CODE;
    }

    public String getSEX() {
        return SEX;
    }

    public String getSUB_DISTRICT() {
        return SUB_DISTRICT;
    }

    public void setADDRESS(String aDDRESS) {
        ADDRESS = aDDRESS;
    }

    public void setDISTRICT_OF_DEBTOR(String dISTRICT_OF_DEBTOR) {
        DISTRICT_OF_DEBTOR = dISTRICT_OF_DEBTOR;
    }

    public void setID_CARD_OF_MANAGER(String iD_CARD_OF_MANAGER) {
        ID_CARD_OF_MANAGER = iD_CARD_OF_MANAGER;
    }

    public void setKECAMATAN(String kECAMATAN) {
        KECAMATAN = kECAMATAN;
    }

    public void setMANAGERS_TAX_REGISTRATION_NO(String mANAGERS_TAX_REGISTRATION_NO) {
        MANAGERS_TAX_REGISTRATION_NO = mANAGERS_TAX_REGISTRATION_NO;
    }

    public void setNAME_OF_MANAGER(String nAME_OF_MANAGER) {
        NAME_OF_MANAGER = nAME_OF_MANAGER;
    }

    public void setNO_PIN(String nO_PIN) {
        NO_PIN = nO_PIN;
    }

    public void setOWNERSHIP_PORTION(String oWNERSHIP_PORTION) {
        OWNERSHIP_PORTION = oWNERSHIP_PORTION;
    }

    public void setPOSITION_CODE(String pOSITION_CODE) {
        POSITION_CODE = pOSITION_CODE;
    }

    public void setPOSTAL_CODE(String pOSTAL_CODE) {
        POSTAL_CODE = pOSTAL_CODE;
    }

    public void setSEX(String sEX) {
        SEX = sEX;
    }

    public void setSUB_DISTRICT(String sUB_DISTRICT) {
        SUB_DISTRICT = sUB_DISTRICT;
    }

    /**
     * Return data from this entity into one line of string using 
     * fixed length for each field defined in {@link PengurusPermata}. 
     */
    @Override
    public String toString() {
        OtherUtil util = new OtherUtil();
        StringBuilder sb = new StringBuilder(433);
        sb.append(util.rightPaddingSpace(NO_PIN, PengurusPermata.NO_PIN.getMaxLength()));
        sb.append(util.rightPaddingSpace(NAME_OF_MANAGER, PengurusPermata.NAME_OF_MANAGER.getMaxLength()));
        sb.append(util.rightPaddingSpace(ID_CARD_OF_MANAGER, PengurusPermata.ID_CARD_OF_MANAGER.getMaxLength()));
        sb.append(util.rightPaddingSpace(MANAGERS_TAX_REGISTRATION_NO, PengurusPermata.MANAGERS_TAX_REGISTRATION_NO.getMaxLength()));
        sb.append(util.rightPaddingSpace(SEX, PengurusPermata.SEX.getMaxLength()));
        sb.append(util.rightPaddingSpace(OWNERSHIP_PORTION, PengurusPermata.OWNERSHIP_PORTION.getMaxLength()));
        sb.append(util.rightPaddingSpace(POSITION_CODE, PengurusPermata.POSITION_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(ADDRESS, PengurusPermata.ADDRESS.getMaxLength()));
        sb.append(util.rightPaddingSpace(SUB_DISTRICT, PengurusPermata.SUB_DISTRICT.getMaxLength()));
        sb.append(util.rightPaddingSpace(KECAMATAN, PengurusPermata.KECAMATAN.getMaxLength()));
        sb.append(util.rightPaddingSpace(DISTRICT_OF_DEBTOR, PengurusPermata.DISTRICT_OF_DEBTOR.getMaxLength()));
        sb.append(util.rightPaddingSpace(POSTAL_CODE, PengurusPermata.POSTAL_CODE.getMaxLength()));
        return sb.toString();
    }

}
