/**
 *
 */
package com.hci.data.entity.permata;

import com.hci.data.helper.permata.SlikPermata;
import com.hci.util.OtherUtil;

/**
 * Entity to cater query result for slik Permata.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportSlikPermata {
    private String NO_PIN;
    private String FINANCING_CREDIT_CODE;
    private String TYPE_OF_CREDIT;
    private String SKIM_CODE;
    private String NEW;
    private String TYPE_OF_USE_CODE_;
    private String ORIENTATION_OF_USE_CODE;
    private String TAKE_OVER_FROM;
    private String EMAIL_ADDRESS;
    private String MOBILE_PHONE_NUMBER;
    private String WORKING_ADDRESS;
    private String GROSS_INCOME_PER_YEAR;
    private String SOURCE_OF_INCOME_;
    private String NUMBER_OF_DEPENDENTS;
    private String SPOUSE_NAME;
    private String SPOUSE_ID;
    private String SPOUSE_DATE_OF_BIRTH;
    private String PERJANJIAN_PISAH_HARTA;
    private String KODE_BIDANG_USAHA;
    private String BUSINESS_ENTITY_CODE;
    private String DEBTOR_GROUP_CODE;
    private String GO_PUBLIC;
    private String DEBTOR_RATING;
    private String RATING_AGENCIES;
    private String DATE_OF_RATING;
    private String DEBTOR_GROUP_NAME;
    private String TYPES_OF_COLLATERAL_CODE;
    private String TYPE_OF_BONDING_CODE;
    private String BINDING_DATE;
    private String COLLATERAL_NAME;
    private String COLLATERAL_ADDRESS;
    private String JOINT_CREDIT_STATUS;
    private String REGENCY_CODE;
    private String INSURANCE;

    public String getBINDING_DATE() {
        return BINDING_DATE;
    }

    public String getBUSINESS_ENTITY_CODE() {
        return BUSINESS_ENTITY_CODE;
    }

    public String getCOLLATERAL_ADDRESS() {
        return COLLATERAL_ADDRESS;
    }

    public String getCOLLATERAL_NAME() {
        return COLLATERAL_NAME;
    }

    public String getDATE_OF_RATING() {
        return DATE_OF_RATING;
    }

    public String getDEBTOR_GROUP_CODE() {
        return DEBTOR_GROUP_CODE;
    }

    public String getDEBTOR_GROUP_NAME() {
        return DEBTOR_GROUP_NAME;
    }

    public String getDEBTOR_RATING() {
        return DEBTOR_RATING;
    }

    public String getEMAIL_ADDRESS() {
        return EMAIL_ADDRESS;
    }

    public String getFINANCING_CREDIT_CODE() {
        return FINANCING_CREDIT_CODE;
    }

    public String getGO_PUBLIC() {
        return GO_PUBLIC;
    }

    public String getGROSS_INCOME_PER_YEAR() {
        return GROSS_INCOME_PER_YEAR;
    }

    public String getINSURANCE() {
        return INSURANCE;
    }

    public String getJOINT_CREDIT_STATUS() {
        return JOINT_CREDIT_STATUS;
    }

    public String getKODE_BIDANG_USAHA() {
        return KODE_BIDANG_USAHA;
    }

    public String getMOBILE_PHONE_NUMBER() {
        return MOBILE_PHONE_NUMBER;
    }

    public String getNEW() {
        return NEW;
    }

    public String getNO_PIN() {
        return NO_PIN;
    }

    public String getNUMBER_OF_DEPENDENTS() {
        return NUMBER_OF_DEPENDENTS;
    }

    public String getORIENTATION_OF_USE_CODE() {
        return ORIENTATION_OF_USE_CODE;
    }

    public String getPERJANJIAN_PISAH_HARTA() {
        return PERJANJIAN_PISAH_HARTA;
    }

    public String getRATING_AGENCIES() {
        return RATING_AGENCIES;
    }

    public String getREGENCY_CODE() {
        return REGENCY_CODE;
    }

    public String getSKIM_CODE() {
        return SKIM_CODE;
    }

    public String getSOURCE_OF_INCOME_() {
        return SOURCE_OF_INCOME_;
    }

    public String getSPOUSE_DATE_OF_BIRTH() {
        return SPOUSE_DATE_OF_BIRTH;
    }

    public String getSPOUSE_ID() {
        return SPOUSE_ID;
    }

    public String getSPOUSE_NAME() {
        return SPOUSE_NAME;
    }

    public String getTAKE_OVER_FROM() {
        return TAKE_OVER_FROM;
    }

    public String getTYPE_OF_BONDING_CODE() {
        return TYPE_OF_BONDING_CODE;
    }

    public String getTYPE_OF_CREDIT() {
        return TYPE_OF_CREDIT;
    }

    public String getTYPE_OF_USE_CODE_() {
        return TYPE_OF_USE_CODE_;
    }

    public String getTYPES_OF_COLLATERAL_CODE() {
        return TYPES_OF_COLLATERAL_CODE;
    }

    public String getWORKING_ADDRESS() {
        return WORKING_ADDRESS;
    }

    public void setBINDING_DATE(String bINDING_DATE) {
        BINDING_DATE = bINDING_DATE;
    }

    public void setBUSINESS_ENTITY_CODE(String bUSINESS_ENTITY_CODE) {
        BUSINESS_ENTITY_CODE = bUSINESS_ENTITY_CODE;
    }

    public void setCOLLATERAL_ADDRESS(String cOLLATERAL_ADDRESS) {
        COLLATERAL_ADDRESS = cOLLATERAL_ADDRESS;
    }

    public void setCOLLATERAL_NAME(String cOLLATERAL_NAME) {
        COLLATERAL_NAME = cOLLATERAL_NAME;
    }

    public void setDATE_OF_RATING(String dATE_OF_RATING) {
        DATE_OF_RATING = dATE_OF_RATING;
    }

    public void setDEBTOR_GROUP_CODE(String dEBTOR_GROUP_CODE) {
        DEBTOR_GROUP_CODE = dEBTOR_GROUP_CODE;
    }

    public void setDEBTOR_GROUP_NAME(String dEBTOR_GROUP_NAME) {
        DEBTOR_GROUP_NAME = dEBTOR_GROUP_NAME;
    }

    public void setDEBTOR_RATING(String dEBTOR_RATING) {
        DEBTOR_RATING = dEBTOR_RATING;
    }

    public void setEMAIL_ADDRESS(String eMAIL_ADDRESS) {
        EMAIL_ADDRESS = eMAIL_ADDRESS;
    }

    public void setFINANCING_CREDIT_CODE(String fINANCING_CREDIT_CODE) {
        FINANCING_CREDIT_CODE = fINANCING_CREDIT_CODE;
    }

    public void setGO_PUBLIC(String gO_PUBLIC) {
        GO_PUBLIC = gO_PUBLIC;
    }

    public void setGROSS_INCOME_PER_YEAR(String gROSS_INCOME_PER_YEAR) {
        GROSS_INCOME_PER_YEAR = gROSS_INCOME_PER_YEAR;
    }

    public void setINSURANCE(String iNSURANCE) {
        INSURANCE = iNSURANCE;
    }

    public void setJOINT_CREDIT_STATUS(String jOINT_CREDIT_STATUS) {
        JOINT_CREDIT_STATUS = jOINT_CREDIT_STATUS;
    }

    public void setKODE_BIDANG_USAHA(String kODE_BIDANG_USAHA) {
        KODE_BIDANG_USAHA = kODE_BIDANG_USAHA;
    }

    public void setMOBILE_PHONE_NUMBER(String mOBILE_PHONE_NUMBER) {
        MOBILE_PHONE_NUMBER = mOBILE_PHONE_NUMBER;
    }

    public void setNEW(String nEW) {
        NEW = nEW;
    }

    public void setNO_PIN(String nO_PIN) {
        NO_PIN = nO_PIN;
    }

    public void setNUMBER_OF_DEPENDENTS(String nUMBER_OF_DEPENDENTS) {
        NUMBER_OF_DEPENDENTS = nUMBER_OF_DEPENDENTS;
    }

    public void setORIENTATION_OF_USE_CODE(String oRIENTATION_OF_USE_CODE) {
        ORIENTATION_OF_USE_CODE = oRIENTATION_OF_USE_CODE;
    }

    public void setPERJANJIAN_PISAH_HARTA(String pERJANJIAN_PISAH_HARTA) {
        PERJANJIAN_PISAH_HARTA = pERJANJIAN_PISAH_HARTA;
    }

    public void setRATING_AGENCIES(String rATING_AGENCIES) {
        RATING_AGENCIES = rATING_AGENCIES;
    }

    public void setREGENCY_CODE(String rEGENCY_CODE) {
        REGENCY_CODE = rEGENCY_CODE;
    }

    public void setSKIM_CODE(String sKIM_CODE) {
        SKIM_CODE = sKIM_CODE;
    }

    public void setSOURCE_OF_INCOME_(String sOURCE_OF_INCOME_) {
        SOURCE_OF_INCOME_ = sOURCE_OF_INCOME_;
    }

    public void setSPOUSE_DATE_OF_BIRTH(String sPOUSE_DATE_OF_BIRTH) {
        SPOUSE_DATE_OF_BIRTH = sPOUSE_DATE_OF_BIRTH;
    }

    public void setSPOUSE_ID(String sPOUSE_ID) {
        SPOUSE_ID = sPOUSE_ID;
    }

    public void setSPOUSE_NAME(String sPOUSE_NAME) {
        SPOUSE_NAME = sPOUSE_NAME;
    }

    public void setTAKE_OVER_FROM(String tAKE_OVER_FROM) {
        TAKE_OVER_FROM = tAKE_OVER_FROM;
    }

    public void setTYPE_OF_BONDING_CODE(String tYPE_OF_BONDING_CODE) {
        TYPE_OF_BONDING_CODE = tYPE_OF_BONDING_CODE;
    }

    public void setTYPE_OF_CREDIT(String tYPE_OF_CREDIT) {
        TYPE_OF_CREDIT = tYPE_OF_CREDIT;
    }

    public void setTYPE_OF_USE_CODE_(String tYPE_OF_USE_CODE_) {
        TYPE_OF_USE_CODE_ = tYPE_OF_USE_CODE_;
    }

    public void setTYPES_OF_COLLATERAL_CODE(String tYPES_OF_COLLATERAL_CODE) {
        TYPES_OF_COLLATERAL_CODE = tYPES_OF_COLLATERAL_CODE;
    }

    public void setWORKING_ADDRESS(String wORKING_ADDRESS) {
        WORKING_ADDRESS = wORKING_ADDRESS;
    }

    /**
     * Return data from this entity into one line of string using 
     * fixed length for each field defined in {@link SlikPermata}. 
     */
    @Override
    public String toString() {
        OtherUtil util = new OtherUtil();
        StringBuilder sb = new StringBuilder(1340);
        sb.append(util.rightPaddingSpace(NO_PIN, SlikPermata.NO_PIN.getMaxLength()));
        sb.append(util.rightPaddingSpace(FINANCING_CREDIT_CODE, SlikPermata.FINANCING_CREDIT_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(TYPE_OF_CREDIT, SlikPermata.TYPE_OF_CREDIT.getMaxLength()));
        sb.append(util.rightPaddingSpace(SKIM_CODE, SlikPermata.SKIM_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(NEW, SlikPermata.NEW.getMaxLength()));
        sb.append(util.rightPaddingSpace(TYPE_OF_USE_CODE_, SlikPermata.TYPE_OF_USE_CODE_.getMaxLength()));
        sb.append(util.rightPaddingSpace(ORIENTATION_OF_USE_CODE, SlikPermata.ORIENTATION_OF_USE_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(TAKE_OVER_FROM, SlikPermata.TAKE_OVER_FROM.getMaxLength()));
        sb.append(util.rightPaddingSpace(EMAIL_ADDRESS, SlikPermata.EMAIL_ADDRESS.getMaxLength()));
        sb.append(util.rightPaddingSpace(MOBILE_PHONE_NUMBER, SlikPermata.MOBILE_PHONE_NUMBER.getMaxLength()));
        sb.append(util.rightPaddingSpace(WORKING_ADDRESS, SlikPermata.WORKING_ADDRESS.getMaxLength()));
        sb.append(util.rightPaddingSpace(GROSS_INCOME_PER_YEAR, SlikPermata.GROSS_INCOME_PER_YEAR.getMaxLength()));
        sb.append(util.rightPaddingSpace(SOURCE_OF_INCOME_, SlikPermata.SOURCE_OF_INCOME_.getMaxLength()));
        sb.append(util.rightPaddingSpace(NUMBER_OF_DEPENDENTS, SlikPermata.NUMBER_OF_DEPENDENTS.getMaxLength()));
        sb.append(util.rightPaddingSpace(SPOUSE_NAME, SlikPermata.SPOUSE_NAME.getMaxLength()));
        sb.append(util.rightPaddingSpace(SPOUSE_ID, SlikPermata.SPOUSE_ID.getMaxLength()));
        sb.append(util.rightPaddingSpace(SPOUSE_DATE_OF_BIRTH, SlikPermata.SPOUSE_DATE_OF_BIRTH.getMaxLength()));
        sb.append(util.rightPaddingSpace(PERJANJIAN_PISAH_HARTA, SlikPermata.PERJANJIAN_PISAH_HARTA.getMaxLength()));
        sb.append(util.rightPaddingSpace(KODE_BIDANG_USAHA, SlikPermata.KODE_BIDANG_USAHA.getMaxLength()));
        sb.append(util.rightPaddingSpace(BUSINESS_ENTITY_CODE, SlikPermata.BUSINESS_ENTITY_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(DEBTOR_GROUP_CODE, SlikPermata.DEBTOR_GROUP_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(GO_PUBLIC, SlikPermata.GO_PUBLIC.getMaxLength()));
        sb.append(util.rightPaddingSpace(DEBTOR_RATING, SlikPermata.DEBTOR_RATING.getMaxLength()));
        sb.append(util.rightPaddingSpace(RATING_AGENCIES, SlikPermata.RATING_AGENCIES.getMaxLength()));
        sb.append(util.rightPaddingSpace(DATE_OF_RATING, SlikPermata.DATE_OF_RATING.getMaxLength()));
        sb.append(util.rightPaddingSpace(DEBTOR_GROUP_NAME, SlikPermata.DEBTOR_GROUP_NAME.getMaxLength()));
        sb.append(util.rightPaddingSpace(TYPES_OF_COLLATERAL_CODE, SlikPermata.TYPES_OF_COLLATERAL_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(TYPE_OF_BONDING_CODE, SlikPermata.TYPE_OF_BONDING_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(BINDING_DATE, SlikPermata.BINDING_DATE.getMaxLength()));
        sb.append(util.rightPaddingSpace(COLLATERAL_NAME, SlikPermata.COLLATERAL_NAME.getMaxLength()));
        sb.append(util.rightPaddingSpace(COLLATERAL_ADDRESS, SlikPermata.COLLATERAL_ADDRESS.getMaxLength()));
        sb.append(util.rightPaddingSpace(JOINT_CREDIT_STATUS, SlikPermata.JOINT_CREDIT_STATUS.getMaxLength()));
        sb.append(util.rightPaddingSpace(REGENCY_CODE, SlikPermata.REGENCY_CODE.getMaxLength()));
        sb.append(util.rightPaddingSpace(INSURANCE, SlikPermata.INSURANCE.getMaxLength()));
        return sb.toString();
    }
    
}
