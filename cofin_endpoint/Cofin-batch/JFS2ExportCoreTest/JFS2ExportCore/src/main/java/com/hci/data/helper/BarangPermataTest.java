package com.hci.data.helper;

import com.hci.data.entity.permata.Jfs2ExportBarangPermata;
import com.hci.data.helper.permata.BarangPermata;

public class BarangPermataTest {

    public static void main(String[] args) {
        Jfs2ExportBarangPermata model = new Jfs2ExportBarangPermata();
        model.setNO_PIN("3700250883");
        model.setCAR_REGISTRATION_NO("");
        model.setCAR_REGISTRATION_DATE("");
        model.setNAME_ON_CAR_REGISTRATION("Yosep Krismono");
        model.setPLACE_OF_CAR_REGISTRATION("");
        model.setENGINE_NUMBER("Redmi 4A");
        model.setCHASIS_NUMBER("");
        model.setLISENCE_PLATE_NUMBER("");
        model.setBRAND("CAT_MP");
        model.setTYPE("CT_TE_MP");
        model.setCONDITION_OF_GOODS("N");
        model.setASSEMBLY_YEAR("2017");
        model.setTYPE_PF_GOODS("DGN2");
        model.setTYPE_OF_GOODS("Xiomi");
        model.setVALUE_OF_GOODS("1500000");
        System.out.println(model.toString());
        System.out.println(model.toString().length());

        Jfs2ExportBarangPermata model2 = new Jfs2ExportBarangPermata();
        System.out.println(model2.toString());
        System.out.println(model2.toString().length());
        Integer total = 0;
        for (BarangPermata permata : BarangPermata.values()) {
            total = total + permata.getMaxLength();
        }
        System.out.println(total);

        // // test set model
        // for (BarangPermata enu : BarangPermata.values()) {
        // System.out.println("model.set" + enu.name() + "();");
        // }
        //
        // // entity model
        // for (BarangPermata enu : BarangPermata.values()) {
        // System.out.println("private String " + enu.name() + ";");
        // }
        //
        // // to string
        // for (BarangPermata enu : BarangPermata.values()) {
        // System.out.println("sb.append(util.rightPaddingSpace(" + enu.name()
        // + ", BarangPermata." + enu.name() + ".getMaxLength()));");
        // }

        // mapper
        for (BarangPermata enu : BarangPermata.values()) {
            System.out.println("model.set" + enu.name() + "(rs.getString(\"" + enu.name()
                    + "\") != null ? rs.getString(\"" + enu.name() + "\") : \"\");");
        }
    }

}
