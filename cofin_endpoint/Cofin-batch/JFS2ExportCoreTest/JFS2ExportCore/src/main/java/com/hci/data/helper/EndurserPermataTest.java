/**
 *
 */
package com.hci.data.helper;

import com.hci.data.entity.permata.Jfs2ExportEnduserPermata;
import com.hci.data.helper.permata.EnduserPermata;

public class EndurserPermataTest {

    public static void main(String[] args) {
        Jfs2ExportEnduserPermata model = new Jfs2ExportEnduserPermata();
        model.setNO_PIN("3700252136");
        model.setEND_USER_NAME("Mudiyanto");
        model.setALIAS("Mudiyanto");
        model.setSTATUS("0199");
        model.setSTATUS_DESCRIPTION("SMP(Junior High school)");
        model.setDEBTOR_CLASS("907");
        model.setSEX("1");
        model.setID_CARD_NUMBER("3173010511950010");
        model.setPASSPORT_NO("");
        model.setBIRTH_CERTIFICATE_NO("");
        model.setPLACE_OF_BIRTH("Jakarta");
        model.setDATE_OF_BIRTH("05/11/1995");
        model.setDATE_OF_FINAL_DEED("");
        model.setTAX_REGISTRATION("");
        model.setDEBTORS_ADDRESS("Jl Pedongkelan RT02 RW13");
        model.setSUB_DISTRICT("KAPUK");
        model.setKECAMATAN("CENGKARENG");
        model.setDISTRICT_OF_DEBTOR("0393");
        model.setPOSTAL_CODE("11720");
        model.setAREA_CODE("");
        model.setTELEPHONE("");
        model.setCOUNTRY_OF_DOMICILE("ID");
        model.setMAIDEN_NAME_OF_BIOLOGICAL_MOTHER("Kholifah");
        model.setOCCUPATION_CODE("099");
        model.setPLACE_OF_WORK("PT Elektronik");
        model.setFIELD_OF_BUSINESS("6000");
        model.setID_TYPE("1");
        model.setMARITAL_STATUS("1");
        model.setKEWARGANEGARAAN("ID");
        model.setRELIGION("7");
        model.setINCOME("2100000");
        model.setID_EXPIRED("05/11/2018");
        System.out.println(model.toString());
        System.out.println(model.toString().length());

        Jfs2ExportEnduserPermata model2 = new Jfs2ExportEnduserPermata();
        System.out.println(model2.toString());
        System.out.println(model2.toString().length());
        Integer total = 0;
        for (EnduserPermata permata : EnduserPermata.values()) {
            total = total + permata.getMaxLength();
        }
        System.out.println(total);

        // // test set model
        // for (EnduserPermata enu : EnduserPermata.values()) {
        // System.out.println("model.set" + enu.name() + "(\"\");");
        // }
        //
        // // entity model
        // for (EnduserPermata enu : EnduserPermata.values()) {
        // System.out.println("private String " + enu.name() + ";");
        // }
        //
        // // to string
        // for (EnduserPermata enu : EnduserPermata.values()) {
        // System.out.println("sb.append(util.rightPaddingSpace(" + enu.name()
        // + ", EnduserPermata." + enu.name() + ".getMaxLength()));");
        // }

        // mapper
        for (EnduserPermata enu : EnduserPermata.values()) {
            System.out.println("model.set" + enu.name() + "(rs.getString(\"" + enu.name()
                    + "\") != null ? rs.getString(\"" + enu.name() + "\") : \"\");");
        }
    }

}
