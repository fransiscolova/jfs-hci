package com.hci.data.helper;

import com.hci.data.entity.permata.Jfs2ExportKontrakPermata;
import com.hci.data.helper.permata.KontrakPermata;

public class KontrakPermataTest {

    public static void main(String[] args) {
        Jfs2ExportKontrakPermata model = new Jfs2ExportKontrakPermata();
        model.setNO_PIN("3700240420");
        model.setNAME("Maya Widiana Komsani");
        model.setMF_BRANCH_CODE("0650");
        model.setNO_PK_ENDURSER("3700240420");
        model.setEFFECTIVE_DATE_END_USER("02/03/2017");
        model.setINITIAL_PRINCIPAL_AT_MF("3524600");
        model.setGOODS_VALUE("3899000");
        model.setMF_INITIAL_TENOR("10");
        model.setAMOUNT_OF_INSTALLMENT_TO_MF("440600");
        model.setMF_INTEREST_TO_END_USER("51.30");
        model.setPRINCIPLE_UPON_ASSIGNMENT("3524600");
        model.setBANK_FINANCED_PRINCIPAL("3172140");
        model.setTENOR_WITH_BANK("10");
        model.setINSTALLMENT_PAYMENT_DATE_1("02/04/2017");
        model.setFINAL_INSTALLMENT_DATE("02/01/2018");
        model.setINTEREST_AT_THE_BANK("34.08");
        model.setAMOUNT_OF_INSTALLMENT_TO_BANK("368841");
        model.setINSTALLMENT_PERIOD("1");
        model.setVEHICLE_CONDITION("N");
        model.setTYPE_OF_CLOSING("T");
        model.setINSURANCE_PREMIUM_AMOUNT("0");
        model.setPREMIUM_PAYMENT("E");
        model.setVEHICLE_CLASS("DG");
        model.setCREDIT_SCORING("0");
        model.setDEBT_BURDEN_RATIO("");
        model.setEND_USER_LOAN_AGREEMENT_DATE("02/03/2017");
        model.setAMOUNT_PREMIUM_PAID("0");
        model.setAMOUNT_DP("781000");
        model.setINSURANCE_COMPANY("003300");
        model.setAREA_OF_BUSINESS("K");
        model.setPURPOSE_OF_USAGE("");
        model.setDOCUMENT_PROOF_OF_INCOME("");
        model.setPBB("");
        model.setSPOUSE_KTP("");
        model.setLICENCING("");
        model.setDEED("");
        model.setNPWP("");
        model.setFINANCING_APPLICATION_FORM("");
        model.setFIDUCIARY("");
        model.setCODE_IN_REAR("0");
        System.out.println(model.toString());
        System.out.println(model.toString().length());

        Jfs2ExportKontrakPermata model2 = new Jfs2ExportKontrakPermata();
        System.out.println(model2.toString());
        System.out.println(model2.toString().length());
        Integer total = 0;
        for (KontrakPermata permata : KontrakPermata.values()) {
            total = total + permata.getMaxLength();
        }
        System.out.println(total);

        // // test set model
        // for (KontrakPermata enu : KontrakPermata.values()) {
        // System.out.println("model.set" + enu.name() + "(\"\");");
        // }
        //
        // // entity model
        // for (KontrakPermata enu : KontrakPermata.values()) {
        // System.out.println("private String " + enu.name() + ";");
        // }
        //
        // // to string
        // for (KontrakPermata enu : KontrakPermata.values()) {
        // System.out.println("sb.append(util.rightPaddingSpace(" + enu.name()
        // + ", KontrakPermata." + enu.name() + ".getMaxLength()));");
        // }

        // mapper
        for (KontrakPermata enu : KontrakPermata.values()) {
            System.out.println("model.set" + enu.name() + "(rs.getString(\"" + enu.name()
                    + "\") != null ? rs.getString(\"" + enu.name() + "\") : \"\");");
        }
    }
}
