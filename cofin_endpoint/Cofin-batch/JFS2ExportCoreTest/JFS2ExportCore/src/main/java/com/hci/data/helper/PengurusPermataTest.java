package com.hci.data.helper;

import com.hci.data.entity.permata.Jfs2ExportPengurusPermata;
import com.hci.data.helper.permata.PengurusPermata;

public class PengurusPermataTest {

    public static void main(String[] args) {
        Jfs2ExportPengurusPermata model = new Jfs2ExportPengurusPermata();
        model.setNO_PIN("3700250883");
        model.setNAME_OF_MANAGER("");
        model.setID_CARD_OF_MANAGER("");
        model.setMANAGERS_TAX_REGISTRATION_NO("");
        model.setSEX("");
        model.setOWNERSHIP_PORTION("");
        model.setPOSITION_CODE("");
        model.setADDRESS("");
        model.setSUB_DISTRICT("");
        model.setKECAMATAN("");
        model.setDISTRICT_OF_DEBTOR("");
        model.setPOSTAL_CODE("");
        System.out.println(model.toString());
        System.out.println(model.toString().length());

        Jfs2ExportPengurusPermata model2 = new Jfs2ExportPengurusPermata();
        System.out.println(model2.toString());
        System.out.println(model2.toString().length());
        Integer total = 0;
        for (PengurusPermata permata : PengurusPermata.values()) {
            total = total + permata.getMaxLength();
        }
        System.out.println(total);

        // // test set model
        // for (PengurusPermata enu : PengurusPermata.values()) {
        // System.out.println("model.set" + enu.name() + "(\"\");");
        // }
        //
        // // entity model
        // for (PengurusPermata enu : PengurusPermata.values()) {
        // System.out.println("private String " + enu.name() + ";");
        // }
        //
        // // to string
        // for (PengurusPermata enu : PengurusPermata.values()) {
        // System.out.println("sb.append(util.rightPaddingSpace(" + enu.name()
        // + ", PengurusPermata." + enu.name() + ".getMaxLength()));");
        // }

        // mapper
        for (PengurusPermata enu : PengurusPermata.values()) {
            System.out.println("model.set" + enu.name() + "(rs.getString(\"" + enu.name()
                    + "\") != null ? rs.getString(\"" + enu.name() + "\") : \"\");");
        }
    }

}
