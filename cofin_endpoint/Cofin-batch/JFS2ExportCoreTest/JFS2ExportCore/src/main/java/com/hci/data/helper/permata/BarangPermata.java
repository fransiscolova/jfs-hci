/**
 * 
 */
package com.hci.data.helper.permata;

/**
 * Barang txt file mapping for each field info about its max length and start number.
 * 
 * @author muhammad.muflihun
 *
 */
public enum BarangPermata {
    NO_PIN(17, 1),
    CAR_REGISTRATION_NO(15, 19),
    CAR_REGISTRATION_DATE(10, 35),
    NAME_ON_CAR_REGISTRATION(50, 46),
    PLACE_OF_CAR_REGISTRATION(4, 97),
    ENGINE_NUMBER(22, 102),
    CHASIS_NUMBER(22, 125),
    LISENCE_PLATE_NUMBER(10, 148),
    BRAND(30, 159),
    TYPE(30, 190),
    CONDITION_OF_GOODS(1, 221),
    ASSEMBLY_YEAR(4, 223),
    TYPE_PF_GOODS(5, 228),
    TYPE_OF_GOODS(51, 234),
    VALUE_OF_GOODS(12, 286);

    private Integer startFrom;
    private Integer maxLength;

    BarangPermata(Integer maxLength, Integer startFrom) {
        this.maxLength = maxLength;
        this.startFrom = startFrom;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public Integer getStartFrom() {
        return startFrom;
    }

}
