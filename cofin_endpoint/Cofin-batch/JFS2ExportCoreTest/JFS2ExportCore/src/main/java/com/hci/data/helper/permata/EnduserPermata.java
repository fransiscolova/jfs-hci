/**
 *
 */
package com.hci.data.helper.permata;

/**
 * End user txt file mapping for each field info about its max length and start number.
 *
 * @author muhammad.muflihun
 *
 */
public enum EnduserPermata {
    NO_PIN(17, 1),
    END_USER_NAME(50, 19),
    ALIAS(50, 70),
    STATUS(4, 121),
    STATUS_DESCRIPTION(50, 126),
    DEBTOR_CLASS(3, 177),
    SEX(1, 181),
    ID_CARD_NUMBER(30, 183),
    PASSPORT_NO(30, 214),
    BIRTH_CERTIFICATE_NO(30, 245),
    PLACE_OF_BIRTH(50, 276),
    DATE_OF_BIRTH(10, 327),
    DATE_OF_FINAL_DEED(10, 338),
    TAX_REGISTRATION(20, 349),
    DEBTORS_ADDRESS(100, 370),
    SUB_DISTRICT(50, 471),
    KECAMATAN(50, 522),
    DISTRICT_OF_DEBTOR(4, 573),
    POSTAL_CODE(5, 578),
    AREA_CODE(4, 584),
    TELEPHONE(10, 589),
    COUNTRY_OF_DOMICILE(3, 600),
    MAIDEN_NAME_OF_BIOLOGICAL_MOTHER(50, 604),
    OCCUPATION_CODE(3, 655),
    PLACE_OF_WORK(50, 659),
    FIELD_OF_BUSINESS(5, 710),
    ID_TYPE(2, 716),
    MARITAL_STATUS(2, 719),
    KEWARGANEGARAAN(2, 722),
    RELIGION(2, 725),
    INCOME(12, 728),
    ID_EXPIRED(10, 741);
    
    private Integer startFrom;
    private Integer maxLength;

    EnduserPermata(Integer maxLength, Integer startFrom) {
        this.maxLength = maxLength;
        this.startFrom = startFrom;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public Integer getStartFrom() {
        return startFrom;
    }

}
