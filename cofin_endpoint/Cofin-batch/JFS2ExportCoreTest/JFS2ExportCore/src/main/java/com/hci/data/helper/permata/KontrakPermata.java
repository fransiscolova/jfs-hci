/**
 *
 */
package com.hci.data.helper.permata;

/**
 * Kontrak txt file mapping for each field info about its max length and start number. 
 * 
 * @author muhammad.muflihun
 *
 */
public enum KontrakPermata {
    NO_PIN(17, 1), 
    NAME(50, 19), 
    MF_BRANCH_CODE(4, 70), 
    NO_PK_ENDURSER(17, 75), 
    EFFECTIVE_DATE_END_USER(10, 93), 
    INITIAL_PRINCIPAL_AT_MF(12, 104),
    GOODS_VALUE(12, 117),
    MF_INITIAL_TENOR(3, 130),
    AMOUNT_OF_INSTALLMENT_TO_MF(12, 134),
    MF_INTEREST_TO_END_USER(5, 147),
    PRINCIPLE_UPON_ASSIGNMENT(12, 153),
    BANK_FINANCED_PRINCIPAL(12, 166),
    TENOR_WITH_BANK(3, 179),
    INSTALLMENT_PAYMENT_DATE_1(10, 183),
    FINAL_INSTALLMENT_DATE(10, 194),
    INTEREST_AT_THE_BANK(5, 205),
    AMOUNT_OF_INSTALLMENT_TO_BANK(12, 211),
    INSTALLMENT_PERIOD(2, 224),
    VEHICLE_CONDITION(1, 227),
    TYPE_OF_CLOSING(1, 229),
    INSURANCE_PREMIUM_AMOUNT(12, 231),
    PREMIUM_PAYMENT(1, 244),
    VEHICLE_CLASS(2, 246),
    CREDIT_SCORING(2, 249),
    DEBT_BURDEN_RATIO(5, 252),
    END_USER_LOAN_AGREEMENT_DATE(10, 258),
    AMOUNT_PREMIUM_PAID(12, 269),
    AMOUNT_DP(12, 282),
    INSURANCE_COMPANY(10, 295),
    AREA_OF_BUSINESS(10, 306),
    PURPOSE_OF_USAGE(10, 317),
    DOCUMENT_PROOF_OF_INCOME(1, 328),
    PBB(1, 330),
    SPOUSE_KTP(1, 332),
    LICENCING(1, 334),
    DEED(1, 336),
    NPWP(1, 338),
    FINANCING_APPLICATION_FORM(1, 340),
    FIDUCIARY(1, 342),
    CODE_IN_REAR(1, 344);

    private Integer startFrom;
    private Integer maxLength;

    KontrakPermata(Integer maxLength, Integer startFrom) {
        this.maxLength = maxLength;
        this.startFrom = startFrom;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public Integer getStartFrom() {
        return startFrom;
    }

}
