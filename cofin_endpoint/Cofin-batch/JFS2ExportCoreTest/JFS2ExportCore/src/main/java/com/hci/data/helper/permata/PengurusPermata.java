/**
 * 
 */
package com.hci.data.helper.permata;

/**
 * Pengurus txt file mapping for each field info about its max length and start number.
 * 
 * @author muhammad.muflihun
 *
 */
public enum PengurusPermata {
    NO_PIN(17, 1),
    NAME_OF_MANAGER(50, 19),
    ID_CARD_OF_MANAGER(30, 70),
    MANAGERS_TAX_REGISTRATION_NO(20, 101),
    SEX(1, 122),
    OWNERSHIP_PORTION(5, 124),
    POSITION_CODE(2, 130),
    ADDRESS(199, 133),
    SUB_DISTRICT(50, 333),
    KECAMATAN(50, 384),
    DISTRICT_OF_DEBTOR(4, 435),
    POSTAL_CODE(5, 440);
    
    private Integer startFrom;
    private Integer maxLength;

    PengurusPermata(Integer maxLength, Integer startFrom) {
        this.maxLength = maxLength;
        this.startFrom = startFrom;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public Integer getStartFrom() {
        return startFrom;
    }

}
