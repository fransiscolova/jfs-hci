/**
 * 
 */
package com.hci.data.helper.permata;

/**
 * Slik txt file mapping for each field info about its max length and start number.
 * 
 * @author muhammad.muflihun
 *
 */
public enum SlikPermata {
    NO_PIN(17, 1),
    FINANCING_CREDIT_CODE(1, 19),
    TYPE_OF_CREDIT(2, 21),
    SKIM_CODE(2, 24),
    NEW(1, 27),
    TYPE_OF_USE_CODE_(1, 29),
    ORIENTATION_OF_USE_CODE(1, 31),
    TAKE_OVER_FROM(6, 33),
    EMAIL_ADDRESS(150, 40),
    MOBILE_PHONE_NUMBER(15, 191),
    WORKING_ADDRESS(300, 207),
    GROSS_INCOME_PER_YEAR(12, 508),
    SOURCE_OF_INCOME_(1, 521),
    NUMBER_OF_DEPENDENTS(2,  523),
    SPOUSE_NAME(150, 526),
    SPOUSE_ID(16, 677),
    SPOUSE_DATE_OF_BIRTH(10, 694),
    PERJANJIAN_PISAH_HARTA(1, 705),
    KODE_BIDANG_USAHA(6, 707),
    BUSINESS_ENTITY_CODE(2, 714),
    DEBTOR_GROUP_CODE(4, 717),
    GO_PUBLIC(1, 722),
    DEBTOR_RATING(6, 724),
    RATING_AGENCIES(2, 731),
    DATE_OF_RATING(10, 734),
    DEBTOR_GROUP_NAME(150, 745),
    TYPES_OF_COLLATERAL_CODE(3, 896),
    TYPE_OF_BONDING_CODE(2, 900),
    BINDING_DATE(10, 903),
    COLLATERAL_NAME(150, 914),
    COLLATERAL_ADDRESS(300, 1065),
    JOINT_CREDIT_STATUS(1, 1366),
    REGENCY_CODE(4, 1368),
    INSURANCE(1, 1373);

    private Integer startFrom;
    private Integer maxLength;

    SlikPermata(Integer maxLength, Integer startFrom) {
        this.maxLength = maxLength;
        this.startFrom = startFrom;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public Integer getStartFrom() {
        return startFrom;
    }
    
}
