package com.hci.data.mapper.btpn;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.btpn.Jfs2ExportKontrakBtpn;

public class Jfs2ExportMapperKontrakBtpn implements RowMapper<Jfs2ExportKontrakBtpn> {

	@Override
	public Jfs2ExportKontrakBtpn mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jfs2ExportKontrakBtpn jfsPayment = new Jfs2ExportKontrakBtpn();
		
		jfsPayment.setContractCode(rs.getString("CONTRACT_CODE") != null ? rs.getString("CONTRACT_CODE") : "");
		jfsPayment.setFullName(rs.getString("FULL_NAME") != null ? rs.getString("FULL_NAME") : "");
		jfsPayment.setCode(rs.getString("CODE") != null ? rs.getString("CODE") : "");
		jfsPayment.setContractCode1(rs.getString("CONTRACT_CODE_1") != null ? rs.getString("CONTRACT_CODE_1") : "");
		jfsPayment.setCreationDate(rs.getString("CREATION_DATE") != null ? rs.getString("CREATION_DATE") : "");
		jfsPayment.setCreditAmount(rs.getString("CREDIT_AMOUNT") != null ? rs.getString("CREDIT_AMOUNT") : "");
		jfsPayment.setGoodsPriceAmount(rs.getString("GOODS_PRICE_AMOUNT") != null ? rs.getString("GOODS_PRICE_AMOUNT") : "");
		jfsPayment.setTerms(rs.getString("TERMS") != null ? rs.getString("TERMS") : "");
		jfsPayment.setAnnuityAmount(rs.getString("ANNUITY_AMOUNT") != null ? rs.getString("ANNUITY_AMOUNT") : "");
		jfsPayment.setInterestRate(rs.getString("INTEREST_RATE") != null ? rs.getString("INTEREST_RATE") : "");
		jfsPayment.setCreditAmount1(rs.getString("CREDIT_AMOUNT_1") != null ? rs.getString("CREDIT_AMOUNT_1") : "");
		jfsPayment.setSendPrincipal(rs.getString("SEND_PRINCIPAL") != null ? rs.getString("SEND_PRINCIPAL") : "");
		jfsPayment.setTerms1(rs.getString("TERMS_1") != null ? rs.getString("TERMS_1") : "");
		jfsPayment.setDueDate(rs.getString("DUE_DATE") != null ? rs.getString("DUE_DATE") : "");
		jfsPayment.setI1DueDate(rs.getString("I1_DUE_DATE") != null ? rs.getString("I1_DUE_DATE") : "");
		jfsPayment.setSendRate(rs.getString("SEND_RATE") != null ? rs.getString("SEND_RATE") : "");
		jfsPayment.setSendInstalment(rs.getString("SEND_INSTALMENT") != null ? rs.getString("SEND_INSTALMENT") : "");
		jfsPayment.setCreationDateCst(rs.getString("CREATION_DATE_CST") != null ? rs.getString("CREATION_DATE_CST") : "");
		jfsPayment.setPayment(rs.getString("PAYMENT") != null ? rs.getString("PAYMENT") : "");
		jfsPayment.setBiEconomicSectorCode(rs.getString("BI_ECONOMIC_SECTOR_CODE") != null ? rs.getString("BI_ECONOMIC_SECTOR_CODE") : "");
		jfsPayment.setAgreementId(rs.getString("AGREEMENT_ID") != null ? rs.getString("AGREEMENT_ID") : "");
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID") != null ? rs.getString("PARTNER_ID") : "");
		
		return jfsPayment;
	}
	
}
