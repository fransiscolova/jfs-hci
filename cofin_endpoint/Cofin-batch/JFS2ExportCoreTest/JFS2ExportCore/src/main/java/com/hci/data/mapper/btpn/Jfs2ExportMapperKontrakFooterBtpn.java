package com.hci.data.mapper.btpn;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.btpn.Jfs2ExportKontrakFooterBtpn;

public class Jfs2ExportMapperKontrakFooterBtpn implements RowMapper<Jfs2ExportKontrakFooterBtpn> {

	@Override
	public Jfs2ExportKontrakFooterBtpn mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jfs2ExportKontrakFooterBtpn jfsPayment = new Jfs2ExportKontrakFooterBtpn();
		
		jfsPayment.setNumOfRows(rs.getString("NUM_OF_ROWS") != null ? rs.getString("NUM_OF_ROWS") : "");
		jfsPayment.setCreditAmount(rs.getString("CREDIT_AMOUNT") != null ? rs.getString("CREDIT_AMOUNT") : "");
		
		return jfsPayment;
	}
	
}
