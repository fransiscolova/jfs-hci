package com.hci.data.mapper.btpn;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.btpn.Jfs2ExportPengurusBtpn;

public class Jfs2ExportMapperPengurusBtpn implements RowMapper<Jfs2ExportPengurusBtpn> {

	@Override
	public Jfs2ExportPengurusBtpn mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jfs2ExportPengurusBtpn jfsPayment = new Jfs2ExportPengurusBtpn();
		
		jfsPayment.setContractCode(rs.getString("CONTRACT_CODE") != null ? rs.getString("CONTRACT_CODE") : "");
		
		return jfsPayment;
	}
	
}
