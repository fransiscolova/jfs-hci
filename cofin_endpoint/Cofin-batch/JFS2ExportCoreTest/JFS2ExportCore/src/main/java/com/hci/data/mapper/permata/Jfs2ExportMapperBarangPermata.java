/**
 *
 */
package com.hci.data.mapper.permata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.permata.Jfs2ExportBarangPermata;

/**
 * Result mapper from query result to {@link Jfs2ExportBarangPermata}.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportMapperBarangPermata implements RowMapper<Jfs2ExportBarangPermata> {

    @Override
    public Jfs2ExportBarangPermata mapRow(ResultSet rs, int rowNum) throws SQLException {
        Jfs2ExportBarangPermata model = new Jfs2ExportBarangPermata();
        model.setNO_PIN(rs.getString("NO_PIN") != null ? rs.getString("NO_PIN") : "");
        model.setCAR_REGISTRATION_NO(rs.getString("CAR_REGISTRATION_NO") != null ? rs.getString("CAR_REGISTRATION_NO") : "");
        model.setCAR_REGISTRATION_DATE(rs.getString("CAR_REGISTRATION_DATE") != null ? rs.getString("CAR_REGISTRATION_DATE") : "");
        model.setNAME_ON_CAR_REGISTRATION(rs.getString("NAME_ON_CAR_REGISTRATION") != null ? rs.getString("NAME_ON_CAR_REGISTRATION") : "");
        model.setPLACE_OF_CAR_REGISTRATION(rs.getString("PLACE_OF_CAR_REGISTRATION") != null ? rs.getString("PLACE_OF_CAR_REGISTRATION") : "");
        model.setENGINE_NUMBER(rs.getString("ENGINE_NUMBER") != null ? rs.getString("ENGINE_NUMBER") : "");
        model.setCHASIS_NUMBER(rs.getString("CHASIS_NUMBER") != null ? rs.getString("CHASIS_NUMBER") : "");
        model.setLISENCE_PLATE_NUMBER(rs.getString("LISENCE_PLATE_NUMBER") != null ? rs.getString("LISENCE_PLATE_NUMBER") : "");
        model.setBRAND(rs.getString("BRAND") != null ? rs.getString("BRAND") : "");
        model.setTYPE(rs.getString("TYPE") != null ? rs.getString("TYPE") : "");
        model.setCONDITION_OF_GOODS(rs.getString("CONDITION_OF_GOODS") != null ? rs.getString("CONDITION_OF_GOODS") : "");
        model.setASSEMBLY_YEAR(rs.getString("ASSEMBLY_YEAR") != null ? rs.getString("ASSEMBLY_YEAR") : "");
        model.setTYPE_PF_GOODS(rs.getString("TYPE_PF_GOODS") != null ? rs.getString("TYPE_PF_GOODS") : "");
        model.setTYPE_OF_GOODS(rs.getString("TYPE_OF_GOODS") != null ? rs.getString("TYPE_OF_GOODS") : "");
        model.setVALUE_OF_GOODS(rs.getString("VALUE_OF_GOODS") != null ? rs.getString("VALUE_OF_GOODS") : "");
        return model;
    }
}
