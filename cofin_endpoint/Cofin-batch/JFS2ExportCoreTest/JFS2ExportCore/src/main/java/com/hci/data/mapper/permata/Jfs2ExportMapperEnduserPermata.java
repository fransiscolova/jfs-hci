/**
 *
 */
package com.hci.data.mapper.permata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.permata.Jfs2ExportEnduserPermata;

/**
 * Result mapper from query result to {@link Jfs2ExportEnduserPermata}.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportMapperEnduserPermata implements RowMapper<Jfs2ExportEnduserPermata> {

    @Override
    public Jfs2ExportEnduserPermata mapRow(ResultSet rs, int rowNum) throws SQLException {
        Jfs2ExportEnduserPermata model = new Jfs2ExportEnduserPermata();
        model.setNO_PIN(rs.getString("NO_PIN") != null ? rs.getString("NO_PIN") : "");
        model.setEND_USER_NAME(rs.getString("END_USER_NAME") != null ? rs.getString("END_USER_NAME") : "");
        model.setALIAS(rs.getString("ALIAS") != null ? rs.getString("ALIAS") : "");
        model.setSTATUS(rs.getString("STATUS") != null ? rs.getString("STATUS") : "");
        model.setSTATUS_DESCRIPTION(rs.getString("STATUS_DESCRIPTION") != null ? rs.getString("STATUS_DESCRIPTION") : "");
        model.setDEBTOR_CLASS(rs.getString("DEBTOR_CLASS") != null ? rs.getString("DEBTOR_CLASS") : "");
        model.setSEX(rs.getString("SEX") != null ? rs.getString("SEX") : "");
        model.setID_CARD_NUMBER(rs.getString("ID_CARD_NUMBER") != null ? rs.getString("ID_CARD_NUMBER") : "");
        model.setPASSPORT_NO(rs.getString("PASSPORT_NO") != null ? rs.getString("PASSPORT_NO") : "");
        model.setBIRTH_CERTIFICATE_NO(rs.getString("BIRTH_CERTIFICATE_NO") != null ? rs.getString("BIRTH_CERTIFICATE_NO") : "");
        model.setPLACE_OF_BIRTH(rs.getString("PLACE_OF_BIRTH") != null ? rs.getString("PLACE_OF_BIRTH") : "");
        model.setDATE_OF_BIRTH(rs.getString("DATE_OF_BIRTH") != null ? rs.getString("DATE_OF_BIRTH") : "");
        model.setDATE_OF_FINAL_DEED(rs.getString("DATE_OF_FINAL_DEED") != null ? rs.getString("DATE_OF_FINAL_DEED") : "");
        model.setTAX_REGISTRATION(rs.getString("TAX_REGISTRATION") != null ? rs.getString("TAX_REGISTRATION") : "");
        model.setDEBTORS_ADDRESS(rs.getString("DEBTORS_ADDRESS") != null ? rs.getString("DEBTORS_ADDRESS") : "");
        model.setSUB_DISTRICT(rs.getString("SUB_DISTRICT") != null ? rs.getString("SUB_DISTRICT") : "");
        model.setKECAMATAN(rs.getString("KECAMATAN") != null ? rs.getString("KECAMATAN") : "");
        model.setDISTRICT_OF_DEBTOR(rs.getString("DISTRICT_OF_DEBTOR") != null ? rs.getString("DISTRICT_OF_DEBTOR") : "");
        model.setPOSTAL_CODE(rs.getString("POSTAL_CODE") != null ? rs.getString("POSTAL_CODE") : "");
        model.setAREA_CODE(rs.getString("AREA_CODE") != null ? rs.getString("AREA_CODE") : "");
        model.setTELEPHONE(rs.getString("TELEPHONE") != null ? rs.getString("TELEPHONE") : "");
        model.setCOUNTRY_OF_DOMICILE(rs.getString("COUNTRY_OF_DOMICILE") != null ? rs.getString("COUNTRY_OF_DOMICILE") : "");
        model.setMAIDEN_NAME_OF_BIOLOGICAL_MOTHER(rs.getString("MAIDEN_NAME_OF_BIOLOGICAL_MOTHER") != null ? rs.getString("MAIDEN_NAME_OF_BIOLOGICAL_MOTHER") : "");
        model.setOCCUPATION_CODE(rs.getString("OCCUPATION_CODE") != null ? rs.getString("OCCUPATION_CODE") : "");
        model.setPLACE_OF_WORK(rs.getString("PLACE_OF_WORK") != null ? rs.getString("PLACE_OF_WORK") : "");
        model.setFIELD_OF_BUSINESS(rs.getString("FIELD_OF_BUSINESS") != null ? rs.getString("FIELD_OF_BUSINESS") : "");
        model.setID_TYPE(rs.getString("ID_TYPE") != null ? rs.getString("ID_TYPE") : "");
        model.setMARITAL_STATUS(rs.getString("MARITAL_STATUS") != null ? rs.getString("MARITAL_STATUS") : "");
        model.setKEWARGANEGARAAN(rs.getString("KEWARGANEGARAAN") != null ? rs.getString("KEWARGANEGARAAN") : "");
        model.setRELIGION(rs.getString("RELIGION") != null ? rs.getString("RELIGION") : "");
        model.setINCOME(rs.getString("INCOME") != null ? rs.getString("INCOME") : "");
        model.setID_EXPIRED(rs.getString("ID_EXPIRED") != null ? rs.getString("ID_EXPIRED") : "");
        return model;
    }
}
