/**
 *
 */
package com.hci.data.mapper.permata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.permata.Jfs2ExportKontrakPermata;

/**
 * Result mapper from query result to {@link Jfs2ExportKontrakPermata}.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportMapperKontrakPermata implements RowMapper<Jfs2ExportKontrakPermata> {

    @Override
    public Jfs2ExportKontrakPermata mapRow(ResultSet rs, int rowNum) throws SQLException {
        Jfs2ExportKontrakPermata model = new Jfs2ExportKontrakPermata();
        model.setNO_PIN(rs.getString("NO_PIN") != null ? rs.getString("NO_PIN") : "");
        model.setNAME(rs.getString("NAME") != null ? rs.getString("NAME") : "");
        model.setMF_BRANCH_CODE(rs.getString("MF_BRANCH_CODE") != null ? rs.getString("MF_BRANCH_CODE") : "");
        model.setNO_PK_ENDURSER(rs.getString("NO_PK_ENDURSER") != null ? rs.getString("NO_PK_ENDURSER") : "");
        model.setEFFECTIVE_DATE_END_USER(rs.getString("EFFECTIVE_DATE_END_USER") != null ? rs.getString("EFFECTIVE_DATE_END_USER") : "");
        model.setINITIAL_PRINCIPAL_AT_MF(rs.getString("INITIAL_PRINCIPAL_AT_MF") != null ? rs.getString("INITIAL_PRINCIPAL_AT_MF") : "");
        model.setGOODS_VALUE(rs.getString("GOODS_VALUE") != null ? rs.getString("GOODS_VALUE") : "");
        model.setMF_INITIAL_TENOR(rs.getString("MF_INITIAL_TENOR") != null ? rs.getString("MF_INITIAL_TENOR") : "");
        model.setAMOUNT_OF_INSTALLMENT_TO_MF(rs.getString("AMOUNT_OF_INSTALLMENT_TO_MF") != null ? rs.getString("AMOUNT_OF_INSTALLMENT_TO_MF") : "");
        model.setMF_INTEREST_TO_END_USER(rs.getString("MF_INTEREST_TO_END_USER") != null ? rs.getString("MF_INTEREST_TO_END_USER") : "");
        model.setPRINCIPLE_UPON_ASSIGNMENT(rs.getString("PRINCIPLE_UPON_ASSIGNMENT") != null ? rs.getString("PRINCIPLE_UPON_ASSIGNMENT") : "");
        model.setBANK_FINANCED_PRINCIPAL(rs.getString("BANK_FINANCED_PRINCIPAL") != null ? rs.getString("BANK_FINANCED_PRINCIPAL") : "");
        model.setTENOR_WITH_BANK(rs.getString("TENOR_WITH_BANK") != null ? rs.getString("TENOR_WITH_BANK") : "");
        model.setINSTALLMENT_PAYMENT_DATE_1(rs.getString("INSTALLMENT_PAYMENT_DATE_1") != null ? rs.getString("INSTALLMENT_PAYMENT_DATE_1") : "");
        model.setFINAL_INSTALLMENT_DATE(rs.getString("FINAL_INSTALLMENT_DATE") != null ? rs.getString("FINAL_INSTALLMENT_DATE") : "");
        model.setINTEREST_AT_THE_BANK(rs.getString("INTEREST_AT_THE_BANK") != null ? rs.getString("INTEREST_AT_THE_BANK") : "");
        model.setAMOUNT_OF_INSTALLMENT_TO_BANK(rs.getString("AMOUNT_OF_INSTALLMENT_TO_BANK") != null ? rs.getString("AMOUNT_OF_INSTALLMENT_TO_BANK") : "");
        model.setINSTALLMENT_PERIOD(rs.getString("INSTALLMENT_PERIOD") != null ? rs.getString("INSTALLMENT_PERIOD") : "");
        model.setVEHICLE_CONDITION(rs.getString("VEHICLE_CONDITION") != null ? rs.getString("VEHICLE_CONDITION") : "");
        model.setTYPE_OF_CLOSING(rs.getString("TYPE_OF_CLOSING") != null ? rs.getString("TYPE_OF_CLOSING") : "");
        model.setINSURANCE_PREMIUM_AMOUNT(rs.getString("INSURANCE_PREMIUM_AMOUNT") != null ? rs.getString("INSURANCE_PREMIUM_AMOUNT") : "");
        model.setPREMIUM_PAYMENT(rs.getString("PREMIUM_PAYMENT") != null ? rs.getString("PREMIUM_PAYMENT") : "");
        model.setVEHICLE_CLASS(rs.getString("VEHICLE_CLASS") != null ? rs.getString("VEHICLE_CLASS") : "");
        model.setCREDIT_SCORING(rs.getString("CREDIT_SCORING") != null ? rs.getString("CREDIT_SCORING") : "");
        model.setDEBT_BURDEN_RATIO(rs.getString("DEBT_BURDEN_RATIO") != null ? rs.getString("DEBT_BURDEN_RATIO") : "");
        model.setEND_USER_LOAN_AGREEMENT_DATE(rs.getString("END_USER_LOAN_AGREEMENT_DATE") != null ? rs.getString("END_USER_LOAN_AGREEMENT_DATE") : "");
        model.setAMOUNT_PREMIUM_PAID(rs.getString("AMOUNT_PREMIUM_PAID") != null ? rs.getString("AMOUNT_PREMIUM_PAID") : "");
        model.setAMOUNT_DP(rs.getString("AMOUNT_DP") != null ? rs.getString("AMOUNT_DP") : "");
        model.setINSURANCE_COMPANY(rs.getString("INSURANCE_COMPANY") != null ? rs.getString("INSURANCE_COMPANY") : "");
        model.setAREA_OF_BUSINESS(rs.getString("AREA_OF_BUSINESS") != null ? rs.getString("AREA_OF_BUSINESS") : "");
        model.setPURPOSE_OF_USAGE(rs.getString("PURPOSE_OF_USAGE") != null ? rs.getString("PURPOSE_OF_USAGE") : "");
        model.setDOCUMENT_PROOF_OF_INCOME(rs.getString("DOCUMENT_PROOF_OF_INCOME") != null ? rs.getString("DOCUMENT_PROOF_OF_INCOME") : "");
        model.setPBB(rs.getString("PBB") != null ? rs.getString("PBB") : "");
        model.setSPOUSE_KTP(rs.getString("SPOUSE_KTP") != null ? rs.getString("SPOUSE_KTP") : "");
        model.setLICENCING(rs.getString("LICENCING") != null ? rs.getString("LICENCING") : "");
        model.setDEED(rs.getString("DEED") != null ? rs.getString("DEED") : "");
        model.setNPWP(rs.getString("NPWP") != null ? rs.getString("NPWP") : "");
        model.setFINANCING_APPLICATION_FORM(rs.getString("FINANCING_APPLICATION_FORM") != null ? rs.getString("FINANCING_APPLICATION_FORM") : "");
        model.setFIDUCIARY(rs.getString("FIDUCIARY") != null ? rs.getString("FIDUCIARY") : "");
        model.setCODE_IN_REAR(rs.getString("CODE_IN_REAR") != null ? rs.getString("CODE_IN_REAR") : "");
        return model;
    }
}
