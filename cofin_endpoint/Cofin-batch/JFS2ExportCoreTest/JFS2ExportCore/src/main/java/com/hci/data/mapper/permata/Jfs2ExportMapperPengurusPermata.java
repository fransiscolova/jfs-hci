/**
 *
 */
package com.hci.data.mapper.permata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.permata.Jfs2ExportPengurusPermata;

/**
 * Result mapper from query result to {@link Jfs2ExportPengurusPermata}.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportMapperPengurusPermata implements RowMapper<Jfs2ExportPengurusPermata> {

    @Override
    public Jfs2ExportPengurusPermata mapRow(ResultSet rs, int rowNum) throws SQLException {
        Jfs2ExportPengurusPermata model = new Jfs2ExportPengurusPermata();
        model.setNO_PIN(rs.getString("NO_PIN") != null ? rs.getString("NO_PIN") : "");
        model.setNAME_OF_MANAGER(rs.getString("NAME_OF_MANAGER") != null ? rs.getString("NAME_OF_MANAGER") : "");
        model.setID_CARD_OF_MANAGER(rs.getString("ID_CARD_OF_MANAGER") != null ? rs.getString("ID_CARD_OF_MANAGER") : "");
        model.setMANAGERS_TAX_REGISTRATION_NO(rs.getString("MANAGERS_TAX_REGISTRATION_NO") != null ? rs.getString("MANAGERS_TAX_REGISTRATION_NO") : "");
        model.setSEX(rs.getString("SEX") != null ? rs.getString("SEX") : "");
        model.setOWNERSHIP_PORTION(rs.getString("OWNERSHIP_PORTION") != null ? rs.getString("OWNERSHIP_PORTION") : "");
        model.setPOSITION_CODE(rs.getString("POSITION_CODE") != null ? rs.getString("POSITION_CODE") : "");
        model.setADDRESS(rs.getString("ADDRESS") != null ? rs.getString("ADDRESS") : "");
        model.setSUB_DISTRICT(rs.getString("SUB_DISTRICT") != null ? rs.getString("SUB_DISTRICT") : "");
        model.setKECAMATAN(rs.getString("KECAMATAN") != null ? rs.getString("KECAMATAN") : "");
        model.setDISTRICT_OF_DEBTOR(rs.getString("DISTRICT_OF_DEBTOR") != null ? rs.getString("DISTRICT_OF_DEBTOR") : "");
        model.setPOSTAL_CODE(rs.getString("POSTAL_CODE") != null ? rs.getString("POSTAL_CODE") : "");
        return model;
    }
}
