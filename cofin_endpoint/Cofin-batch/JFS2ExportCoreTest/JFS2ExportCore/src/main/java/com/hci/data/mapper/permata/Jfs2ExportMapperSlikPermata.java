/**
 *
 */
package com.hci.data.mapper.permata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.permata.Jfs2ExportSlikPermata;

/**
 * Result mapper from query result to {@link Jfs2ExportSlikPermata}.
 *
 * @author muhammad.muflihun
 *
 */
public class Jfs2ExportMapperSlikPermata implements RowMapper<Jfs2ExportSlikPermata> {

    @Override
    public Jfs2ExportSlikPermata mapRow(ResultSet rs, int rowNum) throws SQLException {
        Jfs2ExportSlikPermata model = new Jfs2ExportSlikPermata();
        model.setNO_PIN(rs.getString("NO_PIN") != null ? rs.getString("NO_PIN") : "");
        model.setFINANCING_CREDIT_CODE(rs.getString("FINANCING_CREDIT_CODE") != null ? rs.getString("FINANCING_CREDIT_CODE") : "");
        model.setTYPE_OF_CREDIT(rs.getString("TYPE_OF_CREDIT") != null ? rs.getString("TYPE_OF_CREDIT") : "");
        model.setSKIM_CODE(rs.getString("SKIM_CODE") != null ? rs.getString("SKIM_CODE") : "");
        model.setNEW(rs.getString("NEW") != null ? rs.getString("NEW") : "");
        model.setTYPE_OF_USE_CODE_(rs.getString("TYPE_OF_USE_CODE_") != null ? rs.getString("TYPE_OF_USE_CODE_") : "");
        model.setORIENTATION_OF_USE_CODE(rs.getString("ORIENTATION_OF_USE_CODE") != null ? rs.getString("ORIENTATION_OF_USE_CODE") : "");
        model.setTAKE_OVER_FROM(rs.getString("TAKE_OVER_FROM") != null ? rs.getString("TAKE_OVER_FROM") : "");
        model.setEMAIL_ADDRESS(rs.getString("EMAIL_ADDRESS") != null ? rs.getString("EMAIL_ADDRESS") : "");
        model.setMOBILE_PHONE_NUMBER(rs.getString("MOBILE_PHONE_NUMBER") != null ? rs.getString("MOBILE_PHONE_NUMBER") : "");
        model.setWORKING_ADDRESS(rs.getString("WORKING_ADDRESS") != null ? rs.getString("WORKING_ADDRESS") : "");
        model.setGROSS_INCOME_PER_YEAR(rs.getString("GROSS_INCOME_PER_YEAR") != null ? rs.getString("GROSS_INCOME_PER_YEAR") : "");
        model.setSOURCE_OF_INCOME_(rs.getString("SOURCE_OF_INCOME_") != null ? rs.getString("SOURCE_OF_INCOME_") : "");
        model.setNUMBER_OF_DEPENDENTS(rs.getString("NUMBER_OF_DEPENDENTS") != null ? rs.getString("NUMBER_OF_DEPENDENTS") : "");
        model.setSPOUSE_NAME(rs.getString("SPOUSE_NAME") != null ? rs.getString("SPOUSE_NAME") : "");
        model.setSPOUSE_ID(rs.getString("SPOUSE_ID") != null ? rs.getString("SPOUSE_ID") : "");
        model.setSPOUSE_DATE_OF_BIRTH(rs.getString("SPOUSE_DATE_OF_BIRTH") != null ? rs.getString("SPOUSE_DATE_OF_BIRTH") : "");
        model.setPERJANJIAN_PISAH_HARTA(rs.getString("PERJANJIAN_PISAH_HARTA") != null ? rs.getString("PERJANJIAN_PISAH_HARTA") : "");
        model.setKODE_BIDANG_USAHA(rs.getString("KODE_BIDANG_USAHA") != null ? rs.getString("KODE_BIDANG_USAHA") : "");
        model.setBUSINESS_ENTITY_CODE(rs.getString("BUSINESS_ENTITY_CODE") != null ? rs.getString("BUSINESS_ENTITY_CODE") : "");
        model.setDEBTOR_GROUP_CODE(rs.getString("DEBTOR_GROUP_CODE") != null ? rs.getString("DEBTOR_GROUP_CODE") : "");
        model.setGO_PUBLIC(rs.getString("GO_PUBLIC") != null ? rs.getString("GO_PUBLIC") : "");
        model.setDEBTOR_RATING(rs.getString("DEBTOR_RATING") != null ? rs.getString("DEBTOR_RATING") : "");
        model.setRATING_AGENCIES(rs.getString("RATING_AGENCIES") != null ? rs.getString("RATING_AGENCIES") : "");
        model.setDATE_OF_RATING(rs.getString("DATE_OF_RATING") != null ? rs.getString("DATE_OF_RATING") : "");
        model.setDEBTOR_GROUP_NAME(rs.getString("DEBTOR_GROUP_NAME") != null ? rs.getString("DEBTOR_GROUP_NAME") : "");
        model.setTYPES_OF_COLLATERAL_CODE(rs.getString("TYPES_OF_COLLATERAL_CODE") != null ? rs.getString("TYPES_OF_COLLATERAL_CODE") : "");
        model.setTYPE_OF_BONDING_CODE(rs.getString("TYPE_OF_BONDING_CODE") != null ? rs.getString("TYPE_OF_BONDING_CODE") : "");
        model.setBINDING_DATE(rs.getString("BINDING_DATE") != null ? rs.getString("BINDING_DATE") : "");
        model.setCOLLATERAL_NAME(rs.getString("COLLATERAL_NAME") != null ? rs.getString("COLLATERAL_NAME") : "");
        model.setCOLLATERAL_ADDRESS(rs.getString("COLLATERAL_ADDRESS") != null ? rs.getString("COLLATERAL_ADDRESS") : "");
        model.setJOINT_CREDIT_STATUS(rs.getString("JOINT_CREDIT_STATUS") != null ? rs.getString("JOINT_CREDIT_STATUS") : "");
        model.setREGENCY_CODE(rs.getString("REGENCY_CODE") != null ? rs.getString("REGENCY_CODE") : "");
        model.setINSURANCE(rs.getString("INSURANCE") != null ? rs.getString("INSURANCE") : "");
        return model;
    }
}
