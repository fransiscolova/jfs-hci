package com.hci.job.interfaces;

import java.util.Date;
import java.text.SimpleDateFormat;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.hci.data.dao.ParentJobDao;
import com.hci.job.sub.btpn.JFS2ExportBarangBtpnSub;
import com.hci.job.sub.btpn.JFS2ExportEnduserBtpnSub;
import com.hci.job.sub.btpn.JFS2ExportKontrakBtpnSub;
import com.hci.job.sub.btpn.JFS2ExportPengurusBtpnSub;
import com.hci.job.sub.btpn.JFS2ExportSlikBtpnSub;
import com.hci.job.sub.permata.JFS2ExportBarangPermataSub;
import com.hci.job.sub.permata.JFS2ExportEnduserPermataSub;
import com.hci.job.sub.permata.JFS2ExportKontrakPermataSub;
import com.hci.job.sub.permata.JFS2ExportPengurusPermataSub;
import com.hci.job.sub.permata.JFS2ExportSlikPermataSub;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFS2ExportCore implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private ParentJobDao parentDao;
	private String targetFol;
	private String jobName;
	private String processName;
	
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		if(checkHoliday()) {
			log.info("JFS2ExportCore", "Job skipped, holiday");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(!checkParentJob(processName)) {
			log.info("JFS2ExportCore", "Job skipped, parent job not run");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(checkTodayRunJob(processName)) {
			log.info("JFS2ExportCore", "Job skipped, already runned");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		log.info("JFS2ExportCore", "Job not skipped");
		
		setForceRunToN(processName);

		processGenerateFilePermata(uuid);
		processGenerateFileBtpn(uuid);
		
		log.info("JFS2ExportCore", "Job done");
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}
    
    private boolean processGenerateFilePermata(String uuid) {
        boolean paymentFileStatus = false;
        paymentFileStatus = new JFS2ExportEnduserPermataSub().createFile(targetFol, uuid, "", 0, "", processName);
        paymentFileStatus = new JFS2ExportKontrakPermataSub().createFile(targetFol, uuid, "", 0, "", processName);
        paymentFileStatus = new JFS2ExportBarangPermataSub().createFile(targetFol, uuid, "", 0, "", processName);
        paymentFileStatus = new JFS2ExportPengurusPermataSub().createFile(targetFol, uuid, "", 0, "", processName);
        paymentFileStatus = new JFS2ExportSlikPermataSub().createFile(targetFol, uuid, "", 0, "", processName);
        return paymentFileStatus;
    }
	
	private boolean processGenerateFileBtpn(String uuid) {
	    boolean paymentFileStatus = false;
	    paymentFileStatus = new JFS2ExportEnduserBtpnSub().createFile(targetFol, uuid, "", 0, "", processName);
        paymentFileStatus = new JFS2ExportKontrakBtpnSub().createFile(targetFol, uuid, "", 0, "", processName);
        paymentFileStatus = new JFS2ExportBarangBtpnSub().createFile(targetFol, uuid, "", 0, "", processName);
        paymentFileStatus = new JFS2ExportPengurusBtpnSub().createFile(targetFol, uuid, "", 0, "", processName);
        paymentFileStatus = new JFS2ExportSlikBtpnSub().createFile(targetFol, uuid, "", 0, "", processName);
        return paymentFileStatus;
	}
	
	public boolean checkHoliday() {
		return parentDao.checkHoliday();
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void setForceRunToN(String processName) {
		parentDao.setForceRunNChildJob(processName);
	}
	
	public boolean checkTodayRunJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
}