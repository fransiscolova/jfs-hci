package com.hci.job.interfaces;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.hci.util.LoggerUtil;

public class JFS2ExportCoreComplete implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	private String processName;
	
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		return RepeatStatus.FINISHED;
	}
}