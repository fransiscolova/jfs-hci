package com.hci.job.sub.btpn;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.hci.constant.BatchConstant;
import com.hci.data.dao.Jfs2ExportEnduserDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.data.entity.btpn.Jfs2ExportEnduserBtpn;
import com.hci.job.sub.SubJobParent;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFS2ExportEnduserBtpnSub extends SubJobParent {
    private LoggerUtil log = new LoggerUtil();
    private ApplicationContext appContext;
    private Jfs2ExportEnduserDao jfs2ExportEnduserDao;
    
    public JFS2ExportEnduserBtpnSub() {
        appContext = SpringContextHolder.getApplicationContext();
        jfs2ExportEnduserDao = (Jfs2ExportEnduserDao) appContext.getBean("jfs2ExportEnduserBtpnDao");
    }
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> createPaymentFileData(String header, String runDate) {
        Map<String, Object> extractedValue = new HashMap<String, Object>();
        Map<String, String> partnerAgreement = new HashMap<String, String>();
        List<String> paymentDetailDataString = new ArrayList<String>();
        
        paymentDetailDataString.add(header);
        int numOfRow = 0;
        
        List<Jfs2ExportEnduserBtpn> lists;
        if (runDate.equals("")) {
            lists = (List<Jfs2ExportEnduserBtpn>) jfs2ExportEnduserDao.getJfs2ExporEndusertData();
        } else {
            lists = (List<Jfs2ExportEnduserBtpn>) jfs2ExportEnduserDao.getJfs2ExporEndusertData(runDate);
        }
        
        log.info("JFS2ExportEnduserBtpnSub", "Num of file lines : " + lists.size());
        for(Jfs2ExportEnduserBtpn jp : lists) {
            if(!partnerAgreement.containsKey(jp.getAgreementId())) {
                partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
            }
            
            paymentDetailDataString.add(jp.getContractCode() + "|" + jp.getFullName() + "|" + jp.getNickname() + "|" + jp.getDstCode() + "|" + jp.getDescription() + "|" + jp.getCode8139() + "|" + jp.getGenderType() + "|" + jp.getKtpNum() + "|" +
                    jp.getNum() + "|" + jp.getBirthPlace() + "|" + jp.getBirth() + "|" + jp.getStreetName() + "|" + jp.getFullAddress() + "|" + jp.getVillage() + "|" + jp.getSubDistrict() + "|" + jp.getDestCodeXD() + "|" +
                    jp.getZipCode() + "|" + jp.getValue1() + "|" + jp.getId() + "|" + jp.getName4() + "|" + jp.getEmployerIndustryCode() + "|" + jp.getNameEmployer() + "|" + jp.getDstCodeXI() + "|" + jp.getOne() + "|" + jp.getDstCodeXM() + "|" + jp.getAid() + "|" + jp.getDstCodeXR() + "|" + jp.getIncome() + "|" + jp.getValidTo());
            numOfRow++;
        }
        paymentDetailDataString.add("HCI|" + numOfRow + "|0");
        extractedValue.put("fileData", paymentDetailDataString);
        extractedValue.put("agreementPartner", partnerAgreement);
        return extractedValue;
    }
    
    @SuppressWarnings("unchecked")
    public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
        log.info("JFS2ExportEnduserBtpnSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + 
                " runDate : " + runDate + " numOfRerun : " + numOfRerun + " rerunStatus : " 
                + rerunStatus + " processName : " + processName);
        
        DateUtil dateUtil = new DateUtil();
        Date startJobTime = new Date();
        
        String fileName = targetFol + "ENDUSER_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
        String aesFileName = targetFol + "ENDUSER_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
        
        Map<String, Object> dataToWrite = createPaymentFileData("HCI|1|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
        
        ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
        
        String jobStatus = BatchConstant.Common.SUCCESS;
        jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
        jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
        if (runDate.equals("")) {
            if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
                super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
            } else {
                super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
            }
        } else {
            super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
        }
        
        log.info("JFS2ExportEnduserBtpnSub", "JobStatus : " + jobStatus);
        log.info("JFS2ExportEnduserBtpnSub", "Created file : " + aesFileName);
        return jobStatus.equals(BatchConstant.Common.SUCCESS);
    }
}
