package com.hci.job.sub.btpn;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.hci.constant.BatchConstant;
import com.hci.data.dao.Jfs2ExportKontrakDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.data.entity.btpn.Jfs2ExportKontrakBtpn;
import com.hci.data.entity.btpn.Jfs2ExportKontrakFooterBtpn;
import com.hci.job.sub.SubJobParent;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFS2ExportKontrakBtpnSub extends SubJobParent {
    private LoggerUtil log = new LoggerUtil();
    private ApplicationContext appContext;
    private Jfs2ExportKontrakDao jfs2ExportKontrakDao;
    
    public JFS2ExportKontrakBtpnSub() {
        appContext = SpringContextHolder.getApplicationContext();
        jfs2ExportKontrakDao = (Jfs2ExportKontrakDao) appContext.getBean("jfs2ExportKontrakBtpnDao");
    }
	
	@SuppressWarnings("unchecked")
    public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		List<Jfs2ExportKontrakBtpn> lists;
		List<Jfs2ExportKontrakFooterBtpn> footers;
		if (runDate.equals("")) {
			lists = (List<Jfs2ExportKontrakBtpn>) jfs2ExportKontrakDao.getJfs2ExporKontrakData();
			footers = (List<Jfs2ExportKontrakFooterBtpn>) jfs2ExportKontrakDao.getJfs2ExportKontrakFooterData();
		} else {
			lists = (List<Jfs2ExportKontrakBtpn>) jfs2ExportKontrakDao.getJfs2ExporKontrakData();
			footers = (List<Jfs2ExportKontrakFooterBtpn>) jfs2ExportKontrakDao.getJfs2ExportKontrakFooterData(runDate);
		}
		
		log.info("JFS2ExportKontrakSub", "Num of file lines : " + lists.size());
		log.info("JFS2ExportKontrakSub", "Num of footer file lines : " + footers.size());
		for(Jfs2ExportKontrakBtpn jp : lists) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getContractCode() + "|" + jp.getFullName() + "|" + jp.getCode() + "|" + jp.getContractCode1() + "|" + jp.getCreationDate() + "|" + jp.getCreditAmount() + 
					"|" + jp.getGoodsPriceAmount() + "|" + jp.getTerms() + "|" + jp.getAnnuityAmount() + "|" + jp.getInterestRate() + "|" + jp.getCreditAmount() + "|" + jp.getSendPrincipal() + "|" + jp.getTerms() + "|" + jp.getDueDate() + "|" + jp.getI1DueDate() + "|" + jp.getSendRate() + "|" + jp.getSendInstalment() + 
					"|1|N|T|0|E|DG|0||" + jp.getCreationDate() + "|0|" + jp.getPayment() + "||" + jp.getBiEconomicSectorCode() + "|K|||||||||||||");
		}
		
		for(Jfs2ExportKontrakFooterBtpn jp : footers) {
			paymentDetailDataString.add("HCI|" + jp.getNumOfRows() + "|" + jp.getCreditAmount());
		}
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	@SuppressWarnings("unchecked")
    public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportKontrakSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + 
		        " runDate : " + runDate + " nnumOfRerun : " + numOfRerun + " rerunStatus : " 
		        + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "KONTRAK_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "KONTRAK_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|2|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = BatchConstant.Common.SUCCESS;
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportKontrakSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportKontrakSub", "Created file : " + aesFileName);
		return jobStatus.equals(BatchConstant.Common.SUCCESS);
	}
}
