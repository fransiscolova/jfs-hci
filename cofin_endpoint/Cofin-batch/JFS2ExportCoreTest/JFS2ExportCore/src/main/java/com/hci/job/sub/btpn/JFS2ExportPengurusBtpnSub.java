package com.hci.job.sub.btpn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;

import com.hci.constant.BatchConstant;
import com.hci.data.dao.Jfs2ExportPengurusDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.data.entity.btpn.Jfs2ExportPengurusBtpn;
import com.hci.job.sub.SubJobParent;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFS2ExportPengurusBtpnSub extends SubJobParent {
    private LoggerUtil log = new LoggerUtil();
    private ApplicationContext appContext;
    private Jfs2ExportPengurusDao jfs2ExportPengurusDao;
    
    public JFS2ExportPengurusBtpnSub() {
        appContext = SpringContextHolder.getApplicationContext();
        jfs2ExportPengurusDao = (Jfs2ExportPengurusDao) appContext.getBean("jfs2ExportPengurusBtpnDao");
    }
	
	@SuppressWarnings("unchecked")
    public List<String> createPaymentFileData(String header, String runDate) {
		List<String> paymentDataString = new ArrayList<String>();
		paymentDataString.add(header);
		int numOfRow = 0;
		
		List<Jfs2ExportPengurusBtpn> lists;
		if (runDate.equals("")) {
			lists = (List<Jfs2ExportPengurusBtpn>) jfs2ExportPengurusDao.getJfs2ExporPengurusData();
		} else {
			lists = (List<Jfs2ExportPengurusBtpn>) jfs2ExportPengurusDao.getJfs2ExporPengurusData(runDate);
		}
		
		log.info("JFS2ExportPengurusSub", "Num of file lines : " + lists.size());
		for(Jfs2ExportPengurusBtpn jp : lists) {
			paymentDataString.add(jp.getContractCode() + "||||||||||||||");
			numOfRow++;
		}
		paymentDataString.add("HCI|" + numOfRow + "|0");
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportPengurusSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + 
		        " runDate : " + runDate + " nnumOfRerun : " + numOfRerun + " rerunStatus : " 
		        + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "PENGURUS_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "PENGURUS_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		List<String> dataToWrite = createPaymentFileData("HCI|4|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if(dataToWrite.size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = BatchConstant.Common.SUCCESS;
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus,  aesFileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportPengurusSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportPengurusSub", "Created file : " + aesFileName);
		return jobStatus.equals(BatchConstant.Common.SUCCESS);
	}
}
