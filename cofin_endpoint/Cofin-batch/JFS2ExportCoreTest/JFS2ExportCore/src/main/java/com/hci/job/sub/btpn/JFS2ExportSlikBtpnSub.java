package com.hci.job.sub.btpn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;

import com.hci.constant.BatchConstant;
import com.hci.data.dao.Jfs2ExportSlikDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.data.entity.btpn.Jfs2ExportSlikBtpn;
import com.hci.job.sub.SubJobParent;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFS2ExportSlikBtpnSub extends SubJobParent {
    private LoggerUtil log = new LoggerUtil();
    private ApplicationContext appContext;
    private Jfs2ExportSlikDao jfs2ExportSlikDao;
    
    public JFS2ExportSlikBtpnSub() {
        appContext = SpringContextHolder.getApplicationContext();
        jfs2ExportSlikDao = (Jfs2ExportSlikDao) appContext.getBean("jfs2ExportSlikBtpnDao");                
    }
	
	@SuppressWarnings("unchecked")
    public List<String> createPaymentFileData(String header, String runDate) {
		List<String> paymentDataString = new ArrayList<String>();
		int numOfRow = 0;
		
		List<Jfs2ExportSlikBtpn> lists;
		if (runDate.equals("")) {
			lists = (List<Jfs2ExportSlikBtpn>) jfs2ExportSlikDao.getJfs2ExporSlikData();
		} else {
			lists = (List<Jfs2ExportSlikBtpn>) jfs2ExportSlikDao.getJfs2ExporSlikData(runDate);
		}
		
		log.info("JFS2ExportSlikSub", "Num of file lines : " + lists.size());
		for(Jfs2ExportSlikBtpn jp : lists) {
			paymentDataString.add(jp.getTextContractNumber() + "|" + jp.getKodeSifatKreditPemiayaan() + "|" + jp.getKodeJenisKredit() + "|" + jp.getKodeSkim() + "|" + 
					jp.getBaruPerpanjangan() + "|" + jp.getKodeJenisPenggunaan() + "|" + jp.getKodeOrientasiPenggunaan() + "|" + jp.getTakeoverDari() + "|" + 
					jp.getAlamatEmail() + "|" + jp.getNoTeleponSelular() + "|" + jp.getAlamatTempatBekerja() + "|" + jp.getPenghasilanKotorPerTahun() + "|" +  
					jp.getKodeSumberPenghasilan() + "|" + jp.getJumlahTanggungan() + "|" + jp.getNamaPasangan() + "|" + jp.getNoIdentitasPasangan() + "|" + 
					jp.getTanggalIdentitasPasangan() + "|" + jp.getPerjanjianPisahHarta() + "|" + jp.getKodeBidangUsaha() + "|" + jp.getKodeGolonganDebitur() + "|" + 
					jp.getKodeBentukBadanUsaha() + "|" + jp.getGoPublic() + "|" + jp.getPeringkatRatingDebitur() + "|" + jp.getLembagaPemeringkatRating() + "|" + 
					jp.getTanggalPemeringkat() + "|" +  jp.getNamaGroupDebitur() + "|" + jp.getKodeJenisAgunan() + "|" + jp.getKodeJenisPengikatan() + "|" + 
					jp.getTanggalPengikatan() + "|" + jp.getNamaPemilikAgunan() + "|" + jp.getAlamatAgunan() + "|" + jp.getStatusKreditJoin() + "|" + 
					jp.getDati2() + "|" + jp.getDiasuransikan());
			numOfRow++;
		}
		paymentDataString.add("HCI|" + numOfRow + "|0");
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportSlikSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + 
		        " runDate : " + runDate + " nnumOfRerun : " + numOfRerun + " rerunStatus : " 
		        + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "SLIK_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "SLIK_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		List<String> dataToWrite = createPaymentFileData("HCI|31|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if(dataToWrite.size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = BatchConstant.Common.SUCCESS;
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus,  aesFileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportSlikSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportSlikSub", "Created file : " + aesFileName);
		return jobStatus.equals(BatchConstant.Common.SUCCESS);
	}
}
