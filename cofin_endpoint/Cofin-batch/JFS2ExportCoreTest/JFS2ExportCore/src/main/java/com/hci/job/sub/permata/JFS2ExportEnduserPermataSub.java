package com.hci.job.sub.permata;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.hci.constant.BatchConstant;
import com.hci.data.dao.Jfs2ExportEnduserDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.data.entity.permata.Jfs2ExportEnduserPermata;
import com.hci.job.sub.SubJobParent;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

/**
 * Sub class for generating end user txt for permata.
 * 
 * @author muhammad.muflihun
 *
 */
public class JFS2ExportEnduserPermataSub extends SubJobParent {
    private LoggerUtil log = new LoggerUtil();
    private ApplicationContext appContext;
    private Jfs2ExportEnduserDao jfs2ExportEnduserDao;
    
    public JFS2ExportEnduserPermataSub() {
        appContext = SpringContextHolder.getApplicationContext();
        jfs2ExportEnduserDao = (Jfs2ExportEnduserDao) appContext.getBean("jfs2ExportEnduserPermataDao");
    }
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> createPaymentFileData(String header, String runDate) {
        Map<String, Object> extractedValue = new HashMap<String, Object>();
        Map<String, String> partnerAgreement = new HashMap<String, String>();
        List<String> paymentDetailDataString = new ArrayList<String>();
        
        List<Jfs2ExportEnduserPermata> list;
        if (runDate.equals("")) {
            list = (List<Jfs2ExportEnduserPermata>) jfs2ExportEnduserDao.getJfs2ExporEndusertData();
        } else {
            list = (List<Jfs2ExportEnduserPermata>) jfs2ExportEnduserDao.getJfs2ExporEndusertData(runDate);
        }
        
        log.info("JFS2ExportEnduserPermataSub", "Num of file lines : " + list.size());
        partnerAgreement.put("51", "PERMATA");
        for(Jfs2ExportEnduserPermata enduser : list) {
            paymentDetailDataString.add(enduser.toString());
        }
        extractedValue.put("fileData", paymentDetailDataString);
        extractedValue.put("agreementPartner", partnerAgreement);
        return extractedValue;
    }
    
    @SuppressWarnings("unchecked")
    public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
        log.info("JFS2ExportEnduserPermataSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + 
                " runDate : " + runDate + " numOfRerun : " + numOfRerun + " rerunStatus : " 
                + rerunStatus + " processName : " + processName);
        
        DateUtil dateUtil = new DateUtil();
        Date startJobTime = new Date();
        
        String fileName = targetFol + "[PERMATA]ENDUSER_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
        
        Map<String, Object> dataToWrite = createPaymentFileData(null, runDate);
        List<String> fileData = (List<String>) dataToWrite.get("fileData");
        if (fileData.isEmpty()) {
            return false;
        }
        
        ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
        
        String jobStatus = BatchConstant.Common.SUCCESS;
        jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, fileData);
        if (runDate.equals("")) {
            if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
                super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
            } else {
                super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, fileName);
            }
        } else {
            super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
        }
        
        log.info("JFS2ExportEnduserPermataSub", "JobStatus : " + jobStatus);
        log.info("JFS2ExportEnduserPermataSub", "Created file : " + fileName);
        return jobStatus.equals(BatchConstant.Common.SUCCESS);
    }
}
