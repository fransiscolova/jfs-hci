package com.hci.job.sub.permata;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.hci.constant.BatchConstant;
import com.hci.data.dao.Jfs2ExportKontrakDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.data.entity.permata.Jfs2ExportKontrakPermata;
import com.hci.job.sub.SubJobParent;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

/**
 * Sub class for generating kontrak txt for permata.
 * 
 * @author muhammad.muflihun
 *
 */
public class JFS2ExportKontrakPermataSub extends SubJobParent {
    private LoggerUtil log = new LoggerUtil();
    private ApplicationContext appContext;
    private Jfs2ExportKontrakDao jfs2ExportKontrakDao;
    
    public JFS2ExportKontrakPermataSub() {
        appContext = SpringContextHolder.getApplicationContext();
        jfs2ExportKontrakDao = (Jfs2ExportKontrakDao) appContext.getBean("jfs2ExportKontrakPermataDao");
    }
	
	@SuppressWarnings("unchecked")
    public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		List<Jfs2ExportKontrakPermata> list;
		if (runDate.equals("")) {
			list = (List<Jfs2ExportKontrakPermata>) jfs2ExportKontrakDao.getJfs2ExporKontrakData();
		} else {
			list = (List<Jfs2ExportKontrakPermata>) jfs2ExportKontrakDao.getJfs2ExporKontrakData();
		}
		
		log.info("JFS2ExportKontrakPermataSub", "Num of file lines : " + list.size());
		partnerAgreement.put("51", "PERMATA");
		for(Jfs2ExportKontrakPermata kontrak : list) {
			paymentDetailDataString.add(kontrak.toString());
		}
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	@SuppressWarnings("unchecked")
    public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportKontrakPermataSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + 
		        " runDate : " + runDate + " nnumOfRerun : " + numOfRerun + " rerunStatus : " 
		        + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "KONTRAK_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		
		Map<String, Object> dataToWrite = createPaymentFileData(null, runDate);
		List<String> fileData = (List<String>) dataToWrite.get("fileData");
        if (fileData.isEmpty()) {
            return false;
        }
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = BatchConstant.Common.SUCCESS;
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, fileData);
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, fileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportKontrakPermataSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportKontrakPermataSub", "Created file : " + fileName);
		return jobStatus.equals(BatchConstant.Common.SUCCESS);
	}
}
