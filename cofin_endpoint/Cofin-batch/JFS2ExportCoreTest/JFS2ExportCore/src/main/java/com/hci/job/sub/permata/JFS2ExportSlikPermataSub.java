package com.hci.job.sub.permata;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.hci.constant.BatchConstant;
import com.hci.data.dao.Jfs2ExportSlikDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.data.entity.permata.Jfs2ExportSlikPermata;
import com.hci.job.sub.SubJobParent;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

/**
 * Sub class for generating slik txt for permata.
 * 
 * @author muhammad.muflihun
 *
 */
public class JFS2ExportSlikPermataSub extends SubJobParent {
    private LoggerUtil log = new LoggerUtil();
    private ApplicationContext appContext;
    private Jfs2ExportSlikDao jfs2ExportSlikDao;
    
    public JFS2ExportSlikPermataSub() {
        appContext = SpringContextHolder.getApplicationContext();
        jfs2ExportSlikDao = (Jfs2ExportSlikDao) appContext.getBean("jfs2ExportSlikPermataDao");                
    }
	
	@SuppressWarnings("unchecked")
    public Map<String, Object> createPaymentFileData(String header, String runDate) {
	    Map<String, Object> extractedValue = new HashMap<String, Object>();
        Map<String, String> partnerAgreement = new HashMap<String, String>();
        List<String> paymentDetailDataString = new ArrayList<String>();
		
		List<Jfs2ExportSlikPermata> list;
		if (runDate.equals("")) {
			list = (List<Jfs2ExportSlikPermata>) jfs2ExportSlikDao.getJfs2ExporSlikData();
		} else {
			list = (List<Jfs2ExportSlikPermata>) jfs2ExportSlikDao.getJfs2ExporSlikData(runDate);
		}
		
		log.info("JFS2ExportSlikPermataSub", "Num of file lines : " + list.size());
		partnerAgreement.put("51", "PERMATA");
		for(Jfs2ExportSlikPermata slik : list) {
		    paymentDetailDataString.add(slik.toString());
		}
		extractedValue.put("fileData", paymentDetailDataString);
        extractedValue.put("agreementPartner", partnerAgreement);
        return extractedValue;
	}
	
	@SuppressWarnings("unchecked")
    public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportSlikPermataSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + 
		        " runDate : " + runDate + " nnumOfRerun : " + numOfRerun + " rerunStatus : " 
		        + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "[PERMATA]SLIK_ALL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		
		Map<String, Object> dataToWrite = createPaymentFileData(null, runDate);
		List<String> fileData = (List<String>) dataToWrite.get("fileData");
        if (fileData.isEmpty()) {
            return false;
        }
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = BatchConstant.Common.SUCCESS;
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, fileData);
		jobStatus = super.aescrypt(shellCObj, fileName, fileName);
		
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus,  fileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportSlikPermataSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportSlikPermataSub", "Created file : " + fileName);
		return jobStatus.equals(BatchConstant.Common.SUCCESS);
	}
}
