package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.Jfs2AllExport;
import com.hci.data.entity.Jfs2ExportOptOut;

public interface Jfs2AllExportDao {
	public List<Jfs2AllExport> getJfs2AllExportData();
	public List<Jfs2AllExport> getJfs2AllExportData(String runDate);
}
