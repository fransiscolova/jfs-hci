package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.Jfs2ExportOptOut;

public interface Jfs2ExportOptOutDao {
	public List<Jfs2ExportOptOut> getJfs2ExportOptOutData();
	public List<Jfs2ExportOptOut> getJfs2ExportOptOutData(String runDate);
}
