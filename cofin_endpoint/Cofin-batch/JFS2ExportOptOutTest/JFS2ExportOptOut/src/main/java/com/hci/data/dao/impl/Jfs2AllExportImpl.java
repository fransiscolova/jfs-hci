package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2AllExportDao;
import com.hci.data.dao.Jfs2ExportOptOutDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.Jfs2AllExport;
import com.hci.data.entity.Jfs2ExportOptOut;
import com.hci.data.mapper.Jfs2AllExportMapper;
import com.hci.data.mapper.Jfs2ExportOptOutMapper;

public class Jfs2AllExportImpl implements Jfs2AllExportDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public Jfs2AllExportImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Jfs2AllExport> getJfs2AllExportData() {
		String SQL = "select eligibility_type, to_char(date_signature_contract,'DD-MON-YYYY') as date_signature_contract, " +
				"bank_financing_portion, hcid_financing_portion, cnt_contract " +
				"from jfs_contract_eligibility where trunc(dtime_inserted)=trunc(sysdate) and nvl(id_agreement,0) <4"; 
		List<Jfs2AllExport> jfsPayment = jdbcTemplateObject.query(SQL, new Jfs2AllExportMapper());
		return jfsPayment;
	}
	
	@Override
	public List<Jfs2AllExport> getJfs2AllExportData(String runDate) {
		String SQL = "select eligibility_type, to_char(date_signature_contract,'DD-MON-YYYY') as date_signature_contract, " +
				"bank_financing_portion, hcid_financing_portion, cnt_contract " +
				"from jfs_contract_eligibility where trunc(dtime_inserted)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and nvl(id_agreement,0) <4"; 
		List<Jfs2AllExport> jfsPayment = jdbcTemplateObject.query(SQL, new Jfs2AllExportMapper());
		return jfsPayment;
	}
}
