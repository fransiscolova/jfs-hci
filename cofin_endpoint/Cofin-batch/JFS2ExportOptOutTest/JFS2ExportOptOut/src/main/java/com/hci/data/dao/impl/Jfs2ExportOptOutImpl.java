package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.Jfs2ExportOptOutDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.Jfs2ExportOptOut;
import com.hci.data.mapper.Jfs2ExportOptOutMapper;

public class Jfs2ExportOptOutImpl implements Jfs2ExportOptOutDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public Jfs2ExportOptOutImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Jfs2ExportOptOut> getJfs2ExportOptOutData() {
		String SQL = "select distinct c.contract_code, trim(d.full_name) as CUSTOMER_NAME,cst.creation_date as DATE_SIGNATURE_CONTRACT,round(a.principal_split_rate*fp.credit_amount,0) as BANK_CREDIT_AMOUNT_PORTION " +
				"from jfs_contract_eligibility e left join (select distinct creation_date, contract_id from GTT_JSF_001 where CONTRACT_STATUS_TRANS = 'N') cst on trunc(cst.creation_date)=e.date_signature_contract " +
				"left join (select distinct id, contract_code,PRODUCT_CODE, deal_code,CONTRACT_STATUS from GTT_JSF_002) c on c.id = cst.contract_id " +
				"left join (select distinct credit_amount, contract_id from GTT_JSF_003) fp on c.id = fp.contract_id " +
				"left join (select * from jfs_product_mapping pm " +
				"inner join (select * from jfs_agreement WHERE REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on ja.code = pm.id_agreement " +
				"inner join jfs_product jp on ja.code=jp.id_agreement and pm.bank_product_code=jp.bank_product_code and trunc(sysdate)<=trunc(jp.valid_to) and trunc(sysdate)>=trunc(jp.valid_from) " +
				"left join jfs_partner jp on ja.partner_id = jp.id " +
				"where ja.partner_id='001' and to_number(ja.code, '99') < 4 " +
				") a on c.PRODUCT_CODE = a.code_product " +
				"and trunc(cst.creation_date) between trunc(a.valid_from) and trunc(a.valid_to) " +
				"left join (select distinct code, dbl.full_name from gtt_jsf_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id) d on c.deal_code = d.code " +
				"left join jfs_contract jc on jc.text_contract_number=c.contract_code and jc.id_agreement <4 " +
				"where trunc(e.dtime_inserted)=trunc(sysdate) and e.eligibility_type='OPT-OUT1' and c.CONTRACT_STATUS in ('A','N','Z') " +
				"and jc.text_contract_number is null order by cst.creation_date";
//		String SQL = "select distinct c.contract_code, trim(d.full_name) as CUSTOMER_NAME,cst.creation_date as DATE_SIGNATURE_CONTRACT,round(a.principal_split_rate*fp.credit_amount,0) as BANK_CREDIT_AMOUNT_PORTION " +
//				"from jfs_contract_eligibility e left join GTT_JSF_001 cst on cst.CONTRACT_STATUS_TRANS = 'N' and trunc(cst.creation_date)=e.date_signature_contract " +
//				"left join (select distinct id, contract_code,PRODUCT_CODE, deal_code,CONTRACT_STATUS from GTT_JSF_002) c on c.id = cst.contract_id " +
//				"left join GTT_JSF_003 fp on c.id = fp.contract_id left join (select * from jfs_product_mapping pm " +
//				"inner join (select * from jfs_agreement WHERE REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on ja.code = pm.id_agreement " +
//				"inner join jfs_product jp on ja.code=jp.id_agreement and pm.bank_product_code=jp.bank_product_code and trunc(sysdate)<=trunc(jp.valid_to) " +
//				"and trunc(sysdate)>=trunc(jp.valid_from) left join jfs_partner jp on ja.partner_id = jp.id " +
//				"where ja.partner_id='001' and to_number(ja.code, '99') < 4) a on c.PRODUCT_CODE = a.code_product " +
//				"and trunc(cst.creation_date) between trunc(a.valid_from) and trunc(a.valid_to) " +
//				"left join (select distinct code, full_name from GTT_JSF_005) d on c.deal_code = d.code " +
//				"left join jfs_contract jc on jc.text_contract_number=c.contract_code and jc.id_agreement <4 " +
//				"where trunc(e.dtime_inserted)=trunc(sysdate) and e.eligibility_type='OPT-OUT1' and c.CONTRACT_STATUS in ('A','N','Z') " +
//				"and jc.text_contract_number is null order by cst.creation_date";
		List<Jfs2ExportOptOut> jfsPayment = jdbcTemplateObject.query(SQL, new Jfs2ExportOptOutMapper());
		return jfsPayment;
	}
	
	@Override
	public List<Jfs2ExportOptOut> getJfs2ExportOptOutData(String runDate) {
		String SQL = "select distinct c.contract_code, trim(d.full_name) as CUSTOMER_NAME,cst.creation_date as DATE_SIGNATURE_CONTRACT,round(a.principal_split_rate*fp.credit_amount,0) as BANK_CREDIT_AMOUNT_PORTION " +
				"from jfs_contract_eligibility e left join (select distinct creation_date, contract_id from GTT_JSF_001 where CONTRACT_STATUS_TRANS = 'N') cst on trunc(cst.creation_date)=e.date_signature_contract " +
				"left join (select distinct id, contract_code,PRODUCT_CODE, deal_code,CONTRACT_STATUS from GTT_JSF_002) c on c.id = cst.contract_id " +
				"left join (select distinct credit_amount, contract_id from GTT_JSF_003) fp on c.id = fp.contract_id " +
				"left join (select * from jfs_product_mapping pm " +
				"inner join (select * from jfs_agreement WHERE REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on ja.code = pm.id_agreement " +
				"inner join jfs_product jp on ja.code=jp.id_agreement and pm.bank_product_code=jp.bank_product_code and trunc(sysdate)<=trunc(jp.valid_to) and trunc(sysdate)>=trunc(jp.valid_from) " +
				"left join jfs_partner jp on ja.partner_id = jp.id " +
				"where ja.partner_id='001' and to_number(ja.code, '99') < 4 " +
				") a on c.PRODUCT_CODE = a.code_product " +
				"and trunc(cst.creation_date) between trunc(a.valid_from) and trunc(a.valid_to) " +
				"left join (select distinct code, dbl.full_name from gtt_jsf_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id) d on c.deal_code = d.code " +
				"left join jfs_contract jc on jc.text_contract_number=c.contract_code and jc.id_agreement <4 " +
				"where trunc(e.dtime_inserted)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and e.eligibility_type='OPT-OUT1' and c.CONTRACT_STATUS in ('A','N','Z') " +
				"and jc.text_contract_number is null order by cst.creation_date";
		
//		String SQL = "select distinct c.contract_code, trim(d.full_name) as CUSTOMER_NAME,cst.creation_date as DATE_SIGNATURE_CONTRACT,round(a.principal_split_rate*fp.credit_amount,0) as BANK_CREDIT_AMOUNT_PORTION " +
//				"from jfs_contract_eligibility e left join GTT_JSF_001 cst on cst.CONTRACT_STATUS_TRANS = 'N' and trunc(cst.creation_date)=e.date_signature_contract " +
//				"left join (select distinct id, contract_code,PRODUCT_CODE, deal_code,CONTRACT_STATUS from GTT_JSF_002) c on c.id = cst.contract_id " +
//				"left join GTT_JSF_003 fp on c.id = fp.contract_id left join (select * from jfs_product_mapping pm " +
//				"inner join (select * from jfs_agreement WHERE REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on ja.code = pm.id_agreement " +
//				"inner join jfs_product jp on ja.code=jp.id_agreement and pm.bank_product_code=jp.bank_product_code and trunc(sysdate)<=trunc(jp.valid_to) " +
//				"and trunc(sysdate)>=trunc(jp.valid_from) left join jfs_partner jp on ja.partner_id = jp.id " +
//				"where ja.partner_id='001' and to_number(ja.code, '99') < 4) a on c.PRODUCT_CODE = a.code_product " +
//				"and trunc(cst.creation_date) between trunc(a.valid_from) and trunc(a.valid_to) " +
//				"left join (select distinct code, full_name from GTT_JSF_005) d on c.deal_code = d.code " +
//				"left join jfs_contract jc on jc.text_contract_number=c.contract_code and jc.id_agreement <4 " +
//				"where trunc(e.dtime_inserted)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and e.eligibility_type='OPT-OUT1' and c.CONTRACT_STATUS in ('A','N','Z') " +
//				"and jc.text_contract_number is null order by cst.creation_date";
		List<Jfs2ExportOptOut> jfsPayment = jdbcTemplateObject.query(SQL, new Jfs2ExportOptOutMapper());
		return jfsPayment;
	}
}
