package com.hci.data.entity;

public class Jfs2ExportOptOut {
	private String contractNumber;
	private String customerName;
	private String dateSignatureContract;
	private String bankCreditAmountPortion;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDateSignatureContract() {
		return dateSignatureContract;
	}
	public void setDateSignatureContract(String dateSignatureContract) {
		this.dateSignatureContract = dateSignatureContract;
	}
	public String getBankCreditAmountPortion() {
		return bankCreditAmountPortion;
	}
	public void setBankCreditAmountPortion(String bankCreditAmountPortion) {
		this.bankCreditAmountPortion = bankCreditAmountPortion;
	}
}