package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.Jfs2AllExport;
import com.hci.data.entity.Jfs2ExportOptOut;

public class Jfs2AllExportMapper implements RowMapper<Jfs2AllExport> {

	@Override
	public Jfs2AllExport mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jfs2AllExport jfsPayment = new Jfs2AllExport();
		jfsPayment.setType(rs.getString("ELIGIBILITY_TYPE"));
		jfsPayment.setDateSignature(rs.getString("DATE_SIGNATURE_CONTRACT"));
		jfsPayment.setBtpnPortion(rs.getString("BANK_FINANCING_PORTION"));
		jfsPayment.setHcidPortion(rs.getString("HCID_FINANCING_PORTION"));
		jfsPayment.setCntContract(rs.getString("CNT_CONTRACT"));
		return jfsPayment;
	}
	
}
