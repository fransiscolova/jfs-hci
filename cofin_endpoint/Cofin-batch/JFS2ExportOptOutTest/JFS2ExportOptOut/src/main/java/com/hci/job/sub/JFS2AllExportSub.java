package com.hci.job.sub;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hci.data.dao.Jfs2AllExportDao;
import com.hci.data.dao.Jfs2ExportOptOutDao;
import com.hci.data.entity.Jfs2AllExport;
import com.hci.data.entity.Jfs2ExportOptOut;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFS2AllExportSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<Jfs2AllExport> getAllData() {
		Jfs2AllExportDao jfsPaymentDao = (Jfs2AllExportDao) SpringContextHolder.getApplicationContext().getBean("jfs2AllExportDao");
		List<Jfs2AllExport> jfsPayments = jfsPaymentDao.getJfs2AllExportData();
		return jfsPayments;
	}
	
	public List<Jfs2AllExport> getAllData(String runDate) {
		Jfs2AllExportDao jfsPaymentDao = (Jfs2AllExportDao) SpringContextHolder.getApplicationContext().getBean("jfs2AllExportDao");
		List<Jfs2AllExport> jfsPayments = jfsPaymentDao.getJfs2AllExportData(runDate);
		return jfsPayments;
	}
	
	public List<String> createPaymentFileData(String header, String runDate) {
		List<String> paymentDataString = new ArrayList<String>();
		paymentDataString.add(header);
		
		List<Jfs2AllExport> allList;
		if (runDate.equals("")) {
			allList = getAllData();
		} else {
			allList = getAllData(runDate);
		}
		
		log.info("JFS2AllExportSub", "Num of file lines : " + allList.size());
		
		for(Jfs2AllExport jp : allList) {
			paymentDataString.add(jp.getType() + ";" + jp.getDateSignature() + ";" + jp.getBtpnPortion() + ";" + jp.getHcidPortion() + ";" + jp.getCntContract());
		}
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2AllExportSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_ALL_EXPORT_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		
		List<String> dataToWrite = createPaymentFileData("TYPE;DATE_SIGNATURE;BTPN_PORTION;HCID_PORTION;CNT_CONTRACT", runDate);
		
		if(dataToWrite.size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus,  fileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2AllExportSub", "JobStatus : " + jobStatus);
		log.info("JFS2AllExportSub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
