package com.hci;

import com.hci.database.dao.impl.BatchContextDaoImpl;
import com.hci.entity.BatchContext;
import com.hci.util.FormatConverter;
import com.hci.util.LoggerUtil;
import com.hci.util.SpringContextHolder;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {
	public static void main(String[] args) {
	    LoggerUtil log = new LoggerUtil();
	    
	    BatchContextDaoImpl bcdi = new BatchContextDaoImpl();
	    List<BatchContext> bCon = bcdi.getAllContext();
	    for (BatchContext b : bCon) {
	      log.info("AppMain", b.getBatchJobContext());
	    }
	    Object context2 = new ClassPathXmlApplicationContext(new FormatConverter().contextList2Array(bcdi.getAllContext()));
	    
	    new SpringContextHolder().setApplicationContext((ApplicationContext)context2);
	}
}