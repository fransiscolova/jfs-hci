package com.hci.data;

import java.util.ArrayList;
import java.util.List;
import com.hci.database.dao.impl.BatchContextDaoImpl;
import com.hci.entity.BatchContext;

public class BatchContextData {
	private List<String> context;
	
	public List<String> getContext() {
		return context;
	}

	public void setContext(List<String> context) {
		this.context = context;
	}
	
	public void setContextList() {
		BatchContextDaoImpl bjdi = new BatchContextDaoImpl();
		
		List<BatchContext> bcs = bjdi.getAllContext();
		List<String> jc = new ArrayList<String>();
		
		for(BatchContext bc : bcs) {
			jc.add(bc.getBatchJobContext());
		}
		
		setContext(jc);
	}
}
