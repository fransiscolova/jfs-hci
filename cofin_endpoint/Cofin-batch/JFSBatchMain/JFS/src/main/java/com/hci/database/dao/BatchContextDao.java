/**
 *
 */
package com.hci.database.dao;

import java.util.List;

import com.hci.entity.BatchContext;

/**
 * @author muhammad.muflihun
 *
 */
public abstract interface BatchContextDao {
    public abstract List<BatchContext> getAllContext();
}
