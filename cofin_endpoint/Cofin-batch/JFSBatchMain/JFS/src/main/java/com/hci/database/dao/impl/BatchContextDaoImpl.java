package com.hci.database.dao.impl;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.database.DatabaseConnection;
import com.hci.database.dao.BatchContextDao;
import com.hci.entity.BatchContext;
import com.hci.mapper.BatchContextMapper;

/**
 * @author muhammad.muflihun
 *
 */
public class BatchContextDaoImpl implements BatchContextDao {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public BatchContextDaoImpl() {
        dataSource = new DatabaseConnection().getContextConfigurationDatasource();
        jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public List<BatchContext> getAllContext() {
        String SQL = "select * from batch_job_context where is_active = 'Y' and seq is not null order by seq";
        List<BatchContext> bjc = this.jdbcTemplateObject.query(SQL, new BatchContextMapper());
        return bjc;
    }

}
