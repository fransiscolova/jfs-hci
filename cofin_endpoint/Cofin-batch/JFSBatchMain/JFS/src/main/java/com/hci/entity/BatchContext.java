package com.hci.entity;

public class BatchContext {
	private String batchJobContext;
	private String isActive;
	
	public String getBatchJobContext() {
		return batchJobContext;
	}
	public void setBatchJobContext(String batchJobContext) {
		this.batchJobContext = batchJobContext;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
}
