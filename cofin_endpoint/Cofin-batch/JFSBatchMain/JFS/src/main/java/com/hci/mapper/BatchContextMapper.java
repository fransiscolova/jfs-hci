package com.hci.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.entity.BatchContext;

public class BatchContextMapper implements RowMapper<BatchContext>{

	@Override
	public BatchContext mapRow(ResultSet rs, int rowNum) throws SQLException {
		BatchContext bc = new BatchContext();
		bc.setBatchJobContext(rs.getString("CONTEXT_PATH"));
		bc.setIsActive(rs.getString("IS_ACTIVE"));
		return bc;
	}
}
