package com.hci.runner;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.hci.util.LoggerUtil;
import com.hci.data.BatchJobData;

public class MainJobRunner {
    private BatchJobData jobData;
	private Map<String, String> sch;
	private Map<String, String> sourceFName;
	private Map<String, String> targetFName;
	private Map<String, String> jobClass;
	private Map<String, String> parentClass;
	private List<String> jobNames;
	private LoggerUtil log = new LoggerUtil();
	
	public void setJobData(BatchJobData jobData) {
	    log.info("MainJobRunner", "Going to populate the jobData");
	    log.info("jobclassparent", jobData.getJobClassParent().toString());
	    this.jobData = jobData;
		setJobNames(jobData.getJobName());
		setSch(jobData.getSch());
		setSourceFName(jobData.getSourceFName());
		setTargetFName(jobData.getTargetFName());
		setJobClass(jobData.getJobClass());
		setParentClass(jobData.getJobClassParent());
	}
	
	public void setParentClass(Map<String, String> parentClass) {
	    this.parentClass = parentClass;
	}

	public void setJobClass(Map<String, String> jobClass) {
		this.jobClass = jobClass;
	}

	public void setSch(Map<String, String> sch) {
		this.sch = sch;
	}
	
	public void setSourceFName(Map<String, String> sourceFName) {
		this.sourceFName = sourceFName;
	}

	public void setTargetFName(Map<String, String> targetFName) {
		this.targetFName = targetFName;
	}
	
	public void setJobNames(List<String> jobNames) {
		this.jobNames = jobNames;
	}

    public void run() throws ClassNotFoundException {
	    Iterator<Entry<String, String>> it = parentClass.entrySet().iterator();
	    log.info("MainJobRunner", String.valueOf(parentClass.size()));
	    while (it.hasNext()) {
	      Map.Entry entry = (Map.Entry)it.next();
	      log.info("MainJobRunner", (String)entry.getKey() + " - " + (String)entry.getValue());
	      JobDetail jd = new JobDetail();
	      CronTrigger trigger = new CronTrigger();
	      try {
	        jd.setName((String)entry.getKey());
	        jd.setJobClass(Class.forName((String)entry.getValue()));
	        
	        log.info("MainJobRunner", (String)entry.getKey() + " schedule : " + jd.getJobClass().toString());
	        
	        trigger.setName("Trigger" + entry.getKey());
	        trigger.setCronExpression((String)this.sch.get(entry.getKey()));
	        log.info("MainJobRunner", trigger.getCronExpression());
	        
	        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
	        scheduler.scheduleJob(jd, trigger);
	        scheduler.start();
	        log.info("MainJobRunner", "Succesfully create trigger for : " + (String)entry.getKey());
	      } catch (ParseException e) {
	        log.info("MainJobRunner", "(PE)Fail to create trigger for : " + (String)entry.getKey());
	        e.printStackTrace();
	      } catch (SchedulerException e) {
	        log.info("MainJobRunner", "(SE)Fail to create trigger for : " + (String)entry.getKey());
	        e.printStackTrace();
	      }
	    }
	}
}
