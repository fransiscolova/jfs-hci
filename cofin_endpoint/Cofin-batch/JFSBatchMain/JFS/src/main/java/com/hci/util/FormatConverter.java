package com.hci.util;

import java.util.List;

import com.hci.entity.BatchContext;

public class FormatConverter {
	public String[] contextList2Array(List<BatchContext> bcs) {
		String[] cArray = new String[bcs.size()];
		int i = 0;
		for (BatchContext bc : bcs) {
			cArray[i] = bc.getBatchJobContext();
			i++;
		}
		return cArray;
	}
}
