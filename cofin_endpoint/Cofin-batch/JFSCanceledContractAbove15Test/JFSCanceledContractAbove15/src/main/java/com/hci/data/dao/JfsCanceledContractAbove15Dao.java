package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsCanceledContractAbove15;

public interface JfsCanceledContractAbove15Dao {
	public List<JfsCanceledContractAbove15> getJfsCanceledContractAbove15Data();
	public List<JfsCanceledContractAbove15> getJfsCanceledContractAbove15Data(String runDate);
}
