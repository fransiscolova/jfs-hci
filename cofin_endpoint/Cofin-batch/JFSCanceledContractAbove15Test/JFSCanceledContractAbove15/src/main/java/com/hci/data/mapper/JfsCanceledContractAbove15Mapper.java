package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsCanceledContractAbove15;

public class JfsCanceledContractAbove15Mapper implements RowMapper<JfsCanceledContractAbove15> {

	@Override
	public JfsCanceledContractAbove15 mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsCanceledContractAbove15 jfsPayment = new JfsCanceledContractAbove15();
		jfsPayment.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setClientName(rs.getString("CLIENT_NAME"));
		jfsPayment.setDatePayment(rs.getString("DATE_PAYMENT"));
		jfsPayment.setPmtInstalment(rs.getString("PMT_INSTALMENT"));
		jfsPayment.setPmtPenalty(rs.getString("PMT_PENALTY"));
		jfsPayment.setPmtFee(rs.getString("PMT_FEE"));
		jfsPayment.setPaymentType(rs.getString("PAYMENT_TYPE"));
		jfsPayment.setAgreementId(rs.getString("AGREEMENT_ID"));
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPayment;
	}	
}