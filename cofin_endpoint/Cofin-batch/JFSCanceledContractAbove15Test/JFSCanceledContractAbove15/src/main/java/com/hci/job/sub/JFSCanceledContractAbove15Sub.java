package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsCanceledContractAbove15Dao;
import com.hci.data.entity.JfsCanceledContractAbove15;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSCanceledContractAbove15Sub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsCanceledContractAbove15> getAllData() {
		JfsCanceledContractAbove15Dao jfsPaymentDao = (JfsCanceledContractAbove15Dao) SpringContextHolder.getApplicationContext().getBean("jfsCanceledContractAbove15Dao");
		List<JfsCanceledContractAbove15> jfsPayments = jfsPaymentDao.getJfsCanceledContractAbove15Data();
		return jfsPayments;
	}
	
	public List<JfsCanceledContractAbove15> getAllData(String runDate) {
		JfsCanceledContractAbove15Dao jfsPaymentDao = (JfsCanceledContractAbove15Dao) SpringContextHolder.getApplicationContext().getBean("jfsCanceledContractAbove15Dao");
		List<JfsCanceledContractAbove15> jfsPayments = jfsPaymentDao.getJfsCanceledContractAbove15Data(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		int numOfRow = 0;
		int amtEt = 0;
		
		List<JfsCanceledContractAbove15> contracts;
		if (runDate.equals("")) {
			contracts = getAllData();
		} else {
			contracts = getAllData(runDate);
		}
		
		log.info("JFSCanceledContractAbove15Sub", "Num of file lines : " + contracts.size());
		
		for(JfsCanceledContractAbove15 jp : getAllData()) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getTextContractNumber() + "|" + jp.getClientName() + "|" + jp.getDatePayment() + "|" + 
					jp.getPmtInstalment() + "|" + jp.getPmtPenalty() + "|0|" + jp.getPmtFee() + "|" + jp.getPaymentType());
			numOfRow++;
			amtEt += Integer.valueOf(jp.getPmtInstalment()) + Integer.valueOf(jp.getPmtPenalty()) + Integer.valueOf(jp.getPmtFee());
		}
		paymentDetailDataString.add("HCI|" + numOfRow + "|" + amtEt);
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSCanceledContractAbove15Sub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_ET_after_15days_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|5|" + dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, fileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSCanceledContractAbove15Sub", "JobStatus : " + jobStatus);
		log.info("JFSCanceledContractAbove15Sub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
