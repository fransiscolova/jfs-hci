package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsCanceledContractBelow15;

public interface JfsCanceledContractBelow15Dao {
	public List<JfsCanceledContractBelow15> getJfsCanceledContractBelow15Data();
	public List<JfsCanceledContractBelow15> getJfsCanceledContractBelow15Data(String runDate);
}
