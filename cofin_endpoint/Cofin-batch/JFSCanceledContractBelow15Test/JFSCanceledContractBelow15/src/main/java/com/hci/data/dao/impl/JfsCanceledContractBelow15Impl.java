package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsCanceledContractBelow15Dao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsCanceledContractBelow15;
import com.hci.data.mapper.JfsCanceledContractBelow15Mapper;

public class JfsCanceledContractBelow15Impl implements JfsCanceledContractBelow15Dao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsCanceledContractBelow15Impl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsCanceledContractBelow15> getJfsCanceledContractBelow15Data() {
		String SQL = "select replace(CON.BANK_REFERENCE_NO,chr(13),'') BANK_REFERENCE_NO, " +   
				"CON.BANK_DECISION_DATE, CON.TEXT_CONTRACT_NUMBER, CON.CLIENT_NAME, " +  
				"CC.DTIME_SIGNED, trim(to_char(CON.BANK_PRINCIPAL,'999,999,999')) BANK_PRINCIPAL, " +  
				"CC.PAID_INSTALMENT PAID_INSTALMENT, " +
				"(CON.BANK_PRINCIPAL - CC.PAID_INSTALMENT) AMT_ET, " +   
				"ja.code agreement_id, jp.id partner_id FROM JFS_CANCELED_CONTRACT CC " +  
				"JOIN JFS_CONTRACT CON ON CON.TEXT_CONTRACT_NUMBER = CC.TEXT_CONTRACT_NUMBER " +  
				"left join jfs_agreement ja on con.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " +
				"WHERE TRUNC(CC.DTIME_EXPORTED) = TRUNC(sysdate)"; 
		List<JfsCanceledContractBelow15> jfsPayment = jdbcTemplateObject.query(SQL, new JfsCanceledContractBelow15Mapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsCanceledContractBelow15> getJfsCanceledContractBelow15Data(String runDate) {
		String SQL = "select replace(CON.BANK_REFERENCE_NO,chr(13),'') BANK_REFERENCE_NO, " +   
				"CON.BANK_DECISION_DATE, CON.TEXT_CONTRACT_NUMBER, CON.CLIENT_NAME, " +  
				"CC.DTIME_SIGNED, trim(to_char(CON.BANK_PRINCIPAL,'999,999,999')) BANK_PRINCIPAL, " +  
				"CC.PAID_INSTALMENT PAID_INSTALMENT, " +
				"(CON.BANK_PRINCIPAL - CC.PAID_INSTALMENT) AMT_ET, " +   
				"ja.code agreement_id, jp.id partner_id FROM JFS_CANCELED_CONTRACT CC " +  
				"JOIN JFS_CONTRACT CON ON CON.TEXT_CONTRACT_NUMBER = CC.TEXT_CONTRACT_NUMBER " +  
				"left join jfs_agreement ja on con.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " +
				"WHERE TRUNC(CC.DTIME_EXPORTED) = TRUNC(to_date('" + runDate + "', 'yyyy-mm-dd'))";
		List<JfsCanceledContractBelow15> jfsPayment = jdbcTemplateObject.query(SQL, new JfsCanceledContractBelow15Mapper());
		return jfsPayment;
	}
}
