package com.hci.data.entity;

import java.sql.Date;

public class JfsCanceledContractBelow15 {
	private String bankReferenceNo;
	private String bankDecisionDate;
	private String textContractNumber;
	private String clientName;
	private String dtimeSigned;
	private String bankPrincipal;
	private int paidInstalment;
	private int amtEt;
	private String agreementId;
	private String partnerId;
	
	public String getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getBankReferenceNo() {
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo) {
		this.bankReferenceNo = bankReferenceNo;
	}
	public String getBankDecisionDate() {
		return bankDecisionDate;
	}
	public void setBankDecisionDate(String bankDecisionDate) {
		this.bankDecisionDate = bankDecisionDate;
	}
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getDtimeSigned() {
		return dtimeSigned;
	}
	public void setDtimeSigned(String dtimeSigned) {
		this.dtimeSigned = dtimeSigned;
	}
	public String getBankPrincipal() {
		return bankPrincipal;
	}
	public void setBankPrincipal(String bankPrincipal) {
		this.bankPrincipal = bankPrincipal;
	}
	public int getPaidInstalment() {
		return paidInstalment;
	}
	public void setPaidInstalment(int paidInstalment) {
		this.paidInstalment = paidInstalment;
	}
	public int getAmtEt() {
		return amtEt;
	}
	public void setAmtEt(int amtEt) {
		this.amtEt = amtEt;
	}
}