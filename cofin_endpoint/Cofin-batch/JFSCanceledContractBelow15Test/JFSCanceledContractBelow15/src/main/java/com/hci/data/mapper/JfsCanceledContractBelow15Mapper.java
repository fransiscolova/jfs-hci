package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsCanceledContractBelow15;

public class JfsCanceledContractBelow15Mapper implements RowMapper<JfsCanceledContractBelow15> {

	@Override
	public JfsCanceledContractBelow15 mapRow(ResultSet rs, int rowNum) throws SQLException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		DateFormat cfaSignDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dfFinish = new SimpleDateFormat("dd-MMM-yy");
		String bankDecisionDate = "";
		String cfaSignDate = "";
		try {
			bankDecisionDate = dfFinish.format(df.parse(rs.getString("BANK_DECISION_DATE"))).toString().toUpperCase();
			cfaSignDate = dfFinish.format(cfaSignDateFormat.parse(rs.getString("DTIME_SIGNED"))).toString().toUpperCase();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		JfsCanceledContractBelow15 jfsPayment = new JfsCanceledContractBelow15();
		jfsPayment.setBankReferenceNo(rs.getString("BANK_REFERENCE_NO"));
		//jfsPayment.setBankDecisionDate(rs.getString("BANK_DECISION_DATE"));
		jfsPayment.setBankDecisionDate(bankDecisionDate);
		jfsPayment.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setClientName(rs.getString("CLIENT_NAME"));
		//jfsPayment.setDtimeSigned(rs.getDate("DTIME_SIGNED"));
		jfsPayment.setDtimeSigned(cfaSignDate);
		jfsPayment.setBankPrincipal(rs.getString("BANK_PRINCIPAL"));
		jfsPayment.setPaidInstalment(rs.getInt("PAID_INSTALMENT"));
		jfsPayment.setAmtEt(rs.getInt("AMT_ET"));
		jfsPayment.setAgreementId(rs.getString("AGREEMENT_ID"));
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPayment;
	}
	
}
