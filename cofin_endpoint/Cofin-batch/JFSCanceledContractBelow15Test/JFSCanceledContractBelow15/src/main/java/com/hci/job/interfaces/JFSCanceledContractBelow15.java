package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.data.dao.ParentJobDao;
import com.hci.job.sub.JFSCanceledContractBelow15Sub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSCanceledContractBelow15 implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private String targetFol;
	private String jobName;
	private String processName;
	private ParentJobDao parentDao;

	
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		//parentDao.runProcedure("P_JFS_CANCELED_CONTRACT");
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		/*if(checkHoliday()) {
			log.info("JFSCanceledContractBelow15", "Job skipped, holiday");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(!checkParentJob(processName)) {
			log.info("JFSCanceledContractBelow15", "Job skipped, parent job not run");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(checkTodayRunJob(processName)) {
			log.info("JFSPaymentReversal", "Job skipped, already runned");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}*/
		
		log.info("JFSCanceledContractBelow15", "Job not skipped");
		
		setForceRunToN(processName);
		
		JFSCanceledContractBelow15Sub JFSCanceledContractBelow15Sub = new JFSCanceledContractBelow15Sub();
		paymentFileStatus = JFSCanceledContractBelow15Sub.createFile(targetFol, uuid, "", 0, "", processName);
		
		if (paymentFileStatus) {
			sendEmail();
		}
		
		log.info("JFSCanceledContractBelow15", "Job done");
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		return RepeatStatus.FINISHED;
	}
	
	public boolean checkHoliday() {
		return parentDao.checkHoliday();
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void setForceRunToN(String processName) {
		parentDao.setForceRunNChildJob(processName);
	}
	
	public void sendEmail() throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS - HCID Contract ET within 15 Days " + sdf.format(new Date());
		StringBuffer content = new StringBuffer();
		content.append("There are some contracts have been canceled in HoSel within 15days.\n");
		content.append("BTPN call this process as ET within 15days.\n");
		content.append("You can find the file:\n");
		content.append("Please create confirmation letter for this process manually\n");
		content.append("and proceed to BTPN.\n\n");
		content.append("You can find the file:\n");
		content.append(" /WS/Data/JFS/Export/JFS_ET_within_15days_" + sdf.format(new Date()) + ".txt\n\n");
		content.append("And move the file to folder :\n");
		content.append("/WS/Data/JFS/Export/Registered/\n\nafter you process it.");
		emailUtil.sendEmail(subject, content);
	}
	
	public boolean checkTodayRunJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
}