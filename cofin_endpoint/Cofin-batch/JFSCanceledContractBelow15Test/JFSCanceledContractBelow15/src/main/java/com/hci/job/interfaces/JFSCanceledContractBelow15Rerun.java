package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.JobLauncherJFSCanceledContractBelow15;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ReExecuteJob;
import com.hci.job.sub.JFSCanceledContractBelow15Sub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSCanceledContractBelow15Rerun implements Tasklet, InitializingBean {
	
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private ParentJobDao parentJobDao;
	private String targetFol;
	private String jobName;
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentJobDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		//parentJobDao.runProcedure("P_JFS_CANCELED_CONTRACT");
		
		List<ReExecuteJob> jobs = getAllReExecuteJob(jobName);
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		String fileDate = "";
		
		if(!checkTodayJob(processName)) {
			log.info("JFSCanceledContractBelow15Rerun", "Today job not yet run");
			if(checkParentJob(processName)) {
				JobDetail jd = new JobDetail();
				try {
					jd = new JobDetail("JFSPaymentRun", Scheduler.DEFAULT_GROUP, JobLauncherJFSCanceledContractBelow15.class);
					jd.getJobDataMap().put("test", "testApp");
					Trigger tgr = TriggerUtils.makeImmediateTrigger(0, 0);
					tgr.setName("FireOnceRun");
					
					Scheduler scheduler = new StdSchedulerFactory().getScheduler();
					scheduler.start();
					scheduler.scheduleJob(jd, tgr);
				} catch (SchedulerException e) {
					e.printStackTrace();
				}
				
				new PropertyUtil().setProperty("batch.job.run.status", "N");
				
				return RepeatStatus.FINISHED;
			}
			log.info("JFSCanceledContractBelow15Rerun", "Parent job of today job not yet run");
		}

		log.info("JFSCanceledContractBelow15Rerun", "Rerun the job");
		
		if(jobs.size() != 0) {
			for(ReExecuteJob job : jobs) {
				int numOfRerun = job.getNumOfRerun() + 1;
				
				JFSCanceledContractBelow15Sub jfsPaymentSub = new JFSCanceledContractBelow15Sub();
				paymentFileStatus = jfsPaymentSub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "N", processName);
				
				fileDate = job.getRunDate();
			}
		}
		
		if (paymentFileStatus) {
			sendEmail(fileDate);
		}
		
		log.info("JFSCanceledContractBelow15Rerun", "Rerun job finish");
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		return RepeatStatus.FINISHED;
	}
	
	public List<ReExecuteJob> getAllReExecuteJob(String jobName) {
		List<ReExecuteJob> jobs = parentJobDao.getRerunJob(jobName);
		return jobs;
	}
	
	public boolean checkTodayJob(String processName) {
		return parentJobDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
	
	public boolean checkParentJob(String processName) {
		return parentJobDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void sendEmail(String fileDate) throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS - HCID Contract ET within 15 Days " + fileDate;
		StringBuffer content = new StringBuffer();
		content.append("There are some contracts have been canceled in HoSel within 15days.\n");
		content.append("BTPN call this process as ET within 15days.\n");
		content.append("You can find the file:\n");
		content.append("Please create confirmation letter for this process manually\n");
		content.append("and proceed to BTPN.\n\n");
		content.append("You can find the file:\n");
		content.append(" /WS/Data/JFS/Export/JFS_ET_within_15days_" + fileDate + ".txt\n\n");
		content.append("And move the file to folder :\n");
		content.append("/WS/Data/JFS/Export/Registered/\n\nafter you process it.");
		emailUtil.sendEmail(subject, content);
	}
}