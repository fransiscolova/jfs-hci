package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsCanceledContractBelow15Dao;
import com.hci.data.entity.JfsCanceledContractBelow15;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSCanceledContractBelow15Sub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsCanceledContractBelow15> getAllData() {
		JfsCanceledContractBelow15Dao jfsPaymentDao = (JfsCanceledContractBelow15Dao) SpringContextHolder.getApplicationContext().getBean("jfsCanceledContractBelow15Dao");
		List<JfsCanceledContractBelow15> jfsPayments = jfsPaymentDao.getJfsCanceledContractBelow15Data();
		return jfsPayments;
	}
	
	public List<JfsCanceledContractBelow15> getAllData(String runDate) {
		JfsCanceledContractBelow15Dao jfsPaymentDao = (JfsCanceledContractBelow15Dao) SpringContextHolder.getApplicationContext().getBean("jfsCanceledContractBelow15Dao");
		List<JfsCanceledContractBelow15> jfsPayments = jfsPaymentDao.getJfsCanceledContractBelow15Data(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		List<JfsCanceledContractBelow15> contractList = new ArrayList<JfsCanceledContractBelow15>();
		int paidInstalment = 0;
		int amtEt = 0;
		int footerSum = 0;
		
		if (runDate.equals("")) {
			contractList = getAllData();
		} else {
			contractList = getAllData(runDate);
		}
		
		log.info("JFSCanceledContractBelow15Sub", "Num of file lines : " + contractList.size());
		
		for(JfsCanceledContractBelow15 jp : contractList) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getBankReferenceNo() + "|" + jp.getBankDecisionDate() + "|" + jp.getTextContractNumber() + "|" + jp.getClientName() + "|" + jp.getDtimeSigned() + "|" +
					jp.getBankPrincipal() + "|" + NumberFormat.getNumberInstance(Locale.US).format(jp.getPaidInstalment()) + "|" + NumberFormat.getNumberInstance(Locale.US).format(jp.getAmtEt()));
			paidInstalment += jp.getPaidInstalment();
			amtEt += jp.getAmtEt();
		}
		paymentDetailDataString.add("TOTAL:|" + NumberFormat.getNumberInstance(Locale.US).format(paidInstalment) + "|" + NumberFormat.getNumberInstance(Locale.US).format(amtEt));
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSCanceledContractBelow15Sub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_ET_within_15days_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		
		Map<String, Object> dataToWrite = createPaymentFileData("Confirmation_Batch_No|Date_of_BTPN_Confirmation|contract_number|Customer_Name|CFA_sign_date|BTPN_Financing_Portion|Paid_Instalment|Total_Termination_Amount", runDate);

		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, fileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSCanceledContractBelow15Sub", "JobStatus : " + jobStatus);
		log.info("JFSCanceledContractBelow15Sub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
