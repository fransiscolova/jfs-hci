package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsExportBarangFF;

public interface JfsExportBarangFFDao {
	public List<JfsExportBarangFF> getJfsExporBarangFFData();
	public List<JfsExportBarangFF> getJfsExporBarangFFData(String runDate);	
}
