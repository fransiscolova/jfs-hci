package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsExportEnduserFF;

public interface JfsExportEnduserFFDao {
	public List<JfsExportEnduserFF> getJfsExporEndusertFFData();
	public List<JfsExportEnduserFF> getJfsExporEndusertFFData(String runDate);
}
