package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsExportBarangFF;
import com.hci.data.entity.JfsExportFF;

public interface JfsExportFFDao {
	public List<JfsExportFF> getJfsExporFFData();
}
