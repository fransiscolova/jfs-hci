package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsExportFFSlik;

public interface JfsExportFFSlikDao {
	public List<JfsExportFFSlik> getJfsExporFFSlikData();
	public List<JfsExportFFSlik> getJfsExporFFSlikData(String runDate);
}
