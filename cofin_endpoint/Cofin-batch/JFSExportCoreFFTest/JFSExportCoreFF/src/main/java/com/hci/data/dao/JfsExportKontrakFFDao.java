package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsExportKontrakFF;
import com.hci.data.entity.JfsExportKontrakFFFooter;

public interface JfsExportKontrakFFDao {
	public List<JfsExportKontrakFF> getJfsExporKontrakFFData();
	public List<JfsExportKontrakFFFooter> getJfsExportKontrakFFFooterData();
	public List<JfsExportKontrakFF> getJfsExporKontrakFFData(String runDate);
	public List<JfsExportKontrakFFFooter> getJfsExportKontrakFFFooterData(String runDate);
}
