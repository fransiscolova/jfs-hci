package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsExportPengurusFF;

public interface JfsExportPengurusFFDao {
	public List<JfsExportPengurusFF> getJfsExporPengurusFFData();
	public List<JfsExportPengurusFF> getJfsExporPengurusFFData(String runDate);
}
