package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportBarangFFDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsExportBarangFF;
import com.hci.data.mapper.JfsExportMapperBarangFF;

public class JfsExportBarangFFImpl implements JfsExportBarangFFDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportBarangFFImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsExportBarangFF> getJfsExporBarangFFData() {
		String SQL = "select distinct * from (select c.contract_code ,(case when upper(trim(d.name1))=upper(trim(d.name2)) then d.name1 else d.full_name end) as NAME " +
				",'' as ENGINE_NUMBER,'' as MODEL_SERIAL_NUMBER,JCT.btpn_code_commodity_category as BTPN_CODE_COMMODITY_CATEGORY " +
				",JCT.btpn_code_commodity_type as BTPN_CODE_COMMODITY_TYPE,to_char(sysdate,'YYYY') as SISDATE,case " +  
				"when jct.btpn_code_commodity_type in ('CT_CT_PDA', 'CT_CT_MTAB') then 'DGN2' " +
				"else cg.btpn_commodity_group end as COMMODITY,'' as PRODUCER,jc.id_agreement as AGREEMENT_ID " +
				",ja.partner_id from jfs_contract jc left join (select distinct contract_code, loan_purpose, deal_code from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"left join (select distinct name1,name2,full_name,code from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"JOIN JFS_COMMODITY_PURPOSE JCP ON JCP.CODE_LOAN_PURPOSE=C.LOAN_PURPOSE " +
				"LEFT JOIN JFS_COMMODITY_TYPE JCT ON JCT.HCI_CODE_COMMODITY_TYPE = JCP.COMMODITY_TYPE_CODE " +
				"left join JFS_COMMODITY_GROUP cg on jct.hci_code_commodity_category=cg.hci_code_commodity_category " +
				"left join (select * from jfs_agreement where REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on to_number(ja.code, '99') = jc.id_agreement " +
				"where jc.id_agreement='4' and jc.export_date=trunc(sysdate) order by c.contract_code)";
		List<JfsExportBarangFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperBarangFF());
		return jfsPayment;
	}
	
	@Override
	public List<JfsExportBarangFF> getJfsExporBarangFFData(String runDate) {
		String SQL = "select distinct * from (select c.contract_code ,(case when upper(trim(d.name1))=upper(trim(d.name2)) then d.name1 else d.full_name end) as NAME " +
				",'' as ENGINE_NUMBER,'' as MODEL_SERIAL_NUMBER,JCT.btpn_code_commodity_category as BTPN_CODE_COMMODITY_CATEGORY " +
				",JCT.btpn_code_commodity_type as BTPN_CODE_COMMODITY_TYPE,to_char(sysdate,'YYYY') as SISDATE,case " +  
				"when jct.btpn_code_commodity_type in ('CT_CT_PDA', 'CT_CT_MTAB') then 'DGN2' " +
				"else cg.btpn_commodity_group end as COMMODITY,'' as PRODUCER,jc.id_agreement as AGREEMENT_ID " +
				",ja.partner_id from jfs_contract jc left join (select distinct contract_code, loan_purpose, deal_code from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"left join (select distinct name1,name2,full_name,code from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"JOIN JFS_COMMODITY_PURPOSE JCP ON JCP.CODE_LOAN_PURPOSE=C.LOAN_PURPOSE " +
				"LEFT JOIN JFS_COMMODITY_TYPE JCT ON JCT.HCI_CODE_COMMODITY_TYPE = JCP.COMMODITY_TYPE_CODE " +
				"left join JFS_COMMODITY_GROUP cg on jct.hci_code_commodity_category=cg.hci_code_commodity_category " +
				"left join (select * from jfs_agreement where REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on to_number(ja.code, '99') = jc.id_agreement " +
				"where jc.id_agreement='4' and jc.export_date=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) order by c.contract_code)";
		List<JfsExportBarangFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperBarangFF());
		return jfsPayment;
	}
}
