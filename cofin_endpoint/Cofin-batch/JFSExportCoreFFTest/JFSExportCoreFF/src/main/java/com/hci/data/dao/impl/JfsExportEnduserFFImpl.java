package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportEnduserFFDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsExportEnduserFF;
import com.hci.data.mapper.JfsExportMapperEnduserFF;

public class JfsExportEnduserFFImpl implements JfsExportEnduserFFDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportEnduserFFImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsExportEnduserFF> getJfsExporEndusertFFData() {
		String SQL = "select distinct contract_code, full_name, nickname, dst_code, description, code8139, gender_type, ktp_num, num, birth_place, birth, street_name, full_address, village, sub_district, dst_code_xd, zip_code, value1, id, name4, employer_industry_code, name_employer, dst_code_xi, one, dst_code_xm, aid, dst_code_xr, income, valid_to, agreement_id, partner_id from (select c.contract_code,case when upper(trim(d.name1))=upper(trim(d.name2)) " +
				"then trim(d.name1) else trim(d.full_name) end as full_name,case  " +
				"when d.nickname is not null and length(d.nickname) > 2 then d.nickname " +
				"when d.name1 is not null and length(d.name1) > 2 then d.name1 " +
				"when d.name2 is not null and length(d.name2) > 2 then d.name2 " +
				"when d.name3 is not null and length(d.name3) > 2 then d.name3 " +
				"else d.full_name end as NICKNAME,xe.dst_code,xe.description,'8139' as CODE8139 " +
				",case when upper(d.gender)='F' then 'P' else 'L' end as GENDER_TYPE " +
				",d.DOCUMENT_NUMBER KTP_NUM,pas.num,d.birth_place,to_char(d.birth,'dd/MM/yyyy') as BIRTH " +
				",case when csn.street_name is not null and csn.street_name <> '0' and csn.street_name <> '00' " +
				"and csn.street_name <> '99' and upper(csn.street_name) <> 'XNA' then case " +
				"when UPPER(csn.street_name) like '%JL%' then csn.street_name " +
				"when UPPER(csn.street_name) like '%JALAN%' then csn.street_name " +
				"when UPPER(csn.street_name) like '%GG%' then csn.street_name " +
				"when UPPER(csn.street_name) like '%GANG%' then csn.street_name else 'Jl '||csn.street_name  " +
				"end end as street_name, case when csn.house_number is not null and csn.house_number <> '0' " +
				"and csn.house_number <> '00' and csn.house_number <> '99' and upper(csn.house_number) <> 'XNA' " +
				"then ' '||csn.house_number end ||' RT' || csn.block || ' RW'|| csn.block_set as full_address " +
				",upper(regexp_replace(regexp_replace(case when csn.name is null then csn.NAME_SUBDISTRICT       " +   
				"else csn.name end , '*[[:punct:]]', ' '),'( *[  ])',' ')) as village " +
				",upper(regexp_replace(regexp_replace(case when csn.NAME_SUBDISTRICT is null " +
				"then csn.NAME_SUBDISTRICT else csn.NAME_SUBDISTRICT end, '*[[:punct:]]', ' ') " +
				",'( *[  ])',' ')) as sub_district,case when xd.dst_code is null and csn.zipcode_code is not null then xd.dst_code " +
				"when xd.dst_code is null and csn.zipcode_code is null then xd.dst_code " +
				"when xd.dst_code is not null then xd.dst_code end as DST_CODE_XD " +
				",case when csn.zip_code is null then csn.zipcode_code else csn.zip_code end as ZIP_CODE " +
				",hc.value1,'ID' as ID,d.name4,case when er.economical_status_code='RETIRED' then '012'  " +
				"when er.economical_status_code='STUDENT' then '013'  " +
				"when er.economical_status_code='SELFEMPLOYED' then '014' " +
				"when er.employer_industry_code='ARMY_POLICE' then '011'  " +
				"when er.economical_status_code='STATE_EMPLOYEE' then '010' " +
				"when er.employer_industry_code='GOVERNMENT' then '010'  " +
				"else '099' end as EMPLOYER_INDUSTRY_CODE,case when LENGTH(er.name) < 50 " +
				"then er.name else SUBSTR(er.name, 1, 50) end name_employer ,nvl(xi.dst_code,'9990') as DST_CODE_XI " +
				",'1' as ONE ,nvl(xm.dst_code,'30') as DST_CODE_XM ,'ID' as AID ,nvl(xr.dst_code,'7') as DST_CODE_XR " +
				",(d.occupation_income_amount+nvl(d.by_work_income_amount,0)) as INCOME ,to_char(d.VALID_DATE,'dd/MM/yyyy') VALID_TO " +
				",jc.id_agreement as AGREEMENT_ID,ja.partner_id from jfs_contract jc " +
				"left join (select distinct contract_code, deal_code from GTT_JSF_002) c on jc.text_contract_number=c.contract_code  " +
				"left join (select distinct name1,name2,full_name,nickname,name3,gender,DOCUMENT_NUMBER, " +
				"birth_place,birth,name4,occupation_income_amount,by_work_income_amount,VALID_DATE,code,DOCUMENT_TYPE, " +
				"client_snapshot_id,lm.CUID,education_code,marital_status_code,religion_code,ID_CARD_STATUS " +
				"from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"left join (select distinct CODE, DOCUMENT_ID ID, DOCUMENT_NUMBER NUM from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id " +
				"where DOCUMENT_TYPE = 'PASSPORT' and ID_CARD_STATUS = 'a') pas on pas.code = d.code " +
				"left join GTT_JSF_006 csn on d.client_snapshot_id = csn.client_snapshot_id and csn.role_type = 'PERMANENT' " +
				"left join jfs_code_translate xd on xd.codelist='DISTRICT' and xd.src_code=csn.district_code " +
				"left join (select distinct VALUE1, CLIENT_SNAPSHOT_ID from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id " +
				"where contact_type_code = 'HOME_PHONE') hc on d.client_snapshot_id = hc.client_snapshot_id " +
				"left join (select distinct lm.CUID, TRIM(UPPER(ECONOMICAL_STATUS)) economical_status_code, " +
				"TRIM(UPPER(INDUSTRY)) employer_industry_code, name_employer NAME from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id " +
				"where ID_CARD_STATUS = 'a' and status = 'a' and lm.cuid is not null) er on er.CUID = d.CUID " +
				"left join JFS_code_translate xe on xe.codelist='EDUCATION_TYPE' and d.education_code=xe.src_code " +
				"left join JFS_code_translate xi on xi.codelist='INDUSTRY' and er.employer_industry_code=xi.src_code " +
				"left join JFS_code_translate xm on xm.codelist='MARITAL_STATUS' and d.marital_status_code=xm.src_code " +
				"left join jfs_code_translate xr on xm.codelist='RELIGION' and d.religion_code=xr.src_code " +
				"left join (select * from jfs_agreement where REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on to_number(ja.code, '99') = jc.id_agreement " +
				"where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement = 4 order by jc.id_agreement, c.contract_code)"; 
		List<JfsExportEnduserFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperEnduserFF());
		return jfsPayment;
	}
	
	@Override
	public List<JfsExportEnduserFF> getJfsExporEndusertFFData(String runDate) {
		String SQL = "select distinct contract_code, full_name, nickname, dst_code, description, code8139, gender_type, ktp_num, num, birth_place, birth, street_name, full_address, village, sub_district, dst_code_xd, zip_code, value1, id, name4, employer_industry_code, name_employer, dst_code_xi, one, dst_code_xm, aid, dst_code_xr, income, valid_to, agreement_id, partner_id from (select c.contract_code,case when upper(trim(d.name1))=upper(trim(d.name2)) " +
				"then trim(d.name1) else trim(d.full_name) end as full_name,case  " +
				"when d.nickname is not null and length(d.nickname) > 2 then d.nickname " +
				"when d.name1 is not null and length(d.name1) > 2 then d.name1 " +
				"when d.name2 is not null and length(d.name2) > 2 then d.name2 " +
				"when d.name3 is not null and length(d.name3) > 2 then d.name3 " +
				"else d.full_name end as NICKNAME,xe.dst_code,xe.description,'8139' as CODE8139 " +
				",case when upper(d.gender)='F' then 'P' else 'L' end as GENDER_TYPE " +
				",d.DOCUMENT_NUMBER KTP_NUM,pas.num,d.birth_place,to_char(d.birth,'dd/MM/yyyy') as BIRTH " +
				",case when csn.street_name is not null and csn.street_name <> '0' and csn.street_name <> '00' " +
				"and csn.street_name <> '99' and upper(csn.street_name) <> 'XNA' then case " +
				"when UPPER(csn.street_name) like '%JL%' then csn.street_name " +
				"when UPPER(csn.street_name) like '%JALAN%' then csn.street_name " +
				"when UPPER(csn.street_name) like '%GG%' then csn.street_name " +
				"when UPPER(csn.street_name) like '%GANG%' then csn.street_name else 'Jl '||csn.street_name  " +
				"end end as street_name, case when csn.house_number is not null and csn.house_number <> '0' " +
				"and csn.house_number <> '00' and csn.house_number <> '99' and upper(csn.house_number) <> 'XNA' " +
				"then ' '||csn.house_number end ||' RT' || csn.block || ' RW'|| csn.block_set as full_address " +
				",upper(regexp_replace(regexp_replace(case when csn.name is null then csn.NAME_SUBDISTRICT       " +   
				"else csn.name end , '*[[:punct:]]', ' '),'( *[  ])',' ')) as village " +
				",upper(regexp_replace(regexp_replace(case when csn.NAME_SUBDISTRICT is null " +
				"then csn.NAME_SUBDISTRICT else csn.NAME_SUBDISTRICT end, '*[[:punct:]]', ' ') " +
				",'( *[  ])',' ')) as sub_district,case when xd.dst_code is null and csn.zipcode_code is not null then xd.dst_code " +
				"when xd.dst_code is null and csn.zipcode_code is null then xd.dst_code " +
				"when xd.dst_code is not null then xd.dst_code end as DST_CODE_XD " +
				",case when csn.zip_code is null then csn.zipcode_code else csn.zip_code end as ZIP_CODE " +
				",hc.value1,'ID' as ID,d.name4,case when er.economical_status_code='RETIRED' then '012'  " +
				"when er.economical_status_code='STUDENT' then '013'  " +
				"when er.economical_status_code='SELFEMPLOYED' then '014' " +
				"when er.employer_industry_code='ARMY_POLICE' then '011'  " +
				"when er.economical_status_code='STATE_EMPLOYEE' then '010' " +
				"when er.employer_industry_code='GOVERNMENT' then '010'  " +
				"else '099' end as EMPLOYER_INDUSTRY_CODE,case when LENGTH(er.name) < 50 " +
				"then er.name else SUBSTR(er.name, 1, 50) end name_employer ,nvl(xi.dst_code,'9990') as DST_CODE_XI " +
				",'1' as ONE ,nvl(xm.dst_code,'30') as DST_CODE_XM ,'ID' as AID ,nvl(xr.dst_code,'7') as DST_CODE_XR " +
				",(d.occupation_income_amount+nvl(d.by_work_income_amount,0)) as INCOME ,to_char(d.VALID_DATE,'dd/MM/yyyy') VALID_TO " +
				",jc.id_agreement as AGREEMENT_ID,ja.partner_id from jfs_contract jc " +
				"left join (select distinct contract_code, deal_code from GTT_JSF_002) c on jc.text_contract_number=c.contract_code  " +
				"left join (select distinct name1,name2,full_name,nickname,name3,gender,DOCUMENT_NUMBER, " +
				"birth_place,birth,name4,occupation_income_amount,by_work_income_amount,VALID_DATE,code,DOCUMENT_TYPE, " +
				"client_snapshot_id,lm.CUID,education_code,marital_status_code,religion_code,ID_CARD_STATUS " +
				"from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"left join (select distinct CODE, DOCUMENT_ID ID, DOCUMENT_NUMBER NUM from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id " +
				"where DOCUMENT_TYPE = 'PASSPORT' and ID_CARD_STATUS = 'a') pas on pas.code = d.code " +
				"left join GTT_JSF_006 csn on d.client_snapshot_id = csn.client_snapshot_id and csn.role_type = 'PERMANENT' " +
				"left join jfs_code_translate xd on xd.codelist='DISTRICT' and xd.src_code=csn.district_code " +
				"left join (select distinct VALUE1, CLIENT_SNAPSHOT_ID from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id " +
				"where contact_type_code = 'HOME_PHONE') hc on d.client_snapshot_id = hc.client_snapshot_id " +
				"left join (select distinct lm.CUID, TRIM(UPPER(ECONOMICAL_STATUS)) economical_status_code, " +
				"TRIM(UPPER(INDUSTRY)) employer_industry_code, name_employer NAME from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id " +
				"where ID_CARD_STATUS = 'a' and status = 'a' and lm.cuid is not null) er on er.CUID = d.CUID " +
				"left join JFS_code_translate xe on xe.codelist='EDUCATION_TYPE' and d.education_code=xe.src_code " +
				"left join JFS_code_translate xi on xi.codelist='INDUSTRY' and er.employer_industry_code=xi.src_code " +
				"left join JFS_code_translate xm on xm.codelist='MARITAL_STATUS' and d.marital_status_code=xm.src_code " +
				"left join jfs_code_translate xr on xm.codelist='RELIGION' and d.religion_code=xr.src_code " +
				"left join (select * from jfs_agreement where REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on to_number(ja.code, '99') = jc.id_agreement " +
				"where trunc(jc.export_date)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and jc.id_agreement = 4 order by jc.id_agreement, c.contract_code)";
		List<JfsExportEnduserFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperEnduserFF());
		return jfsPayment;
	}
}
