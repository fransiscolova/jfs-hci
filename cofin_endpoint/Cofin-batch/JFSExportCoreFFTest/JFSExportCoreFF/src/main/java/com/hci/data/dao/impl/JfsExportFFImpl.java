package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportBarangFFDao;
import com.hci.data.dao.JfsExportFFDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsExportBarangFF;
import com.hci.data.entity.JfsExportFF;
import com.hci.data.mapper.JfsExportMapperBarangFF;
import com.hci.data.mapper.JfsExportMapperFF;

public class JfsExportFFImpl implements JfsExportFFDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportFFImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsExportFF> getJfsExporFFData() {
		String SQL = "select eligibility_type, to_char(date_signature_contract,'DD-MON-YYYY') as date_signature_contract, " +
					"bank_financing_portion, hcid_financing_portion, cnt_contract " +
					"from jfs_contract_eligibility where trunc(dtime_inserted)=trunc(sysdate) and id_agreement='4'"; 
		List<JfsExportFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperFF());
		return jfsPayment;
	}
}
