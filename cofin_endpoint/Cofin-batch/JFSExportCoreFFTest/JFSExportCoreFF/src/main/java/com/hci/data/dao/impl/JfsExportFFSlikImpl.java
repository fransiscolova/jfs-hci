package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportFFSlikDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsExportFFSlik;
import com.hci.data.mapper.JfsExportFFMapperSlik;

public class JfsExportFFSlikImpl implements JfsExportFFSlikDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportFFSlikImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsExportFFSlik> getJfsExporFFSlikData() {
		String SQL = "select jc.text_contract_number,s.KODE_SIFAT_KREDIT_PEMBIAYAAN,s.KODE_JENIS_KREDIT " +
				",s.KODE_SKIM,s.BARU_PERPANJANGAN,s.KODE_JENIS_PENGGUNAAN,s.KODE_ORIENTASI_PENGGUNAAN " +
				",s.TAKEOVER_DARI,s.ALAMAT_EMAIL,s.NO_TELEPON_SELULAR,s.ALAMAT_TEMPAT_BEKERJA,s.PENGHASILAN_KOTOR_PER_TAHUN " +
				",s.KODE_SUMBER_PENGHASILAN,s.JUMLAH_TANGGUNGAN,s.NAMA_PASANGAN,s.NO_IDENTITAS_PASANGAN " +
				",s.TANGGAL_IDENTITAS_PASANGAN,s.PERJANJIAN_PISAH_HARTA,s.KODE_BIDANG_USAHA,s.KODE_GOLONGAN_DEBITUR " +
				",s.KODE_BENTUK_BADAN_USAHA,s.GO_PUBLIC,s.PERINGKAT_RATING_DEBITUR,s.LEMBAGA_PEMERINGKAT_RATING " +
				",s.TANGGAL_PEMERINGKAT,s.NAMA_GRUP_DEBITUR,s.KODE_JENIS_AGUNAN,s.KODE_JENIS_PENGIKATAN,s.TANGGAL_PENGIKATAN " +
				",s.NAMA_PEMILIK_AGUNAN,s.ALAMAT_AGUNAN,s.DATI2,s.STATUS_KREDIT_JOIN,s.DIASURANSIKAN from JFS_CONTRACT jc " +  
				"join jfs_slik_tmp_dwh s on jc.text_contract_number=s.text_contract_number " +
				"WHERE JC.ID_AGREEMENT=4 and trunc(jc.export_date) = trunc(sysdate)";
		List<JfsExportFFSlik> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportFFMapperSlik());
		return jfsPayment;
	}
	
	@Override
	public List<JfsExportFFSlik> getJfsExporFFSlikData(String runDate) {
		String SQL = "select jc.text_contract_number,s.KODE_SIFAT_KREDIT_PEMBIAYAAN,s.KODE_JENIS_KREDIT " +
				",s.KODE_SKIM,s.BARU_PERPANJANGAN,s.KODE_JENIS_PENGGUNAAN,s.KODE_ORIENTASI_PENGGUNAAN " +
				",s.TAKEOVER_DARI,s.ALAMAT_EMAIL,s.NO_TELEPON_SELULAR,s.ALAMAT_TEMPAT_BEKERJA,s.PENGHASILAN_KOTOR_PER_TAHUN " +
				",s.KODE_SUMBER_PENGHASILAN,s.JUMLAH_TANGGUNGAN,s.NAMA_PASANGAN,s.NO_IDENTITAS_PASANGAN " +
				",s.TANGGAL_IDENTITAS_PASANGAN,s.PERJANJIAN_PISAH_HARTA,s.KODE_BIDANG_USAHA,s.KODE_GOLONGAN_DEBITUR " +
				",s.KODE_BENTUK_BADAN_USAHA,s.GO_PUBLIC,s.PERINGKAT_RATING_DEBITUR,s.LEMBAGA_PEMERINGKAT_RATING " +
				",s.TANGGAL_PEMERINGKAT,s.NAMA_GRUP_DEBITUR,s.KODE_JENIS_AGUNAN,s.KODE_JENIS_PENGIKATAN,s.TANGGAL_PENGIKATAN " +
				",s.NAMA_PEMILIK_AGUNAN,s.ALAMAT_AGUNAN,s.DATI2,s.STATUS_KREDIT_JOIN,s.DIASURANSIKAN from JFS_CONTRACT jc " +  
				"join jfs_slik_tmp_dwh s on jc.text_contract_number=s.text_contract_number " +
				"WHERE JC.ID_AGREEMENT=4 and trunc(jc.export_date) = TRUNC(to_date('" + runDate + "', 'yyyy-mm-dd'))";
		List<JfsExportFFSlik> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportFFMapperSlik());
		return jfsPayment;
	}
}
