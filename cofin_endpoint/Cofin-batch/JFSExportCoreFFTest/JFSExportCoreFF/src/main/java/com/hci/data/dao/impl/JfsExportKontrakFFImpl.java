package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportKontrakFFDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsExportKontrakFF;
import com.hci.data.entity.JfsExportKontrakFFFooter;
import com.hci.data.mapper.JfsExportMapperKontrakFF;
import com.hci.data.mapper.JfsExportMapperKontrakFFFooter;

public class JfsExportKontrakFFImpl implements JfsExportKontrakFFDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportKontrakFFImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsExportKontrakFF> getJfsExporKontrakFFData() {
		String SQL = "select distinct * from (select c.contract_code,(case when upper(trim(d.name1))=upper(trim(d.name2)) then d.name1 else d.full_name end) as FULL_NAME " +
				",c.SALESROOM_CODE as CODE,c.contract_code as CONTRACT_CODE_1,to_char(ADD_MONTHS(i.due_date, -1),'dd/MM/yyyy') as CREATION_DATE " +
				",fp.credit_amount as CREDIT_AMOUNT,fp.credit_amount as GOODS_PRICE_AMOUNT,fp.terms as TERMS,fp.annuity_amount " +
				",trim(to_char(case when (power(1+fp.interest_rate,1/12)-1)*1200>100 then 99 else (power(1+fp.interest_rate,1/12)-1)*1200 end,'00.00')) as INTEREST_RATE " +
				",fp.credit_amount as CREDIT_AMOUNT_1,jc.send_principal as SEND_PRINCIPAL,fp.terms as TERMS_1 " +
				",to_char(i.due_date,'dd/MM/yyyy') as DUE_DATE,to_char(i1.due_date,'dd/MM/yyyy') as I1_DUE_DATE " +
				",trim(to_char(jc.send_rate,'00.00')) as SEND_RATE,jc.send_instalment " +
				",to_char(c.prepared_to_sign_date, 'dd/MM/yyyy') as CREATION_DATE_CST " +
				",(nvl(fp.cash_payment_amount, 0)-nvl((fp.advanced_payment_number * fp.annuity_amount), 0)-nvl(ft.AMT_FEES_IN_ADVANCE, 0)) as PAYMENT " +
				",es.bi_economic_sector_code as BI_ECONOMIC_SECTOR_CODE,jc.id_agreement as AGREEMENT_ID,ja.partner_id " +
				"from jfs_contract jc left join (select distinct contract_code, prepared_to_sign_date, deal_code, id, loan_purpose, SALESROOM_CODE from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"left join (select distinct name1,name2,full_name,code from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"left join (select distinct contract_id, trunc(creation_date) creation_date from GTT_JSF_001 where contract_status_trans = 'N') cst on c.id = cst.contract_id " +
				"left join (select distinct contract_id,credit_amount,goods_price_amount,terms,annuity_amount,interest_rate,cash_payment_amount,advanced_payment_number,financial_parameters_id from GTT_JSF_003) fp on c.id = fp.contract_id " +
				"left join (select fi.financial_parameters_id,sum ( case when fi.usage_type in ('A', 'S') and fi.charging_periodicity = 'ONE_TIME' " + 
                "THEN fi.ITEM_AMOUNT else 0 end)amt_fees_in_advance from GTT_JSF_003 fi where fi.active_flag = 1 " + 
                "group by fi.financial_parameters_id) ft on ft.financial_parameters_id = fp.FINANCIAL_PARAMETERS_ID " +
                "left join GTT_JSF_007 i on c.id = i.contract_id and i.installment_version = 1 and i.installment_number = 1 " +
                "left join GTT_JSF_007 i1 on c.id = i1.contract_id and i1.installment_version = 1 and i1.installment_number = fp.terms " +
                "LEFT JOIN JFS_COMMODITY_PURPOSE JCP ON JCP.CODE_LOAN_PURPOSE=C.LOAN_PURPOSE " +
                "LEFT JOIN JFS_COMMODITY_TYPE JCT ON JCT.HCI_CODE_COMMODITY_TYPE = JCP.COMMODITY_TYPE_CODE " +
                "left join JFS_COMMODITY_GROUP cg on jct.hci_code_commodity_category=cg.hci_code_commodity_category " +
                "left join JFS_ECONOMIC_SECTOR es on cg.btpn_commodity_group=es.bank_commodity_group " + 
                "left join (select * from jfs_agreement where REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on to_number(ja.code, '99') = jc.id_agreement " +
                "where jc.id_agreement='4' AND jc.export_date=trunc(sysdate) order by c.contract_code)";
		List<JfsExportKontrakFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperKontrakFF());
		return jfsPayment;
	}

	@Override
	public List<JfsExportKontrakFFFooter> getJfsExportKontrakFFFooterData() {
		String SQL = "select count(*) as NUM_OF_ROWS, sum(credit_amount)  as CREDIT_AMOUNT from (select distinct * from (select * " +
				"from jfs_contract jc " +
				"left join (select distinct deal_code, contract_code, id from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"left join (select distinct name1,name2,full_name,code from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"left join (select distinct contract_id, trunc(creation_date) creation_date from GTT_JSF_001 where contract_status_trans = 'N') cst on cst.contract_id = c.id " +
				"left join GTT_JSF_003 fp on c.id = fp.contract_id " +
				"where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement = '4'))";
		List<JfsExportKontrakFFFooter> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperKontrakFFFooter());
		return jfsPayment;
	}
	
	@Override
	public List<JfsExportKontrakFF> getJfsExporKontrakFFData(String runDate) {
		String SQL = "select distinct * from (select c.contract_code,(case when upper(trim(d.name1))=upper(trim(d.name2)) then d.name1 else d.full_name end) as FULL_NAME " +
				",c.SALESROOM_CODE as CODE,c.contract_code as CONTRACT_CODE_1,to_char(ADD_MONTHS(i.due_date, -1),'dd/MM/yyyy') as CREATION_DATE " +
				",fp.credit_amount as CREDIT_AMOUNT,fp.credit_amount as GOODS_PRICE_AMOUNT,fp.terms as TERMS,fp.annuity_amount " +
				",trim(to_char(case when (power(1+fp.interest_rate,1/12)-1)*1200>100 then 99 else (power(1+fp.interest_rate,1/12)-1)*1200 end,'00.00')) as INTEREST_RATE " +
				",fp.credit_amount as CREDIT_AMOUNT_1,jc.send_principal as SEND_PRINCIPAL,fp.terms as TERMS_1 " +
				",to_char(i.due_date,'dd/MM/yyyy') as DUE_DATE,to_char(i1.due_date,'dd/MM/yyyy') as I1_DUE_DATE " +
				",trim(to_char(jc.send_rate,'00.00')) as SEND_RATE,jc.send_instalment " +
				",to_char(c.prepared_to_sign_date, 'dd/MM/yyyy') as CREATION_DATE_CST " +
				",(nvl(fp.cash_payment_amount, 0)-nvl((fp.advanced_payment_number * fp.annuity_amount), 0)-nvl(ft.AMT_FEES_IN_ADVANCE, 0)) as PAYMENT " +
				",es.bi_economic_sector_code as BI_ECONOMIC_SECTOR_CODE,jc.id_agreement as AGREEMENT_ID,ja.partner_id " +
				"from jfs_contract jc left join (select distinct contract_code, prepared_to_sign_date, deal_code, id, loan_purpose, SALESROOM_CODE from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"left join (select distinct name1,name2,full_name,code from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"left join (select distinct contract_id, trunc(creation_date) creation_date from GTT_JSF_001 where contract_status_trans = 'N') cst on c.id = cst.contract_id " +
				"left join (select distinct contract_id,credit_amount,goods_price_amount,terms,annuity_amount,interest_rate,cash_payment_amount,advanced_payment_number,financial_parameters_id from GTT_JSF_003) fp on c.id = fp.contract_id " +
				"left join (select fi.financial_parameters_id,sum ( case when fi.usage_type in ('A', 'S') and fi.charging_periodicity = 'ONE_TIME' " + 
                "THEN fi.ITEM_AMOUNT else 0 end)amt_fees_in_advance from GTT_JSF_003 fi  where fi.active_flag = 1 " + 
                "group by fi.financial_parameters_id) ft on ft.financial_parameters_id = fp.FINANCIAL_PARAMETERS_ID " +
                "left join GTT_JSF_007 i on c.id = i.contract_id and i.installment_version = 1 and i.installment_number = 1 " +
                "left join GTT_JSF_007 i1 on c.id = i1.contract_id and i1.installment_version = 1 and i1.installment_number = fp.terms " +
                "LEFT JOIN JFS_COMMODITY_PURPOSE JCP ON JCP.CODE_LOAN_PURPOSE=C.LOAN_PURPOSE " +
                "LEFT JOIN JFS_COMMODITY_TYPE JCT ON JCT.HCI_CODE_COMMODITY_TYPE = JCP.COMMODITY_TYPE_CODE " +
                "left join JFS_COMMODITY_GROUP cg on jct.hci_code_commodity_category=cg.hci_code_commodity_category " +
                "left join JFS_ECONOMIC_SECTOR es on cg.btpn_commodity_group=es.bank_commodity_group " + 
                "left join (select * from jfs_agreement where REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on to_number(ja.code, '99') = jc.id_agreement " +
                "where jc.id_agreement='4' AND jc.export_date=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) order by c.contract_code)";
		List<JfsExportKontrakFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperKontrakFF());
		return jfsPayment;
	}

	@Override
	public List<JfsExportKontrakFFFooter> getJfsExportKontrakFFFooterData(String runDate) {
		String SQL = "select count(*) as NUM_OF_ROWS, sum(credit_amount)  as CREDIT_AMOUNT from (select distinct * from (select * " +
				"from jfs_contract jc " +
				"left join (select distinct deal_code, contract_code, id from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"left join (select distinct name1,name2,full_name,code from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"left join (select distinct contract_id, trunc(creation_date) creation_date from GTT_JSF_001 where contract_status_trans = 'N') cst on cst.contract_id = c.id " +
				"left join GTT_JSF_003 fp on c.id = fp.contract_id " +
				"where trunc(jc.export_date)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and jc.id_agreement = '4'))";
		List<JfsExportKontrakFFFooter> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperKontrakFFFooter());
		return jfsPayment;
	}
}
