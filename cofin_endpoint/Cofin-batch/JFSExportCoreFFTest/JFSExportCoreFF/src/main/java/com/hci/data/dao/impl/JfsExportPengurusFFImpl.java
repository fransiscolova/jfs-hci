package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportPengurusFFDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsExportPengurusFF;
import com.hci.data.mapper.JfsExportMapperPengurusFF;

public class JfsExportPengurusFFImpl implements JfsExportPengurusFFDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportPengurusFFImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsExportPengurusFF> getJfsExporPengurusFFData() {
		String SQL = "select c.contract_code from jfs_contract jc " +
				"inner join (select distinct contract_code from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement = '4' " +
				"order by jc.id_agreement, c.contract_code";
		List<JfsExportPengurusFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperPengurusFF());
		return jfsPayment;
	}
	
	@Override
	public List<JfsExportPengurusFF> getJfsExporPengurusFFData(String runDate) {
		String SQL = "select c.contract_code from jfs_contract jc " +
				"inner join (select distinct contract_code from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"where trunc(jc.export_date)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and jc.id_agreement = '4' " +
				"order by jc.id_agreement, c.contract_code";
		List<JfsExportPengurusFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportMapperPengurusFF());
		return jfsPayment;
	}
}
