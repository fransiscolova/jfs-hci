package com.hci.data.entity;

public class JfsExportBarangFF {
	private String contractCode;
	private String name;
	private String btpnCodeCommodityCategory;
	private String btpnCodeCommodityType;
	private String commodity;
	private String sisdate;
	private String agreementId;
	private String partnerId;
	
	public String getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getSisdate() {
		return sisdate;
	}
	public void setSisdate(String sisdate) {
		this.sisdate = sisdate;
	}
	public String getContractCode() {
		return contractCode;
	}
	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBtpnCodeCommodityCategory() {
		return btpnCodeCommodityCategory;
	}
	public void setBtpnCodeCommodityCategory(String btpnCodeCommodityCategory) {
		this.btpnCodeCommodityCategory = btpnCodeCommodityCategory;
	}
	public String getBtpnCodeCommodityType() {
		return btpnCodeCommodityType;
	}
	public void setBtpnCodeCommodityType(String btpnCodeCommodityType) {
		this.btpnCodeCommodityType = btpnCodeCommodityType;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
}
