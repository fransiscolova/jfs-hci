package com.hci.data.entity;

import java.sql.Date;

public class JfsExportEnduserFF {
	private String contractCode;
	private String fullName;
	private String nickname;
	private String dstCode;
	private String description;
	private String code8139;
	private String genderType;
	private String ktpNum;
	private String num;
	private String birthPlace;
	private String birth;
	private String streetName;
	private String fullAddress;
	private String village;
	private String subDistrict;
	private String destCodeXD;
	private String zipCode;
	private String value1;
	private String id;
	private String name4;
	private String employerIndustryCode;
	private String nameEmployer;
	private String dstCodeXI;
	private String one;
	private String dstCodeXM;
	private String aid;
	private String dstCodeXR;
	private String income;
	private String validTo;
	private String agreementId;
	private String partnerId;
	
	public String getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getContractCode() {
		return contractCode;
	}
	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getDstCode() {
		return dstCode;
	}
	public void setDstCode(String dstCode) {
		this.dstCode = dstCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode8139() {
		return code8139;
	}
	public void setCode8139(String code8139) {
		this.code8139 = code8139;
	}
	public String getGenderType() {
		return genderType;
	}
	public void setGenderType(String genderType) {
		this.genderType = genderType;
	}
	public String getKtpNum() {
		return ktpNum;
	}
	public void setKtpNum(String ktpNum) {
		this.ktpNum = ktpNum;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getFullAddress() {
		return fullAddress;
	}
	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getSubDistrict() {
		return subDistrict;
	}
	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}
	public String getDestCodeXD() {
		return destCodeXD;
	}
	public void setDestCodeXD(String destCodeXD) {
		this.destCodeXD = destCodeXD;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName4() {
		return name4;
	}
	public void setName4(String name4) {
		this.name4 = name4;
	}
	public String getEmployerIndustryCode() {
		return employerIndustryCode;
	}
	public void setEmployerIndustryCode(String employerIndustryCode) {
		this.employerIndustryCode = employerIndustryCode;
	}
	public String getNameEmployer() {
		return nameEmployer;
	}
	public void setNameEmployer(String nameEmployer) {
		this.nameEmployer = nameEmployer;
	}
	public String getDstCodeXI() {
		return dstCodeXI;
	}
	public void setDstCodeXI(String dstCodeXI) {
		this.dstCodeXI = dstCodeXI;
	}
	public String getOne() {
		return one;
	}
	public void setOne(String one) {
		this.one = one;
	}
	public String getDstCodeXM() {
		return dstCodeXM;
	}
	public void setDstCodeXM(String dstCodeXM) {
		this.dstCodeXM = dstCodeXM;
	}
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getDstCodeXR() {
		return dstCodeXR;
	}
	public void setDstCodeXR(String dstCodeXR) {
		this.dstCodeXR = dstCodeXR;
	}
	public String getIncome() {
		return income;
	}
	public void setIncome(String income) {
		this.income = income;
	}
	public String getValidTo() {
		return validTo;
	}
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
}