package com.hci.data.entity;

public class JfsExportFFSlik {
	private String textContractNumber;
	private String kodeSifatKreditPemiayaan;
	private String kodeJenisKredit;
	private String kodeSkim;
	private String baruPerpanjangan;
	private String kodeJenisPenggunaan;
	private String kodeOrientasiPenggunaan;
	private String takeoverDari;
	private String alamatEmail;
	private String noTeleponSelular;
	private String alamatTempatBekerja;
	private String penghasilanKotorPerTahun;
	private String kodeSumberPenghasilan;
	private String jumlahTanggungan;
	private String namaPasangan;
	private String noIdentitasPasangan;
	private String tanggalIdentitasPasangan;
	private String perjanjianPisahHarta;
	private String kodeBidangUsaha;
	private String kodeGolonganDebitur;
	private String kodeBentukBadanUsaha;
	private String goPublic;
	private String peringkatRatingDebitur;
	private String lembagaPemeringkatRating;
	private String tanggalPemeringkat;
	private String namaGroupDebitur;
	private String kodeJenisAgunan;
	private String kodeJenisPengikatan;
	private String tanggalPengikatan;
	private String namaPemilikAgunan;
	private String alamatAgunan;
	private String statusKreditJoin;
	private String dati2;
	private String diasuransikan;
	
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getKodeSifatKreditPemiayaan() {
		return kodeSifatKreditPemiayaan;
	}
	public void setKodeSifatKreditPemiayaan(String kodeSifatKreditPemiayaan) {
		this.kodeSifatKreditPemiayaan = kodeSifatKreditPemiayaan;
	}
	public String getKodeJenisKredit() {
		return kodeJenisKredit;
	}
	public void setKodeJenisKredit(String kodeJenisKredit) {
		this.kodeJenisKredit = kodeJenisKredit;
	}
	public String getKodeSkim() {
		return kodeSkim;
	}
	public void setKodeSkim(String kodeSkim) {
		this.kodeSkim = kodeSkim;
	}
	public String getBaruPerpanjangan() {
		return baruPerpanjangan;
	}
	public void setBaruPerpanjangan(String baruPerpanjangan) {
		this.baruPerpanjangan = baruPerpanjangan;
	}
	public String getKodeJenisPenggunaan() {
		return kodeJenisPenggunaan;
	}
	public void setKodeJenisPenggunaan(String kodeJenisPenggunaan) {
		this.kodeJenisPenggunaan = kodeJenisPenggunaan;
	}
	public String getKodeOrientasiPenggunaan() {
		return kodeOrientasiPenggunaan;
	}
	public void setKodeOrientasiPenggunaan(String kodeOrientasiPenggunaan) {
		this.kodeOrientasiPenggunaan = kodeOrientasiPenggunaan;
	}
	public String getTakeoverDari() {
		return takeoverDari;
	}
	public void setTakeoverDari(String takeoverDari) {
		this.takeoverDari = takeoverDari;
	}
	public String getAlamatEmail() {
		return alamatEmail;
	}
	public void setAlamatEmail(String alamatEmail) {
		this.alamatEmail = alamatEmail;
	}
	public String getNoTeleponSelular() {
		return noTeleponSelular;
	}
	public void setNoTeleponSelular(String noTeleponSelular) {
		this.noTeleponSelular = noTeleponSelular;
	}
	public String getAlamatTempatBekerja() {
		return alamatTempatBekerja;
	}
	public void setAlamatTempatBekerja(String alamatTempatBekerja) {
		this.alamatTempatBekerja = alamatTempatBekerja;
	}
	public String getPenghasilanKotorPerTahun() {
		return penghasilanKotorPerTahun;
	}
	public void setPenghasilanKotorPerTahun(String penghasilanKotorPerTahun) {
		this.penghasilanKotorPerTahun = penghasilanKotorPerTahun;
	}
	public String getKodeSumberPenghasilan() {
		return kodeSumberPenghasilan;
	}
	public void setKodeSumberPenghasilan(String kodeSumberPenghasilan) {
		this.kodeSumberPenghasilan = kodeSumberPenghasilan;
	}
	public String getJumlahTanggungan() {
		return jumlahTanggungan;
	}
	public void setJumlahTanggungan(String jumlahTanggungan) {
		this.jumlahTanggungan = jumlahTanggungan;
	}
	public String getNamaPasangan() {
		return namaPasangan;
	}
	public void setNamaPasangan(String namaPasangan) {
		this.namaPasangan = namaPasangan;
	}
	public String getNoIdentitasPasangan() {
		return noIdentitasPasangan;
	}
	public void setNoIdentitasPasangan(String noIdentitasPasangan) {
		this.noIdentitasPasangan = noIdentitasPasangan;
	}
	public String getTanggalIdentitasPasangan() {
		return tanggalIdentitasPasangan;
	}
	public void setTanggalIdentitasPasangan(String tanggalIdentitasPasangan) {
		this.tanggalIdentitasPasangan = tanggalIdentitasPasangan;
	}
	public String getPerjanjianPisahHarta() {
		return perjanjianPisahHarta;
	}
	public void setPerjanjianPisahHarta(String perjanjianPisahHarta) {
		this.perjanjianPisahHarta = perjanjianPisahHarta;
	}
	public String getKodeBidangUsaha() {
		return kodeBidangUsaha;
	}
	public void setKodeBidangUsaha(String kodeBidangUsaha) {
		this.kodeBidangUsaha = kodeBidangUsaha;
	}
	public String getKodeGolonganDebitur() {
		return kodeGolonganDebitur;
	}
	public void setKodeGolonganDebitur(String kodeGolonganDebitur) {
		this.kodeGolonganDebitur = kodeGolonganDebitur;
	}
	public String getKodeBentukBadanUsaha() {
		return kodeBentukBadanUsaha;
	}
	public void setKodeBentukBadanUsaha(String kodeBentukBadanUsaha) {
		this.kodeBentukBadanUsaha = kodeBentukBadanUsaha;
	}
	public String getGoPublic() {
		return goPublic;
	}
	public void setGoPublic(String goPublic) {
		this.goPublic = goPublic;
	}
	public String getPeringkatRatingDebitur() {
		return peringkatRatingDebitur;
	}
	public void setPeringkatRatingDebitur(String peringkatRatingDebitur) {
		this.peringkatRatingDebitur = peringkatRatingDebitur;
	}
	public String getLembagaPemeringkatRating() {
		return lembagaPemeringkatRating;
	}
	public void setLembagaPemeringkatRating(String lembagaPemeringkatRating) {
		this.lembagaPemeringkatRating = lembagaPemeringkatRating;
	}
	public String getTanggalPemeringkat() {
		return tanggalPemeringkat;
	}
	public void setTanggalPemeringkat(String tanggalPemeringkat) {
		this.tanggalPemeringkat = tanggalPemeringkat;
	}
	public String getNamaGroupDebitur() {
		return namaGroupDebitur;
	}
	public void setNamaGroupDebitur(String namaGroupDebitur) {
		this.namaGroupDebitur = namaGroupDebitur;
	}
	public String getKodeJenisAgunan() {
		return kodeJenisAgunan;
	}
	public void setKodeJenisAgunan(String kodeJenisAgunan) {
		this.kodeJenisAgunan = kodeJenisAgunan;
	}
	public String getKodeJenisPengikatan() {
		return kodeJenisPengikatan;
	}
	public void setKodeJenisPengikatan(String kodeJenisPengikatan) {
		this.kodeJenisPengikatan = kodeJenisPengikatan;
	}
	public String getTanggalPengikatan() {
		return tanggalPengikatan;
	}
	public void setTanggalPengikatan(String tanggalPengikatan) {
		this.tanggalPengikatan = tanggalPengikatan;
	}
	public String getNamaPemilikAgunan() {
		return namaPemilikAgunan;
	}
	public void setNamaPemilikAgunan(String namaPemilikAgunan) {
		this.namaPemilikAgunan = namaPemilikAgunan;
	}
	public String getAlamatAgunan() {
		return alamatAgunan;
	}
	public void setAlamatAgunan(String alamatAgunan) {
		this.alamatAgunan = alamatAgunan;
	}
	public String getStatusKreditJoin() {
		return statusKreditJoin;
	}
	public void setStatusKreditJoin(String statusKreditJoin) {
		this.statusKreditJoin = statusKreditJoin;
	}
	public String getDati2() {
		return dati2;
	}
	public void setDati2(String dati2) {
		this.dati2 = dati2;
	}
	public String getDiasuransikan() {
		return diasuransikan;
	}
	public void setDiasuransikan(String diasuransikan) {
		this.diasuransikan = diasuransikan;
	}
}
