package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsExportBarangFF;

public class JfsExportMapperBarangFF implements RowMapper<JfsExportBarangFF> {

	@Override
	public JfsExportBarangFF mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsExportBarangFF jfsPayment = new JfsExportBarangFF();
		
		jfsPayment.setContractCode(rs.getString("CONTRACT_CODE") != null ? rs.getString("CONTRACT_CODE") : "");
		jfsPayment.setName(rs.getString("NAME") != null ? rs.getString("NAME") : "");
		jfsPayment.setBtpnCodeCommodityCategory(rs.getString("BTPN_CODE_COMMODITY_CATEGORY") != null ? rs.getString("BTPN_CODE_COMMODITY_CATEGORY") : "");
		jfsPayment.setBtpnCodeCommodityType(rs.getString("BTPN_CODE_COMMODITY_TYPE") != null ? rs.getString("BTPN_CODE_COMMODITY_TYPE") : "");
		jfsPayment.setSisdate(rs.getString("SISDATE") != null ? rs.getString("SISDATE") : "");
		jfsPayment.setCommodity(rs.getString("COMMODITY") != null ? rs.getString("COMMODITY") : "");
		jfsPayment.setAgreementId(rs.getString("AGREEMENT_ID") != null ? rs.getString("AGREEMENT_ID") : "");
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID") != null ? rs.getString("PARTNER_ID") : "");
		
		return jfsPayment;
	}
	
}
