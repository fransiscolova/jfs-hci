package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsExportEnduserFF;

public class JfsExportMapperEnduserFF implements RowMapper<JfsExportEnduserFF> {

	@Override
	public JfsExportEnduserFF mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsExportEnduserFF jfsPayment = new JfsExportEnduserFF();
		
		jfsPayment.setContractCode(rs.getString("CONTRACT_CODE") != null ? rs.getString("CONTRACT_CODE") : "");
		jfsPayment.setFullName(rs.getString("FULL_NAME") != null ? rs.getString("FULL_NAME") : "");
		jfsPayment.setNickname(rs.getString("NICKNAME") != null ? rs.getString("NICKNAME") : "");
		jfsPayment.setDstCode(rs.getString("DST_CODE") != null ? rs.getString("DST_CODE") : "");
		jfsPayment.setDescription(rs.getString("DESCRIPTION") != null ? rs.getString("DESCRIPTION") : "");
		jfsPayment.setCode8139(rs.getString("CODE8139") != null ? rs.getString("CODE8139") : "");
		jfsPayment.setGenderType(rs.getString("GENDER_TYPE") != null ? rs.getString("GENDER_TYPE") : "");
		jfsPayment.setKtpNum(rs.getString("KTP_NUM") != null ? rs.getString("KTP_NUM") : "");
		jfsPayment.setNum(rs.getString("NUM") != null ? rs.getString("NUM") : "");
		jfsPayment.setBirthPlace(rs.getString("BIRTH_PLACE") != null ? rs.getString("BIRTH_PLACE") : "");
		jfsPayment.setBirth(rs.getString("BIRTH") != null ? rs.getString("BIRTH") : "");
		jfsPayment.setStreetName(rs.getString("STREET_NAME") != null ? rs.getString("STREET_NAME") : "");
		jfsPayment.setFullAddress(rs.getString("FULL_ADDRESS") != null ? rs.getString("FULL_ADDRESS") : "");
		jfsPayment.setVillage(rs.getString("VILLAGE") != null ? rs.getString("VILLAGE") : "");
		jfsPayment.setSubDistrict(rs.getString("SUB_DISTRICT") != null ? rs.getString("SUB_DISTRICT") : "");
		jfsPayment.setDestCodeXD(rs.getString("DST_CODE_XD") != null ? rs.getString("DST_CODE_XD") : "");
		jfsPayment.setZipCode(rs.getString("ZIP_CODE") != null ? rs.getString("ZIP_CODE") : "");
		jfsPayment.setValue1(rs.getString("VALUE1") != null ? rs.getString("VALUE1") : "");
		jfsPayment.setId(rs.getString("ID") != null ? rs.getString("ID") : "");
		jfsPayment.setName4(rs.getString("NAME4") != null ? rs.getString("NAME4") : "");
		jfsPayment.setEmployerIndustryCode(rs.getString("EMPLOYER_INDUSTRY_CODE") != null ? rs.getString("EMPLOYER_INDUSTRY_CODE") : "");
		jfsPayment.setNameEmployer(rs.getString("NAME_EMPLOYER") != null ? rs.getString("NAME_EMPLOYER") : "");
		jfsPayment.setDstCodeXI(rs.getString("DST_CODE_XI") != null ? rs.getString("DST_CODE_XI") : "");
		jfsPayment.setOne(rs.getString("ONE") != null ? rs.getString("ONE") : "");
		jfsPayment.setDstCodeXM(rs.getString("DST_CODE_XM") != null ? rs.getString("DST_CODE_XM") : "");
		jfsPayment.setAid(rs.getString("AID") != null ? rs.getString("AID") : "");
		jfsPayment.setDstCodeXR(rs.getString("DST_CODE_XR") != null ? rs.getString("DST_CODE_XR") : "");
		jfsPayment.setIncome(rs.getString("INCOME") != null ? rs.getString("INCOME") : "");
		jfsPayment.setValidTo(rs.getString("VALID_TO") != null ? rs.getString("VALID_TO") : "");
		jfsPayment.setAgreementId(rs.getString("AGREEMENT_ID") != null ? rs.getString("AGREEMENT_ID") : "");
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID") != null ? rs.getString("PARTNER_ID") : "");
		
		return jfsPayment;
	}
	
}
