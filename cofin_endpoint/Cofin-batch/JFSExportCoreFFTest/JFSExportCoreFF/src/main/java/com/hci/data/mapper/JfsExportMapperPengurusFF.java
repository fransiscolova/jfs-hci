package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsExportPengurusFF;

public class JfsExportMapperPengurusFF implements RowMapper<JfsExportPengurusFF> {

	@Override
	public JfsExportPengurusFF mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsExportPengurusFF jfsPayment = new JfsExportPengurusFF();
		
		jfsPayment.setContractCode(rs.getString("CONTRACT_CODE") != null ? rs.getString("CONTRACT_CODE") : "");
		
		return jfsPayment;
	}
	
}
