package com.hci.job.interfaces;

import java.util.Date;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.JobLauncherJFSExportFF;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ReExecuteJob;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.job.sub.JFSExportBarangFFSub;
import com.hci.job.sub.JFSExportEnduserFFSub;
import com.hci.job.sub.JFSExportFFSlikSub;
import com.hci.job.sub.JFSExportFFSub;
import com.hci.job.sub.JFSExportKontrakFFSub;
import com.hci.job.sub.JFSExportPengurusFFSub;
import com.hci.util.DateUtil;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportFFRerun implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private ParentJobDao parentDao;
	private String targetFol;
	private String jobName;
	private String processName;
	
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		
		if(!checkTodayJob(processName)) {
			log.info("JFSExportFFRerun", "Today job not yet run");
			if(checkParentJob(processName)) {
				JobDetail jd = new JobDetail();
				try {
					jd = new JobDetail("JFSPaymentRun", Scheduler.DEFAULT_GROUP, JobLauncherJFSExportFF.class);
					jd.getJobDataMap().put("test", "testApp");
					Trigger tgr = TriggerUtils.makeImmediateTrigger(0, 0);
					tgr.setName("FireOnceRun");
					
					Scheduler scheduler = new StdSchedulerFactory().getScheduler();
					scheduler.start();
					scheduler.scheduleJob(jd, tgr);
				} catch (SchedulerException e) {
					e.printStackTrace();
				}
				new PropertyUtil().setProperty("batch.job.run.status", "N");
				return RepeatStatus.FINISHED;
			}
			log.info("JFSExportFFRerun", "Parent job of today job not yet run");
		}
		
		log.info("JFSExportFFRerun", "Rerun the job");
		
		List<ReExecuteJob> jobs = getAllReExecuteJob(jobName);
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		if(jobs.size() != 0) {
			for(ReExecuteJob job : jobs) {
				int numOfRerun = job.getNumOfRerun() + 1;
			
				paymentFileStatus = new JFSExportEnduserFFSub().createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
				paymentFileStatus = new JFSExportKontrakFFSub().createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
				paymentFileStatus = new JFSExportBarangFFSub().createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
				paymentFileStatus = new JFSExportPengurusFFSub().createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "N", processName);
				paymentFileStatus = new JFSExportFFSlikSub().createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "N", processName);
			}
		}
		
		log.info("JFSExportFFRerun", "Rerun job finish");
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}
	
	public List<ReExecuteJob> getAllReExecuteJob(String jobName) {
		List<ReExecuteJob> jobs = parentDao.getRerunJob(jobName);
		return jobs;
	}
	
	public boolean checkTodayJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
}