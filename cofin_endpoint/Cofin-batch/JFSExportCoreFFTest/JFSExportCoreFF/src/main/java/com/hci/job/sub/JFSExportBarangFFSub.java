package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsExportBarangFFDao;
import com.hci.data.entity.JfsExportBarangFF;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportBarangFFSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsExportBarangFF> getAllData() {
		JfsExportBarangFFDao jfsPaymentDao = (JfsExportBarangFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportBarangFFDao");
		List<JfsExportBarangFF> jfsPayments = jfsPaymentDao.getJfsExporBarangFFData();
		return jfsPayments;
	}
	
	public List<JfsExportBarangFF> getAllData(String runDate) {
		JfsExportBarangFFDao jfsPaymentDao = (JfsExportBarangFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportBarangFFDao");
		List<JfsExportBarangFF> jfsPayments = jfsPaymentDao.getJfsExporBarangFFData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		int numOfData = 0;
		
		List<JfsExportBarangFF> barangList;
		if (runDate.equals("")) {
			barangList = getAllData();
		} else {
			barangList = getAllData(runDate);
		}
		
		log.info("JFSExportBarangFFSub", "Num of file lines : " + barangList.size());
		
		for(JfsExportBarangFF jp : barangList) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getContractCode() + "|||" + jp.getName() + "|||||" + jp.getBtpnCodeCommodityCategory() + "|" + jp.getBtpnCodeCommodityType() + "|N|" + jp.getSisdate() +
					"|" + jp.getCommodity() + "|MPF");
			numOfData++;
		}
		
		paymentDetailDataString.add("HCI|" + numOfData + "|0");
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSExportBarangFFSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "BARANG_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "BARANG_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|3|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, targetFol + "BARANG_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt");
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, targetFol + "BARANG_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt");
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSExportBarangFFSub", "JobStatus : " + jobStatus);
		log.info("JFSExportBarangFFSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
