package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsExportEnduserFFDao;
import com.hci.data.entity.JfsExportBarangFF;
import com.hci.data.entity.JfsExportEnduserFF;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportEnduserFFSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsExportEnduserFF> getAllData() {
		JfsExportEnduserFFDao jfsPaymentDao = (JfsExportEnduserFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportEnduserFFDao");
		List<JfsExportEnduserFF> jfsPayments = jfsPaymentDao.getJfsExporEndusertFFData();
		return jfsPayments;
	}
	
	public List<JfsExportEnduserFF> getAllData(String runDate) {
		JfsExportEnduserFFDao jfsPaymentDao = (JfsExportEnduserFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportEnduserFFDao");
		List<JfsExportEnduserFF> jfsPayments = jfsPaymentDao.getJfsExporEndusertFFData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		int numOfRow = 0;
		paymentDetailDataString.add(header);
		
		List<JfsExportEnduserFF> enduserList;
		if (runDate.equals("")) {
			enduserList = getAllData();
		} else {
			enduserList = getAllData(runDate);
		}
		
		log.info("JFSExportEnduserFFSub", "Num of file lines : " + enduserList.size());
		
		for(JfsExportEnduserFF jp : enduserList) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getContractCode() + "|" + jp.getFullName() + "|" + jp.getNickname() + "|" + jp.getDstCode() + "|" + jp.getDescription() + "|" + jp.getCode8139() + "|" + jp.getGenderType() + "|" + jp.getKtpNum() + "|" +
					jp.getNum() + "||" + jp.getBirthPlace() + "|" + jp.getBirth() + "|||" + jp.getStreetName() + jp.getFullAddress() + "|" + jp.getVillage() + "|" + jp.getSubDistrict() + "|" + jp.getDestCodeXD() + "|" +
					jp.getZipCode() + "||" + jp.getValue1() + "|" + jp.getId() + "|" + jp.getName4() + "|" + jp.getEmployerIndustryCode() + "|" + jp.getNameEmployer() + "|" + jp.getDstCodeXI() + "|" + jp.getOne() + "|" + jp.getDstCodeXM() + "|" + jp.getAid() + "|" + jp.getDstCodeXR() + "|" + jp.getIncome() + "|" + jp.getValidTo());
			numOfRow++;
		}
		
		paymentDetailDataString.add("HCI|" + numOfRow + "|0");
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSExportEnduserFFSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "ENDUSER_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "ENDUSER_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|1|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {	
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSExportEnduserFFSub", "JobStatus : " + jobStatus);
		log.info("JFSExportEnduserFFSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
