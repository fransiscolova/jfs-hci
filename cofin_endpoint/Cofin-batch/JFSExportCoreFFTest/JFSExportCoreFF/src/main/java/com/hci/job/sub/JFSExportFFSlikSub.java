package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsExportFFSlikDao;
import com.hci.data.entity.JfsExportFFSlik;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportFFSlikSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsExportFFSlik> getAllData() {
		JfsExportFFSlikDao jfsPaymentDao = (JfsExportFFSlikDao) SpringContextHolder.getApplicationContext().getBean("jfsExportFFSlikDao");
		List<JfsExportFFSlik> jfsPayments = jfsPaymentDao.getJfsExporFFSlikData();
		return jfsPayments;
	}
	
	public List<JfsExportFFSlik> getAllData(String runDate) {
		JfsExportFFSlikDao jfsPaymentDao = (JfsExportFFSlikDao) SpringContextHolder.getApplicationContext().getBean("jfsExportFFSlikDao");
		List<JfsExportFFSlik> jfsPayments = jfsPaymentDao.getJfsExporFFSlikData(runDate);
		return jfsPayments;
	}
	
	public List<String> createPaymentFileData(String header, String runDate) {
		List<String> paymentDataString = new ArrayList<String>();
		paymentDataString.add(header);
		int numOfRow = 0;
		
		List<JfsExportFFSlik> lists;
		if (runDate.equals("")) {
			lists = getAllData();
		} else {
			lists = getAllData(runDate);
		}
		
		log.info("JFS2ExportSlikSub", "Num of file lines : " + lists.size());
		
		for(JfsExportFFSlik jp : lists) {
			
			paymentDataString.add(jp.getTextContractNumber() + "|" + jp.getKodeSifatKreditPemiayaan() + "|" + jp.getKodeJenisKredit() + "|" + jp.getKodeSkim() + "|" + 
					jp.getBaruPerpanjangan() + "|" + jp.getKodeJenisPenggunaan() + "|" + jp.getKodeOrientasiPenggunaan() + "|" + jp.getTakeoverDari() + "|" + 
					jp.getAlamatEmail() + "|" + jp.getNoTeleponSelular() + "|" + jp.getAlamatTempatBekerja() + "|" + jp.getPenghasilanKotorPerTahun() + "|" +  
					jp.getKodeSumberPenghasilan() + "|" + jp.getJumlahTanggungan() + "|" + jp.getNamaPasangan() + "|" + jp.getNoIdentitasPasangan() + "|" + 
					jp.getTanggalIdentitasPasangan() + "|" + jp.getPerjanjianPisahHarta() + "|" + jp.getKodeBidangUsaha() + "|" + jp.getKodeGolonganDebitur() + "|" + 
					jp.getKodeBentukBadanUsaha() + "|" + jp.getGoPublic() + "|" + jp.getPeringkatRatingDebitur() + "|" + jp.getLembagaPemeringkatRating() + "|" + 
					jp.getTanggalPemeringkat() + "|" +  jp.getNamaGroupDebitur() + "|" + jp.getKodeJenisAgunan() + "|" + jp.getKodeJenisPengikatan() + "|" + 
					jp.getTanggalPengikatan() + "|" + jp.getNamaPemilikAgunan() + "|" + jp.getAlamatAgunan() + "|" + jp.getStatusKreditJoin() + "|" + 
					jp.getDati2() + "|" + jp.getDiasuransikan());
			numOfRow++;
		}
		
		paymentDataString.add("HCI|" + numOfRow + "|0");
		
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportSlikSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "SLIK_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "SLIK_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		List<String> dataToWrite = createPaymentFileData("HCI|31|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if(dataToWrite.size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus,  aesFileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSExportFFSlikSub", "JobStatus : " + jobStatus);
		log.info("JFSExportFFSlikSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
