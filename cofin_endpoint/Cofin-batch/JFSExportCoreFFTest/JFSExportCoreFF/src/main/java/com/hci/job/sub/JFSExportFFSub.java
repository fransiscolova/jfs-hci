package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.hci.data.dao.JfsExportBarangFFDao;
import com.hci.data.dao.JfsExportFFDao;
import com.hci.data.entity.JfsExportBarangFF;
import com.hci.data.entity.JfsExportFF;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportFFSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsExportFF> getAllData() {
		JfsExportFFDao jfsPaymentDao = (JfsExportFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportFFDao");
		List<JfsExportFF> jfsPayments = jfsPaymentDao.getJfsExporFFData();
		return jfsPayments;
	}
	
	public List<String> createPaymentFileData(String header) {
		List<String> paymentDataString = new ArrayList<String>();
		
		List<JfsExportFF> exportList = getAllData();
		log.info("JFSExportFFSub", "Num of file lines : " + exportList.size());
		
		for(JfsExportFF jp : exportList) {
			paymentDataString.add(jp.getType() + ";" + jp.getDateSignature() + ";" + jp.getBtpnPortion() + ";" + jp.getHcidPortion() + ";" + jp.getCntContract());
		}
		
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSExportFFSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		List<String> dataToWrite = createPaymentFileData("TYPE;DATE_SIGNATURE;BTPN_PORTION;HCID_PORTION;CNT_CONTRACT");
		
		if(dataToWrite.size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new ShellConnectionObject("WS_account", "3Makas!H", "10.56.8.49", "", 22);
		new SSHUtil().removeAndWriteToFile(shellCObj, targetFol + "JFS_EXPORT_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt", dataToWrite);
		
		return true;
	}
}
