package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsExportKontrakFFDao;
import com.hci.data.entity.JfsExportEnduserFF;
import com.hci.data.entity.JfsExportKontrakFF;
import com.hci.data.entity.JfsExportKontrakFFFooter;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportKontrakFFSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsExportKontrakFF> getAllData() {
		JfsExportKontrakFFDao jfsPaymentDao = (JfsExportKontrakFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportKontrakFFDao");
		List<JfsExportKontrakFF> jfsPayments = jfsPaymentDao.getJfsExporKontrakFFData();
		return jfsPayments;
	}
	
	public List<JfsExportKontrakFFFooter> getFooterData() {
		JfsExportKontrakFFDao jfsPaymentDao = (JfsExportKontrakFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportKontrakFFDao");
		List<JfsExportKontrakFFFooter> jfsPayments = jfsPaymentDao.getJfsExportKontrakFFFooterData();
		return jfsPayments;
	}
	
	public List<JfsExportKontrakFF> getAllData(String runDate) {
		JfsExportKontrakFFDao jfsPaymentDao = (JfsExportKontrakFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportKontrakFFDao");
		List<JfsExportKontrakFF> jfsPayments = jfsPaymentDao.getJfsExporKontrakFFData(runDate);
		return jfsPayments;
	}
	
	public List<JfsExportKontrakFFFooter> getFooterData(String runDate) {
		JfsExportKontrakFFDao jfsPaymentDao = (JfsExportKontrakFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportKontrakFFDao");
		List<JfsExportKontrakFFFooter> jfsPayments = jfsPaymentDao.getJfsExportKontrakFFFooterData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		
		List<JfsExportKontrakFF> kontrakList;
		List<JfsExportKontrakFFFooter> kontrakFooterList;
		if (runDate.equals("")) {
			kontrakList = getAllData();
			kontrakFooterList = getFooterData();
		} else {
			kontrakList = getAllData(runDate);
			kontrakFooterList = getFooterData(runDate);
		}
		
		log.info("JFSExportKontrakFFSub", "Num of file lines : " + kontrakList.size());
		log.info("JFSExportKontrakFFSub", "Num of footer file lines : " + kontrakFooterList.size());
		
		for(JfsExportKontrakFF jp : kontrakList) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			/*paymentDetailDataString.add(jp.getContractCode() + "|" + jp.getFullName() + "|" + jp.getCode() + "|" + jp.getContractCode() +
					jp.getDueDate() + "|" + jp.getCreditAmount() + "|" + jp.getCreditAmount1() + "|" + jp.getTerms() + "|" + jp.getAnnuityAmount() +
					jp.getInterestRate() + "|" + jp.getCreditAmount1() + "|" + jp.getSendPrincipal() + "|" + jp.getTerms1() + "|" +
					jp.getI1DueDate() + "|" + jp.getI1DueDate() + "|" + jp.getSendRate() + "|" + jp.getSendInstalment() + "|1|N|T|0|E|DG|0||" + 
					jp.getPreparedToSignDate() + "|0|" + jp.getPayment() + "|" + jp.getBiEconomicSectorCode() + "|K|||||||||||||");*/
			paymentDetailDataString.add(jp.getContractCode() + "|" + jp.getFullName() + "|" + jp.getCode() + "|" + jp.getContractCode1() + "|" + jp.getCreationDate() + "|" + jp.getCreditAmount() + 
					"|" + jp.getGoodsPriceAmount() + "|" + jp.getTerms() + "|" + jp.getAnnuityAmount() + "|" + jp.getInterestRate() + "|" + jp.getCreditAmount() + "|" + jp.getSendPrincipal() + "|" + jp.getTerms() + "|" + jp.getDueDate() + "|" + jp.getI1DueDate() + "|" + jp.getSendRate() + "|" + jp.getSendInstalment() + 
					"|1|N|T|0|E|DG|0||" + jp.getCreationDateCst() + "|0|" + jp.getPayment() + "||" + jp.getBiEconomicSectorCode() + "|K|||||||||||||");
		}
		
		for(JfsExportKontrakFFFooter jp : kontrakFooterList) {
			paymentDetailDataString.add("HCI|" + jp.getNumOfRows() + "|" + jp.getCreditAmount());
		}
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSExportKontrakFFSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "KONTRAK_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "KONTRAK_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|2|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSExportKontrakFFSub", "JobStatus : " + jobStatus);
		log.info("JFSExportKontrakFFSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
