package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsExportPengurusFFDao;
import com.hci.data.entity.JfsExportKontrakFF;
import com.hci.data.entity.JfsExportKontrakFFFooter;
import com.hci.data.entity.JfsExportPengurusFF;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportPengurusFFSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsExportPengurusFF> getAllData() {
		JfsExportPengurusFFDao jfsPaymentDao = (JfsExportPengurusFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportPengurusFFDao");
		List<JfsExportPengurusFF> jfsPayments = jfsPaymentDao.getJfsExporPengurusFFData();
		return jfsPayments;
	}
	
	public List<JfsExportPengurusFF> getAllData(String runDate) {
		JfsExportPengurusFFDao jfsPaymentDao = (JfsExportPengurusFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportPengurusFFDao");
		List<JfsExportPengurusFF> jfsPayments = jfsPaymentDao.getJfsExporPengurusFFData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		int numOfRow = 0;
		
		List<JfsExportPengurusFF> pengurusList;
		if (runDate.equals("")) {
			pengurusList = getAllData();
		} else {
			pengurusList = getAllData(runDate);
		}
		
		log.info("JFSExportPengurusFFSub", "Num of file lines : " + pengurusList.size());
		
		for(JfsExportPengurusFF jp : pengurusList) {
			paymentDetailDataString.add(jp.getContractCode() + "||||||||||||||");
			numOfRow++;
		}
		
		paymentDetailDataString.add("HCI|" + numOfRow + "|0");
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSExportPengurusFFSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "PENGURUS_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "PENGURUS_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|4|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSExportPengurusFFSub", "JobStatus : " + jobStatus);
		log.info("JFSExportPengurusFFSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
