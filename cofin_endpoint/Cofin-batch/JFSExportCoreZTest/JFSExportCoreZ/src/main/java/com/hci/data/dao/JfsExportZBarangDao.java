package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JFSExportZBarang;

public interface JfsExportZBarangDao {
	public List<JFSExportZBarang> getJfsExporZBarangData();
	public List<JFSExportZBarang> getJfsExporZBarangData(String rowDate);
}
