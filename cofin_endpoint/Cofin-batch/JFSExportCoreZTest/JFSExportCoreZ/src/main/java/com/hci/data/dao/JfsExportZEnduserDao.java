package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JFSExportZEnduser;

public interface JfsExportZEnduserDao {
	public List<JFSExportZEnduser> getJfsExporZEndusertData();
	public List<JFSExportZEnduser> getJfsExporZEndusertData(String runDate);
}
