package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JFSExportZKontrak;
import com.hci.data.entity.JFSExportZKontrakFooter;

public interface JfsExportZKontrakDao {
	public List<JFSExportZKontrak> getJfsExporZKontrakData();
	public List<JFSExportZKontrakFooter> getJfsExportZKontrakFooterData();
	public List<JFSExportZKontrak> getJfsExporZKontrakData(String runDate);
	public List<JFSExportZKontrakFooter> getJfsExportZKontrakFooterData(String runDate);
}
