package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JFSExportZPengurus;

public interface JfsExportZPengurusDao {
	public List<JFSExportZPengurus> getJfsExporZPengurusData();
	public List<JFSExportZPengurus> getJfsExporZPengurusData(String runDate);
}
