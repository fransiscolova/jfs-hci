package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JFSExportZSlik;

public interface JfsExportZSlikDao {
	public List<JFSExportZSlik> getJfsExporZSlikData();
	public List<JFSExportZSlik> getJfsExporZSlikData(String runDate);
}
