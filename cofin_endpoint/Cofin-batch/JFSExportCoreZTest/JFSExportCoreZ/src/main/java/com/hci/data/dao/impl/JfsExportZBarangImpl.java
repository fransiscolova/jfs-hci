package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportZBarangDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JFSExportZBarang;
import com.hci.data.mapper.JFSExportZMapperBarang;

public class JfsExportZBarangImpl implements JfsExportZBarangDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportZBarangImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JFSExportZBarang> getJfsExporZBarangData() {
		String SQL = "select distinct * from (select c.contract_code,(case when upper(trim(d.name1))=upper(trim(d.name2)) then d.name1 else d.full_name end) as NAME " +
				",hc.engine_number,substr(hc.model_number||' '||hc.serial_number,1,22) as MODEL_SERIAL_NUMBER " +
				",ct.btpn_code_commodity_category as BTPN_CODE_COMMODITY_CATEGORY,ct.btpn_code_commodity_type as BTPN_CODE_COMMODITY_TYPE " +
				",to_char(sysdate,'YYYY') as SISDATE,case when ct.btpn_code_commodity_type in ('CT_CT_PDA', 'CT_CT_MTAB') then 'DGN2' " +
				"else cg.btpn_commodity_group end as COMMODITY,hc.producer,jc.id_agreement as AGREEMENT_ID,ja.partner_id from jfs_contract jc " +
				"left join (select distinct contract_code, deal_code, id from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"left join (select distinct name1,name2,full_name,code from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"left join GTT_JSF_004 hc on c.id = hc.contract_id " +
				"left join jfs_commodity_type ct on hc.commodity_category_code=ct.hci_code_commodity_category and hc.COMMODITY_TYPE_CODE=ct.hci_code_commodity_type " +
				"left join JFS_COMMODITY_GROUP cg on hc.commodity_category_code=cg.hci_code_commodity_category and jc.id_agreement = cg.id_agreement " +
				"left join (select * from jfs_agreement where REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on to_number(ja.code, '99') = jc.id_agreement " +
				"where trunc(jc.export_date)=trunc(sysdate) and jc.id_agreement = 5 order by jc.id_agreement, c.contract_code)";
		List<JFSExportZBarang> jfsPayment = jdbcTemplateObject.query(SQL, new JFSExportZMapperBarang());
		return jfsPayment;
	}
	
	@Override
	public List<JFSExportZBarang> getJfsExporZBarangData(String runDate) {
		String SQL = "select distinct * from (select c.contract_code,(case when upper(trim(d.name1))=upper(trim(d.name2)) then d.name1 else d.full_name end) as NAME " +
				",hc.engine_number,substr(hc.model_number||' '||hc.serial_number,1,22) as MODEL_SERIAL_NUMBER " +
				",ct.btpn_code_commodity_category as BTPN_CODE_COMMODITY_CATEGORY,ct.btpn_code_commodity_type as BTPN_CODE_COMMODITY_TYPE " +
				",to_char(sysdate,'YYYY') as SISDATE,case when ct.btpn_code_commodity_type in ('CT_CT_PDA', 'CT_CT_MTAB') then 'DGN2' " +
				"else cg.btpn_commodity_group end as COMMODITY,hc.producer,jc.id_agreement as AGREEMENT_ID,ja.partner_id from jfs_contract jc " +
				"left join (select distinct contract_code, deal_code, id from GTT_JSF_002) c on jc.text_contract_number = c.contract_code " +
				"left join (select distinct name1,name2,full_name,code from GTT_JSF_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id where DOCUMENT_TYPE = 'KTP' and ID_CARD_STATUS='a') d on c.deal_code=d.code " +
				"left join GTT_JSF_004 hc on c.id = hc.contract_id " +
				"left join jfs_commodity_type ct on hc.commodity_category_code=ct.hci_code_commodity_category and hc.COMMODITY_TYPE_CODE=ct.hci_code_commodity_type " +
				"left join JFS_COMMODITY_GROUP cg on hc.commodity_category_code=cg.hci_code_commodity_category and jc.id_agreement = cg.id_agreement " +
				"left join (select * from jfs_agreement where REGEXP_LIKE(code, '^[[:digit:]]+$')) ja on to_number(ja.code, '99') = jc.id_agreement " +
				"where trunc(jc.export_date)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and jc.id_agreement = 5 order by jc.id_agreement, c.contract_code)";
		List<JFSExportZBarang> jfsPayment = jdbcTemplateObject.query(SQL, new JFSExportZMapperBarang());
		return jfsPayment;
	}
}
