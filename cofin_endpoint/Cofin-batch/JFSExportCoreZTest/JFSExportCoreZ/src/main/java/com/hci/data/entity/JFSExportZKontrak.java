package com.hci.data.entity;

public class JFSExportZKontrak {
	private String contractCode;
	private String fullName;
	private String code;
	private String contractCode1;
	private String creationDate;
	private String creditAmount;
	private String goodsPriceAmount;
	private String terms;
	private String annuityAmount;
	private String interestRate;
	private String creditAmount1;
	private String sendPrincipal;
	private String terms1;
	private String dueDate;
	private String i1DueDate;
	private String sendRate;
	private String sendInstalment;
	private String creationDateCst;
	private String payment;
	private String biEconomicSectorCode;
	private String agreementId;
	private String partnerId;
	
	public String getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getContractCode() {
		return contractCode;
	}
	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getContractCode1() {
		return contractCode1;
	}
	public void setContractCode1(String contractCode1) {
		this.contractCode1 = contractCode1;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getGoodsPriceAmount() {
		return goodsPriceAmount;
	}
	public void setGoodsPriceAmount(String goodsPriceAmount) {
		this.goodsPriceAmount = goodsPriceAmount;
	}
	public String getTerms() {
		return terms;
	}
	public void setTerms(String terms) {
		this.terms = terms;
	}
	public String getAnnuityAmount() {
		return annuityAmount;
	}
	public void setAnnuityAmount(String annuityAmount) {
		this.annuityAmount = annuityAmount;
	}
	public String getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}
	public String getCreditAmount1() {
		return creditAmount1;
	}
	public void setCreditAmount1(String creditAmount1) {
		this.creditAmount1 = creditAmount1;
	}
	public String getSendPrincipal() {
		return sendPrincipal;
	}
	public void setSendPrincipal(String sendPrincipal) {
		this.sendPrincipal = sendPrincipal;
	}
	public String getTerms1() {
		return terms1;
	}
	public void setTerms1(String terms1) {
		this.terms1 = terms1;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getI1DueDate() {
		return i1DueDate;
	}
	public void setI1DueDate(String i1DueDate) {
		this.i1DueDate = i1DueDate;
	}
	public String getSendRate() {
		return sendRate;
	}
	public void setSendRate(String sendRate) {
		this.sendRate = sendRate;
	}
	public String getSendInstalment() {
		return sendInstalment;
	}
	public void setSendInstalment(String sendInstalment) {
		this.sendInstalment = sendInstalment;
	}
	public String getCreationDateCst() {
		return creationDateCst;
	}
	public void setCreationDateCst(String creationDateCst) {
		this.creationDateCst = creationDateCst;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getBiEconomicSectorCode() {
		return biEconomicSectorCode;
	}
	public void setBiEconomicSectorCode(String biEconomicSectorCode) {
		this.biEconomicSectorCode = biEconomicSectorCode;
	}
}
