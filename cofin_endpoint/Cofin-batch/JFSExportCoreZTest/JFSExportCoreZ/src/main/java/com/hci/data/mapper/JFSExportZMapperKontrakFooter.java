package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JFSExportZKontrakFooter;

public class JFSExportZMapperKontrakFooter implements RowMapper<JFSExportZKontrakFooter> {

	@Override
	public JFSExportZKontrakFooter mapRow(ResultSet rs, int rowNum) throws SQLException {
		JFSExportZKontrakFooter jfsPayment = new JFSExportZKontrakFooter();
		
		jfsPayment.setNumOfRows(rs.getString("NUM_OF_ROWS") != null ? rs.getString("NUM_OF_ROWS") : "");
		jfsPayment.setCreditAmount(rs.getString("CREDIT_AMOUNT") != null ? rs.getString("CREDIT_AMOUNT") : "");
		
		return jfsPayment;
	}
	
}
