package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JFSExportZPengurus;

public class JFSExportZMapperPengurus implements RowMapper<JFSExportZPengurus> {

	@Override
	public JFSExportZPengurus mapRow(ResultSet rs, int rowNum) throws SQLException {
		JFSExportZPengurus jfsPayment = new JFSExportZPengurus();
		
		jfsPayment.setContractCode(rs.getString("CONTRACT_CODE") != null ? rs.getString("CONTRACT_CODE") : "");
		
		return jfsPayment;
	}
	
}
