package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JFSExportZSlik;

public class JFSExportZMapperSlik implements RowMapper<JFSExportZSlik> {

	@Override
	public JFSExportZSlik mapRow(ResultSet rs, int rowNum) throws SQLException {
		JFSExportZSlik jfsSlik = new JFSExportZSlik();
		
		jfsSlik.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER") != null ? rs.getString("TEXT_CONTRACT_NUMBER") : "");
		jfsSlik.setKodeSifatKreditPemiayaan(rs.getString("KODE_SIFAT_KREDIT_PEMBIAYAAN") != null ? rs.getString("KODE_SIFAT_KREDIT_PEMBIAYAAN") : "");
		jfsSlik.setKodeJenisKredit(rs.getString("KODE_JENIS_KREDIT") != null ? rs.getString("KODE_JENIS_KREDIT") : "");
		jfsSlik.setKodeSkim(rs.getString("KODE_SKIM") != null ? rs.getString("KODE_SKIM") : "");
		jfsSlik.setBaruPerpanjangan(rs.getString("BARU_PERPANJANGAN") != null ? rs.getString("BARU_PERPANJANGAN") : "");
		jfsSlik.setKodeJenisPenggunaan(rs.getString("KODE_JENIS_PENGGUNAAN") != null ? rs.getString("KODE_JENIS_PENGGUNAAN") : "");
		jfsSlik.setKodeOrientasiPenggunaan(rs.getString("KODE_ORIENTASI_PENGGUNAAN") != null ? rs.getString("KODE_ORIENTASI_PENGGUNAAN") : "");
		jfsSlik.setTakeoverDari(rs.getString("TAKEOVER_DARI") != null ? rs.getString("TAKEOVER_DARI") : "");
		jfsSlik.setAlamatEmail(rs.getString("ALAMAT_EMAIL") != null ? rs.getString("ALAMAT_EMAIL") : "");
		jfsSlik.setNoTeleponSelular(rs.getString("NO_TELEPON_SELULAR") != null ? rs.getString("NO_TELEPON_SELULAR") : "");
		jfsSlik.setAlamatTempatBekerja(rs.getString("ALAMAT_TEMPAT_BEKERJA") != null ? rs.getString("ALAMAT_TEMPAT_BEKERJA") : "");
		jfsSlik.setPenghasilanKotorPerTahun(rs.getString("PENGHASILAN_KOTOR_PER_TAHUN") != null ? rs.getString("PENGHASILAN_KOTOR_PER_TAHUN") : "");
		jfsSlik.setKodeSumberPenghasilan(rs.getString("KODE_SUMBER_PENGHASILAN") != null ? rs.getString("KODE_SUMBER_PENGHASILAN") : "");
		jfsSlik.setJumlahTanggungan(rs.getString("JUMLAH_TANGGUNGAN") != null ? rs.getString("JUMLAH_TANGGUNGAN") : "");
		jfsSlik.setNamaPasangan(rs.getString("NAMA_PASANGAN") != null ? rs.getString("NAMA_PASANGAN") : "");
		jfsSlik.setNoIdentitasPasangan(rs.getString("NO_IDENTITAS_PASANGAN") != null ? rs.getString("NO_IDENTITAS_PASANGAN") : "");
		jfsSlik.setTanggalIdentitasPasangan(rs.getString("TANGGAL_IDENTITAS_PASANGAN") != null ? rs.getString("TANGGAL_IDENTITAS_PASANGAN") : "");
		jfsSlik.setPerjanjianPisahHarta(rs.getString("PERJANJIAN_PISAH_HARTA") != null ? rs.getString("PERJANJIAN_PISAH_HARTA") : "");
		jfsSlik.setKodeBidangUsaha(rs.getString("KODE_BIDANG_USAHA") != null ? rs.getString("KODE_BIDANG_USAHA") : "");
		jfsSlik.setKodeGolonganDebitur(rs.getString("KODE_GOLONGAN_DEBITUR") != null ? rs.getString("KODE_GOLONGAN_DEBITUR") : "");
		jfsSlik.setKodeBentukBadanUsaha(rs.getString("KODE_BENTUK_BADAN_USAHA") != null ? rs.getString("KODE_BENTUK_BADAN_USAHA") : "");
		jfsSlik.setGoPublic(rs.getString("GO_PUBLIC") != null ? rs.getString("GO_PUBLIC") : "");
		jfsSlik.setPeringkatRatingDebitur(rs.getString("PERINGKAT_RATING_DEBITUR") != null ? rs.getString("PERINGKAT_RATING_DEBITUR") : "");
		jfsSlik.setLembagaPemeringkatRating(rs.getString("LEMBAGA_PEMERINGKAT_RATING") != null ? rs.getString("LEMBAGA_PEMERINGKAT_RATING") : "");
		jfsSlik.setTanggalPemeringkat(rs.getString("TANGGAL_PEMERINGKAT") != null ? rs.getString("TANGGAL_PEMERINGKAT") : "");
		jfsSlik.setNamaGroupDebitur(rs.getString("NAMA_GRUP_DEBITUR") != null ? rs.getString("NAMA_GRUP_DEBITUR") : "");
		jfsSlik.setKodeJenisAgunan(rs.getString("KODE_JENIS_AGUNAN") != null ? rs.getString("KODE_JENIS_AGUNAN") : "");
		jfsSlik.setKodeJenisPengikatan(rs.getString("KODE_JENIS_PENGIKATAN") != null ? rs.getString("KODE_JENIS_PENGIKATAN") : "");
		jfsSlik.setTanggalPengikatan(rs.getString("TANGGAL_PENGIKATAN") != null ? rs.getString("TANGGAL_PENGIKATAN") : "");
		jfsSlik.setNamaPemilikAgunan(rs.getString("NAMA_PEMILIK_AGUNAN") != null ? rs.getString("NAMA_PEMILIK_AGUNAN") : "");
		jfsSlik.setAlamatAgunan(rs.getString("ALAMAT_AGUNAN") != null ? rs.getString("ALAMAT_AGUNAN") : "");
		jfsSlik.setStatusKreditJoin(rs.getString("STATUS_KREDIT_JOIN") != null ? rs.getString("STATUS_KREDIT_JOIN") : "");
		jfsSlik.setDati2(rs.getString("DATI2") != null ? rs.getString("DATI2") : "");
		jfsSlik.setDiasuransikan(rs.getString("DIASURANSIKAN") != null ? rs.getString("DIASURANSIKAN") : "");
		
		return jfsSlik;
	}
	
}
