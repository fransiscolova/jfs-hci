package com.hci.job.interfaces;

import java.util.Date;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.hci.data.dao.ParentJobDao;
import com.hci.job.sub.JFSExportZBarangSub;
import com.hci.job.sub.JFSExportZEnduserSub;
import com.hci.job.sub.JFSExportZKontrakSub;
import com.hci.job.sub.JFSExportZPengurusSub;
import com.hci.job.sub.JFSExportZSlikSub;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZEnduser implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private ParentJobDao parentDao;
	private String targetFol;
	private String jobName;
	private String processName;
	
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		if(checkHoliday()) {
			log.info("JFS2ExportEnduser", "Job skipped, holiday");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(!checkParentJob(processName)) {
			log.info("JFS2ExportEnduser", "Job skipped, parent job not run");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(checkTodayRunJob(processName)) {
			log.info("JFS2ExportEnduser", "Job skipped, already runned");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		log.info("JFS2ExportEnduser", "Job not skipped");
		
		setForceRunToN(processName);
		
		paymentFileStatus = new JFSExportZEnduserSub().createFile(targetFol, uuid, "", 0, "", processName);
		paymentFileStatus = new JFSExportZKontrakSub().createFile(targetFol, uuid, "", 0, "", processName);
		paymentFileStatus = new JFSExportZBarangSub().createFile(targetFol, uuid, "", 0, "", processName);
		paymentFileStatus = new JFSExportZPengurusSub().createFile(targetFol, uuid, "", 0, "", processName);
		paymentFileStatus = new JFSExportZSlikSub().createFile(targetFol, uuid, "", 0, "", processName);
		
		log.info("JFS2ExportEnduser", "Job done");
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}
	
	public boolean checkHoliday() {
		return parentDao.checkHoliday();
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void setForceRunToN(String processName) {
		parentDao.setForceRunNChildJob(processName);
	}
	
	public boolean checkTodayRunJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
}