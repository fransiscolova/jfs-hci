package com.hci.job.interfaces;

import java.util.Date;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.hci.data.dao.ParentJobDao;
import com.hci.job.sub.JFSExportZBarangSub;
import com.hci.job.sub.JFSExportZEnduserSub;
import com.hci.job.sub.JFSExportZKontrakSub;
import com.hci.job.sub.JFSExportZPengurusSub;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZEnduserFail implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	private String processName;
	
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}
}