package com.hci.job.sub;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hci.data.dao.JfsExportZBarangDao;
import com.hci.data.entity.JFSExportZBarang;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZBarangSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JFSExportZBarang> getAllData() {
		JfsExportZBarangDao jfsPaymentDao = (JfsExportZBarangDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZBarangDao");
		List<JFSExportZBarang> jfsPayments = jfsPaymentDao.getJfsExporZBarangData();
		return jfsPayments;
	}
	
	public List<JFSExportZBarang> getAllData(String runDate) {
		JfsExportZBarangDao jfsPaymentDao = (JfsExportZBarangDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZBarangDao");
		List<JFSExportZBarang> jfsPayments = jfsPaymentDao.getJfsExporZBarangData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		int numOfData = 0;
		
		List<JFSExportZBarang> lists;
		if (runDate.equals("")) {
			lists = getAllData();
		} else {
			lists = getAllData(runDate);
		}
		
		log.info("JFS2ExportBarangSub", "Num of file lines : " + lists.size());
		
		for(JFSExportZBarang jp : lists) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			String engineNumber = jp.getEngineNumber() == null? "": jp.getEngineNumber();
			paymentDetailDataString.add(jp.getContractCode() + "|||" + jp.getName() + "||" + engineNumber + "|" + 
					jp.getModelSerialNumber() + "||" + jp.getBtpnCodeCommodityCategory() + "|" + jp.getBtpnCodeCommodityType() + 
					"|N|" + jp.getSisdate() + "|" + jp.getCommodity() + "|" + jp.getProducer());
			numOfData++;
		}
		paymentDetailDataString.add("HCI|" + numOfData + "|0");
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportBarangSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "BARANG_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "BARANG_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|3|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportBarangSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportBarangSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}