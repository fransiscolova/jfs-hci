package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsExportZEnduserDao;
import com.hci.data.entity.JFSExportZEnduser;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZEnduserSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JFSExportZEnduser> getAllData() {
		JfsExportZEnduserDao jfsPaymentDao = (JfsExportZEnduserDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZEnduserDao");
		List<JFSExportZEnduser> jfsPayments = jfsPaymentDao.getJfsExporZEndusertData();
		return jfsPayments;
	}
	
	public List<JFSExportZEnduser> getAllData(String runDate) {
		JfsExportZEnduserDao jfsPaymentDao = (JfsExportZEnduserDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZEnduserDao");
		List<JFSExportZEnduser> jfsPayments = jfsPaymentDao.getJfsExporZEndusertData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		int numOfRow = 0;
		
		List<JFSExportZEnduser> lists;
		if (runDate.equals("")) {
			lists = getAllData();
		} else {
			lists = getAllData(runDate);
		}
		
		log.info("JFS2ExportEnduserSub", "Num of file lines : " + lists.size());
		
		for(JFSExportZEnduser jp : lists) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getContractCode() + "|" + jp.getFullName() + "|" + jp.getNickname() + "|" + jp.getDstCode() + "|" + jp.getDescription() + "|" + jp.getCode8139() + "|" + jp.getGenderType() + "|" + jp.getKtpNum() + "|" +
					jp.getNum() + "||" + jp.getBirthPlace() + "|" + jp.getBirth() + "|||" + jp.getStreetName() + jp.getFullAddress() + "|" + jp.getVillage() + "|" + jp.getSubDistrict() + "|" + jp.getDestCodeXD() + "|" +
					jp.getZipCode() + "||" + jp.getValue1() + "|" + jp.getId() + "|" + jp.getName4() + "|" + jp.getEmployerIndustryCode() + "|" + jp.getNameEmployer() + "|" + jp.getDstCodeXI() + "|" + jp.getOne() + "|" + jp.getDstCodeXM() + "|" + jp.getAid() + "|" + jp.getDstCodeXR() + "|" + jp.getIncome() + "|" + jp.getValidTo());
			numOfRow++;
		}
		paymentDetailDataString.add("HCI|" + numOfRow + "|0");
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportEnduserSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "ENDUSER_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "ENDUSER_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|1|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportEnduserSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportEnduserSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		} 
	}
}
