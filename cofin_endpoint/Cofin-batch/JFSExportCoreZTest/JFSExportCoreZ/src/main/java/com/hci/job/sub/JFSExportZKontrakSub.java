package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsExportZKontrakDao;
import com.hci.data.entity.JFSExportZKontrak;
import com.hci.data.entity.JFSExportZKontrakFooter;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZKontrakSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JFSExportZKontrak> getAllData() {
		JfsExportZKontrakDao jfsPaymentDao = (JfsExportZKontrakDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZKontrakDao");
		List<JFSExportZKontrak> jfsPayments = jfsPaymentDao.getJfsExporZKontrakData();
		return jfsPayments;
	}
	
	public List<JFSExportZKontrakFooter> getFooterData() {
		JfsExportZKontrakDao jfsPaymentDao = (JfsExportZKontrakDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZKontrakDao");
		List<JFSExportZKontrakFooter> jfsPayments = jfsPaymentDao.getJfsExportZKontrakFooterData();
		return jfsPayments;
	}
	
	public List<JFSExportZKontrak> getAllData(String runDate) {
		JfsExportZKontrakDao jfsPaymentDao = (JfsExportZKontrakDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZKontrakDao");
		List<JFSExportZKontrak> jfsPayments = jfsPaymentDao.getJfsExporZKontrakData();
		return jfsPayments;
	}
	
	public List<JFSExportZKontrakFooter> getFooterData(String runDate) {
		JfsExportZKontrakDao jfsPaymentDao = (JfsExportZKontrakDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZKontrakDao");
		List<JFSExportZKontrakFooter> jfsPayments = jfsPaymentDao.getJfsExportZKontrakFooterData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		int numOfRow = 0;
		
		List<JFSExportZKontrak> lists;
		List<JFSExportZKontrakFooter> footers;
		if (runDate.equals("")) {
			lists = getAllData();
			footers = getFooterData();
		} else {
			lists = getAllData(runDate);
			footers = getFooterData(runDate);
		}
		
		log.info("JFS2ExportKontrakSub", "Num of file lines : " + lists.size());
		log.info("JFS2ExportKontrakSub", "Num of footer file lines : " + footers.size());
		
		for(JFSExportZKontrak jp : lists) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getContractCode() + "|" + jp.getFullName() + "|" + jp.getCode() + "|" + jp.getContractCode1() + "|" + jp.getCreationDate() + "|" + jp.getCreditAmount() + 
					"|" + jp.getGoodsPriceAmount() + "|" + jp.getTerms() + "|" + jp.getAnnuityAmount() + "|" + jp.getInterestRate() + "|" + jp.getCreditAmount() + "|" + jp.getSendPrincipal() + "|" + jp.getTerms() + "|" + jp.getDueDate() + "|" + jp.getI1DueDate() + "|" + jp.getSendRate() + "|" + jp.getSendInstalment() + 
					"|1|N|T|0|E|DG|0||" + jp.getCreationDate() + "|0|" + jp.getPayment() + "||" + jp.getBiEconomicSectorCode() + "|K|||||||||||||");
		}
		
		for(JFSExportZKontrakFooter jp : footers) {
			paymentDetailDataString.add("HCI|" + jp.getNumOfRows() + "|" + jp.getCreditAmount());
		}
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportKontrakSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "KONTRAK_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "KONTRAK_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|2|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportKontrakSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportKontrakSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		} 
	}
}
