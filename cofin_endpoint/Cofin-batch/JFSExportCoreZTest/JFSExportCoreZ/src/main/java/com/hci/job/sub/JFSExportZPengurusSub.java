package com.hci.job.sub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hci.data.dao.JfsExportZPengurusDao;
import com.hci.data.entity.JFSExportZPengurus;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZPengurusSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JFSExportZPengurus> getAllData() {
		JfsExportZPengurusDao jfsPaymentDao = (JfsExportZPengurusDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZPengurusDao");
		List<JFSExportZPengurus> jfsPayments = jfsPaymentDao.getJfsExporZPengurusData();
		return jfsPayments;
	}
	
	public List<JFSExportZPengurus> getAllData(String runDate) {
		JfsExportZPengurusDao jfsPaymentDao = (JfsExportZPengurusDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZPengurusDao");
		List<JFSExportZPengurus> jfsPayments = jfsPaymentDao.getJfsExporZPengurusData(runDate);
		return jfsPayments;
	}
	
	public List<String> createPaymentFileData(String header, String runDate) {
		List<String> paymentDataString = new ArrayList<String>();
		paymentDataString.add(header);
		int numOfRow = 0;
		
		List<JFSExportZPengurus> lists;
		if (runDate.equals("")) {
			lists = getAllData();
		} else {
			lists = getAllData(runDate);
		}
		
		log.info("JFS2ExportPengurusSub", "Num of file lines : " + lists.size());
		
		for(JFSExportZPengurus jp : lists) {
			paymentDataString.add(jp.getContractCode() + "||||||||||||||");
			numOfRow++;
		}
		
		paymentDataString.add("HCI|" + numOfRow + "|0");
		
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportPengurusSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "PENGURUS_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "PENGURUS_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		List<String> dataToWrite = createPaymentFileData("HCI|4|"+ dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if(dataToWrite.size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus,  aesFileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportPengurusSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportPengurusSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
