package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsExportOptOutFF;

public interface JfsExportOptOutFFDao {
	public List<JfsExportOptOutFF> getJfsExportFFData();
	public List<JfsExportOptOutFF> getJfsExportFFData(String runDate);
}
