package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportOptOutFFDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsExportOptOutFF;
import com.hci.data.mapper.JfsExportFFMapper;

public class JfsExportOptOutFFImpl implements JfsExportOptOutFFDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportOptOutFFImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	@Override
	public List<JfsExportOptOutFF> getJfsExportFFData() {
		String SQL = "select eligibility_type, to_char(date_signature_contract,'DD-MON-YYYY') as date_signature_contract, " +
				"bank_financing_portion, hcid_financing_portion, cnt_contract " +
				"from jfs_contract_eligibility where trunc(dtime_inserted)=trunc(sysdate) and id_agreement = 4"; 
		List<JfsExportOptOutFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportFFMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsExportOptOutFF> getJfsExportFFData(String runDate) {
		String SQL = "select eligibility_type, to_char(date_signature_contract,'DD-MON-YYYY') as date_signature_contract, " +
				"bank_financing_portion, hcid_financing_portion, cnt_contract " +
				"from jfs_contract_eligibility where trunc(dtime_inserted)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and id_agreement = 4"; 
		List<JfsExportOptOutFF> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportFFMapper());
		return jfsPayment;
	}
}
