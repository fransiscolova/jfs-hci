package com.hci.job.sub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hci.data.dao.JfsExportOptOutFFDao;
import com.hci.data.entity.JfsExportOptOutFF;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportOptOutFFSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsExportOptOutFF> getAllData() {
		JfsExportOptOutFFDao jfsPaymentDao = (JfsExportOptOutFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportOptOutFFDao");
		List<JfsExportOptOutFF> jfsPayments = jfsPaymentDao.getJfsExportFFData();
		return jfsPayments;
	}
	
	public List<JfsExportOptOutFF> getAllData(String runDate) {
		JfsExportOptOutFFDao jfsPaymentDao = (JfsExportOptOutFFDao) SpringContextHolder.getApplicationContext().getBean("jfsExportOptOutFFDao");
		List<JfsExportOptOutFF> jfsPayments = jfsPaymentDao.getJfsExportFFData(runDate);
		return jfsPayments;
	}
	
	public List<String> createPaymentFileData(String header, String runDate) {
		List<String> paymentDataString = new ArrayList<String>();
		paymentDataString.add(header);
		
		List<JfsExportOptOutFF> exportList;
		if (runDate.equals("")) {
			exportList = getAllData();
		} else {
			exportList = getAllData(runDate);
		}
		
		log.info("JFSExportOptOutFFSub", "Num of file lines : " + exportList.size());
		
		for(JfsExportOptOutFF jp : exportList) {
			paymentDataString.add(jp.getType() + ";" + jp.getDateSignature() + ";" + jp.getBtpnPortion() + ";" + jp.getHcidPortion() + ";" + jp.getCntContract());
		}
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSExportOptOutFFSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_EXPORT_FF_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		
		List<String> dataToWrite = createPaymentFileData("TYPE;DATE_SIGNATURE;BTPN_PORTION;HCID_PORTION;CNT_CONTRACT", runDate);
		
		if(dataToWrite.size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
			
		log.info("JFSExportOptOutFFSub", "JobStatus : " + jobStatus);
		log.info("JFSExportOptOutFFSub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
