package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsAllExportZ;

public interface JfsAllExportZDao {
	public List<JfsAllExportZ> getJfsAllExportZData();
	public List<JfsAllExportZ> getJfsAllExportZData(String runDate);
}
