package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsExportZOptOut;

public interface JfsExportZOptOutDao {
	public List<JfsExportZOptOut> getJfsExportZOptOutData();
	public List<JfsExportZOptOut> getJfsExportZOptOutData(String runDate);
}
