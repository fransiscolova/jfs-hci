package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsAllExportZDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsAllExportZ;
import com.hci.data.mapper.JfsAllExportZMapper;

public class JfsAllExportZImpl implements JfsAllExportZDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsAllExportZImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	@Override
	public List<JfsAllExportZ> getJfsAllExportZData() {
		String SQL = "select eligibility_type, to_char(date_signature_contract,'DD-MON-YYYY') as date_signature_contract, " +
				"bank_financing_portion, hcid_financing_portion, cnt_contract " +
				"from jfs_contract_eligibility where trunc(dtime_inserted)=trunc(sysdate) and nvl(id_agreement,0) ='5'"; 
		List<JfsAllExportZ> jfsPayment = jdbcTemplateObject.query(SQL, new JfsAllExportZMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsAllExportZ> getJfsAllExportZData(String runDate) {
		String SQL = "select eligibility_type, to_char(date_signature_contract,'DD-MON-YYYY') as date_signature_contract, " +
				"bank_financing_portion, hcid_financing_portion, cnt_contract " +
				"from jfs_contract_eligibility where trunc(dtime_inserted)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and nvl(id_agreement,0) ='5'"; 
		List<JfsAllExportZ> jfsPayment = jdbcTemplateObject.query(SQL, new JfsAllExportZMapper());
		return jfsPayment;
	}
}
