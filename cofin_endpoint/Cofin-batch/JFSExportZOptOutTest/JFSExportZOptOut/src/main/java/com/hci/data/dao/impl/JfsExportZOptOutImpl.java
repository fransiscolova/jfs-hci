package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsExportZOptOutDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsExportZOptOut;
import com.hci.data.mapper.JfsExportZOptOutMapper;

public class JfsExportZOptOutImpl implements JfsExportZOptOutDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsExportZOptOutImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsExportZOptOut> getJfsExportZOptOutData() {
		String SQL = "select distinct c.contract_code, trim(d.full_name) as CUSTOMER_NAME,cst.creation_date as DATE_SIGNATURE_CONTRACT,round(a.principal_split_rate*fp.credit_amount,0) as BANK_CREDIT_AMOUNT_PORTION " +
				"from jfs_contract_eligibility e " +
				"left join (select distinct creation_date, contract_id from GTT_JSF_001 where CONTRACT_STATUS_TRANS = 'N') cst on trunc(cst.creation_date)=e.date_signature_contract " +
				"left join (select distinct id, contract_code, PRODUCT_CODE, deal_code, CONTRACT_STATUS from GTT_JSF_002) c on c.id = cst.contract_id " +
				"left join (select distinct credit_amount, contract_id from GTT_JSF_003) fp on c.id = fp.contract_id " +
				"left join (select * from jfs_agreement ja inner join jfs_product_mapping pm on  to_number(ja.code, '99') = pm.id_agreement " +
				"inner join jfs_product jp on to_number(ja.code, '99')=jp.id_agreement and pm.bank_product_code=jp.bank_product_code and trunc(sysdate)<=trunc(jp.valid_to) and trunc(sysdate)>=trunc(jp.valid_from) " +
				"where ja.partner_id='001' " +
				") a on c.PRODUCT_CODE = a.code_product and  trunc(cst.creation_date) between trunc(a.valid_from) and trunc(a.valid_to) " + 
				"left join (select distinct code, dbl.full_name from gtt_jsf_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id) d on c.deal_code = d.code " +
				"left join jfs_contract jc on jc.text_contract_number = c.contract_code and jc.id_agreement = 4 " +
				"where trunc(e.dtime_inserted)=trunc(sysdate) and e.eligibility_type='OPT-OUT1' and c.CONTRACT_STATUS in ('A','N','Z') " +
				"and jc.text_contract_number is null order by cst.creation_date";
		
//		String SQL = "select distinct c.contract_code, trim(d.full_name) as CUSTOMER_NAME,cst.creation_date as DATE_SIGNATURE_CONTRACT,round(a.principal_split_rate*fp.credit_amount,0) as BANK_CREDIT_AMOUNT_PORTION " +
//				"from jfs_contract_eligibility e left join GTT_JSF_001 cst on cst.CONTRACT_STATUS_TRANS = 'N' and trunc(cst.creation_date)=e.date_signature_contract " +
//				"left join (select distinct id, contract_code, PRODUCT_CODE, deal_code, CONTRACT_STATUS from GTT_JSF_002) c on c.id = cst.contract_id " +
//				"left join GTT_JSF_003 fp on c.id = fp.contract_id left join (select * from jfs_agreement ja " +
//				"inner join jfs_product_mapping pm on  to_number(ja.code, '99') = pm.id_agreement " +
//				"inner join jfs_product jp on to_number(ja.code, '99')=jp.id_agreement and pm.bank_product_code=jp.bank_product_code and trunc(sysdate)<=trunc(jp.valid_to) and trunc(sysdate)>=trunc(jp.valid_from) " +
//				"where ja.partner_id='001') a on c.PRODUCT_CODE = a.code_product and  trunc(cst.creation_date) between trunc(a.valid_from) and trunc(a.valid_to) " + 
//				"left join GTT_JSF_005 d on c.deal_code = d.code left join jfs_contract jc on jc.text_contract_number = c.contract_code and jc.id_agreement = 4 " +
//				"where trunc(e.dtime_inserted)=trunc(sysdate) and e.eligibility_type='OPT-OUT1' and c.CONTRACT_STATUS in ('A','N','Z') " +
//				"and jc.text_contract_number is null order by cst.creation_date";
		List<JfsExportZOptOut> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportZOptOutMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsExportZOptOut> getJfsExportZOptOutData(String runDate) {
		String SQL = "select distinct c.contract_code, trim(d.full_name) as CUSTOMER_NAME,cst.creation_date as DATE_SIGNATURE_CONTRACT,round(a.principal_split_rate*fp.credit_amount,0) as BANK_CREDIT_AMOUNT_PORTION " +
				"from jfs_contract_eligibility e " +
				"left join (select distinct creation_date, contract_id from GTT_JSF_001 where CONTRACT_STATUS_TRANS = 'N') cst on trunc(cst.creation_date)=e.date_signature_contract " +
				"left join (select distinct id, contract_code, PRODUCT_CODE, deal_code, CONTRACT_STATUS from GTT_JSF_002) c on c.id = cst.contract_id " +
				"left join (select distinct credit_amount, contract_id from GTT_JSF_003) fp on c.id = fp.contract_id " +
				"left join (select * from jfs_agreement ja inner join jfs_product_mapping pm on  to_number(ja.code, '99') = pm.id_agreement " +
				"inner join jfs_product jp on to_number(ja.code, '99')=jp.id_agreement and pm.bank_product_code=jp.bank_product_code and trunc(sysdate)<=trunc(jp.valid_to) and trunc(sysdate)>=trunc(jp.valid_from) " +
				"where ja.partner_id='001' " +
				") a on c.PRODUCT_CODE = a.code_product and  trunc(cst.creation_date) between trunc(a.valid_from) and trunc(a.valid_to) " + 
				"left join (select distinct code, dbl.full_name from gtt_jsf_005 lm left join gtt_jsf_012 dbl on lm.cuid = dbl.cuid left join gtt_jsf_011 sbs on dbl.id_document = sbs.document_id) d on c.deal_code = d.code " +
				"left join jfs_contract jc on jc.text_contract_number = c.contract_code and jc.id_agreement = 4 " +
				"where trunc(e.dtime_inserted)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and e.eligibility_type='OPT-OUT1' and c.CONTRACT_STATUS in ('A','N','Z') " +
				"and jc.text_contract_number is null order by cst.creation_date";
		
//		String SQL = "select distinct c.contract_code, trim(d.full_name) as CUSTOMER_NAME,cst.creation_date as DATE_SIGNATURE_CONTRACT,round(a.principal_split_rate*fp.credit_amount,0) as BANK_CREDIT_AMOUNT_PORTION " +
//				"from jfs_contract_eligibility e left join GTT_JSF_001 cst on cst.CONTRACT_STATUS_TRANS = 'N' and trunc(cst.creation_date)=e.date_signature_contract " +
//				"left join (select distinct id, contract_code, PRODUCT_CODE, deal_code, CONTRACT_STATUS from GTT_JSF_002) c on c.id = cst.contract_id " +
//				"left join GTT_JSF_003 fp on c.id = fp.contract_id left join (select * from jfs_agreement ja " +
//				"inner join jfs_product_mapping pm on  to_number(ja.code, '99') = pm.id_agreement " +
//				"inner join jfs_product jp on to_number(ja.code, '99')=jp.id_agreement and pm.bank_product_code=jp.bank_product_code and trunc(sysdate)<=trunc(jp.valid_to) and trunc(sysdate)>=trunc(jp.valid_from) " +
//				"where ja.partner_id='001') a on c.PRODUCT_CODE = a.code_product and  trunc(cst.creation_date) between trunc(a.valid_from) and trunc(a.valid_to) " + 
//				"left join GTT_JSF_005 d on c.deal_code = d.code left join jfs_contract jc on jc.text_contract_number = c.contract_code and jc.id_agreement = 4 " +
//				"where trunc(e.dtime_inserted)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and e.eligibility_type='OPT-OUT1' and c.CONTRACT_STATUS in ('A','N','Z') " +
//				"and jc.text_contract_number is null order by cst.creation_date";
		List<JfsExportZOptOut> jfsPayment = jdbcTemplateObject.query(SQL, new JfsExportZOptOutMapper());
		return jfsPayment;
	}
}
