package com.hci.data.entity;

public class JfsAllExportZ {
	private String type;
	private String dateSignature;
	private String btpnPortion;
	private String hcidPortion;
	private String cntContract;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDateSignature() {
		return dateSignature;
	}
	public void setDateSignature(String dateSignature) {
		this.dateSignature = dateSignature;
	}
	public String getBtpnPortion() {
		return btpnPortion;
	}
	public void setBtpnPortion(String btpnPortion) {
		this.btpnPortion = btpnPortion;
	}
	public String getHcidPortion() {
		return hcidPortion;
	}
	public void setHcidPortion(String hcidPortion) {
		this.hcidPortion = hcidPortion;
	}
	public String getCntContract() {
		return cntContract;
	}
	public void setCntContract(String cntContract) {
		this.cntContract = cntContract;
	}
	
	
}