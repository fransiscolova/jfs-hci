package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsAllExportZ;

public class JfsAllExportZMapper implements RowMapper<JfsAllExportZ> {

	@Override
	public JfsAllExportZ mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsAllExportZ jfsPayment = new JfsAllExportZ();
		jfsPayment.setType(rs.getString("ELIGIBILITY_TYPE"));
		jfsPayment.setDateSignature(rs.getString("DATE_SIGNATURE_CONTRACT"));
		jfsPayment.setBtpnPortion(rs.getString("BANK_FINANCING_PORTION"));
		jfsPayment.setHcidPortion(rs.getString("HCID_FINANCING_PORTION"));
		jfsPayment.setCntContract(rs.getString("CNT_CONTRACT"));
		return jfsPayment;
	}
	
}
