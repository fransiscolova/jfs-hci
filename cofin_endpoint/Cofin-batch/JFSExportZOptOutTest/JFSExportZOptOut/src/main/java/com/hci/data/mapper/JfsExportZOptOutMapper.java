package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsExportZOptOut;

public class JfsExportZOptOutMapper implements RowMapper<JfsExportZOptOut> {

	@Override
	public JfsExportZOptOut mapRow(ResultSet rs, int rowNum) throws SQLException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		DateFormat dfFinish = new SimpleDateFormat("dd-MMM-yy");
		String datePaymentString = "";
		try {
			datePaymentString = dfFinish.format(df.parse(rs.getString("DATE_SIGNATURE_CONTRACT"))).toString().toUpperCase();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		JfsExportZOptOut jfsPayment = new JfsExportZOptOut();
		jfsPayment.setContractNumber(rs.getString("CONTRACT_CODE"));
		jfsPayment.setCustomerName(rs.getString("CUSTOMER_NAME"));
		//jfsPayment.setDateSignatureContract(rs.getString("DATE_SIGNATURE_CONTRACT"));
		jfsPayment.setDateSignatureContract(datePaymentString);
		jfsPayment.setBankCreditAmountPortion(rs.getString("BANK_CREDIT_AMOUNT_PORTION"));
		return jfsPayment;
	}
	
}
