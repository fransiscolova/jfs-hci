package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.hci.data.dao.ParentJobDao;
import com.hci.job.sub.JFSAllExportZSub;
import com.hci.job.sub.JFSExportZOptOutSub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZOptOut implements Tasklet, InitializingBean {
	
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private ParentJobDao parentDao;
	
	private String targetFol;
	private String jobName;
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		if(checkHoliday()) {
			log.info("JFS2ExportOptOut", "Job skipped, holiday");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(!checkParentJob(processName)) {
			log.info("JFS2ExportOptOut", "Job skipped, parent job not run");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(checkTodayRunJob(processName)) {
			log.info("JFS2ExportOptOut", "Job skipped, already runned");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		log.info("JFS2ExportOptOut", "Job not skipped");
		
		setForceRunToN(processName);
		
		paymentFileStatus = new JFSExportZOptOutSub().createFile(targetFol, uuid, "", 0, "", processName);
		paymentFileStatus = new JFSAllExportZSub().createFile(targetFol, uuid, "", 0, "", processName);
		
		if (paymentFileStatus) {
			sendEmail();
		}
		
		log.info("JFS2ExportOptOut", "Job done");
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}
	
	public boolean checkHoliday() {
		return parentDao.checkHoliday();
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void setForceRunToN(String processName) {
		parentDao.setForceRunNChildJob(processName);
	}
	
	public void sendEmail() throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS Disbursement Registration - "  + sdf.format(new Date());
		StringBuffer content = new StringBuffer();
		content.append("Dear All\n\n\n ");
		content.append("There are new contract to be proceessed as Disbursement Registration. Please use GPG encryption for files : ");
		content.append("- ENDUSER_" + sdf.format(new Date()) + ".txt.aes\n ");
		content.append("- KONTRAK_" + sdf.format(new Date()) + ".txt.aes\n ");
		content.append("- BARANG_" + sdf.format(new Date()) + ".txt.aes\n ");
		content.append("- PENGURUS_" + sdf.format(new Date()) + ".txt.aes\n ");
		content.append("and create additional letter:\n ");
		content.append("- Application letter (Disbursement).docx\n ");
		content.append("- Opt-out 1 disbursement.doc  (with list of contract attached in file JFS_OPT-OUT1_" + sdf.format(new Date()) + ".txt)\n\n\n ");
		content.append("file available in Workarround Server folder /WS/Data/JFS/Export/");
		content.append("Best Regards\nPT Home Credit Indonesia\nGraha Paramita 8/F\nJl.Denpasar Raya Blok D-2\nJakarta 12040");
		emailUtil.sendEmail(subject, content);
	}
	
	public boolean checkTodayRunJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
}