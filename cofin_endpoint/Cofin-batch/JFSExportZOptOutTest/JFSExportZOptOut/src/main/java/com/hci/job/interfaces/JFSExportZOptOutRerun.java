package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.hci.JobLauncherJFSExportZOptOut;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ReExecuteJob;
import com.hci.job.sub.JFSAllExportZSub;
import com.hci.job.sub.JFSExportZOptOutSub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZOptOutRerun implements Tasklet, InitializingBean {

	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private ParentJobDao parentDao;
	private String targetFol;
	private String jobName;
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		List<ReExecuteJob> jobs = getAllReExecuteJob(jobName);
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		if(!checkTodayJob(processName)) {
			log.info("JFS2ExportOptOutRerun", "Today job not yet run");
			if(checkParentJob(processName)) {
				JobDetail jd = new JobDetail();
				try {
					jd = new JobDetail("JFSPaymentRun", Scheduler.DEFAULT_GROUP, JobLauncherJFSExportZOptOut.class);
					jd.getJobDataMap().put("test", "testApp");
					Trigger tgr = TriggerUtils.makeImmediateTrigger(0, 0);
					tgr.setName("FireOnceRun");
					
					Scheduler scheduler = new StdSchedulerFactory().getScheduler();
					scheduler.start();
					scheduler.scheduleJob(jd, tgr);
				} catch (SchedulerException e) {
					e.printStackTrace();
				}
				new PropertyUtil().setProperty("batch.job.run.status", "N");
				return RepeatStatus.FINISHED;
			}
			log.info("JFS2ExportOptOutRerun", "Parent job of today job not yet run");
		}
		
		log.info("JFS2ExportOptOutRerun", "Rerun the job");
		
		if(jobs.size() != 0) {
			for(ReExecuteJob job : jobs) {
				int numOfRerun = job.getNumOfRerun() + 1;

				paymentFileStatus = new JFSExportZOptOutSub().createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
				paymentFileStatus = new JFSAllExportZSub().createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "N", processName);
			}
		}
		
		if (paymentFileStatus) {
			sendEmail();
		}
		
		log.info("JFS2ExportOptOutRerun", "Rerun job finish");
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}
	
	public List<ReExecuteJob> getAllReExecuteJob(String jobName) {
		List<ReExecuteJob> jobs = parentDao.getRerunJob(jobName);
		return jobs;
	}
	
	public boolean checkTodayJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void sendEmail() throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS Disbursement Registration - "  + sdf.format(new Date());
		StringBuffer content = new StringBuffer();
		content.append("Dear All\n\n\n ");
		content.append("There are new contract to be proceessed as Disbursement Registration. Please use GPG encryption for files : ");
		content.append("- ENDUSER_" + sdf.format(new Date()) + ".txt.aes\n ");
		content.append("- KONTRAK_" + sdf.format(new Date()) + ".txt.aes\n ");
		content.append("- BARANG_" + sdf.format(new Date()) + ".txt.aes\n ");
		content.append("- PENGURUS_" + sdf.format(new Date()) + ".txt.aes\n ");
		content.append("and create additional letter:\n ");
		content.append("- Application letter (Disbursement).docx\n ");
		content.append("- Opt-out 1 disbursement.doc  (with list of contract attached in file JFS_OPT-OUT1_" + sdf.format(new Date()) + ".txt)\n\n\n ");
		content.append("file available in Workarround Server folder /WS/Data/JFS/Export/");
		content.append("Best Regards\nPT Home Credit Indonesia\nGraha Paramita 8/F\nJl.Denpasar Raya Blok D-2\nJakarta 12040");
		emailUtil.sendEmail(subject, content);
	}
}