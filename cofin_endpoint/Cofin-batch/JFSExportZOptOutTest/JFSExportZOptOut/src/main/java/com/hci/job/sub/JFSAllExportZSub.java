package com.hci.job.sub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hci.data.dao.JfsAllExportZDao;
import com.hci.data.entity.JfsAllExportZ;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSAllExportZSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsAllExportZ> getAllData() {
		JfsAllExportZDao jfsPaymentDao = (JfsAllExportZDao) SpringContextHolder.getApplicationContext().getBean("jfsAllExportZDao");
		List<JfsAllExportZ> jfsPayments = jfsPaymentDao.getJfsAllExportZData();
		return jfsPayments;
	}
	
	public List<JfsAllExportZ> getAllData(String runDate) {
		JfsAllExportZDao jfsPaymentDao = (JfsAllExportZDao) SpringContextHolder.getApplicationContext().getBean("jfsAllExportZDao");
		List<JfsAllExportZ> jfsPayments = jfsPaymentDao.getJfsAllExportZData(runDate);
		return jfsPayments;
	}
	
	public List<String> createPaymentFileData(String header, String runDate) {
		List<String> paymentDataString = new ArrayList<String>();
		paymentDataString.add(header);
		
		List<JfsAllExportZ> allList;
		if (runDate.equals("")) {
			allList = getAllData();
		} else {
			allList = getAllData(runDate);
		}
		
		log.info("JFS2AllExportSub", "Num of file lines : " + allList.size());
		
		for(JfsAllExportZ jp : allList) {
			paymentDataString.add(jp.getType() + ";" + jp.getDateSignature() + ";" + jp.getBtpnPortion() + ";" + jp.getHcidPortion() + ";" + jp.getCntContract());
		}
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2AllExportSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_EXPORT_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		
		List<String> dataToWrite = createPaymentFileData("TYPE;DATE_SIGNATURE;BTPN_PORTION;HCID_PORTION;CNT_CONTRACT", runDate);
		if(dataToWrite.size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus,  fileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2AllExportSub", "JobStatus : " + jobStatus);
		log.info("JFS2AllExportSub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
