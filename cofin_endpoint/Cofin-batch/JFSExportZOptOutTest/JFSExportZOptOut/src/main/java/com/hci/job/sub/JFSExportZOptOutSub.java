package com.hci.job.sub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hci.data.dao.JfsExportZOptOutDao;
import com.hci.data.entity.JfsExportZOptOut;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSExportZOptOutSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsExportZOptOut> getAllData() {
		JfsExportZOptOutDao jfsPaymentDao = (JfsExportZOptOutDao) SpringContextHolder.getApplicationContext().getBean("jfsExportZOptOutDao");
		List<JfsExportZOptOut> jfsPayments = jfsPaymentDao.getJfsExportZOptOutData();
		return jfsPayments;
	}
	
	public List<String> createPaymentFileData(String header) {
		List<String> paymentDataString = new ArrayList<String>();
		paymentDataString.add(header);
		
		List<JfsExportZOptOut> exportLists = getAllData();
		log.info("JFS2ExportOptOutSub", "Num of file lines : " + exportLists.size());
		
		for(JfsExportZOptOut jp : exportLists) {
			if(jp.getContractNumber() == null) {
				jp.setContractNumber("");
			}
			if(jp.getCustomerName() == null) {
				jp.setCustomerName("");
			}
			if(jp.getDateSignatureContract() == null) {
				jp.setDateSignatureContract("");
			}
			if(jp.getBankCreditAmountPortion() == null) {
				jp.setBankCreditAmountPortion("");
			}
			paymentDataString.add(jp.getContractNumber() + ";" + jp.getCustomerName() + ";" + jp.getDateSignatureContract() + ";" + jp.getBankCreditAmountPortion());
		}
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFS2ExportOptOutSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_OPT-OUT1_Z_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		List<String> dataToWrite = createPaymentFileData("CONTRACT_NUMBER;CUSTOMER_NAME;DATE_SIGNATURE_CONTRACT;BANK_CREDIT_AMOUNT_PORTION");
		if(dataToWrite.size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus,  fileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFS2ExportOptOutSub", "JobStatus : " + jobStatus);
		log.info("JFS2ExportOptOutSub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
