package com.hci.data.dao;

import java.sql.Date;
import java.util.List;

import com.hci.data.entity.JfsLastPayment;

public interface JfsLastPaymentDao {
	public List<JfsLastPayment> getMonthlyReconcileContract(String queryDate);
}
