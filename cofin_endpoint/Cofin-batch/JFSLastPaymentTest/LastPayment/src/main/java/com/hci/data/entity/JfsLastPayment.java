package com.hci.data.entity;

import java.sql.Date;

public class JfsLastPayment {
	private String textContractNumber;
	private String outstandingAmount;
	private String paidPrincipal;
	private Long outstandingPrincipal;
	private String bankInstallment;
	private Date dateFirstDue;
	private String contractDpd;
	
	public String getContractDpd() {
		return contractDpd;
	}
	public void setContractDpd(String contractDpd) {
		this.contractDpd = contractDpd;
	}
	public String getBankInstallment() {
		return bankInstallment;
	}
	public void setBankInstallment(String bankInstallment) {
		this.bankInstallment = bankInstallment;
	}
	public Date getDateFirstDue() {
		return dateFirstDue;
	}

	public void setDateFirstDue(Date dateFirstDue) {
		this.dateFirstDue = dateFirstDue;
	}

	public Long getOutstandingPrincipal() {
		return outstandingPrincipal;
	}

	public void setOutstandingPrincipal(Long outstandingPrincipal) {
		this.outstandingPrincipal = outstandingPrincipal;
	}

	public String getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(String outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public String getPaidPrincipal() {
		return paidPrincipal;
	}

	public void setPaidPrincipal(String paidPrincipal) {
		this.paidPrincipal = paidPrincipal;
	}

	public String getTextContractNumber() {
		return textContractNumber;
	}

	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	
}