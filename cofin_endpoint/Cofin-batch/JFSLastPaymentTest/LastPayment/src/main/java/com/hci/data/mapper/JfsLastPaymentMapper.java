package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsLastPayment;

public class JfsLastPaymentMapper implements RowMapper<JfsLastPayment> {

	@Override
	public JfsLastPayment mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsLastPayment jfsContract = new JfsLastPayment();
		jfsContract.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsContract.setOutstandingAmount(rs.getString("OUTSTANDING_PRINCIPAL"));
		jfsContract.setPaidPrincipal(rs.getString("PAID_PRINCIPAL"));
		jfsContract.setBankInstallment(rs.getString("BANK_INSTALLMENT"));
		jfsContract.setDateFirstDue(rs.getDate("DATE_FIRST_DUE"));
		return jfsContract;
	}	
}