package com.hci.job.interfaces;

import java.text.SimpleDateFormat;

import org.apache.commons.mail.EmailException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;

import com.hci.data.dao.JfsLastPaymentDao;
import com.hci.data.dao.ParentJobDao;
import com.hci.job.sub.SubJobParent;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;

public class JFSLastPayment extends SubJobParent implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	private String targetFol;
	private String processName;
	private String jobName;
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
	
	private ParentJobDao parentDao;
	JfsLastPaymentDao jfsLastPaymentDao;

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		/*
		 * 
		 */
		return RepeatStatus.FINISHED;
	}
	
	public void sendEmail(String date) throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS - HCID Reconciliation";
		StringBuffer content = new StringBuffer();
		content.append("There are new HCID Reconciliation that have to be registered to BTPN\n\n");
		content.append("You can find the file:\n");
		content.append("/WS/Data/JFS/Export/JFS_RECONCILE_" + date + ".txt.aes");
		emailUtil.sendEmail(subject, content);
	}
	
	public String getTargetFol() {
		return targetFol;
	}

	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
}