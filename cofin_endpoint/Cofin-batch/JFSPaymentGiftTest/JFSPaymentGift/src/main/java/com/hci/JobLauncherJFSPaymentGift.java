package com.hci;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;

import com.hci.util.LoggerUtil;
import com.hci.util.SpringContextHolder;

public class JobLauncherJFSPaymentGift implements org.quartz.Job {
	LoggerUtil log = new LoggerUtil();
	private Job job;
	private JobLocator jobLocator;
	private JobLauncher jobLauncher;
	
//	@SuppressWarnings("resource")
	public void execute(JobExecutionContext context) {
		//JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		job = (Job) SpringContextHolder.getApplicationContext().getBean("jfsPaymentGiftJob");
		jobLocator = (JobLocator) SpringContextHolder.getApplicationContext().getBean("jfsPaymentGiftJobRegistry");
		jobLauncher = (JobLauncher) SpringContextHolder.getApplicationContext().getBean("jobLauncherJFSPaymentGift");

		try {
			JobParameters jobParameters =
					  new JobParametersBuilder()
					  .addLong("time",System.currentTimeMillis()).toJobParameters();
			jobLauncher.run(job, jobParameters);
		} catch (JobExecutionException e) {
			log.info("JobLauncherJFSPaymentGift", "Job Registration error: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
