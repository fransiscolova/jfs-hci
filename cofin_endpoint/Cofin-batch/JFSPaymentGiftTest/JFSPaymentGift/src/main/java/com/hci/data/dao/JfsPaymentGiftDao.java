package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsPaymentGift;

public interface JfsPaymentGiftDao {
	public List<JfsPaymentGift> getJfsPaymentGiftData();
	public List<JfsPaymentGift> getJfsPaymentGiftData(String runDate);
}
