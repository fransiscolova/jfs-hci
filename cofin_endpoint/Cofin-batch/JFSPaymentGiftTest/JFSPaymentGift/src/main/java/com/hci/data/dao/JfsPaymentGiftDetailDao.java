package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsPaymentGiftDetail;

public interface JfsPaymentGiftDetailDao {
	public List<JfsPaymentGiftDetail> getJfsPaymentGiftDetailData();
	public List<JfsPaymentGiftDetail> getJfsPaymentGiftDetailData(String runDate);
}
