package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsPaymentGiftDetailDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsPaymentGiftDetail;
import com.hci.data.mapper.JfsPaymentGiftDetailMapper;

public class JfsPaymentGiftDetailImpl implements JfsPaymentGiftDetailDao {
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsPaymentGiftDetailImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsPaymentGiftDetail> getJfsPaymentGiftDetailData() {
		String SQL = "select replace(jc.BANK_REFERENCE_NO,chr(13),'') BANK_REFERENCE_NO, " +  
				  "jp.text_contract_number, jp.date_payment, ja.code agreement_id, jpr.id as partner_id " +
				  "from JFS_PAYMENT_INT jp left join JFS_CONTRACT JC on jp.text_contract_number=jc.text_contract_number " +  
				  "left join jfs_agreement ja on jc.id_agreement = ja.code " +
				  "left join jfs_partner jpr on ja.partner_id = jpr.id " +
				  "where trunc(jp.date_export)=trunc(sysdate) and jp.payment_type='G' " +  
				  "order by jp.date_payment"; 
		List<JfsPaymentGiftDetail> jfsPaymentDetails = jdbcTemplateObject.query(SQL,  new JfsPaymentGiftDetailMapper());
		return jfsPaymentDetails;
	}
	
	@Override
	public List<JfsPaymentGiftDetail> getJfsPaymentGiftDetailData(String runDate) {
		String SQL = "select replace(jc.BANK_REFERENCE_NO,chr(13),'') BANK_REFERENCE_NO, " +  
				  "jp.text_contract_number, jp.date_payment, ja.code agreement_id, jpr.id as partner_id " +
				  "from JFS_PAYMENT_INT jp left join JFS_CONTRACT JC on jp.text_contract_number=jc.text_contract_number " +  
				  "left join jfs_agreement ja on jc.id_agreement = ja.code " +
				  "left join jfs_partner jpr on ja.partner_id = jpr.id " +
				  "where trunc(jp.date_export)=trunc(to_date('" + runDate + "', 'dd-mm-yyyy')) and jp.payment_type='G' " +  
				  "order by jp.date_payment";
		List<JfsPaymentGiftDetail> jfsPaymentDetails = jdbcTemplateObject.query(SQL,  new JfsPaymentGiftDetailMapper());
		return jfsPaymentDetails;
	}
}
