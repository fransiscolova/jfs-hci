package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsPaymentGiftDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsPaymentGift;
import com.hci.data.mapper.JfsPaymentGiftMapper;

public class JfsPaymentGiftImpl implements JfsPaymentGiftDao {
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsPaymentGiftImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsPaymentGift> getJfsPaymentGiftData() {
		String SQL = "select p.text_contract_number, c.client_name, " +
				"to_char(p.date_payment,'dd/MM/yyyy') date_payment, " +  
				"p.payment_type, ja.code agreement_id, jp.id as partner_id " +
				"from JFS_PAYMENT_INT p inner join JFS_CONTRACT c on p.text_contract_number=c.text_contract_number " +  
				"left join jfs_agreement ja on c.id_agreement = ja.code " +  
				"left join jfs_partner jp on ja.partner_id = jp.id " +  
				"where trunc(p.date_export)=trunc(sysdate) and p.payment_type='G' order by p.date_payment"; 
		List<JfsPaymentGift> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentGiftMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsPaymentGift> getJfsPaymentGiftData(String runDate) {
		String SQL = "select p.text_contract_number, c.client_name, " +
				"to_char(p.date_payment,'dd/MM/yyyy') date_payment, " +  
				"p.payment_type, ja.code agreement_id, jp.id as partner_id " +
				"from JFS_PAYMENT_INT p inner join JFS_CONTRACT c on p.text_contract_number=c.text_contract_number " +  
				"left join jfs_agreement ja on c.id_agreement = ja.code " +  
				"left join jfs_partner jp on ja.partner_id = jp.id " +  
				"where trunc(p.date_export)=trunc(to_date('" + runDate + "', 'dd-mm-yyyy')) and p.payment_type='G' order by p.date_payment";
		List<JfsPaymentGift> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentGiftMapper());
		return jfsPayment;
	}
}
