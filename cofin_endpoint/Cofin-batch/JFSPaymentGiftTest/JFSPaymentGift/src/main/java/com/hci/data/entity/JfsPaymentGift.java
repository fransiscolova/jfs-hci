package com.hci.data.entity;

import java.sql.Date;

public class JfsPaymentGift {
	private String textContractNumber;
	private String clientName;
	private String datePayment;
	private String paymentType;
	private String idAgreement;
	private String partnerId;
	
	
	public String getIdAgreement() {
		return idAgreement;
	}
	public void setIdAgreement(String idAgreement) {
		this.idAgreement = idAgreement;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(String datePayment) {
		this.datePayment = datePayment;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
}