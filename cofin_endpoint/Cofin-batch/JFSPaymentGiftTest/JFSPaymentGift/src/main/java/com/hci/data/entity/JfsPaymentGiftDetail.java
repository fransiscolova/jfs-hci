package com.hci.data.entity;

public class JfsPaymentGiftDetail {
	private String bankReferenceNo;
	private String textContractNumber;
	private String datePayment;
	private String idAgreement;
	private String partnerid;
	
	public String getIdAgreement() {
		return idAgreement;
	}
	public void setIdAgreement(String idAgreement) {
		this.idAgreement = idAgreement;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	public String getBankReferenceNo() {
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo) {
		this.bankReferenceNo = bankReferenceNo;
	}
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(String datePayment) {
		this.datePayment = datePayment;
	}
}
