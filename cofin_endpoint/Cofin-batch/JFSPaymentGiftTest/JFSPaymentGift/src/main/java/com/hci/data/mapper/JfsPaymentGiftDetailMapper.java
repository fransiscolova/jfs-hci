package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsPaymentGiftDetail;

public class JfsPaymentGiftDetailMapper  implements RowMapper<JfsPaymentGiftDetail> {

	@Override
	public JfsPaymentGiftDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		DateFormat dfFinish = new SimpleDateFormat("dd-MMM-yy");
		String datePaymentString = "";
		try {
			datePaymentString = dfFinish.format(df.parse(rs.getString("DATE_PAYMENT"))).toString().toUpperCase();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		JfsPaymentGiftDetail jfsPaymentDetail = new JfsPaymentGiftDetail();
		jfsPaymentDetail.setBankReferenceNo(rs.getString("BANK_REFERENCE_NO"));
		jfsPaymentDetail.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
//		jfsPaymentDetail.setDatePayment(rs.getString("DATE_PAYMENT"));
		jfsPaymentDetail.setDatePayment(datePaymentString);
		jfsPaymentDetail.setIdAgreement(rs.getString("AGREEMENT_ID"));
		jfsPaymentDetail.setPartnerid(rs.getString("PARTNER_ID"));
		return jfsPaymentDetail;
	}
}
