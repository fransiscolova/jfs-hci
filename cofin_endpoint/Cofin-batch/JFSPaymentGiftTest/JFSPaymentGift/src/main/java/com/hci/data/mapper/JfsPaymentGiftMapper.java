package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsPaymentGift;

public class JfsPaymentGiftMapper implements RowMapper<JfsPaymentGift> {

	@Override
	public JfsPaymentGift mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsPaymentGift jfsPayment = new JfsPaymentGift();
		jfsPayment.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setClientName(rs.getString("CLIENT_NAME"));
		jfsPayment.setDatePayment(rs.getString("DATE_PAYMENT"));
		jfsPayment.setPaymentType(rs.getString("PAYMENT_TYPE"));
		jfsPayment.setIdAgreement(rs.getString("AGREEMENT_ID"));
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPayment;
	}
	
}
