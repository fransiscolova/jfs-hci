package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.JobLauncherJFSPaymentGift;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ReExecuteJob;
import com.hci.job.sub.JFSPaymentGiftDetail1Sub;
import com.hci.job.sub.JFSPaymentGiftDetailSub;
import com.hci.job.sub.JFSPaymentGiftSub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentGiftRerun implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private ParentJobDao parentJob;
	
	private String targetFol;
	private String jobName;
	private String processName;
	
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentJob = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		//parentJob.runProcedure("P_JFS_PAYMENT_GIFT");
		List<ReExecuteJob> jobs = getAllReExecuteJob(jobName);
		boolean paymentFileStatus = false;
		
		if(!checkTodayJob(processName)) {
			// Check whether the parent has been run
			log.info("JFSPaymentGiftRerun", "Today job not yet run");
			if(checkParentJob(processName)) {
				JobDetail jd = new JobDetail();
				try {
					jd = new JobDetail("JFSPaymentGiftRun", Scheduler.DEFAULT_GROUP, JobLauncherJFSPaymentGift.class);
					jd.getJobDataMap().put("test", "testApp");
					Trigger tgr = TriggerUtils.makeImmediateTrigger(0, 0);
					tgr.setName("FireOnceRun");
					
					Scheduler scheduler = new StdSchedulerFactory().getScheduler();
					scheduler.start();
					scheduler.scheduleJob(jd, tgr);
				} catch (SchedulerException e) {
					e.printStackTrace();
				}
				
				return RepeatStatus.FINISHED;
			}
			log.info("JFSPaymentGiftRerun", "Parent job of today job not yet run");
		}
		log.info("JFSPaymentGiftRerun", "Rerun the job");
		
		String fileDate = "";
		
		for (ReExecuteJob job : jobs) {
			int numOfRerun = job.getNumOfRerun() + 1;
		
			JFSPaymentGiftSub jfsPaymentSub = new JFSPaymentGiftSub();
			paymentFileStatus = jfsPaymentSub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
			
			JFSPaymentGiftDetailSub jfsPaymentDetailSub = new JFSPaymentGiftDetailSub();
			jfsPaymentDetailSub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
		
			JFSPaymentGiftDetail1Sub jfsPaymentDetail1Sub = new JFSPaymentGiftDetail1Sub();
			jfsPaymentDetail1Sub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "N", processName);
			
			fileDate = job.getRunDate();
		}
		
		if (paymentFileStatus) {
			sendEmail(fileDate);
		}
		
		log.info("JFSPaymentGiftRerun", "Rerun job finish");
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		return RepeatStatus.FINISHED;
	}
	
	public List<ReExecuteJob> getAllReExecuteJob(String jobName) {
		List<ReExecuteJob> jobs = parentJob.getRerunJob(jobName);
		return jobs;
	}
	
	public boolean checkTodayJob(String processName) {
		return parentJob.checkTodayRunJob(processName, sdf.format(new Date()));
	}
	
	public boolean checkParentJob(String processName) {
		return parentJob.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void sendEmail(String fileDate) throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS - HCID Payment Gift";
		StringBuffer content = new StringBuffer();
		content.append("There are new HCID Payment-Gift that have to be registered to BTPN\n\n");
		content.append("You can find the file:\n");
		content.append(" /WS/Data/JFS/Export/JFS_PAYMENT_GIFT_" + fileDate + ".txt.aes");
		content.append(" /WS/Data/JFS/Export/JFS_PAYMENT_G_DETAIL_" + fileDate + ".txt");
		emailUtil.sendEmail(subject, content);
	}
}