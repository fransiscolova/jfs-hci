package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsPaymentInpayReversalContract;
import com.hci.data.entity.JfsPaymentInt;
import com.hci.data.entity.JfsTmpPayDetail;
import com.hci.data.entity.JfsTmpPayHead;

public interface JfsPaymentInpayReversalDao {
	public List<JfsPaymentInpayReversalContract> getJfsContractInpay();
	public List<JfsTmpPayHead> getJfsTmpJFSPayHead(String contracts);
	public List<JfsTmpPayDetail> getJfsTmpJFSPayDetail(String contracts);
	public void setInpayAllocation(String contractNumber, String dueDate, String partIndex, String amtPrincipal, String amtInterest, String incomingPaymentId, String amtOverpayment);
	public JfsPaymentInt getPaymentInt(String status, String paymentType, String incomingPaymentId);
}
