package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsPaymentInpayReversalDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsPaymentInpayReversalContract;
import com.hci.data.entity.JfsPaymentInt;
import com.hci.data.entity.JfsTmpPayDetail;
import com.hci.data.entity.JfsTmpPayHead;
import com.hci.data.mapper.JfsPaymentInpayReversalContractMapper;
import com.hci.data.mapper.JfsPaymentIntMapper;
import com.hci.data.mapper.JfsTmpPayDetailMapper;
import com.hci.data.mapper.JfsTmpPayHeadMapper;

public class JfsPaymentInpayReversalImpl implements JfsPaymentInpayReversalDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	public JfsPaymentInpayReversalImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsPaymentInpayReversalContract> getJfsContractInpay() {
		String SQL = "select jc.text_contract_number, count(jp.incoming_payment_id) as NUM_OF_REC " +
				"from jfs_payment_int jp,  jfs_contract jc " +
				"where  jc.text_contract_number=jp.text_contract_number " +
				"and jp.status='A' and jp.payment_type in ('R') " +
				"and not jp.text_contract_number is null " +
				"and trunc(jp.dtime_status_updated)=trunc(to_date('01-OCT-15', 'DD-MON-YY')) " +
				"and trunc(jp.date_export)=trunc(to_date('01-OCT-15', 'DD-MON-YY')) " +
				"and jp.pmt_instalment < 0 group by jc.text_contract_number";
		List<JfsPaymentInpayReversalContract> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentInpayReversalContractMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsTmpPayHead> getJfsTmpJFSPayHead(String contracts) {
		String SQL = "select jc.text_contract_number,jp.incoming_payment_id,jp.date_payment " +
				",jp.pmt_instalment,jp.pmt_instalment balance " +
				",row_number() over(partition by jc.text_contract_number order by jc.text_contract_number,jp.incoming_payment_id, jp.date_payment,jp.pmt_instalment) PART_INDEX " +
				",b.cnt_day_past_due from jfs_contract jc " +
				"left join jfs_payment_int jp on jc.text_contract_number=jp.text_contract_number and jp.payment_type in ('R')  and jp.pmt_instalment < 0 " +
				"left join (select jc.text_contract_number " +
				",nvl(case  when min(f.due_date)<=to_date('01-OCT-15', 'DD-MON-YY') " +
				"then (to_date('01-OCT-15', 'DD-MON-YY')-min(f.due_date))+1 else 0 end,0) cnt_day_past_due " +
				"from jfs_contract jc left join ( " +
				"select jc2.text_contract_number,js.due_date,js.principal+js.interest amt_instalment,sum(nvl(pa.amt_principal,0)+nvl(pa.amt_interest,0)) amt_payment " +
				"from jfs_contract jc2 " + 
				"left join jfs_schedule js on jc2.text_contract_number=js.text_contract_number " +
				"left join jfs_payment_allocation pa on js.text_contract_number=pa.text_contract_number and pa.due_date=js.due_date and pa.archived=0 " +
				"where trunc(pa.dtime_created)<trunc(to_date('01-OCT-15', 'DD-MON-YY')) " +
				"group by jc2.text_contract_number,js.due_date,js.principal+js.interest " +
				"having sum(nvl(pa.amt_principal,0)+nvl(pa.amt_interest,0))<js.principal+js.interest " +
				"order by jc2.text_contract_number,js.due_date)f on jc.text_contract_number=f.text_contract_number " +
				"group by jc.text_contract_number)b on jc.text_contract_number=b.text_contract_number " +
				"where trunc(dtime_status_updated)=trunc(to_date('01-OCT-15', 'DD-MON-YY')) " +
				"and trunc(date_export) = trunc(to_date('01-OCT-15', 'DD-MON-YY')) " +
				"and jp.status='A' and jc.text_contract_number in (" + contracts + ") " +
				"order by jp.incoming_payment_id,jp.date_payment,jp.pmt_instalment"; 
		List<JfsTmpPayHead> tmpPayHead = jdbcTemplateObject.query(SQL, new JfsTmpPayHeadMapper());
		return tmpPayHead;
	}
	
	@Override
	public List<JfsTmpPayDetail> getJfsTmpJFSPayDetail(String contracts) {
		String SQL = "select jc.text_contract_number,jp.due_date,jp.amt_principal,jp.amt_interest " +
				",row_number() over(partition by jc.text_contract_number order by jc.text_contract_number,jp.due_date) PART_INDEX " +
				",jp.amt_principal balance_principal,jp.amt_interest balance_interest from jfs_contract jc " +
				"left join jfs_payment_allocation jp on jc.text_contract_number=jp.text_contract_number " +
				"where jp.archived=0 and (jp.amt_principal < 0 or jp.amt_interest < 0) " +
				"and trunc(jp.dtime_created)=trunc(to_date('01-OCT-15', 'DD-MON-YY')) " +
				"and jc.text_contract_number in (" + contracts + ")";
		List<JfsTmpPayDetail> tmpPayDetail = jdbcTemplateObject.query(SQL, new JfsTmpPayDetailMapper());
		return tmpPayDetail;
	}
	
	@Override
	public void setInpayAllocation(String contractNumber, String dueDate, String partIndex, String amtPrincipal, String amtInterest, String incomingPaymentId, String amtOverpayment) {
		String SQL = "insert into jfs_inpay_allocation_fix(TEXT_CONTRACT_NUMBER,due_date,part_index,amt_principal,amt_interest,incoming_payment_id,dtime_created, amt_overpayment) values " +
				"('" + contractNumber + "', to_date('" + dueDate + "', 'DD-MON-YY'), '" + partIndex + "', '" + amtPrincipal + "', '" + amtInterest + "', '" + incomingPaymentId + "', to_date(sysdate, 'DD-MON-YY'),";
		if(amtOverpayment == null) {
			SQL = SQL + amtOverpayment + ")";
		} else {
			SQL = SQL + "'" + amtOverpayment + "')";
		}
		
		jdbcTemplateObject.update(SQL);
	}
	
	@Override
	public JfsPaymentInt getPaymentInt(String status, String paymentType, String incomingPaymentId) {
		String SQL = "select * from jfs_payment_int where status = '" + status + "' and payment_type = '" + paymentType + "' and incoming_payment_id = '" + incomingPaymentId + "'";
		JfsPaymentInt jfsPaymentInts = (JfsPaymentInt) jdbcTemplateObject.query(SQL, new JfsPaymentIntMapper());
		return jfsPaymentInts;
	}
}
