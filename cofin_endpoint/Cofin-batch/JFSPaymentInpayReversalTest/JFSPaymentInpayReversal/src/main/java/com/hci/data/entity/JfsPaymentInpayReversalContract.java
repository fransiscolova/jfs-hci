package com.hci.data.entity;

public class JfsPaymentInpayReversalContract {
	private String contractNumber;
	private int numOfRecords;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public int getNumOfRecords() {
		return numOfRecords;
	}
	public void setNumOfRecords(int numOfRecords) {
		this.numOfRecords = numOfRecords;
	}
}
