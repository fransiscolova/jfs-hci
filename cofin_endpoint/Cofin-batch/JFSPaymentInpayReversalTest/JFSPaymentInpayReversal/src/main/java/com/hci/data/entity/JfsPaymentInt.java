package com.hci.data.entity;

public class JfsPaymentInt {
	private String incomingPaymentId;
	private String textContractNumber;
	private String totalAmtPayment;
	private String amtPayment;
	private String datePayment;
	private String dateExport;
	private String pmtInstalment;
	private String pmtFee;
	private String pmtPenalty;
	private String pmtOverpayment;
	private String pmtOther;
	private String parentId;
	private String status;
	private String reason;
	private String dtimeStatusUpdate;
	private String paymentType;
	private String dateBankProcess;
	
	public String getIncomingPaymentId() {
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(String incomingPaymentId) {
		this.incomingPaymentId = incomingPaymentId;
	}
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getTotalAmtPayment() {
		return totalAmtPayment;
	}
	public void setTotalAmtPayment(String totalAmtPayment) {
		this.totalAmtPayment = totalAmtPayment;
	}
	public String getAmtPayment() {
		return amtPayment;
	}
	public void setAmtPayment(String amtPayment) {
		this.amtPayment = amtPayment;
	}
	public String getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(String datePayment) {
		this.datePayment = datePayment;
	}
	public String getDateExport() {
		return dateExport;
	}
	public void setDateExport(String dateExport) {
		this.dateExport = dateExport;
	}
	public String getPmtInstalment() {
		return pmtInstalment;
	}
	public void setPmtInstalment(String pmtInstalment) {
		this.pmtInstalment = pmtInstalment;
	}
	public String getPmtFee() {
		return pmtFee;
	}
	public void setPmtFee(String pmtFee) {
		this.pmtFee = pmtFee;
	}
	public String getPmtPenalty() {
		return pmtPenalty;
	}
	public void setPmtPenalty(String pmtPenalty) {
		this.pmtPenalty = pmtPenalty;
	}
	public String getPmtOverpayment() {
		return pmtOverpayment;
	}
	public void setPmtOverpayment(String pmtOverpayment) {
		this.pmtOverpayment = pmtOverpayment;
	}
	public String getPmtOther() {
		return pmtOther;
	}
	public void setPmtOther(String pmtOther) {
		this.pmtOther = pmtOther;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getDtimeStatusUpdate() {
		return dtimeStatusUpdate;
	}
	public void setDtimeStatusUpdate(String dtimeStatusUpdate) {
		this.dtimeStatusUpdate = dtimeStatusUpdate;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getDateBankProcess() {
		return dateBankProcess;
	}
	public void setDateBankProcess(String dateBankProcess) {
		this.dateBankProcess = dateBankProcess;
	}
}
