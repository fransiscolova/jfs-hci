package com.hci.data.entity;

public class JfsTmpPayHead {
	private String contractNumber;
	private String incomingPaymentId;
	private String datePayment;
	private String pmtInstalment;
	private String balance;
	private String partIndex;
	private String cntDpd;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getIncomingPaymentId() {
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(String incomingPaymentId) {
		this.incomingPaymentId = incomingPaymentId;
	}
	public String getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(String datePayment) {
		this.datePayment = datePayment;
	}
	public String getPmtInstalment() {
		return pmtInstalment;
	}
	public void setPmtInstalment(String pmtInstalment) {
		this.pmtInstalment = pmtInstalment;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getPartIndex() {
		return partIndex;
	}
	public void setPartIndex(String partIndex) {
		this.partIndex = partIndex;
	}
	public String getCntDpd() {
		return cntDpd;
	}
	public void setCntDpd(String cntDpd) {
		this.cntDpd = cntDpd;
	}
}
