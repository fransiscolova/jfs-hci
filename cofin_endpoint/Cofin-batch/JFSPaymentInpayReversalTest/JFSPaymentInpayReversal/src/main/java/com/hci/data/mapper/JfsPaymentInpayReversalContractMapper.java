package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsPaymentInpayReversal;
import com.hci.data.entity.JfsPaymentInpayReversalContract;

public class JfsPaymentInpayReversalContractMapper implements RowMapper<JfsPaymentInpayReversalContract> {

	@Override
	public JfsPaymentInpayReversalContract mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsPaymentInpayReversalContract jfsPayment = new JfsPaymentInpayReversalContract();
		jfsPayment.setContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setNumOfRecords(rs.getInt("NUM_OF_REC"));
		return jfsPayment;
	}	
}