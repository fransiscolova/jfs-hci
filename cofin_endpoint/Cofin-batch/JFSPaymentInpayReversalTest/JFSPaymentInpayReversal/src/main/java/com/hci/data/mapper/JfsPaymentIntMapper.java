package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsPaymentInt;
import com.hci.data.entity.JfsTmpPayHead;

public class JfsPaymentIntMapper implements RowMapper<JfsPaymentInt> {

	@Override
	public JfsPaymentInt mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsPaymentInt paymentInt = new JfsPaymentInt();
		
		paymentInt.setIncomingPaymentId(rs.getString("INCOMING_PAYMENT_ID"));
		paymentInt.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		paymentInt.setTotalAmtPayment(rs.getString("TOTAL_AMT_PAYMENT"));
		paymentInt.setAmtPayment(rs.getString("AMT_PAYMENT"));
		paymentInt.setDatePayment(rs.getString("DATE_PAYMENT"));
		paymentInt.setDateExport(rs.getString("DATE_EXPORT"));
		paymentInt.setPmtInstalment(rs.getString("PMT_INSTALMENT"));
		paymentInt.setPmtFee(rs.getString("PMT_FEE"));
		paymentInt.setPmtPenalty(rs.getString("PMT_PENALTY"));
		paymentInt.setPmtOverpayment(rs.getString("PMT_OVERPAYMENT"));
		paymentInt.setPmtOther(rs.getString("PMT_OTHER"));
		paymentInt.setParentId(rs.getString("PARENT_ID"));
		paymentInt.setStatus(rs.getString("STATUS"));
		paymentInt.setReason(rs.getString("REASON"));
		paymentInt.setDtimeStatusUpdate(rs.getString("DTIME_STATUS_UPDATED"));
		paymentInt.setPaymentType(rs.getString("PAYMENT_TYPE"));
		paymentInt.setDateBankProcess(rs.getString("DATE_BANK_PROCESS"));
		
		return paymentInt;
	}	
}