package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsTmpPayDetail;

public class JfsTmpPayDetailMapper implements RowMapper<JfsTmpPayDetail> {

	@Override
	public JfsTmpPayDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsTmpPayDetail tmpPayDetail = new JfsTmpPayDetail();
		tmpPayDetail.setContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		tmpPayDetail.setDueDate(rs.getString("DUE_DATE"));
		tmpPayDetail.setPrincipal(rs.getString("AMT_PRINCIPAL"));
		tmpPayDetail.setInterest(rs.getString("AMT_INTEREST"));
		tmpPayDetail.setPartIndex(rs.getString("PART_INDEX"));
		tmpPayDetail.setBalancePrincipal(rs.getString("BALANCE_PRINCIPAL"));
		tmpPayDetail.setBalanceInterest(rs.getString("BALANCE_INTEREST"));
		return tmpPayDetail;
	}	
}