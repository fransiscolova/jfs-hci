package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;

import com.hci.data.dao.JfsPaymentInpayReversalDao;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.JfsPaymentInpayReversalContract;
import com.hci.data.entity.JfsPaymentInt;
import com.hci.data.entity.JfsTmpInpayReversalAllocation;
import com.hci.data.entity.JfsTmpPayDetail;
import com.hci.data.entity.JfsTmpPayHead;
import com.hci.util.LoggerUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentInpayReversal implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private ParentJobDao parentDao;
	/*private String targetFol;
	private String jobName;
	private String processName;*/
	JfsPaymentInpayReversalDao jfsPaymentInpayDao;

	/*public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}*/

	@Override
	public void afterPropertiesSet() throws Exception {
		//Assert.notNull(targetFol, "targetFol must be set");
		//Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		jfsPaymentInpayDao = (JfsPaymentInpayReversalDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentInpayReversalDao");
		List<String> contracts = contractCleansing(jfsPaymentInpayDao.getJfsContractInpay(), 100);
		
		for(String cnt : contracts) {
			// Create the tmpPayHead
			Map<String, List<JfsTmpPayHead>> tmpPayHead = list2MapPayHead(jfsPaymentInpayDao.getJfsTmpJFSPayHead(cnt));
			
			// Create the tmpPayDetail
			Map<String, List<JfsTmpPayDetail>> tmpPayDetail = list2MapPayDetail(jfsPaymentInpayDao.getJfsTmpJFSPayDetail(cnt));
			
			List<JfsTmpInpayReversalAllocation> inpayAllocations = new ArrayList<JfsTmpInpayReversalAllocation>();
			JfsTmpInpayReversalAllocation inpayAllocation = new JfsTmpInpayReversalAllocation();
			
			for(Map.Entry<String, List<JfsTmpPayHead>> payHead : tmpPayHead.entrySet()) {
				String amtPrincipalSum = "0";
				String amtInterestSum = "0";
				
				String paymentAmount = tmpPayHead.get(payHead.getKey()).get(0).getBalance();
				while(Integer.valueOf(paymentAmount) > 0) {
				
					inpayAllocation = new JfsTmpInpayReversalAllocation();
					JfsTmpPayHead head = getMinimalIndexHead(tmpPayHead.get(payHead.getKey()));
					JfsTmpPayDetail detail = getMinimalIndexDetail(tmpPayDetail.get(payHead.getKey()));
					
					inpayAllocation.setIncomingPaymentId(head.getIncomingPaymentId());
					inpayAllocation.setContractNumber(head.getContractNumber());
					inpayAllocation.setDueDate(detail.getDueDate());
					inpayAllocation.setPartIndex(detail.getPartIndex());
					inpayAllocation.setAmtPrincipal(getAllocatedPrincipal(head, detail));
					inpayAllocation.setAmtInterest(getAlocatedInterest(head, detail));
					
					amtPrincipalSum = String.valueOf(Integer.valueOf(amtPrincipalSum) + Integer.valueOf(inpayAllocation.getAmtPrincipal()));
					amtInterestSum = String.valueOf(Integer.valueOf(amtInterestSum) + Integer.valueOf(inpayAllocation.getAmtInterest()));
					
					updateJfsDetail(inpayAllocation, tmpPayDetail.get(inpayAllocation.getContractNumber()));
					updateJfsHead(inpayAllocation, tmpPayHead.get(inpayAllocation.getContractNumber()), amtPrincipalSum, amtInterestSum);
					
					paymentAmount = tmpPayHead.get(payHead.getKey()).get(0).getBalance();
					
					if((Integer.valueOf(tmpPayDetail.get(inpayAllocation.getContractNumber()).get(0).getBalanceInterest()) + Integer.valueOf(tmpPayDetail.get(inpayAllocation.getContractNumber()).get(0).getBalancePrincipal())) > 0) {
						inpayAllocation.setAmtOverpayment(paymentAmount);
						paymentAmount = "0";
					} else {
						inpayAllocation.setAmtOverpayment(null);
					}
					
					// Insert into jfs_inpay_allocation
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
					Date dt = sdf.parse(inpayAllocation.getDueDate());
					sdf.applyPattern("dd-MMM-yy");
					
					jfsPaymentInpayDao.setInpayAllocation(inpayAllocation.getContractNumber(), sdf.format(dt), inpayAllocation.getPartIndex(), 
							inpayAllocation.getAmtPrincipal(), inpayAllocation.getAmtInterest(), inpayAllocation.getIncomingPaymentId(), inpayAllocation.getAmtOverpayment());
				}
			}
		}
		
		return RepeatStatus.FINISHED;
	}
	
	public List<String> contractCleansing(List<JfsPaymentInpayReversalContract> contracts, int numOfLine) {
		List<String> contractList = new ArrayList<String>();
		String contractsString = "'";
		int norInContractString = 0;
		for(JfsPaymentInpayReversalContract contract : contracts) {
			if(contract.getNumOfRecords() > 0) {
				if(norInContractString < numOfLine) {
					contractsString = contractsString + contract.getContractNumber() + "','";
					norInContractString++;
				} else {
					contractList.add(contractsString.substring(0, contractsString.length() - 2));
					norInContractString = 1;
					contractsString = "'" + contract.getContractNumber() + "','";
					
				}
			} else {
				log.info("JFSPaymentInpay", contract.getContractNumber() + "doesn't have any record");
			}
		}
		contractList.add(contractsString.substring(0, contractsString.length() - 2));
		
		return contractList;
	}
	
	public Map<String, List<JfsTmpPayDetail>> list2MapPayDetail(List<JfsTmpPayDetail> payDetails) {
		Map<String, List<JfsTmpPayDetail>> payDetailMap = new HashMap<String, List<JfsTmpPayDetail>>();
		List<JfsTmpPayDetail> tmpPayDetail = new ArrayList<JfsTmpPayDetail>();
		for(JfsTmpPayDetail payDetail : payDetails) {
			if(!payDetailMap.containsKey(payDetail.getContractNumber())) {
				tmpPayDetail = new ArrayList<JfsTmpPayDetail>();
				tmpPayDetail.add(payDetail);
				payDetailMap.put(payDetail.getContractNumber(), tmpPayDetail);
			} else {
				tmpPayDetail = payDetailMap.get(payDetail.getContractNumber());
				tmpPayDetail.add(payDetail);
				payDetailMap.put(payDetail.getContractNumber(), tmpPayDetail);
			}
		}
		
		return payDetailMap;
	}
	
	public Map<String, List<JfsTmpPayHead>> list2MapPayHead(List<JfsTmpPayHead> payHeads) {
		Map<String, List<JfsTmpPayHead>> payHeadMap = new HashMap<String, List<JfsTmpPayHead>>();
		List<JfsTmpPayHead> tmpPayHead = new ArrayList<JfsTmpPayHead>();
		for(JfsTmpPayHead payHead : payHeads) {
			if(!payHeadMap.containsKey(payHead.getContractNumber())) {
				tmpPayHead = new ArrayList<JfsTmpPayHead>();
				tmpPayHead.add(payHead);
				payHeadMap.put(payHead.getContractNumber(), tmpPayHead);
			} else {
				tmpPayHead = payHeadMap.get(payHead.getContractNumber());
				tmpPayHead.add(payHead);
				payHeadMap.put(payHead.getContractNumber(), tmpPayHead);
			}
		}
		
		return payHeadMap;
	}
	
	public JfsTmpPayHead getMinimalIndexHead(List<JfsTmpPayHead> payHeads) {
		JfsTmpPayHead payHead = payHeads.get(0);
		for(JfsTmpPayHead ph : payHeads) {
			if(Integer.valueOf(ph.getBalance()) < 0) {
				if(Integer.valueOf(ph.getPartIndex()) < Integer.valueOf(payHead.getPartIndex())) {
					payHead = ph;
				}
			}
		}
		return payHead;
	}
	
	public JfsTmpPayDetail getMinimalIndexDetail(List<JfsTmpPayDetail> payDetails) {
		JfsTmpPayDetail payDetail = payDetails.get(0);
		for(JfsTmpPayDetail pd : payDetails) {
			if(Integer.valueOf(pd.getBalancePrincipal()) < 0 && Integer.valueOf(pd.getBalanceInterest()) < 0) {
				if(Integer.valueOf(pd.getPartIndex()) < Integer.valueOf(payDetail.getPartIndex())) {
					payDetail = pd;
				}
			}
		}
		return payDetail;
	}
	
	public String getAllocatedPrincipal(JfsTmpPayHead head, JfsTmpPayDetail detail) {
		int cntDpd = Integer.valueOf(head.getCntDpd());
		int balance = Integer.valueOf(head.getBalance());
		int balancePrincipal = Integer.valueOf(detail.getBalancePrincipal());
		int balanceInterest = Integer.valueOf(detail.getBalanceInterest());
		
		if(cntDpd <= 90) {
			if(balance <= (balancePrincipal + balanceInterest)) {
				return detail.getBalancePrincipal();
			} else if(balance > (balancePrincipal + balanceInterest) && balance < balanceInterest && (balance - balanceInterest) > balancePrincipal) {
				return String.valueOf(balance - balanceInterest);
			} else if(balance > (balancePrincipal + balanceInterest) && (balance - balanceInterest) >= 0) {
				return "0";
			}
		} else {
			if(balance <= (balancePrincipal + balanceInterest)) {
				return String.valueOf(balancePrincipal);
			} else if(balance > (balancePrincipal + balanceInterest) && balance <= balancePrincipal) {
				return String.valueOf(balancePrincipal);
			} else if(balance > (balancePrincipal + balanceInterest) && balance > balancePrincipal) {
				return String.valueOf(balance);
			}
		}
		return "";
	}
	
	public String getNewBalancePrincipal(JfsTmpPayHead head, JfsTmpPayDetail detail) {
		int cntDpd = Integer.valueOf(head.getCntDpd());
		int balance = Integer.valueOf(head.getBalance());
		int balancePrincipal = Integer.valueOf(detail.getBalancePrincipal());
		int balanceInterest = Integer.valueOf(detail.getBalanceInterest());
		
		if(cntDpd <= 90) {
			if(balance <= (balancePrincipal + balanceInterest)) {
				return "0";
			} else if(balance > (balancePrincipal + balanceInterest) && balance < balanceInterest && (balance - balanceInterest) > balancePrincipal) {
				return String.valueOf(balancePrincipal - balance - balanceInterest);
			} else if(balance > (balancePrincipal + balanceInterest) && (balance - balanceInterest) >= 0) {
				return String.valueOf(balancePrincipal);
			}
		} else {
			if(balance <= (balancePrincipal + balanceInterest)) {
				return "0";
			} else if(balance > (balancePrincipal + balanceInterest) && balance <= balancePrincipal) {
				return "0";
			} else if(balance > (balancePrincipal + balanceInterest) && balance > balancePrincipal) {
				return String.valueOf(balancePrincipal - balance);
			}
		}
		return "";
	}
	
	public String getAlocatedInterest(JfsTmpPayHead head, JfsTmpPayDetail detail) {
		int cntDpd = Integer.valueOf(head.getCntDpd());
		int balance = Integer.valueOf(head.getBalance());
		int balancePrincipal = Integer.valueOf(detail.getBalancePrincipal());
		int balanceInterest = Integer.valueOf(detail.getBalanceInterest());
		
		if(cntDpd <= 90) {
			if(balance <= (balancePrincipal + balanceInterest)) {
				return String.valueOf(balanceInterest);
			} else if(balance > (balancePrincipal + balanceInterest) && balance <= balanceInterest) {
				return String.valueOf(balanceInterest);
			} else if(balance > (balancePrincipal + balanceInterest) && (balance > balanceInterest)) {
				return String.valueOf(balance);
			}
		} else {
			if(balance <= (balancePrincipal + balanceInterest)) {
				return String.valueOf(balanceInterest);
			} else if(balance > (balancePrincipal + balanceInterest) && balance < balancePrincipal && (balance - balancePrincipal) > balanceInterest) {
				return String.valueOf(balance - balancePrincipal);
			} else if(balance > (balancePrincipal + balanceInterest) && (balance - balancePrincipal) >= 0) {
				return "0";
			}
		}
		
		return "";
	}
	
	public String getNewBalanceInterest(JfsTmpPayHead head, JfsTmpPayDetail detail) {
		int cntDpd = Integer.valueOf(head.getCntDpd());
		int balance = Integer.valueOf(head.getBalance());
		int balancePrincipal = Integer.valueOf(detail.getBalancePrincipal());
		int balanceInterest = Integer.valueOf(detail.getBalanceInterest());
		
		if(cntDpd <= 90) {
			if(balance <= (balancePrincipal + balanceInterest)) {
				return "0";
			} else if(balance > (balancePrincipal + balanceInterest) && balance <= balanceInterest) {
				return "0";
			} else if(balance > (balancePrincipal + balanceInterest) && (balance > balanceInterest)) {
				return String.valueOf(balanceInterest - balance);
			}
		} else {
			if(balance <= (balancePrincipal + balanceInterest)) {
				return "0";
			} else if(balance > (balancePrincipal + balanceInterest) && balance < balancePrincipal && (balance - balancePrincipal) > balanceInterest) {
				return String.valueOf(balanceInterest - balance - balancePrincipal);
			} else if(balance < (balancePrincipal + balanceInterest) && (balance - balancePrincipal) >= 0) {
				return String.valueOf(balanceInterest);
			}
		}
		
		return "";
	}
	
	public void updateJfsDetail(JfsTmpInpayReversalAllocation tmpInpayAllocation, List<JfsTmpPayDetail> details) {
		int index = 0;
		for(JfsTmpPayDetail payDetail : details) {
			if(payDetail.getDueDate().equals(tmpInpayAllocation.getDueDate()) && (Integer.valueOf(payDetail.getBalancePrincipal()) < 0 || Integer.valueOf(payDetail.getBalanceInterest()) < 0)) {
				payDetail.setBalancePrincipal(String.valueOf(Integer.valueOf(payDetail.getBalancePrincipal()) - Integer.valueOf(tmpInpayAllocation.getAmtPrincipal())));
				payDetail.setBalanceInterest(String.valueOf(Integer.valueOf(payDetail.getBalanceInterest()) - Integer.valueOf(tmpInpayAllocation.getAmtInterest())));
				details.remove(index);
				details.add(index, payDetail);
				break;
			}
			index++;
		}
	}
	
	public void updateJfsHead(JfsTmpInpayReversalAllocation tmpInpayAllocation, List<JfsTmpPayHead> heads, String amtPrincipalSum, String amtInterestSum) {
		int index = 0;
		JfsPaymentInt paymentInt = jfsPaymentInpayDao.getPaymentInt("A", "R", tmpInpayAllocation.getIncomingPaymentId());
		for(JfsTmpPayHead payHead : heads) {
			if(payHead.getIncomingPaymentId().equals(tmpInpayAllocation.getIncomingPaymentId())) {
				payHead.setBalance(String.valueOf(Integer.valueOf(paymentInt.getPmtInstalment()) - Integer.valueOf(amtPrincipalSum) - Integer.valueOf(amtInterestSum)));
				heads.remove(index);
				heads.add(index, payHead);
				break;
			}
			index++;
		}
	}
}