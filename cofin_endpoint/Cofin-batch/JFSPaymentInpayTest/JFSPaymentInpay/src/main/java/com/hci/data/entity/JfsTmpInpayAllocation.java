package com.hci.data.entity;

public class JfsTmpInpayAllocation {
	private String contractNumber;
	private String dueDate;
	private String partIndex;
	private String amtPrincipal;
	private String amtInterest;
	private String incomingPaymentId;
	private String dtimeCreated;
	private String procNo;
	private String amtOverpayment;
	
	public String getAmtOverpayment() {
		return amtOverpayment;
	}
	public void setAmtOverpayment(String amtOverpayment) {
		this.amtOverpayment = amtOverpayment;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getPartIndex() {
		return partIndex;
	}
	public void setPartIndex(String partIndex) {
		this.partIndex = partIndex;
	}
	public String getAmtPrincipal() {
		return amtPrincipal;
	}
	public void setAmtPrincipal(String amtPrincipal) {
		this.amtPrincipal = amtPrincipal;
	}
	public String getAmtInterest() {
		return amtInterest;
	}
	public void setAmtInterest(String amtInterest) {
		this.amtInterest = amtInterest;
	}
	public String getIncomingPaymentId() {
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(String incomingPaymentId) {
		this.incomingPaymentId = incomingPaymentId;
	}
	public String getDtimeCreated() {
		return dtimeCreated;
	}
	public void setDtimeCreated(String dtimeCreated) {
		this.dtimeCreated = dtimeCreated;
	}
	public String getProcNo() {
		return procNo;
	}
	public void setProcNo(String procNo) {
		this.procNo = procNo;
	}
}