package com.hci.data.entity;

public class JfsTmpPayDetail {
	private String contractNumber;
	private String dueDate;
	private String principal;
	private String interest;
	private String partIndex;
	private String balancePrincipal;
	private String balanceInterest;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	public String getInterest() {
		return interest;
	}
	public void setInterest(String interest) {
		this.interest = interest;
	}
	public String getPartIndex() {
		return partIndex;
	}
	public void setPartIndex(String partIndex) {
		this.partIndex = partIndex;
	}
	public String getBalancePrincipal() {
		return balancePrincipal;
	}
	public void setBalancePrincipal(String balancePrincipal) {
		this.balancePrincipal = balancePrincipal;
	}
	public String getBalanceInterest() {
		return balanceInterest;
	}
	public void setBalanceInterest(String balanceInterest) {
		this.balanceInterest = balanceInterest;
	}
}
