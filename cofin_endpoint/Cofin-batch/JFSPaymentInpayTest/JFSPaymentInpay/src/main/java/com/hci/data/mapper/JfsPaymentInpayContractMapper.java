package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsPaymentInpay;
import com.hci.data.entity.JfsPaymentInpayContract;

public class JfsPaymentInpayContractMapper implements RowMapper<JfsPaymentInpayContract> {

	@Override
	public JfsPaymentInpayContract mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsPaymentInpayContract jfsPayment = new JfsPaymentInpayContract();
		jfsPayment.setContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setNumOfRecords(rs.getInt("NUM_OF_REC"));
		return jfsPayment;
	}	
}