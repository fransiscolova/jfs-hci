package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.hci.data.entity.JfsPaymentInpay;

public class JfsPaymentInpayMapper implements RowMapper<JfsPaymentInpay> {

	@Override
	public JfsPaymentInpay mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsPaymentInpay jfsPayment = new JfsPaymentInpay();
		jfsPayment.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setClientName(rs.getString("CLIENT_NAME"));
		jfsPayment.setDatePayment(rs.getString("DATE_PAYMENT"));
		jfsPayment.setPmtInstalment(rs.getString("PMT_INSTALMENT"));
		jfsPayment.setPmtPenalty(rs.getString("PMT_PENALTY"));
		jfsPayment.setPmtFee(rs.getString("PMT_FEE"));
		jfsPayment.setNameAgreement(rs.getString("AGREEMENT_ID"));
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPayment;
	}
	
}
