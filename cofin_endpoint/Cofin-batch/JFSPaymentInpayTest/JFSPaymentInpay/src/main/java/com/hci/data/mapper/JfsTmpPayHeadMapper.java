package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.hci.data.entity.JfsTmpPayHead;

public class JfsTmpPayHeadMapper implements RowMapper<JfsTmpPayHead> {

	@Override
	public JfsTmpPayHead mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsTmpPayHead tmpPayHead = new JfsTmpPayHead();
		tmpPayHead.setContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		tmpPayHead.setIncomingPaymentId(rs.getString("INCOMING_PAYMENT_ID"));
		tmpPayHead.setDatePayment(rs.getString("DATE_PAYMENT"));
		tmpPayHead.setPmtInstalment(rs.getString("PMT_INSTALMENT"));
		tmpPayHead.setBalance(rs.getString("BALANCE"));
		tmpPayHead.setPartIndex(rs.getString("PART_INDEX"));
		tmpPayHead.setCntDpd(rs.getString("CNT_DAY_PAST_DUE"));
		return tmpPayHead;
	}	
}