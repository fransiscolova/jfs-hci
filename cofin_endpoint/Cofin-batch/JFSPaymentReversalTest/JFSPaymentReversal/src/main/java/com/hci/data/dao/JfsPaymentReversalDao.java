package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsPaymentReversal;

public interface JfsPaymentReversalDao {
	public List<JfsPaymentReversal> getJfsPaymentReversalData();
	public List<JfsPaymentReversal> getJfsPaymentReversalData(String runDate);
}
