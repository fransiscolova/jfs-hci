package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsPaymentReversalDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsPaymentReversal;
import com.hci.data.mapper.JfsPaymentReversalMapper;

public class JfsPaymentReversalImpl implements JfsPaymentReversalDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	public JfsPaymentReversalImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsPaymentReversal> getJfsPaymentReversalData() {
		String SQL = "select p.text_contract_number, c.client_name, " +
				"to_char(p.date_payment,'dd/MM/yyyy') date_payment, p.pmt_instalment, " + 
				"p.pmt_penalty, p.pmt_fee, ja.code as agreement_id, jp.id as partner_id " +
				"from jfs_payment_int p inner join jfs_contract c " + 
				"on p.text_contract_number=c.text_contract_number " + 
				"left join jfs_agreement ja on c.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " + 
				"where trunc(p.date_export)=trunc(sysdate) and not parent_id is null " + 
				"order by p.text_contract_number";
		List<JfsPaymentReversal> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentReversalMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsPaymentReversal> getJfsPaymentReversalData(String runDate) {
		String SQL = "select p.text_contract_number, c.client_name, " +
				"to_char(p.date_payment,'dd/MM/yyyy') date_payment, p.pmt_instalment, " + 
				"p.pmt_penalty, p.pmt_fee, ja.code as agreement_id, jp.id as partner_id " +
				"from jfs_payment_int p inner join jfs_contract c " + 
				"on p.text_contract_number=c.text_contract_number " + 
				"left join jfs_agreement ja on c.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " + 
				"where trunc(p.date_export)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and not parent_id is null " + 
				"order by p.text_contract_number";
		List<JfsPaymentReversal> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentReversalMapper());
		return jfsPayment;
	}
}
