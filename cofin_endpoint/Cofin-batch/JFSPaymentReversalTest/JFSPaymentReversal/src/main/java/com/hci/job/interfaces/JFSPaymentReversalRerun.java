package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.hci.JobLauncherJFSPaymentReversal;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ReExecuteJob;
import com.hci.job.sub.JFSPaymentReversalSub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentReversalRerun implements Tasklet, InitializingBean {
	
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private ParentJobDao reExecuteJobs;
	
	private String targetFol;
	private String jobName;
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		reExecuteJobs = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		//reExecuteJobs.runProcedure("P_JFS_PAYMENT_REVERSAL");
		
		if(!checkTodayJob(processName)) {
			// Check whether the parent has been run
			log.info("JFSPaymentReversalRerun", "Today job not yet run");
			if(checkParentJob(processName)) {
				JobDetail jd = new JobDetail();
				try {
					jd = new JobDetail("JFSPaymentReversalRun", Scheduler.DEFAULT_GROUP, JobLauncherJFSPaymentReversal.class);
					jd.getJobDataMap().put("test", "testApp");
					Trigger tgr = TriggerUtils.makeImmediateTrigger(0, 0);
					tgr.setName("FireOnceRun");
					
					Scheduler scheduler = new StdSchedulerFactory().getScheduler();
					scheduler.start();
					scheduler.scheduleJob(jd, tgr);
				} catch (SchedulerException e) {
					e.printStackTrace();
				}
				
				return RepeatStatus.FINISHED;
			}
			log.info("JFSPaymentReversalRerun", "Parent job of today job not yet run");
		}
		
		log.info("JFSPaymentReversalRerun", "Rerun the job");
		
		List<ReExecuteJob> jobs = getAllReExecuteJob(jobName);
		boolean paymentFileStatus = false;
		String fileDate = "";
		if(jobs.size() != 0) {
			for (ReExecuteJob job : jobs) {
				int numOfRerun = job.getNumOfRerun() + 1;
				
				JFSPaymentReversalSub jfsPaymentSub = new JFSPaymentReversalSub();
				paymentFileStatus = jfsPaymentSub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "N", processName);
				fileDate = job.getRunDate();
			}
		}
		
		if (paymentFileStatus) {
			sendEmail(fileDate);
		}
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		log.info("JFSPaymentReversalRerun", "Rerun job finish");
		
		return RepeatStatus.FINISHED;
	}
	
	public List<ReExecuteJob> getAllReExecuteJob(String jobName) {
		List<ReExecuteJob> jobs = reExecuteJobs.getRerunJob(jobName);
		return jobs;
	}
	
	public boolean checkTodayJob(String processName) {
		return reExecuteJobs.checkTodayRunJob(processName, sdf.format(new Date()));
	}
	
	public boolean checkParentJob(String processName) {
		return reExecuteJobs.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void sendEmail(String fileDate) throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS - HCID Payment Reversal";
		StringBuffer content = new StringBuffer();
		content.append("There are new HCID Payment Reversal that have to be registered to BTPN\n\n");
		content.append("You can find the file:\n");
		content.append("/WS/Data/JFS/Export/JFS_PAYMENT_REVERSAL_" + fileDate + ".txt.aes");
		emailUtil.sendEmail(subject, content);
	}
}