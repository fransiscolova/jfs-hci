package com.hci.job.sub;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hci.data.dao.JfsPaymentReversalDao;
import com.hci.data.entity.JfsPaymentReversal;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentReversalSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsPaymentReversal> getAllData() {
		JfsPaymentReversalDao jfsPaymentDao = (JfsPaymentReversalDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentReversalDao");
		List<JfsPaymentReversal> jfsPayments = jfsPaymentDao.getJfsPaymentReversalData();
		return jfsPayments;
	}
	
	public List<JfsPaymentReversal> getAllData(String runDate) {
		JfsPaymentReversalDao jfsPaymentDao = (JfsPaymentReversalDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentReversalDao");
		List<JfsPaymentReversal> jfsPayments = jfsPaymentDao.getJfsPaymentReversalData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		BigDecimal result = BigDecimal.ZERO;
		
		paymentDetailDataString.add(header);
		long footerSum = 0;
		List<JfsPaymentReversal> jps;
		
		if(runDate.equals("")) {
			jps = getAllData();
		} else {
			jps = getAllData(runDate);
		}
		
		log.info("JFSPaymentReversalSub", "Num of file lines : " + jps.size());
		
		for(JfsPaymentReversal jp : jps) {
			if(!partnerAgreement.containsKey(jp.getNameAgreement())) {
				partnerAgreement.put(jp.getNameAgreement(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getTextContractNumber() + "|" + jp.getClientName() + "|" + jp.getDatePayment() + "|" + jp.getPmtInstalment() + "|" + jp.getPmtPenalty() + "|0|" + jp.getPmtFee() + "|R" );
			result = result.add(new BigDecimal(Integer.valueOf(jp.getPmtInstalment()) + Integer.valueOf(jp.getPmtPenalty() + Integer.valueOf(jp.getPmtFee()))));
		}
		paymentDetailDataString.add("HCI|" + jps.size() + "|" + result.toString());
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSPaymentReversalSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();

		String fileName = targetFol + "JFS_PAYMENT_REVERSAL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "JFS_PAYMENT_REVERSAL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		String processedFileName = targetFol + "_processed/JFS_PAYMENT_REVERSAL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|5|" + dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if(runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, processedFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, processedFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSPaymentReversalSub", "JobStatus : " + jobStatus);
		log.info("JFSPaymentReversalSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
