package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsPaymentSmallUnderpayment;

public interface JfsPaymentSmallUnderpaymentDao {
	public List<JfsPaymentSmallUnderpayment> getJfsPaymentSmallUnderpaymentData();
	public List<JfsPaymentSmallUnderpayment> getJfsPaymentSmallUnderpaymentData(String runDate);
}
