package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsPaymentSmallUnderpaymentDetail;

public interface JfsPaymentSmallUnderpaymentDetailDao {
	public List<JfsPaymentSmallUnderpaymentDetail> getJfsPaymentSmallUnderpaymentDetailData();
	public List<JfsPaymentSmallUnderpaymentDetail> getJfsPaymentSmallUnderpaymentDetailData(String runDate);
}
