package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsPaymentSmallUnderpaymentDetailDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsPaymentSmallUnderpaymentDetail;
import com.hci.data.mapper.JfsPaymentSmallUnderpaymentDetailMapper;

public class JfsPaymentSmallUnderpaymentDetailImpl implements JfsPaymentSmallUnderpaymentDetailDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsPaymentSmallUnderpaymentDetailImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	@Override
	public List<JfsPaymentSmallUnderpaymentDetail> getJfsPaymentSmallUnderpaymentDetailData() {
		String SQL = "select replace(jc.BANK_REFERENCE_NO,chr(13),'') BANK_REFERENCE_NO, " +  
				"jp.text_contract_number, jp.date_payment, ja.code agreement_id, jpr.id partner_id " + 
				"from jfs_payment_int jp left join jfs_contract jc on jp.text_contract_number=jc.text_contract_number " +  
				"left join jfs_agreement ja on jc.id_agreement = ja.code " +
				"left join jfs_partner jpr on jpr.id = ja.partner_id " +
				"where trunc(jp.date_export)=trunc(sysdate) and jp.payment_type='U' order by jp.date_payment"; 
		List<JfsPaymentSmallUnderpaymentDetail> jfsPaymentDetails = jdbcTemplateObject.query(SQL,  new JfsPaymentSmallUnderpaymentDetailMapper());
		return jfsPaymentDetails;
	}
	
	@Override
	public List<JfsPaymentSmallUnderpaymentDetail> getJfsPaymentSmallUnderpaymentDetailData(String runDate) {
		String SQL = "select replace(jc.BANK_REFERENCE_NO,chr(13),'') BANK_REFERENCE_NO, " +  
				"jp.text_contract_number, jp.date_payment, ja.code agreement_id, jpr.id partner_id " + 
				"from jfs_payment_int jp left join jfs_contract jc on jp.text_contract_number=jc.text_contract_number " +  
				"left join jfs_agreement ja on jc.id_agreement = ja.code " +
				"left join jfs_partner jpr on jpr.id = ja.partner_id " +
				"where trunc(jp.date_export)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and jp.payment_type='U' order by jp.date_payment";
		List<JfsPaymentSmallUnderpaymentDetail> jfsPaymentDetails = jdbcTemplateObject.query(SQL,  new JfsPaymentSmallUnderpaymentDetailMapper());
		return jfsPaymentDetails;
	}
}
