package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsPaymentSmallUnderpaymentDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsPaymentSmallUnderpayment;
import com.hci.data.mapper.JfsPaymentSmallUnderpaymentMapper;

public class JfsPaymentSmallUnderpaymentImpl implements JfsPaymentSmallUnderpaymentDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsPaymentSmallUnderpaymentImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsPaymentSmallUnderpayment> getJfsPaymentSmallUnderpaymentData() {
		String SQL = "select p.text_contract_number, c.client_name, " +  
				"to_char(p.date_payment,'dd/MM/yyyy') date_payment, " + 
				"p.payment_type, ja.code agreement_id, jp.id partner_id " + 
				"from jfs_payment_int p " +  						
				"inner join jfs_contract c on p.text_contract_number=c.text_contract_number " +  
				"left join jfs_agreement ja on c.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " +
				"where trunc(p.date_export)=trunc(sysdate) " + 
				"and p.payment_type='U' order by p.date_payment"; 
		List<JfsPaymentSmallUnderpayment> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentSmallUnderpaymentMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsPaymentSmallUnderpayment> getJfsPaymentSmallUnderpaymentData(String runDate) {
		String SQL = "select p.text_contract_number, c.client_name, " +  
				"to_char(p.date_payment,'dd/MM/yyyy') date_payment, " + 
				"p.payment_type, ja.code agreement_id, jp.id partner_id " + 
				"from jfs_payment_int p " +  						
				"inner join jfs_contract c on p.text_contract_number=c.text_contract_number " +  
				"left join jfs_agreement ja on c.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " +
				"where trunc(p.date_export)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) " + 
				"and p.payment_type='U' order by p.date_payment";
		List<JfsPaymentSmallUnderpayment> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentSmallUnderpaymentMapper());
		return jfsPayment;
	}
}
