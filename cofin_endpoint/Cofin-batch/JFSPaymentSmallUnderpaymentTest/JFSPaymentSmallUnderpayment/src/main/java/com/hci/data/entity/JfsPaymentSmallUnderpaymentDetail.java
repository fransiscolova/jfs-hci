package com.hci.data.entity;

public class JfsPaymentSmallUnderpaymentDetail {
	private String bankReferenceNo;
	private String textContractNumber;
	private String datePayment;
	private String agreementId;
	private String partnerId;
	
	public String getBankReferenceNo() {
		return bankReferenceNo;
	}
	public String getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public void setBankReferenceNo(String bankReferenceNo) {
		this.bankReferenceNo = bankReferenceNo;
	}
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(String datePayment) {
		this.datePayment = datePayment;
	}
}
