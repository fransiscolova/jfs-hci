package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsPaymentSmallUnderpaymentDetail;

public class JfsPaymentSmallUnderpaymentDetailMapper  implements RowMapper<JfsPaymentSmallUnderpaymentDetail> {

	@Override
	public JfsPaymentSmallUnderpaymentDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		DateFormat dfFinish = new SimpleDateFormat("dd-MMM-yy");
		String datePaymentString = "";
		try {
			datePaymentString = dfFinish.format(df.parse(rs.getString("DATE_PAYMENT"))).toString().toUpperCase();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		JfsPaymentSmallUnderpaymentDetail jfsPaymentDetail = new JfsPaymentSmallUnderpaymentDetail();
		jfsPaymentDetail.setBankReferenceNo(rs.getString("BANK_REFERENCE_NO"));
		jfsPaymentDetail.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		//jfsPaymentDetail.setDatePayment(rs.getString("DATE_PAYMENT"));
		jfsPaymentDetail.setDatePayment(datePaymentString);
		jfsPaymentDetail.setAgreementId(rs.getString("AGREEMENT_ID"));
		jfsPaymentDetail.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPaymentDetail;
	}
}
