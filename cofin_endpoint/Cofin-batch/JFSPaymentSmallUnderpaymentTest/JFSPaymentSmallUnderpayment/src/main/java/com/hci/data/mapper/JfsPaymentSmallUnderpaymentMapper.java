package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.hci.data.entity.JfsPaymentSmallUnderpayment;

public class JfsPaymentSmallUnderpaymentMapper implements RowMapper<JfsPaymentSmallUnderpayment> {

	@Override
	public JfsPaymentSmallUnderpayment mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsPaymentSmallUnderpayment jfsPayment = new JfsPaymentSmallUnderpayment();
		jfsPayment.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setClientName(rs.getString("CLIENT_NAME"));
		jfsPayment.setDatePayment(rs.getString("DATE_PAYMENT"));
		jfsPayment.setPaymentType(rs.getString("PAYMENT_TYPE"));
		jfsPayment.setAgreementId(rs.getString("AGREEMENT_ID"));
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPayment;
	}
	
}
