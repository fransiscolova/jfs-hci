package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ReExecuteJob;
import com.hci.job.sub.JFSPaymentSmallUnderpaymentDetail1Sub;
import com.hci.job.sub.JFSPaymentSmallUnderpaymentDetailSub;
import com.hci.job.sub.JFSPaymentSmallUnderpaymentSub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentSmallUnderpayment implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private ParentJobDao parentDao;
	
	private String targetFol;
	private String jobName;
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(processName, "processName must be set");
		Assert.notNull(targetFol, "targetFol must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		parentDao.runProcedure("P_JFS_PAYMENT_SUP");
		parentDao.runProcedure("P_JFS_PAYMENT_SUP_GIFT");
		
		if(checkHoliday()) {
			log.info("JFSPaymentSmallUnderpayment", "Job skipped, holiday");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(!checkParentJob(processName)) {
			log.info("JFSPaymentSmallUnderpayment", "Job skipped, parent job not run");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(checkTodayRunJob(processName)) {
			log.info("JFSPaymentSmallUnderpayment", "Job skipped, already runned");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		log.info("JFSPaymentSmallUnderpayment", "Job not skipped");
		
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		setForceRunToN(processName);
		
		JFSPaymentSmallUnderpaymentSub jfsPaymentSub = new JFSPaymentSmallUnderpaymentSub();
		paymentFileStatus = jfsPaymentSub.createFile(targetFol, uuid, "", 0, "", processName);
		
		JFSPaymentSmallUnderpaymentDetailSub jfsPaymentDetailSub = new JFSPaymentSmallUnderpaymentDetailSub();
		jfsPaymentDetailSub.createFile(targetFol, uuid, "", 0, "", processName);
		
		JFSPaymentSmallUnderpaymentDetail1Sub jfsPaymentDetail1Sub = new JFSPaymentSmallUnderpaymentDetail1Sub();
		jfsPaymentDetail1Sub.createFile(targetFol, uuid, "", 0, "", processName);
		
		if (paymentFileStatus) {
			sendEmail();
		}
		
		log.info("JFSPaymentSmallUnderpayment", "Job done");
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		return RepeatStatus.FINISHED;
	}
	
	public boolean checkHoliday() {
		return parentDao.checkHoliday();
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void setForceRunToN(String processName) {
		parentDao.setForceRunNChildJob(processName);
	}
	
	public void sendEmail() throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS - HCID Payment";
		StringBuffer content = new StringBuffer();
		content.append("There are new HCID Payment that have to be registered to BTPN\n\n");
		content.append("You can find the file:\n");
		content.append(" /WS/Data/JFS/Export/JFS_SMALL_UNDERPAYMENT_" + sdf.format(new Date()) + ".txt");
		content.append(" /WS/Data/JFS/Export/JFS_SMALL_UNDERPAYMENT_DETAIL_" + sdf.format(new Date()) + ".txt");
		emailUtil.sendEmail(subject, content);
	}
	
	public boolean checkTodayRunJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
}