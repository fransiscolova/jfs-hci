package com.hci.job.sub;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hci.data.dao.JfsPaymentSmallUnderpaymentDetailDao;
import com.hci.data.entity.JfsPaymentSmallUnderpaymentDetail;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentSmallUnderpaymentDetailSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsPaymentSmallUnderpaymentDetail> getAllData() {
		JfsPaymentSmallUnderpaymentDetailDao jfsPaymentDetailDao = (JfsPaymentSmallUnderpaymentDetailDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentSmallUnderpaymentDetailDao");
		List<JfsPaymentSmallUnderpaymentDetail> jfsPaymentDetailss = jfsPaymentDetailDao.getJfsPaymentSmallUnderpaymentDetailData();
		return jfsPaymentDetailss;
	}
	
	public List<JfsPaymentSmallUnderpaymentDetail> getAllData(String runDate) {
		JfsPaymentSmallUnderpaymentDetailDao jfsPaymentDetailDao = (JfsPaymentSmallUnderpaymentDetailDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentSmallUnderpaymentDetailDao");
		List<JfsPaymentSmallUnderpaymentDetail> jfsPaymentDetailss = jfsPaymentDetailDao.getJfsPaymentSmallUnderpaymentDetailData(runDate);
		return jfsPaymentDetailss;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		int numOfRec = 0;
		List<JfsPaymentSmallUnderpaymentDetail> jps;
		if(runDate.equals("")) {
			jps = getAllData();
		} else {
			jps = getAllData(runDate);
		}
		
		log.info("JFSPaymentSmallUnderpaymentDetailSub", "Num of file lines : " + jps.size());
		
		for(JfsPaymentSmallUnderpaymentDetail jp : jps) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getBankReferenceNo() + ";" + jp.getTextContractNumber() + ";" + jp.getDatePayment() + ";0");
			numOfRec++;
		}
		
		for (int i = 0; i < numOfRec; i++) {
			paymentDetailDataString.add("TOTAL;;0");
		}
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSPaymentSmallUnderpaymentDetailSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_SMALL_UNDERPAYMENT_DETAIL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".csv";
		
		Map<String, Object> dataToWrite = createPaymentFileData("REF;TEXT_CONTRACT_NUMBER;TRANSACTION_DATE;AMT_PAYMENT", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		if(runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, fileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSPaymentSmallUnderpaymentDetailSub", "JobStatus : " + jobStatus);
		log.info("JFSPaymentSmallUnderpaymentDetailSub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
