package com.hci.job.sub;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hci.data.dao.JfsPaymentSmallUnderpaymentDao;
import com.hci.data.entity.JfsPaymentSmallUnderpayment;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentSmallUnderpaymentSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsPaymentSmallUnderpayment> getAllData() {
		JfsPaymentSmallUnderpaymentDao jfsPaymentDao = (JfsPaymentSmallUnderpaymentDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentSmallUnderpaymentDao");
		List<JfsPaymentSmallUnderpayment> jfsPayments = jfsPaymentDao.getJfsPaymentSmallUnderpaymentData();
		return jfsPayments;
	}
	
	public List<JfsPaymentSmallUnderpayment> getAllData(String runDate) {
		JfsPaymentSmallUnderpaymentDao jfsPaymentDao = (JfsPaymentSmallUnderpaymentDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentSmallUnderpaymentDao");
		List<JfsPaymentSmallUnderpayment> jfsPayments = jfsPaymentDao.getJfsPaymentSmallUnderpaymentData(runDate);
		return jfsPayments;
	}
		
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		int numOfData = 0;
		
		List<JfsPaymentSmallUnderpayment> jps;
		if (runDate.equals("")) {
			jps = getAllData();
		} else {
			jps = getAllData(runDate);
		}
		
		log.info("JFSPaymentSmallUnderpaymentSub", "Num of file lines : " + jps.size());
		
		for(JfsPaymentSmallUnderpayment jp : jps) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getTextContractNumber() + "|" + jp.getClientName() + "|" + jp.getDatePayment() + "|0|0|0|0|" + jp.getPaymentType());
			numOfData++;
		}
		paymentDetailDataString.add("HCI|" + numOfData + "|0");
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSPaymentSmallUnderpaymentSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_SMALL_UNDERPAYMENT_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "JFS_SMALL_UNDERPAYMENT_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|5|" + dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if(runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSPaymentSmallUnderpaymentSub", "JobStatus : " + jobStatus);
		log.info("JFSPaymentSmallUnderpaymentSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
