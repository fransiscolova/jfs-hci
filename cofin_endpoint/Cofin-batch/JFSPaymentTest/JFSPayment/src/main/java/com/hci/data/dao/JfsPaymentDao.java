package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsPayment;

public interface JfsPaymentDao {
	public List<JfsPayment> getJfsPaymentData();
	public List<JfsPayment> getJfsPaymentData(String runDate);
}
