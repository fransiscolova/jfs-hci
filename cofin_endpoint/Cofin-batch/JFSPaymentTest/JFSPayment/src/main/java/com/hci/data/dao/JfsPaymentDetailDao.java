package com.hci.data.dao;

import java.util.List;
import com.hci.data.entity.JfsPaymentDetail;

public interface JfsPaymentDetailDao {
	public List<JfsPaymentDetail> getJfsPaymentDetailData();
	public List<JfsPaymentDetail> getJfsPaymentDetailData(String runDate);
}
