package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.hci.data.dao.JfsPaymentDetailDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsPayment;
import com.hci.data.entity.JfsPaymentDetail;
import com.hci.data.mapper.JfsPaymentDetailMapper;
import com.hci.data.mapper.JfsPaymentMapper;

public class JfsPaymentDetailImpl implements JfsPaymentDetailDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	public JfsPaymentDetailImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsPaymentDetail> getJfsPaymentDetailData() {
		String SQL = "select replace(jc.BANK_REFERENCE_NO,chr(13),'') BANK_REFERENCE_NO, " +  
				"jp.text_contract_number, jp.date_payment, jp.pmt_instalment, " +  
				"ja.code ID_AGREEMENT, jpn.id PARTNER_ID from jfs_payment_int jp " +  
				"left join jfs_contract jc on jp.text_contract_number=jc.text_contract_number " +  
				"left join jfs_agreement ja on jc.id_agreement = ja.code " +
				"left join jfs_partner jpn on ja.partner_id = jpn.id " +
				"where trunc(jp.date_export)=trunc(sysdate) and jp.payment_type='R' " +  
				"and jp.pmt_instalment > 0 order by jp.date_payment";
		List<JfsPaymentDetail> jfsPaymentDetails = jdbcTemplateObject.query(SQL,  new JfsPaymentDetailMapper());
		return jfsPaymentDetails;
	}
	
	@Override
	public List<JfsPaymentDetail> getJfsPaymentDetailData(String runDate) {
		String SQL = "select replace(jc.BANK_REFERENCE_NO,chr(13),'') BANK_REFERENCE_NO, " +  
				"jp.text_contract_number, jp.date_payment, jp.pmt_instalment, " +  
				"ja.code ID_AGREEMENT, jpn.id PARTNER_ID from jfs_payment_int jp " +  
				"left join jfs_contract jc on jp.text_contract_number=jc.text_contract_number " +  
				"left join jfs_agreement ja on jc.id_agreement = ja.code " +
				"left join jfs_partner jpn on ja.partner_id = jpn.id " +
				//"where trunc(jp.date_export)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and jp.payment_type='R' " +  
				"where trunc(jp.date_export)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) and jp.payment_type='R' " +
				"and jp.pmt_instalment > 0 order by jp.date_payment";
		List<JfsPaymentDetail> jfsPaymentDetails = jdbcTemplateObject.query(SQL,  new JfsPaymentDetailMapper());
		return jfsPaymentDetails;
	}
}
