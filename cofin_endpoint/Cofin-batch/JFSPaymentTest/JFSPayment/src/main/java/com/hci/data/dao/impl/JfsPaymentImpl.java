package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.hci.data.dao.JfsPaymentDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsPayment;
import com.hci.data.mapper.JfsPaymentMapper;

public class JfsPaymentImpl implements JfsPaymentDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	public JfsPaymentImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}	
	
	@Override
	public List<JfsPayment> getJfsPaymentData() {
		String SQL = "select " +
				"p.text_contract_number, c.client_name, to_char(p.date_payment,'dd/MM/yyyy') as date_payment, " + 
				"p.pmt_instalment, p.pmt_penalty, p.pmt_fee, p.payment_type, ja.code as agreement_id, " + 
				"jp.id as partner_id " +
				"from jfs_payment_int p " + 
				"inner join jfs_contract c on p.text_contract_number=c.text_contract_number " + 
				"left join jfs_agreement ja on c.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " +
				"where trunc(p.date_export)=trunc(sysdate) " + 
				"and p.payment_type='R' and p.pmt_instalment > 0 order by p.date_payment";
		List<JfsPayment> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsPayment> getJfsPaymentData(String runDate) {
		String SQL = "select " +
				"p.text_contract_number, c.client_name, to_char(p.date_payment,'dd/MM/yyyy') as date_payment, " + 
				"p.pmt_instalment, p.pmt_penalty, p.pmt_fee, p.payment_type, ja.code as agreement_id, " + 
				"jp.id as partner_id " +
				"from jfs_payment_int p " + 
				"inner join jfs_contract c on p.text_contract_number=c.text_contract_number " + 
				"left join jfs_agreement ja on c.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " +
				//"where trunc(p.date_export)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) " +
				"where trunc(p.date_export)=trunc(to_date('" + runDate + "', 'yyyy-mm-dd')) " +
				"and p.payment_type='R' and p.pmt_instalment > 0 order by p.date_payment";
		List<JfsPayment> jfsPayment = jdbcTemplateObject.query(SQL, new JfsPaymentMapper());
		return jfsPayment;
	}
}
