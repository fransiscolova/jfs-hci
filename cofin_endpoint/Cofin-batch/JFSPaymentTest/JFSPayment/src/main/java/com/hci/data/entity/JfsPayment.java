package com.hci.data.entity;

import java.sql.Date;

public class JfsPayment {
	private String textContractNumber;
	private String clientName;
	private String datePayment;
	private String pmtInstalment;
	private String pmtPenalty;
	private String pmtFee;
	private String paymentType;
	private String idAgreement;
	private String partnerId;
	
	public String getIdAgreement() {
		return idAgreement;
	}
	public void setIdAgreement(String idAgreement) {
		this.idAgreement = idAgreement;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(String datePayment) {
		this.datePayment = datePayment;
	}
	public String getPmtInstalment() {
		return pmtInstalment;
	}
	public void setPmtInstalment(String pmtInstalment) {
		this.pmtInstalment = pmtInstalment;
	}
	public String getPmtPenalty() {
		return pmtPenalty;
	}
	public void setPmtPenalty(String pmtPenalty) {
		this.pmtPenalty = pmtPenalty;
	}
	public String getPmtFee() {
		return pmtFee;
	}
	public void setPmtFee(String pmtFee) {
		this.pmtFee = pmtFee;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
}