package com.hci.data.entity;

public class JfsPaymentDetail {
	private String bankReferenceNo;
	private String textContractNumber;
	private String datePayment;
	private String pmtInstalment;
	private String idAgreement;
	private String partnerId;
	
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getIdAgreement() {
		return idAgreement;
	}
	public void setIdAgreement(String idAgreement) {
		this.idAgreement = idAgreement;
	}
	public String getBankReferenceNo() {
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo) {
		this.bankReferenceNo = bankReferenceNo;
	}
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(String datePayment) {
		this.datePayment = datePayment;
	}
	public String getPmtInstalment() {
		return pmtInstalment;
	}
	public void setPmtInstalment(String pmtInstalment) {
		this.pmtInstalment = pmtInstalment;
	}
}
