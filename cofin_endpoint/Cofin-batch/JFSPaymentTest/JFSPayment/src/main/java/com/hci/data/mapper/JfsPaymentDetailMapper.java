package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsPaymentDetail;

public class JfsPaymentDetailMapper  implements RowMapper<JfsPaymentDetail> {

	@Override
	public JfsPaymentDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		DateFormat dfFinish = new SimpleDateFormat("dd-MMM-yy");
		String datePaymentString = "";
		try {
			datePaymentString = dfFinish.format(df.parse(rs.getString("DATE_PAYMENT"))).toString().toUpperCase();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JfsPaymentDetail jfsPaymentDetail = new JfsPaymentDetail();
		jfsPaymentDetail.setBankReferenceNo(rs.getString("BANK_REFERENCE_NO"));
		jfsPaymentDetail.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPaymentDetail.setDatePayment(datePaymentString);
		jfsPaymentDetail.setPmtInstalment(rs.getString("PMT_INSTALMENT"));
		jfsPaymentDetail.setIdAgreement(rs.getString("ID_AGREEMENT"));
		jfsPaymentDetail.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPaymentDetail;
	}
}
