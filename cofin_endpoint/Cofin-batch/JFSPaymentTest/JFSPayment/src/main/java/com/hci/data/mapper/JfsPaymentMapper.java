package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsPayment;

public class JfsPaymentMapper implements RowMapper<JfsPayment> {

	@Override
	public JfsPayment mapRow(ResultSet rs, int rowNum) throws SQLException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat dfFinish = new SimpleDateFormat("dd/MM/yyyy");
		String datePaymentString = "";
		try {
			datePaymentString = dfFinish.format(df.parse(rs.getString("DATE_PAYMENT"))).toString().toUpperCase();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JfsPayment jfsPayment = new JfsPayment();
		jfsPayment.setTextContractNumber(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setClientName(rs.getString("CLIENT_NAME"));
		jfsPayment.setDatePayment(datePaymentString);
		jfsPayment.setPmtInstalment(rs.getString("PMT_INSTALMENT"));
		jfsPayment.setPmtPenalty(rs.getString("PMT_PENALTY"));
		jfsPayment.setPmtFee(rs.getString("PMT_FEE"));
		jfsPayment.setPaymentType(rs.getString("PAYMENT_TYPE"));
		jfsPayment.setIdAgreement(rs.getString("AGREEMENT_ID"));
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPayment;
	}
	
}
