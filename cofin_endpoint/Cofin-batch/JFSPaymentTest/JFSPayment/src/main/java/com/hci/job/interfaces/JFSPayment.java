package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.data.dao.JfsPaymentDao;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.JfsPayment;
import com.hci.data.entity.ReExecuteJob;
import com.hci.job.sub.JFSPaymentDetail1Sub;
import com.hci.job.sub.JFSPaymentDetailSub;
import com.hci.job.sub.JFSPaymentSub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSPayment implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
	
	private DataSource dataSource;
	
	private String targetFol;
	private String jobName;
	private String processName;
	private ParentJobDao parentDao;	

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		parentDao.runProcedure("P_JFS_PAYMENT");
		//jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("P_JFS_PAYMENT");
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		if(checkHoliday()) {
			log.info("JFSPayment", "Job skipped, holiday");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(!checkParentJob(processName)) {
			log.info("JFSPayment", "Job skipped, parent job not run");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(checkTodayRunJob(processName)) {
			log.info("JFSPayment", "Job skipped, already runned");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		log.info("JFSPayment", "Job not skipped");
		
		setForceRunToN(processName);
		
		JFSPaymentSub jfsPaymentSub = new JFSPaymentSub();
		paymentFileStatus = jfsPaymentSub.createFile(targetFol, uuid, "", 0, "", processName);

		JFSPaymentDetailSub jfsPaymentDetailSub = new JFSPaymentDetailSub();
		paymentFileStatus = jfsPaymentDetailSub.createFile(targetFol, uuid, "", 0, "", processName);

		JFSPaymentDetail1Sub jfsPaymentDetail1Sub = new JFSPaymentDetail1Sub();
		paymentFileStatus = jfsPaymentDetail1Sub.createFile(targetFol, uuid, "", 0, "", processName);
		
		if (paymentFileStatus) {
			sendEmail();
		}
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		log.info("JFSPayment", "Job done");
		
		return RepeatStatus.FINISHED;
	}
	
	public RepeatStatus rerun() throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		//jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("P_JFS_PAYMENT");
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		setForceRunToN(processName);
		
		JFSPaymentSub jfsPaymentSub = new JFSPaymentSub();
		paymentFileStatus = jfsPaymentSub.createFile(targetFol, uuid, "", 0, "", processName);

		JFSPaymentDetailSub jfsPaymentDetailSub = new JFSPaymentDetailSub();
		paymentFileStatus = jfsPaymentDetailSub.createFile(targetFol, uuid, "", 0, "", processName);

		JFSPaymentDetail1Sub jfsPaymentDetail1Sub = new JFSPaymentDetail1Sub();
		paymentFileStatus = jfsPaymentDetail1Sub.createFile(targetFol, uuid, "", 0, "", processName);
		
		if (paymentFileStatus) {
			sendEmail();
		}
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		log.info("JFSPayment", "Job done");
		
		return RepeatStatus.FINISHED;
	}
	
	public boolean checkHoliday() {
		return parentDao.checkHoliday();
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void setForceRunToN(String processName) {
		parentDao.setForceRunNChildJob(processName);
	}
	
	public void sendEmail() throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS - HCID Payment";
		StringBuffer content = new StringBuffer();
		content.append("There are new HCID Payment that have to be registered to BTPN\n\n");
		content.append("You can find the file:\n");
		content.append("/WS/Data/JFS/Export/JFS_PAYMENT_" + sdf.format(new Date()) + ".txt.aes");
		content.append("/WS/Data/JFS/Export/JFS_PAYMENT_DETAIL_" + sdf.format(new Date()) + ".txt");
		emailUtil.sendEmail(subject, content);
	}
	
	public boolean checkTodayRunJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
}