package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.data.dao.JfsPaymentDao;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.JfsPayment;
import com.hci.data.entity.ReExecuteJob;
import com.hci.job.sub.JFSPaymentDetail1Sub;
import com.hci.job.sub.JFSPaymentDetailSub;
import com.hci.job.sub.JFSPaymentSub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentFail implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(processName, "targetFol must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}
}