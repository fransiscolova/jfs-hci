package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.JobLauncherJFSPayment;
import com.hci.JobLauncherJFSPaymentRerun;
import com.hci.data.dao.JfsPaymentDao;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.JfsPayment;
import com.hci.data.entity.ReExecuteJob;
import com.hci.job.sub.JFSPaymentDetail1Sub;
import com.hci.job.sub.JFSPaymentDetailSub;
import com.hci.job.sub.JFSPaymentSub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentRerun implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private DataSource dataSource;
	private ParentJobDao reExecuteJobs;
	private String targetFol;
	private String jobName;
	private String processName;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		reExecuteJobs = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		//reExecuteJobs.runProcedure("P_JFS_PAYMENT");
		
		// Check whether the job is run today
		if(!checkTodayJob(processName)) {
			log.info("JFSPaymentRerun", "Today job not yet run");
			// Check whether the parent has been run
			if(checkParentJob(processName)) {
				JobDetail jd = new JobDetail();
				try {
					jd = new JobDetail("JFSPaymentRun", Scheduler.DEFAULT_GROUP, JobLauncherJFSPayment.class);
					jd.getJobDataMap().put("test", "testApp");
					Trigger tgr = TriggerUtils.makeImmediateTrigger(0, 0);
					tgr.setName("FireOnceRun");
					
					Scheduler scheduler = new StdSchedulerFactory().getScheduler();
					scheduler.start();
					scheduler.scheduleJob(jd, tgr);
				} catch (SchedulerException e) {
					e.printStackTrace();
				}
				
				new PropertyUtil().setProperty("batch.job.run.status", "N");
				return RepeatStatus.FINISHED;
			}
			log.info("JFSPaymentRerun", "Parent job of today job not yet run");
		}
		
		log.info("JFSPaymentRerun", "Rerun the job");
		
		List<ReExecuteJob> jobs = getAllReExecuteJob(jobName);
		boolean paymentFileStatus = false;
		String fileDate = "";
		
		if(jobs.size() != 0) {
			for(ReExecuteJob job : jobs) {
				int numOfRerun = job.getNumOfRerun() + 1;
			
				JFSPaymentSub jfsPaymentSub = new JFSPaymentSub();
				paymentFileStatus = jfsPaymentSub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
		
				JFSPaymentDetailSub jfsPaymentDetailSub = new JFSPaymentDetailSub();
				paymentFileStatus = jfsPaymentDetailSub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
				
				JFSPaymentDetail1Sub jfsPaymentDetail1Sub = new JFSPaymentDetail1Sub();
				paymentFileStatus = jfsPaymentDetail1Sub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "N", processName);
				
				fileDate = job.getRunDate();
			}
		}
		
		if (paymentFileStatus) {
			sendEmail(fileDate);
		}
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		log.info("JFSPaymentRerun", "Rerun job finish");
		
		return RepeatStatus.FINISHED;
	}
	
	public List<ReExecuteJob> getAllReExecuteJob(String jobName) {
		List<ReExecuteJob> jobs = reExecuteJobs.getRerunJob(jobName);
		return jobs;
	}
	
	public boolean checkTodayJob(String processName) {
		return reExecuteJobs.checkTodayRunJob(processName, sdf.format(new Date()));
	}
	
	public boolean checkParentJob(String processName) {
		return reExecuteJobs.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void sendEmail(String fileDate) throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS - HCID Payment";
		StringBuffer content = new StringBuffer();
		content.append("There are new HCID Payment that have to be registered to BTPN\n\n");
		content.append("You can find the file:\n");
		content.append("/WS/Data/JFS/Export/JFS_PAYMENT_" + fileDate + ".txt.aes");
		content.append("/WS/Data/JFS/Export/JFS_PAYMENT_DETAIL_" + fileDate + ".txt");
		emailUtil.sendEmail(subject, content);
	}
}