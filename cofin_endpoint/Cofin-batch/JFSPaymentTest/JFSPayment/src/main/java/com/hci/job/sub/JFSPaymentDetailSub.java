package com.hci.job.sub;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hci.data.dao.JfsPaymentDetailDao;
import com.hci.data.entity.JfsPaymentDetail;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentDetailSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsPaymentDetail> getAllData() {
		JfsPaymentDetailDao jfsPaymentDetailDao = (JfsPaymentDetailDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentDetailDao");
		List<JfsPaymentDetail> jfsPaymentDetailss = jfsPaymentDetailDao.getJfsPaymentDetailData();
		return jfsPaymentDetailss;
	}
	
	public List<JfsPaymentDetail> getAllData(String runDate) {
		JfsPaymentDetailDao jfsPaymentDetailDao = (JfsPaymentDetailDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentDetailDao");
		List<JfsPaymentDetail> jfsPaymentDetailss = jfsPaymentDetailDao.getJfsPaymentDetailData(runDate);
		return jfsPaymentDetailss;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		BigDecimal result = BigDecimal.ZERO;
		
		paymentDetailDataString.add(header);
		long footerSum = 0;
		List<JfsPaymentDetail> jps;
		
		if(runDate.equals("")) {
			jps = getAllData();
		} else {
			jps = getAllData(runDate);
		}
		
		log.info("JFSPaymentDetailSub", "Num of file lines : " + jps.size());
		
		for(JfsPaymentDetail jp : jps) {
			if(!partnerAgreement.containsKey(jp.getIdAgreement())) {
				partnerAgreement.put(jp.getIdAgreement(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getBankReferenceNo() + ";" + jp.getTextContractNumber() + ";" + jp.getDatePayment() + ";" + jp.getPmtInstalment());
			result = result.add(new BigDecimal(Integer.valueOf(jp.getPmtInstalment())));
		}
		paymentDetailDataString.add("TOTAL;;" + result.toString());
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSPaymentDetailSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_PAYMENT_DETAIL_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".csv";
		
		Map<String, Object> dataToWrite = createPaymentFileData("REF;TEXT_CONTRACT_NUMBER;TRANSACTION_DATE;AMT_PAYMENT", runDate);
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));

		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, fileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSPaymentDetailSub", "JobStatus : " + jobStatus);
		log.info("JFSPaymentDetailSub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
