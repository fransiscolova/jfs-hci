package com.hci.job.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.hci.data.dao.JfsPaymentDao;
import com.hci.data.entity.JfsPayment;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSPaymentSub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsPayment> getAllData() {
		JfsPaymentDao jfsPaymentDao = (JfsPaymentDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentDao");
		List<JfsPayment> jfsPayments = jfsPaymentDao.getJfsPaymentData();
		return jfsPayments;
	}
	
	public List<JfsPayment> getAllData(String runDate) {
		JfsPaymentDao jfsPaymentDao = (JfsPaymentDao) SpringContextHolder.getApplicationContext().getBean("jfsPaymentDao");
		List<JfsPayment> jfsPayments = jfsPaymentDao.getJfsPaymentData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		BigDecimal result = BigDecimal.ZERO;
		
		List<String> paymentDataString = new ArrayList<String>();
		paymentDataString.add(header);
		long footerSum = 0;
		int numOfData = 0;
		List<JfsPayment> jfsPayments;
		
		if (runDate.equals("")) {
			jfsPayments = getAllData();
		} else {
			jfsPayments = getAllData(runDate);
		}
		
		log.info("JFSPaymentSub", "Num of file lines : " + jfsPayments.size());
		
		for(JfsPayment jp : jfsPayments) {
			if(!partnerAgreement.containsKey(jp.getIdAgreement())) {
				partnerAgreement.put(jp.getIdAgreement(), jp.getPartnerId());
			}
			
			paymentDataString.add(jp.getTextContractNumber() + "|" + jp.getClientName() + 
					"|" + jp.getDatePayment() + "|" + jp.getPmtInstalment() + "|" + 
					jp.getPmtPenalty() + "|0|" + jp.getPmtFee() + "|" + jp.getPaymentType());
			
			result = result.add(new BigDecimal(Integer.valueOf(jp.getPmtInstalment()) + Integer.valueOf(jp.getPmtPenalty()) + Integer.valueOf(jp.getPmtFee())));
			numOfData++;
		}
		paymentDataString.add("HCI|" + numOfData + "|" + result.toString());
		extractedValue.put("fileData", paymentDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		
		return extractedValue;
	}
	
	@SuppressWarnings("unchecked")
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSPaymentSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_PAYMENT_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		String aesFileName = targetFol + "JFS_PAYMENT_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt.aes";
		
		Map<String, Object> dataToWrite = createPaymentFileData("HCI|5|" + dateUtil.dateToString(new Date(), "ddMMyyHHmmss") + "|NO. PKS.087/DIR/TFI/VI/2014", runDate);
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		if (((List<String>) dataToWrite.get("fileData")).size() <= 2) {
			return false;
		}
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		jobStatus = super.aescrypt(shellCObj, fileName, aesFileName);
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, aesFileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, aesFileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSPaymentSub", "JobStatus : " + jobStatus);
		log.info("JFSPaymentSub", "Created file : " + aesFileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		} 
	}
}
