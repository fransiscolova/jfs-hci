package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsSalesroomExport;

public interface JfsSalesroomExportDao {
	public List<JfsSalesroomExport> getJfsSalesroomExportData();
}
