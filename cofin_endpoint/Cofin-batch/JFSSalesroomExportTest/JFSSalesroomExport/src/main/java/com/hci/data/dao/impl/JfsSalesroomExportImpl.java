package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsSalesroomExportDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsSalesroomExport;
import com.hci.data.mapper.JfsSalesroomExportMapper;

public class JfsSalesroomExportImpl implements JfsSalesroomExportDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsSalesroomExportImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsSalesroomExport> getJfsSalesroomExportData() {
		String SQL = "select salesroom_code, salesroom_name, description, description, dst_code, status " +
						"from tmp_jfs_salesroom order by salesroom_code"; 
		List<JfsSalesroomExport> jfsPayment = jdbcTemplateObject.query(SQL, new JfsSalesroomExportMapper());
		return jfsPayment;
	}
}
