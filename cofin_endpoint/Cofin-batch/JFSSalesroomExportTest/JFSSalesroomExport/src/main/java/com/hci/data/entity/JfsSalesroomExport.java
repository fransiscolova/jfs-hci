package com.hci.data.entity;

import java.sql.Date;

public class JfsSalesroomExport {
	private String salesroomCode;
	private String salesroomName;
	private String description;
	private String dstCode;
	private String status;

	public String getSalesroomCode() {
		return salesroomCode;
	}
	public void setSalesroomCode(String salesroomCode) {
		this.salesroomCode = salesroomCode;
	}
	public String getSalesroomName() {
		return salesroomName;
	}
	public void setSalesroomName(String salesroomName) {
		this.salesroomName = salesroomName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDstCode() {
		return dstCode;
	}
	public void setDstCode(String dstCode) {
		this.dstCode = dstCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}