package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsSalesroomExport;

public class JfsSalesroomExportMapper implements RowMapper<JfsSalesroomExport> {

	@Override
	public JfsSalesroomExport mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsSalesroomExport jfsPayment = new JfsSalesroomExport();
		jfsPayment.setSalesroomCode(rs.getString("SALESROOM_CODE"));
		jfsPayment.setSalesroomName(rs.getString("SALESROOM_NAME"));
		jfsPayment.setDescription(rs.getString("DESCRIPTION"));
		jfsPayment.setDstCode(rs.getString("DST_CODE"));
		jfsPayment.setStatus(rs.getString("STATUS"));
		return jfsPayment;
	}
	
}
