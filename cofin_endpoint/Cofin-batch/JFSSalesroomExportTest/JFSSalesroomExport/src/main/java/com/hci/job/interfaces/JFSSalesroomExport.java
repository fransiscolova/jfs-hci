package com.hci.job.interfaces;

import java.util.Date;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.job.sub.JFSSalesroomExport1Sub;
import com.hci.job.sub.JFSSalesroomExportSub;
import com.hci.job.sub.SubJobParent;
import com.hci.util.DateUtil;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSSalesroomExport implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private ParentJobDao parentDao;
	private String targetFol;
	private String jobName;
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("date_export", getLastRunTime());
		parentDao.runProcedure("P_JFS_SALESROOM", param);
		
		boolean paymentFileStatus = false;
		DateUtil dateUtil = new DateUtil();
		String uuid = new OtherUtil().uuidGenerator();
		
		if(checkHoliday()) {
			log.info("JFSSalesroomExport", "Job skipped, holiday");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(!checkParentJob(processName)) {
			log.info("JFSSalesroomExport", "Job skipped, parent job not run");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(checkTodayRunJob(processName)) {
			log.info("JFSSalesroomExport", "Job skipped, already runned");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		log.info("JFSSalesroomExport", "Job not skipped");
		
		setForceRunToN(processName);
		
		ShellConnectionObject shellCObj = new ShellConnectionObject("WS_account", "3Makas!H", "10.56.8.49", "", 22);
		List<String> sshMessage = new ArrayList<String>();
		sshMessage.add("# It is possible to update JFS_SALESROOM_MARK variable to enable to regenerate older SALESROOM.\n\n\nJFS_SALESROOM_MARK=" + dateUtil.dateToString(new java.util.Date(), "yyyy-MM-dd_HH:mm:ss"));
		
		boolean res = new SubJobParent().removeAndWriteToFile(shellCObj, "/WS/convertor/jobs/JFS.Salesroom.Export.mark.sh", sshMessage);
				
		JFSSalesroomExportSub JFSSalesroomExportSub = new JFSSalesroomExportSub();
		JFSSalesroomExportSub.createFile(targetFol, uuid, "", 0, "", processName);
		
		JFSSalesroomExport1Sub JFSSalesroomExportSub1 = new JFSSalesroomExport1Sub();
		paymentFileStatus = JFSSalesroomExportSub1.createFile(targetFol, uuid, "", 0, "", processName);
		
		if (paymentFileStatus) {
			sendEmail();
		}
		
		log.info("JFSSalesroomExport", "Job done");
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}	
	
	public boolean checkHoliday() {
		return parentDao.checkHoliday();
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void setForceRunToN(String processName) {
		parentDao.setForceRunNChildJob(processName);
	}
	
	public void sendEmail() throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS New POS Registration by e-mail";
		StringBuffer content = new StringBuffer();
		content.append("Dear All\n\n\n ");
		content.append("Please proces file JFS_HCI_BRANCH to be processed as New POS Registration. File available in Workarround Server folder:\n");
		content.append("/WS/Data/JFS/Export/\n\n\n\n");
		content.append("Best Regards\nPT Home Credit Indonesia\nGraha Paramita 8/F\nJl.Denpasar Raya Blok D-2\nJakarta 12040");
		emailUtil.sendEmail(subject, content);
	}
	
	public boolean checkTodayRunJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
	
	public String getLastRunTime() {
		String returnMess = "";
		ShellConnectionObject shellObj = new ShellConnectionObject();
		PropertyUtil prop = new PropertyUtil();
		shellObj.setIp(prop.getProperty("linux.ip"));
		shellObj.setPort(Integer.parseInt(prop.getProperty("linux.port")));
		shellObj.setUsername(prop.getProperty("linux.username"));
		shellObj.setPassword(prop.getProperty("linux.password"));
		
		List<String> mess = new SSHUtil().sendCommandStdAlone("less /WS/convertor/jobs/JFS.Salesroom.Export.mark.sh", shellObj);
		for(String mes : mess) {
			if(mes.startsWith("JFS_SALESROOM_MARK")) {
				returnMess = mes.split("=")[1];
			}
		}
		
		return returnMess;
	}
}