package com.hci.job.interfaces;

import java.util.Date;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.mail.EmailException;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.JobLauncherJFSSalesroomExport;
import com.hci.data.dao.ParentJobDao;
import com.hci.data.entity.ReExecuteJob;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.job.sub.JFSSalesroomExport1Sub;
import com.hci.job.sub.JFSSalesroomExportSub;
import com.hci.util.DateUtil;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSSalesroomExportRerun implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private ParentJobDao parentDao;
	private String targetFol;
	private String jobName;
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "targetFol must be set");
	}

	public List<ReExecuteJob> getAllReExecuteJob(String jobName) {
		List<ReExecuteJob> jobs = parentDao.getRerunJob(jobName);
		return jobs;
	}
	
	public boolean checkTodayJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		
		//Map<String, Object> param = new HashMap<String, Object>();
		//param.put("date_export", getLastRunTime());
		//parentDao.runProcedure("P_JFS_SALESROOM", param);
		
		if(!checkTodayJob(processName)) {
			log.info("JFSSalesroomExportRerun", "Today job not yet run");
			if(checkParentJob(processName)) {
				JobDetail jd = new JobDetail();
				try {
					jd = new JobDetail("JFSPaymentRun", Scheduler.DEFAULT_GROUP, JobLauncherJFSSalesroomExport.class);
					jd.getJobDataMap().put("test", "testApp");
					Trigger tgr = TriggerUtils.makeImmediateTrigger(0, 0);
					tgr.setName("FireOnceRun");
					
					Scheduler scheduler = new StdSchedulerFactory().getScheduler();
					scheduler.start();
					scheduler.scheduleJob(jd, tgr);
				} catch (SchedulerException e) {
					e.printStackTrace();
				}
				new PropertyUtil().setProperty("batch.job.run.status", "N");
				return RepeatStatus.FINISHED;
			}
			log.info("JFSSalesroomExportRerun", "Parent job of today job not yet run");
		}
		
		log.info("JFSSalesroomExportRerun", "Rerun the job");
		
		List<ReExecuteJob> jobs = getAllReExecuteJob(jobName);
		boolean paymentFileStatus = false;
		DateUtil dateUtil = new DateUtil();
		String uuid = new OtherUtil().uuidGenerator();
		
		if(jobs.size() != 0) {
			for(ReExecuteJob job : jobs) {
				int numOfRerun = job.getNumOfRerun() + 1;
			
				JFSSalesroomExportSub jfsPaymentSub = new JFSSalesroomExportSub();
				paymentFileStatus = jfsPaymentSub.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "Y", processName);
				
				JFSSalesroomExport1Sub jfsPaymentSub1 = new JFSSalesroomExport1Sub();
				paymentFileStatus = jfsPaymentSub1.createFile(targetFol, job.getRunId(), job.getRunDate(), numOfRerun, "N", processName);
			}
		}
		
		if (paymentFileStatus) {
			sendEmail();
		}
		
		log.info("JFSSalesroomExportRerun", "Rerun job finish");
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		return RepeatStatus.FINISHED;
	}
	
	public String extractSalesExportMark(List<String> strings) {
		for(String string : strings) {
			if(string.startsWith("JFS_SALESROOM_MARK")) {
				return string.split("=")[1];
			}
		}
		return "";
	}
	
	public void sendEmail() throws EmailException {
		EmailUtil emailUtil = new EmailUtil();
		String subject = "JFS New POS Registration by e-mail";
		StringBuffer content = new StringBuffer();
		content.append("Dear All\n\n\n ");
		content.append("Please proces file JFS_HCI_BRANCH to be processed as New POS Registration. File available in Workarround Server folder:\n");
		content.append("/WS/Data/JFS/Export/\n\n\n\n");
		content.append("Best Regards\nPT Home Credit Indonesia\nGraha Paramita 8/F\nJl.Denpasar Raya Blok D-2\nJakarta 12040");
		emailUtil.sendEmail(subject, content);
	}
	
	public String getLastRunTime() {
		String returnMess = "";
		ShellConnectionObject shellObj = new ShellConnectionObject();
		PropertyUtil prop = new PropertyUtil();
		
		shellObj.setIp(prop.getProperty("linux.ip"));
		shellObj.setPort(Integer.parseInt(prop.getProperty("linux.port")));
		shellObj.setUsername(prop.getProperty("linux.username"));
		shellObj.setPassword(prop.getProperty("linux.password"));
		
		List<String> mess = new SSHUtil().sendCommandStdAlone("less /WS/convertor/jobs/JFS.Salesroom.Export.mark.sh", shellObj);
		for(String mes : mess) {
			if(mes.startsWith("JFS_SALESROOM_MARK")) {
				returnMess = mes.split("=")[1];
			}
		}
		
		return returnMess;
	}
}