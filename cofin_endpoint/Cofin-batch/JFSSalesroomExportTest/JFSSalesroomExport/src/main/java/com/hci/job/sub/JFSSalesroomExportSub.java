package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsSalesroomExportDao;
import com.hci.data.entity.JfsSalesroomExport;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSSalesroomExportSub extends SubJobParent {
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsSalesroomExport> getAllData() {
		JfsSalesroomExportDao jfsPaymentDao = (JfsSalesroomExportDao) SpringContextHolder.getApplicationContext().getBean("jfsSalesroomExportDao");
		List<JfsSalesroomExport> jfsPayments = jfsPaymentDao.getJfsSalesroomExportData();
		return jfsPayments;
	}
	
	public List<String> createPaymentFileData(String header) {
		List<String> paymentDataString = new ArrayList<String>();
		
		List<JfsSalesroomExport> salesroomList = getAllData();
		log.info("JFSSalesroomExportSub", "Num of file lines : " + salesroomList.size());
		
		for(JfsSalesroomExport jp : salesroomList) {
			paymentDataString.add(jp.getSalesroomCode() + "|" + jp.getSalesroomName() + "|" + 
					jp.getDescription() + "|" + jp.getDescription() + "|" + jp.getDstCode() + "|" + jp.getStatus());
			
		}
		return paymentDataString;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSSalesroomExportSub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "JFS_HCI_BRANCH_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".txt";
		
		List<String> dataToWrite = createPaymentFileData("BRANCH_CODE|BRANCH_NAME|DATI2CODE|DESCRIPTION'");
		
		if(dataToWrite.size() <= 1) {
			return false;
		}
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, dataToWrite);
		
		if (runDate.equals("")) {
			super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSSalesroomExportSub", "JobStatus : " + jobStatus);
		log.info("JFSSalesroomExportSub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
