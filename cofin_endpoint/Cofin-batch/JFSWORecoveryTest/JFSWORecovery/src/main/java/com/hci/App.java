package com.hci;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.hci.util.SpringContextHolder;

public class App {
	public static void main(String[] args) {
		ApplicationContext context2 = new ClassPathXmlApplicationContext(new String[] {"BatchConfigDataApplicationContext.xml", "spring/batch/jobs/applicationContext_BatchJobHelper.xml", "spring/batch/jobs/applicationContext_JFSWORecovery.xml"});
		//ApplicationContext context2 = new ClassPathXmlApplicationContext(new String[] {"spring/batch/jobs/applicationContext_JFSWORecovery.xml"});
		new SpringContextHolder().setApplicationContext(context2);
		JobDetail jd = new JobDetail();
		try {
			jd = new JobDetail("JFSPayment", Scheduler.DEFAULT_GROUP, JobLauncherJFSWORecovery.class);
//			jd = new JobDetail("JFSPayment", Scheduler.DEFAULT_GROUP, JobLauncherJFSWORecoveryRerun.class);
			jd.getJobDataMap().put("test", "testApp");
			Trigger tgr = TriggerUtils.makeImmediateTrigger(0, 0);
			tgr.setName("FireOnce");
			
			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(jd, tgr);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
}