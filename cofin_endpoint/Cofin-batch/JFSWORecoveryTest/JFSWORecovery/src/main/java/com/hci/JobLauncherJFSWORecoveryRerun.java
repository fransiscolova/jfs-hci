package com.hci;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;

import com.hci.util.SpringContextHolder;

public class JobLauncherJFSWORecoveryRerun implements org.quartz.Job {
	private Job job;
	private JobLocator jobLocator;
	private JobLauncher jobLauncher;
	
	@SuppressWarnings("resource")
	public void execute(JobExecutionContext context) {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		job = (Job) SpringContextHolder.getApplicationContext().getBean("jfsWORecoveryJobRerun");
		jobLocator = (JobLocator) SpringContextHolder.getApplicationContext().getBean("jfsWORecoveryJobRegistry");
		jobLauncher = (JobLauncher) SpringContextHolder.getApplicationContext().getBean("jobLauncherJFSWORecovery");

		try {
			JobParameters jobParameters =
					  new JobParametersBuilder()
					  .addLong("time",System.currentTimeMillis()).toJobParameters();
			jobLauncher.run(job, jobParameters);
		} catch (JobExecutionException e) {
			e.printStackTrace();
		}
	}
}
