package com.hci.data.dao;

import java.util.List;

import com.hci.data.entity.JfsWORecovery;

public interface JfsWORecoveryDao {
	public List<JfsWORecovery> getJfsWORecoveryData();
	public List<JfsWORecovery> getJfsWORecoveryData(String runDate);
}
