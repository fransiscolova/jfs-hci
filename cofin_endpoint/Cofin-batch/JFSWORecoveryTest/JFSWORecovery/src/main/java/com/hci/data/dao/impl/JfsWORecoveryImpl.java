package com.hci.data.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hci.data.dao.JfsWORecoveryDao;
import com.hci.data.database.DatabaseConnection;
import com.hci.data.entity.JfsWORecovery;
import com.hci.data.mapper.JfsWORecoveryMapper;

public class JfsWORecoveryImpl implements JfsWORecoveryDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public JfsWORecoveryImpl() {
		dataSource = new DatabaseConnection().getContextDatasource();
		jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<JfsWORecovery> getJfsWORecoveryData() {
		String SQL = "select regexp_replace(jc.bank_reference_no,'[[:space:]]','') bank_reference_no, " +  
				"jc.text_contract_number, jc.client_name, (select (select sum(js.principal) from JFS_SCHEDULE js " + 
				"where js.text_contract_number = jc.text_contract_number) - (select sum(jpa.amt_principal) " + 
				"from JFS_PAYMENT_ALLOCATION jpa where jpa.text_contract_number = jc.text_contract_number " +  
				") as nilai_hapus_buku from dual) principal_amt_principal, wop.btpn_amt_payment, ja.code agreement_id, " + 
				"jp.id partner_id from jfs_contract jc " + 
				"join JFS_WO_PAYMENT wop on wop.text_contract_number = jc.text_contract_number " +  
				"left join jfs_agreement ja on jc.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " +
				"where trunc(wop.date_export) = trunc(sysdate)"; 
		List<JfsWORecovery> jfsPayment = jdbcTemplateObject.query(SQL, new JfsWORecoveryMapper());
		return jfsPayment;
	}
	
	@Override
	public List<JfsWORecovery> getJfsWORecoveryData(String runDate) {
		String SQL = "select regexp_replace(jc.bank_reference_no,'[[:space:]]','') bank_reference_no, " +  
				"jc.text_contract_number, jc.client_name, (select (select sum(js.principal) from JFS_SCHEDULE js " + 
				"where js.text_contract_number = jc.text_contract_number) - (select sum(jpa.amt_principal) " + 
				"from JFS_PAYMENT_ALLOCATION jpa where jpa.text_contract_number = jc.text_contract_number " +  
				") as nilai_hapus_buku from dual) principal_amt_principal, wop.btpn_amt_payment, ja.code agreement_id, " + 
				"jp.id partner_id from jfs_contract jc " + 
				"join JFS_WO_PAYMENT wop on wop.text_contract_number = jc.text_contract_number " +  
				"left join jfs_agreement ja on jc.id_agreement = ja.code " +
				"left join jfs_partner jp on jp.id = ja.partner_id " +
				"where trunc(wop.date_export) = trunc(to_date('" + runDate + "', 'yyyy-mm-dd'))";	 
		List<JfsWORecovery> jfsPayment = jdbcTemplateObject.query(SQL, new JfsWORecoveryMapper());
		return jfsPayment;
	}
}
