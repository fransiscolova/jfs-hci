package com.hci.data.entity;

import java.sql.Date;

public class JfsWORecovery {
	private String bankReferenceNo;
	private String textContractNumer;
	private String clientName;
	private String principalAmtPrincipal;
	private String btpnAmtPayment;
	private String agreementId;
	private String partnerId;
	
	public String getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getBankReferenceNo() {
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo) {
		this.bankReferenceNo = bankReferenceNo;
	}
	public String getTextContractNumer() {
		return textContractNumer;
	}
	public void setTextContractNumer(String textContractNumer) {
		this.textContractNumer = textContractNumer;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getPrincipalAmtPrincipal() {
		return principalAmtPrincipal;
	}
	public void setPrincipalAmtPrincipal(String principalAmtPrincipal) {
		this.principalAmtPrincipal = principalAmtPrincipal;
	}
	public String getBtpnAmtPayment() {
		return btpnAmtPayment;
	}
	public void setBtpnAmtPayment(String btpnAmtPayment) {
		this.btpnAmtPayment = btpnAmtPayment;
	}
}