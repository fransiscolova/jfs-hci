package com.hci.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hci.data.entity.JfsWORecovery;

public class JfsWORecoveryMapper implements RowMapper<JfsWORecovery> {

	@Override
	public JfsWORecovery mapRow(ResultSet rs, int rowNum) throws SQLException {
		JfsWORecovery jfsPayment = new JfsWORecovery();
		jfsPayment.setBankReferenceNo(rs.getString("BANK_REFERENCE_NO"));
		jfsPayment.setTextContractNumer(rs.getString("TEXT_CONTRACT_NUMBER"));
		jfsPayment.setClientName(rs.getString("CLIENT_NAME"));
		jfsPayment.setPrincipalAmtPrincipal(rs.getString("PRINCIPAL_AMT_PRINCIPAL"));
		jfsPayment.setBtpnAmtPayment(rs.getString("BTPN_AMT_PAYMENT"));
		jfsPayment.setAgreementId(rs.getString("AGREEMENT_ID"));
		jfsPayment.setPartnerId(rs.getString("PARTNER_ID"));
		return jfsPayment;
	}
	
}
