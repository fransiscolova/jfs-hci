package com.hci.job.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.DataSource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.Assert;

import com.hci.data.dao.ParentJobDao;
import com.hci.job.sub.JFSWORecoverySub;
import com.hci.util.EmailUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.OtherUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SpringContextHolder;

public class JFSWORecovery implements Tasklet, InitializingBean {
	LoggerUtil log = new LoggerUtil();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private ParentJobDao parentDao;
	private String targetFol;
	private String jobName;
	private String processName;

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public void setTargetFol(String targetFol) {
		this.targetFol = targetFol;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(targetFol, "targetFol must be set");
		Assert.notNull(processName, "processName must be set");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		parentDao = (ParentJobDao) SpringContextHolder.getApplicationContext().getBean("parentJobDao");
		parentDao.runProcedure("P_JFS_WO_PAYMENT");
		boolean paymentFileStatus = false;
		String uuid = new OtherUtil().uuidGenerator();
		
		if(checkHoliday()) {
			log.info("JFSWORecovery", "Job skipped, holiday");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		/*if(!checkParentJob(processName)) {
			log.info("JFSWORecovery", "Job skipped, parent job not run");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}
		
		if(checkTodayRunJob(processName)) {
			log.info("JFSPaymentReversal", "Job skipped, already runned");
			new PropertyUtil().setProperty("batch.job.run.status", "N");
			return RepeatStatus.FINISHED;
		}*/
		
		log.info("JFSWORecovery", "Job not skipped");
		
		
		setForceRunToN(processName);
		
		JFSWORecoverySub JFSWORecoverySub = new JFSWORecoverySub();
		paymentFileStatus = JFSWORecoverySub.createFile(targetFol, uuid, "", 0, "", processName);
		
		log.info("JFSWORecovery", "Job done");
		
		new PropertyUtil().setProperty("batch.job.run.status", "N");
		
		return RepeatStatus.FINISHED;
	}
	
	public boolean checkHoliday() {
		return parentDao.checkHoliday();
	}
	
	public boolean checkParentJob(String processName) {
		return parentDao.getParentJobStatus(processName, sdf.format(new Date()));
	}
	
	public void setForceRunToN(String processName) {
		parentDao.setForceRunNChildJob(processName);
	}
	
	public boolean checkTodayRunJob(String processName) {
		return parentDao.checkTodayRunJob(processName, sdf.format(new Date()));
	}
}