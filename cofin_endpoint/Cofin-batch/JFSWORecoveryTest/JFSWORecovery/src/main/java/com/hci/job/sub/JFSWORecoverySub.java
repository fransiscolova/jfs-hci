package com.hci.job.sub;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.hci.data.dao.JfsWORecoveryDao;
import com.hci.data.entity.JfsWORecovery;
import com.hci.data.entity.ShellConnectionObject;
import com.hci.util.DateUtil;
import com.hci.util.FileUtil;
import com.hci.util.LoggerUtil;
import com.hci.util.PropertyUtil;
import com.hci.util.SSHUtil;
import com.hci.util.SpringContextHolder;

public class JFSWORecoverySub extends SubJobParent {
	
	LoggerUtil log = new LoggerUtil();
	
	public List<JfsWORecovery> getAllData() {
		JfsWORecoveryDao jfsPaymentDao = (JfsWORecoveryDao) SpringContextHolder.getApplicationContext().getBean("jfsWORecoveryDao");
		List<JfsWORecovery> jfsPayments = jfsPaymentDao.getJfsWORecoveryData();
		return jfsPayments;
	}
	
	public List<JfsWORecovery> getAllData(String runDate) {
		JfsWORecoveryDao jfsPaymentDao = (JfsWORecoveryDao) SpringContextHolder.getApplicationContext().getBean("jfsWORecoveryDao");
		List<JfsWORecovery> jfsPayments = jfsPaymentDao.getJfsWORecoveryData(runDate);
		return jfsPayments;
	}
	
	public Map<String, Object> createPaymentFileData(String header, String runDate) {
		Map<String, Object> extractedValue = new HashMap<String, Object>();
		Map<String, String> partnerAgreement = new HashMap<String, String>();
		List<String> paymentDetailDataString = new ArrayList<String>();
		
		paymentDetailDataString.add(header);
		
		List<JfsWORecovery> recoverLists;
		if (runDate.equals("")) {
			recoverLists = getAllData();
		} else {
			recoverLists = getAllData(runDate);
		}
		
		log.info("JFSWORecoverySub", "Num of file lines : " + recoverLists.size());
		
		for(JfsWORecovery jp : recoverLists) {
			if(!partnerAgreement.containsKey(jp.getAgreementId())) {
				partnerAgreement.put(jp.getAgreementId(), jp.getPartnerId());
			}
			
			paymentDetailDataString.add(jp.getBankReferenceNo() + ";" + jp.getTextContractNumer() + ";" + jp.getClientName() + ";" + jp.getPrincipalAmtPrincipal() + ";" + jp.getBtpnAmtPayment());
		}
		
		extractedValue.put("fileData", paymentDetailDataString);
		extractedValue.put("agreementPartner", partnerAgreement);
		return extractedValue;
	}
	
	public boolean createFile(String targetFol, String uuid, String runDate, int numOfRerun, String rerunStatus, String processName) {
		log.info("JFSWORecoverySub", "Run with targetFol: " + targetFol + " uuid : " + uuid + " nnumOfRerun : " + numOfRerun + " rerunStatus : " + rerunStatus + " processName : " + processName);
		
		DateUtil dateUtil = new DateUtil();
		Date startJobTime = new Date();
		
		String fileName = targetFol + "HCI_WRITEOFF_RECOVERY_" + dateUtil.dateToString(new Date(), "yyyy-MM-dd") + ".csv";
		
		Map<String, Object> dataToWrite = createPaymentFileData("Nomor Reference;Nomor Kontract;Nama Klien;Nilai Hapus Buku;Nilai Recovery", runDate);
		
		ShellConnectionObject shellCObj = new PropertyUtil().getShellConnectionObject();
		String jobStatus = "Success";
		
		jobStatus = new SSHUtil().removeAndWriteToFile(shellCObj, fileName, (List<String>) dataToWrite.get("fileData"));
		
		if (runDate.equals("")) {
			if(((Map<String, String>) dataToWrite.get("agreementPartner")).isEmpty()) {
				super.saveXNAToDbLog(uuid, startJobTime, processName, jobStatus, fileName);
			} else {
				super.saveLogToDb(uuid, dataToWrite, startJobTime, processName, jobStatus, fileName);
			}
		} else {
			super.updateDBLog(uuid, jobStatus, numOfRerun, rerunStatus);
		}
		
		log.info("JFSWORecoverySub", "JobStatus : " + jobStatus);
		log.info("JFSWORecoverySub", "Created file : " + fileName);
		
		if(jobStatus.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}
}
