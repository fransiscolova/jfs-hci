package com.hci.jfs;

import java.util.Map;

import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.SerializableEventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Messagebox;

public abstract class CommonComposer extends GenericForwardComposer<Component> {

	private static final long serialVersionUID = -3587039907014480894L;

	protected void navigateTo(String url, Map<String, Object> arg,
			Component from) {
		Component parent = from.getParent();
		Executions.createComponents(url, parent, arg);
		from.detach();
	}

	protected void backTo(Component to, Component from) {
		to.setParent(from.getParent());
		from.detach();
	}

	protected void showConfirmDialog(String message,
			final Map<String, Object> args) {	

		Messagebox.show(message, "Confirm",
				Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
				Messagebox.NO, new SerializableEventListener<Event>() {

					private static final long serialVersionUID = 3906507102085950345L;

					@Override
					public void onEvent(Event event) throws Exception {
						int data = (Integer) event.getData();
						switch (data) {
						case Messagebox.YES:
							confirmDialogDoYes(args);
							break;
						case Messagebox.NO:
							confirmDialogDoNo(args);
							break;
						}
					}
				});
	}

	protected void confirmDialogDoYes(Map<String, Object> args) {
	}

	protected void confirmDialogDoNo(Map<String, Object> args) {
	}

	protected void showInformationDialog(String message) {
//		Messagebox.setTemplate("/WEB-INF/zul/html/messagebox.zul");
		Messagebox.show(message, CommonFns.getLabel("msg.common.information"),
				Messagebox.OK, Messagebox.INFORMATION);
	}

	protected void showErrorDialog(String message) {
//		Messagebox.setTemplate("/WEB-INF/zul/html/messagebox.zul");

		Messagebox.show(message, CommonFns.getLabel("msg.common.information"),
				Messagebox.OK, Messagebox.ERROR);
	}
}
