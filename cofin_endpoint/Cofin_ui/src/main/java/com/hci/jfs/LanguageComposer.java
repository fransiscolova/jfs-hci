package com.hci.jfs;

import java.io.IOException;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;

public class LanguageComposer extends CommonComposer {

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(LanguageComposer.class.getName());
	
	private Radiogroup rg;
	private Radio ra1;
	private Radio ra2;
	
	@Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
        Session sess = Sessions.getCurrent();
		String data = (String) sess.getAttribute("radio");
        
        if (data != null) {
        	if (data.equals("en")) {
        		ra1.setSelected(true);
        	} else if (data.equals("in")) {
        		ra2.setSelected(true);
        	} else {
        		ra1.setSelected(true);
        	}
        	
        	sess.removeAttribute("radio");
        
        } else {
        	String language = "en";
        	
        	Locale locale = org.zkoss.util.Locales.getLocale(language);
			org.zkoss.lang.Library.setProperty("org.zkoss.web.preferred.locale",language);
			org.zkoss.zk.ui.util.Clients.reloadMessages(locale);
			org.zkoss.util.Locales.setThreadLocal(locale);
			Locale.setDefault(locale);
        }
    }
	
	public void onCheck$rg(Event event) {
		try {
			String language = rg.getSelectedItem().getLabel();
			System.out.println(language);
			
			Locale locale = org.zkoss.util.Locales.getLocale(language);
			org.zkoss.lang.Library.setProperty("org.zkoss.web.preferred.locale",language);
			org.zkoss.zk.ui.util.Clients.reloadMessages(locale);
			org.zkoss.util.Locales.setThreadLocal(locale);
			Locale.setDefault(locale);
			
			String data = "en";
			
			if (ra1.isSelected()) {
				data = "en";
			} else {
				data = "in";
			}
			
			Session sess = Sessions.getCurrent();
			sess.setAttribute("radio", data);
			
			Executions.sendRedirect("");
			
		} catch (IOException e) {	
			e.printStackTrace();
		}
	}
}
