package com.hci.jfs;

/**
 *
 * @author Muhammad.Agaputra
 */
import java.util.Iterator;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SerializableEventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

public class SidebarComposer extends SelectorComposer<Component> {

    private static final long serialVersionUID = 1L;
    
    @Wire
    Grid fnList;

    //wire service
    SidebarPageConfig pageConfig = new SidebarPageConfigImpl();

    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        //to initial view after view constructed.
        Rows rows = fnList.getRows();

        for (SidebarPage page : pageConfig.getPages()) {
            Row row = constructSidebarRow(page.getName(), page.getLabel(), page.getUri());
            rows.appendChild(row);
        }
    }

    private Row constructSidebarRow(final String name, String label, final String locationUri) {

        //construct component and hierarchy
        Row row = new Row();
        final Label lab = new Label(label);

        row.appendChild(lab);

        //set style attribute
        row.setSclass("sidebar-fn");

        //new and register listener for events
        EventListener<Event> onActionListener = new SerializableEventListener<Event>() {
            private static final long serialVersionUID = 1L;

            public void onEvent(Event event) throws Exception {
                //redirect current url to new location
                if (locationUri.startsWith("http")) {
                    //open a new browser tab
                    Executions.getCurrent().sendRedirect(locationUri);
                } else {
                    //use iterable to find the first include only
                    Include include = (Include) Selectors.iterable(fnList.getPage(), "#mainInclude")
                            .iterator().next();
                    include.setSrc(locationUri);
                    removeStyle();
                    lab.setStyle("font-weight: bold");
                    //advance bookmark control, 
                    //bookmark with a prefix
//                    if (name != null) {                    	
//                    	getPage().getDesktop().setBookmark("p_" + name);
//                    }
                }
            }
        };
        row.addEventListener(Events.ON_CLICK, onActionListener);

        return row;
    }
    
    public void removeStyle(){
    	 
    	 for (Iterator <Component> i = fnList.getRows().getChildren().iterator(); i.hasNext(); ) {
             final Row row = (Row) i.next();
             List <Label> rowLabel = row.getChildren();
             Label label = (Label) rowLabel.get(0);
             
             label.setStyle("font-weight: normal");
    	 }
    }
}
