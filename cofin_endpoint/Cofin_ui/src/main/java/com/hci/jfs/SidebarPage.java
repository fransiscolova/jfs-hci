package com.hci.jfs;

/**
 *
 * @author Muhammad.Agaputra
 */

import java.io.Serializable;

public class SidebarPage implements Serializable{
	private static final long serialVersionUID = 1L;
	String name;
	String label;
	String uri;

	public SidebarPage(String name, String label, String uri) {
		this.name = name;
		this.label = label;
		this.uri = uri;
	}

	public String getName() {
		return name;
	}

	public String getLabel() {
		return label;
	}

	public String getUri() {
		return uri;
	}
}