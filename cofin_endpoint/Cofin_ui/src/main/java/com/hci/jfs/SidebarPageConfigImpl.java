
package com.hci.jfs;

/**
 *
 * @author Muhammad.Agaputra
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.hci.jfs.constant.ROLE;
import com.hci.jfs.security.SecurityUtil;

public class SidebarPageConfigImpl implements SidebarPageConfig {

    HashMap<String, SidebarPage> pageMap = new LinkedHashMap<String, SidebarPage>();

    public SidebarPageConfigImpl() {

    	if(SecurityUtil.isAnyGranted(ROLE.ROLE_ADMIN.name())) {
    		pageMap.put("user_management", new SidebarPage("user_management", "User Management", "/zul/jfs/user_management/user_management.zul"));
    		pageMap.put("my_batch", new SidebarPage("my_batch", "My Batch List", "/zul/jfs/my_batch/my_batch_list.zul"));
        }
    	
    	if(SecurityUtil.isAnyGranted(ROLE.ROLE_JFS.name())) {
    		pageMap.put("partner", new SidebarPage("partner", "Partner", "/zul/jfs/partner/PartnerRegistration.zul"));
    		pageMap.put("batch_process", new SidebarPage("batch_process", "Batch Process", "/zul/jfs/batch/BatchProcess.zul"));
    		pageMap.put("agreement_and_criteria", new SidebarPage("agreement_and_criteria", "Agreement & Criteria", "/zul/jfs/agreement_and_criteria/agreement_and_criteria_list.zul"));
       }
    	
    	if(SecurityUtil.isAnyGranted(ROLE.ROLE_USER.name())) {
    		pageMap.put("contact", new SidebarPage("contact", "Contact", ""));
        	pageMap.put("file_exchange", new SidebarPage("file_exchange", "File Exchange", ""));
        	pageMap.put("file_generation_upload", new SidebarPage("file_generation_and_upload", "File Generation & Upload", "/zul/jfs/file_exchange/file_generation_upload_generate.zul"));
        	pageMap.put("monitoring_error_contract", new SidebarPage("monitoring_error_contract", "Monitoring Error Contract", "/zul/jfs/monitoring/monitoring_add_contract_error.zul"));
         	pageMap.put("monitoring_error_contract_type", new SidebarPage("monitoring_error_contract_type", "Monitoring Error Contract Type", "/zul/jfs/monitoring/monitoring_add_contract_type_error.zul"));
            
        }
    }

    public List<SidebarPage> getPages() {
        return new ArrayList<SidebarPage>(pageMap.values());
        
    }

    public SidebarPage getPage(String name) {
        return pageMap.get(name);
    }
}
