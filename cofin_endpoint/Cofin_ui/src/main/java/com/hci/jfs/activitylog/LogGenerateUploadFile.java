package com.hci.jfs.activitylog;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ACTIVITY_LOG_JFS_FILE")
public class LogGenerateUploadFile implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	private String id;
	
	@Column(name = "PARTNER_CODE")
	private String partnerCode;
	
	@Column(name = "AGREEMENT_CODE")
	private String agreementCode;
	
	@Column(name = "PROCESS_TYPE_CODE")
	private String processTypeCode;
	
	@Column(name = "FILENAME")
	private String filename;
	
	@Column(name = "DATE_UPLOAD")
	private Date dateUpload;
	
	@Column(name = "FILETYPE")
	private String filetype;
	
	public String getFiletype() {
		return filetype;
	}
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getAgreementCode() {
		return agreementCode;
	}
	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}
	public String getProcessTypeCode() {
		return processTypeCode;
	}
	public void setProcessTypeCode(String processTypeCode) {
		this.processTypeCode = processTypeCode;
	}
	public Date getDateUpload() {
		return dateUpload;
	}
	public void setDateUpload(Date dateUpload) {
		this.dateUpload = dateUpload;
	}
	@Override
	public String toString() {
		return "LogGenerateUploadFile [id=" + id + ", partnerCode="
				+ partnerCode + ", agreementCode=" + agreementCode
				+ ", processTypeCode=" + processTypeCode + ", filename="
				+ filename + ", dateUpload=" + dateUpload + ", filetype="
				+ filetype + "]";
	}
	
}
