package com.hci.jfs.batch;

import java.lang.reflect.Method;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.hci.jfs.dao.impl.MyBatchLogDAOImpl;
import com.hci.jfs.entity.MyBatchLog;
import com.hci.jfs.util.SpringContextHolder;

public class MyBatchJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			MyBatchLogDAOImpl batchLogDAO = (MyBatchLogDAOImpl) SpringContextHolder.getApplicationContext().getBean("MyBatchLogDAO");
			
			JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();
			
			MyBatchLog log = new MyBatchLog();
			log.setJobId((String) dataMap.get("jobId"));
			log.setExecuteBy("SYSTEM");
			log.setExecuteDate(new Date());
			log.setIsError("P");
			
			batchLogDAO.save(log);

			try {
				Object o = SpringContextHolder.getApplicationContext().getBean((String) dataMap.get("service"));
				Method m = o.getClass().getDeclaredMethod((String) dataMap.get("method"), new Class[] {});
				m.invoke(o);
				
				log.setIsError("N");
				batchLogDAO.update(log);
				
			} catch (Exception e) {
				e.printStackTrace();
				
				log.setIsError("Y");
				log.setErrorLog(e.getMessage());
				batchLogDAO.update(log);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
