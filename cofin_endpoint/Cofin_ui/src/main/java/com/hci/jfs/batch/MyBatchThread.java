package com.hci.jfs.batch;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.hci.jfs.dao.impl.MyBatchDAOImpl;
import com.hci.jfs.entity.MyBatch;
import com.hci.jfs.util.SpringContextHolder;

public class MyBatchThread implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			System.out.println("masuk Batch Thread");

			MyBatchDAOImpl batchDAO = (MyBatchDAOImpl) SpringContextHolder.getApplicationContext().getBean("MyBatchDAO");

			List<MyBatch> batchList = batchDAO.getAllMyBatch();

			for (int i = 0; i < batchList.size(); i++) {
				
				MyBatch batch = batchList.get(i);
				
				JobDetail job = new JobDetail();
				job.setName(batch.getJobName());
				job.setJobClass(MyBatchJob.class);
				
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("jobId", batch.getJobId());
				map.put("service", batch.getService());
				map.put("method", batch.getMethod());
				
				JobDataMap jobDataMap = new JobDataMap(map);
				job.setJobDataMap(jobDataMap);

				CronTrigger trigger = new CronTrigger();
				trigger.setName(batch.getJobName().concat(" ").concat("Trigger"));
				trigger.setCronExpression(batch.getCronTime());
				
				Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		    	scheduler.start();
		    	scheduler.scheduleJob(job, trigger);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
