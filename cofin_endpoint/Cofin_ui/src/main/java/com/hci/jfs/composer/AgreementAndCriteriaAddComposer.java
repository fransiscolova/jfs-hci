package com.hci.jfs.composer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.dao.PartnerDAO;
import com.hci.jfs.dao.impl.AgreementDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.entity.Product;
import com.hci.jfs.services.ProductService;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;

/**
 *
 * @author Igor
 */
public class AgreementAndCriteriaAddComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(AgreementAndCriteriaAddComposer.class.getName());

	private AgreementDAOImpl agreementDAO;
	private PartnerDAO partnerDAO;
	private String screenState;
	private Agreement agreement;
	private ProductService productService;

	Window winAgreementAndCriteriaList;
	Window winAgreementAndCriteriaAdd;
	Label labelSuccessMessage;
	Label labelErrorMessage;

	Listbox listboxPartner;
	Textbox textboxAgreementCode;
	Textbox textboxAgreementName;
	Decimalbox decimalboxInterestRate;
	Decimalbox decimalboxPrincipalSplitRate;
	Decimalbox decimalboxAdminFeeRate;
	Datebox dateboxValidFrom;
	Datebox dateboxValidTo;
	Textbox textboxDescription;

	Label messagePartner;
	Label messageAgreementCode;
	Label messageAgreementName;
	Label messageInterestRate;
	Label messagePrincipalSplitRate;
	Label messageAdminFeeRate;
	Label messageValidFrom;
	Label messageValidTo;

	Label labelSubTitle;
	Button buttonNext;

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		agreementDAO = (AgreementDAOImpl) SpringContextHolder.getApplicationContext().getBean("AgreementDAO");
		partnerDAO = (PartnerDAO) SpringContextHolder.getApplicationContext().getBean("PartnerDAO");
		productService = (ProductService) SpringContextHolder.getApplicationContext().getBean("productService");
		
		List<Partner> partnerList = partnerDAO.getAllPartner();
		
		for (int i = 0; i < partnerList.size(); i++) {
			Partner partner = partnerList.get(i);
			
			Listitem li = new Listitem();
			li.setLabel(partner.getName());
			li.setValue(partner.getId());
			li.setParent(listboxPartner);
		}

		screenState = (String) Executions.getCurrent().getArg().get("screenState");

		if (screenState.equals("edit")) {
			agreement = (Agreement) Executions.getCurrent().getArg().get("agreement");
			Product product = productService.getProductByAgreement(Long.parseLong(agreement.getCode())).get(0);
			labelSubTitle.setValue(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.updateagreementandcriteria"));
			
			for (int i = 0; i < partnerList.size(); i++) {
				Partner partner = partnerList.get(i);
				
				if (agreement.getPartner().getId().equals(partner.getId())) {
					listboxPartner.setSelectedIndex(i);
				}
			}
			
			textboxAgreementCode.setValue(agreement.getCode());
			textboxAgreementCode.setDisabled(true);
			textboxAgreementName.setValue(agreement.getName());
			decimalboxInterestRate.setValue(product.getInterestRate() == null ? BigDecimal.ZERO : product.getInterestRate());
			decimalboxPrincipalSplitRate.setValue(product.getPrincipalSplitRate() == null ? BigDecimal.ZERO : product.getPrincipalSplitRate());
			decimalboxAdminFeeRate.setValue(product.getAdminFeeRate() == null ? BigDecimal.ZERO : product.getAdminFeeRate());
			dateboxValidFrom.setValue(agreement.getValidFrom());
			dateboxValidTo.setValue(agreement.getValidTo());
			textboxDescription.setValue(agreement.getDescription());

			buttonNext.setVisible(false);
		}

		clearMessage();
	}

	public void onClick$buttonSave(Event e) throws InterruptedException {
		clearMessage();

		if (checkValidation()) {
			if (screenState.equals("add")) {
				Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationadd"), MessageResources.getMessageResources("zul.common.label.confirmationadd"), Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {
					@Override
					public void onEvent(Event e) throws Exception {
						if (Messagebox.ON_OK.equals(e.getName())) {
							save();
						} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
						} 
					}
				});
			} else {
				Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationedit"), MessageResources.getMessageResources("zul.common.label.confirmationedit"), Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {
					@Override
					public void onEvent(Event e) throws Exception {
						if (Messagebox.ON_OK.equals(e.getName())) {
							update();
						} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
						} 
					}
				});
			}
		}
	}

	public void onClick$buttonNext(Event e) throws InterruptedException {
		clearMessage();
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("partnerName", listboxPartner.getSelectedItem().getLabel());
		args.put("agreementCode", textboxAgreementCode.getValue());
		args.put("agreementName", textboxAgreementName.getValue());
		navigateTo("/zul/jfs/agreement_and_criteria/product_mapping_add.zul", args, winAgreementAndCriteriaAdd);
	}

	public void onClick$buttonCancel(Event e) throws InterruptedException {
		clearMessage();
//		backTo(winAgreementAndCriteriaList, winAgreementAndCriteriaAdd);
		Map<String, Object> args = new HashMap<String, Object>();
		navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_list.zul", args, winAgreementAndCriteriaAdd);
	}

	public void clearMessage() {
		labelSuccessMessage.setValue("");
		labelErrorMessage.setValue("");
		messagePartner.setValue("");
		messageAgreementCode.setValue("");
		messageAgreementName.setValue("");
		messageInterestRate.setValue("");
		messagePrincipalSplitRate.setValue("");
		messageAdminFeeRate.setValue("");
		messageValidFrom.setValue("");
		messageValidTo.setValue("");
	}

	public boolean checkValidation() {
		boolean result = true;

		if (listboxPartner.getSelectedItem() == null) {
			result = false;
			messagePartner.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (textboxAgreementCode.getValue().equals("")) {
			result = false;
			messageAgreementCode.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (textboxAgreementName.getValue().equals("")) {
			result = false;
			messageAgreementName.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (decimalboxInterestRate.getValue() == null) {
			result = false;
			messageInterestRate.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (decimalboxPrincipalSplitRate.getValue() == null) {
			result = false;
			messagePrincipalSplitRate.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (decimalboxAdminFeeRate.getValue() == null) {
			result = false;
			messageAdminFeeRate.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (dateboxValidFrom.getValue() == null) {
			result = false;
			messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			if (dateboxValidFrom.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantoday"));
			}
		}

		if (dateboxValidTo.getValue() == null) {
			result = false;
			messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			if (dateboxValidTo.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.todatebiggerthantoday"));
			}
		}

		if (dateboxValidFrom.getValue() != null && dateboxValidTo.getValue() != null) {
			if (dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) == 0 || dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) > 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantodate"));
			}
		}

		return result;
	}

	public void save() {
		try {
			Agreement agreement = agreementDAO.getAgreementByCode(textboxAgreementCode.getValue());

			if (agreement == null) {

				agreement = new Agreement();
				//agreement.setId(String.valueOf(Utility.generateRandomID()));
				Partner partner = new Partner();
				partner.setId(listboxPartner.getSelectedItem().getValue());
				agreement.setPartner(partner);
				agreement.setCode(textboxAgreementCode.getValue());
				agreement.setName(textboxAgreementName.getValue());
				agreement.setInterestRate(decimalboxInterestRate.getValue());
				agreement.setPrincipalSplitRate(decimalboxPrincipalSplitRate.getValue());
				agreement.setAdminFeeRate(decimalboxAdminFeeRate.getValue());
				agreement.setValidFrom(new Timestamp(dateboxValidFrom.getValue().getTime()));
				agreement.setValidTo(new Timestamp(dateboxValidTo.getValue().getTime()));
				agreement.setDescription(textboxDescription.getValue());

				agreementDAO.save(agreement);

				labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.savesuccess"));

			} else {
				labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.alreadyexist"));
			}
		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}

	public void update() {
		try {
			Partner partner = new Partner();
			partner.setId(listboxPartner.getSelectedItem().getValue());
			agreement.setPartner(partner);
			agreement.setPartner(partner);
			agreement.setName(textboxAgreementName.getValue());
			agreement.setInterestRate(decimalboxInterestRate.getValue());
			agreement.setPrincipalSplitRate(decimalboxPrincipalSplitRate.getValue());
			agreement.setAdminFeeRate(decimalboxAdminFeeRate.getValue());
			agreement.setValidFrom(new Timestamp(dateboxValidFrom.getValue().getTime()));
			agreement.setValidTo(new Timestamp(dateboxValidTo.getValue().getTime()));
			agreement.setDescription(textboxDescription.getValue());

			agreementDAO.update(agreement);

			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.updatesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.updateerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}