package com.hci.jfs.composer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.AgreementDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.Product;
import com.hci.jfs.services.ProductService;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;


/**
 *
 * @author Igor
 */
public class AgreementAndCriteriaDetailComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(AgreementAndCriteriaDetailComposer.class.getName());
	
	private ProductService productService = (ProductService) SpringContextHolder.getApplicationContext().getBean("productService");
	private AgreementDAOImpl AgreementDAO = (AgreementDAOImpl) SpringContextHolder.getApplicationContext().getBean("AgreementDAO");

	@Wire
	Window winAgreementAndCriteriaDetail;
	
	@Wire
	Button buttonDelete;

	private Agreement agreementDetail = (Agreement) Executions.getCurrent().getArg().get("agreement");

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
	}

	private void initComponent() throws Exception{
		if(agreementDetail.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO)){
			buttonDelete.setLabel(MessageResources.getMessageResources("zul.common.label.deactive"));
		}else{
			buttonDelete.setLabel(MessageResources.getMessageResources("zul.common.label.reactive"));
		}

		List<Product> listProducts = productService.getProductByAgreement(Long.parseLong(agreementDetail.getCode()));
		boolean status=true;
		if(listProducts==null){
			status=false;
		}

		if(listProducts.size()==0){
			status=false;
		}
		if(status){
			Product product = listProducts.get(0);
			setupViewDisplay(product);
			Include includeProductMapping = (Include) winAgreementAndCriteriaDetail.getFellow("includeProductMapping");
			includeProductMapping.setSrc("/zul/jfs/agreement_and_criteria/product_mapping_list.zul?agreementCode="+agreementDetail.getCode());

			Include includePublicHolidays = (Include) winAgreementAndCriteriaDetail.getFellow("includePublicHolidays");
			includePublicHolidays.setSrc("/zul/jfs/agreement_and_criteria/public_holiday_list.zul");

			Include includeCommodityCategory = (Include) winAgreementAndCriteriaDetail.getFellow("includeCommodityCategory");
			includeCommodityCategory.setSrc("/zul/jfs/agreement_and_criteria/commodity_category_mapping_list.zul?agreementCode="+agreementDetail.getCode());

			Include includeCommodityType = (Include) winAgreementAndCriteriaDetail.getFellow("includeCommodityType");
			includeCommodityType.setSrc("/zul/jfs/agreement_and_criteria/commodity_type_mapping_list.zul?agreementCode="+agreementDetail.getCode());
		}else{
			showErrorDialog("Product is not available for this agreement");
		}
	}
	
	public void onClick$buttonEdit(Event e) throws InterruptedException {
		Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationedit"), MessageResources.getMessageResources("zul.common.label.confirmationedit"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
			@Override
			public void onEvent(Event e) throws Exception {
				if (Messagebox.ON_OK.equals(e.getName())) {
					Map<String, Object> args = new HashMap<String, Object>();					
					args.put("screenState", "edit");
					args.put("agreement", agreementDetail);
					navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_add.zul", args, winAgreementAndCriteriaDetail);
				} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
				} 
			}
		});
	}
	
	public void onClick$buttonDelete(Event e) throws InterruptedException {
		String labelConfirmationMessage = CONSTANT_UTIL.DEFAULT_EMPTY;
		String labelConfirmation =CONSTANT_UTIL.DEFAULT_EMPTY;

		if(agreementDetail.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO)){
			labelConfirmationMessage = MessageResources.getMessageResources("zul.common.label.message.confirmationdelete");
			labelConfirmation =MessageResources.getMessageResources("zul.common.label.confirmationdelete");                        		
		}else if(agreementDetail.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)){
			labelConfirmationMessage = MessageResources.getMessageResources("zul.common.label.message.confirmationreactive");
			labelConfirmation =MessageResources.getMessageResources("zul.common.label.confirmationreactive"); 
		}
		Messagebox.show(labelConfirmationMessage, labelConfirmation, Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
			@Override
			public void onEvent(Event e) throws Exception {
				if (Messagebox.ON_OK.equals(e.getName())) {
					if(agreementDetail.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO)){
						showInformationDialog("Agreement successfully Deactive");
					}else{
						showInformationDialog("Agreement successfully Re-Active");
					}
					agreementDetail.setIsDelete(agreementDetail.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO) ? CONSTANT_UTIL.DEFAULT_YES : CONSTANT_UTIL.DEFAULT_NO);
					AgreementDAO.update(agreementDetail);

					Map<String, Object> args = new HashMap<String, Object>();
					navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_list.zul", args, winAgreementAndCriteriaDetail);
				} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
				} 
			}
		});
	}
	
	public void onClick$buttonBack(Event e) throws InterruptedException {
		Map<String, Object> args = new HashMap<String, Object>();
		navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_list.zul", args, winAgreementAndCriteriaDetail);
	}
	
	public void edit() {
	}
	
	public void setupViewDisplay(Product product){
		Agreement agreement = AgreementDAO.getAgreementByCode(String.valueOf(product.getAgreement()));
		Label labelPartner = (Label) winAgreementAndCriteriaDetail.getFellow("labelPartner");
		labelPartner.setValue(agreement.getPartner().getName());

		Label labelValidFrom = (Label) winAgreementAndCriteriaDetail.getFellow("labelValidFrom");
		labelValidFrom.setValue(String.valueOf(product.getValidFrom()));

		Label labelValidTo = (Label) winAgreementAndCriteriaDetail.getFellow("labelValidTo");
		labelValidTo.setValue(String.valueOf(product.getValidTo()));

		Label labelAgreementCode = (Label) winAgreementAndCriteriaDetail.getFellow("labelAgreementCode");
		labelAgreementCode.setValue(agreement.getCodeAgreement());

		Label labelAgreementName = (Label) winAgreementAndCriteriaDetail.getFellow("labelAgreementName");
		labelAgreementName.setValue(agreement.getName());

		Label labelInterestRate = (Label) winAgreementAndCriteriaDetail.getFellow("labelInterestRate");
		labelInterestRate.setValue(product.getInterestRate()==null ? CONSTANT_UTIL.NUMBER_0 : product.getInterestRate().toString());

		Label labelPrincipalSplitRate = (Label) winAgreementAndCriteriaDetail.getFellow("labelPrincipalSplitRate");
		labelPrincipalSplitRate.setValue(product.getPrincipalSplitRate()==null ? CONSTANT_UTIL.NUMBER_0 : product.getPrincipalSplitRate().toString());

		Label labelAdminFeeRate = (Label) winAgreementAndCriteriaDetail.getFellow("labelAdminFeeRate");
		labelAdminFeeRate.setValue(product.getAdminFeeRate()==null ? CONSTANT_UTIL.NUMBER_0 : product.getAdminFeeRate().toString());


	}


}