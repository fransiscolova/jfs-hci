package com.hci.jfs.composer;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.AgreementDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;

/**
 *
 * @author Igor
 */
public class AgreementAndCriteriaListComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(AgreementAndCriteriaListComposer.class.getName());

	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	private AgreementDAOImpl agreementDAO;

	Window winAgreementAndCriteriaList;
	Label labelSuccessMessage;
	Label labelErrorMessage;
	Grid gridAgreement;
	Checkbox showAllData;

	List<Agreement> agreementList;

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		agreementDAO = (AgreementDAOImpl) SpringContextHolder.getApplicationContext().getBean("AgreementDAO");

		getData();

		showAllData.setValue(MessageResources.getMessageResources("zul.common.label.showalldata"));

		clearMessage();
	}

	public void onClick$buttonAdd(Event e) throws InterruptedException {
		clearMessage();
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("screenState", "add");
		navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_add.zul", args, winAgreementAndCriteriaList);
	}

	public void onClick$showAllData(Event e) throws InterruptedException {
		clearMessage();

		if (showAllData.isChecked()) {
			getAllData();
		} else {
			getData();
		}
	}

	public void getData() {
		logger.debug("Fetching Data");
		agreementList = agreementDAO.getAgreement();

		gridAgreement.setModel(new SimpleListModel <Agreement> (agreementList));

		gridAgreement.setRowRenderer(new RowRenderer<Agreement>() {
			public void render(Row row, final Agreement agreement, int i) throws Exception {                
				row.appendChild(new Label(agreement.getCode()));
				row.appendChild(new Label(agreement.getName()));
				row.appendChild(new Label(agreement.getPartner().getName()));
				row.appendChild(new Label(sdf.format(agreement.getValidFrom())));
				row.appendChild(new Label(sdf.format(agreement.getValidTo())));

				Div div = new Div();
				Button btnView = new Button(null, "/images/icon/view.png");
				btnView.setTooltip("Detail");
				btnView.setParent(div);
				btnView.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						clearMessage();
						Map<String, Object> args = new HashMap<String, Object>();
						args.put("screenState", "detail");
						args.put("agreement", agreement);
						navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_detail.zul", args, winAgreementAndCriteriaList);
					}
				});
				Button btnEdit = new Button(null, "/images/icon/edit.png");
				btnView.setTooltip("Edit");
				btnEdit.setParent(div);
				btnEdit.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						clearMessage();
						Map<String, Object> args = new HashMap<String, Object>();
						args.put("screenState", "edit");
						args.put("agreement", agreement);
						navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_add.zul", args, winAgreementAndCriteriaList);
					}
				});
				Button btnDelete = new Button(null, "/images/icon/delete.png");
				btnView.setTooltip("Delete");
				btnDelete.setParent(div);
				btnDelete.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationdelete"), MessageResources.getMessageResources("zul.common.label.confirmationdelete"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
							@Override
							public void onEvent(Event e) throws Exception {
								clearMessage();
								if (Messagebox.ON_OK.equals(e.getName())) {
									delete(agreement);
									showAllData.setChecked(false);
								} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
								} 
							}
						});
					}
				});
				row.appendChild(div);
			}
		});
	}

	public void getAllData() {
		agreementList = agreementDAO.getAllAgreement();

		gridAgreement.setModel(new SimpleListModel <Agreement> (agreementList));

		gridAgreement.setRowRenderer(new RowRenderer<Agreement>() {
			public void render(Row row, final Agreement agreement, int i) throws Exception {                
				row.appendChild(new Label(agreement.getCode()));
				row.appendChild(new Label(agreement.getName()));
				row.appendChild(new Label(agreement.getPartner().getName()));
				row.appendChild(new Label(sdf.format(agreement.getValidFrom())));
				row.appendChild(new Label(sdf.format(agreement.getValidTo())));

				Div div = new Div();
				Button btnView = new Button(null, "/images/icon/view.png");
				btnView.setParent(div);
				btnView.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						clearMessage();
						Map<String, Object> args = new HashMap<String, Object>();
						args.put("screenState", "detail");
						args.put("agreement", agreement);
						navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_detail.zul", args, winAgreementAndCriteriaList);
					}
				});
				Button btnEdit = new Button(null, "/images/icon/edit.png");
				btnEdit.setParent(div);
				btnEdit.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						clearMessage();
						Map<String, Object> args = new HashMap<String, Object>();
						args.put("screenState", "edit");
						args.put("agreement", agreement);
						navigateTo("/zul/jfs/agreement_and_criteria/agreement_and_criteria_add.zul", args, winAgreementAndCriteriaList);
					}
				});
				Button btnDelete = new Button(null, "/images/icon/delete.png");
				btnDelete.setParent(div);
				btnDelete.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						String labelConfirmationMessage = CONSTANT_UTIL.DEFAULT_EMPTY;
						String labelConfirmation =CONSTANT_UTIL.DEFAULT_EMPTY;

						if(agreement.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO)){
							labelConfirmationMessage = MessageResources.getMessageResources("zul.common.label.message.confirmationdelete");
							labelConfirmation =MessageResources.getMessageResources("zul.common.label.confirmationdelete");                        		
						}else if(agreement.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)){
							labelConfirmationMessage = MessageResources.getMessageResources("zul.common.label.message.confirmationreactive");
							labelConfirmation =MessageResources.getMessageResources("zul.common.label.confirmationreactive"); 
						}

						Messagebox.show(labelConfirmationMessage,labelConfirmation, Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
							@Override
							public void onEvent(Event e) throws Exception {
								clearMessage();
								if (Messagebox.ON_OK.equals(e.getName())) {
									agreement.setIsDelete(agreement.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO) ? CONSTANT_UTIL.DEFAULT_YES : CONSTANT_UTIL.DEFAULT_NO);
									agreementDAO.update(agreement);
									showAllData.setChecked(false);
								} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
								} 
							}
						});
					}
				});
				row.appendChild(div);
			}
		});
	}

	public void clearMessage() {
		labelSuccessMessage.setValue("");
		labelErrorMessage.setValue("");
	}

	public void delete(Agreement agreement) {
		try {
			agreement.setIsDelete(CONSTANT_UTIL.DEFAULT_YES);
			agreementDAO.update(agreement);

			if (showAllData.isChecked()) {
				getAllData();
			} else {
				getData();
			}

			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deletesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deleteerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}