package com.hci.jfs.composer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.util.MessageResources;


/**
 *
 * @author Igor
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class CommodityCategoryMappingAddComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(CommodityCategoryMappingAddComposer.class.getName());
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	Window winProductMappingAdd;
	Window winCommodityCategoryMappingAdd;
	Label labelSuccessMessage;
	Label labelErrorMessage;
	Grid gridCommodityCategoryMapping;
	
	List<Object> commodityCategoryMappingList;
	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
		getData();
		clearMessage();
	}
	
	public void onClick$buttonAdd(Event event) {
		clearMessage();
		Map<String, Object> map = new HashMap<String, Object>();
		
		Window window = (Window) Executions.createComponents("/zul/jfs/agreement_and_criteria/commodity_category_mapping_popup.zul", null, map);
		window.setTitle(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.addcommoditycategorymapping"));
		window.addEventListener("onClose", new EventListener () {
			public void onEvent (Event event) {
				logger.debug("refresh commodity category mapping");
			}
		});
		window.doModal();
	}
	
	public void onClick$buttonSave(Event e) throws InterruptedException {
		clearMessage();
		Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationadd"), MessageResources.getMessageResources("zul.common.label.confirmationadd"), Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {
			@Override
			public void onEvent(Event e) throws Exception {
				if (Messagebox.ON_OK.equals(e.getName())) {
					save();
				} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
				} 
			}
		});
	}
	
	public void onClick$buttonNext(Event e) throws InterruptedException {
		clearMessage();
		Map <String, Object> args = new HashMap<String, Object>();
		navigateTo("/zul/jfs/agreement_and_criteria/commodity_type_mapping_add.zul", args, winCommodityCategoryMappingAdd);
	}
	
	public void onClick$buttonCancel(Event e) throws InterruptedException {
		clearMessage();
		backTo(winProductMappingAdd, winCommodityCategoryMappingAdd);
	}
	
	public void getData() {
		commodityCategoryMappingList = new ArrayList<Object>();
		
		gridCommodityCategoryMapping.setModel(new SimpleListModel <Object> (commodityCategoryMappingList));
        
		gridCommodityCategoryMapping.setRowRenderer(new RowRenderer<Object>() {
            public void render(Row row, final Object obj, int i) throws Exception {                
                
                Div div = new Div();
                Button btnEdit = new Button(null, "/images/icon/edit.png");
                btnEdit.setParent(div);
                btnEdit.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                    	clearMessage();
                    	Map<String, Object> args = new HashMap<String, Object>();
                        args.put("screenState", "edit");
                        args.put("object", obj);
                		
                        Window window = (Window) Executions.createComponents("/zul/jfs/agreement_and_criteria/commodity_category_mapping_popup.zul", null, args);
                		window.setTitle(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.updatecommoditycategorymapping"));
                		window.addEventListener("onClose", new EventListener () {
                			public void onEvent (Event event) {
                				logger.debug("refresh product mapping");
                			}
                		});
                		window.doModal();
                    }
                });
                Button btnDelete = new Button(null, "/images/icon/delete.png");
                btnDelete.setParent(div);
                btnDelete.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                    	Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationdelete"), MessageResources.getMessageResources("zul.common.label.confirmationdelete"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
            				@Override
            				public void onEvent(Event e) throws Exception {
            					clearMessage();
            					if (Messagebox.ON_OK.equals(e.getName())) {
            						delete(obj);
            					} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
            					} 
            				}
            			});
                    }
                });
                row.appendChild(div);
            }
        });
	}
	
	public void clearMessage() {
		labelSuccessMessage.setValue("");
		labelErrorMessage.setValue("");
	}
	
	public void save() {
		try {
			getData();
			
			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.savesuccess"));
			
		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}
	
	public void delete(Object obj) {
		try {
			getData();
			
			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deletesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deleteerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}