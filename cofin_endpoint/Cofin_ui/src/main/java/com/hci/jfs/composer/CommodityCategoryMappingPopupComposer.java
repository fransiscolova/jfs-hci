package com.hci.jfs.composer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.ProductDAOImpl;
import com.hci.jfs.entity.CommodityGroup;
import com.hci.jfs.entity.form.CommodityMappingForm;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;


/**
 *
 * @author Igor
 */
public class CommodityCategoryMappingPopupComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(CommodityCategoryMappingPopupComposer.class.getName());
	private ProductDAOImpl productDAO = (ProductDAOImpl) SpringContextHolder.getApplicationContext().getBean("productDAO");
	private Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	
	Window winCommodityCategoryMappingPopup;
	Label labelErrorMessage;
	
	Listbox listboxHCIDCommodityCategory;
	Textbox textboxPartnerCommodityCategory;
	Listbox listboxBIEconomySector;
	Datebox dateboxValidFrom;
	Datebox dateboxValidTo;
	Button buttonAdd;
	
	Label messageHCIDCommodityCategory;
	Label messagePartnerCommodityCategory;
	Label messageBIEconomySector;
	Label messageValidFrom;
	Label messageValidTo;
	
	private CommodityMappingForm form;
	private String screenstate=CONSTANT_UTIL.DEFAULT_COMMA;
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
		clearMessage();
	}
	
	private void initComponent(){
		try{
			screenstate = (String) Executions.getCurrent().getArg().get("screenState");
			if(screenstate.equals("edit")){
				form = (CommodityMappingForm) Executions.getCurrent().getArg().get("commodity");
				new Listitem(form.getHciCodeCommodityCategory(), form.getIdCommodity()).setParent(listboxHCIDCommodityCategory);
				listboxHCIDCommodityCategory.setSelectedIndex(0);
								
				new Listitem(form.getBptnCommodityGroup().getBiEconomicSector(),form.getBptnCommodityGroup().getBiEconomicSector()).setParent(listboxBIEconomySector);
				listboxBIEconomySector.setSelectedIndex(0);
				
				textboxPartnerCommodityCategory.setValue(form.getBptnCommodityGroup().getBankCommodityGroup());
				
				if(form.getValidFrom()!=null){
					dateboxValidFrom.setValue(form.getValidFrom());
				}
				
				if(form.getValidTo()!=null){
					dateboxValidTo.setValue(form.getValidTo());
				}
				buttonAdd.setLabel(MessageResources.getMessageResources("zul.common.label.edit"));
			}


		}catch(Exception e){
			showErrorDialog("Product is not found");
			e.printStackTrace();
		}
	}
	public void onClick$buttonAdd(Event e) throws InterruptedException {
		clearMessage();
		
		if (checkValidation()) {
			save();
		}
	}
	
	public void onClick$buttonCancel(Event e) throws InterruptedException {
		clearMessage();
		winCommodityCategoryMappingPopup.detach();
	}
	
	public void clearMessage() {
		labelErrorMessage.setValue("");
		messageHCIDCommodityCategory.setValue("");
		messagePartnerCommodityCategory.setValue("");
		messageBIEconomySector.setValue("");
		messageValidFrom.setValue("");
		messageValidTo.setValue("");
	}

	public boolean checkValidation() {
		boolean result = true;
		
		if (listboxHCIDCommodityCategory.getSelectedItem() == null) {
			result = false;
			messageHCIDCommodityCategory.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (textboxPartnerCommodityCategory.getValue().equals("")) {
			result = false;
			messagePartnerCommodityCategory.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}
		
		if (listboxBIEconomySector.getSelectedItem() == null) {
			result = false;
			messageBIEconomySector.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}
		
		if (dateboxValidFrom.getValue() == null) {
			result = false;
			messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			/*if (dateboxValidFrom.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantoday"));
			}*/
		}
		
		if (dateboxValidTo.getValue() == null) {
			result = false;
			messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			if (dateboxValidTo.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.todatebiggerthantoday"));
			}
		}
		
		if (dateboxValidFrom.getValue() != null && dateboxValidTo.getValue() != null) {
			if (dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) == 0 || dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) > 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantodate"));
			}
		}

		return result;
	}
	
	public void save() {
		try {
			winCommodityCategoryMappingPopup.detach();
			UserCredential userCredential = (UserCredential) auth.getPrincipal();
			if(screenstate.equals("edit")){
				CommodityGroup commodityGroup = new CommodityGroup();
				commodityGroup.setId(form.getIdCommodity());
				commodityGroup.setBptnCommodityGroup(form.getBptnCommodityGroup());
				commodityGroup.setDescription(form.getDescription());
				commodityGroup.setHciCodeCommodityCategory(form.getHciCodeCommodityCategory());
				commodityGroup.setIdAgreement(form.getIdAgreement());
				commodityGroup.setValidFrom(dateboxValidFrom.getValue());
				commodityGroup.setValidTo(dateboxValidTo.getValue());
				commodityGroup.setValidTo(dateboxValidTo.getValue());
				commodityGroup.setIsDelete(form.getIsDelete());
				commodityGroup.setUpdatedBy(userCredential.getUsername());
				commodityGroup.setUpdatedDate(new Date());
				productDAO.update(commodityGroup);
			}
			
			Map<String, Object> args = new HashMap<String, Object>();		
			Events.sendEvent(new Event("onClose", winCommodityCategoryMappingPopup, args));
		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}