package com.hci.jfs.composer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.entity.CommodityType;
import com.hci.jfs.entity.form.CommodityMappingForm;
import com.hci.jfs.services.CommodityService;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;


/**
 *
 * @author Igor
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class CommodityTypeMappingListComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(CommodityTypeMappingListComposer.class.getName());

	private CommodityService commodityService = (CommodityService) SpringContextHolder.getApplicationContext().getBean("commodityService");

	Window winProductMappingAdd;
	Label labelSuccessMessage;
	Label labelErrorMessage;
	Grid gridCommodityTypeMapping;

	List<CommodityMappingForm> commodityTypeMappingList;

	private String agreementCode=CONSTANT_UTIL.DEFAULT_EMPTY;

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
		getData();
		clearMessage();
	}


	private void initComponent(){
		try{
			agreementCode = ((String[])param.get("agreementCode"))[0];
		}catch(Exception e){
			showErrorDialog("Agreement is not found");
			e.printStackTrace();
		}
	}

	public void onClick$buttonAdd(Event event) {
		clearMessage();
		Map<String, Object> map = new HashMap<String, Object>();

		Window window = (Window) Executions.createComponents("/zul/jfs/agreement_and_criteria/commodity_type_mapping_popup.zul", null, map);
		window.setTitle(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.addcommoditytypemapping"));
		window.addEventListener("onClose", new EventListener () {
			public void onEvent (Event event) {
				logger.debug("refresh commodity type mapping");
			}
		});
		window.doModal();
	}

	public void getData() throws Exception {
		commodityTypeMappingList =  commodityService.getCommodityGroupTypeByAgreementCode(Long.parseLong(agreementCode));

		List<CommodityType> listCommodityType=null;
		if(commodityTypeMappingList!=null){
			if(commodityTypeMappingList.size()>0){
				listCommodityType = new ArrayList<CommodityType>();
				for (CommodityMappingForm commodityMappingForm : commodityTypeMappingList) {
					listCommodityType.addAll(commodityMappingForm.getListCommodityType());
				}
			}
		}	

		gridCommodityTypeMapping.setModel(new SimpleListModel <CommodityType> (listCommodityType));

		gridCommodityTypeMapping.setRowRenderer(new RowRenderer<CommodityType>() {
			public void render(Row row, final CommodityType obj, int i) throws Exception {                
				new Label(obj.getHciCodeCommodityCategory()).setParent(row);
				new Label(obj.getBtpnCodeCommodityCategory()).setParent(row);
				new Label(obj.getHciCodeCommodityType()).setParent(row);
				new Label(obj.getBtpnCodeCommodityType()).setParent(row);
				new Label(CONSTANT_UTIL.MINUS).setParent(row);
				new Label(CONSTANT_UTIL.MINUS).setParent(row);
				
				Div div = new Div();
				Button btnEdit = new Button(null, "/images/icon/edit.png");
				btnEdit.setParent(div);
				btnEdit.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						clearMessage();
						Map<String, Object> args = new HashMap<String, Object>();
						args.put("screenState", "edit");
						args.put("object", obj);

						Window window = (Window) Executions.createComponents("/zul/jfs/agreement_and_criteria/commodity_type_mapping_popup.zul", null, args);
						window.setTitle(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.updatecommoditytypemapping"));
						window.addEventListener("onClose", new EventListener () {
							public void onEvent (Event event) {
								logger.debug("refresh product mapping");
							}
						});
						window.doModal();
					}
				});
				Button btnDelete = new Button(null, "/images/icon/delete.png");
				btnDelete.setParent(div);
				btnDelete.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationdelete"), MessageResources.getMessageResources("zul.common.label.confirmationdelete"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
							@Override
							public void onEvent(Event e) throws Exception {
								clearMessage();
								if (Messagebox.ON_OK.equals(e.getName())) {
									delete(obj);
								} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
								} 
							}
						});
					}
				});
				div.setParent(row);
			}
		});
	}

	public void clearMessage() {
		labelSuccessMessage.setValue("");
		labelErrorMessage.setValue("");
	}

	public void save() {
		try {
			getData();

			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.savesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}

	public void delete(Object obj) {
		try {
			getData();

			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deletesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deleteerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}