package com.hci.jfs.composer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.util.MessageResources;


/**
 *
 * @author Igor
 */
public class CommodityTypeMappingPopupComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(CommodityTypeMappingPopupComposer.class.getName());
	
	Window winCommodityTypeMappingPopup;
	Label labelErrorMessage;
	
	Listbox listboxHCIDCommodityCategory;
	Textbox textboxPartnerCommodityCategory;
	Listbox listboxHCIDCommodityType;
	Textbox textboxPartnerCommodityType;
	Datebox dateboxValidFrom;
	Datebox dateboxValidTo;
	
	Label messageHCIDCommodityCategory;
	Label messagePartnerCommodityCategory;
	Label messageHCIDCommodityType;
	Label messagePartnerCommodityType;
	Label messageValidFrom;
	Label messageValidTo;
	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
		clearMessage();
	}
	
	public void onClick$buttonAdd(Event e) throws InterruptedException {
		clearMessage();
		
		if (checkValidation()) {
			save();
		}
	}
	
	public void onClick$buttonCancel(Event e) throws InterruptedException {
		clearMessage();
		winCommodityTypeMappingPopup.detach();
	}
	
	public void clearMessage() {
		labelErrorMessage.setValue("");
		messageHCIDCommodityCategory.setValue("");
		messagePartnerCommodityCategory.setValue("");
		messageHCIDCommodityType.setValue("");
		messagePartnerCommodityType.setValue("");
		messageValidFrom.setValue("");
		messageValidTo.setValue("");
	}

	public boolean checkValidation() {
		boolean result = true;
		
		if (listboxHCIDCommodityCategory.getSelectedItem() == null) {
			result = false;
			messageHCIDCommodityCategory.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (textboxPartnerCommodityCategory.getValue().equals("")) {
			result = false;
			messagePartnerCommodityCategory.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}
		
		if (listboxHCIDCommodityType.getSelectedItem() == null) {
			result = false;
			messageHCIDCommodityType.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}
		
		if (textboxPartnerCommodityType.getValue().equals("")) {
			result = false;
			messagePartnerCommodityType.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}
		
		if (dateboxValidFrom.getValue() == null) {
			result = false;
			messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			if (dateboxValidFrom.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantoday"));
			}
		}
		
		if (dateboxValidTo.getValue() == null) {
			result = false;
			messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			if (dateboxValidTo.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.todatebiggerthantoday"));
			}
		}
		
		if (dateboxValidFrom.getValue() != null && dateboxValidTo.getValue() != null) {
			if (dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) == 0 || dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) > 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantodate"));
			}
		}

		return result;
	}
	
	public void save() {
		try {
			winCommodityTypeMappingPopup.detach();
			
			Map<String, Object> args = new HashMap<String, Object>();		
			Events.sendEvent(new Event("onClose", winCommodityTypeMappingPopup, args));
		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}