package com.hci.jfs.composer;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.activitylog.LogGenerateUploadFile;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.constant.ENUM_ERROR;
import com.hci.jfs.dao.impl.AgreementDAOImpl;
import com.hci.jfs.dao.impl.BatchDAOImpl;
import com.hci.jfs.dao.impl.PartnerDAOImpl;
import com.hci.jfs.dao.impl.PaymentDAOImpl;
import com.hci.jfs.dao.impl.ProcessTypeDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.BatchJobLog;
import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.GenerateFile;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.entity.ProcessType;
import com.hci.jfs.entity.UploadFile;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ApplicationExceptionService;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.services.impl.BatchProcessDescriptionServiceImpl;
import com.hci.jfs.util.ApplicationProperties;
import com.hci.jfs.util.DateUtil;
import com.hci.jfs.util.FileUtil;
import com.hci.jfs.util.InputValidator;
import com.hci.jfs.util.SpringContextHolder;

/**
 *
 * @author Igor
 */
public class FileGenerationUploadComposer extends CommonComposer {

    private static final long serialVersionUID = 1L;
    final static Logger logger = LogManager.getLogger(FileGenerationUploadComposer.class.getName());

    private ApplicationContext appContext = SpringContextHolder.getApplicationContext();
    private PartnerDAOImpl partnerDAO = (PartnerDAOImpl) appContext.getBean("PartnerDAO");
    private ApplicationExceptionService applicationExceptionService = (ApplicationExceptionService) appContext.getBean("applicationExceptionService");
    private AgreementDAOImpl agreementDAO = (AgreementDAOImpl) appContext.getBean("AgreementDAO");
    private BatchProcessDescriptionServiceImpl batchProcessDescriptionService = (BatchProcessDescriptionServiceImpl) appContext.getBean("batchProcessDescriptionService");
    private DateUtil dateUtil = (DateUtil) appContext.getBean("dateUtil");
    private ProcessTypeDAOImpl processTypeDAO = (ProcessTypeDAOImpl) appContext.getBean("ProcessTypeDAO");
    private BatchDAOImpl BatchDAO = (BatchDAOImpl) appContext.getBean("BatchDAO");
    private Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    private ApplicationProperties applicationProperties = (ApplicationProperties) appContext.getBean("applicationProperties");
    private FileUtil fileUtil = (FileUtil) appContext.getBean("fileUtil");
    private PaymentDAOImpl paymentDAO = (PaymentDAOImpl) appContext.getBean("PaymentDAO");
    private Media media;

    @Wire
    private Listbox partnerList;

    @Wire
    private Listbox partnerUploadList;

    @Wire
    private Listbox agreementCodeList;

    @Wire
    private Listbox agreementCodeUploadList;

    @Wire
    private Textbox agreementNameText;

    @Wire
    private Textbox agreementNameTextUpload;

    @Wire
    private Textbox agreementLetterNumber;

    @Wire
    private Listbox processTypeList;

    @Wire
    private Listbox processTypeListUpload;

    @Wire
    private Datebox processDate;

    @Wire
    private Datebox processDateUpload;

    @Wire
    private Listbox processTypeFileListUpload;

    @Wire
    private Label labelTypeFileUpload;

    private void clearAfterAction() {
        media = null;
        processDate.setValue(null);
        processDateUpload.setValue(null);
        processTypeListUpload.setSelectedIndex(0);
        processTypeList.setSelectedIndex(0);
        processTypeFileListUpload.setSelectedIndex(0);
        agreementCodeList.setSelectedIndex(0);
        agreementCodeUploadList.setSelectedIndex(0);
        partnerList.setSelectedIndex(0);
        partnerUploadList.setSelectedIndex(0);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        initComponent();
    }

    private String getChecksumFile(String content) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(content.getBytes());

        byte byteData[] = md.digest();
        // convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private Employee getEmployeeDetail(String username) {
        List<Map<String, Object>> listParam = new ArrayList<Map<String, Object>>();
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("key", "textEmailAddress");
        param.put("value", username.concat(CONSTANT_UTIL.WILDCARD));
        param.put("clause", CONSTANT_UTIL.CLAUSE_LIKE);
        listParam.add(param);
        Employee employee = partnerDAO.getByParamWithCustomClauseWhere(Employee.class, listParam);
        return employee;
    }

    public void initComponent() throws Exception {
        setGenerateTabPartnerList();
        setUploadTabPartnerList();
        if (partnerList.getItems().size() > 0) {
            partnerList.setSelectedIndex(0);
            String partnerId = partnerList.getSelectedItem().getValue();
            setGenerateTabAgreementByPartnerId(partnerId);
            agreementCodeList.setSelectedIndex(0);
            List<Agreement> listAgreement = agreementDAO.getAgreementByPartnerId(partnerId);
            if (listAgreement != null && !listAgreement.isEmpty()) {
                Agreement agreement = listAgreement.get(0);
                agreementNameText.setValue(agreement.getName());
            }
        }

        if (partnerUploadList.getItems().size() > 0) {
            partnerUploadList.setSelectedIndex(0);
            String partnerId = partnerUploadList.getSelectedItem().getValue();
            setUploadTabAgreementByPartnerId(partnerId);
            agreementCodeUploadList.setSelectedIndex(0);
            List<Agreement> listAgreement = agreementDAO.getAgreementByPartnerId(partnerId);
            if (listAgreement != null && !listAgreement.isEmpty()) {
                Agreement agreement = listAgreement.get(0);
                agreementNameTextUpload.setValue(agreement.getName());
            }
        }
        setGenerateProcessType();
        setUploadProcessType();
        setFileUploadTypeProcesss();
    }

    public void onClick$btnGenerate(Event e) throws Exception {
        try {
            InputValidator.validateGenerateFileInput(agreementCodeList, partnerList,
                    processTypeList, processDate);
            UserCredential userCredential = (UserCredential) auth.getPrincipal();
            Employee userlogin = getEmployeeDetail(userCredential.getUsername());

            String idAgreement = agreementCodeList.getSelectedItem().getValue();
            String idPartner = partnerList.getSelectedItem().getValue();
            String processType = processTypeList.getSelectedItem().getValue();
            String letterNumber = agreementLetterNumber.getValue();
            Date createdDate = processDate.getValue();
            String runDate = dateUtil.getFormattedCurrentDatePattern2(createdDate);
            String processDateCheck = dateUtil.getFormattedCurrentDatePattern10(createdDate);

            if (letterNumber == null || CONSTANT_UTIL.DEFAULT_EMPTY.equals(letterNumber)) {
                throw new ApplicationException("ERR-JFS-FILE-GENERATED-002");
            }

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("agreementCode", idAgreement);
            param.put("partnerCode", idPartner);
            param.put("processCode", processType);
            param.put("letterNumber", letterNumber);
            param.put("userCredential", userCredential);
            param.put("processDateCheck", processDateCheck);

            GenerateFile generateFile = null;
            if (processType.equals(CONSTANT_UTIL.JFS_CR_REGULAR)
                    || processType.equals(CONSTANT_UTIL.JFS_CR_MPF)
                    || processType.equals(CONSTANT_UTIL.JFS_CR_ZERO)) {
                Map<String, String> processTypeMap = applicationProperties
                        .generateMapByKeyProperties("process.type.contract.registration",
                                CONSTANT_UTIL.JFS_PROPERTIES, CONSTANT_UTIL.DEFAULT_SEPARATOR,
                                CONSTANT_UTIL.DEFAULT_COMMA);
                String processContract = processTypeMap.get(processType);
                generateFile = processTypeDAO
                        .getFileGenerateByAgreementCodeByCreateDateByPartnerCodeByProcessCode(
                                CONSTANT_UTIL.UNKNOWN, runDate, CONSTANT_UTIL.UNKNOWN,
                                processContract);
            } else {
                generateFile = processTypeDAO
                        .getFileGenerateByAgreementCodeByCreateDateByPartnerCodeByProcessCode(
                                idAgreement, runDate, idPartner, processType);
            }

            if (generateFile != null) {
                throw new ApplicationException("ERR-JFS-PAYMENT-FILE-EXIST");
            }

            ProcessType type = processTypeDAO.getProcessTypeByCode(processType);
            param.put("createdDate", runDate);
            param.put("proceedDate", createdDate);
            param.put("processDate", createdDate);
            param.put("userLogin", userlogin);

            BatchJobLog batchJobLog = null;
            if (processType.equals(CONSTANT_UTIL.JFS_CR_REGULAR)
                    || processType.equals(CONSTANT_UTIL.JFS_CR_MPF)
                    || processType.equals(CONSTANT_UTIL.JFS_CR_ZERO)) {
                Map<String, String> fileProcessTypeMap = applicationProperties
                        .generateMapByKeyProperties("file.process.type.contract.registration",
                                CONSTANT_UTIL.JFS_PROPERTIES, CONSTANT_UTIL.DEFAULT_SEPARATOR,
                                CONSTANT_UTIL.DEFAULT_COMMA);
                String filename = fileProcessTypeMap.get(processType);
                Map<String, String> processTypeMap = applicationProperties
                        .generateMapByKeyProperties("process.type.contract.registration",
                                CONSTANT_UTIL.JFS_PROPERTIES, CONSTANT_UTIL.DEFAULT_SEPARATOR,
                                CONSTANT_UTIL.DEFAULT_COMMA);
                String processContract = processTypeMap.get(processType);
                batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                        CONSTANT_UTIL.UNKNOWN, CONSTANT_UTIL.UNKNOWN, processContract, runDate,
                        filename);
                param.put("type", filename);
            } else {
                batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(idPartner,
                        idAgreement, processType, runDate, type.getTypeFile());
                param.put("type", type.getTypeFile());
            }
            param.put("fileType", "generated");
            if (batchJobLog == null) {
                throw new ApplicationException(ENUM_ERROR.ERR_FILE_NOT_EXIST.toString());
            }

            paymentDAO.checkingLogGenerateUploadFile(param);

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        LogGenerateUploadFile log = batchProcessDescriptionService
                                .saveLogGenerateUploadFile(param);
                        batchProcessDescriptionService.exportFileBasedOnProcessType(param);
                        BatchDAO.delete(log);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
            this.showInformationDialog("Please wait few minutes the system is processing the file,\r\nSystem will send email to you after finish.\r\n Thank You");

        } catch (ApplicationException exception) {
            exception.printStackTrace();
            showErrorDialog(applicationExceptionService.getApplicationExceptionByErrorCode(
                    exception.getErrorCode()).getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            showErrorDialog(exception.getMessage());
        }
    }

    public void onClick$btnUpload(Event e) throws Exception {
        try {
            InputValidator.validateUploadFileInput(partnerUploadList, agreementCodeUploadList,
                    processTypeListUpload, processTypeFileListUpload, applicationProperties,
                    processDateUpload, media);

            String fileType = processTypeFileListUpload.getSelectedItem().getValue();
            UserCredential userCredential = (UserCredential) auth.getPrincipal();
            Employee userLogin = getEmployeeDetail(userCredential.getUsername());

            String idAgreement = agreementCodeUploadList.getSelectedItem().getValue();
            String idPartner = partnerUploadList.getSelectedItem().getValue();
            String processType = processTypeListUpload.getSelectedItem().getValue();
            String processTypeName = processTypeListUpload.getSelectedItem().getLabel();
            String content = CONSTANT_UTIL.DEFAULT_EMPTY;
            if (media.getFormat() != null) {
                content = media.getStringData().replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC,
                        CONSTANT_UTIL.DEFAULT_EMPTY);
            } else {
                byte[] bytesContent = media.getByteData();
                content = new String(bytesContent).replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC,
                        CONSTANT_UTIL.DEFAULT_EMPTY);
            }
            byte[] bytes = fileUtil.getBytes(new ByteArrayInputStream(content
                    .getBytes(StandardCharsets.UTF_8)));

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("agreementCode", idAgreement);
            param.put("partnerCode", idPartner);
            param.put("processCode", processType);
            param.put("media", media);
            param.put("userCredential", userCredential);
            param.put("processDate", processDateUpload.getValue());
            String createdDate = dateUtil.getFormattedCurrentDatePattern2(processDateUpload
                    .getValue());
            param.put("createdDate", createdDate);
            param.put("processTypeName", processTypeName);
            param.put("filename", media.getName());

            String checksum = getChecksumFile(content);
            List<Map<String, Object>> listParam = new ArrayList<Map<String, Object>>();

            Map<String, Object> paramVar1 = new HashMap<String, Object>();
            paramVar1.put("key", "checksum");
            paramVar1.put("value", checksum);

            Map<String, Object> paramVar2 = new HashMap<String, Object>();
            paramVar2.put("key", "sizeFile");
            paramVar2.put("value", String.valueOf(bytes.length));

            Map<String, Object> paramVar3 = new HashMap<String, Object>();
            paramVar3.put("key", "processType.code");
            paramVar3.put("value", processType);

            Map<String, Object> paramVar4 = new HashMap<String, Object>();
            paramVar4.put("key", "agreement.code");
            paramVar4.put("value", idAgreement);

            Map<String, Object> paramVar5 = new HashMap<String, Object>();
            paramVar5.put("key", "partner.id");
            paramVar5.put("value", idPartner);

            Map<String, Object> paramVar6 = new HashMap<String, Object>();
            paramVar6.put("key", "fileType");
            paramVar6.put("value", fileType);

            listParam.add(paramVar1);
            listParam.add(paramVar2);
            listParam.add(paramVar3);
            listParam.add(paramVar4);
            listParam.add(paramVar5);
            listParam.add(paramVar6);

            UploadFile uploadFile = BatchDAO.getByParam(UploadFile.class, listParam);
            if (uploadFile != null) {
                throw new ApplicationException(ENUM_ERROR.ERR_FILE_EXIST.toString());
            }

            param.put("checksum", checksum);
            param.put("size", String.valueOf(bytes.length));
            param.put("userLogin", userLogin);
            param.put("fileType", fileType);
            param.put("processDateCheck",
                    dateUtil.getFormattedCurrentDatePattern10(processDateUpload.getValue()));

            paymentDAO.checkingLogGenerateUploadFile(param);
            if (fileType.equals(CONSTANT_UTIL.FILE_TYPE_UPLOAD_ALLOCATION)) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            LogGenerateUploadFile log = batchProcessDescriptionService
                                    .saveLogGenerateUploadFile(param);
                            batchProcessDescriptionService
                            .uploadConvertFileAllocationBasedOnProcessType(param);
                            BatchDAO.delete(log);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                Thread thread = new Thread(runnable);
                thread.start();
            } else {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            LogGenerateUploadFile log = batchProcessDescriptionService
                                    .saveLogGenerateUploadFile(param);
                            batchProcessDescriptionService
                            .uploadConvertFileBasedOnProcessType(param);
                            BatchDAO.delete(log);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                Thread thread = new Thread(runnable);
                thread.start();
            }
            this.showInformationDialog("Please wait few minutes the system is processing the file,\r\nSystem will send email to you after finish.\r\n Thank You");
            clearAfterAction();
        } catch (ApplicationException exception) {
            exception.printStackTrace();
            showErrorDialog(applicationExceptionService.getApplicationExceptionByErrorCode(
                    exception.getErrorCode()).getErrorMessage());
        } catch (Exception error) {
            error.printStackTrace();
            showErrorDialog(error.getMessage());
        }
    }

    public void onSelect$agreementCodeList() throws Exception {
        String agreementId = agreementCodeList.getSelectedItem().getValue();
        Agreement agreement = agreementDAO.getAgreementById(agreementId);
        agreementNameText.setValue(agreement.getName());
    }

    public void onSelect$agreementCodeUploadList() throws Exception {
        String agreementId = agreementCodeUploadList.getSelectedItem().getValue();
        Agreement agreement = agreementDAO.getAgreementById(agreementId);
        if (agreement != null) {
            agreementNameTextUpload.setValue(agreement.getName());
        } else {
            agreementNameTextUpload.setValue(CONSTANT_UTIL.DEFAULT_EMPTY);
        }
    }

    public void onSelect$partnerList() throws Exception {
        String partnerId = partnerList.getSelectedItem().getValue();
        agreementCodeList.getItems().clear();
        setGenerateTabAgreementByPartnerId(partnerId);
        List<Agreement> listAgreement = agreementDAO.getAgreementByPartnerId(partnerId);
        if (listAgreement != null && !listAgreement.isEmpty()) {
            agreementCodeList.setSelectedIndex(0);
            processTypeList.setSelectedIndex(0);
        } else {
            agreementNameText.setValue(CONSTANT_UTIL.DEFAULT_EMPTY);
            showErrorDialog("Partner Doesnt Have Agreement.");
        }

    }

    public void onSelect$partnerUploadList() throws Exception {
        String partnerId = partnerUploadList.getSelectedItem().getValue();
        agreementCodeUploadList.getItems().clear();
        setUploadTabAgreementByPartnerId(partnerId);
        List<Agreement> listAgreement = agreementDAO.getAgreementByPartnerId(partnerId);
        if (listAgreement != null && !listAgreement.isEmpty()) {
            agreementCodeUploadList.setSelectedIndex(0);
            processTypeListUpload.setSelectedIndex(0);
        } else {
            agreementNameText.setValue(CONSTANT_UTIL.DEFAULT_EMPTY);
            showErrorDialog("Partner Doesnt Have Agreement.");
        }

    }

    public void onSelect$processTypeListUpload() throws Exception {
        String processType = processTypeListUpload.getSelectedItem().getValue();
        Map<String, String> mapProcessType = applicationProperties.generateMapByKeyProperties(
                "process.type.allocation.file.upload", CONSTANT_UTIL.JFS_PROPERTIES,
                CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_SEPARATOR);
        if (mapProcessType.get(processType) != null) {
            processTypeFileListUpload.setDisabled(false);
        } else {
            processTypeFileListUpload.setDisabled(true);
        }

    }

    public void onUpload$uploadFileData(UploadEvent event) throws Exception {
        try {
            media = event.getMedia();
            InputValidator.validateUploadFileInput(partnerUploadList, agreementCodeUploadList,
                    processTypeListUpload, processTypeFileListUpload, applicationProperties,
                    processDateUpload, media);

            UserCredential userCredential = (UserCredential) auth.getPrincipal();
            Employee userlogin = getEmployeeDetail(userCredential.getUsername());
            String idAgreement = agreementCodeUploadList.getSelectedItem().getValue();
            String idPartner = partnerUploadList.getSelectedItem().getValue();
            String processType = processTypeListUpload.getSelectedItem().getValue();
            String fileType = processTypeFileListUpload.getSelectedItem().getValue();
            Date processDate = processDateUpload.getValue();

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("agreementCode", idAgreement);
            param.put("partnerCode", idPartner);
            param.put("processCode", processType);
            param.put("media", media);
            param.put("userLogin", userlogin);
            param.put("userCredential", userCredential);
            param.put("fileType", fileType);
            param.put("processDate", processDate);

            if (fileType.equals(CONSTANT_UTIL.FILE_TYPE_UPLOAD_CONFIRMATION)) {
                batchProcessDescriptionService.uploadValidationFileBasedOnProcessType(param);
            } else if (fileType.equals(CONSTANT_UTIL.FILE_TYPE_UPLOAD_ALLOCATION)) {
                batchProcessDescriptionService
                .uploadValidationFileAllocationBasedOnProcessType(param);
            }
        } catch (ApplicationException exception) {
            media = null;
            exception.printStackTrace();
            showErrorDialog(applicationExceptionService.getApplicationExceptionByErrorCode(
                    exception.getErrorCode()).getErrorMessage());
        } catch (Exception e) {
            media = null;
            e.printStackTrace();
            showErrorDialog(e.getMessage());
        }
    }

    public void setFileUploadTypeProcesss() {
        List<HashMap<String, String>> listType = applicationProperties
                .getDroplist("jfs.file.generate.upload.process.upload.type");

        // populate data from application properties
        for (HashMap<String, String> hashMap : listType) {
            Listitem item = new Listitem();
            item.setValue(hashMap.get("id"));
            item.setLabel(hashMap.get("value"));
            item.setParent(processTypeFileListUpload);
        }
        processTypeFileListUpload.setSelectedIndex(0);
    }

    /**
     * Populate Generate Process Type data.
     */
    public void setGenerateProcessType() {
        try {
            List<ProcessType> listProcessTypes = processTypeDAO.getAllProcessType();

            // init default value
            Listitem item = new Listitem();
            item.setLabel("Choose");
            item.setValue(CONSTANT_UTIL.NUMBER_0);
            item.setParent(processTypeList);
            processTypeList.setSelectedIndex(0);

            // populate data from entity ProcessType
            if (listProcessTypes != null && !listProcessTypes.isEmpty()) {
                for (ProcessType processType : listProcessTypes) {
                    if (processType.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)) {
                        continue;
                    }
                    item = new Listitem();
                    item.setLabel(processType.getName());
                    item.setValue(processType.getCode());
                    item.setParent(processTypeList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e.getMessage());
        }
    }

    public void setGenerateTabAgreementByPartnerId(String partnerId) {
        try {
            List<Agreement> agreements = agreementDAO.getAgreementByPartnerId(partnerId);
            if (agreements != null) {
                Listitem item = new Listitem();
                item.setLabel("Choose");
                item.setValue(CONSTANT_UTIL.NUMBER_0);
                item.setParent(agreementCodeList);

                for (Agreement agreement : agreements) {
                    if (agreement.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)) {
                        continue;
                    }

                    item = new Listitem();
                    item.setLabel(agreement.getDescription() + " - " + agreement.getCode() + " - "
                            + agreement.getCodeAgreement());
                    item.setValue(agreement.getCode());
                    item.setParent(agreementCodeList);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e.getMessage());
        }
    }

    public void setGenerateTabPartnerList() {
        try {
            List<Partner> partners = partnerDAO.getAllPartner();
            if (partners != null) {
                Listitem item = new Listitem();
                item.setLabel("Choose");
                item.setValue(CONSTANT_UTIL.NUMBER_0);
                item.setParent(partnerList);
                for (Partner partner : partners) {
                    if (partner.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)) {
                        continue;
                    }

                    item = new Listitem();
                    item.setLabel(partner.getName());
                    item.setValue(partner.getId());
                    item.setParent(partnerList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e.getMessage());
        }
    }

    /**
     * Populate Upload Process Type data.
     */
    public void setUploadProcessType() {
        try {
            List<ProcessType> listProcessTypes = processTypeDAO.getAllProcessType();

            // init default value
            Listitem item = new Listitem();
            item.setLabel("Choose");
            item.setValue(CONSTANT_UTIL.NUMBER_0);
            item.setParent(processTypeListUpload);
            processTypeListUpload.setSelectedIndex(0);

            // populate data from entity ProcessType
            if (listProcessTypes != null && !listProcessTypes.isEmpty()) {
                for (ProcessType processType : listProcessTypes) {
                    if (processType.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)) {
                        continue;
                    }

                    item = new Listitem();
                    item.setLabel(processType.getName());
                    item.setValue(processType.getCode());
                    item.setParent(processTypeListUpload);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e.getMessage());
        }
    }

    public void setUploadTabAgreementByPartnerId(String partnerId) {
        try {
            List<Agreement> agreements = agreementDAO.getAgreementByPartnerId(partnerId);
            if (agreements != null) {
                Listitem item = new Listitem();
                item.setLabel("Choose");
                item.setValue(CONSTANT_UTIL.NUMBER_0);
                item.setParent(agreementCodeUploadList);

                for (Agreement agreement : agreements) {
                    if (agreement.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)) {
                        continue;
                    }

                    item = new Listitem();
                    item.setLabel(agreement.getDescription() + " - " + agreement.getCode() + " - "
                            + agreement.getCodeAgreement());
                    item.setValue(agreement.getCode());
                    item.setParent(agreementCodeUploadList);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e.getMessage());
        }
    }

    public void setUploadTabPartnerList() {
        processTypeFileListUpload.setDisabled(true);
        try {
            List<Partner> partners = partnerDAO.getAllPartner();
            if (partners != null) {
                Listitem item = new Listitem();
                item.setLabel("Choose");
                item.setValue(CONSTANT_UTIL.NUMBER_0);
                item.setParent(partnerUploadList);
                for (Partner partner : partners) {
                    if (partner.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)) {
                        continue;
                    }

                    item = new Listitem();
                    item.setLabel(partner.getName());
                    item.setValue(partner.getId());
                    item.setParent(partnerUploadList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e.getMessage());
        }
    }
}