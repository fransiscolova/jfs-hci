package com.hci.jfs.composer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.ProcessTypeDAOImpl;
import com.hci.jfs.entity.Contract;
import com.hci.jfs.entity.ErrorContractMonitoring;
import com.hci.jfs.entity.ErrorTypeContract;
import com.hci.jfs.entity.ProcessType;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ErrorTypeContractService;
import com.hci.jfs.services.MonitoringContractErrorService;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;

public class MonitoringContractErrorComposer extends CommonComposer{
	private static final long serialVersionUID = 1L;	

	@Autowired
	Listbox errorList;

	@Autowired
	Textbox contractText;

	@Autowired
	Grid grdListContract;

	@Autowired
	Button btnSaveContract;
	
	@Wire
	private Listbox processTypeList;

	@Wire
	private Datebox processDate;


	final static Logger logger = LogManager.getLogger(MonitoringContractErrorComposer.class.getName());
	private ErrorTypeContractService errorTypeContractService = (ErrorTypeContractService) SpringContextHolder.getApplicationContext().getBean("errorTypeContractService");
	private List<ErrorContractMonitoring> listContractError= new ArrayList<ErrorContractMonitoring>();
	private MonitoringContractErrorService monitoringContractErrorService = (MonitoringContractErrorService) SpringContextHolder.getApplicationContext().getBean("monitoringContractErrorService");
	private Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	private ProcessTypeDAOImpl processTypeDAO = (ProcessTypeDAOImpl) SpringContextHolder.getApplicationContext().getBean("ProcessTypeDAO");
	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
	}

	public void initComponent() throws ApplicationException{
		List<ErrorTypeContract> listError =  errorTypeContractService.getAllErrorTypeContract();
		if(!listError.isEmpty()){
			for (ErrorTypeContract error : listError) {
				Listitem listitem = new Listitem();
				listitem.setLabel(error.getCode().concat(CONSTANT_UTIL.MINUS).concat(error.getReason()));
				listitem.setValue(error.getCode());
				listitem.setParent(errorList);
			}
			errorList.setSelectedIndex(0);
		}		
		setGenerateProcessType();
	}	
	
	public void setGenerateProcessType(){
		try{
			List<ProcessType> listProcessTypes = processTypeDAO.getAllProcessType();
			if(listProcessTypes !=null && !listProcessTypes.isEmpty()){
				Listitem item =null;
				for (ProcessType processType : listProcessTypes) {
					if (processType.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES))
						continue;

					item = new Listitem();
					item.setLabel(processType.getName());
					item.setValue(processType.getCode());
					item.setParent(processTypeList);
				}
				processTypeList.setSelectedIndex(0);

			}
		}catch(Exception e){
			e.printStackTrace();			
			showErrorDialog(e.getMessage());
		}
	}

	public void onClick$btnAddContract(Event e) throws Exception {
		// Validation Input
		if(contractText.getValue()==null || contractText.getValue().equals(CONSTANT_UTIL.DEFAULT_EMPTY)){
			showErrorDialog("Contract Number is Mandatory");
			return;
		}
		
		if(processDate.getValue()==null || processDate.getValue().equals(CONSTANT_UTIL.DEFAULT_EMPTY)){
			showErrorDialog("Process Date is Mandatory");
			return;
		}
		Contract contract = new Contract(contractText.getValue());
		ErrorTypeContract errorTypeContract = new ErrorTypeContract(errorList.getSelectedItem().getValue());
		errorTypeContract.setReason(errorList.getSelectedItem().getLabel());
		if(listContractError.isEmpty() || listContractError.size()==0){
			ErrorContractMonitoring error =new ErrorContractMonitoring(contract, errorTypeContract);
			error.setProcess(new ProcessType(processTypeList.getSelectedItem().getValue()));
			error.setProcessDate(processDate.getValue());
			listContractError.add(error);
		}else if(listContractError.size()>0){
			int i=0;
			boolean status=Boolean.TRUE;
			while(status && i < listContractError.size()){
				ErrorContractMonitoring monitoring = listContractError.get(i);
				if(monitoring.getContract().getTextContractNumber().equals(contract.getTextContractNumber())){
					showErrorDialog("Contract Number is Already Registered, Please Input Another Contract Number");
					status=Boolean.FALSE;
				}
				i++;
			}
			if(status){
				ErrorContractMonitoring error =new ErrorContractMonitoring(contract, errorTypeContract);
				error.setProcess(processTypeList.getSelectedItem().getValue());
				error.setProcessDate(processDate.getValue());
				listContractError.add(error);
			}
		}
		setGridContract();	
	}

	public void onClick$btnSaveContract(Event e) throws Exception {

		Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationadd"), MessageResources.getMessageResources("zul.common.label.save"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
			@Override
			public void onEvent(Event e) {
				try{
					if (Messagebox.ON_OK.equals(e.getName())) {
						Date createdDate = new Date();
						for (ErrorContractMonitoring errorContractMonitoring : listContractError) {
							errorContractMonitoring.setCreatedBy(((UserCredential) auth.getPrincipal()).getUsername());
							errorContractMonitoring.setCreatedDate(createdDate);
						}
						monitoringContractErrorService.insert(listContractError);
						listContractError.clear();
						setGridContract();
					} 
				}catch(Exception e2){
					e2.printStackTrace();
					showErrorDialog("Error : "+e2.getMessage());
				}
			}
		});	

	}


	private void setGridContract()throws Exception{
		contractText.setValue(CONSTANT_UTIL.DEFAULT_EMPTY);
		if(listContractError.size()>0){
			btnSaveContract.setVisible(true);
			grdListContract.setVisible(true);
		}else {
			btnSaveContract.setVisible(false);
			grdListContract.setVisible(false);
		}

		grdListContract.setModel(new SimpleListModel <ErrorContractMonitoring> (listContractError));       
		grdListContract.setRowRenderer(new RowRenderer<ErrorContractMonitoring>() {

			@Override
			public void render(Row row, ErrorContractMonitoring data, int index)
					throws Exception {
				new Label(String.valueOf(data.getContract().getTextContractNumber())).setParent(row);
				new Label(data.getErrorTypeContract().getReason()).setParent(row);
				Button btnDelete = new Button(null, "/images/icon/delete.png");
				btnDelete.setParent(row);
				btnDelete.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationdelete"), MessageResources.getMessageResources("zul.common.label.confirmationdelete"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
							@Override
							public void onEvent(Event e) throws Exception {
								if (Messagebox.ON_OK.equals(e.getName())) {
									int i=0;
									for (ErrorContractMonitoring errorContractMonitoring : listContractError) {
										if(errorContractMonitoring.getErrorTypeContract().getCode().equals(data.getErrorTypeContract().getCode()) && errorContractMonitoring.getContract().getTextContractNumber().equals(data.getContract().getTextContractNumber())){
											listContractError.remove(i);
											break;
										}
										i++;
									}
									setGridContract();
								} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
								} 
							}
						});
					}
				});
			}
		});
	}
}
