package com.hci.jfs.composer;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.entity.ErrorTypeContract;
import com.hci.jfs.services.ErrorTypeContractService;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;

public class MonitoringContractTypeErrorComposer extends CommonComposer {
	private static final long serialVersionUID = 1L;	
	private Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	private ErrorTypeContractService errorTypeContractService = (ErrorTypeContractService) SpringContextHolder.getApplicationContext().getBean("errorTypeContractService");

	@Autowired
	Textbox codeText;

	@Autowired
	Textbox reasonText;

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}

	public void onClick$btnSave(Event e) throws Exception {

		if(codeText.getValue()==null || codeText.getValue().equals(CONSTANT_UTIL.DEFAULT_EMPTY)){
			showErrorDialog("Code is Mandatory");
		}

		if(reasonText.getValue()==null || reasonText.getValue().equals(CONSTANT_UTIL.DEFAULT_EMPTY)){
			showErrorDialog("Reason is Mandatory");
		}
		
		Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationadd"), MessageResources.getMessageResources("zul.common.label.save"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
			@Override
			public void onEvent(Event e) {
				try{
					if (Messagebox.ON_OK.equals(e.getName())) {
						Date createdDate = new Date();
						String name = ((UserCredential) auth.getPrincipal()).getUsername();
						ErrorTypeContract errorType = new ErrorTypeContract();
						errorType.setCode(codeText.getText());
						errorType.setReason(reasonText.getText());
						errorType.setCreatedBy(name);
						errorType.setCreatedDate(createdDate);
						errorType.setStatus(CONSTANT_UTIL.DEFAULT_YES);
						errorTypeContractService.insertErrorTypeContract(errorType);
						codeText.setValue(CONSTANT_UTIL.DEFAULT_EMPTY);
						reasonText.setValue(CONSTANT_UTIL.DEFAULT_EMPTY);						
					} 
				}catch(Exception e2){
					e2.printStackTrace();
					showErrorDialog("Error : "+e2.getMessage());
				}
			}
		});	
	}
}
