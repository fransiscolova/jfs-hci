package com.hci.jfs.composer;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.dao.impl.BatchDAOImpl;
import com.hci.jfs.dao.impl.MyBatchLogDAOImpl;
import com.hci.jfs.dao.impl.UserDAOImpl;
import com.hci.jfs.entity.Batch;
import com.hci.jfs.entity.MyBatchLog;
import com.hci.jfs.security.SecurityUtil;
import com.hci.jfs.util.ApplicationProperties;
import com.hci.jfs.util.SpringContextHolder;

/**
 *
 * @author Igor
 */
public class MyBatchListComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(MyBatchListComposer.class.getName());
	
	private BatchDAOImpl batchDAO;
	private UserDAOImpl userDAO;
	private String username;
	private ApplicationProperties applicationProperties;
	
	Window winBatchList;
	Label labelSuccessMessage;
	Label labelErrorMessage;
	Grid gridBatch;
	
	List<Batch> batchList;
	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
		batchDAO = (BatchDAOImpl) SpringContextHolder.getApplicationContext().getBean("BatchDAO");
		userDAO = (UserDAOImpl) SpringContextHolder.getApplicationContext().getBean("UserDAO");
        username = userDAO.getLdapUsername(SecurityUtil.getUser().getUsername());
        applicationProperties =  (ApplicationProperties) SpringContextHolder.getApplicationContext().getBean("applicationProperties");
		
        List<HashMap<String, String>> flags = applicationProperties.getDroplist("droplist.isactive");
        System.out.println(flags);
        
		getAllData();
		clearMessage();
	}
	
	public void getAllData() {
		batchList = batchDAO.getAllBatch();
		
		gridBatch.setModel(new SimpleListModel <Batch> (batchList));
        
		gridBatch.setRowRenderer(new RowRenderer<Batch>() {
            public void render(Row row, final Batch data, int i) throws Exception {                
            	row.appendChild(new Label(data.getJobId()));
            	row.appendChild(new Label(data.getJobName()));
                
                Div div = new Div();
                
                Button btnView = new Button(null, "/images/icon/view.png");
                btnView.setParent(div);
                btnView.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                    	clearMessage();
                    	
                    	MyBatchLogDAOImpl batchLogDAO = (MyBatchLogDAOImpl) SpringContextHolder.getApplicationContext().getBean("MyBatchLogDAO");
            			
            			MyBatchLog log = new MyBatchLog();
            			log.setJobId(data.getJobId());
            			log.setExecuteBy(username);
            			log.setExecuteDate(new Date());
            			log.setIsError("P");
            			
            			batchLogDAO.save(log);
                    	
                    	try {
                    		Object o = SpringContextHolder.getApplicationContext().getBean(data.getService());
                        	Method m = o.getClass().getDeclaredMethod(data.getMethod(), new Class[] {});
                        	m.invoke(o);
            				
            				log.setIsError("N");
            				batchLogDAO.update(log);
            				
            			} catch (Exception e) {
            				e.printStackTrace();
            				
            				log.setIsError("Y");
            				log.setErrorLog(e.getMessage());
            				batchLogDAO.update(log);
            			}
                    }
                });
                row.appendChild(div);
            }
        });
	}
	
	public void clearMessage() {
		labelSuccessMessage.setValue("");
		labelErrorMessage.setValue("");
	}
}