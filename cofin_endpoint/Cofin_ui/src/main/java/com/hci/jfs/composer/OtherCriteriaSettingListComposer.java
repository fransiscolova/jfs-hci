package com.hci.jfs.composer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.CommodityDAOImpl;
import com.hci.jfs.entity.OtherCriteriaSetting;
import com.hci.jfs.services.CommodityService;
import com.hci.jfs.util.DateUtil;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;


/**
 *
 * @author Igor
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class OtherCriteriaSettingListComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(OtherCriteriaSettingListComposer.class.getName());
	private CommodityService commodityService = (CommodityService) SpringContextHolder.getApplicationContext().getBean("commodityService");
	private DateUtil dateUtil = (DateUtil) SpringContextHolder.getApplicationContext().getBean("dateUtil");
	private CommodityDAOImpl commodityDAO = (CommodityDAOImpl) SpringContextHolder.getApplicationContext().getBean("commodityDAO");

	Window winProductMappingAdd;
	Label labelSuccessMessage;
	Label labelErrorMessage;
	Grid gridOtherCriteriaSetting;

	List<OtherCriteriaSetting> otherCriteriaSettingList;

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		getData();
		clearMessage();
	}

	public void onClick$buttonAdd(Event event) {
		clearMessage();
		Map<String, Object> map = new HashMap<String, Object>();

		Window window = (Window) Executions.createComponents("/zul/jfs/agreement_and_criteria/other_criteria_setting_popup.zul", null, map);
		window.setTitle(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.addothercriteriasetting"));
		window.addEventListener("onClose", new EventListener () {
			public void onEvent (Event event) {
				logger.debug("refresh other criteria setting");
			}
		});
		window.doModal();
	}

	public void getData() throws Exception {
		otherCriteriaSettingList = commodityService.getAllCriteriaSetting();

		gridOtherCriteriaSetting.setModel(new SimpleListModel <OtherCriteriaSetting> (otherCriteriaSettingList));

		gridOtherCriteriaSetting.setRowRenderer(new RowRenderer<OtherCriteriaSetting>() {
			public void render(Row row, final OtherCriteriaSetting otherCriteriaSetting, int i) throws Exception {                
				new Label(otherCriteriaSetting.getCommodityTypeCode()).setParent(row);
				new Label(otherCriteriaSetting.getCaMin().toString()).setParent(row);
				new Label(otherCriteriaSetting.getCaMax().toString()).setParent(row);
				new Label(otherCriteriaSetting.getValidFrom()==null ? CONSTANT_UTIL.MINUS : dateUtil.getFormattedCurrentDatePattern10(otherCriteriaSetting.getValidFrom())).setParent(row);
				new Label(otherCriteriaSetting.getValidTo()==null ? CONSTANT_UTIL.MINUS : dateUtil.getFormattedCurrentDatePattern10(otherCriteriaSetting.getValidTo())).setParent(row);

				Div div = new Div();
				Button btnEdit = new Button(null, "/images/icon/edit.png");
				btnEdit.setParent(div);
				btnEdit.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						clearMessage();
						Map<String, Object> args = new HashMap<String, Object>();
						args.put("screenState", "edit");
						args.put("otherCriteriaSetting", otherCriteriaSetting);

						Window window = (Window) Executions.createComponents("/zul/jfs/agreement_and_criteria/other_criteria_setting_popup.zul", null, args);
						window.setTitle(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.updateothercriteriasetting"));
						window.addEventListener("onClose", new EventListener () {
							public void onEvent (Event event) throws Exception {
								logger.debug("refresh product mapping");
								getData();
							}
						});
						window.doModal();
					}
				});
				Button btnDelete = new Button(null, "/images/icon/delete.png");
				btnDelete.setParent(div);
				btnDelete.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationdelete"), MessageResources.getMessageResources("zul.common.label.confirmationdelete"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
							@Override
							public void onEvent(Event e) throws Exception {

								clearMessage();
								if (Messagebox.ON_OK.equals(e.getName())) {
									otherCriteriaSetting.setIsDelete(CONSTANT_UTIL.DEFAULT_YES);
									commodityDAO.update(otherCriteriaSetting);
									getData();
								} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
								} 
							}
						});
					}
				});
				div.setParent(row);
			}
		});
	}

	public void clearMessage() {
		labelSuccessMessage.setValue("");
		labelErrorMessage.setValue("");
	}

	public void save() {
		try {
			getData();

			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.savesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}

	public void delete(Object obj) {
		try {
			getData();

			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deletesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deleteerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}