package com.hci.jfs.composer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.CommodityDAOImpl;
import com.hci.jfs.entity.OtherCriteriaSetting;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;


/**
 *
 * @author Igor
 */
public class OtherCriteriaSettingPopupComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(OtherCriteriaSettingPopupComposer.class.getName());
	
	Window winOtherCriteriaSettingPopup;
	Label labelErrorMessage;
	
	Listbox listboxTypeOfCriteria;
	Decimalbox intboxMinimalValue;
	Decimalbox intboxMaximalValue;
	Datebox dateboxValidFrom;
	Datebox dateboxValidTo;
	
	Label messageTypeOfCriteria;
	Label messageMinimalValue;
	Label messageMaximalValue;
	Label messageValidFrom;
	Label messageValidTo;
	
	private OtherCriteriaSetting criteriaSetting;
	private String screenstate=CONSTANT_UTIL.DEFAULT_COMMA;
	private CommodityDAOImpl commodityDAO = (CommodityDAOImpl) SpringContextHolder.getApplicationContext().getBean("commodityDAO");
	private Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	
	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
		clearMessage();
	}
	
	public void initComponent(){
		try{
			screenstate = (String) Executions.getCurrent().getArg().get("screenState");
			if(screenstate.equals("edit")){
				criteriaSetting = (OtherCriteriaSetting) Executions.getCurrent().getArg().get("otherCriteriaSetting");
				new Listitem(criteriaSetting.getCommodityTypeCode(), criteriaSetting.getCommodityTypeCode()).setParent(listboxTypeOfCriteria);
				listboxTypeOfCriteria.setSelectedIndex(0);
				
				intboxMinimalValue.setValue(criteriaSetting.getCaMin());
				intboxMaximalValue.setValue(criteriaSetting.getCaMax());
								
				if(criteriaSetting.getValidFrom()!=null){
					dateboxValidFrom.setValue(criteriaSetting.getValidFrom());
				}
				
				if(criteriaSetting.getValidTo()!=null){
					dateboxValidTo.setValue(criteriaSetting.getValidTo());
				}
				
			}


		}catch(Exception e){
			showErrorDialog("Product is not found");
			e.printStackTrace();
		}
	}
	
	public void onClick$buttonAdd(Event e) throws InterruptedException {
		clearMessage();
		
		if (checkValidation()) {
			save();
		}
	}
	
	public void onClick$buttonCancel(Event e) throws InterruptedException {
		clearMessage();
		winOtherCriteriaSettingPopup.detach();
	}
	
	public void clearMessage() {
		labelErrorMessage.setValue("");
		messageTypeOfCriteria.setValue("");
		messageMinimalValue.setValue("");
		messageMaximalValue.setValue("");
		messageValidFrom.setValue("");
		messageValidTo.setValue("");
	}

	public boolean checkValidation() {
		boolean result = true;
		
		if (listboxTypeOfCriteria.getSelectedItem() == null) {
			result = false;
			messageTypeOfCriteria.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (intboxMinimalValue.getValue() == null) {
			result = false;
			messageMinimalValue.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}
		
		if (intboxMaximalValue.getValue() == null) {
			result = false;
			messageMaximalValue.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}
		
		if (intboxMinimalValue.getValue() != null && intboxMaximalValue.getValue() != null) {
			if (intboxMinimalValue.getValue().compareTo(intboxMaximalValue.getValue()) > 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.maximalbiggerthanminimal"));
			}
		}
		
		if (dateboxValidFrom.getValue() == null) {
			result = false;
			messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			if (dateboxValidFrom.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantoday"));
			}
		}
		
		if (dateboxValidTo.getValue() == null) {
			result = false;
			messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			if (dateboxValidTo.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.todatebiggerthantoday"));
			}
		}
		
		if (dateboxValidFrom.getValue() != null && dateboxValidTo.getValue() != null) {
			if (dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) == 0 || dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) > 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantodate"));
			}
		}

		return result;
	}
	
	public void save() {
		try {
			winOtherCriteriaSettingPopup.detach();
			UserCredential userCredential = (UserCredential) auth.getPrincipal();
			if(screenstate.equals("edit")){
				criteriaSetting.setCaMax(intboxMaximalValue.getValue());
				criteriaSetting.setCaMin(intboxMinimalValue.getValue());
				criteriaSetting.setValidFrom(dateboxValidFrom.getValue());
				criteriaSetting.setValidTo(dateboxValidTo.getValue());
				criteriaSetting.setUpdatedDate(new Date());
				criteriaSetting.setUpdatedBy(userCredential.getUsername());
				commodityDAO.update(criteriaSetting);
			}
			
			Map<String, Object> args = new HashMap<String, Object>();		
			Events.sendEvent(new Event("onClose", winOtherCriteriaSettingPopup, args));
		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}