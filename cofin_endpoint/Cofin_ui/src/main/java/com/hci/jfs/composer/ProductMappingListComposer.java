package com.hci.jfs.composer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.ProductDAOImpl;
import com.hci.jfs.entity.ProductMapping;
import com.hci.jfs.entity.form.ProductMappingForm;
import com.hci.jfs.services.ProductService;
import com.hci.jfs.util.DateUtil;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;


/**
 *
 * @author Igor
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ProductMappingListComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(ProductMappingListComposer.class.getName());

	private ProductService productService = (ProductService) SpringContextHolder.getApplicationContext().getBean("productService");
	private ProductDAOImpl productDAO = (ProductDAOImpl) SpringContextHolder.getApplicationContext().getBean("productDAO");
	private DateUtil dateUtil = (DateUtil) SpringContextHolder.getApplicationContext().getBean("dateUtil");

	Window winProductMappingList;
	Label labelSuccessMessage;
	Label labelErrorMessage;
	Grid gridProductMapping;

	List<ProductMappingForm> productMappingList;

	private String agreementCode=CONSTANT_UTIL.DEFAULT_EMPTY;

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
		getData();
		clearMessage();
	}

	private void initComponent(){
		try{
			agreementCode = ((String[])param.get("agreementCode"))[0];
		}catch(Exception e){
			showErrorDialog("Agreement is not found");
			e.printStackTrace();
		}
	}

	public void onClick$buttonAdd(Event event) {
		clearMessage();
		Map<String, Object> map = new HashMap<String, Object>();

		Window window = (Window) Executions.createComponents("/zul/jfs/agreement_and_criteria/product_mapping_popup.zul", null, map);
		window.setTitle(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.addproductmapping"));
		window.addEventListener("onClose", new EventListener () {
			public void onEvent (Event event) throws Exception {
				logger.debug("refresh product mapping");
				getData();
			}
		});
		window.doModal();
	}

	public void getData() throws Exception {
		productMappingList = productService.getProductMappingFormByAgreementCode(Long.parseLong(agreementCode));

		gridProductMapping.setModel(new SimpleListModel <ProductMappingForm> (productMappingList));

		gridProductMapping.setRowRenderer(new RowRenderer<ProductMappingForm>() {
			public void render(Row row, final ProductMappingForm product, int i) throws Exception {    
				if(product.getProductHCI()!=null){
					new Label(product.getProductHCI().getNameProduct()).setParent(row);
					new Label(product.getProductHCI().getCodeProduct()).setParent(row);
					new Label(product.getBankProductCode()).setParent(row);
					new Label(product.getIsDefault()).setParent(row);
					new Label(dateUtil.getFormattedCurrentDatePattern10(product.getValidFrom())).setParent(row);
					new Label(dateUtil.getFormattedCurrentDatePattern10(product.getValidTo())).setParent(row);

					Div div = new Div();
					Button btnEdit = new Button(null, "/images/icon/edit.png");
					btnEdit.setParent(div);
					btnEdit.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
						public void onEvent(Event event) throws Exception {
							clearMessage();
							Map<String, Object> args = new HashMap<String, Object>();
							args.put("screenState", "edit");
							args.put("product", product);
							Window window = (Window) Executions.createComponents("/zul/jfs/agreement_and_criteria/product_mapping_popup.zul", null, args);
							window.setTitle(MessageResources.getMessageResources("zul.jfs.agreementandcriteria.label.updateproductmapping"));
							window.addEventListener("onClose", new EventListener () {
								public void onEvent (Event event) throws Exception {
									getData();
								}
							});
							window.doModal();
						}
					});
					Button btnDelete = new Button(null, "/images/icon/delete.png");
					btnDelete.setParent(div);
					btnDelete.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
						public void onEvent(Event event) throws Exception {
							Messagebox.show(MessageResources.getMessageResources("zul.common.label.message.confirmationdelete"), MessageResources.getMessageResources("zul.common.label.confirmationdelete"), Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR, new EventListener<Event>() {
								@Override
								public void onEvent(Event e) throws Exception {
									clearMessage();
									if (Messagebox.ON_OK.equals(e.getName())) {
										ProductMapping mapping = new ProductMapping();
										mapping.setIdProduct(product.getIdProduct());
										mapping.setBankProductCode(product.getBankProductCode());
										mapping.setIsDefault(product.getIsDefault());
										mapping.setIsDelete(CONSTANT_UTIL.DEFAULT_YES);
										mapping.setAgreement(product.getAgreement());
										mapping.setValidFrom(product.getValidFrom());
										mapping.setValidTo(product.getValidTo());
										mapping.setCodeProduct(product.getCodeProduct());
										productDAO.update(mapping);	
										getData();
									} else if(Messagebox.ON_CANCEL.equals(e.getName())) {
									} 
								}
							});
						}
					});
					div.setParent(row);
				}
			}
		});
	}

	public void clearMessage() {
		labelSuccessMessage.setValue("");
		labelErrorMessage.setValue("");
	}

	public void save() {
		try {
			getData();

			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.savesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}

	public void delete(Object obj) {
		try {
			getData();

			labelSuccessMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deletesuccess"));

		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.deleteerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}