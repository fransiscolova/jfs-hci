package com.hci.jfs.composer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.ProductDAOImpl;
import com.hci.jfs.entity.ProductMapping;
import com.hci.jfs.entity.form.ProductMappingForm;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.MessageResources;
import com.hci.jfs.util.SpringContextHolder;


/**
 *
 * @author Igor
 */
public class ProductMappingPopupComposer extends CommonComposer{

	private static final long serialVersionUID = 1L;	
	final static Logger logger = LogManager.getLogger(ProductMappingPopupComposer.class.getName());
	private ProductDAOImpl productDAO = (ProductDAOImpl) SpringContextHolder.getApplicationContext().getBean("productDAO");
	private Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
	Window winProductMappingPopup;
	Label labelErrorMessage;

	Listbox listboxHCIDProductCode;
	Textbox textboxHCIDProductName;
	Textbox textboxPartnerProductName;
	Datebox dateboxValidFrom;
	Datebox dateboxValidTo;
	@Wire
	Checkbox checkboxIsDefault;

	Label messageHCIDProductCode;
	Label messageHCIDProductName;
	Label messagePartnerProductName;
	Label messageValidFrom;
	Label messageValidTo;

	Button buttonAdd;

	private ProductMappingForm form;
	private String screenstate=CONSTANT_UTIL.DEFAULT_COMMA;
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
		clearMessage();
	}

	private void initComponent(){
		try{
			screenstate = (String) Executions.getCurrent().getArg().get("screenState");
			if(screenstate.equals("edit")){
				form = (ProductMappingForm) Executions.getCurrent().getArg().get("product");
				new Listitem(form.getProductHCI().getCodeProduct(), form.getProductHCI().getIdProduct()).setParent(listboxHCIDProductCode);
				listboxHCIDProductCode.setSelectedIndex(0);
				listboxHCIDProductCode.setDisabled(true);
				textboxHCIDProductName.setValue(form.getProductHCI().getNameProduct());
				textboxHCIDProductName.setReadonly(true);				
				textboxPartnerProductName.setValue(form.getBankProductCode());
				textboxPartnerProductName.setReadonly(true);
				
//				List<ProductHCI> listProductHCI = productService.getProductHCIByAgreementCode(form.getAgreement());
//				
//				if(listProductHCI!=null){
//					int index=0,i=0;
//					for (ProductHCI productHCI : listProductHCI) {
//						Listitem item = new Listitem(productHCI.getCodeProduct(), productHCI.getIdProduct());
//						if(form.getProductHCI().getCodeProduct().equals(productHCI.getCodeProduct())){
//							index=i;
//						}
//						i++;
//						item.setParent(listboxHCIDProductCode);
//					}
//					if(listProductHCI.size()>0){
//						listboxHCIDProductCode.setSelectedIndex(index);
//						textboxHCIDProductName.setValue(listProductHCI.get(0).getNameProduct());
//						textboxPartnerProductName.setValue(form.getBankProductCode());
//					}
//				}


				if(form.getValidFrom()!=null){
					dateboxValidFrom.setValue(form.getValidFrom());
				}

				if(form.getValidTo()!=null){
					dateboxValidTo.setValue(form.getValidTo());
				}

				if(form.getIsDefault().equals(CONSTANT_UTIL.DEFAULT_YES)){
					checkboxIsDefault.setChecked(true);
				}				
				buttonAdd.setLabel(MessageResources.getMessageResources("zul.common.label.edit"));
			}


		}catch(Exception e){
			showErrorDialog("Product is not found");
			e.printStackTrace();
		}
	}

	public void onClick$buttonAdd(Event e) throws InterruptedException {
		clearMessage();

		if (checkValidation()) {
			save();
		}
	}

	public void onClick$buttonCancel(Event e) throws InterruptedException {
		clearMessage();
		winProductMappingPopup.detach();
	}

	public void clearMessage() {
		labelErrorMessage.setValue("");
		messageHCIDProductCode.setValue("");
		messageHCIDProductName.setValue("");
		messagePartnerProductName.setValue("");
		messageValidFrom.setValue("");
		messageValidTo.setValue("");
	}

	public boolean checkValidation() {
		boolean result = true;

		if (listboxHCIDProductCode.getSelectedItem() == null) {
			result = false;
			messageHCIDProductCode.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (textboxHCIDProductName.getValue().equals("")) {
			result = false;
			messageHCIDProductName.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (textboxPartnerProductName.getValue().equals("")) {
			result = false;
			messagePartnerProductName.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		}

		if (dateboxValidFrom.getValue() == null) {
			result = false;
			messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
//			if (dateboxValidFrom.getValue().compareTo(new Date()) < 0) {
//				result = false;
//				messageValidFrom.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantoday"));
//			}
		}

		if (dateboxValidTo.getValue() == null) {
			result = false;
			messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.mandatory"));
		} else {
			if (dateboxValidTo.getValue().compareTo(new Date()) < 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.todatebiggerthantoday"));
			}
		}

		if (dateboxValidFrom.getValue() != null && dateboxValidTo.getValue() != null) {
			if (dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) == 0 || dateboxValidFrom.getValue().compareTo(dateboxValidTo.getValue()) > 0) {
				result = false;
				messageValidTo.setValue(MessageResources.getMessageResources("zul.common.label.message.fromdatebiggerthantodate"));
			}
		}

		return result;
	}

	public void save() {
		try {
			UserCredential userCredential = (UserCredential) auth.getPrincipal();
			if(screenstate.equals("edit")){
				ProductMapping mapping = new ProductMapping();
				mapping.setIdProduct(form.getIdProduct());
				mapping.setBankProductCode(form.getBankProductCode());
				mapping.setIsDefault(checkboxIsDefault.isChecked() == Boolean.TRUE ?CONSTANT_UTIL.DEFAULT_YES : CONSTANT_UTIL.DEFAULT_NO);
				mapping.setIsDelete(form.getIsDelete());
				mapping.setAgreement(form.getAgreement());
				mapping.setValidFrom(dateboxValidFrom.getValue());
				mapping.setValidTo(dateboxValidTo.getValue());
				mapping.setCodeProduct(form.getCodeProduct());
				mapping.setUpdatedDate(new Date());
				mapping.setUpdatedBy(userCredential.getUsername());
				productDAO.update(mapping);				
			}
			winProductMappingPopup.detach();

			Map<String, Object> args = new HashMap<String, Object>();		
			Events.sendEvent(new Event("onClose", winProductMappingPopup, args));
		} catch(Exception ex) {
			logger.error(ex);
			labelErrorMessage.setValue(MessageResources.getMessageResources("zul.common.label.message.saveerror").concat(" : ").concat(ex.getMessage()));
		}
	}
}