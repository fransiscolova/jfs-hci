package com.hci.jfs.composer;

import java.io.Serializable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.entity.PublicHoliday;
import com.hci.jfs.services.ProductService;
import com.hci.jfs.util.DateUtil;
import com.hci.jfs.util.SpringContextHolder;

public class PublicHolidaysComposer extends CommonComposer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final static Logger logger = LogManager.getLogger(PublicHolidaysComposer.class.getName());
	private ProductService productService = (ProductService) SpringContextHolder.getApplicationContext().getBean("productService");
	private DateUtil dateUtil = (DateUtil) SpringContextHolder.getApplicationContext().getBean("dateUtil");

	
	List<PublicHoliday> publicHolidays;
	@Wire
	Grid gridPublicHoliday;

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
	}
	
	public void initComponent() throws Exception{
		setupDisplay();
	}
	
	private void setupDisplay() throws Exception{
		publicHolidays = productService.getCurrentPublicHoliday();
		gridPublicHoliday.setModel(new SimpleListModel <PublicHoliday> (publicHolidays));
		gridPublicHoliday.setRowRenderer(new RowRenderer<PublicHoliday>() {
			public void render(Row row, final PublicHoliday holiday, int i) throws Exception {    
				new Label(dateUtil.getFormattedCurrentDatePattern10(holiday.getDateFrom())).setParent(row);
				new Label(dateUtil.getFormattedCurrentDatePattern10(holiday.getDateTo())).setParent(row);
				new Label(dateUtil.getFormattedCurrentDatePattern10(holiday.getDateGenerate())).setParent(row);
				new Label(holiday.getDescription()).setParent(row);
				new Button("Edit").setParent(row);
			}
		});
	}
}
