package com.hci.jfs.composer.batch;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import com.hci.jfs.dao.PartnerDAO;
import com.hci.jfs.dao.impl.AgreementDAOImpl;
import com.hci.jfs.dao.impl.BatchDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.BatchJob;
import com.hci.jfs.entity.BatchJobLog;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.util.SpringContextHolder;

public class BatchProcessComposer extends SelectorComposer<Component> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8669818843176894939L;

	@Wire
	Grid grdBatchProcess;
	
	private BatchDAOImpl batchDao;
	private AgreementDAOImpl agreementDao;
	private PartnerDAO partnerDao;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Wire
	private Listbox lbAgreementCode;
	
	@Wire
	private Textbox txtAgreementName;
	
	@Wire
	private Listbox lbPartnerName;
	
	@Wire
	private Datebox dbDateFrom;
	
	@Wire
	private Datebox dbDateTo;
	
	@Wire
	private Label lblBatchProcessError;
	
	@Wire
	private Listbox lbProcessName;
	
	//Map<String, String> agreementName = new HashMap<String, String>();
	List<String> agreementName = new ArrayList<String>();
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		lblBatchProcessError.setVisible(false);
		batchDao = (BatchDAOImpl) SpringContextHolder.getApplicationContext().getBean("BatchDAO");
		agreementDao = (AgreementDAOImpl) SpringContextHolder.getApplicationContext().getBean("AgreementDAO");
		partnerDao = (PartnerDAO) SpringContextHolder.getApplicationContext().getBean("PartnerDAO");
		
		List<Agreement> agreements = agreementDao.getAllAgreement();
		for(Iterator <Agreement> iterators = agreements.iterator(); iterators.hasNext();) {
			Agreement agr = (Agreement) iterators.next();
			Listitem item = new Listitem();
			item.setLabel(agr.getCode());
			item.setValue(agr.getCode());			
			item.setParent(lbAgreementCode);
			
			agreementName.add(agr.getName());
		}		
		
		List<Partner> partners = partnerDao.getAllPartner();
		for(Iterator <Partner> iterators = partners.iterator(); iterators.hasNext();) {
			Partner part = (Partner) iterators.next();
			Listitem item = new Listitem();
			item.setLabel(part.getName());
			item.setValue(part.getName());
			item.setParent(lbPartnerName);
		}
		
		List<BatchJob> batchJobs = batchDao.getAllBatchJob();
		for(Iterator <BatchJob> iterators = batchJobs.iterator(); iterators.hasNext();) {
			BatchJob batchJ = (BatchJob) iterators.next();
			Listitem item = new Listitem();
			item.setLabel(batchJ.getProcessName());
			item.setValue(batchJ.getProcessName());
			item.setParent(lbProcessName);
		}
	}
	
	@Listen("onClick = #btnBatchProcessSearch")
	public void doQueryRecord() {
		lblBatchProcessError.setVisible(false);
		if(lbPartnerName.getSelectedItem() == null || lbAgreementCode.getSelectedItem() == null || lbProcessName.getSelectedItem() == null || dbDateFrom.getValue().equals("") || dbDateTo.getValue().equals("")) {
			lblBatchProcessError.setValue("Partner name, agreemnt code, process name, processing date from, processing date to has to be specified");
			lblBatchProcessError.setVisible(true);
			return;
		}
		
		String from = sdf.format(dbDateFrom.getValue());
		String to = sdf.format(dbDateTo.getValue());
        List<BatchJobLog> batches = batchDao.getBatchJob(lbPartnerName.getSelectedItem().getValue(), txtAgreementName.getValue(), lbProcessName.getSelectedItem().getValue(), from, to);
        //batches.add(b);
        
        ListModel <BatchJobLog> listModel = new SimpleListModel <BatchJobLog> (batches);
		grdBatchProcess.setModel(listModel);
		grdBatchProcess.setRowRenderer(new RowRenderer<BatchJobLog>() {
			
			@Override
			public void render(Row row, BatchJobLog b, int i) throws Exception {
                new Label(b.getRunDate()).setParent(row);
                new Label(b.getRunTime()).setParent(row);
                new Label(b.getPartner().getId()).setParent(row);
                new Label(b.getAgreement().getCode()).setParent(row);
                new Label(b.getProcess().getCode()).setParent(row);
                new Label(b.getStatus()).setParent(row);
                new Label(b.getReason()).setParent(row);
                new Button("View").setParent(row);
                if(!b.getStatus().equals("Success")) {
                	new Button("Re-Execute").setParent(row);
                } else {
                	new Label("").setParent(row);
                }
			}
		});
	}
	
	@Listen("onSelect = #lbAgreementCode") 
	public void doChangeName() {	
		txtAgreementName.setText(agreementName.get(lbAgreementCode.getSelectedIndex()));
	}
}