package com.hci.jfs.composer.partner;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.PartnerDAOImpl;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.util.InputValidator;
import com.hci.jfs.util.SpringContextHolder;

public class NewPartnerRegistrationComposer extends SelectorComposer<Component> {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(NewPartnerRegistrationComposer.class
            .getName());
    private PartnerDAOImpl partnerDAO;
    private Partner partner;

    @Wire
    private Window newPartnerDialog;
    @Wire
    private Textbox txbPartnerName, txbPartnerAddress, txbPartnerContactPerson,
    txbPartnerPhoneNumber, txbPartnerEmail;
    @Wire
    private Label lblPartnerUPError;

    /**
     * Update data model in screen.
     *
     * @param partner
     */
    private void addModelData(Partner partner) {
        List<Partner> partners = PartnerRegistrationComposer.partners;
        partners.add(partner);
        Collections.sort(partners, (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName()));
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        partnerDAO = (PartnerDAOImpl) SpringContextHolder.getApplicationContext().getBean(
                "PartnerDAO");
        partner = new Partner();
    }

    @Listen("onClick = #btnPartnerClose")
    public void onClose() {
        newPartnerDialog.detach();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Listen("onClick = #btnPartnerSave")
    public void onSave() {
        // validate user screen input
        String errorString = InputValidator.validatePartnerInput(txbPartnerName, txbPartnerAddress,
                txbPartnerContactPerson, txbPartnerPhoneNumber, txbPartnerEmail, null);
        if (errorString != null) {
            lblPartnerUPError.setValue(errorString);
            lblPartnerUPError.setVisible(true);
            LOGGER.info(errorString);
            return;
        }

        Messagebox.show("Are you sure want to create this partner?", "", Messagebox.OK
                | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event evt) throws InterruptedException {
                if (evt.getName().equals("onOK")) {
                    saveData();
                    Events.postEvent(Events.ON_CLOSE, newPartnerDialog, null);
                }
            }
        });
    }

    /**
     * Insert user input {@link Partner} data from screen to database.
     */
    private void saveData() {
        partner.setName(txbPartnerName.getValue());
        partner.setAddress(txbPartnerAddress.getValue());
        partner.setEmail(txbPartnerEmail.getValue());
        partner.setIsDelete(CONSTANT_UTIL.DEFAULT_NO);
        partner.setMainContact(txbPartnerContactPerson.getValue());
        partner.setPhoneNumber(txbPartnerPhoneNumber.getValue());
        partnerDAO.save(partner);
        addModelData(partner);
    }
}