package com.hci.jfs.composer.partner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Window;

import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.PartnerDAO;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.util.SpringContextHolder;

public class PartnerRegistrationComposer extends SelectorComposer<Component> {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(PartnerRegistrationComposer.class
            .getName());
    public static List<Partner> partners = new ArrayList<Partner>();
    private PartnerDAO partnerDAO;
    private EventListener<Event> eventlistener;
    private ListModel<Partner> listModel;

    // Main page
    @Wire
    private Button btnPartnerNew;
    @Wire
    private Grid grdPartnerDetail;
    @Wire
    private Checkbox chPartnerShowAll;

    @SuppressWarnings("rawtypes")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        eventlistener = new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                grdPartnerDetail.setModel(setModelAfterClose());
            }
        };

        Map<String, EventListener> eventBehaviour = new HashMap<String, EventListener>();
        eventBehaviour.put(Events.ON_CLOSE, eventlistener);

        // Get from database
        partnerDAO = (PartnerDAO) SpringContextHolder.getApplicationContext().getBean("PartnerDAO");
        partners = partnerDAO.getAllPartner();
        Collections.sort(partners, (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName()));

        listModel = new SimpleListModel<Partner>(partners);
        grdPartnerDetail.setModel(setModelForActive());
        grdPartnerDetail.setRowRenderer(new RowRenderer<Partner>() {
            @Override
            public void render(Row row, Partner p, int i) throws Exception {
                i++;
                new Label(i + "").setParent(row);
                new Label(p.getId()).setParent(row);
                new Label(p.getName()).setParent(row);
                new Label(p.getAddress()).setParent(row);
                new Label(p.getMainContact()).setParent(row);
                new Label(p.getPhoneNumber()).setParent(row);
                new Label(p.getEmail()).setParent(row);
                Image img = new Image("images/icon/magnifier17.png");
                Map<String, Object> args = new HashMap<String, Object>();
                args.put("idPartner", p.getId());

                img.addEventListener(
                        Events.ON_CLICK,
                        elShowPageGenerator("/zul/jfs/partner/UpdatePartnerRegistration.zul", args,
                                eventBehaviour));
                img.setParent(row);
                if (p.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_YES)) {
                    row.setStyle("background-color:#CCCCCC");
                }
            }
        });

        btnPartnerNew.addEventListener(
                Events.ON_CLICK,
                elShowPageGenerator("/zul/jfs/partner/NewPartnerRegistration.zul", null,
                        eventBehaviour));
    }

    @Listen("onCheck = #chPartnerShowAll")
    public void doShowAll() {
        grdPartnerDetail.setModel(setModelAfterClose());
    }

    @SuppressWarnings("rawtypes")
    public EventListener<Event> elShowPageGenerator(String page, Map<String, Object> args,
            Map<String, EventListener> eventBehaviour) {
        EventListener<Event> events = new EventListener<Event>() {
            @SuppressWarnings("unchecked")
            @Override
            public void onEvent(Event event) throws Exception {
                Window window = (Window) Executions.createComponents(page, null, args);
                for (Entry<String, EventListener> entry : eventBehaviour.entrySet()) {
                    window.addEventListener(entry.getKey(), entry.getValue());
                }
                window.doModal();
            }
        };
        return events;
    }

    private ListModel<Partner> setModelAfterClose() {
        List<Partner> partnerList = new ArrayList<Partner>();
        if (chPartnerShowAll.isChecked()) {
            partnerList = partners;
        } else {
            for (Partner partner : partners) {
                if (partner.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO)) {
                    partnerList.add(partner);
                }
            }
        }

        listModel = new SimpleListModel<Partner>(partnerList);
        return listModel;
    }

    private ListModel<Partner> setModelForActive() {
        List<Partner> partnerList = new ArrayList<Partner>();
        for (Partner partner : partners) {
            if (partner.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO)) {
                partnerList.add(partner);
            }
        }
        listModel = new SimpleListModel<Partner>(partnerList);
        return listModel;
    }
}