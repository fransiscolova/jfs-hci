package com.hci.jfs.composer.partner;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.PartnerDAOImpl;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.util.InputValidator;
import com.hci.jfs.util.SpringContextHolder;

public class UpdatePartnerRegistrationComposer extends SelectorComposer<Component> {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager
            .getLogger(UpdatePartnerRegistrationComposer.class.getName());
    private PartnerDAOImpl partnerDAO;
    private String partnerId;
    private Partner partner;

    @Wire
    private Window updateDialog;
    @Wire
    private Textbox txbPartnerName, txbPartnerAddress, txbPartnerContactPerson,
            txbPartnerPhoneNumber, txbPartnerEmail;
    @Wire
    private Label lbPartnerId, lblPartnerUPError;
    @Wire
    private Button btnPartnerActiveStatus;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        partnerId = (String) Executions.getCurrent().getArg().get("idPartner");
        partnerDAO = (PartnerDAOImpl) SpringContextHolder.getApplicationContext().getBean(
                "PartnerDAO");
        partner = partnerDAO.getPartnerById(partnerId);

        setActiveButton(partner.getIsDelete());

        lbPartnerId.setValue(partner.getId());
        txbPartnerName.setValue(partner.getName());
        txbPartnerAddress.setValue(partner.getAddress());
        txbPartnerContactPerson.setValue(partner.getMainContact());
        txbPartnerPhoneNumber.setValue(partner.getPhoneNumber());
        txbPartnerEmail.setValue(partner.getEmail());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Listen("onClick = #btnPartnerActiveStatus")
    public void onChangeActiveStatus() {
        if (!partner.getAgreements().isEmpty()) {
            Messagebox
                    .show("Selected partner cannot be dactivated because it still have at least one active agreement");
            return;
        }

        if (partner.getIsDelete().equals(CONSTANT_UTIL.DEFAULT_NO)) {
            Messagebox.show("Are you sure want to deactivate selected partner?", "", Messagebox.OK
                    | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
                public void onEvent(Event evt) throws InterruptedException {
                    if (evt.getName().equals("onOK")) {
                        updatePartnerStatus(CONSTANT_UTIL.DEFAULT_YES);
                        Messagebox.show("Partner is successfully deactivated");
                        Events.postEvent(Events.ON_CLOSE, updateDialog, null);
                    } else {
                        return;
                    }
                }
            });
        } else {
            Messagebox.show("Are you sure want to re-activate selected partner?", "", Messagebox.OK
                    | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
                public void onEvent(Event evt) throws InterruptedException {
                    if (evt.getName().equals("onOK")) {
                        updatePartnerStatus(CONSTANT_UTIL.DEFAULT_NO);
                        Messagebox.show("Partner is successfully re-activated");
                        Events.postEvent(Events.ON_CLOSE, updateDialog, null);
                    } else {
                        return;
                    }
                }
            });
        }
    }

    @Listen("onClick = #btnPartnerClose")
    public void onClose() {
        updateDialog.detach();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Listen("onClick = #btnPartnerSave")
    public void onSave() {
        // validate user screen input
        String errorString = InputValidator.validatePartnerInput(txbPartnerName, txbPartnerAddress,
                txbPartnerContactPerson, txbPartnerPhoneNumber, txbPartnerEmail, lbPartnerId);
        if (errorString != null) {
            lblPartnerUPError.setValue(errorString);
            lblPartnerUPError.setVisible(true);
            LOGGER.info(errorString);
            return;
        }

        Messagebox.show("Are you sure want to update this partner's information?", "",
                Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event evt) throws InterruptedException {
                        if (evt.getName().equals("onOK")) {
                            updatePartnerData();
                            Events.postEvent(Events.ON_CLOSE, updateDialog, null);
                        }
                    }
                });
    }

    /**
     * Change label either 'Reactive' or 'Deactive' depends of {@link Partner} is deleted status.
     *
     * @param isDeletedFlag
     */
    private void setActiveButton(String isDeletedFlag) {
        if (isDeletedFlag.equals(CONSTANT_UTIL.DEFAULT_YES)) {
            btnPartnerActiveStatus.setLabel(Labels.getLabel("zul.jfs.partner.label.reactivate"));
            setReadOnlyTextbox(true);
        } else {
            btnPartnerActiveStatus.setLabel(Labels.getLabel("zul.jfs.partner.label.deactivate"));
            setReadOnlyTextbox(false);
        }
    }

    /**
     * Set text box to read-only when {@link Partner} is deleted.
     *
     * @param status
     */
    private void setReadOnlyTextbox(boolean status) {
        txbPartnerName.setReadonly(status);
        txbPartnerAddress.setReadonly(status);
        txbPartnerContactPerson.setReadonly(status);
        txbPartnerPhoneNumber.setReadonly(status);
        txbPartnerEmail.setReadonly(status);
    }

    /**
     * Update data model in screen.
     *
     * @param partner
     */
    private void updateModelData(Partner partner) {
        List<Partner> partners = PartnerRegistrationComposer.partners;
        for (Partner partnerz : partners) {
            if (partnerz.getId().equals(partner.getId())) {
                partnerz.setName(partner.getName());
                partnerz.setAddress(partner.getAddress());
                partnerz.setMainContact(partner.getMainContact());
                partnerz.setPhoneNumber(partner.getPhoneNumber());
                partnerz.setEmail(partner.getEmail());
                partnerz.setIsDelete(partner.getIsDelete());
            }
        }
        Collections.sort(partners, (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName()));
    }

    /**
     * Update user input {@link Partner} data from screen to database.
     */
    private void updatePartnerData() {
        partner.setName(txbPartnerName.getValue());
        partner.setAddress(txbPartnerAddress.getValue());
        partner.setEmail(txbPartnerEmail.getValue());
        partner.setMainContact(txbPartnerContactPerson.getValue());
        partner.setPhoneNumber(txbPartnerPhoneNumber.getValue());
        partnerDAO.update(partner);
        updateModelData(partner);
    }

    /**
     * Update {@link Partner} status to be 'Active' or 'Deleted'.
     *
     * @param isDeleted
     */
    private void updatePartnerStatus(String isDeleted) {
        partner.setIsDelete(isDeleted);
        partnerDAO.update(partner);
        updateModelData(partner);
    }
}