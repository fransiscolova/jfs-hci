package com.hci.jfs.composer.usermanagement;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Vlayout;

import com.hci.jfs.CommonComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.constant.ROLE;
import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.Role;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.UserManagementService;
import com.hci.jfs.util.SpringContextHolder;

public class UserManagementComposer extends CommonComposer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final static Logger logger = LogManager.getLogger(UserManagementComposer.class.getName());

	private UserManagementService userManagementService;

	@Wire
	private Listbox lbUser;

	@Wire
	private Vlayout vLayoutRoles;

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		userManagementService = (UserManagementService) SpringContextHolder.getApplicationContext().getBean("userManagementService");
		init();

	}

	private void init() throws ApplicationException{
		loadEmployee();
	}

	private void loadEmployee() throws ApplicationException{
		List<Employee> listEmployee = userManagementService.getAllEmployee();

		Listitem listitem = new Listitem();
		listitem.setId("00");
		listitem.setLabel("-- Choose New User --");
		listitem.setParent(lbUser);

		String employeeFormatted;
		for (Employee employee : listEmployee) {
			employeeFormatted = employee.getTextEmailAddress().split("@")[0];
			
			listitem = new Listitem();
			listitem.setId(employeeFormatted);
			listitem.setValue(employeeFormatted);
			listitem.setLabel(employeeFormatted);
			listitem.setParent(lbUser);
		}
		lbUser.setSelectedIndex(0);
	}
	
	private void loadRoles(String username) throws ApplicationException{
		vLayoutRoles.getChildren().clear();

		List<Role> myRoles = userManagementService.getRolesByUserName(username);

		for(ROLE role : ROLE.values()){
			Checkbox checkbox = new Checkbox(role.name());
			for (Role roleUser : myRoles) {
				if(roleUser.getRole().equals(role.name())){
					checkbox.setChecked(true);
					break;
				}
			}
			checkbox.setParent(vLayoutRoles);
		}	
	}

	public void onSelect$lbUser() throws ApplicationException{
		if (lbUser.getSelectedItem().getId() != "00"){
			loadRoles(lbUser.getSelectedItem().getValue());
		}else{
			vLayoutRoles.getChildren().clear();
		}
	}

	public void onClick$btnChange() {		
		showConfirmDialog("Are you sure?", null);
	}

	@Override
	protected void confirmDialogDoYes(Map<String, Object> args) {
		List<Checkbox> cbRoles = vLayoutRoles.getChildren();
		List<Role> newRoles = new ArrayList<Role>();
		String selectedUsername = lbUser.getSelectedItem().getValue();
		
		for (Checkbox checkbox : cbRoles) {
			if(checkbox.isChecked()) {
				Role role = new Role();
				role.setRole(checkbox.getLabel());
				role.setUsername(selectedUsername);
				newRoles.add(role);
			}
		}
		try {
			userManagementService.deleteRolesByUserName(selectedUsername);
			userManagementService.insertRolesByUsername(newRoles);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
