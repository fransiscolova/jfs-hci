package com.hci.jfs.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hci.jfs.constant.CONSTANT_REPORT;

public class InitConfiguration implements ServletContextListener {

	final static Logger LOGGER = LogManager.getLogger(InitConfiguration.class.getName());

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		LOGGER.info("Init Configuration");
		LOGGER.info("PATH");
		LOGGER.info("JFS");
		LOGGER.info("Contract Registraion Disbursement Standard Easy 10 Product "+CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT);
		LOGGER.info("Contract Registraion Disbursement Standard Easy 10 Product Sub "+CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT_SUB_REPORT);
		LOGGER.info("Contract Registraion Disbursement MPF Product "+CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT);
		LOGGER.info("Contract Registraion Disbursement MPF Product Sub "+CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT_SUB_REPORT);
		LOGGER.info("Contract Registraion Disbursement ZERO Product "+CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT);
		LOGGER.info("Contract Registraion Disbursement ZERO Product Sub "+CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT_SUB_REPORT);
		LOGGER.info("Contract Payment Reversal "+CONSTANT_REPORT.PAYMENT_REVERSAL);
		LOGGER.info("Contract Payment Reversal Sub Report "+CONSTANT_REPORT.PAYMENT_REVERSAL_SUB_REPORT);
		LOGGER.info("Contract Payment Regular "+CONSTANT_REPORT.PAYMENT_REGULAR);
		LOGGER.info("Contract Payment Regular Sub Report "+CONSTANT_REPORT.PAYMENT_REGULAR_SUB_REPORT);
		LOGGER.info("Contract Payment Gift "+CONSTANT_REPORT.PAYMENT_GIFT);
		LOGGER.info("Contract Payment Gift Sub Report "+CONSTANT_REPORT.PAYMENT_GIFT_SUB_REPORT);
		LOGGER.info("Contract Payment Small Under Payment "+CONSTANT_REPORT.PAYMENT_SMALL_UNDERPAYMENT);
		LOGGER.info("Contract Payment Small Under Payment "+CONSTANT_REPORT.PAYMENT_SMALL_UNDERPAYMENT_SUB_REPORT);
		LOGGER.info("Contract Payment Last Payment "+CONSTANT_REPORT.PAYMENT_LAST_PAYMENT);
		LOGGER.info("Contract Payment Small Under Payment "+CONSTANT_REPORT.PAYMENT_LAST_PAYMENT_SUB_REPORT);
		LOGGER.info("Contract Payment Early Termination Within 15 Days "+CONSTANT_REPORT.EARLY_TERMINATION_WITHIN_15_DAYS);
		LOGGER.info("Contract Payment Early Termination Within 15 Days Sub Report"+CONSTANT_REPORT.EARLY_TERMINATION_WITHIN_15_DAYS_SUB_REPORT);
		LOGGER.info("Contract Payment Early Termination After 15 Days "+CONSTANT_REPORT.EARLY_TERMINATION_AFTER_15_DAYS);
		LOGGER.info("Contract Payment Early Termination After 15 Days Sub Report"+CONSTANT_REPORT.EARLY_TERMINATION_AFTER_15_DAYS_SUB_REPORT);
		LOGGER.info("Contract Payment Write Off Recovery Payment "+CONSTANT_REPORT.WRITE_OFF_RECOVERY_PAYMENT);
		LOGGER.info("Contract Payment Write Off Recovery Payment Sub Report"+CONSTANT_REPORT.WRITE_OFF_RECOVERY_PAYMENT_SUB_REPORT);
		LOGGER.info("Contract Payment Write Off Contract Registration Payment "+CONSTANT_REPORT.WRITE_OFF_CONTRACT_REGISTRATION);
		LOGGER.info("Contract Payment Write Off Contract Registration Payment Sub Report"+CONSTANT_REPORT.WRITE_OFF_CONTRACT_REGISTRATION_SUB_REPORT);
		LOGGER.info("End Init Configuration");
	}
}
