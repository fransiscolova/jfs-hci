package com.hci.jfs.constant;

public class CONSTANT_DATE {
	public static final String DATE_FORMAT_PATTERN_1 = "yyyyMMdd";
	public static final String DATE_FORMAT_PATTERN_2 = "yyyy-MM-dd";
	public static final String DATE_FORMAT_PATTERN_3 = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_PATTERN_4 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String DATE_FORMAT_PATTERN_5 = "yyyyMMddHHmmss";
	public static final String DATE_FORMAT_PATTERN_6 = "yyyy-MM-dd_HHMMSS";
	public static final String DATE_FORMAT_PATTERN_7 = "YYYYMMdd";
	public static final String DATE_FORMAT_PATTERN_8 = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
	public static final String DATE_FORMAT_PATTERN_9 = "yyyy/MM/dd";
	public static final String DATE_FORMAT_PATTERN_10 = "dd/MM/yyyy";
	public static final String DATE_FORMAT_PATTERN_11 = "dd MMMM yyyy";
	public static final String DATE_FORMAT_PATTERN_12 = "ddMMyyyy";
	public static final String DATE_FORMAT_PATTERN_13 = "YYYY-MM-DD";
	public static final String DATE_FORMAT_PATTERN_14 = "ddMMyyHHmmss";
	public static final String DATE_FORMAT_PATTERN_15 = "dd-MM-yyyy";
	public static final String DATE_FORMAT_PATTERN_16 = "dd-MMM-yyyy";
	public static final String DATE_FORMAT_PATTERN_17 = "dd-MM-yyyy HH:mm:ss";
}
