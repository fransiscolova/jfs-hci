package com.hci.jfs.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CONSTANT_REPORT {

	// Production Path
	public static String CONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT;
	public static String CONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT_SUB_REPORT;

	public static String CONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT;
	public static String CONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT_SUB_REPORT;

	public static String CONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT;
	public static String CONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT_SUB_REPORT;

	public static String PAYMENT_REVERSAL;
	public static String PAYMENT_REVERSAL_SUB_REPORT;

	public static String PAYMENT_REGULAR;
	public static String PAYMENT_REGULAR_SUB_REPORT;

	public static String PAYMENT_GIFT;
	public static String PAYMENT_GIFT_SUB_REPORT;

	public static String PAYMENT_SMALL_UNDERPAYMENT;
	public static String PAYMENT_SMALL_UNDERPAYMENT_SUB_REPORT;

	public static String PAYMENT_LAST_PAYMENT;
	public static String PAYMENT_LAST_PAYMENT_SUB_REPORT;

	public static String EARLY_TERMINATION_WITHIN_15_DAYS;
	public static String EARLY_TERMINATION_WITHIN_15_DAYS_SUB_REPORT;

	public static String EARLY_TERMINATION_AFTER_15_DAYS;
	public static String EARLY_TERMINATION_AFTER_15_DAYS_SUB_REPORT;

	public static String WRITE_OFF_RECOVERY_PAYMENT;
	public static String WRITE_OFF_RECOVERY_PAYMENT_SUB_REPORT;

	public static String WRITE_OFF_CONTRACT_REGISTRATION;
	public static String WRITE_OFF_CONTRACT_REGISTRATION_SUB_REPORT;


	public static String CLAWBACK_MPF;
	public static String CLAWBACK_MPF_SUB_REPORT;

	public static String CLAWBACK_REGISTRATION;
	public static String CLAWBACK_REGISTRATION_SUB_REPORT;

	@Value("${path.contract.registration.disbursement.standard.easy.product.report}")
	public void setCONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT(
			String cONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT) {
		CONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT = cONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT;
	}
	@Value("${path.contract.registration.disbursement.standard.easy.product.report.sub}")
	public void setCONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT_SUB_REPORT(
			String cONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT_SUB_REPORT) {
		CONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT_SUB_REPORT = cONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT_SUB_REPORT;
	}
	@Value("${path.contract.registration.disbursement.mpf.product.report}")
	public void setCONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT(
			String cONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT) {
		CONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT = cONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT;
	}
	@Value("${path.contract.registration.disbursement.mpf.product.report.sub}")
	public void setCONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT_SUB_REPORT(
			String cONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT_SUB_REPORT) {
		CONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT_SUB_REPORT = cONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT_SUB_REPORT;
	}
	@Value("${path.contract.registration.disbursement.zero.product.report.sub}")
	public void setCONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT(
			String cONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT) {
		CONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT = cONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT;
	}
	@Value("${path.contract.registration.disbursement.zero.product.report.sub}")
	public void setCONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT_SUB_REPORT(
			String cONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT_SUB_REPORT) {
		CONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT_SUB_REPORT = cONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT_SUB_REPORT;
	}
	@Value("${path.payment.reversal.report}")
	public void setPAYMENT_REVERSAL(String pAYMENT_REVERSAL) {
		PAYMENT_REVERSAL = pAYMENT_REVERSAL;
	}
	@Value("${path.payment.reversal.report.sub}")
	public void setPAYMENT_REVERSAL_SUB_REPORT(
			String pAYMENT_REVERSAL_SUB_REPORT) {
		PAYMENT_REVERSAL_SUB_REPORT = pAYMENT_REVERSAL_SUB_REPORT;
	}
	@Value("${path.payment.regular.report}")
	public void setPAYMENT_REGULAR(String pAYMENT_REGULAR) {
		PAYMENT_REGULAR = pAYMENT_REGULAR;
	}
	@Value("${path.payment.regular.report.sub}")
	public void setPAYMENT_REGULAR_SUB_REPORT(
			String pAYMENT_REGULAR_SUB_REPORT) {
		PAYMENT_REGULAR_SUB_REPORT = pAYMENT_REGULAR_SUB_REPORT;
	}
	@Value("${path.payment.gift.report}")
	public void setPAYMENT_GIFT(String pAYMENT_GIFT) {
		PAYMENT_GIFT = pAYMENT_GIFT;
	}
	@Value("${path.payment.gift.report.sub}")
	public void setPAYMENT_GIFT_SUB_REPORT(String pAYMENT_GIFT_SUB_REPORT) {
		PAYMENT_GIFT_SUB_REPORT = pAYMENT_GIFT_SUB_REPORT;
	}
	@Value("${path.payment.small.underpayment.report}")
	public void setPAYMENT_SMALL_UNDERPAYMENT(
			String pAYMENT_SMALL_UNDERPAYMENT) {
		PAYMENT_SMALL_UNDERPAYMENT = pAYMENT_SMALL_UNDERPAYMENT;
	}
	@Value("${path.payment.small.underpayment.report.sub}")
	public void setPAYMENT_SMALL_UNDERPAYMENT_SUB_REPORT(
			String pAYMENT_SMALL_UNDERPAYMENT_SUB_REPORT) {
		PAYMENT_SMALL_UNDERPAYMENT_SUB_REPORT = pAYMENT_SMALL_UNDERPAYMENT_SUB_REPORT;
	}
	@Value("${path.payment.last.report}")
	public void setPAYMENT_LAST_PAYMENT(String pAYMENT_LAST_PAYMENT) {
		PAYMENT_LAST_PAYMENT = pAYMENT_LAST_PAYMENT;
	}
	@Value("${path.payment.last.report.sub}")
	public void setPAYMENT_LAST_PAYMENT_SUB_REPORT(
			String pAYMENT_LAST_PAYMENT_SUB_REPORT) {
		PAYMENT_LAST_PAYMENT_SUB_REPORT = pAYMENT_LAST_PAYMENT_SUB_REPORT;
	}
	@Value("${path.payment.early.termination.within.15.days.report}")
	public void setEARLY_TERMINATION_WITHIN_15_DAYS(
			String eARLY_TERMINATION_WITHIN_15_DAYS) {
		EARLY_TERMINATION_WITHIN_15_DAYS = eARLY_TERMINATION_WITHIN_15_DAYS;
	}
	@Value("${path.payment.early.termination.within.15.days.report.sub}")
	public void setEARLY_TERMINATION_WITHIN_15_DAYS_SUB_REPORT(
			String eARLY_TERMINATION_WITHIN_15_DAYS_SUB_REPORT) {
		EARLY_TERMINATION_WITHIN_15_DAYS_SUB_REPORT = eARLY_TERMINATION_WITHIN_15_DAYS_SUB_REPORT;
	}
	@Value("${path.contract.registration.disbursement.mpf.product.report.sub}")
	public void setEARLY_TERMINATION_AFTER_15_DAYS(
			String eARLY_TERMINATION_AFTER_15_DAYS) {
		EARLY_TERMINATION_AFTER_15_DAYS = eARLY_TERMINATION_AFTER_15_DAYS;
	}
	@Value("${path.payment.early.termination.after.15.days.report}")
	public void setEARLY_TERMINATION_AFTER_15_DAYS_SUB_REPORT(
			String eARLY_TERMINATION_AFTER_15_DAYS_SUB_REPORT) {
		EARLY_TERMINATION_AFTER_15_DAYS_SUB_REPORT = eARLY_TERMINATION_AFTER_15_DAYS_SUB_REPORT;
	}
	@Value("${path.payment.write.off.recovery.payment.report}")
	public void setWRITE_OFF_RECOVERY_PAYMENT(
			String wRITE_OFF_RECOVERY_PAYMENT) {
		WRITE_OFF_RECOVERY_PAYMENT = wRITE_OFF_RECOVERY_PAYMENT;
	}
	@Value("${path.payment.write.off.recovery.payment.report.sub}")
	public void setWRITE_OFF_RECOVERY_PAYMENT_SUB_REPORT(
			String wRITE_OFF_RECOVERY_PAYMENT_SUB_REPORT) {
		WRITE_OFF_RECOVERY_PAYMENT_SUB_REPORT = wRITE_OFF_RECOVERY_PAYMENT_SUB_REPORT;
	}
	@Value("${path.payment.write.off.contract.registration.report}")
	public void setWRITE_OFF_CONTRACT_REGISTRATION(
			String wRITE_OFF_CONTRACT_REGISTRATION) {
		WRITE_OFF_CONTRACT_REGISTRATION = wRITE_OFF_CONTRACT_REGISTRATION;
	}
	@Value("${path.payment.write.off.contract.registration.report.sub}")
	public void setWRITE_OFF_CONTRACT_REGISTRATION_SUB_REPORT(
			String wRITE_OFF_CONTRACT_REGISTRATION_SUB_REPORT) {
		WRITE_OFF_CONTRACT_REGISTRATION_SUB_REPORT = wRITE_OFF_CONTRACT_REGISTRATION_SUB_REPORT;
	}
	@Value("${path.payment.clawback.mpf.report}")
	public void setCLAWBACK_MPF(String cLAWBACK_MPF) {
		CLAWBACK_MPF = cLAWBACK_MPF;
	}
	@Value("${path.payment.clawback.mpf.report.sub}")
	public void setCLAWBACK_MPF_SUB_REPORT(String cLAWBACK_MPF_SUB_REPORT) {
		CLAWBACK_MPF_SUB_REPORT = cLAWBACK_MPF_SUB_REPORT;
	}
	@Value("${path.payment.clawback.registration.report}")
	public void setCLAWBACK_REGISTRATION(String cLAWBACK_REGISTRATION) {
		CLAWBACK_REGISTRATION = cLAWBACK_REGISTRATION;
	}
	@Value("${path.payment.clawback.registration.report.sub}")
	public void setCLAWBACK_REGISTRATION_SUB_REPORT(
			String cLAWBACK_REGISTRATION_SUB_REPORT) {
		CLAWBACK_REGISTRATION_SUB_REPORT = cLAWBACK_REGISTRATION_SUB_REPORT;
	}
	
	//Filename
	public static final String CONTRACT_REGISTRATION_FILENAME = "Application Letter (Disbursement) ";
	public static final String CONTRACT_REGISTRATION_ZERO_PROMO_FILENAME = "Application Letter (Disbursement)Zero Promo_";
	public static final String REGULAR_PAYMENT_FILENAME = "Recapitulation of Regular Installment Payment Data JFS ";
	public static final String REVERSAL_PAYMENT_FILENAME = "Permohonan Pengembalian Pembayaran ";
	public static final String GIFT_PAYMENT_FILENAME = "Application For Gift Installment ";
	public static final String SMALL_UNDER_PAYMENT_FILENAME = "Application For Small Underpayment ";
	public static final String LAST_PAYMENT_FILENAME = "Rekapitulasi Data Pembayaran Angsuran Last Payment ";
	public static final String ET_WITHIN_FILENAME = "ET Within 14 days ";
	public static final String ET_AFTER_FILENAME = "ET After 14 days_";
	public static final String WRITE_OFF_RECOVERY_FILENAME="Pemberitahuan Pengembalian Hapus Buku_";
}
