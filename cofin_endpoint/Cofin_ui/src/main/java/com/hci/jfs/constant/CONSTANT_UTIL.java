package com.hci.jfs.constant;

public class CONSTANT_UTIL {
    public static final String DEFAULT_YES = "Y";
    public static final String DEFAULT_DOT = ".";
    public static final String DEFAULT_NO = "N";
    public static final String DEFAULT_EMPTY = "";
    public static final String DEFAULT_SEPARATOR = "|";
    public static final String DEFAULT_HASH_ALGORITHM = "SHA-1";
    public static final String DEFAULT_UNDERSCORE = "_";
    public static final String DEFAULT_DASH = " ";
    public static final String DEFAULT_COMMA = ",";
    public static final String DEFAULT_BACKSLASH = "\\";
    public static final String DEFAULT_SLASH = "//";
    public static final String DEFAULT_ATSIGN_CHARACTER = "@";
    public static final String DEFAULT_ENTER_CHARACTER = "\n";
    public static final String CONSTANT_SFTP = "sftp";
    public static final String WILDCARD = "%";
    public static final String MINUS = "-";
    public static final String SEMICOLON = ";";

    // Initial Value
    public static final String NUMBER_0 = "0";

    public static final String CLAUSE_EQUAL = "1";
    public static final String CLAUSE_LIKE = "2";
    public static final String CLAUSE_SQL_QUERY = "3";

    public static final String FILE_TYPE_UPLOAD_CONFIRMATION = "1";
    public static final String FILE_TYPE_UPLOAD_ALLOCATION = "2";

    public static final String APPLICATION_PROPERTIES_JFS_KEY = "jfs_payment.properties";
    public static final String APPLICATION_PROPERTIES_JFS_PATH_GENERATE_1 = "file.path.generate.jobs.jfs";

    // JFS Process Type
    public static final String JFS_CR_REGULAR = "CRD_STANDARD";
    public static final String JFS_CR_MPF = "CRD_MPF";
    public static final String JFS_CR_ZERO = "CRD_ZERO";
    public static final String UNKNOWN = "XNA";
    public static final String ELIGIBLE = "ELIGIBLE";

    // Type File
    public static final String TYPE_PDF = ".pdf";
    public static final String TYPE_XLS = ".xls";
    public static final String TYPE_CSV = ".csv";
    public static final String TYPE_TXT_AES = ".txt.aes";
    public static final String TYPE_TXT = ".txt";

    // Properties filename
    public static final String JFS_PROPERTIES = "jfs_payment.properties";

    // Regex
    public static final String REGEX_APHANUMERIC = "[^\\p{L}\\p{Nd}]+";

    // Page
    public static final int MAX_ROW = 100;
    public static final int MEDIUM_ROW = 50;

    public static final String WS_TARGETNAMESPACE = "http://homecredit.net/homerselect/ws/financialpartnership/partnership/v1";
}
