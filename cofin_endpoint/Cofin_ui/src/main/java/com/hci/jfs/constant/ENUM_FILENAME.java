package com.hci.jfs.constant;

public enum ENUM_FILENAME {
	// Contract & registration Regular
	SLIK_ALL,
	PENGURUS_ALL,
	BARANG_ALL,
	KONTRAK_ALL,
	ENDUSER_ALL,
	// Contract & registration Regular MPF
	SLIK_FF,
	PENGURUS_FF,
	BARANG_FF,
	KONTRAK_FF,
	ENDUSER_FF,
	// Contract & registration Regular Zero
	SLIK_Z,
	PENGURUS_Z,
	BARANG_Z,
	KONTRAK_Z,
	ENDUSER_Z,
	
	HCI_WRITEOFF,
	JFS_ALL_EXPORT,
	JFS_ALL_OPT,
	JFS_ET_after_15days,
	JFS_ET_within_15days,
	JFS_EXPORT_FF,
	JFS_EXPORT_Z,
	JFS_HCI_BRANCH,
	
	// Payment Regular
	JFS_PAYMENT,
	JFS_PAYMENT_DETAIL,
	JFS_PAYMENT_DETAIL1,
	
	// Payment Gift
	JFS_PAYMENT_GIFT,
	JFS_PAYMENT_G_DETAIL,
	JFS_PAYMENT_G_DETAIL1,
	
	
}
