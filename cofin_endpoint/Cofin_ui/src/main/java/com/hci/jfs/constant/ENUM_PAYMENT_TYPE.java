package com.hci.jfs.constant;

public enum ENUM_PAYMENT_TYPE {
	// Regular
	PEMB_ANGSURAN,
	// Reversal
	REVERSAL,
	// ET After
	PELUNASAN,
	// Gift
	GIFT_INST,
	// Small Under Payment
	UNDERPAYS,
	//Last Payment
	LUNAS
}
