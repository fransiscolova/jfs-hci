package com.hci.jfs.constant;

public enum ROLE {
	ROLE_USER, 
	ROLE_ADMIN, 
	ROLE_IT, 
	ROLE_JFS
}
