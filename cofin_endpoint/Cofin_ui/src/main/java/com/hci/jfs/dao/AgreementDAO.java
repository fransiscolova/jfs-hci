package com.hci.jfs.dao;

import java.util.List;

import com.hci.jfs.entity.Agreement;

public interface AgreementDAO {

	public List<Agreement> getAllAgreement();
	
	public List<Agreement> getAgreement();
	
	public Agreement getAgreementById(String id);
	
	public Agreement getAgreementByCode(String code);
	
	public void batchAgreement();
	
	public List<String> getAgreementNames();

	public List<Agreement> getAgreementByPartnerId(String partnerId) throws Exception;
}
