package com.hci.jfs.dao;

import java.util.List;

import com.hci.jfs.entity.Batch;
import com.hci.jfs.entity.BatchJob;
import com.hci.jfs.entity.BatchJobLog;

public interface BatchDAO {

	public List<Batch> getAllBatch();
	
	public Batch getBatchById(String jobId);
	
	public List<BatchJobLog> getBatchJob(String partner, String agreementName, String processName, String processDateFrom, String processDateTo);
	//public List<BatchJobLog> getBatchJob(String partner, String agreementName, String processName);
	
	public List<BatchJob> getAllBatchJob();
	
	public BatchJobLog getBatchJobByPartnerByAgreementAndProcessCode(String partnerCode,String agreementCode,String processCode,String runDate) throws Exception;
	
	public BatchJobLog getBatchJobByPartnerByAgreementAndProcessCode(String partnerCode,String agreementCode,String processCode,String runDate,String type) throws Exception;
}
