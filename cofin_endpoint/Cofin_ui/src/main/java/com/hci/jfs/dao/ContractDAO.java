package com.hci.jfs.dao;

import com.hci.jfs.entity.Contract;
import com.hci.jfs.security.ApplicationException;

public interface ContractDAO {
	public Contract getContractByAgreementType(String contactNumber,String agreementCode) throws ApplicationException;
	public Contract getContractByContractCode(String contactNumber);
}
