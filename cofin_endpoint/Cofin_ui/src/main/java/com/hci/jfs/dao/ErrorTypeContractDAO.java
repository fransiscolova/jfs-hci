package com.hci.jfs.dao;

import java.util.List;

import com.hci.jfs.entity.ErrorTypeContract;
import com.hci.jfs.security.ApplicationException;

public interface ErrorTypeContractDAO {
	public List<ErrorTypeContract> getAllErrorTypeContract() throws ApplicationException;

	public void insertErrorTypeContract(ErrorTypeContract errorTypeContract) throws ApplicationException;
}
