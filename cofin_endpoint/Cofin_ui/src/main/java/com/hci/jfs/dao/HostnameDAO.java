package com.hci.jfs.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.hci.jfs.entity.Hostname;

@Transactional
public interface HostnameDAO {

	public List<Hostname> selectAllHostname();
	public List<Hostname> searchHostname (HashMap<String, Object> param);
	public Hostname selectById (Short id);	
	public int updateHostnamePointer(Hostname hostname);	
}