package com.hci.jfs.dao;

import java.util.List;

import com.hci.jfs.entity.ErrorContractMonitoring;
import com.hci.jfs.security.ApplicationException;

public interface MonitoringContractErrorDAO {
	public void insert(List<ErrorContractMonitoring> list) throws ApplicationException;
}
