package com.hci.jfs.dao;

import java.util.List;

import com.hci.jfs.entity.MyBatch;

public interface MyBatchLogDAO {

	public List<MyBatch> getAllMyBatch();
	
	public MyBatch getMyBatchById(String jobId);
}
