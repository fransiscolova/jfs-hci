package com.hci.jfs.dao;

import java.util.List;

import com.hci.jfs.entity.Partner;

public interface PartnerDAO {
    public List<Partner> getAllPartner();
    public Partner getPartnerByContractNumber(String contractNumber);
    public Partner getPartnerById(String partnerId);
}
