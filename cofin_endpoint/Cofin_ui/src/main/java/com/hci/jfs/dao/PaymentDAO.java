package com.hci.jfs.dao;

import java.util.List;
import java.util.Map;

import com.hci.jfs.entity.Contract;
import com.hci.jfs.entity.ContractStatus;


public interface PaymentDAO {
	public Contract getJFSContractByContractNumber(String contractNumber) throws Exception;
	
	public Contract getJFSContractByContractNumberAndStatus(String contractNumber,String status) throws Exception;
	
	public List<ContractStatus> getJFSContractStatusByContractNumber(String contractNumber) throws Exception;
	
	public List<ContractStatus> getJFSAllContractStatusByContractNumberByStatus(String contractNumber,String status) throws Exception;
	
	public ContractStatus getJFSContractStatusByContractNumberByStatus(String contractNumber,String status) throws Exception;
	
	public void checkingLogGenerateUploadFile(Map<String,Object> param) throws Exception;
}
