package com.hci.jfs.dao;

import java.util.List;
import java.util.Map;

import com.hci.jfs.entity.PaymentInt;
import com.hci.jfs.security.ApplicationException;

public interface PaymentIntDAO {

	public PaymentInt getPaymentIntByIncomingPaymentId(int id) throws ApplicationException;
	
	public PaymentInt getPaymentIntByContractNumber(String contractNumber) throws ApplicationException;
	
	public List<PaymentInt> getPaymentIntByContractNumberReguler(String contractNumber) throws ApplicationException;
	
	public List<PaymentInt> getPaymentRegularContract(Map<String,Object> param) throws ApplicationException;
	
	public List<PaymentInt> getPaymentReversalContract(Map<String,Object> param) throws ApplicationException;
	
	public List<PaymentInt> getPaymentGiftContract(Map<String,Object> param) throws ApplicationException;
	
	public List<PaymentInt> getPaymentSmallUnderContract(Map<String,Object> param) throws ApplicationException;
	
	public List<PaymentInt> getPaymentLastPaymentContract(Map<String,Object> param) throws ApplicationException;
	
	public List<PaymentInt> getPaymentETAfterContract(Map<String,Object> param) throws ApplicationException;
	
	public List<PaymentInt> getPaymentContractByContractWithStatusAndDatePayment(Map<String,Object> param) throws ApplicationException;
	
	public List<PaymentInt> getPaymentIntByContractNumberAndPaymentDate(Map<String,Object> param) throws ApplicationException;
	
	public List<PaymentInt> getPaymentIntByContractNumberAndPaymentType(Map<String,Object> param) throws ApplicationException;
	
}
