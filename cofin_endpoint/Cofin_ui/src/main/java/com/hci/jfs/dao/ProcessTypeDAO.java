package com.hci.jfs.dao;

import java.util.List;

import com.hci.jfs.entity.GenerateFile;
import com.hci.jfs.entity.ProcessType;

public interface ProcessTypeDAO {
	public List<ProcessType> getAllProcessType() throws Exception;

	public List<ProcessType> getAllProcessTypeStillActive() throws Exception;

	public ProcessType getProcessTypeByCode(String code) throws Exception;

	public List<ProcessType> getAllProcessTypeByProcess(String code,String name) throws Exception;
	
	public GenerateFile getFileGenerateByAgreementCodeByCreateDateByPartnerCodeByProcessCode(String agreementCode,String createdDate,String partnerCode,String processCode) throws Exception;
	
}
