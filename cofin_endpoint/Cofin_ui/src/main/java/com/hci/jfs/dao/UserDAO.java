package com.hci.jfs.dao;

import java.util.HashMap;
import java.util.List;

import com.hci.jfs.entity.OpsOperator;
import com.hci.jfs.entity.Role;
import com.hci.jfs.entity.User;

/**
 *
 * @author Muhammad.Agaputra
 */
public interface UserDAO {
    
	List<String> getExistingUsername ();
	List<String> getRolesByUserName (String username);
	User getUserByUserName(String username);
	int updatePassword(HashMap<String, Object> param);
	int deleteRolesByUserName (String username);
	int insertListRole (List<Role> record);
	int deleteOpsOperator();
	int insertOpsOperator(OpsOperator opsOperator);
	String getLdapUsername (String adUsername);
}