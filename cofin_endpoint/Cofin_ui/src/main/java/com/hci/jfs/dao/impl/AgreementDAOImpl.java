package com.hci.jfs.dao.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hci.jfs.dao.AgreementDAO;
import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.entity.Agreement;

@SuppressWarnings("unchecked")
@Service
public class AgreementDAOImpl extends BaseDAO implements AgreementDAO {

	@Override
	public List<Agreement> getAllAgreement() {
		return getCurrentSession().getNamedQuery("jfs.Agreement.getAllAgreement").list();
	}
	
	@Override
	public List<Agreement> getAgreement() {
		return getCurrentSession().getNamedQuery("jfs.Agreement.getAgreement").list();
	}

	@Override
	public Agreement getAgreementById(String id) {
		return (Agreement) getCurrentSession().getNamedQuery("jfs.Agreement.getAgreementById")
				.setString("id", id)
				.uniqueResult();
	}
	
	@Override
	public Agreement getAgreementByCode(String code) {
		return (Agreement) getCurrentSession().getNamedQuery("jfs.Agreement.getAgreementByCode")
				.setString("code", code)
				.uniqueResult();
	}
	
	@Override
	public void batchAgreement() {
		List<Agreement> result = getCurrentSession().getNamedQuery("jfs.Agreement.getAllAgreement").list();
		
		if (result.size() > 0) {
			
		}
	}
	
	@Override
	public List<String> getAgreementNames() {		
		return getCurrentSession().getNamedQuery("jfs.Agreement.getAgreementNames")
				.list();
	}

	@Override
	public List<Agreement> getAgreementByPartnerId(String partnerId) throws Exception {
		return getCurrentSession().getNamedQuery("jfs.Agreement.getAgreementByPartnerId")
				.setString("partnerId", partnerId)
				.list();
	}
}
