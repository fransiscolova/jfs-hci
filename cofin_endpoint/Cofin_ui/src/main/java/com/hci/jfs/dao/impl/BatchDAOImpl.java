package com.hci.jfs.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.BatchDAO;
import com.hci.jfs.entity.Batch;
import com.hci.jfs.entity.BatchJob;
import com.hci.jfs.entity.BatchJobLog;

@SuppressWarnings("unchecked")
public class BatchDAOImpl extends BaseDAO implements BatchDAO {

    @Override
    public List<Batch> getAllBatch() {
        return getCurrentSession().getNamedQuery("batch.getAllBatch").list();
    }

    @Override
    public Batch getBatchById(String jobId) {
        return (Batch) getCurrentSession().getNamedQuery("batch.getBatchById")
                .setString("jobId", jobId).uniqueResult();
    }

    @Override
    public List<BatchJobLog> getBatchJob(String partner, String agreementName, String processName,
            String processDateFrom, String processDateTo) {
        return getCurrentSession().getNamedQuery("batch.getBatchJobByParameter")
                .setString("partner", partner).setString("agreementName", agreementName)
                .setString("processName", processName)
                .setString("processDateFrom", processDateFrom)
                .setString("processDateTo", processDateTo).list();
    }

    @Override
    public List<BatchJob> getAllBatchJob() {
        return getCurrentSession().getNamedQuery("batch.getAllBatchJob").list();
    }

    @Override
    public BatchJobLog getBatchJobByPartnerByAgreementAndProcessCode(String partnerCode,
            String agreementCode, String processCode, String runDate, String type) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(BatchJobLog.class);
        criteria.add(Restrictions.eq("partner.id", partnerCode));
        criteria.add(Restrictions.eq("agreement.code", agreementCode));
        criteria.add(Restrictions.eq("process.code", processCode));
        criteria.add(Restrictions.eq("runDate", runDate));
        criteria.add(Restrictions.sqlRestriction("FILE_NAME like '%" + type + "'"));
        BatchJobLog batchJobLog = (BatchJobLog) criteria.uniqueResult();
        if (batchJobLog != null) {
            batchJobLog.getAgreement().getName();
            batchJobLog.getPartner().getName();
            batchJobLog.getProcess().getName();
        }
        return batchJobLog;
    }

    @Override
    public BatchJobLog getBatchJobByPartnerByAgreementAndProcessCode(String partnerCode,
            String agreementCode, String processCode, String runDate) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(BatchJobLog.class);
        criteria.add(Restrictions.eq("partner.id", partnerCode));
        criteria.add(Restrictions.eq("agreement.code", agreementCode));
        criteria.add(Restrictions.eq("process.code", processCode));
        criteria.add(Restrictions.eq("runDate", runDate));
        BatchJobLog batchJobLog = (BatchJobLog) criteria.uniqueResult();
        if (batchJobLog != null) {
            batchJobLog.getAgreement().getName();
            batchJobLog.getPartner().getName();
            batchJobLog.getProcess().getName();
        }
        return batchJobLog;
    }
}
