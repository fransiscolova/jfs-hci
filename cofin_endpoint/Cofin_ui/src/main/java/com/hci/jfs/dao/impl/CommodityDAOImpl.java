package com.hci.jfs.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.CommodityDAO;
import com.hci.jfs.entity.CommodityCategory;
import com.hci.jfs.entity.CommodityGroup;
import com.hci.jfs.entity.CommodityPurpose;
import com.hci.jfs.entity.CommodityType;
import com.hci.jfs.entity.OtherCriteriaSetting;

public class CommodityDAOImpl extends BaseDAO implements CommodityDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<CommodityCategory> getAllCommodityCategory() throws Exception{
		return getCurrentSession().createCriteria(CommodityCategory.class).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommodityType> getAllCommodityType() throws Exception{
		return getCurrentSession().createCriteria(CommodityType.class).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommodityGroup> getAllCommodityGroup() throws Exception{
		return getCurrentSession().createCriteria(CommodityGroup.class).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommodityPurpose> getAllCommodityPurpose() throws Exception {
		return getCurrentSession().createCriteria(CommodityPurpose.class).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommodityCategory> getCommodityCategoryByHciCodeCommodityCategory (
			String codeCommodityCategory) throws Exception {
		Criteria criteria = getCurrentSession().createCriteria(CommodityCategory.class);
		criteria.add(Restrictions.eq("hciCodeCommodityCategory", codeCommodityCategory));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommodityGroup> getCommodityGroupByAgreementCode(
			Long agreementCode) throws Exception {
		Date currentDate = new Date();
		Criteria criteria = getCurrentSession().createCriteria(CommodityGroup.class);
		criteria.add(Restrictions.eq("idAgreement", agreementCode));
		criteria.add(Restrictions.eq("isDelete", CONSTANT_UTIL.DEFAULT_NO));
//		criteria.add(Restrictions.lt("validFrom", currentDate));
//		criteria.add(Restrictions.gt("validTo", currentDate));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommodityType> getCommodityTypeByHciCodeCommodityCategory(
			String codeCommodityCategory) throws Exception {
		Criteria criteria = getCurrentSession().createCriteria(CommodityType.class);
		criteria.add(Restrictions.eq("hciCodeCommodityCategory", codeCommodityCategory));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OtherCriteriaSetting> getAllCriteriaSetting() throws Exception {
		Criteria criteria = getCurrentSession().createCriteria(OtherCriteriaSetting.class);
		return criteria.list();
	}
	
}
