package com.hci.jfs.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.ContractDAO;
import com.hci.jfs.entity.Contract;
import com.hci.jfs.security.ApplicationException;

public class ContractDAOImpl extends BaseDAO implements ContractDAO{

	@Override
	public Contract getContractByAgreementType(String contactNumber,
			String agreementCode) throws ApplicationException {
		Criteria criteria = getCurrentSession().createCriteria(Contract.class);
		criteria.add(Restrictions.eq("textContractNumber",contactNumber));
		criteria.add(Restrictions.sqlRestriction("ID_AGREEMENT in ("+agreementCode+")"));
		return (Contract) criteria.uniqueResult();
	}

	@Override
	public Contract getContractByContractCode(String contactNumber) {
		Criteria criteria = getCurrentSession().createCriteria(Contract.class);
		criteria.add(Restrictions.eq("textContractNumber",contactNumber));
		return (Contract) criteria.uniqueResult();
	}
}
