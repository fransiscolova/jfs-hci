package com.hci.jfs.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.ErrorTypeContractDAO;
import com.hci.jfs.entity.ErrorTypeContract;
import com.hci.jfs.security.ApplicationException;

public class ErrorTypeContractDAOImpl extends BaseDAO implements ErrorTypeContractDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ErrorTypeContract> getAllErrorTypeContract()
			throws ApplicationException {
		Criteria criteria = getCurrentSession().createCriteria(ErrorTypeContract.class);
		criteria.addOrder(Order.asc("code"));
		return criteria.list();
	}

	@Override
	public void insertErrorTypeContract(ErrorTypeContract errorTypeContract)
			throws ApplicationException {
		getCurrentSession().save(errorTypeContract);
	}

}
