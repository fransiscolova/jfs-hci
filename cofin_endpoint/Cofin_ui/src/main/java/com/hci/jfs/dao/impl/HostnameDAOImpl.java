package com.hci.jfs.dao.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.HostnameDAO;
import com.hci.jfs.entity.Hostname;

/**
 *
 * @author Muhammad.Agaputra
 */
@Transactional
@SuppressWarnings("unchecked")
public class HostnameDAOImpl extends BaseDAO implements HostnameDAO {
	
	@Override
	public List<Hostname> selectAllHostname() {
		return getCurrentSession().getNamedQuery("jfs.Hostname.selectAllHostname").list();
	}
	
	@Override
	public List<Hostname> searchHostname(HashMap<String, Object> param) {
		return null;
	}

	@Override
	public Hostname selectById(Short id) { 
		return null;
	}

	@Override
	public int updateHostnamePointer(Hostname hostname) {
		return 0;
	}	

}