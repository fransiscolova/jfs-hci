package com.hci.jfs.dao.impl;

import java.util.List;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.MonitoringContractErrorDAO;
import com.hci.jfs.entity.ErrorContractMonitoring;
import com.hci.jfs.security.ApplicationException;

public class MonitoringContractErrorDAOImpl extends BaseDAO implements
MonitoringContractErrorDAO  {

	@Override
	public void insert(List<ErrorContractMonitoring> list) throws ApplicationException {
		saveByBatch(list);
	}

}
