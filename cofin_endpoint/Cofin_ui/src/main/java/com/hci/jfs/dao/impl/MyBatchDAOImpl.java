package com.hci.jfs.dao.impl;

import java.util.List;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.MyBatchDAO;
import com.hci.jfs.entity.MyBatch;

@SuppressWarnings("unchecked")
public class MyBatchDAOImpl extends BaseDAO implements MyBatchDAO {

	@Override
	public List<MyBatch> getAllMyBatch() {
		return getCurrentSession().getNamedQuery("mybatch.getAllMyBatch").list();
	}

	@Override
	public MyBatch getMyBatchById(String jobId) {
		return (MyBatch) getCurrentSession().getNamedQuery("mybatch.getMyBatchById")
				.setString("jobId", jobId)
				.uniqueResult();
	}
}
