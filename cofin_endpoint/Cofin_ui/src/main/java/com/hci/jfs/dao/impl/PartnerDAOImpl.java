package com.hci.jfs.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.PartnerDAO;
import com.hci.jfs.entity.Contract;
import com.hci.jfs.entity.Partner;

public class PartnerDAOImpl extends BaseDAO implements PartnerDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<Partner> getAllPartner() {
        return getCurrentSession().getNamedQuery("partner.getAllPartner").list();
    }

    @Override
    public Partner getPartnerByContractNumber(String contractNumber) {
        Criteria criteria = getCurrentSession().createCriteria(Contract.class);
        criteria.add(Restrictions.eq("textContractNumber", contractNumber));
        Contract contract = (Contract) criteria.uniqueResult();
        if (contract != null) {
            return contract.getIdAgreement().getPartner();
        }
        return null;
    }

    @Override
    public Partner getPartnerById(String partnerId) {
        Criteria criteria = getCurrentSession().createCriteria(Partner.class);
        criteria.add(Restrictions.eq("id", partnerId));
        return (Partner) criteria.uniqueResult();
    }
}
