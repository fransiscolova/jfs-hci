package com.hci.jfs.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.hci.jfs.activitylog.LogGenerateUploadFile;
import com.hci.jfs.constant.ENUM_ERROR;
import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.PaymentDAO;
import com.hci.jfs.entity.Contract;
import com.hci.jfs.entity.ContractStatus;
import com.hci.jfs.security.ApplicationException;

@Service
public class PaymentDAOImpl extends BaseDAO implements PaymentDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<ContractStatus> getJFSContractStatusByContractNumber(String contractNumber)
            throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ContractStatus.class);
        criteria.add(Restrictions.eq("textContractNumber", contractNumber));
        return criteria.list();
    }

    @Override
    public Contract getJFSContractByContractNumber(String contractNumber) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(Contract.class);
        criteria.add(Restrictions.eq("textContractNumber", contractNumber));
        return (Contract) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ContractStatus> getJFSAllContractStatusByContractNumberByStatus(
            String contractNumber, String status) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ContractStatus.class);
        criteria.add(Restrictions.eq("textContractNumber.textContractNumber", contractNumber));
        criteria.add(Restrictions.eq("codeStatus", status));
        return criteria.list();
    }

    @Override
    public ContractStatus getJFSContractStatusByContractNumberByStatus(String contractNumber,
            String status) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ContractStatus.class);
        criteria.add(Restrictions.eq("textContractNumber.textContractNumber", contractNumber));
        criteria.add(Restrictions.eq("codeStatus", status));
        return (ContractStatus) criteria.uniqueResult();
    }

    @Override
    public Contract getJFSContractByContractNumberAndStatus(String contractNumber, String status)
            throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(Contract.class);
        criteria.add(Restrictions.eq("textContractNumber", contractNumber));
        criteria.add(Restrictions.eq("status", status));
        return (Contract) criteria.uniqueResult();
    }

    @Override
    public void checkingLogGenerateUploadFile(Map<String, Object> param) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(LogGenerateUploadFile.class);
        criteria.add(Restrictions.eq("partnerCode", param.get("partnerCode")));
        criteria.add(Restrictions.eq("agreementCode", param.get("agreementCode")));
        criteria.add(Restrictions.eq("processTypeCode", param.get("processCode")));
        criteria.add(Restrictions.sqlRestriction("TRUNC(DATE_UPLOAD) = TRUNC(TO_DATE('"
                + param.get("processDateCheck") + "','dd/mm/yyyy'))"));
        criteria.add(Restrictions.eq("filetype", param.get("fileType")));
        if (criteria.uniqueResult() != null) {
            throw new ApplicationException(ENUM_ERROR.ERR_FILE_IS_STILL_PROCESS.name());
        }
    }

}
