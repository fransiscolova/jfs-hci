package com.hci.jfs.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.PaymentIntDAO;
import com.hci.jfs.entity.PaymentInt;
import com.hci.jfs.security.ApplicationException;

public class PaymentIntDAOImpl extends BaseDAO implements PaymentIntDAO {

	@Override
	public PaymentInt getPaymentIntByIncomingPaymentId(int id)
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("id",id));
		return (PaymentInt) criteria.uniqueResult();
	}

	@Override
	public PaymentInt getPaymentIntByContractNumber(String contractNumber)
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber",contractNumber));
		PaymentInt paymentInt = (PaymentInt) criteria.uniqueResult();
		return paymentInt;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PaymentInt> getPaymentIntByContractNumberReguler(String contractNumber)
			throws ApplicationException {

		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber",contractNumber));
		return criteria.list();
	}

	@Override
	public List<PaymentInt> getPaymentRegularContract(Map<String, Object> param)
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_PAYMENT) = TRUNC(TO_DATE('"+param.get("transactionDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_EXPORT) = TRUNC(TO_DATE('"+param.get("processDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.eq("status","P"));
		criteria.add(Restrictions.eq("paymentType","R"));
		criteria.add(Restrictions.sqlRestriction("this_.PMT_INSTALMENT > 0"));
		criteria.add(Restrictions.sqlRestriction("this_.AMT_PAYMENT > 0"));
		criteria.add(Restrictions.ne("status",param.get("status")));
		return criteria.list();
	}

	@Override
	public List<PaymentInt> getPaymentReversalContract(Map<String, Object> param)
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_PAYMENT) = TRUNC(TO_DATE('"+param.get("transactionDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_EXPORT) = TRUNC(TO_DATE('"+param.get("processDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.sqlRestriction("PARENT_ID != null"));
		criteria.add(Restrictions.eq("status","P"));
		criteria.add(Restrictions.eq("paymentType","R"));
		criteria.add(Restrictions.sqlRestriction("this_.PMT_INSTALMENT < 0"));
		criteria.add(Restrictions.sqlRestriction("this_.AMT_PAYMENT < 0"));
		criteria.add(Restrictions.ne("status",param.get("status")));
		return criteria.list();
	}

	@Override
	public List<PaymentInt> getPaymentGiftContract(Map<String, Object> param)
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_PAYMENT) = TRUNC(TO_DATE('"+param.get("transactionDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_EXPORT) = TRUNC(TO_DATE('"+param.get("processDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.eq("status","P"));
		criteria.add(Restrictions.eq("paymentType","G"));
		criteria.add(Restrictions.ne("status",param.get("status")));
		return criteria.list();
	}

	@Override
	public List<PaymentInt> getPaymentSmallUnderContract(Map<String, Object> param)
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.sqlRestriction("this_.INCOMING_PAYMENT_ID != '"+param.get("contractNumber")+"'"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_PAYMENT) = TRUNC(TO_DATE('"+param.get("transactionDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_EXPORT) = TRUNC(TO_DATE('"+param.get("processDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.eq("status","P"));
		criteria.add(Restrictions.eq("paymentType","U"));
		criteria.add(Restrictions.sqlRestriction("this_.PMT_INSTALMENT > 0"));
		criteria.add(Restrictions.sqlRestriction("this_.AMT_PAYMENT = 0"));
		criteria.add(Restrictions.ne("status",param.get("status")));
		return criteria.list();
	}

	@Override
	public List<PaymentInt> getPaymentLastPaymentContract(Map<String, Object> param)
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.sqlRestriction("this_.INCOMING_PAYMENT_ID = '"+param.get("contractNumber")+"'"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_PAYMENT) = TRUNC(TO_DATE('"+param.get("transactionDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_EXPORT) = TRUNC(TO_DATE('"+param.get("processDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.eq("status","P"));
		criteria.add(Restrictions.eq("paymentType","L"));
		criteria.add(Restrictions.sqlRestriction("this_.PMT_INSTALMENT = 0"));
		criteria.add(Restrictions.sqlRestriction("this_.AMT_PAYMENT = 0"));
		criteria.add(Restrictions.ne("status",param.get("status")));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PaymentInt> getPaymentContractByContractWithStatusAndDatePayment(
			Map<String, Object> param) throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.eq("status","A"));
		criteria.add(Restrictions.sqlRestriction("this_.PAYMENT_TYPE in ('T','G','U','L')"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_PAYMENT) = TRUNC(TO_DATE('"+param.get("transactionDate")+"','dd/mm/yyyy'))"));
		return criteria.list();
	}

	@Override
	public List<PaymentInt> getPaymentETAfterContract(Map<String, Object> param)
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_PAYMENT) = TRUNC(TO_DATE('"+param.get("transactionDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_EXPORT) = TRUNC(TO_DATE('"+param.get("processDate")+"','dd/mm/yyyy'))"));
		criteria.add(Restrictions.eq("status","P"));
		criteria.add(Restrictions.eq("paymentType","T"));
		criteria.add(Restrictions.sqlRestriction("this_.AMT_PAYMENT > 0"));
		criteria.add(Restrictions.ne("status",param.get("status")));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PaymentInt> getPaymentIntByContractNumberAndPaymentDate(
			Map<String, Object> param) throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.sqlRestriction("TRUNC(this_.DATE_PAYMENT) = TRUNC(TO_DATE('"+param.get("transactionDate")+"','dd/mm/yyyy'))"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PaymentInt> getPaymentIntByContractNumberAndPaymentType(
			Map<String, Object> param) throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(PaymentInt.class);
		criteria.add(Restrictions.eq("textContractNumber.textContractNumber", param.get("contractNumber")));
		criteria.add(Restrictions.eq("paymentType",param.get("paymentType")));
		return criteria.list();
	}

}
