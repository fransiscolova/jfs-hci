package com.hci.jfs.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.ProcessTypeDAO;
import com.hci.jfs.entity.GenerateFile;
import com.hci.jfs.entity.ProcessType;

@Service
public class ProcessTypeDAOImpl extends BaseDAO implements ProcessTypeDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcessType> getAllProcessType() throws Exception {
		Criteria criteria = getCurrentSession().createCriteria(ProcessType.class);
		return criteria.list();
	}

	@Override
	public ProcessType getProcessTypeByCode(String code) throws Exception {
		Criteria criteria = getCurrentSession().createCriteria(ProcessType.class);
		criteria.add(Restrictions.eq("isDelete", CONSTANT_UTIL.DEFAULT_NO));
		criteria.add(Restrictions.eq("code", code));
		return (ProcessType) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcessType> getAllProcessTypeStillActive() throws Exception {
		Criteria criteria = getCurrentSession().createCriteria(ProcessType.class);
		criteria.add(Restrictions.eq("isDelete", CONSTANT_UTIL.DEFAULT_YES));
		return criteria.list();
	}

	@Override
	public GenerateFile getFileGenerateByAgreementCodeByCreateDateByPartnerCodeByProcessCode(
			String agreementCode, String createdDate, String partnerCode,
			String processCode) throws Exception {
		Criteria criteria = getCurrentSession().createCriteria(GenerateFile.class);
		criteria.add(Restrictions.eq("processType.code", processCode));
		criteria.add(Restrictions.eq("agreement.code", agreementCode));
		criteria.add(Restrictions.eq("partner.id", partnerCode));
		criteria.add(Restrictions.sqlRestriction("TRUNC(PROCEED_DATE) = TRUNC(TO_DATE('"+createdDate+"','yyyy-mm-dd'))"));		
		criteria.add(Restrictions.sqlRestriction("TRUNC(CREATED_DATE) = TRUNC(sysdate)"));	
		return (GenerateFile) ((criteria.list()!=null && criteria.list().size()>1) ? criteria.list().get(0) : criteria.uniqueResult());
	}

	@Override
	public List<ProcessType> getAllProcessTypeByProcess(String code, String name)
			throws Exception {
		Criteria criteria = getCurrentSession().createCriteria(ProcessType.class);
		criteria.add(Restrictions.eq("isDelete", CONSTANT_UTIL.DEFAULT_NO));
		criteria.add(Restrictions.like("code", code));
		criteria.add(Restrictions.like("name",name));
		
		return criteria.list();
	}

}
