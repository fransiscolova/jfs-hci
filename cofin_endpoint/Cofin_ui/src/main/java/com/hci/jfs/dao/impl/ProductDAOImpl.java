package com.hci.jfs.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.ProductDAO;
import com.hci.jfs.entity.Product;
import com.hci.jfs.entity.ProductHCI;
import com.hci.jfs.entity.ProductMapping;
import com.hci.jfs.entity.PublicHoliday;

public class ProductDAOImpl extends BaseDAO implements ProductDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> getAllProduct() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(Product.class);
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductHCI> getAllProductHCI() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ProductHCI.class);
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductMapping> getAllProductMapping() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ProductMapping.class);
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PublicHoliday> getCurrentPublicHoliday() throws Exception {
        // TODO Auto-generated method stub
        Criteria criteria = getCurrentSession().createCriteria(PublicHoliday.class);
        criteria.add(Restrictions
                .sqlRestriction("EXTRACT( YEAR FROM DATE_FROM) = EXTRACT(YEAR FROM SYSDATE) "));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> getProductByAgreement(Long agreement) throws Exception {
        Date currentDate = new Date();
        Criteria criteria = getCurrentSession().createCriteria(Product.class);
        criteria.add(Restrictions.lt("validFrom", currentDate));
        criteria.add(Restrictions.gt("validTo", currentDate));
        criteria.add(Restrictions.eq("agreement", agreement));
        return criteria.list();
    }

    @Override
    public Product getProductByBankProductCodeByAgreementCode(String bankProductCode,
            Long agreementCode) throws Exception {
        Date currentDate = new Date();
        Criteria criteria = getCurrentSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("agreement", agreementCode));
        criteria.add(Restrictions.eq("bankProductCode", bankProductCode));
        criteria.add(Restrictions.lt("validFrom", currentDate));
        criteria.add(Restrictions.gt("validTo", currentDate));
        return (Product) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductHCI> getProductHCIByAgreementCode(Long agreementCode) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ProductHCI.class);
        criteria.add(Restrictions.eq("agreement", agreementCode));
        return criteria.list();
    }

    @Override
    public ProductHCI getProductHCIByCodeProductByAgreementCode(String codeProduct,
            Long agreementCode) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ProductHCI.class);
        criteria.add(Restrictions.eq("agreement", agreementCode));
        criteria.add(Restrictions.eq("codeProduct", codeProduct));
        return (ProductHCI) criteria.uniqueResult();
    }

    @Override
    public ProductHCI getProductHCIByIdProduct(String id) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ProductHCI.class);
        criteria.add(Restrictions.eq("idProduct", id));
        return (ProductHCI) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductMapping> getProductMappingByAgreementCode(Long agreementCode)
            throws Exception {
        Date currentDate = new Date();
        Criteria criteria = getCurrentSession().createCriteria(ProductMapping.class);
        criteria.add(Restrictions.lt("validFrom", currentDate));
        criteria.add(Restrictions.gt("validTo", currentDate));
        criteria.add(Restrictions.eq("agreement", agreementCode));
        criteria.add(Restrictions.eq("isDelete", CONSTANT_UTIL.DEFAULT_NO));
        return criteria.list();
    }

    @Override
    public ProductMapping getProductMappingByIdProduct(String id) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ProductMapping.class);
        criteria.add(Restrictions.eq("idProduct", id));
        return (ProductMapping) criteria.uniqueResult();
    }
}
