package com.hci.jfs.dao.impl;

import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.UserDAO;
import com.hci.jfs.entity.OpsOperator;
import com.hci.jfs.entity.Role;
import com.hci.jfs.entity.User;

/**
 *
 * @author Muhammad.Agaputra
 */
@Transactional
public class UserDAOImpl extends BaseDAO implements UserDAO {
	private static final Logger LOGGER = LogManager.getLogger(UserDAOImpl.class.getName());
	
	@Override
	public User getUserByUserName(String username) {		
//		return (User) getSqlSession().selectOne("com.hci.jfs.dao.UserMapper.getUserByUserName", username);
		
		return null;
	}
	
	@Override
	public List<String> getRolesByUserName(String username) {		
		return getCurrentSession().getNamedQuery("jfs.User.getRolesByUserName")
				.setString("username", username)
				.list();
	}
	
	@Override
	public int updatePassword(HashMap<String, Object> param) {		
//		return getSqlSession().update("com.hci.jfs.dao.UserMapper.updatePassword", param);
		
		return 0;
	}

	@Override
	public int deleteRolesByUserName(String username) {
//		return getSqlSession().delete("com.hci.jfs.dao.UserMapper.deleteRolesByUserName", username);
		
		return 0;
	}

	@Override
	public int insertListRole(List<Role> record) {
//		return getSqlSession().insert("com.hci.jfs.dao.UserMapper.insertListRole", record);
		
		return 0;
	}

	@Override
	public List<String> getExistingUsername() {
//		return getSqlSession().selectList("com.hci.jfs.dao.UserMapper.getExistingUsername");
		
		return null;
	}

	@Override
	public int deleteOpsOperator() {
//		return getSqlSession().delete("com.hci.jfs.dao.UserMapper.deleteOpsOperator");
		
		return 0;
	}

	@Override
	public int insertOpsOperator(OpsOperator opsOperator) {
//		return getSqlSession().insert("com.hci.jfs.dao.UserMapper.insertOpsOperator", opsOperator);
		
		return 0;
	}	
	
	@Override
	public String getLdapUsername(String adUsername) {
		String result = null;
		try {
//			result = getSqlSession().selectOne("com.hci.jfs.dao.UserMapper.getLdapUsername", adUsername);
		} catch (Exception e){
			LOGGER.error(e);
			e.printStackTrace();
		}
		
		if(result == null)
			return adUsername;
		else
			return result;
	}	
}
