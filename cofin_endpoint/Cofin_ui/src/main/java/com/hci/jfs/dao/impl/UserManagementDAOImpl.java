package com.hci.jfs.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.composer.usermanagement.UserManagementComposer;
import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.dao.UserManagementDAO;
import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.Role;
import com.hci.jfs.security.ApplicationException;

public class UserManagementDAOImpl extends BaseDAO implements UserManagementDAO {

	final static Logger logger = LogManager.getLogger(UserManagementDAOImpl.class.getName());

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getAllEmployee() throws ApplicationException {
		Criteria criteria = getCurrentSession().createCriteria(Employee.class);
		criteria.addOrder(Order.asc("textEmailAddress"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getRolesByUserName(String username)
			throws ApplicationException {
		Criteria criteria = getCurrentSession().createCriteria(Role.class);
		criteria.add(Restrictions.eq("username", username));
		return criteria.list();
	}

	@Override
	public Employee getUserByUserName(String username)
			throws ApplicationException {
		return null;
	}

	@Override
	public void deleteRolesByUserName(String username)
			throws ApplicationException {
		Query query = getCurrentSession().createQuery("delete Role where username = :username");
		query.setParameter("username", username);
		 
		int result = query.executeUpdate();
		 
		if (result > 0) {
		   logger.debug("Roles from "+username+" already removed");
		}
	}

	@Override
	public void insertRolesByUsername(List<Role> listRoles)
			throws ApplicationException {
		saveByBatch(listRoles);		
	}

}
