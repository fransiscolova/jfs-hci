package com.hci.jfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_agreement")
public class Agreement implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String name;
    private String codeAgreement;
    private Partner partner;
    private Timestamp validFrom;
    private Timestamp validTo;
    private BigDecimal interestRate;
    private BigDecimal principalSplitRate;
    private BigDecimal adminFeeRate;
    private String description;
    private String isDelete;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;

    @Column(name = "adm_fee_rate", precision = 5, scale = 2)
    public BigDecimal getAdminFeeRate() {
        return adminFeeRate;
    }

    @Id
    @Column(name = "code", length = 50)
    public String getCode() {
        return code;
    }

    @Column(name = "code_agreement")
    public String getCodeAgreement() {
        return codeAgreement;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    @Column(name = "description", length = 100)
    public String getDescription() {
        return description;
    }

    @Column(name = "int_rate", precision = 5, scale = 2)
    public BigDecimal getInterestRate() {
        return interestRate;
    }

    @Column(name = "is_delete", length = 1)
    public String getIsDelete() {
        return isDelete;
    }

    @Column(name = "name", length = 100)
    public String getName() {
        return name;
    }

    @ManyToOne
    @JoinColumn(name = "partner_id", nullable = false)
    public Partner getPartner() {
        return partner;
    }

    @Column(name = "prin_split_rate", precision = 5, scale = 2)
    public BigDecimal getPrincipalSplitRate() {
        return principalSplitRate;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    @Column(name = "valid_from")
    public Timestamp getValidFrom() {
        return validFrom;
    }

    @Column(name = "valid_to")
    public Timestamp getValidTo() {
        return validTo;
    }

    public void setAdminFeeRate(BigDecimal adminFeeRate) {
        this.adminFeeRate = adminFeeRate;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCodeAgreement(String codeAgreement) {
        this.codeAgreement = codeAgreement;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public void setPrincipalSplitRate(BigDecimal principalSplitRate) {
        this.principalSplitRate = principalSplitRate;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public void setValidFrom(Timestamp validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTo(Timestamp validTo) {
        this.validTo = validTo;
    }

    @Override
    public String toString() {
        return "Agreement [code=" + code + ", name=" + name + ", codeAgreement=" + codeAgreement
                + ", partner=" + partner + ", validFrom=" + validFrom + ", validTo=" + validTo
                + ", interestRate=" + interestRate + ", principalSplitRate=" + principalSplitRate
                + ", adminFeeRate=" + adminFeeRate + ", description=" + description + ", isDelete="
                + isDelete + ", createdBy=" + createdBy + ", createdDate=" + createdDate
                + ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate + "]";
    }

}
