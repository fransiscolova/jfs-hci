package com.hci.jfs.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

public class AgreementType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CODE")
	private String code;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "IS_DELETE")
	private String isDelete;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "AgreementType [code=" + code + ", description=" + description
				+ ", isDelete=" + isDelete + "]";
	}	
}
