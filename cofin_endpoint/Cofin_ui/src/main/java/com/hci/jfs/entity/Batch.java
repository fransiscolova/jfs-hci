package com.hci.jfs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="my_batch")
public class Batch {

	@Id
	@Column(name = "job_id", length = 200)
	private String jobId;
	
	@Column(name = "job_name", length = 200)
	private String jobName;
	
	@Column(name = "service", length = 200)
	private String service;
	
	@Column(name = "method", length = 200)
	private String method;
	
	@Column(name = "cron_time", length = 200)
	private String cronTime;
	
	@Column(name = "parameter", length = 200)
	private String parameter;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getCronTime() {
		return cronTime;
	}

	public void setCronTime(String cronTime) {
		this.cronTime = cronTime;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	@Override
	public String toString() {
		return "Batch [jobId=" + jobId + ", jobName=" + jobName + ", service="
				+ service + ", method=" + method + ", cronTime=" + cronTime
				+ ", parameter=" + parameter + "]";
	}
}
