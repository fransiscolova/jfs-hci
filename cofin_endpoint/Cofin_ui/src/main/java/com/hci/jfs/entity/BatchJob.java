package com.hci.jfs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="batch_job")
public class BatchJob {

	@Id
	@Column(name = "job_id", length = 5)
	private String jobId;
	
	@Column(name = "parent_job", length = 5)
	private String parentJob;
	
	@Column(name = "job_name", length = 50)
	private String jobName;
	
	@Column(name = "source_folder_name", length = 100)
	private String sourceFolderName;
	
	@Column(name = "source_file_name", length = 100)
	private String sourceFileName;
	
	@Column(name = "target_folder_name", length = 100)
	private String targetFolderName;
	
	@Column(name = "cron_expression", length = 50)
	private String cronExpression;
	
	@Column(name = "force_run", length = 1)
	private String forceRun;
	
	@Column(name = "job_status", length = 10)
	private String jobStatus;
	
	@Column(name = "is_active", length = 1)
	private String isActive;
	
	@Column(name = "job_class", length = 100)
	private String jobClass;
	
	@Column(name = "tmp_folder", length = 100)
	private String tmpFolder;
	
	@Column(name = "process_name", length = 200)
	private String processName;
    
	
	public BatchJob() {}
	public BatchJob(String jobId, String parentJob, String jobName, String sourceFolderName,
			String sourceFileName, String targetFolderName, String cronExpression, String forceRun,
			String jobStatus, String isActive, String jobClass, String tmpFolder, String processName) {
		this.jobId = jobId;
		this.parentJob = parentJob;
		this.jobName = jobName;
		this.sourceFolderName = sourceFolderName;
		this.sourceFileName = sourceFileName;
		this.targetFolderName = targetFolderName;
		this.cronExpression = cronExpression;
		this.forceRun = forceRun;
		this.jobStatus = jobStatus;
		this.isActive = isActive;
		this.jobClass = jobClass;
		this.tmpFolder = tmpFolder;
		this.processName = processName;
	}
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getParentJob() {
		return parentJob;
	}
	public void setParentJob(String parentJob) {
		this.parentJob = parentJob;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getSourceFolderName() {
		return sourceFolderName;
	}
	public void setSourceFolderName(String sourceFolderName) {
		this.sourceFolderName = sourceFolderName;
	}
	public String getSourceFileName() {
		return sourceFileName;
	}
	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}
	public String getTargetFolderName() {
		return targetFolderName;
	}
	public void setTargetFolderName(String targetFolderName) {
		this.targetFolderName = targetFolderName;
	}
	public String getCronExpression() {
		return cronExpression;
	}
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	public String getForceRun() {
		return forceRun;
	}
	public void setForceRun(String forceRun) {
		this.forceRun = forceRun;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getJobClass() {
		return jobClass;
	}
	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}
	public String getTmpFolder() {
		return tmpFolder;
	}
	public void setTmpFolder(String tmpFolder) {
		this.tmpFolder = tmpFolder;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
}
