package com.hci.jfs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="batch_job_log")
public class BatchJobLog {

	@Id
	@Column(name = "run_id", length = 50)
	private String runId;
    
	@Column(name = "run_date", length = 15)
	private String runDate;
	
	@Column(name = "run_time", length = 15)
	private String runTime;
	
	@Column(name = "file_name", length = 300)
	private String fileName;
	
	@Column(name = "status", length = 100)
	private String status;
	
	@Column(name = "reason", length = 500)
	private String reason;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "partner_code")
	private Partner partner;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "agreement_code")
	private Agreement agreement;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "process_code")
	private ProcessType process;

	public BatchJobLog() {}
	
	public String getRunId() {
		return runId;
	}
	public void setRunId(String runId) {
		this.runId = runId;
	}
	public String getRunDate() {
		return runDate;
	}
	public void setRunDate(String runDate) {
		this.runDate = runDate;
	}
	public String getRunTime() {
		return runTime;
	}
	public void setRunTime(String runTime) {
		this.runTime = runTime;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public Agreement getAgreement() {
		return agreement;
	}

	public void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}

	public ProcessType getProcess() {
		return process;
	}

	public void setProcess(ProcessType process) {
		this.process = process;
	}
	
}
