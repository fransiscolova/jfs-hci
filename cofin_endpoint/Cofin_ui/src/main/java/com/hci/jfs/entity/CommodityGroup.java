package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_COMMODITY_GROUP")
public class CommodityGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name="ID")
	private String id;

	@Column(name="HCI_CODE_COMMODITY_CATEGORY")
	private String hciCodeCommodityCategory;

	@ManyToOne
	@JoinColumn(name="btpn_commodity_group")
	private EconomicSector bptnCommodityGroup;

	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="ID_AGREEMENT")
	private Long idAgreement;

	@Column(name="VALID_TO")
	private Date validTo;

	@Column(name="VALID_FROM")
	private Date validFrom;

	@Column(name="IS_DELETE")
	private String isDelete;	

	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;	

	public CommodityGroup() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Date getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public EconomicSector getBptnCommodityGroup() {
		return bptnCommodityGroup;
	}

	public void setBptnCommodityGroup(EconomicSector bptnCommodityGroup) {
		this.bptnCommodityGroup = bptnCommodityGroup;
	}

	public String getHciCodeCommodityCategory() {
		return hciCodeCommodityCategory;
	}

	public void setHciCodeCommodityCategory(String hciCodeCommodityCategory) {
		this.hciCodeCommodityCategory = hciCodeCommodityCategory;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getIdAgreement() {
		return idAgreement;
	}

	public void setIdAgreement(Long idAgreement) {
		this.idAgreement = idAgreement;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	@Override
	public String toString() {
		return "CommodityGroup [id=" + id + ", hciCodeCommodityCategory="
				+ hciCodeCommodityCategory + ", bptnCommodityGroup="
				+ bptnCommodityGroup + ", description=" + description
				+ ", idAgreement=" + idAgreement + ", validTo=" + validTo
				+ ", validFrom=" + validFrom + ", isDelete=" + isDelete
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate
				+ "]";
	}

}
