package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_COMMODITY_PURPOSE")
public class CommodityPurpose implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name="ID")
	private String id;
	
	@Column(name="CODE_LOAN_PURPOSE")
	private String codeLoanPurpose;
	
	@Column(name="COMMODITY_TYPE_CODE")
	private String commodityTypeCode;
	
	@Column(name="DATE_INSRTED")
	private Date dateInserted;
	public CommodityPurpose() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodeLoanPurpose() {
		return codeLoanPurpose;
	}
	public void setCodeLoanPurpose(String codeLoanPurpose) {
		this.codeLoanPurpose = codeLoanPurpose;
	}
	public String getCommodityTypeCode() {
		return commodityTypeCode;
	}
	public void setCommodityTypeCode(String commodityTypeCode) {
		this.commodityTypeCode = commodityTypeCode;
	}
	public Date getDateInserted() {
		return dateInserted;
	}
	public void setDateInserted(Date dateInserted) {
		this.dateInserted = dateInserted;
	}
	@Override
	public String toString() {
		return "CommodityPurpose [id=" + id + ", codeLoanPurpose="
				+ codeLoanPurpose + ", commodityTypeCode=" + commodityTypeCode
				+ ", dateInserted=" + dateInserted + "]";
	}



}
