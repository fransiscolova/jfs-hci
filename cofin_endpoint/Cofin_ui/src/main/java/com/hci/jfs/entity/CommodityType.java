package com.hci.jfs.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_COMMODITY_TYPE")
public class CommodityType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name="ID")
	private String id;
	
	@Column(name="HCI_CODE_COMMODITY_CATEGORY")
	private String hciCodeCommodityCategory;
	
	@Column(name="BTPN_CODE_COMMODITY_CATEGORY")
	private String btpnCodeCommodityCategory;
	
	@Column(name="HCI_CODE_COMMODITY_TYPE")
	private String hciCodeCommodityType;
	
	@Column(name="BTPN_CODE_COMMODITY_TYPE")
	private String btpnCodeCommodityType;
	
	@Column(name="DESCRIPTION")
	private String description;

	public CommodityType() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHciCodeCommodityCategory() {
		return hciCodeCommodityCategory;
	}

	public void setHciCodeCommodityCategory(String hciCodeCommodityCategory) {
		this.hciCodeCommodityCategory = hciCodeCommodityCategory;
	}

	public String getBtpnCodeCommodityCategory() {
		return btpnCodeCommodityCategory;
	}

	public void setBtpnCodeCommodityCategory(String btpnCodeCommodityCategory) {
		this.btpnCodeCommodityCategory = btpnCodeCommodityCategory;
	}

	public String getHciCodeCommodityType() {
		return hciCodeCommodityType;
	}

	public void setHciCodeCommodityType(String hciCodeCommodityType) {
		this.hciCodeCommodityType = hciCodeCommodityType;
	}

	public String getBtpnCodeCommodityType() {
		return btpnCodeCommodityType;
	}

	public void setBtpnCodeCommodityType(String btpnCodeCommodityType) {
		this.btpnCodeCommodityType = btpnCodeCommodityType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CommodityType [id=" + id + ", hciCodeCommodityCategory="
				+ hciCodeCommodityCategory + ", btpnCodeCommodityCategory="
				+ btpnCodeCommodityCategory + ", hciCodeCommodityType="
				+ hciCodeCommodityType + ", btpnCodeCommodityType="
				+ btpnCodeCommodityType + ", description=" + description + "]";
	}
	
}
