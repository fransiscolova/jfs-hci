package com.hci.jfs.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="jfs_contract")
public class Contract {
	
	@Id
	@Column(name = "TEXT_CONTRACT_NUMBER")
	private String textContractNumber;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "EXPORT_DATE")
	private Date exportDate;
	
	@Column(name = "SKP_CONTRACT")
	private BigDecimal skpContract; 
	
	@Column(name = "SEND_PRINCIPAL")
	private BigDecimal sendPrincipal;
	
	@Column(name = "SEND_INSTALMENT")
	private BigDecimal sendInstalment;
	
	@Column(name = "SEND_TENOR")
	private BigDecimal sendTenor;
	
	@Column(name = "SEND_RATE")
	private BigDecimal sendRate;
	
	@Column(name = "DATE_FIRST_DUE")
	private Date dateFirstDue;
	
	@Column(name = "ACCEPTED_DATE")
	private Date acceptedDate;
	
	@Column(name = "REJECT_REASON")
	private String rejectReason;
	
	@Column(name = "BANK_INTEREST_RATE")
	private BigDecimal bankInterestRate;
	
	@Column(name = "BANK_PRINCIPAL")
	private BigDecimal bankPrincipal;
	
	@Column(name = "BANK_INTEREST")
	private BigDecimal bankInterest;
	
	@Column(name = "BANK_PROVISION")
	private BigDecimal bankProvision;
	
	@Column(name = "BANK_ADMIN_FEE")
	private BigDecimal bankAdminFee;
	
	@Column(name = "BANK_INSTALLMENT")
	private BigDecimal bankInstallment;
	
	@Column(name = "BANK_TENOR")
	private BigDecimal bankTenor; 
	
	@Column(name = "BANK_SPLIT_RATE")
	private BigDecimal bankSplitRate;
	
	@Column(name = "CLIENT_NAME")
	private String clientName;

	@Column(name = "AMT_INSTALMENT")
	private BigDecimal amtInstalment;
	
	@Column(name = "AMT_MONTHLY_FEE")
	private BigDecimal amtMonthlyfee;
	
	@ManyToOne
	@JoinColumn(name="ID_AGREEMENT")
	private Agreement idAgreement;
	
	@Column(name = "REASON")
	private String reason;
	
	@Column(name = "BANK_DECISION_DATE")
	private Date bankDecisionDate;
	
	@Column(name = "BANK_CLAWBACK_DATE")
	private Date bankClawbackDate;
	
	@Column(name = "BANK_CLAWBACK_AMOUNT")
	private BigDecimal bankClawbackAmount;
	
	@Column(name = "BANK_PRODUCT_CODE")
	private String bankProductCode; 
	
	@Column(name = "BANK_REFERENCE_NO")
	private String bankReferenceNo;
	
	@Column(name = "CUID")
	private String cuid;
	
	
	public Contract() {
		super();
	}

	public Contract(String textContractNumber) {
		super();
		this.textContractNumber = textContractNumber;
	}

	public String getTextContractNumber() {
		return textContractNumber;
	}

	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getExportDate() {
		return exportDate;
	}

	public void setExportDate(Date exportDate) {
		this.exportDate = exportDate;
	}

	
	public BigDecimal getSendPrincipal() {
		return sendPrincipal;
	}

	public void setSendPrincipal(BigDecimal sendPrincipal) {
		this.sendPrincipal = sendPrincipal;
	}

	public BigDecimal getSendInstalment() {
		return sendInstalment;
	}

	public void setSendInstalment(BigDecimal sendInstalment) {
		this.sendInstalment = sendInstalment;
	}


	public BigDecimal getSendRate() {
		return sendRate;
	}

	public void setSendRate(BigDecimal sendRate) {
		this.sendRate = sendRate;
	}

	public Date getDateFirstDue() {
		return dateFirstDue;
	}

	public void setDateFirstDue(Date dateFirstDue) {
		this.dateFirstDue = dateFirstDue;
	}

	public Date getAcceptedDate() {
		return acceptedDate;
	}

	public void setAcceptedDate(Date acceptedDate) {
		this.acceptedDate = acceptedDate;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public BigDecimal getBankInterestRate() {
		return bankInterestRate;
	}

	public void setBankInterestRate(BigDecimal bankInterestRate) {
		this.bankInterestRate = bankInterestRate;
	}

	public BigDecimal getBankPrincipal() {
		return bankPrincipal;
	}

	public void setBankPrincipal(BigDecimal bankPrincipal) {
		this.bankPrincipal = bankPrincipal;
	}

	public BigDecimal getBankInterest() {
		return bankInterest;
	}

	public void setBankInterest(BigDecimal bankInterest) {
		this.bankInterest = bankInterest;
	}

	public BigDecimal getBankProvision() {
		return bankProvision;
	}

	public void setBankProvision(BigDecimal bankProvision) {
		this.bankProvision = bankProvision;
	}

	public BigDecimal getBankAdminFee() {
		return bankAdminFee;
	}

	public void setBankAdminFee(BigDecimal bankAdminFee) {
		this.bankAdminFee = bankAdminFee;
	}

	public BigDecimal getBankInstallment() {
		return bankInstallment;
	}

	public void setBankInstallment(BigDecimal bankInstallment) {
		this.bankInstallment = bankInstallment;
	}

	

	public BigDecimal getSkpContract() {
		return skpContract;
	}

	public void setSkpContract(BigDecimal skpContract) {
		this.skpContract = skpContract;
	}

	public BigDecimal getSendTenor() {
		return sendTenor;
	}

	public void setSendTenor(BigDecimal sendTenor) {
		this.sendTenor = sendTenor;
	}

	public BigDecimal getBankTenor() {
		return bankTenor;
	}

	public void setBankTenor(BigDecimal bankTenor) {
		this.bankTenor = bankTenor;
	}

	public BigDecimal getBankSplitRate() {
		return bankSplitRate;
	}

	public void setBankSplitRate(BigDecimal bankSplitRate) {
		this.bankSplitRate = bankSplitRate;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public BigDecimal getAmtInstalment() {
		return amtInstalment;
	}

	public void setAmtInstalment(BigDecimal amtInstalment) {
		this.amtInstalment = amtInstalment;
	}

	public BigDecimal getAmtMonthlyfee() {
		return amtMonthlyfee;
	}

	public void setAmtMonthlyfee(BigDecimal amtMonthlyfee) {
		this.amtMonthlyfee = amtMonthlyfee;
	}

	public Agreement getIdAgreement() {
		return idAgreement;
	}

	public void setIdAgreement(Agreement idAgreement) {
		this.idAgreement = idAgreement;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getBankDecisionDate() {
		return bankDecisionDate;
	}

	public void setBankDecisionDate(Date bankDecisionDate) {
		this.bankDecisionDate = bankDecisionDate;
	}

	public Date getBankClawbackDate() {
		return bankClawbackDate;
	}

	public void setBankClawbackDate(Date bankClawbackDate) {
		this.bankClawbackDate = bankClawbackDate;
	}

	public BigDecimal getBankClawbackAmount() {
		return bankClawbackAmount;
	}

	public void setBankClawbackAmount(BigDecimal bankClawbackAmount) {
		this.bankClawbackAmount = bankClawbackAmount;
	}

	public String getBankProductCode() {
		return bankProductCode;
	}

	public void setBankProductCode(String bankProductCode) {
		this.bankProductCode = bankProductCode;
	}

	public String getBankReferenceNo() {
		return bankReferenceNo;
	}

	public void setBankReferenceNo(String bankReferenceNo) {
		this.bankReferenceNo = bankReferenceNo;
	}

	public String getCuid() {
		return cuid;
	}

	public void setCuid(String cuid) {
		this.cuid = cuid;
	}

	@Override
	public String toString() {
		return "Contract [ textContractNumber="
				+ textContractNumber + ", status=" + status + ", exportDate="
				+ exportDate + ", skpContract=" + skpContract
				+ ", sendPrincipal=" + sendPrincipal + ", sendInstalment="
				+ sendInstalment + ", sendTenor=" + sendTenor + ", sendRate="
				+ sendRate + ", dateFirstDue=" + dateFirstDue
				+ ", acceptedDate=" + acceptedDate + ", rejectReason="
				+ rejectReason + ", bankInterestRate=" + bankInterestRate
				+ ", bankPrincipal=" + bankPrincipal + ", bankInterest="
				+ bankInterest + ", bankProvision=" + bankProvision
				+ ", bankAdminFee=" + bankAdminFee + ", bankInstallment="
				+ bankInstallment + ", bankTenor=" + bankTenor
				+ ", bankSplitRate=" + bankSplitRate + ", clientName="
				+ clientName + ", amtInstalment=" + amtInstalment
				+ ", amtMonthlyfee=" + amtMonthlyfee + ", idAgreement="
				+ idAgreement + ", reason=" + reason + ", bankDecisionDate="
				+ bankDecisionDate + ", bankClawbackDate=" + bankClawbackDate
				+ ", bankClawbackAmount=" + bankClawbackAmount
				+ ", bankProductCode=" + bankProductCode + ", bankReferenceNo="
				+ bankReferenceNo + ", cuid=" + cuid + "]";
	}
}
