package com.hci.jfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_CONTRACT_ET")
public class ContractET implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "ID")
	private String id;
	
	@Column(name = "BANK_REFERENCE_NO")
	private String bankReferenceNo;
	
	@Column(name = "DATE_BANK_DISBURSED")
	private Date dateBankDisbursed;
	
	@Column(name = "DTIME_CREATED")
	private Date dtimeCreated;
	
	@ManyToOne
	@JoinColumn(name = "TEXT_CONTRACT_NUMBER")
	private Contract contract;
	
	@Column(name = "DATE_SIGNED")
	private Date dateSigned;
	
	@Column(name = "AMT_TOTAL_PAYMENT")
	private BigDecimal amtTotalPayment;
	
	@Column(name = "AMT_ET")
	private BigDecimal amtET;
	
	@Column(name = "CNT_ET_DAYS")
	private BigDecimal cntETDays;
	
	@Column(name = "DATE_BANK_ET")
	private Date dateBankET;
	
	@Column(name = "AMT_BANK_DISBURSED")
	private BigDecimal amtBankDisbursed;
	
	@Column(name = "AMT_BANK_PAYMENT")
	private BigDecimal amtBankPayment;
	
	@Column(name = "AMT_BANK_ET")
	private BigDecimal amtBankET;
		
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "DATE_BANK_PRINTED")
	private Date dateBankPrinted;
	
	@Column(name = "DTIME_UPDATED")
	private Date dtimeUpdate;
	
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	@Column(name = "BANK_PRINCIPAL")
	private BigDecimal bankPrincipal;
	
	@Column(name = "DATE_ET")
	private Date dateET;
	
	@Column(name = "INCOMING_PAYMENT_ID")
	private String incomingPaymentId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBankReferenceNo() {
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo) {
		this.bankReferenceNo = bankReferenceNo;
	}
	public Date getDateBankDisbursed() {
		return dateBankDisbursed;
	}
	public void setDateBankDisbursed(Date dateBankDisbursed) {
		this.dateBankDisbursed = dateBankDisbursed;
	}
	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	public Date getDateSigned() {
		return dateSigned;
	}
	public void setDateSigned(Date dateSigned) {
		this.dateSigned = dateSigned;
	}
	public BigDecimal getAmtTotalPayment() {
		return amtTotalPayment;
	}
	public void setAmtTotalPayment(BigDecimal amtTotalPayment) {
		this.amtTotalPayment = amtTotalPayment;
	}
	public BigDecimal getAmtET() {
		return amtET;
	}
	public void setAmtET(BigDecimal amtET) {
		this.amtET = amtET;
	}
	public BigDecimal getCntETDays() {
		return cntETDays;
	}
	public void setCntETDays(BigDecimal cntETDays) {
		this.cntETDays = cntETDays;
	}
	public Date getDateBankET() {
		return dateBankET;
	}
	public void setDateBankET(Date dateBankET) {
		this.dateBankET = dateBankET;
	}
	public BigDecimal getAmtBankDisbursed() {
		return amtBankDisbursed;
	}
	public void setAmtBankDisbursed(BigDecimal amtBankDisbursed) {
		this.amtBankDisbursed = amtBankDisbursed;
	}
	public BigDecimal getAmtBankPayment() {
		return amtBankPayment;
	}
	public void setAmtBankPayment(BigDecimal amtBankPayment) {
		this.amtBankPayment = amtBankPayment;
	}
	public BigDecimal getAmtBankET() {
		return amtBankET;
	}
	public void setAmtBankET(BigDecimal amtBankET) {
		this.amtBankET = amtBankET;
	}
	public Date getDtimeCreated() {
		return dtimeCreated;
	}
	public void setDtimeCreated(Date dtimeCreated) {
		this.dtimeCreated = dtimeCreated;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getDateBankPrinted() {
		return dateBankPrinted;
	}
	public void setDateBankPrinted(Date dateBankPrinted) {
		this.dateBankPrinted = dateBankPrinted;
	}
	public Date getDtimeUpdate() {
		return dtimeUpdate;
	}
	public void setDtimeUpdate(Date dtimeUpdate) {
		this.dtimeUpdate = dtimeUpdate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public BigDecimal getBankPrincipal() {
		return bankPrincipal;
	}
	public void setBankPrincipal(BigDecimal bankPrincipal) {
		this.bankPrincipal = bankPrincipal;
	}
	public Date getDateET() {
		return dateET;
	}
	public void setDateET(Date dateET) {
		this.dateET = dateET;
	}
	public String getIncomingPaymentId() {
		return incomingPaymentId;
	}
	public void setIncomingPaymentId(String incomingPaymentId) {
		this.incomingPaymentId = incomingPaymentId;
	}
	@Override
	public String toString() {
		return "ContractET [id=" + id + ", bankReferenceNo=" + bankReferenceNo
				+ ", dateBankDisbursed=" + dateBankDisbursed
				+ ", dtimeCreated=" + dtimeCreated + ", contract=" + contract
				+ ", dateSigned=" + dateSigned + ", amtTotalPayment="
				+ amtTotalPayment + ", amtET=" + amtET + ", cntETDays="
				+ cntETDays + ", dateBankET=" + dateBankET
				+ ", amtBankDisbursed=" + amtBankDisbursed
				+ ", amtBankPayment=" + amtBankPayment + ", amtBankET="
				+ amtBankET + ", createdBy=" + createdBy + ", dateBankPrinted="
				+ dateBankPrinted + ", dtimeUpdate=" + dtimeUpdate
				+ ", updatedBy=" + updatedBy + ", bankPrincipal="
				+ bankPrincipal + ", dateET=" + dateET + ", incomingPaymentId="
				+ incomingPaymentId + "]";
	}
	
	
}
