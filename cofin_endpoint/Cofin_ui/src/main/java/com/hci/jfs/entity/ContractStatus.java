package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_CONTRACT_STATUS")
public class ContractStatus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name="ID")
	private String id;
	
	@ManyToOne
	@JoinColumn(name = "TEXT_CONTRACT_NUMBER")
	private Contract textContractNumber;	

	@Column(name = "DTIME_CREATED")
	private Date dtimeCreated;
	
	@Column(name = "CNT_DPD")
	private Integer cntDPD;
	
	@Column(name = "CODE_STATUS")
	private String codeStatus;
	
	@Column(name = "DTIME_BANK_PROCESS")
	private Date dtimeBankProcess;
	
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	@Column(name = "DTIME_UPDATED")
	private Date dtimeUpdated;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "DATE_UPLOAD")
	private Date dateUpload;
	
	@Column(name = "FILENAME")
	private String filename;

	public String getId() {
		return id;
	}
	public Date getDateUpload() {
		return dateUpload;
	}
	public void setDateUpload(Date dateUpload) {
		this.dateUpload = dateUpload;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Contract getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(Contract textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public Date getDtimeCreated() {
		return dtimeCreated;
	}
	public void setDtimeCreated(Date dtimeCreated) {
		this.dtimeCreated = dtimeCreated;
	}
	public Integer getCntDPD() {
		return cntDPD;
	}
	public void setCntDPD(Integer cntDPD) {
		this.cntDPD = cntDPD;
	}
	public String getCodeStatus() {
		return codeStatus;
	}
	public void setCodeStatus(String codeStatus) {
		this.codeStatus = codeStatus;
	}
	public Date getDtimeBankProcess() {
		return dtimeBankProcess;
	}
	public void setDtimeBankProcess(Date dtimeBankProcess) {
		this.dtimeBankProcess = dtimeBankProcess;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getDtimeUpdated() {
		return dtimeUpdated;
	}
	public void setDtimeUpdated(Date dtimeUpdated) {
		this.dtimeUpdated = dtimeUpdated;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Override
	public String toString() {
		return "ContractStatus [id=" + id + ", textContractNumber="
				+ textContractNumber + ", dtimeCreated=" + dtimeCreated
				+ ", cntDPD=" + cntDPD + ", codeStatus=" + codeStatus
				+ ", dtimeBankProcess=" + dtimeBankProcess + ", updatedBy="
				+ updatedBy + ", dtimeUpdated=" + dtimeUpdated + ", createdBy="
				+ createdBy + ", dateUpload=" + dateUpload + ", filename="
				+ filename + "]";
	}
	
	 
}
