package com.hci.jfs.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="JFS_ECONOMIC_SECTOR")
public class EconomicSector implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="BANK_COMMODITY_GROUP")
	private String bankCommodityGroup;
	
	@Column(name="BI_ECONOMIC_SECTOR_CODE")
	private String biEconomicSector;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	public String getBankCommodityGroup() {
		return bankCommodityGroup;
	}
	public void setBankCommodityGroup(String bankCommodityGroup) {
		this.bankCommodityGroup = bankCommodityGroup;
	}
	public String getBiEconomicSector() {
		return biEconomicSector;
	}
	public void setBiEconomicSector(String biEconomicSector) {
		this.biEconomicSector = biEconomicSector;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "EconomicSector [bankCommodityGroup=" + bankCommodityGroup
				+ ", biEconomicSector=" + biEconomicSector + ", description="
				+ description + "]";
	}
}
