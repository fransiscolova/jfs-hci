package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="EMPLOYEE_HR")
@XmlRootElement(name = "Employee")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Employee")
public class Employee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CODE_EMPLOYEE")
	private String codeEmployee;
	
	@Column(name = "TEXT_USERNAME_EMPLOYEE")
	private String textUsernameEmployee;

	@Column(name = "NAME_COMMON")
	private String nameCommon;
	
	@Column(name = "NAME_FIRST")
	private String nameFirst;
	
	@Column(name = "NAME_MIDDLE")
	private String nameMiddle;
	
	@Column(name = "NAME_LAST")
	private String nameLast;
	
	@Column(name = "POSITION")
	private String position;
	
	@Column(name = "DATE_JOIN")
	private Date dateJoin;
	
	@Column(name = "DATE_BIRTH")
	private Date dateBirth;
	
	@Column(name = "CODE_EMPLOYEE_LDAP")
	private String codeEmployeeLDAP;
	
	@Column(name = "TEXT_EMAIL_ADDRESS")
	private String textEmailAddress;
	
	@Column(name = "TEXT_PASSWORD")
	private String textPassword;
	
	@Column(name = "TEXT_CONTACT")
	private String textContact;
	
	@Column(name = "CODE_STATUS")
	private String codeStatus;
	
	@Column(name = "DTIME_INSERTED")
	private Date dtimeInserted;
	
	@Column(name = "DTIME_UPDATED")
	private Date dtimeUpdated;
	
	@Column(name = "STAGE")
	private Integer stage;
	
	@Column(name = "KTP")
	private String KTP;
	
	@Column(name = "TEXT_INSERTED_BY")
	private String textInsertedBy;
	
	@Column(name = "ID_EMP")
	private Integer idEmp;
	
	@Column(name = "TEXT_UPDATED_BY")
	private String textUpdatedBy;
	
	@Column(name = "INITIAL_PASSWORD")
	private String initialPassword;

	@Column(name = "IS_DELETE")
	private String isDelete;
		
	public String getCodeEmployee() {
		return codeEmployee;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public void setCodeEmployee(String codeEmployee) {
		this.codeEmployee = codeEmployee;
	}

	public String getTextUsernameEmployee() {
		return textUsernameEmployee;
	}

	public void setTextUsernameEmployee(String textUsernameEmployee) {
		this.textUsernameEmployee = textUsernameEmployee;
	}

	public String getNameCommon() {
		return nameCommon;
	}

	public void setNameCommon(String nameCommon) {
		this.nameCommon = nameCommon;
	}

	public String getNameFirst() {
		return nameFirst;
	}

	public void setNameFirst(String nameFirst) {
		this.nameFirst = nameFirst;
	}

	public String getNameMiddle() {
		return nameMiddle;
	}

	public void setNameMiddle(String nameMiddle) {
		this.nameMiddle = nameMiddle;
	}

	public String getNameLast() {
		return nameLast;
	}

	public void setNameLast(String nameLast) {
		this.nameLast = nameLast;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getDateJoin() {
		return dateJoin;
	}

	public void setDateJoin(Date dateJoin) {
		this.dateJoin = dateJoin;
	}

	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getCodeEmployeeLDAP() {
		return codeEmployeeLDAP;
	}

	public void setCodeEmployeeLDAP(String codeEmployeeLDAP) {
		this.codeEmployeeLDAP = codeEmployeeLDAP;
	}

	public String getTextEmailAddress() {
		return textEmailAddress;
	}

	public void setTextEmailAddress(String textEmailAddress) {
		this.textEmailAddress = textEmailAddress;
	}

	public String getTextPassword() {
		return textPassword;
	}

	public void setTextPassword(String textPassword) {
		this.textPassword = textPassword;
	}

	public String getTextContact() {
		return textContact;
	}

	public void setTextContact(String textContact) {
		this.textContact = textContact;
	}

	public String getCodeStatus() {
		return codeStatus;
	}

	public void setCodeStatus(String codeStatus) {
		this.codeStatus = codeStatus;
	}

	public Date getDtimeInserted() {
		return dtimeInserted;
	}

	public void setDtimeInserted(Date dtimeInserted) {
		this.dtimeInserted = dtimeInserted;
	}

	public Date getDtimeUpdated() {
		return dtimeUpdated;
	}

	public void setDtimeUpdated(Date dtimeUpdated) {
		this.dtimeUpdated = dtimeUpdated;
	}

	public Integer getStage() {
		return stage;
	}

	public void setStage(Integer stage) {
		this.stage = stage;
	}

	public String getKTP() {
		return KTP;
	}

	public void setKTP(String kTP) {
		KTP = kTP;
	}

	public String getTextInsertedBy() {
		return textInsertedBy;
	}

	public void setTextInsertedBy(String textInsertedBy) {
		this.textInsertedBy = textInsertedBy;
	}

	public Integer getIdEmp() {
		return idEmp;
	}

	public void setIdEmp(Integer idEmp) {
		this.idEmp = idEmp;
	}

	public String getTextUpdatedBy() {
		return textUpdatedBy;
	}

	public void setTextUpdatedBy(String textUpdatedBy) {
		this.textUpdatedBy = textUpdatedBy;
	}

	public String getInitialPassword() {
		return initialPassword;
	}

	public void setInitialPassword(String initialPassword) {
		this.initialPassword = initialPassword;
	}

	@Override
	public String toString() {
		return "Employee [codeEmployee=" + codeEmployee
				+ ", textUsernameEmployee=" + textUsernameEmployee
				+ ", nameCommon=" + nameCommon + ", nameFirst=" + nameFirst
				+ ", nameMiddle=" + nameMiddle + ", nameLast=" + nameLast
				+ ", position=" + position + ", dateJoin=" + dateJoin
				+ ", dateBirth=" + dateBirth + ", codeEmployeeLDAP="
				+ codeEmployeeLDAP + ", textEmailAddress=" + textEmailAddress
				+ ", textPassword=" + textPassword + ", textContact="
				+ textContact + ", codeStatus=" + codeStatus
				+ ", dtimeInserted=" + dtimeInserted + ", dtimeUpdated="
				+ dtimeUpdated + ", stage=" + stage + ", KTP=" + KTP
				+ ", textInsertedBy=" + textInsertedBy + ", idEmp=" + idEmp
				+ ", textUpdatedBy=" + textUpdatedBy + ", initialPassword="
				+ initialPassword + "]";
	}
	
}
