package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="ERROR_CONTRACT_MONITORING")
public class ErrorContractMonitoring implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name="ID")
	private String id;

	@ManyToOne
	@JoinColumn(name="TEXT_CONTRACT_NUMBER")
	private Contract contract;

	@ManyToOne
	@JoinColumn(name="CODE")
	private ErrorTypeContract errorTypeContract;
	
	@ManyToOne
	@JoinColumn(name="PROCESS_CODE")
	private ProcessType process;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "PROCESS_DATE")
	private Date processDate;
	
	public ErrorContractMonitoring() {
		super();
	}

	public ErrorContractMonitoring(Contract contract,
			ErrorTypeContract errorTypeContract) {
		super();
		this.contract = contract;
		this.errorTypeContract = errorTypeContract;
	}
	 
	public ProcessType getProcess() {
		return process;
	}

	public void setProcess(ProcessType process) {
		this.process = process;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public ErrorTypeContract getErrorTypeContract() {
		return errorTypeContract;
	}

	public void setErrorTypeContract(ErrorTypeContract errorTypeContract) {
		this.errorTypeContract = errorTypeContract;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "ErrorContractMonitoring [id=" + id + ", contract=" + contract
				+ ", errorTypeContract=" + errorTypeContract + ", process="
				+ process + ", createdBy=" + createdBy + ", createdDate="
				+ createdDate + ", processDate=" + processDate + "]";
	}

}
