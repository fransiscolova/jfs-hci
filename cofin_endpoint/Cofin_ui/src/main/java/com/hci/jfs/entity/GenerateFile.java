package com.hci.jfs.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="JFS_FILE_GENERATE_DETAIL")
public class GenerateFile {
	
	@Id
	@Column(name = "FILE_NAME")
	private String fileName;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "AGREEMENT_CODE")
	private Agreement agreement;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "PARTNER_CODE")
	private Partner partner;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "PROCESS_CODE")
	private ProcessType processType;

	@Column(name = "LETTER_NUMBER")
	private String letterNumber;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "PROCEED_DATE")
	private Date proceedDate;
	
	@Column(name = "IS_CREATED")
	private String isCreated;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "RUN_ID_BATCH_JOB_LOG")
	private String runIdBatchJobLog;
	
	@Column(name = "CREATED_BY")
	private String createdBy;	
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Agreement getAgreement() {
		return agreement;
	}
	
	public void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}
	
	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public ProcessType getProcessType() {
		return processType;
	}

	public void setProcessType(ProcessType processType) {
		this.processType = processType;
	}

	public String getLetterNumber() {
		return letterNumber;
	}

	public void setLetterNumber(String letterNumber) {
		this.letterNumber = letterNumber;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIsCreated() {
		return isCreated;
	}

	public void setIsCreated(String isCreated) {
		this.isCreated = isCreated;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRunIdBatchJobLog() {
		return runIdBatchJobLog;
	}

	public void setRunIdBatchJobLog(String runIdBatchJobLog) {
		this.runIdBatchJobLog = runIdBatchJobLog;
	}

	

	public Date getProceedDate() {
		return proceedDate;
	}

	public void setProceedDate(Date proceedDate) {
		this.proceedDate = proceedDate;
	}

	@Override
	public String toString() {
		return "GenerateFile [fileName=" + fileName + ", agreement="
				+ agreement + ", partner=" + partner + ", processType="
				+ processType + ", letterNumber=" + letterNumber
				+ ", createdDate=" + createdDate + ", proceedDate="
				+ proceedDate + ", isCreated=" + isCreated + ", name=" + name
				+ ", runIdBatchJobLog=" + runIdBatchJobLog + ", createdBy="
				+ createdBy + "]";
	}

}
