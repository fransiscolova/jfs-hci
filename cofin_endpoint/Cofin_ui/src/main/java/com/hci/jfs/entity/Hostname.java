package com.hci.jfs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mst_hostname")
public class Hostname {
    
	@Id
	@Column(name = "id", length = 40)
	private int id;
    
	@Column(name = "city_name", length = 100)
	private String cityName;
    
	@Column(name = "city_code", length = 3)
	private String cityCode;
    
	@Column(name = "pointer", length = 40)
	private int pointer;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public int getPointer() {
		return pointer;
	}
	public void setPointer(int pointer) {
		this.pointer = pointer;
	}

    
}