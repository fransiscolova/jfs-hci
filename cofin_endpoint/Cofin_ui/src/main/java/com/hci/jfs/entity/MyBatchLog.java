package com.hci.jfs.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="my_batch_log")
public class MyBatchLog {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "id", unique = true)
	private String id;
	
	@Column(name = "job_id", length = 200)
	private String jobId;
	
	@Column(name = "execute_by", length = 200)
	private String executeBy;
	
	@Column(name = "execute_date")
	private Date executeDate;
	
	@Column(name = "is_error", length = 1)
	private String isError;
	
	@Column(name = "error_log", length = 200)
	private String errorLog;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getExecuteBy() {
		return executeBy;
	}

	public void setExecuteBy(String executeBy) {
		this.executeBy = executeBy;
	}

	public Date getExecuteDate() {
		return executeDate;
	}

	public void setExecuteDate(Date executeDate) {
		this.executeDate = executeDate;
	}

	public String getIsError() {
		return isError;
	}

	public void setIsError(String isError) {
		this.isError = isError;
	}

	public String getErrorLog() {
		return errorLog;
	}

	public void setErrorLog(String errorLog) {
		this.errorLog = errorLog;
	}

	@Override
	public String toString() {
		return "MyBatchLog [id=" + id + ", jobId=" + jobId + ", executeBy="
				+ executeBy + ", executeDate=" + executeDate + ", isError="
				+ isError + ", errorLog=" + errorLog + "]";
	}
}
