package com.hci.jfs.entity;

import java.util.Date;

/**
 *
 * @author Muhammad.Agaputra
 */
public class OpsOperator {

	private String operatorId;
	private String operatorName;
	private String productType;
	private String portionToAssign;	
	private Date dtimeInserted;
	
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getPortionToAssign() {
		return portionToAssign;
	}
	public void setPortionToAssign(String portionToAssign) {
		this.portionToAssign = portionToAssign;
	}
	public Date getDtimeInserted() {
		return dtimeInserted;
	}
	public void setDtimeInserted(Date dtimeInserted) {
		this.dtimeInserted = dtimeInserted;
	}
	
	
	
}