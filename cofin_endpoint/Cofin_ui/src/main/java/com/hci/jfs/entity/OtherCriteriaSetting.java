package com.hci.jfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="JFS_FF_COMMODITY_PARAMETER")
public class OtherCriteriaSetting implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "COMMODITY_TYPE_CODE")
	private String commodityTypeCode;
	
	@Column(name = "TERMS_MIN")
	private Long termsMin;
	
	@Column(name = "TERMS_MAX")	
	private Long termsMax;
	
	@Column(name = "CA_MIN")
	private BigDecimal caMin;
	
	@Column(name = "CA_MAX")
	private BigDecimal caMax;
	
	@Column(name = "IS_DELETE")
	private String isDelete;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;	
	
	@Column(name = "VALID_FROM")
	private Date validFrom;
	
	@Column(name = "VALID_TO")
	private Date validTo;
		
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getCommodityTypeCode() {
		return commodityTypeCode;
	}
	public void setCommodityTypeCode(String commodityTypeCode) {
		this.commodityTypeCode = commodityTypeCode;
	}
	public Long getTermsMin() {
		return termsMin;
	}
	public void setTermsMin(Long termsMin) {
		this.termsMin = termsMin;
	}
	public Long getTermsMax() {
		return termsMax;
	}
	public void setTermsMax(Long termsMax) {
		this.termsMax = termsMax;
	}
	public BigDecimal getCaMin() {
		return caMin;
	}
	public void setCaMin(BigDecimal caMin) {
		this.caMin = caMin;
	}
	public BigDecimal getCaMax() {
		return caMax;
	}
	public void setCaMax(BigDecimal caMax) {
		this.caMax = caMax;
	}
	
	@Override
	public String toString() {
		return "OtherCriteriaSetting [commodityTypeCode=" + commodityTypeCode
				+ ", termsMin=" + termsMin + ", termsMax=" + termsMax
				+ ", caMin=" + caMin + ", caMax=" + caMax + ", isDelete="
				+ isDelete + ", createdBy=" + createdBy + ", createdDate="
				+ createdDate + ", updatedBy=" + updatedBy + ", updatedDate="
				+ updatedDate + ", validFrom=" + validFrom + ", validTo="
				+ validTo + "]";
	}		
}
