package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "jfs_partner")
public class Partner implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String address;
    private String mainContact;
    private String phoneNumber;
    private String email;
    private String isDelete;
    private List<Agreement> agreements = new ArrayList<Agreement>();

    @Column(name = "address", length = 200)
    public String getAddress() {
        return address;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "partner", fetch = FetchType.EAGER)
    public List<Agreement> getAgreements() {
        return agreements;
    }

    @Column(name = "email", length = 100)
    public String getEmail() {
        return email;
    }

    @Id
    @GenericGenerator(name = "partner_generator", strategy = "com.hci.jfs.util.PartnerIdGenerator")
    @GeneratedValue(generator = "partner_generator")
    @Column(name = "id", length = 10)
    public String getId() {
        return id;
    }

    @Column(name = "is_delete", length = 1)
    public String getIsDelete() {
        return isDelete;
    }

    @Column(name = "main_contact", length = 100)
    public String getMainContact() {
        return mainContact;
    }

    @Column(name = "name", length = 50)
    public String getName() {
        return name;
    }

    @Column(name = "phone_number", length = 20)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAgreement(List<Agreement> agreements) {
        this.agreements = agreements;
    }

    public void setAgreements(List<Agreement> agreements) {
        this.agreements = agreements;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public void setMainContact(String mainContact) {
        this.mainContact = mainContact;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Partner [id=" + id + ", name=" + name + ", address=" + address + ", mainContact="
                + mainContact + ", phoneNumber=" + phoneNumber + ", email=" + email + ", isDelete="
                + isDelete + "]";
    }
}