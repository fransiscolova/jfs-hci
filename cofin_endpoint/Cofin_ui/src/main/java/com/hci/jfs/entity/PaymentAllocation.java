package com.hci.jfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_PAYMENT_ALLOCATION")
public class PaymentAllocation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name="ID")
	private String id;
	
	@ManyToOne
	@JoinColumn(name = "TEXT_CONTRACT_NUMBER")
	private Contract contract;
	
	@Column(name = "DUE_DATE")
	private Date dueDate;
	
	@Column(name = "PART_INDEX")
	private Integer partIndex;
	
	@Column(name = "AMT_PRINCIPAL")
	private BigDecimal amtPrincipal;
	
	@Column(name = "AMT_INTEREST")
	private BigDecimal amtInterest;
	
	@Column(name = "ARCHIVED")
	private Integer archieved;
	
	@Column(name = "DTIME_CREATED")
	private Date dtimeCreated;
	
	@Column(name = "DTIME_UPDATED")
	private Date dtimeUpdated;
	
	@Column(name = "PAYMENT_TYPE")
	private String paymentType;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "DATE_UPLOAD")
	private Date dateUpload;
	
	@Column(name = "FILENAME")
	private String filename;
		
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Date getDateUpload() {
		return dateUpload;
	}

	public void setDateUpload(Date dateUpload) {
		this.dateUpload = dateUpload;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Integer getPartIndex() {
		return partIndex;
	}

	public void setPartIndex(Integer partIndex) {
		this.partIndex = partIndex;
	}

	public BigDecimal getAmtPrincipal() {
		return amtPrincipal;
	}

	public void setAmtPrincipal(BigDecimal amtPrincipal) {
		this.amtPrincipal = amtPrincipal;
	}

	public BigDecimal getAmtInterest() {
		return amtInterest;
	}

	public void setAmtInterest(BigDecimal amtInterest) {
		this.amtInterest = amtInterest;
	}

	public Integer getArchieved() {
		return archieved;
	}

	public void setArchieved(Integer archieved) {
		this.archieved = archieved;
	}

	public Date getDtimeCreated() {
		return dtimeCreated;
	}

	public void setDtimeCreated(Date dtimeCreated) {
		this.dtimeCreated = dtimeCreated;
	}

	public Date getDtimeUpdated() {
		return dtimeUpdated;
	}

	public void setDtimeUpdated(Date dtimeUpdated) {
		this.dtimeUpdated = dtimeUpdated;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	@Override
	public String toString() {
		return "PaymentAllocation [id=" + id + ", contract=" + contract
				+ ", dueDate=" + dueDate + ", partIndex=" + partIndex
				+ ", amtPrincipal=" + amtPrincipal + ", amtInterest="
				+ amtInterest + ", archieved=" + archieved + ", dtimeCreated="
				+ dtimeCreated + ", dtimeUpdated=" + dtimeUpdated
				+ ", paymentType=" + paymentType + ", createdBy=" + createdBy
				+ ", dateUpload=" + dateUpload + ", filename=" + filename + "]";
	}
	
}
