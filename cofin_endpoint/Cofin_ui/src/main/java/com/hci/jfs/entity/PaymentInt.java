package com.hci.jfs.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_PAYMENT_INT")
public class PaymentInt {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "ID")
	private String id;
	
	@Column(name = "INCOMING_PAYMENT_ID")
	private BigDecimal incomingPaymentId;
	
	@ManyToOne
	@JoinColumn(name = "TEXT_CONTRACT_NUMBER")
	private Contract textContractNumber;

	@Column(name = "TOTAL_AMT_PAYMENT")
	private BigDecimal totalAmtPayment;

	@Column(name = "AMT_PAYMENT")
	private BigDecimal amtPayment;

	@Column(name = "DATE_PAYMENT")
	private Date datePayment;

	@Column(name = "DATE_EXPORT")
	private Date dateExport;

	@Column(name = "PMT_INSTALMENT")
	private BigDecimal pmtInstalment;

	@Column(name = "PMT_FEE")
	private BigDecimal pmtFee;

	@Column(name = "PMT_PENALTY")
	private BigDecimal pmtPenalty;

	@Column(name = "PMT_OVERPAYMENT")
	private BigDecimal pmtOverpayment;

	@Column(name = "PMT_OTHER")
	private BigDecimal pmtOther;

	@Column(name = "PARENT_ID",nullable=true)
	private Long parentId;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "REASON")
	private String reason;

	@Column(name = "DTIME_STATUS_UPDATED")
	private Date DtimeStatusUpdated;

	@Column(name = "PAYMENT_TYPE")
	private String paymentType;

	@Column(name = "DATE_BANK_PROCESS")
	private Date dateBankProcess;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "DATE_UPLOAD")
	private Date dateUpload;
	
	@Column(name = "FILENAME")
	private String filename;

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpload() {
		return dateUpload;
	}

	public void setDateUpload(Date dateUpload) {
		this.dateUpload = dateUpload;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public BigDecimal getIncomingPaymentId() {
		return incomingPaymentId;
	}

	public void setIncomingPaymentId(BigDecimal incomingPaymentId) {
		this.incomingPaymentId = incomingPaymentId;
	}

	public Contract getTextContractNumber() {
		return textContractNumber;
	}

	public void setTextContractNumber(Contract textContractNumber) {
		this.textContractNumber = textContractNumber;
	}

	public BigDecimal getTotalAmtPayment() {
		return totalAmtPayment;
	}

	public void setTotalAmtPayment(BigDecimal totalAmtPayment) {
		this.totalAmtPayment = totalAmtPayment;
	}

	public BigDecimal getAmtPayment() {
		return amtPayment;
	}

	public void setAmtPayment(BigDecimal amtPayment) {
		this.amtPayment = amtPayment;
	}

	public Date getDatePayment() {
		return datePayment;
	}

	public void setDatePayment(Date datePayment) {
		this.datePayment = datePayment;
	}

	public Date getDateExport() {
		return dateExport;
	}

	public void setDateExport(Date dateExport) {
		this.dateExport = dateExport;
	}
	
	public BigDecimal getPmtInstalment() {
		return pmtInstalment;
	}

	public void setPmtInstalment(BigDecimal pmtInstalment) {
		this.pmtInstalment = pmtInstalment;
	}

	public BigDecimal getPmtFee() {
		return pmtFee;
	}

	public void setPmtFee(BigDecimal pmtFee) {
		this.pmtFee = pmtFee;
	}

	public BigDecimal getPmtPenalty() {
		return pmtPenalty;
	}

	public void setPmtPenalty(BigDecimal pmtPenalty) {
		this.pmtPenalty = pmtPenalty;
	}

	public BigDecimal getPmtOverpayment() {
		return pmtOverpayment;
	}

	public void setPmtOverpayment(BigDecimal pmtOverpayment) {
		this.pmtOverpayment = pmtOverpayment;
	}

	public BigDecimal getPmtOther() {
		return pmtOther;
	}

	public void setPmtOther(BigDecimal pmtOther) {
		this.pmtOther = pmtOther;
	}	
	
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getDtimeStatusUpdated() {
		return DtimeStatusUpdated;
	}

	public void setDtimeStatusUpdated(Date dtimeStatusUpdated) {
		DtimeStatusUpdated = dtimeStatusUpdated;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Date getDateBankProcess() {
		return dateBankProcess;
	}

	public void setDateBankProcess(Date dateBankProcess) {
		this.dateBankProcess = dateBankProcess;
	}

	@Override
	public String toString() {
		return "PaymentInt [id=" + id + ", incomingPaymentId="
				+ incomingPaymentId + ", textContractNumber="
				+ textContractNumber + ", totalAmtPayment=" + totalAmtPayment
				+ ", amtPayment=" + amtPayment + ", datePayment=" + datePayment
				+ ", dateExport=" + dateExport + ", pmtInstalment="
				+ pmtInstalment + ", pmtFee=" + pmtFee + ", pmtPenalty="
				+ pmtPenalty + ", pmtOverpayment=" + pmtOverpayment
				+ ", pmtOther=" + pmtOther + ", parentId=" + parentId
				+ ", status=" + status + ", reason=" + reason
				+ ", DtimeStatusUpdated=" + DtimeStatusUpdated
				+ ", paymentType=" + paymentType + ", dateBankProcess="
				+ dateBankProcess + ", createdBy=" + createdBy
				+ ", dateUpload=" + dateUpload + ", filename=" + filename + "]";
	}

}
