package com.hci.jfs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="JFS_PROCESS_TYPE")
public class ProcessType {
	
	@Id
	@Column(name = "CODE",unique =true)
	private String code;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "IS_DELETE")
	private String isDelete;

	@Column(name = "TYPE_FILE")
	private String typeFile;
	
	@Column(name = "SEQUENCE")
	private Integer sequence;
	
	public ProcessType() {
		super();
	}
		
	public ProcessType(String code) {
		super();
		this.code = code;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	
	public String getTypeFile() {
		return typeFile;
	}


	public void setTypeFile(String typeFile) {
		this.typeFile = typeFile;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "ProcessType [code=" + code + ", name=" + name
				+ ", description=" + description + ", isDelete=" + isDelete
				+ ", typeFile=" + typeFile + ", sequence=" + sequence.intValue() + "]";
	}
}
