package com.hci.jfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_PRODUCT")
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name="ID")
	private String idProduct;

	@Column(name="BANK_PRODUCT_CODE")
	private String bankProductCode;

	@Column(name="INTEREST_RATE")
	private BigDecimal interestRate;

	@Column(name="PRINCIPAL_SPLIT_RATE")
	private BigDecimal principalSplitRate;

	@Column(name="VALID_FROM")
	private Date validFrom;

	@Column(name="VALID_TO")
	private Date validTo;

	@Column(name="ID_AGREEMENT")
	private Long agreement;

	@Column(name="ADMIN_FEE_RATE")
	private BigDecimal adminFeeRate;

	public Product() {
		super();
	}

	public Product(long agreement) {
		super();
		this.agreement = agreement;
	}

	public String getBankProductCode() {
		return bankProductCode;
	}

	public void setBankProductCode(String bankProductCode) {
		this.bankProductCode = bankProductCode;
	}

	public String getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public BigDecimal getPrincipalSplitRate() {
		return principalSplitRate;
	}

	public void setPrincipalSplitRate(BigDecimal principalSplitRate) {
		this.principalSplitRate = principalSplitRate;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Long getAgreement() {
		return agreement;
	}

	public void setAgreement(long agreement) {
		this.agreement = agreement;
	}

	public BigDecimal getAdminFeeRate() {
		return adminFeeRate;
	}

	public void setAdminFeeRate(BigDecimal adminFeeRate) {
		this.adminFeeRate = adminFeeRate;
	}

	@Override
	public String toString() {
		return "Product [idProduct=" + idProduct + ", bankProductCode="
				+ bankProductCode + ", interestRate=" + interestRate
				+ ", principalSplitRate=" + principalSplitRate + ", validFrom="
				+ validFrom + ", validTo=" + validTo + ", agreement="
				+ agreement + ", adminFeeRate=" + adminFeeRate + "]";
	}

}
