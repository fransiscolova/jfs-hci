package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_PRODUCT_HCI")
public class ProductHCI implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name="ID")
	private String idProduct;
	
	@Column(name="CODE_PRODUCT")
	private String codeProduct;
	
	@Column(name="NAME_PRODUCT")
	private String nameProduct;
	
	@Column(name="ID_AGREEMENT")
	private Long agreement;
	
	@Column(name="DTIME_INSERTED")
	private Date dtimeInserted;
	
	public ProductHCI() {
		super();
	}
	
	public ProductHCI(Long agreement) {
		super();
		this.agreement = agreement;
	}

	public String getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}

	public String getCodeProduct() {
		return codeProduct;
	}

	public void setCodeProduct(String codeProduct) {
		this.codeProduct = codeProduct;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public Long getAgreement() {
		return agreement;
	}

	public void setAgreement(Long agreement) {
		this.agreement = agreement;
	}

	public Date getDtimeInserted() {
		return dtimeInserted;
	}

	public void setDtimeInserted(Date dtimeInserted) {
		this.dtimeInserted = dtimeInserted;
	}

	@Override
	public String toString() {
		return "ProductHCI [idProduct=" + idProduct + ", codeProduct="
				+ codeProduct + ", nameProduct=" + nameProduct + ", agreement="
				+ agreement + ", dtimeInserted=" + dtimeInserted + "]";
	}	
}
