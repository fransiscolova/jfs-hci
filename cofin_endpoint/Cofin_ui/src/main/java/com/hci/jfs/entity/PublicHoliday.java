package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="JFS_PUBLIC_HOLIDAY_ID")
public class PublicHoliday implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DATE_FROM")
	private Date dateFrom;
	
	@Column(name="DATE_TO")
	private Date dateTo;
	
	@Column(name="DATE_GENERATE")
	private Date dateGenerate;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="DTIME_INSERTED")
	private Date dtimeInserted;
	
	public PublicHoliday() {
		super();
	}

	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	public Date getDateGenerate() {
		return dateGenerate;
	}
	public void setDateGenerate(Date dateGenerate) {
		this.dateGenerate = dateGenerate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDtimeInserted() {
		return dtimeInserted;
	}
	public void setDtimeInserted(Date dtimeInserted) {
		this.dtimeInserted = dtimeInserted;
	}

	@Override
	public String toString() {
		return "PublicHoliday [dateFrom=" + dateFrom + ", dateTo=" + dateTo
				+ ", dateGenerate=" + dateGenerate + ", description="
				+ description + ", dtimeInserted=" + dtimeInserted + "]";
	}
}
