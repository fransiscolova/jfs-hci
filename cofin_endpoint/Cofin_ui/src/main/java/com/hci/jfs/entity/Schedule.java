package com.hci.jfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="JFS_SCHEDULE")
public class Schedule implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1255669814987049687L;
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name="ID")
	private String id;
	
	@ManyToOne
	@JoinColumn(name="TEXT_CONTRACT_NUMBER")
	private Contract contract;
	
	@Column(name = "DUE_DATE")
	private Date dueDate;
	
	@Column(name = "PART_INDEX")
	private Integer partIndex;
	
	@Column(name = "PRINCIPAL")
	private BigDecimal principal;
	
	@Column(name = "INTEREST")
	private BigDecimal interest;
	
	@Column(name = "FEE")
	private BigDecimal fee;
	
	@Column(name = "OUTSTANDING_PRINCIPAL")
	private BigDecimal outstandingPrincipal;
	
	@Column(name = "PREV_BALANCE")
	private BigDecimal prevBalance;
	
	@Column(name = "BALANCE")
	private BigDecimal balance;
	
	@Column(name = "PENALTY")
	private BigDecimal penalty;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "DATE_UPLOAD")
	private Date dateUpload;
	
	@Column(name = "FILENAME")
	private String filename;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getDateUpload() {
		return dateUpload;
	}
	public void setDateUpload(Date dateUpload) {
		this.dateUpload = dateUpload;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Integer getPartIndex() {
		return partIndex;
	}
	public void setPartIndex(Integer partIndex) {
		this.partIndex = partIndex;
	}
	public BigDecimal getPrincipal() {
		return principal;
	}
	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}
	public BigDecimal getInterest() {
		return interest;
	}
	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}
	public BigDecimal getFee() {
		return fee;
	}
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	public BigDecimal getOutstandingPrincipal() {
		return outstandingPrincipal;
	}
	public void setOutstandingPrincipal(BigDecimal outstandingPrincipal) {
		this.outstandingPrincipal = outstandingPrincipal;
	}
	public BigDecimal getPrevBalance() {
		return prevBalance;
	}
	public void setPrevBalance(BigDecimal prevBalance) {
		this.prevBalance = prevBalance;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public BigDecimal getPenalty() {
		return penalty;
	}
	public void setPenalty(BigDecimal penalty) {
		this.penalty = penalty;
	}
	@Override
	public String toString() {
		return "Schedule [id=" + id + ", contract=" + contract + ", dueDate="
				+ dueDate + ", partIndex=" + partIndex + ", principal="
				+ principal + ", interest=" + interest + ", fee=" + fee
				+ ", outstandingPrincipal=" + outstandingPrincipal
				+ ", prevBalance=" + prevBalance + ", balance=" + balance
				+ ", penalty=" + penalty + ", createdBy=" + createdBy
				+ ", dateUpload=" + dateUpload + ", filename=" + filename + "]";
	}	
}
