package com.hci.jfs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="JFS_FILE_UPLOAD_DETAIL")
public class UploadFile implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CHECKSUM")
	private String checksum;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "AGREEMENT_CODE")
	private Agreement agreement;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "PARTNER_CODE")
	private Partner partner;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "PROCESS_CODE")
	private ProcessType processType;

	@Column(name = "FILEPATH")
	private String filepath;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "SIZE_FILE")
	private String sizeFile;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "FILE_TYPE")
	private String fileType;

	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public Agreement getAgreement() {
		return agreement;
	}

	public void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public ProcessType getProcessType() {
		return processType;
	}

	public void setProcessType(ProcessType processType) {
		this.processType = processType;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSizeFile() {
		return sizeFile;
	}

	public void setSizeFile(String sizeFile) {
		this.sizeFile = sizeFile;
	}

	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Override
	public String toString() {
		return "UploadFile [checksum=" + checksum + ", agreement=" + agreement
				+ ", partner=" + partner + ", processType=" + processType
				+ ", filepath=" + filepath + ", createdDate=" + createdDate
				+ ", name=" + name + ", sizeFile=" + sizeFile + ", createdBy="
				+ createdBy + ", fileType=" + fileType + "]";
	}

	
}
