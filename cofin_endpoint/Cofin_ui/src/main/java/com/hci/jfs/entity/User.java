package com.hci.jfs.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Muhammad.Agaputra
 */
public class User {

    private String username;
    private String password;
    private int enabled;
    private List<Role> roles = new ArrayList<Role>();

    public User(){
    	
    }
    
    public User(String username){ 
    	this.username = username;
    }
    
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getEnabled() {
		return enabled;
	}
	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}          
}