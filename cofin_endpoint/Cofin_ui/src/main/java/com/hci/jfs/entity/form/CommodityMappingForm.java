package com.hci.jfs.entity.form;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.hci.jfs.entity.CommodityCategory;
import com.hci.jfs.entity.CommodityType;
import com.hci.jfs.entity.EconomicSector;


public class CommodityMappingForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idCommodity;
	private String hciCodeCommodityCategory;
	private EconomicSector bptnCommodityGroup;
	private String description;
	private Long idAgreement;
	private Date validTo;
	private Date validFrom;
	private String isDelete;
	
	List<CommodityCategory> listCommodityCategory;
	List<CommodityType> listCommodityType;
	public CommodityMappingForm() {
		super();
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public String getIdCommodity() {
		return idCommodity;
	}
	public void setIdCommodity(String idCommodity) {
		this.idCommodity = idCommodity;
	}
	public String getHciCodeCommodityCategory() {
		return hciCodeCommodityCategory;
	}
	public void setHciCodeCommodityCategory(String hciCodeCommodityCategory) {
		this.hciCodeCommodityCategory = hciCodeCommodityCategory;
	}
	public EconomicSector getBptnCommodityGroup() {
		return bptnCommodityGroup;
	}
	public void setBptnCommodityGroup(EconomicSector bptnCommodityGroup) {
		this.bptnCommodityGroup = bptnCommodityGroup;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getIdAgreement() {
		return idAgreement;
	}
	public void setIdAgreement(Long idAgreement) {
		this.idAgreement = idAgreement;
	}
	public List<CommodityCategory> getListCommodityCategory() {
		return listCommodityCategory;
	}
	public void setListCommodityCategory(
			List<CommodityCategory> listCommodityCategory) {
		this.listCommodityCategory = listCommodityCategory;
	}
	public List<CommodityType> getListCommodityType() {
		return listCommodityType;
	}
	public void setListCommodityType(List<CommodityType> listCommodityType) {
		this.listCommodityType = listCommodityType;
	}	
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	@Override
	public String toString() {
		return "CommodityMappingForm [idCommodity=" + idCommodity
				+ ", hciCodeCommodityCategory=" + hciCodeCommodityCategory
				+ ", bptnCommodityGroup=" + bptnCommodityGroup
				+ ", description=" + description + ", idAgreement="
				+ idAgreement + ", validTo=" + validTo + ", validFrom="
				+ validFrom + ", isDelete=" + isDelete
				+ ", listCommodityCategory=" + listCommodityCategory
				+ ", listCommodityType=" + listCommodityType + "]";
	}
}
