package com.hci.jfs.entity.form;

import java.io.Serializable;
import java.util.Date;

import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.Product;
import com.hci.jfs.entity.ProductHCI;

public class ProductMappingForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idProduct;
	private String codeProduct;
	private String bankProductCode;
	private Date validFrom;
	private Date validTo;
	private Long Agreement;
	private Product product;
	private ProductHCI productHCI;
	private String isDelete;
	private String isDefault;	
	
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public String getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
	public String getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
	public String getCodeProduct() {
		return codeProduct;
	}
	public void setCodeProduct(String codeProduct) {
		this.codeProduct = codeProduct;
	}
	public String getBankProductCode() {
		return bankProductCode;
	}
	public void setBankProductCode(String bankProductCode) {
		this.bankProductCode = bankProductCode;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public Long getAgreement() {
		return Agreement;
	}
	public void setAgreement(Long agreement) {
		Agreement = agreement;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public ProductHCI getProductHCI() {
		return productHCI;
	}
	public void setProductHCI(ProductHCI productHCI) {
		this.productHCI = productHCI;
	}
	
	@Override
	public String toString() {
		return "ProductMappingForm [idProduct=" + idProduct + ", codeProduct="
				+ codeProduct + ", bankProductCode=" + bankProductCode
				+ ", validFrom=" + validFrom + ", validTo=" + validTo
				+ ", Agreement=" + Agreement + ", product=" + product
				+ ", productHCI=" + productHCI + ", isDelete=" + isDelete
				+ ", isDefault=" + isDefault + "]";
	}
}
