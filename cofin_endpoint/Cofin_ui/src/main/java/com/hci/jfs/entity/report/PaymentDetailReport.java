package com.hci.jfs.entity.report;

public class PaymentDetailReport {
	// Payment
	private String ref;
	private String textContractNumber;
	private String transactionDate;
	private String amtPayment;
	
	//Contract Registration & Disbursement
	private String financingPortionHCI;
	private String financingPortionBTPN;
	private String numberOfEligibleAgreement;
	
	// Early Termination Payment
	private String dateFinancingBTPNPortionApproval;
	private String customerName;
	private String dateCFASigned;
	private String financingPortionDisbursementAmount;
	private String totalPaidInstallment;
	private String totalEarlyTerminationAmount;
	private String pmtFee;
	
	// Write Off
	private String recoveryAmount;
	
	// General
	private String totalAmount;
	private String size;
	
	public PaymentDetailReport() {
		super();
	}
	
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getTextContractNumber() {
		return textContractNumber;
	}
	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getAmtPayment() {
		return amtPayment;
	}
	public void setAmtPayment(String amtPayment) {
		this.amtPayment = amtPayment;
	}
			
	public String getFinancingPortionHCI() {
		return financingPortionHCI;
	}

	public void setFinancingPortionHCI(String financingPortionHCI) {
		this.financingPortionHCI = financingPortionHCI;
	}

	public String getFinancingPortionBTPN() {
		return financingPortionBTPN;
	}

	public void setFinancingPortionBTPN(String financingPortionBTPN) {
		this.financingPortionBTPN = financingPortionBTPN;
	}

	public String getNumberOfEligibleAgreement() {
		return numberOfEligibleAgreement;
	}

	public void setNumberOfEligibleAgreement(String numberOfEligibleAgreement) {
		this.numberOfEligibleAgreement = numberOfEligibleAgreement;
	}
	
	public String getDateFinancingBTPNPortionApproval() {
		return dateFinancingBTPNPortionApproval;
	}

	public void setDateFinancingBTPNPortionApproval(
			String dateFinancingBTPNPortionApproval) {
		this.dateFinancingBTPNPortionApproval = dateFinancingBTPNPortionApproval;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDateCFASigned() {
		return dateCFASigned;
	}

	public void setDateCFASigned(String dateCFASigned) {
		this.dateCFASigned = dateCFASigned;
	}

	public String getFinancingPortionDisbursementAmount() {
		return financingPortionDisbursementAmount;
	}

	public void setFinancingPortionDisbursementAmount(
			String financingPortionDisbursementAmount) {
		this.financingPortionDisbursementAmount = financingPortionDisbursementAmount;
	}

	public String getTotalPaidInstallment() {
		return totalPaidInstallment;
	}

	public void setTotalPaidInstallment(String totalPaidInstallment) {
		this.totalPaidInstallment = totalPaidInstallment;
	}

	public String getTotalEarlyTerminationAmount() {
		return totalEarlyTerminationAmount;
	}

	public void setTotalEarlyTerminationAmount(String totalEarlyTerminationAmount) {
		this.totalEarlyTerminationAmount = totalEarlyTerminationAmount;
	}
	
	public String getPmtFee() {
		return pmtFee;
	}

	public void setPmtFee(String pmtFee) {
		this.pmtFee = pmtFee;
	}

	public String getRecoveryAmount() {
		return recoveryAmount;
	}

	public void setRecoveryAmount(String recoveryAmount) {
		this.recoveryAmount = recoveryAmount;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
}
