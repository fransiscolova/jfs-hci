package com.hci.jfs.entity.report;

import java.util.List;

public class PaymentReport {

	private String letterNumber;
	private String letterDate;
	private String totalDebtor;
	private String totalInstallment;
	
	private List<PaymentDetailReport> paymentDetails;

	public String getLetterNumber() {
		return letterNumber;
	}

	public void setLetterNumber(String letterNumber) {
		this.letterNumber = letterNumber;
	}

	public String getLetterDate() {
		return letterDate;
	}

	public void setLetterDate(String letterDate) {
		this.letterDate = letterDate;
	}

	public String getTotalDebtor() {
		return totalDebtor;
	}

	public void setTotalDebtor(String totalDebtor) {
		this.totalDebtor = totalDebtor;
	}

	public String getTotalInstallment() {
		return totalInstallment;
	}

	public void setTotalInstallment(String totalInstallment) {
		this.totalInstallment = totalInstallment;
	}

	public List<PaymentDetailReport> getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(List<PaymentDetailReport> paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	@Override
	public String toString() {
		return "PaymentReport [letterNumber=" + letterNumber + ", letterDate="
				+ letterDate + ", totalDebtor=" + totalDebtor
				+ ", totalInstallment=" + totalInstallment
				+ ", paymentDetails=" + paymentDetails + "]";
	}



}
