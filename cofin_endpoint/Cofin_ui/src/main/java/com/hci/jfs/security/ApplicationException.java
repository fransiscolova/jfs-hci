package com.hci.jfs.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="APPLICATION_ERROR")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationException", propOrder = {
    "errorCode","errorMessage","description"
})
public class ApplicationException extends Exception{

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ERROR_CODE", length = 40, unique =true)
	private String errorCode;
	
	@Column(name = "ERROR_MESSAGE", length = 200)
	private String errorMessage;
	
	@Column(name = "DESCRIPTION", length = 400)
	private String description;
	
	
	public ApplicationException(String errorCode, String errorMessage,
			String description) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.description = description;
	}	
	
	public ApplicationException(String errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}	

	public ApplicationException(String errorCode) {
		super();
		this.errorCode = errorCode;
	}
	

	public ApplicationException() {
		super();
	}

	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "ApplicationException [errorCode=" + errorCode
				+ ", errorMessage=" + errorMessage + ", description="
				+ description + "]";
	}
	
}
