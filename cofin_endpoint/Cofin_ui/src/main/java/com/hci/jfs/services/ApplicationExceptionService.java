package com.hci.jfs.services;

import java.util.List;

import com.hci.jfs.security.ApplicationException;

public interface ApplicationExceptionService {
	
	public ApplicationException getApplicationExceptionByErrorCode(String error) throws Exception;
	
	public List<ApplicationException> getApplicationExceptionAll() throws Exception;
	
}
