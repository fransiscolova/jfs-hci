package com.hci.jfs.services;

import java.util.Map;

import com.hci.jfs.activitylog.LogGenerateUploadFile;

public interface BatchProcessDescriptionService {
	public void exportFileBasedOnProcessType(Map<String,Object> param) throws Exception;
	
	public void uploadValidationFileBasedOnProcessType(Map<String,Object> param) throws Exception;
	
	public void uploadConvertFileBasedOnProcessType(Map<String,Object> param) throws Exception;
	
	public void uploadValidationFileAllocationBasedOnProcessType(Map<String,Object> param) throws Exception;
	
	public void uploadConvertFileAllocationBasedOnProcessType(Map<String,Object> param) throws Exception;
	
	public void checkingLogGenerateUploadFile(Map<String,Object> param) throws Exception;

	public LogGenerateUploadFile saveLogGenerateUploadFile(Map<String,Object> param) throws Exception;
}
