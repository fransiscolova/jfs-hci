package com.hci.jfs.services;

/**
 *
 * @author Muhammad.Agaputra
 */
import com.hci.jfs.SidebarPage;
import com.hci.jfs.SidebarPageConfig;
import com.hci.jfs.SidebarPageConfigImpl;

import java.util.Collections;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;

public class BookmarkChangeViewModel {

    //todo: wire service
    private SidebarPageConfig pageConfig = new SidebarPageConfigImpl();

    @Command("onBookmarkNavigate")
    public void onBookmarkNavigate(@BindingParam("bookmark") String bookmark) {
        if (bookmark.startsWith("p_")) {
            //retrieve page from config
            String p = bookmark.substring("p_".length());
            SidebarPage page = pageConfig.getPage(p);
            if (page != null) {
                //and post command to NavigationViewModel
                BindUtils.postGlobalCommand(null, null, "onNavigate", Collections.<String, Object>singletonMap("page", page));
            }
        }
    }
}
