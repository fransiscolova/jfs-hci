package com.hci.jfs.services;

import java.util.Map;

public interface ClawBackService {
    /**
     * Export pdf for JFS clawback MPF.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSClawbackMPF(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS clawback registration.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSClawbackRegistration(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

}
