package com.hci.jfs.services;

import java.util.List;

import com.hci.jfs.entity.CommodityCategory;
import com.hci.jfs.entity.CommodityGroup;
import com.hci.jfs.entity.CommodityPurpose;
import com.hci.jfs.entity.CommodityType;
import com.hci.jfs.entity.OtherCriteriaSetting;
import com.hci.jfs.entity.form.CommodityMappingForm;

public interface CommodityService {
	public List<CommodityCategory> getAllCommodityCategory() throws Exception;
	public List<CommodityType> getAllCommodityType() throws Exception;
	public List<CommodityGroup> getAllCommodityGroup() throws Exception;
	public List<CommodityPurpose> getAllCommodityPurpose() throws Exception;

	public List<CommodityCategory> getCommodityCategoryByHciCodeCommodityCategory(String codeCommodityCategory) throws Exception;
	public List<CommodityGroup> getCommodityGroupByAgreementCode(Long agreementCode) throws Exception;
	public List<CommodityType> getCommodityTypeByHciCodeCommodityCategory(String codeCommodityCategory) throws Exception;
	
	public List<CommodityMappingForm> getCommodityGroupTypeByAgreementCode(Long agreementCode) throws Exception;
	
	public List<CommodityType> getCommodityTypeByAgreementCode(Long agreementCode) throws Exception;
	public List<OtherCriteriaSetting> getAllCriteriaSetting() throws Exception;
}
