package com.hci.jfs.services;

import java.util.Map;

public interface ContractRegistrationDisbursementService {
    /**
     *
     * @param param
     * @throws Exception
     */
    public void convertJFSContractRegistrationAndDisbursement(Map<String, Object> param)
            throws Exception;

    /**
     * Export pdf file for JFS MPF Product.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSMPFProducts(Map<String, Object> param) throws Exception;

    /**
     * Export pdf file for JFS Standard Easy 10 Product.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSStandardEasy10Products(Map<String, Object> param) throws Exception;

    /**
     * Export pdf file for JFS Zero Product.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSZeroProducts(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void navigateToUpload(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    //
    // public List<String> validationFileUpload(Map<String,Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void uploadJFContractRegistration(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void uploadJSFDisbursement(Map<String, Object> param) throws Exception;
}
