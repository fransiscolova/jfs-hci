package com.hci.jfs.services;

import java.util.Map;

public interface EarlyTerminationService {
    /**
     *
     * @param param
     * @throws Exception
     */
    public void convertJFSEarlyTerminationAfterFile(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void convertJFSEarlyTerminationWithinFile(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS early termination after 15 days.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSEarlyTerminationAfter15Days(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS early termination within 15 days.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSEarlyTerminationWithin15Days(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void mergeJFSETAfterStatus(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void updateContractStatusETAfter(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void uploadJFSEarlyTerminationAfterFile(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void uploadJFSEarlyTerminationWithinFile(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void validationContractETAfter(Map<String, Object> param) throws Exception;

}
