package com.hci.jfs.services;

import java.util.List;

import com.hci.jfs.entity.ErrorTypeContract;
import com.hci.jfs.security.ApplicationException;


public interface ErrorTypeContractService  {

	public List<ErrorTypeContract> getAllErrorTypeContract() throws ApplicationException;
	
	public void insertErrorTypeContract(ErrorTypeContract errorTypeContract) throws ApplicationException;
	
}
