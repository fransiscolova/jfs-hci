package com.hci.jfs.services;

import java.util.List;

import com.hci.jfs.entity.ErrorContractMonitoring;
import com.hci.jfs.security.ApplicationException;

public interface MonitoringContractErrorService {

	public void insert(List<ErrorContractMonitoring> list) throws ApplicationException;
}
