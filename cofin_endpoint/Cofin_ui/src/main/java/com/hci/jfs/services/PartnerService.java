package com.hci.jfs.services;

import java.util.List;

import com.hci.jfs.entity.Partner;
import com.hci.jfs.ws.entity.Commodity;
import com.hci.jfs.ws.entity.Contract;
import com.hci.jfs.ws.entity.ContractJFSPartnership;
import com.hci.jfs.ws.entity.Customer;
import com.hci.jfs.ws.entity.Product;

public interface PartnerService {
	public List<Partner> getAllPartner();
	public Partner getPartner(String partnerId);
	public void changeParterNactiveStatus(String partnerId, String flag);
	public void updatePartnerData(Partner partner);
	public void addPartnerData(Partner partner);
	public Partner getPartnerByContractNumber(String contractNumber) throws Exception;

	public ContractJFSPartnership GetContractJFSPartnership(			
			Contract contract, 
			Customer customer, 
			Product product,
			Commodity commodity);

}
