package com.hci.jfs.services;

import java.util.Map;

public interface PaymentIntService {

    /**
     *
     * @param param
     * @throws Exception
     */
    public void convertJFSPaymentAllocationFile(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void convertJFSPaymentFile(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS payment gift.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSPaymentGift(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS payment last payment.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSPaymentLastPayment(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS payment regular.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSPaymentRegular(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS payment reversal.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSPaymentReversal(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS payment small underpayment.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSPaymentSmallUnderpayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void mergeJFSStatus(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void uploadJFSPaymentAllocationFile(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void uploadJFSPaymentFile(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void validationContractPaymentAllocation(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void validationContractPaymentConfirmation(Map<String, Object> param) throws Exception;
}
