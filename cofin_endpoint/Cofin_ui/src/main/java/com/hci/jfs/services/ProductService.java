package com.hci.jfs.services;

import java.util.List;

import com.hci.jfs.entity.OtherCriteriaSetting;
import com.hci.jfs.entity.Product;
import com.hci.jfs.entity.ProductHCI;
import com.hci.jfs.entity.ProductMapping;
import com.hci.jfs.entity.PublicHoliday;
import com.hci.jfs.entity.form.ProductMappingForm;

public interface ProductService {

	public List<Product> getAllProduct() throws Exception;
	public List<ProductHCI> getAllProductHCI() throws Exception;
	public List<ProductMapping> getAllProductMapping() throws Exception;
	public List<Product> getProductByAgreement(Long agreement) throws Exception;
	public List<ProductMapping> getProductMappingByAgreementCode(Long agreementCode) throws Exception;
	public List<ProductMappingForm> getProductMappingFormByAgreementCode(Long agreementCode) throws Exception;
	public List<ProductHCI> getProductHCIByAgreementCode(Long agreementCode) throws Exception;
	public ProductHCI getProductHCIByCodeProductByAgreementCode(String codeProduct,Long agreementCode) throws Exception;
	public Product getProductByBankProductCodeByAgreementCode(String bankProductCode,Long agreementCode) throws Exception;
	
	public List<PublicHoliday> getCurrentPublicHoliday() throws Exception;
	public ProductHCI getProductHCIByIdProduct(String id) throws Exception;
	public ProductMapping getProductMappingByIdProduct(String id) throws Exception;
}
