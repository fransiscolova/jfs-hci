package com.hci.jfs.services;

import java.util.List;

import com.hci.jfs.entity.SystemParameter;
import com.hci.jfs.security.ApplicationException;

public interface SystemParameterService {

	public List<SystemParameter> getAllSystemParameters() throws ApplicationException;
	
	public SystemParameter getSystemParameterById(String idCode) throws ApplicationException;
}
