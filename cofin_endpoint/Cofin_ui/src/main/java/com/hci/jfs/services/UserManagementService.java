package com.hci.jfs.services;

import java.util.List;

import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.Role;
import com.hci.jfs.security.ApplicationException;

public interface UserManagementService {
	public List<Employee> getAllEmployee() throws ApplicationException;
	public List<Role> getRolesByUserName (String username) throws ApplicationException;
	public Employee getUserByUserName(String username) throws ApplicationException;
	public void deleteRolesByUserName(String username) throws ApplicationException;
	public void insertRolesByUsername(List<Role> listRoles) throws ApplicationException;
	
}
