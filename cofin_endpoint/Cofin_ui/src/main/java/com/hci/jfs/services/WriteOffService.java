package com.hci.jfs.services;

import java.util.Map;

public interface WriteOffService {
    /**
     *
     * @param param
     * @throws Exception
     */
    public void convertJFSWriteOffFile(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS write off contract registration.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSWriteOffConctractRegistration(Map<String, Object> param) throws Exception;

    /**
     * Export pdf for JFS write off recovery payment.
     *
     * @param param
     * @throws Exception
     */
    public void exportJFSWriteOffRecoveryPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception;

    /**
     *
     * @param param
     * @throws Exception
     */
    public void uploadJFSWriteOffFile(Map<String, Object> param) throws Exception;

}
