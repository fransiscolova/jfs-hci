package com.hci.jfs.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ApplicationExceptionService;

public class ApplicationExceptionServiceImpl extends BaseDAO implements
		ApplicationExceptionService {

	@Override
	public ApplicationException getApplicationExceptionByErrorCode(String error)
			throws Exception {
		Criteria criteria =this.getCurrentSession().createCriteria(ApplicationException.class).add(Restrictions.eq("errorCode", error));
		return (ApplicationException) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApplicationException> getApplicationExceptionAll()
			throws Exception {
		Criteria criteria =this.getCurrentSession().createCriteria(ApplicationException.class);
		List<ApplicationException> result = new ArrayList<ApplicationException>(criteria.list());
		return result;
	}

}
