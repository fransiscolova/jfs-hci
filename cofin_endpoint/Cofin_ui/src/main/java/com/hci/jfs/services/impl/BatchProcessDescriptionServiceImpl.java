package com.hci.jfs.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.hci.jfs.activitylog.LogGenerateUploadFile;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.constant.ENUM_ERROR;
import com.hci.jfs.constant.ENUM_PROCESS_TYPE;
import com.hci.jfs.dao.impl.ProcessTypeDAOImpl;
import com.hci.jfs.entity.ProcessType;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.BatchProcessDescriptionService;
import com.hci.jfs.util.ApplicationProperties;

public class BatchProcessDescriptionServiceImpl implements BatchProcessDescriptionService {

    PaymentIntServiceImpl paymentIntService;
    ApplicationProperties applicationProperties;
    ContractRegistrationDisbursementServiceImpl contractRegistrationDisbursementService;
    EarlyTerminationServiceImpl earlyTerminationService;
    WriteOffServiceImpl writeOffService;
    ClawbackServiceImpl clawbackService;
    ProcessTypeDAOImpl processTypeDAO;

    @Override
    public void checkingLogGenerateUploadFile(Map<String, Object> param) throws Exception {
        Map<String, Object> param1 = new HashMap<String, Object>();
        param1.put("key", "partnerCode");
        param1.put("clause", CONSTANT_UTIL.CLAUSE_EQUAL);
        param1.put("value", param.get("partnerCode"));

        Map<String, Object> param2 = new HashMap<String, Object>();
        param2.put("key", "agreementCode");
        param2.put("clause", CONSTANT_UTIL.CLAUSE_EQUAL);
        param2.put("value", param.get("agreementCode"));

        Map<String, Object> param3 = new HashMap<String, Object>();
        param3.put("key", "processTypeCode");
        param3.put("clause", CONSTANT_UTIL.CLAUSE_EQUAL);
        param3.put("value", param.get("processCode"));

        Map<String, Object> param4 = new HashMap<String, Object>();
        param4.put("key", "dateUpload");
        param4.put("clause", CONSTANT_UTIL.CLAUSE_SQL_QUERY);
        param4.put("value", "TRUNC(DATE_UPLOAD) = TRUNC(TO_DATE('" + param.get("processDateCheck")
                + "','dd/mm/yyyy'))");

        List<Map<String, Object>> listParam = new ArrayList<Map<String, Object>>();
        listParam.add(param1);
        listParam.add(param2);
        listParam.add(param3);
        listParam.add(param4);

        LogGenerateUploadFile log = processTypeDAO.getByParamWithCustomClauseWhere(
                LogGenerateUploadFile.class, listParam);
        if (log != null) {
            throw new ApplicationException(ENUM_ERROR.ERR_FILE_IS_STILL_PROCESS.toString());
        }
    }

    @Override
    public void exportFileBasedOnProcessType(Map<String, Object> param) throws Exception {
        validationEmptyFile(param);

        ProcessType processType = processTypeDAO.getProcessTypeByCode((String) param
                .get("processCode"));
        switch (processType.getSequence().intValue()) {
            case 0:
                contractRegistrationDisbursementService.exportJFSStandardEasy10Products(param);
                break;
            case 1:
                contractRegistrationDisbursementService.exportJFSMPFProducts(param);
                break;
            case 17:
                contractRegistrationDisbursementService.exportJFSZeroProducts(param);
                break;
            case 2:
                paymentIntService.exportJFSPaymentRegular(param);
                break;
            case 3:
                paymentIntService.exportJFSPaymentReversal(param);
                break;
            case 4:
                paymentIntService.exportJFSPaymentGift(param);
                break;
            case 5:
                paymentIntService.exportJFSPaymentSmallUnderpayment(param);
                break;
            case 6:
                paymentIntService.exportJFSPaymentLastPayment(param);
                break;
            case 7:
                earlyTerminationService.exportJFSEarlyTerminationWithin15Days(param);
                break;
            case 8:
                earlyTerminationService.exportJFSEarlyTerminationAfter15Days(param);
                break;
            case 9:
                writeOffService.exportJFSWriteOffConctractRegistration(param);
                break;
            case 10:
                writeOffService.exportJFSWriteOffRecoveryPayment(param);
                break;
            case 11:
                clawbackService.exportJFSClawbackRegistration(param);
                break;
            case 12:
                clawbackService.exportJFSClawbackMPF(param);
                break;
            default:
                throw new ApplicationException("ERR-GENERAL-001");
        }
    }

    @Override
    public LogGenerateUploadFile saveLogGenerateUploadFile(Map<String, Object> param)
            throws Exception {
        LogGenerateUploadFile log = new LogGenerateUploadFile();
        log.setId(UUID.randomUUID().toString());
        log.setPartnerCode((String) param.get("partnerCode"));
        log.setProcessTypeCode((String) param.get("processCode"));
        log.setAgreementCode((String) param.get("agreementCode"));
        log.setDateUpload((Date) param.get("processDate"));
        log.setFilename(param.get("filename") == null ? "Generate" : (String) param.get("filename"));
        if (param.get("fileType") != null) {
            log.setFiletype((String) param.get("fileType"));
        }
        processTypeDAO.save(log);
        return log;
    }

    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setClawbackService(ClawbackServiceImpl clawbackService) {
        this.clawbackService = clawbackService;
    }

    public void setContractRegistrationDisbursementService(
            ContractRegistrationDisbursementServiceImpl contractRegistrationDisbursementService) {
        this.contractRegistrationDisbursementService = contractRegistrationDisbursementService;
    }

    public void setEarlyTerminationService(EarlyTerminationServiceImpl earlyTerminationService) {
        this.earlyTerminationService = earlyTerminationService;
    }

    public void setPaymentIntService(PaymentIntServiceImpl paymentIntService) {
        this.paymentIntService = paymentIntService;
    }

    public void setProcessTypeDAO(ProcessTypeDAOImpl processTypeDAO) {
        this.processTypeDAO = processTypeDAO;
    }

    public void setWriteOffService(WriteOffServiceImpl writeOffService) {
        this.writeOffService = writeOffService;
    }

    @Override
    public void uploadConvertFileAllocationBasedOnProcessType(Map<String, Object> param)
            throws Exception {
        validationEmptyFile(param);
        Map<String, String> processTypeAllocationMap = applicationProperties
                .generateMapByKeyProperties("process.type.allocation.file.name.type",
                        CONSTANT_UTIL.JFS_PROPERTIES, ",", "|");
        String processCode = (String) param.get("processCode");
        if (processTypeAllocationMap.get(processCode) != null) {
            if (processCode.equals(ENUM_PROCESS_TYPE.CRD_STANDARD.toString())
                    || processCode.equals(ENUM_PROCESS_TYPE.CRD_MPF.toString())
                    || processCode.equals(ENUM_PROCESS_TYPE.CRD_ZERO.toString())) {
                contractRegistrationDisbursementService.uploadJSFDisbursement(param);
            } else {
                paymentIntService.uploadJFSPaymentAllocationFile(param);
            }
        }
    }

    @Override
    public void uploadConvertFileBasedOnProcessType(Map<String, Object> param) throws Exception {

        validationEmptyFile(param);

        ProcessType processType = processTypeDAO.getProcessTypeByCode((String) param
                .get("processCode"));
        switch (processType.getSequence().intValue()) {
            case 0:
                contractRegistrationDisbursementService.uploadJFContractRegistration(param);
                break;
            case 1:
                contractRegistrationDisbursementService.uploadJFContractRegistration(param);
                break;
            case 17:
                contractRegistrationDisbursementService.uploadJFContractRegistration(param);
                break;
            case 2:
                paymentIntService.uploadJFSPaymentFile(param);
                break;
            case 3:
                paymentIntService.uploadJFSPaymentFile(param);
                break;
            case 4:
                paymentIntService.uploadJFSPaymentFile(param);
                break;
            case 5:
                paymentIntService.uploadJFSPaymentFile(param);
                break;
            case 6:
                paymentIntService.uploadJFSPaymentFile(param);
                break;
            case 7:
                earlyTerminationService.uploadJFSEarlyTerminationWithinFile(param);
                break;
            case 8:
                earlyTerminationService.uploadJFSEarlyTerminationAfterFile(param);
                break;
            case 9:
                writeOffService.uploadJFSWriteOffFile(param);
                break;
            case 10:
                writeOffService.uploadJFSWriteOffFile(param);
                break;
            default:
                throw new ApplicationException("ERR-GENERAL-001");
        }

    }

    @Override
    public void uploadValidationFileAllocationBasedOnProcessType(Map<String, Object> param)
            throws Exception {
        validationEmptyFile(param);
        Map<String, String> processTypeAllocationMap = applicationProperties
                .generateMapByKeyProperties("process.type.allocation.file.name.type",
                        CONSTANT_UTIL.JFS_PROPERTIES, ",", "|");
        String processCode = (String) param.get("processCode");
        if (processTypeAllocationMap.get(param.get("processCode")) != null) {
            if (processCode.equals(ENUM_PROCESS_TYPE.CRD_STANDARD.toString())
                    || processCode.equals(ENUM_PROCESS_TYPE.CRD_MPF.toString())
                    || processCode.equals(ENUM_PROCESS_TYPE.CRD_ZERO.toString())) {
                contractRegistrationDisbursementService
                        .convertJFSContractRegistrationAndDisbursement(param);
            } else {
                paymentIntService.convertJFSPaymentAllocationFile(param);
            }
        }
    }

    @Override
    public void uploadValidationFileBasedOnProcessType(Map<String, Object> param) throws Exception {
        validationEmptyFile(param);

        ProcessType processType = processTypeDAO.getProcessTypeByCode((String) param
                .get("processCode"));
        switch (processType.getSequence().intValue()) {
            case 0:
                contractRegistrationDisbursementService
                        .convertJFSContractRegistrationAndDisbursement(param);
                break;
            case 1:
                contractRegistrationDisbursementService
                        .convertJFSContractRegistrationAndDisbursement(param);
                break;
            case 17:
                contractRegistrationDisbursementService
                        .convertJFSContractRegistrationAndDisbursement(param);
                break;
            case 2:
                paymentIntService.convertJFSPaymentFile(param);
                break;
            case 3:
                paymentIntService.convertJFSPaymentFile(param);
                break;
            case 4:
                paymentIntService.convertJFSPaymentFile(param);
                break;
            case 5:
                paymentIntService.convertJFSPaymentFile(param);
                break;
            case 6:
                paymentIntService.convertJFSPaymentFile(param);
                break;
            case 7:
                earlyTerminationService.convertJFSEarlyTerminationWithinFile(param);
                break;
            case 8:
                earlyTerminationService.convertJFSEarlyTerminationAfterFile(param);
                break;
            case 9:
                writeOffService.convertJFSWriteOffFile(param);
                break;
            case 10:
                writeOffService.convertJFSWriteOffFile(param);
                break;
            default:
                throw new ApplicationException("ERR-GENERAL-001");
        }
    }

    private void validationEmptyFile(Map<String, Object> param) throws Exception {
        if (param == null) {
            throw new ApplicationException("ERR-GENERAL-001");
        }
        if (param.get("processCode") == null) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-001");
        }
    }
}
