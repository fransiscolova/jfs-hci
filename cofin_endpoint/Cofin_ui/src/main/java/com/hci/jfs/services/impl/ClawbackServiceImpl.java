package com.hci.jfs.services.impl;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hci.jfs.constant.CONSTANT_REPORT;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.BatchDAOImpl;
import com.hci.jfs.entity.BatchJobLog;
import com.hci.jfs.entity.GenerateFile;
import com.hci.jfs.entity.report.PaymentDetailReport;
import com.hci.jfs.entity.report.PaymentReport;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ClawBackService;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.ApplicationProperties;
import com.hci.jfs.util.EmailEngine;
import com.hci.jfs.util.FileUtil;
import com.hci.jfs.util.SSHManager;
import com.jcraft.jsch.ChannelSftp;

public class ClawbackServiceImpl implements ClawBackService {
    final static Logger LOGGER = LogManager.getLogger(ClawbackServiceImpl.class.getName());

    private BatchDAOImpl BatchDAO;

    private ApplicationProperties applicationProperties;

    private EmailEngine emailEngine;

    private SSHManager sshManager;

    private FileUtil fileUtil;

    @Override
    public void exportJFSClawbackMPF(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");

            // Get original file
            BatchJobLog batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory("/WS/Data/JFS/Export/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSClawbackMPF");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/");

                // instantiate generate file
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.clawback.list", "jfs_payment.properties", "|", ",");
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat("CLAWBACK_MPF_").concat(
                        runDate + CONSTANT_UTIL.TYPE_PDF);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CLAWBACK_MPF);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CLAWBACK_MPF_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                String[] textLines = text.toString().split("\n");
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(";");
                    PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                    System.out.println("index " + i);
                    if (i == textLines.length - 1) {
                        PaymentDetailReport paymentDetailReportDet = paymentDetailReports
                                .get(paymentDetailReports.size() - 1);
                        paymentDetailReportDet.setSize(String.valueOf(paymentDetailReports.size()));
                        paymentDetailReportDet.setTotalAmount(data[2]);
                        paymentDetailReports.set(paymentDetailReports.size() - 1,
                                paymentDetailReportDet);
                    } else {
                        paymentDetailReport.setRef(data[0]);
                        paymentDetailReport.setTextContractNumber(data[1]);
                        paymentDetailReport.setTransactionDate(data[2]);
                        paymentDetailReport.setAmtPayment(data[3]);
                        paymentDetailReports.add(paymentDetailReport);
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // save generate detail log
                saveGenerateDetailLogExportJFSPayment(param);

                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSClawbackMPF");
                try {
                    String content = "Export JSF Payment - Clawback Testing";
                    StringWriter outputWriter = new StringWriter();
                    outputWriter.write(content);
                    emailEngine.sendEmail("rizki.rachman02@homecredit.co.id",
                            "wina.hardini@homecredit.co.id", "rizki.rachman02@homecredit.co.id",
                            "", "Export JSF Payment - Clawback", outputWriter, filename);
                } catch (Exception e) {
                    LOGGER.debug("Sending email failed exportJFSClawbackMPF");
                }
                LOGGER.debug("Start exportJFSClawbackMPF");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exportJFSClawbackRegistration(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");

            // Get original file
            BatchJobLog batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory("/WS/Data/JFS/Export/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }

                LOGGER.debug("Start exportJFSClawbackRegistration");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/");

                // instantiate generate file
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.clawback.list", "jfs_payment.properties", "|", ",");
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat("CLAWBACK_REGISTRATION_").concat(
                        runDate + CONSTANT_UTIL.TYPE_PDF);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CLAWBACK_REGISTRATION);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CLAWBACK_REGISTRATION_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                String[] textLines = text.toString().split("\n");
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(";");
                    PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                    if (data[0].equals("TOTAL")) {
                        if (paymentDetailReports.size() == 0) {
                            paymentDetailReport.setRef("-");
                            paymentDetailReport.setTextContractNumber("-");
                            paymentDetailReport.setTransactionDate("-");
                            paymentDetailReport.setAmtPayment("-");
                            paymentDetailReports.add(paymentDetailReport);
                        } else {
                            PaymentDetailReport paymentDetailReportDet = paymentDetailReports
                                    .get(paymentDetailReports.size() - 1);
                            paymentDetailReportDet.setSize(String.valueOf(paymentDetailReports
                                    .size()));
                            paymentDetailReportDet.setTotalAmount(data[2]);
                            paymentDetailReports.set(paymentDetailReports.size() - 1,
                                    paymentDetailReportDet);
                        }
                    } else {
                        paymentDetailReport.setRef(data[0]);
                        paymentDetailReport.setTextContractNumber(data[1]);
                        paymentDetailReport.setTransactionDate(data[2]);
                        paymentDetailReport.setAmtPayment(data[3]);
                        paymentDetailReports.add(paymentDetailReport);
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // save generate detail log
                saveGenerateDetailLogExportJFSPayment(param);

                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSClawbackRegistration");
                try {
                    String content = "Export JSF Payment - Clawback Testing";
                    StringWriter outputWriter = new StringWriter();
                    outputWriter.write(content);

                    emailEngine.sendEmail("rizki.rachman02@homecredit.co.id",
                            "wina.hardini@homecredit.co.id", "rizki.rachman02@homecredit.co.id",
                            "", "Export JSF Payment - Clawback", outputWriter, filename);
                } catch (Exception e) {
                    LOGGER.debug("Send email failed exportJFSClawbackRegistration");
                }
                LOGGER.debug("End exportJFSClawbackRegistration");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        String filename = (String) param.get("filename");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        BatchJobLog log = (BatchJobLog) param.get("log");
        String letterNumber = (String) param.get("letterNumber");

        String name = filename.split(Pattern.quote("\\"))[filename.split(Pattern.quote("\\")).length - 1];
        GenerateFile generateFile = new GenerateFile();
        generateFile.setAgreement(log.getAgreement());
        generateFile.setPartner(log.getPartner());
        generateFile.setProcessType(log.getProcess());
        generateFile.setFileName(filename);
        generateFile.setCreatedDate(new Date());
        generateFile.setProceedDate((Date) param.get("proceedDate"));
        generateFile.setLetterNumber(letterNumber);
        generateFile.setIsCreated(CONSTANT_UTIL.DEFAULT_YES);
        generateFile.setRunIdBatchJobLog(log.getRunId());
        generateFile.setName(name);
        generateFile.setCreatedBy(userCredential.getUsername());
        this.BatchDAO.save(generateFile);
    }

    @Override
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {

    }

    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setBatchDAO(BatchDAOImpl batchDAO) {
        BatchDAO = batchDAO;
    }

    public void setEmailEngine(EmailEngine emailEngine) {
        this.emailEngine = emailEngine;
    }

    public void setFileUtil(FileUtil fileUtil) {
        this.fileUtil = fileUtil;
    }

    public void setSshManager(SSHManager sshManager) {
        this.sshManager = sshManager;
    }

}
