package com.hci.jfs.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.hci.jfs.dao.CommodityDAO;
import com.hci.jfs.entity.CommodityCategory;
import com.hci.jfs.entity.CommodityGroup;
import com.hci.jfs.entity.CommodityPurpose;
import com.hci.jfs.entity.CommodityType;
import com.hci.jfs.entity.OtherCriteriaSetting;
import com.hci.jfs.entity.form.CommodityMappingForm;
import com.hci.jfs.services.CommodityService;

public class CommodityServiceImpl implements CommodityService{
	
	CommodityDAO commodityDAO;
	public void setCommodityDAO(CommodityDAO commodityDAO) {
		this.commodityDAO = commodityDAO;
	}

	@Override
	public List<CommodityCategory> getAllCommodityCategory() throws Exception {
		return commodityDAO.getAllCommodityCategory();
	}

	@Override
	public List<CommodityType> getAllCommodityType() throws Exception {
		return commodityDAO.getAllCommodityType();
	}

	@Override
	public List<CommodityGroup> getAllCommodityGroup() throws Exception {
		return commodityDAO.getAllCommodityGroup();
	}

	@Override
	public List<CommodityPurpose> getAllCommodityPurpose() throws Exception {
		return commodityDAO.getAllCommodityPurpose();
	}

	@Override
	public List<CommodityCategory> getCommodityCategoryByHciCodeCommodityCategory(
			String codeCommodityCategory) throws Exception {
		return commodityDAO.getCommodityCategoryByHciCodeCommodityCategory(codeCommodityCategory);
	}

	@Override
	public List<CommodityGroup> getCommodityGroupByAgreementCode(
			Long agreementCode) throws Exception {
		return commodityDAO.getCommodityGroupByAgreementCode(agreementCode);
	}

	@Override
	public List<CommodityType> getCommodityTypeByHciCodeCommodityCategory(
			String codeCommodityCategory) throws Exception {
		return commodityDAO.getCommodityTypeByHciCodeCommodityCategory(codeCommodityCategory);
	}

	@Override
	public List<CommodityMappingForm> getCommodityGroupTypeByAgreementCode(
			Long agreementCode) throws Exception {
		List<CommodityMappingForm> mappingForms=null;
		List<CommodityGroup> listCommodityGroup = getCommodityGroupByAgreementCode(agreementCode);
		
		if(listCommodityGroup!=null){
			if(listCommodityGroup.size()>0){
				mappingForms = new ArrayList<CommodityMappingForm>();
				for (CommodityGroup commodityGroup : listCommodityGroup) {
					CommodityMappingForm mappingForm = new CommodityMappingForm();
					mappingForm.setIdCommodity(commodityGroup.getId());
					mappingForm.setBptnCommodityGroup(commodityGroup.getBptnCommodityGroup());
					mappingForm.setDescription(commodityGroup.getDescription());
					mappingForm.setIdAgreement(commodityGroup.getIdAgreement());
					mappingForm.setHciCodeCommodityCategory(commodityGroup.getHciCodeCommodityCategory());
					mappingForm.setListCommodityCategory(getCommodityCategoryByHciCodeCommodityCategory(commodityGroup.getHciCodeCommodityCategory()));
					mappingForm.setListCommodityType(getCommodityTypeByHciCodeCommodityCategory(commodityGroup.getHciCodeCommodityCategory()));
					mappingForm.setValidFrom(commodityGroup.getValidFrom());
					mappingForm.setValidTo(commodityGroup.getValidTo());	
					mappingForm.setIsDelete(commodityGroup.getIsDelete());
					mappingForms.add(mappingForm);
				}
			}
		}
		return mappingForms;
	}

	@Override
	public List<CommodityType> getCommodityTypeByAgreementCode(
			Long agreementCode) throws Exception {
		List<CommodityMappingForm> mappingForms=getCommodityGroupTypeByAgreementCode(agreementCode);
		List<CommodityType> commodityTypes=null;
		if(mappingForms!=null){
			if(mappingForms.size()>0){
				commodityTypes = new ArrayList<CommodityType>();
				for (CommodityMappingForm commodityMappingForm : mappingForms) {
					commodityTypes.addAll(commodityMappingForm.getListCommodityType());
				}
			}
		}		
		return commodityTypes;
	}

	@Override
	public List<OtherCriteriaSetting> getAllCriteriaSetting() throws Exception {
		// TODO Auto-generated method stub
		return commodityDAO.getAllCriteriaSetting();
	}
}
