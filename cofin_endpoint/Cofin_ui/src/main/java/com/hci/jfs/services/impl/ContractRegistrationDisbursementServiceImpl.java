package com.hci.jfs.services.impl;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.util.media.Media;

import com.hci.jfs.constant.CONSTANT_REPORT;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.constant.ENUM_ERROR;
import com.hci.jfs.constant.ENUM_FILENAME;
import com.hci.jfs.constant.ENUM_PROCESS_TYPE;
import com.hci.jfs.dao.impl.BatchDAOImpl;
import com.hci.jfs.dao.impl.ContractDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.BatchJobLog;
import com.hci.jfs.entity.Contract;
import com.hci.jfs.entity.ContractStatus;
import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.GenerateFile;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.entity.ProcessType;
import com.hci.jfs.entity.Schedule;
import com.hci.jfs.entity.SystemParameter;
import com.hci.jfs.entity.UploadFile;
import com.hci.jfs.entity.report.PaymentDetailReport;
import com.hci.jfs.entity.report.PaymentReport;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ApplicationExceptionService;
import com.hci.jfs.services.ContractRegistrationDisbursementService;
import com.hci.jfs.services.SystemParameterService;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.ApplicationProperties;
import com.hci.jfs.util.DateUtil;
import com.hci.jfs.util.EmailEngine;
import com.hci.jfs.util.FileUtil;
import com.hci.jfs.util.SSHManager;
import com.jcraft.jsch.ChannelSftp;

public class ContractRegistrationDisbursementServiceImpl implements
ContractRegistrationDisbursementService {
    final static Logger LOGGER = LogManager
            .getLogger(ContractRegistrationDisbursementServiceImpl.class.getName());

    private ContractDAOImpl contractDAO;
    private BatchDAOImpl batchDAO;
    private DateUtil dateUtil;
    private ApplicationProperties applicationProperties;
    private EmailEngine emailEngine;
    private SSHManager sshManager;
    private FileUtil fileUtil;
    private ApplicationExceptionService applicationExceptionService;
    private SystemParameterService systemParameterService;
    private List<Map<String, Object>> tmp_jfs_schedule;
    private Map<String, Object> mapContractNotFound;

    private List<String> confirmationJFSContractRegistrationStep1(Map<String, Object> param)
            throws Exception {
        LOGGER.debug("Step 1 Start " + param.get("processCode"));
        // Get Type Agreement
        String processCode = (String) param.get("processCode");
        SystemParameter sysParam = systemParameterService
                .getSystemParameterById(getAgreementTypeByProcess(processCode));
        if (sysParam == null) {
            throw new ApplicationException(
                    ENUM_ERROR.ERR_CONFIGURE_AGREEMENT_TYPE_PROCESS.toString());
        }
        String type = sysParam.getValue();
        // Done
        List<Contract> listUpdate = new ArrayList<Contract>();
        List<String> listWarning = new ArrayList<String>();

        // Set Variable
        String[] data = getStringDataFile((Media) param.get("media"));
        int headerSize = (Integer.parseInt(applicationProperties.getProperties(
                "write.off.header.size", CONSTANT_UTIL.JFS_PROPERTIES))) + 3;

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        Map<String, Integer> duplicateContract = new HashMap<String, Integer>();
        for (int i = headerSize; i < data.length; i++) {
            if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                        + param.get("processCode"));
            }
            String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
            try {
                if (item != null) {
                    String noPinjaman = item[0];
                    String status = item[5];
                    String keterangan = item[6];

                    if (duplicateContract.get(noPinjaman.trim()) != null) {
                        int j = 0;
                        for (Contract contract : listUpdate) {
                            String textContract = contract.getTextContractNumber();
                            if (textContract.trim().equals(noPinjaman.trim())) {
                                String ket = keterangan.concat(CONSTANT_UTIL.DEFAULT_COMMA)
                                        .concat(CONSTANT_UTIL.DEFAULT_DASH)
                                        .concat(contract.getReason());
                                contract.setReason(ket);
                                listUpdate.set(j, contract);
                                break;
                            }
                            j++;
                        }
                        continue;
                    } else {
                        duplicateContract.put(noPinjaman.trim(), i);
                    }

                    Contract contract = contractDAO.getContractByAgreementType(noPinjaman, type);
                    if (contract == null) {
                        mapContractNotFound.put(noPinjaman, "Contract : " + noPinjaman
                                + " Warning : contract not found");
                        continue;
                    }
                    if (!status.equals("S") && !status.equals("R")) {
                        listWarning.add("Contract : " + noPinjaman + " Error: unknown status : "
                                + status);
                        continue;
                    }

                    if (!status.equals("S") && !status.equals("R") && !status.equals("A")) {
                        listWarning.add("Contract : " + noPinjaman
                                + " error: can not replace status to : (S,A,R)");
                        continue;
                    }

                    if ((status.equals("S") || status.equals("R"))
                            && (!contract.getStatus().equals("A") || !contract.getStatus().equals(
                                    "R")) || contract.getReason() != null) {
                        contract.setStatus(status.trim().equals("S") ? "A" : "R");
                        contract.setReason(keterangan);
                        listUpdate.add(contract);
                    }
                }
            } catch (Exception e) {
                LOGGER.debug("Error Fetch Data " + param.get("processCode") + " : " + item);
                throw e;
            }
        }

        if (listUpdate.size() > 0) {
            LOGGER.debug("Update Contract Data " + param.get("processCode"));
            batchDAO.updateByBatch(listUpdate);
            LOGGER.debug("End Update Data " + param.get("processCode"));
        }
        LOGGER.debug("Step 1 End " + param.get("processCode"));
        return listWarning;
    }

    private void confirmationJFSContractRegistrationStep2(Map<String, Object> param)
            throws Exception {
        LOGGER.debug("Step 2 Start " + param.get("processCode"));
        Media file = (Media) param.get("media");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        String[] data = getStringDataFile((Media) param.get("media"));

        int headerSize = Integer.parseInt(applicationProperties.getProperties(
                "write.off.header.size", CONSTANT_UTIL.JFS_PROPERTIES));
        List<ContractStatus> listAdd = new ArrayList<ContractStatus>();
        String dateBankProcess = data[6].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1];

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = headerSize + 3; i < data.length; i++) {
            if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                        + param.get("processCode"));
            }
            String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
            try {
                if (item != null) {
                    String noPinjaman = item[0];
                    String status = item[5];
                    if (status.equals("S") || status.equals("R")) {
                        if (mapContractNotFound.get(noPinjaman) == null) {
                            Contract contract = new Contract();
                            contract.setTextContractNumber(noPinjaman.trim());

                            ContractStatus contractStatus = new ContractStatus();
                            contractStatus.setTextContractNumber(contract);
                            status = status.trim().equals("S") ? "A" : "R";
                            contractStatus.setCodeStatus(status);
                            contractStatus.setCntDPD(0);
                            contractStatus.setDtimeCreated(dateUtil
                                    .getFormattedCurrentDatePattern10(dateBankProcess));
                            contractStatus.setDtimeBankProcess(dateUtil
                                    .getFormattedCurrentDatePattern10(dateBankProcess));
                            contractStatus.setCreatedBy(userCredential.getUsername());
                            contractStatus.setFilename(file.getName());
                            contractStatus.setDateUpload(new Date());
                            listAdd.add(contractStatus);
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.debug("Error Fetch Data " + param.get("processCode") + " : " + item);
                throw e;
            }
        }
        if (listAdd.size() > 0) {
            LOGGER.debug("Create Contract Status");
            batchDAO.saveByBatch(listAdd);
            LOGGER.debug("End Create Data");
        }
        LOGGER.debug("Step 2 End");
    }
    private void confirmationJFSDisbursementStep1(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 1 Start " + param.get("processCode"));

        // Get Type Agreement
        String processCode = (String) param.get("processCode");
        SystemParameter sysParam = systemParameterService
                .getSystemParameterById(getAgreementTypeByProcess(processCode));
        if (sysParam == null) {
            throw new ApplicationException(
                    ENUM_ERROR.ERR_CONFIGURE_AGREEMENT_TYPE_PROCESS.toString());
        }
        String type = sysParam.getValue();
        // Done

        String[] data = getStringDataFile((Media) param.get("media"));
        // Split Rate
        String porsiPencairan = data[5].split(":")[1].trim()
                .replaceAll(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                .replaceAll("%", CONSTANT_UTIL.DEFAULT_EMPTY);
        Double splitRate = new Double(porsiPencairan);
        // Tanggal
        String tanggal = data[2].split(":")[1].trim().replaceAll(CONSTANT_UTIL.DEFAULT_DASH,
                CONSTANT_UTIL.DEFAULT_EMPTY);
        // Bank Reference No
        String bankReferenceNo = data[1].split(":")[1].trim().replaceAll(
                CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY);
        int detailSize = Integer.parseInt(applicationProperties.getProperties(
                "disbursement.detail.size", CONSTANT_UTIL.JFS_PROPERTIES));

        List<Contract> listUpdate = new ArrayList<Contract>();
        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailSize; i < data.length; i++) {
            if (data[i] != null) {
                if (data[i].contains("PERORANGAN") && data[i].contains("BULAN")) {
                    if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                        LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                                + param.get("processCode"));
                    }
                    String noKontrakMF = data[i].substring(27, 38).trim();
                    String namaPelanggan = data[i].substring(55, 79).trim();
                    String amtPencairan = data[i].substring(80, 98).trim();
                    String bunga = data[i].substring(99, 114).trim();
                    String eff = data[i].substring(132, 141).trim();
                    String tenor = data[i].substring(155, 157).trim();
                    String provisi = data[i].substring(164, 181).trim()
                            .replace(CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_EMPTY)
                            .replace(CONSTANT_UTIL.DEFAULT_DOT, CONSTANT_UTIL.DEFAULT_EMPTY);
                    String administrasi = data[i].substring(189, 198).trim();

                    Contract contract = contractDAO.getContractByAgreementType(noKontrakMF, type);
                    if (contract != null) {
                        BigDecimal amtBankPrincipal = new BigDecimal(amtPencairan.replace(
                                CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_EMPTY));
                        BigDecimal amtBankInterest = new BigDecimal(bunga.replace(
                                CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_EMPTY));
                        BigDecimal bankRate = new BigDecimal(eff.replace(
                                CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_EMPTY));
                        BigDecimal bankProvision = new BigDecimal(provisi.replace(
                                CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_EMPTY));
                        BigDecimal bankAdminFee = new BigDecimal(administrasi.replace(
                                CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_EMPTY));
                        int bankTenor = Integer.parseInt(tenor.replace(CONSTANT_UTIL.DEFAULT_COMMA,
                                CONSTANT_UTIL.DEFAULT_EMPTY));

                        contract.setStatus("A");
                        contract.setBankInterestRate(bankRate);
                        contract.setBankPrincipal(amtBankPrincipal);
                        contract.setBankInterest(amtBankInterest);
                        contract.setBankProvision(bankProvision);
                        contract.setBankAdminFee(bankAdminFee);

                        contract.setBankSplitRate(new BigDecimal(splitRate * 0.01).setScale(2,
                                RoundingMode.HALF_UP));
                        contract.setBankTenor(new BigDecimal(Integer.parseInt(tenor)));

                        // ROUND((cf.amt_bank_principal*(cf.bank_rate/1200*POWER(1+cf.bank_rate/1200,cf.bank_tenor)))/
                        // (POWER(1+cf.bank_rate/1200,cf.bank_tenor)-1),0)
                        BigDecimal step1 = BigDecimal.ZERO;
                        BigDecimal step3 = BigDecimal.ZERO;
                        BigDecimal step2 = BigDecimal.ZERO;
                        BigDecimal bankInstallment = BigDecimal.ZERO;
                        if (bankRate.compareTo(BigDecimal.ZERO) != 0) {
                            step1 = step1.add(bankRate).divide(new BigDecimal("1200"),
                                    MathContext.DECIMAL64);
                            step1 = step1.add(BigDecimal.ONE);
                            step1 = step1.pow(bankTenor);
                            step3 = step3.add(step1);
                            step3 = step3.subtract(BigDecimal.ONE);
                            step2 = step2.add(bankRate)
                                    .divide(new BigDecimal("1200"), MathContext.DECIMAL64)
                                    .multiply(step1);
                            step2 = step2.multiply(amtBankPrincipal);
                            bankInstallment = step2.divide(step3, MathContext.DECIMAL64).setScale(
                                    0, RoundingMode.HALF_UP);
                        } else {
                            bankInstallment = amtBankPrincipal.divide(new BigDecimal(bankTenor),
                                    MathContext.DECIMAL64).setScale(0, RoundingMode.HALF_UP);
                        }

                        contract.setBankInstallment(bankInstallment);
                        contract.setAcceptedDate(dateUtil.getFormattedCurrentDatePattern15(tanggal));
                        contract.setClientName(namaPelanggan);
                        contract.setBankReferenceNo(bankReferenceNo);
                        contract.setBankDecisionDate(contract.getAcceptedDate());
                        listUpdate.add(contract);
                    } else if (contract == null) {
                        mapContractNotFound.put(noKontrakMF, data[i]);
                    }
                }
            }
        }
        if (listUpdate.size() > 0) {
            LOGGER.debug("Update Contract Data " + param.get("processCode"));
            batchDAO.updateByBatch(listUpdate);
            LOGGER.debug("End Update Data " + param.get("processCode"));
        }
        LOGGER.debug("Step 1 End " + param.get("processCode"));
    }

    private void confirmationJFSDisbursementStep2(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 2 Start " + param.get("processCode"));
        // Get Type Agreement
        String processCode = (String) param.get("processCode");
        SystemParameter sysParam = systemParameterService
                .getSystemParameterById(getAgreementTypeByProcess(processCode));
        if (sysParam == null) {
            throw new ApplicationException(
                    ENUM_ERROR.ERR_CONFIGURE_AGREEMENT_TYPE_PROCESS.toString());
        }
        String type = sysParam.getValue();
        // Done

        String[] data = getStringDataFile((Media) param.get("media"));
        int detailSize = Integer.parseInt(applicationProperties.getProperties(
                "disbursement.detail.size", CONSTANT_UTIL.JFS_PROPERTIES));

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailSize; i < data.length; i++) {
            if (data[i] != null) {
                if (data[i].contains("PERORANGAN") && data[i].contains("BULAN")) {
                    if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                        LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                                + param.get("processCode"));
                    }
                    String noKontrakMF = data[i].substring(27, 38).trim();
                    int tenor = Integer.parseInt(data[i].substring(155, 157).trim());

                    if (mapContractNotFound.get(noKontrakMF) == null) {
                        Contract contract = contractDAO.getContractByAgreementType(noKontrakMF,
                                type);
                        if (contract != null) {
                            if (1 <= contract.getBankTenor().intValue()) {
                                Map<String, Object> itemDetail = new HashMap<String, Object>();
                                itemDetail.put("contract", contract);

                                itemDetail.put("part_index", 1);
                                BigDecimal interest = BigDecimal.ZERO;
                                interest = interest.add(contract.getBankPrincipal()
                                        .multiply(contract.getBankInterestRate())
                                        .divide(new BigDecimal("1200"), MathContext.DECIMAL128)
                                        .setScale(0, RoundingMode.HALF_UP));

                                BigDecimal principal = BigDecimal.ZERO;
                                principal = principal.add(contract.getBankInstallment().subtract(
                                        interest));

                                itemDetail.put("principal", principal);
                                itemDetail.put("interest", interest);
                                itemDetail.put("fee", BigDecimal.ZERO);

                                BigDecimal outstandingPrincipal = BigDecimal.ZERO;
                                outstandingPrincipal = outstandingPrincipal.add(contract
                                        .getBankPrincipal().subtract(principal));

                                BigDecimal balance = BigDecimal.ZERO;
                                balance = balance.add(principal).add(interest);

                                itemDetail.put("outstandingPrincipal", outstandingPrincipal);
                                itemDetail.put("prevBalance", BigDecimal.ZERO);
                                itemDetail.put("balance", balance);
                                itemDetail.put("tenor", tenor);
                                tmp_jfs_schedule.add(itemDetail);
                            }
                        }
                    }
                }
            }
        }
        LOGGER.debug("Step 2 End " + param.get("processCode"));
    }

    private void confirmationJFSDisbursementStep3(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 3 Start " + param.get("processCode"));
        UserCredential userLogin = (UserCredential) param.get("userCredential");
        Media file = (Media) param.get("media");
        Date dateUpload = new Date();
        List<Schedule> listSchedules = null;
        for (Map<String, Object> map : tmp_jfs_schedule) {
            Contract contract = (Contract) map.get("contract");
            BigDecimal principal = BigDecimal.ZERO;
            BigDecimal outstandingPrincipal = BigDecimal.ZERO;
            BigDecimal interest = BigDecimal.ZERO;
            BigDecimal balance = (BigDecimal) map.get("balance");
            BigDecimal fee = (BigDecimal) map.get("fee");
            Schedule schedule = null;
            listSchedules = new ArrayList<Schedule>();
            for (int i = 1; i <= contract.getBankTenor().intValue(); i++) {
                if (contract.getDateFirstDue() == null) {
                    LOGGER.debug("This Contract " + contract.getTextContractNumber()
                            + " doesnt have date first due");
                    throw new ApplicationException(
                            ENUM_ERROR.ERR_DISBURSEMENT_CONTRACT_MANDATORY_DATE_FIRST__DUE.name(),
                            contract.getTextContractNumber());
                }
                schedule = new Schedule();
                schedule.setContract(contract);

                Calendar cal = Calendar.getInstance();
                cal.setTime(contract.getDateFirstDue());
                cal.add(Calendar.MONTH, i);

                if (i == 1) {
                    interest = interest.add(contract.getBankPrincipal()
                            .multiply(contract.getBankInterestRate())
                            .divide(new BigDecimal("1200"), MathContext.DECIMAL64)
                            .setScale(0, RoundingMode.HALF_UP));
                    principal = principal.add(contract.getBankInstallment().subtract(interest));
                    outstandingPrincipal = outstandingPrincipal.add(contract.getBankPrincipal()
                            .subtract(principal));
                } else {
                    interest = outstandingPrincipal.multiply(
                            contract.getBankInterestRate().divide(new BigDecimal("1200"),
                                    MathContext.DECIMAL64)).setScale(0, RoundingMode.HALF_UP);
                    principal = contract.getBankInstallment().subtract(
                            outstandingPrincipal.multiply(
                                    contract.getBankInterestRate().divide(new BigDecimal("1200"),
                                            MathContext.DECIMAL64)).setScale(0,
                                                    RoundingMode.HALF_UP));
                    outstandingPrincipal = outstandingPrincipal.subtract(principal);
                    if (i == contract.getBankTenor().intValue()) {
                        BigDecimal interestDet = new BigDecimal(interest.toBigInteger());
                        BigDecimal principalDet = new BigDecimal(principal.toBigInteger());
                        BigDecimal outstandingPrincipalDet = new BigDecimal(
                                outstandingPrincipal.toBigInteger());

                        interest = interestDet.subtract(outstandingPrincipalDet);
                        principal = principalDet.add(outstandingPrincipalDet);
                        outstandingPrincipal = outstandingPrincipalDet
                                .subtract(outstandingPrincipalDet);
                    }
                }
                BigDecimal balanceDet = balance.multiply(new BigDecimal(i)).add(fee);
                BigDecimal prevBalance = i == 1 ? BigDecimal.ZERO : balance.multiply(
                        new BigDecimal(i - 1)).add(fee);

                schedule.setBalance(balanceDet);
                schedule.setPrevBalance(prevBalance);
                schedule.setDueDate(cal.getTime());
                schedule.setInterest(interest);
                schedule.setPrincipal(principal);
                schedule.setOutstandingPrincipal(outstandingPrincipal);
                schedule.setPartIndex(i);
                schedule.setFee(fee);
                schedule.setDateUpload(dateUpload);
                schedule.setCreatedBy(userLogin.getUsername());
                schedule.setFilename(file.getName());
                listSchedules.add(schedule);
            }
            if (listSchedules.size() > 0) {
                LOGGER.debug("Create Schedule Data " + param.get("processCode"));
                batchDAO.saveByBatch(listSchedules);
                LOGGER.debug("End Create Data " + param.get("processCode"));
            }
            LOGGER.debug("Step 3 End " + param.get("processCode"));
        }
    }

    @Override
    public void convertJFSContractRegistrationAndDisbursement(Map<String, Object> param)
            throws Exception {
        String[] data = getStringDataFile((Media) param.get("media"));

        // Validation
        String detailColumn = CONSTANT_UTIL.DEFAULT_EMPTY;
        String fileType = (String) param.get("fileType");

        if (fileType.contains(CONSTANT_UTIL.FILE_TYPE_UPLOAD_CONFIRMATION)) {
            detailColumn = "contract.registration.detail";
            if (data[6]
                    .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                    .trim()
                    .contains(
                            applicationProperties
                            .getProperties("contract.registration.header.7",
                                    CONSTANT_UTIL.JFS_PROPERTIES)
                                    .replace(CONSTANT_UTIL.DEFAULT_DASH,
                                            CONSTANT_UTIL.DEFAULT_EMPTY).trim())) {
                String tanggalCetak = data[6].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1]
                        .trim();
                Date processDate = (Date) param.get("processDate");
                if (!dateUtil.isSameDay(processDate,
                        dateUtil.getFormattedCurrentDatePattern10(tanggalCetak))) {
                    throw new ApplicationException(
                            ENUM_ERROR.ERR_JFS_PROCESS_DATE_INCORRECT.toString());
                }
            } else {
                throw new ApplicationException(ENUM_ERROR.ERR_JFS_PAYMENT_WRONG_HEADER.toString());
            }

            // Detail Column
            if (!data[8]
                    .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                    .trim()
                    .contains(
                            applicationProperties
                            .getProperties(detailColumn, CONSTANT_UTIL.JFS_PROPERTIES)
                            .replace(CONSTANT_UTIL.DEFAULT_DASH,
                                    CONSTANT_UTIL.DEFAULT_EMPTY).trim())) {
                throw new ApplicationException("ERR-JFS-PAYMENT-WRONG-DETAIL");
            }
        } else if (fileType.equals(CONSTANT_UTIL.FILE_TYPE_UPLOAD_ALLOCATION)) {
            detailColumn = "disbursement.detail";

            if (data[6]
                    .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                    .trim()
                    .contains(
                            applicationProperties
                            .getProperties("disbursement.header.7",
                                    CONSTANT_UTIL.JFS_PROPERTIES)
                                    .replace(CONSTANT_UTIL.DEFAULT_DASH,
                                            CONSTANT_UTIL.DEFAULT_EMPTY).trim())) {
                String tanggalCetak = data[6].substring(17, 28).trim();
                Date processDate = (Date) param.get("processDate");
                if (!dateUtil.isSameDay(processDate,
                        dateUtil.getFormattedCurrentDatePattern15(tanggalCetak))) {
                    throw new ApplicationException(
                            ENUM_ERROR.ERR_JFS_PROCESS_DATE_INCORRECT.toString());
                }
            } else {
                throw new ApplicationException(ENUM_ERROR.ERR_JFS_PAYMENT_WRONG_HEADER.toString());
            }

            // Detail Column
            if (!data[9]
                    .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                    .trim()
                    .contains(
                            applicationProperties
                            .getProperties(detailColumn, CONSTANT_UTIL.JFS_PROPERTIES)
                            .replace(CONSTANT_UTIL.DEFAULT_DASH,
                                    CONSTANT_UTIL.DEFAULT_EMPTY).trim())) {
                throw new ApplicationException("ERR-JFS-PAYMENT-WRONG-DETAIL");
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void createFileRejected(Map<String, Object> param) throws Exception {
        if (param.get("listRejected") != null) {
            List<String> listRejected = (List<String>) param.get("listRejected");
            if (listRejected.size() > 0) {
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.upload.contract.registration.list",
                        CONSTANT_UTIL.JFS_PROPERTIES, CONSTANT_UTIL.DEFAULT_SEPARATOR,
                        CONSTANT_UTIL.DEFAULT_COMMA);
                String filenameCsv = "JFS_REJECTED_".concat((String) param.get("processTypeName"))
                        .concat("_").concat((String) param.get("createdDate")).toUpperCase()
                        .concat(CONSTANT_UTIL.TYPE_CSV);
                StringBuilder text = new StringBuilder();
                text.append("CONTRACT_NUMBER;CUSTOMER_NAME;AMOUNT_DISBURSEMENT;STATUS;DESCRIPRION;");
                text.append(System.lineSeparator());

                Map<String, Object> paramReject = new HashMap<String, Object>();
                for (String string : listRejected) {
                    text.append(string);
                }

                paramReject.put("isRejected", CONSTANT_UTIL.DEFAULT_YES);
                paramReject.put("filename", filenameCsv);
                paramReject.put("content", text.toString());
                paramReject.put("path", mapPath.get(param.get("processCode")));
                paramReject.put("processCode", param.get("processCode"));

                paramReject.put("checksum", fileUtil.getChecksumOnFile(text.toString()));
                paramReject.put("size", String.valueOf(text.toString().getBytes().length));
                paramReject.put("userCredential", param.get("userCredential"));
                paramReject.put("agreementCode", param.get("agreementCode"));
                paramReject.put("partnerCode", param.get("partnerCode"));
                paramReject.put("fileType", param.get("fileType"));
                saveUploadDetailLogExportJFSPayment(paramReject);
            }
        }
    }

    @Override
    public void exportJFSMPFProducts(Map<String, Object> param) throws Exception {
        try {
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Need verify
            // BatchJobLog batchJobLog =
            // BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(partnerCode, agreementCode,
            // processCode, runDate,type);

            // Get original file
            BatchJobLog batchJobLog = batchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    CONSTANT_UTIL.UNKNOWN, CONSTANT_UTIL.UNKNOWN, "CRD_OPT_OUT", runDate, type);
            String[] splittedBatchJobLogFilePath = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLogFilePath[splittedBatchJobLogFilePath.length - 1];

            sshManager.connect();
            String exportPath = applicationProperties.getProperties(
                    CONSTANT_UTIL.APPLICATION_PROPERTIES_JFS_PATH_GENERATE_1,
                    CONSTANT_UTIL.APPLICATION_PROPERTIES_JFS_KEY);
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory(exportPath);
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSMPFProducts");
                LOGGER.debug("Filename" + entry.getFilename());

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd(exportPath);

                // Instantiate generate file
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.contract.registration.list",
                        CONSTANT_UTIL.JFS_PROPERTIES, CONSTANT_UTIL.DEFAULT_SEPARATOR,
                        CONSTANT_UTIL.DEFAULT_COMMA);
                String processPath = mapPath.get(processCode);
                String suffixDateTxtAes = CONSTANT_UTIL.DEFAULT_UNDERSCORE.concat(runDate).concat(
                        CONSTANT_UTIL.TYPE_TXT_AES);
                String filename = processPath
                        .concat(CONSTANT_REPORT.CONTRACT_REGISTRATION_FILENAME).concat(runDate)
                        .concat(CONSTANT_UTIL.TYPE_PDF);
                String slik = ENUM_FILENAME.SLIK_FF.toString().concat(suffixDateTxtAes);
                String pengurus = ENUM_FILENAME.PENGURUS_FF.toString().concat(suffixDateTxtAes);
                String barang = ENUM_FILENAME.BARANG_FF.toString().concat(suffixDateTxtAes);
                String kontrak = ENUM_FILENAME.KONTRAK_FF.toString().concat(suffixDateTxtAes);
                String enduser = ENUM_FILENAME.ENDUSER_FF.toString().concat(suffixDateTxtAes);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write files
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));
                fileUtil.writeToFile(sftpChannel.get(slik), processPath.concat(slik));
                fileUtil.writeToFile(sftpChannel.get(pengurus), processPath.concat(pengurus));
                fileUtil.writeToFile(sftpChannel.get(barang), processPath.concat(barang));
                fileUtil.writeToFile(sftpChannel.get(kontrak), processPath.concat(kontrak));
                fileUtil.writeToFile(sftpChannel.get(enduser), processPath.concat(enduser));

                // Populate files
                List<String> files = new ArrayList<String>();
                String pathTxt = processPath.concat(entry.getFilename());
                files.add(pathTxt);
                files.add(filename);
                files.add(processPath.concat(slik));
                files.add(processPath.concat(pengurus));
                files.add(processPath.concat(barang));
                files.add(processPath.concat(kontrak));
                files.add(processPath.concat(enduser));

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_MPF_PRODUCT_SUB_REPORT);
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();
                PaymentDetailReport detailReport = new PaymentDetailReport();
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();

                // Populate report data
                BigDecimal amountPortionHCI = BigDecimal.ZERO;
                BigDecimal amountPortionBTPN = BigDecimal.ZERO;
                BigDecimal totalContractEligible = BigDecimal.ZERO;
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(Pattern.quote(CONSTANT_UTIL.SEMICOLON));
                    if (data[0].equals(CONSTANT_UTIL.ELIGIBLE)) {
                        BigDecimal amountPortionHCIdet = new BigDecimal(data[3]);
                        BigDecimal amountPortionBTPNdet = new BigDecimal(data[2]);
                        BigDecimal eligibleContract = new BigDecimal(data[4].trim());

                        amountPortionBTPN = amountPortionBTPN.add(amountPortionBTPNdet);
                        amountPortionHCI = amountPortionHCI.add(amountPortionHCIdet);
                        totalContractEligible = totalContractEligible.add(eligibleContract);
                    }
                }

                detailReport.setNumberOfEligibleAgreement(totalContractEligible.toString());
                detailReport.setFinancingPortionHCI(amountPortionHCI.toString());
                detailReport.setFinancingPortionBTPN(amountPortionBTPN.toString());
                paymentDetailReports.add(detailReport);
                LOGGER.debug("Total size " + paymentDetailReports.size());

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, processPath);

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", processPath);
                param.put("files", files);
                saveGenerateDetailLogExportJFSPayment(param);

                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSMPFProducts to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Generate Contract Registration & Disbursement MPF Product",
                            htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Send Email failed exportJFSMPFProducts");
                }
                LOGGER.debug("End exportJFSMPFProducts");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            try {
                Employee userLogin = (Employee) param.get("userLogin");
                StringWriter htmlMessage = new StringWriter();
                htmlMessage
                .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                        + e + "\nThank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Generate Contract Registration & Disbursement MPF Product", htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed exportJFSStandardEasy10Products");
            }
            throw e;
        }
    }
    @Override
    public void exportJFSStandardEasy10Products(Map<String, Object> param) throws Exception {
        try {
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Need verify
            // BatchJobLog batchJobLog =
            // BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(partnerCode, agreementCode,
            // processCode, runDate,type);

            // Get original file
            BatchJobLog batchJobLog = batchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    CONSTANT_UTIL.UNKNOWN, CONSTANT_UTIL.UNKNOWN, "CRD_OPT_OUT_STD", runDate, type);
            String[] splittedBatchJobLogFilePath = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLogFilePath[splittedBatchJobLogFilePath.length - 1];

            sshManager.connect();
            String exportPath = applicationProperties.getProperties(
                    CONSTANT_UTIL.APPLICATION_PROPERTIES_JFS_PATH_GENERATE_1,
                    CONSTANT_UTIL.APPLICATION_PROPERTIES_JFS_KEY);
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory(exportPath);
            for (ChannelSftp.LsEntry entry : list) {
                // process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSStandardEasy10Products");
                LOGGER.debug("Filename" + entry.getFilename());

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd(exportPath);

                // instantiate generate file
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.contract.registration.list",
                        CONSTANT_UTIL.JFS_PROPERTIES, CONSTANT_UTIL.DEFAULT_SEPARATOR,
                        CONSTANT_UTIL.DEFAULT_COMMA);
                String processPath = mapPath.get(processCode);
                String suffixDateTxtAes = CONSTANT_UTIL.DEFAULT_UNDERSCORE.concat(runDate).concat(
                        CONSTANT_UTIL.TYPE_TXT_AES);
                String filename = processPath
                        .concat(CONSTANT_REPORT.CONTRACT_REGISTRATION_FILENAME).concat(runDate)
                        .concat(CONSTANT_UTIL.TYPE_PDF);
                String slik = ENUM_FILENAME.SLIK_ALL.toString().concat(suffixDateTxtAes);
                String pengurus = ENUM_FILENAME.PENGURUS_ALL.toString().concat(suffixDateTxtAes);
                String barang = ENUM_FILENAME.BARANG_ALL.toString().concat(suffixDateTxtAes);
                String kontrak = ENUM_FILENAME.KONTRAK_ALL.toString().concat(suffixDateTxtAes);
                String enduser = ENUM_FILENAME.ENDUSER_ALL.toString().concat(suffixDateTxtAes);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write files
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));
                fileUtil.writeToFile(sftpChannel.get(slik), processPath.concat(slik));
                fileUtil.writeToFile(sftpChannel.get(pengurus), processPath.concat(pengurus));
                fileUtil.writeToFile(sftpChannel.get(barang), processPath.concat(barang));
                fileUtil.writeToFile(sftpChannel.get(kontrak), processPath.concat(kontrak));
                fileUtil.writeToFile(sftpChannel.get(enduser), processPath.concat(enduser));

                // Populate files
                List<String> files = new ArrayList<String>();
                String pathTxt = processPath.concat(entry.getFilename());
                files.add(pathTxt);
                files.add(filename);
                files.add(processPath.concat(slik));
                files.add(processPath.concat(pengurus));
                files.add(processPath.concat(barang));
                files.add(processPath.concat(kontrak));
                files.add(processPath.concat(enduser));

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_STANDARD_EASY_10_PRODUCT_SUB_REPORT);
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();
                PaymentDetailReport detailReport = new PaymentDetailReport();
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();

                // Populate report data
                BigDecimal amountPortionHCI = BigDecimal.ZERO;
                BigDecimal amountPortionBTPN = BigDecimal.ZERO;
                BigDecimal totalContractEligible = BigDecimal.ZERO;
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(Pattern.quote(CONSTANT_UTIL.SEMICOLON));
                    if (data[0].equals(CONSTANT_UTIL.ELIGIBLE)) {
                        BigDecimal amountPortionHCIdet = new BigDecimal(data[3]);
                        BigDecimal amountPortionBTPNdet = new BigDecimal(data[2]);
                        BigDecimal eligibleContract = new BigDecimal(data[4].trim());

                        amountPortionBTPN = amountPortionBTPN.add(amountPortionBTPNdet);
                        amountPortionHCI = amountPortionHCI.add(amountPortionHCIdet);
                        totalContractEligible = totalContractEligible.add(eligibleContract);
                    }
                }

                detailReport.setNumberOfEligibleAgreement(totalContractEligible.toString());
                detailReport.setFinancingPortionHCI(amountPortionHCI.toString());
                detailReport.setFinancingPortionBTPN(amountPortionBTPN.toString());
                LOGGER.debug("Total size " + paymentDetailReports.size());
                paymentDetailReports.add(detailReport);

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                param.put("files", files);
                saveGenerateDetailLogExportJFSPayment(param);

                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSStandardEasy10Products to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Generate Contract Registration & Disbursement Regular Product",
                            htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Send email failed exportJFSStandardEasy10Products");
                }
                LOGGER.debug("End exportJFSStandardEasy10Products");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            try {
                Employee userLogin = (Employee) param.get("userLogin");
                StringWriter htmlMessage = new StringWriter();
                htmlMessage
                .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                        + e + "\nThank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Generate Contract Registration & Disbursement Regular Product",
                        htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed exportJFSStandardEasy10Products");
            }
            throw e;
        }
    }

    @Override
    public void exportJFSZeroProducts(Map<String, Object> param) throws Exception {
        try {
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Need verify
            // BatchJobLog batchJobLog =
            // BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(partnerCode, agreementCode,
            // processCode, runDate,type);

            // Get original file
            BatchJobLog batchJobLog = batchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    CONSTANT_UTIL.UNKNOWN, CONSTANT_UTIL.UNKNOWN, "CRD_OPT_OUT_Z", runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            String exportPath = applicationProperties.getProperties(
                    CONSTANT_UTIL.APPLICATION_PROPERTIES_JFS_PATH_GENERATE_1,
                    CONSTANT_UTIL.APPLICATION_PROPERTIES_JFS_KEY);
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory(exportPath);
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSZeroProducts");
                LOGGER.debug("Filename" + entry.getFilename());

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd(exportPath);

                // Instantiate generate file
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.contract.registration.list",
                        CONSTANT_UTIL.JFS_PROPERTIES, CONSTANT_UTIL.DEFAULT_SEPARATOR,
                        CONSTANT_UTIL.DEFAULT_COMMA);
                String suffixDateTxtAes = CONSTANT_UTIL.DEFAULT_UNDERSCORE.concat(runDate).concat(
                        CONSTANT_UTIL.TYPE_TXT_AES);
                String processPath = mapPath.get(processCode);
                String filename = processPath
                        .concat(CONSTANT_REPORT.CONTRACT_REGISTRATION_ZERO_PROMO_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_PDF);
                String slik = ENUM_FILENAME.SLIK_Z.toString().concat(suffixDateTxtAes);
                String pengurus = ENUM_FILENAME.PENGURUS_Z.toString().concat(suffixDateTxtAes);
                String barang = ENUM_FILENAME.BARANG_Z.toString().concat(suffixDateTxtAes);
                String kontrak = ENUM_FILENAME.KONTRAK_Z.toString().concat(suffixDateTxtAes);
                String enduser = ENUM_FILENAME.ENDUSER_Z.toString().concat(suffixDateTxtAes);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write files
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));
                fileUtil.writeToFile(sftpChannel.get(slik), processPath.concat(slik));
                fileUtil.writeToFile(sftpChannel.get(pengurus), processPath.concat(pengurus));
                fileUtil.writeToFile(sftpChannel.get(barang), processPath.concat(barang));
                fileUtil.writeToFile(sftpChannel.get(kontrak), processPath.concat(kontrak));
                fileUtil.writeToFile(sftpChannel.get(enduser), processPath.concat(enduser));

                // Populate files
                List<String> files = new ArrayList<String>();
                String pathTxt = processPath.concat(entry.getFilename());
                files.add(pathTxt);
                files.add(filename);
                files.add(processPath.concat(slik));
                files.add(processPath.concat(pengurus));
                files.add(processPath.concat(barang));
                files.add(processPath.concat(kontrak));
                files.add(processPath.concat(enduser));

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.CONTRACT_REGISTRATION_DISBURSEMENT_ZERO_PRODUCT_SUB_REPORT);
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();
                PaymentDetailReport detailReport = new PaymentDetailReport();
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();

                // Populate report data
                BigDecimal amountPortionHCI = BigDecimal.ZERO;
                BigDecimal amountPortionBTPN = BigDecimal.ZERO;
                BigDecimal totalContractEligible = BigDecimal.ZERO;
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(Pattern.quote(CONSTANT_UTIL.SEMICOLON));
                    if (data[0].equals(CONSTANT_UTIL.ELIGIBLE)) {
                        BigDecimal amountPortionHCIdet = new BigDecimal(data[3]);
                        BigDecimal amountPortionBTPNdet = new BigDecimal(data[2]);
                        BigDecimal eligibleContract = new BigDecimal(data[4].trim());

                        amountPortionBTPN = amountPortionBTPN.add(amountPortionBTPNdet);
                        amountPortionHCI = amountPortionHCI.add(amountPortionHCIdet);
                        totalContractEligible = totalContractEligible.add(eligibleContract);
                    }
                }

                detailReport.setNumberOfEligibleAgreement(totalContractEligible.toString());
                detailReport.setFinancingPortionHCI(amountPortionHCI.toString());
                detailReport.setFinancingPortionBTPN(amountPortionBTPN.toString());
                paymentDetailReports.add(detailReport);

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                param.put("files", files);
                saveGenerateDetailLogExportJFSPayment(param);

                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSZeroProducts to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.<br>Thank You.");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Generate Contract Registration & Disbursement Zero Product",
                            htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Send Email failed exportJFSZeroProducts");
                }
                LOGGER.debug("End exportJFSZeroProducts");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            try {
                Employee userLogin = (Employee) param.get("userLogin");
                StringWriter htmlMessage = new StringWriter();
                htmlMessage
                .append("The process is unsucessfull please contact our support team.<br>For detail information error : <br>"
                        + e + "<br>Thank You.");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Generate Contract Registration & Disbursement Zero Product", htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed exportJFSStandardEasy10Products");
            }
            throw e;
        }

    }

    private List<String> exportRejectedContractRegistration(Map<String, Object> param)
            throws Exception {
        LOGGER.debug("Step export rejected start " + param.get("processCode"));
        List<String> listReject = new ArrayList<String>();
        String[] data = getStringDataFile((Media) param.get("media"));
        int headerSize = Integer.parseInt(applicationProperties.getProperties(
                "write.off.header.size", CONSTANT_UTIL.JFS_PROPERTIES));

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = headerSize + 3; i < data.length; i++) {
            if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                        + param.get("processCode"));
            }
            String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
            try {
                if (item != null) {
                    String status = item[5];
                    if (status.equals("R")) {
                        String noPinjaman = item[0];
                        String namaDebitur = item[1];
                        String pokok = item[2];
                        String keterangan = item[6];
                        if (mapContractNotFound.get(noPinjaman) == null) {
                            listReject
                            .add(noPinjaman.concat(CONSTANT_UTIL.SEMICOLON)
                                    .concat(namaDebitur).concat(CONSTANT_UTIL.SEMICOLON)
                                    .concat(pokok).concat(CONSTANT_UTIL.SEMICOLON)
                                    .concat(status).concat(CONSTANT_UTIL.SEMICOLON)
                                    .concat(keterangan).concat(CONSTANT_UTIL.SEMICOLON)
                                    .concat(System.lineSeparator()));
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.debug("Error Fetch Data " + param.get("processCode") + " : " + item);
                throw e;
            }
        }
        LOGGER.debug("Step export rejected End " + param.get("processCode"));
        return listReject;
    }

    private String getAgreementTypeByProcess(String processCode) {
        String agreement = CONSTANT_UTIL.DEFAULT_EMPTY;
        if (processCode.equals(ENUM_PROCESS_TYPE.CRD_STANDARD.toString())) {
            agreement = "JFS_AGREEMENT_REGULAR";
        } else if (processCode.equals(ENUM_PROCESS_TYPE.CRD_MPF.toString())) {
            agreement = "JFS_AGREEMENT_MPF";
        } else if (processCode.equals(ENUM_PROCESS_TYPE.CRD_ZERO.toString())) {
            agreement = "JFS_AGREEMENT_ZERO";
        }
        return agreement;
    }

    private String[] getStringDataFile(Media file) {
        return file.getFormat() != null ? file.getStringData().split(System.lineSeparator())
                : new String(file.getByteData()).split(System.lineSeparator());
    }

    private void init() {
        if (tmp_jfs_schedule != null) {
            tmp_jfs_schedule.clear();
        } else {
            tmp_jfs_schedule = new ArrayList<Map<String, Object>>();
        }

        if (mapContractNotFound != null) {
            mapContractNotFound.clear();
        } else {
            mapContractNotFound = new HashMap<String, Object>();
        }
    }

    @Override
    public void navigateToUpload(Map<String, Object> param) throws Exception {
        Media file = (Media) param.get("media");
        String[] data = getStringDataFile(file);
        if (data[0].contains(applicationProperties.getProperties("contract.registration.header.1",
                CONSTANT_UTIL.JFS_PROPERTIES))) {
            uploadJFContractRegistration(param);
        } else if (data[0].contains(applicationProperties.getProperties("disbursement.header.1",
                CONSTANT_UTIL.JFS_PROPERTIES))) {
            uploadJSFDisbursement(param);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        if (param.get("files") != null) {
            LOGGER.debug("Start saveGenerateDetailLogExportJFSPayment " + param.get("processCode"));
            List<String> files = (List<String>) param.get("files");
            for (String filename : files) {
                UserCredential userCredential = (UserCredential) param.get("userCredential");
                BatchJobLog log = (BatchJobLog) param.get("log");
                String letterNumber = (String) param.get("letterNumber");

                String name = filename.split(Pattern.quote("\\"))[filename.split(Pattern
                        .quote("\\")).length - 1];
                LOGGER.debug("File " + name);
                GenerateFile generateFile = new GenerateFile();
                generateFile.setAgreement(log.getAgreement());
                generateFile.setPartner(log.getPartner());
                generateFile.setProcessType(log.getProcess());
                generateFile.setFileName(filename);
                generateFile.setCreatedDate(new Date());
                generateFile.setProceedDate((Date) param.get("proceedDate"));
                generateFile.setLetterNumber(letterNumber);
                generateFile.setIsCreated(CONSTANT_UTIL.DEFAULT_YES);
                generateFile.setRunIdBatchJobLog(log.getRunId());
                generateFile.setName(name);
                generateFile.setCreatedBy(userCredential.getUsername());
                batchDAO.save(generateFile);
            }
            LOGGER.debug("End saveGenerateDetailLogExportJFSPayment " + param.get("processCode"));
        }
    }

    @Override
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        LOGGER.debug("Start Upload JSP Payment File to Local Temporary " + param.get("processCode"));
        Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                "file.path.upload.contract.registration.list", CONSTANT_UTIL.JFS_PROPERTIES,
                CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);

        UserCredential userCredential = (UserCredential) param.get("userCredential");

        String content = null;
        UploadFile uploadFile = new UploadFile();

        ProcessType processType = new ProcessType();
        processType.setCode((String) param.get("processCode"));
        uploadFile.setProcessType(processType);
        uploadFile.setCreatedDate(new Date());
        uploadFile.setSizeFile((String) param.get("size"));
        uploadFile.setChecksum((String) param.get("checksum"));

        if (param.get("isRejected") != null) {
            LOGGER.debug("Is Rejected");
            if (param.get("isRejected").equals(CONSTANT_UTIL.DEFAULT_YES)) {
                uploadFile.setFilepath(((String) param.get("path")).concat((String) param
                        .get("filename")));
                uploadFile.setName((String) param.get("filename"));
                content = (String) param.get("content");
            }
        } else {
            Media file = (Media) param.get("media");
            uploadFile.setName(file.getName());
            uploadFile.setFilepath(mapPath.get(processType.getCode()).concat(file.getName()));

            if (file.getFormat() != null) {
                content = file.getStringData();
            } else {
                content = new String(file.getByteData());
            }
        }

        Agreement agreement = new Agreement();
        agreement.setCode((String) param.get("agreementCode"));
        uploadFile.setAgreement(agreement);

        Partner partner = new Partner();
        partner.setId((String) param.get("partnerCode"));
        uploadFile.setPartner(partner);
        uploadFile.setCreatedBy(userCredential.getUsername());
        uploadFile.setFileType((String) param.get("fileType"));
        batchDAO.save(uploadFile);

        fileUtil.writeToFile(content, uploadFile.getFilepath());
        LOGGER.debug("End Upload JSP Payment File to Local Temporary " + param.get("processCode"));
    }

    public void setApplicationExceptionService(
            ApplicationExceptionService applicationExceptionService) {
        this.applicationExceptionService = applicationExceptionService;
    }

    public void setapplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setBatchDAO(BatchDAOImpl batchDAO) {
        this.batchDAO = batchDAO;
    }

    public void setContractDAO(ContractDAOImpl contractDAO) {
        this.contractDAO = contractDAO;
    }

    public void setDateUtil(DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    public void setEmailEngine(EmailEngine emailEngine) {
        this.emailEngine = emailEngine;
    }

    public void setFileUtil(FileUtil fileUtil) {
        this.fileUtil = fileUtil;
    }

    public void setSshManager(SSHManager sshManager) {
        this.sshManager = sshManager;
    }

    public void setSystemParameterService(SystemParameterService systemParameterService) {
        this.systemParameterService = systemParameterService;
    }

    @Override
    public void uploadJFContractRegistration(Map<String, Object> param) throws Exception {
        try {
            Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                    "file.path.upload.contract.registration.list", CONSTANT_UTIL.JFS_PROPERTIES,
                    CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
            String filenameCsv = "JFS_REJECTED_".concat((String) param.get("processTypeName"))
                    .concat("_").concat((String) param.get("createdDate")).toUpperCase()
                    .concat(CONSTANT_UTIL.TYPE_CSV);
            String pathfilenameCsv = mapPath.get(param.get("processCode")).concat(filenameCsv);

            LOGGER.debug("Upload JFS Contract Registration " + param.get("processCode"));
            init();
            // Process Import data and merge with jfs contract
            List<String> listWarning = confirmationJFSContractRegistrationStep1(param);
            // Process insert into jfs contract status
            confirmationJFSContractRegistrationStep2(param);
            List<String> listRejected = exportRejectedContractRegistration(param);
            if (listRejected.size() > 0) {
                // Export rejected contract
                param.put("listRejected", listRejected);
                this.createFileRejected(param);
            }
            saveUploadDetailLogExportJFSPayment(param);

            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage.append("The process is already done.<br>");
            LOGGER.debug("Email Upload JFS Contract Registration " + param.get("processCode")
                    + " to " + userLogin.getTextEmailAddress());
            try {
                if (mapContractNotFound.size() > 0 || listRejected.size() > 1) {
                    htmlMessage.append("Detail Information<br>");
                }
                if (mapContractNotFound.size() > 0) {
                    int i = 1;
                    htmlMessage.append("Contract not found in our database<br>");
                    for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + entry.getKey() + "<br>");
                        i++;
                    }
                    htmlMessage.append("<br>");
                }
                if (listWarning.size() > 0) {
                    int i = 1;
                    htmlMessage.append("Warning Contract");
                    for (String string : listRejected) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + string + "<br>");
                        i++;
                    }
                    htmlMessage.append("<br>");
                }

                if (mapContractNotFound.size() > 0 || listRejected.size() > 1) {
                    htmlMessage.append("Please contact our support team or jfs team<br>");
                }
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Upload Contract And Registration Confirmation "
                        .concat((String) param.get("processTypeName")), htmlMessage,
                        pathfilenameCsv);
            } catch (Exception e) {
                LOGGER.debug("Send email failed uploadJFContractRegistration "
                        + param.get("processCode"));
            }
            LOGGER.debug("End Upload JFS Contract Registration " + param.get("processCode"));
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            try {
                Employee userLogin = (Employee) param.get("userLogin");
                StringWriter htmlMessage = new StringWriter();
                htmlMessage.append("The process is unsucessfull<br>");
                if (e instanceof ApplicationException) {
                    ApplicationException ex = (ApplicationException) e;
                    String error = applicationExceptionService.getApplicationExceptionByErrorCode(
                            ex.getErrorCode()).getErrorMessage();
                    htmlMessage.append("For detail information error : <i>" + error + "</i><br>");
                    if (mapContractNotFound.size() > 0) {
                        for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                            htmlMessage.append(entry.getValue() + "<br>");
                        }
                    }
                    htmlMessage.append("Please contact our support team or jfs team<br>");
                } else {
                    htmlMessage.append("For detail information error : \n" + e + "<br>");
                }
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Upload Contract And Registration Confirmation "
                        .concat((String) param.get("processTypeName")), htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed");
            }
            throw e;
        }
    }

    @Override
    public void uploadJSFDisbursement(Map<String, Object> param) throws Exception {
        try {
            LOGGER.debug("Start Upload JFS Disbursement " + param.get("processCode"));
            init();
            // Update JFS Contract
            confirmationJFSDisbursementStep1(param);
            // repayment schedule generation
            // create 1st installment
            confirmationJFSDisbursementStep2(param);
            // create all installment
            confirmationJFSDisbursementStep3(param);

            // Save history file
            saveUploadDetailLogExportJFSPayment(param);

            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage.append("The process is already done.<br>");
            LOGGER.debug("Email Upload JFS Disbursement " + param.get("processCode") + " to "
                    + userLogin.getTextEmailAddress());
            try {
                if (mapContractNotFound.size() > 0) {
                    htmlMessage.append("Detail Information<br>");
                }
                if (mapContractNotFound.size() > 0) {
                    int i = 1;
                    htmlMessage.append("Contract not found in our database<br>");
                    for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + entry.getKey() + "<br>");
                        i++;
                    }
                    htmlMessage.append("<br>");
                }
                if (mapContractNotFound.size() > 0) {
                    htmlMessage.append("Please contact our support team or jfs team<br>");
                }
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("ist@homecredit.co.id", userLogin.getTextEmailAddress(),
                        CONSTANT_UTIL.DEFAULT_EMPTY, CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Upload Contract And Registration Allocation "
                        .concat((String) param.get("processTypeName")), htmlMessage);
            } catch (Exception e) {
                LOGGER.debug("Send email failed uploadJSFDisbursement " + param.get("processCode"));
            }
            LOGGER.debug("End Upload JFS Disbursement " + param.get("processCode"));
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage.append("The process is unsucessfull<br>");
            if (e instanceof ApplicationException) {
                ApplicationException ex = (ApplicationException) e;
                String error = applicationExceptionService.getApplicationExceptionByErrorCode(
                        ex.getErrorCode()).getErrorMessage();
                if (((ApplicationException) e).getErrorMessage() != null) {
                    error = error.concat(" " + ((ApplicationException) e).getErrorMessage());
                }
                htmlMessage.append("For detail information error : <i>" + error + "</i><br>");
                if (mapContractNotFound.size() > 0) {
                    int i = 1;
                    htmlMessage.append("Contract not found in our database<br>");
                    for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + entry.getKey() + "<br>");
                        i++;
                    }
                    htmlMessage.append("<br>");
                }
            } else {
                htmlMessage.append("For detail information error : \n" + e + "<br>");
            }
            htmlMessage.append("Please contact our support team or jfs team<br>");
            htmlMessage.append("Thank You");
            try {
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Upload Contract And Registration Allocation "
                        .concat((String) param.get("processTypeName")), htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed exportJFSStandardEasy10Products");
            }
            throw e;
        }
    }
}
