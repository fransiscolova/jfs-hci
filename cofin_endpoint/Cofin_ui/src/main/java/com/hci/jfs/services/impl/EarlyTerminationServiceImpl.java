package com.hci.jfs.services.impl;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.util.media.Media;

import com.hci.jfs.constant.CONSTANT_REPORT;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.constant.ENUM_ERROR;
import com.hci.jfs.constant.ENUM_PAYMENT_TYPE;
import com.hci.jfs.constant.ENUM_PROCESS_TYPE;
import com.hci.jfs.dao.impl.BatchDAOImpl;
import com.hci.jfs.dao.impl.ContractDAOImpl;
import com.hci.jfs.dao.impl.PaymentDAOImpl;
import com.hci.jfs.dao.impl.PaymentIntDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.BatchJobLog;
import com.hci.jfs.entity.Contract;
import com.hci.jfs.entity.ContractStatus;
import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.GenerateFile;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.entity.PaymentInt;
import com.hci.jfs.entity.ProcessType;
import com.hci.jfs.entity.SystemParameter;
import com.hci.jfs.entity.UploadFile;
import com.hci.jfs.entity.report.PaymentDetailReport;
import com.hci.jfs.entity.report.PaymentReport;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ApplicationExceptionService;
import com.hci.jfs.services.EarlyTerminationService;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.ApplicationProperties;
import com.hci.jfs.util.DateUtil;
import com.hci.jfs.util.EmailEngine;
import com.hci.jfs.util.FileUtil;
import com.hci.jfs.util.SSHManager;
import com.jcraft.jsch.ChannelSftp;

@Transactional
public class EarlyTerminationServiceImpl implements EarlyTerminationService {
    final static Logger LOGGER = LogManager.getLogger(EarlyTerminationServiceImpl.class.getName());

    private ContractDAOImpl contractDAO;
    private BatchDAOImpl batchDAO;
    private SystemParameterServiceImpl systemParameterService;
    private PaymentDAOImpl PaymentDAO;
    private PaymentIntDAOImpl PaymentIntDAO;
    private FileUtil fileUtil;

    @Autowired
    private DateUtil dateUtil;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private EmailEngine emailEngine;

    @Autowired
    private SSHManager sshManager;

    private ApplicationExceptionService applicationExceptionService;

    private Map<String, Object> mapContractNotFound;

    private Map<String, Object> mapContractStatusNotFound;

    private List<String> checkingRejectedJFSEarlyTerminationAfterFile(Map<String, Object> param)
            throws Exception {
        List<String> listReject = new ArrayList<String>();
        int detailLength = (int) param.get("detailLength");
        String[] data = (String[]) param.get("data");

        for (int i = detailLength; i < data.length; i++) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[i])) {
                String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String status = item[9];

                if (!status.equals("S")) {
                    String contractNumber = item[1];
                    String transactionDate = item[4];
                    Map<String, Object> paramContract = new HashMap<String, Object>();
                    paramContract.put("contractNumber", contractNumber);
                    paramContract.put("transactionDate", transactionDate);

                    List<PaymentInt> listPaymentInt = PaymentIntDAO
                            .getPaymentIntByContractNumberAndPaymentDate(paramContract);
                    if (listPaymentInt != null) {
                        if (listPaymentInt.size() > 0) {
                            listReject.add(data[i]);
                        }
                    }
                }
            }
        }
        return listReject;
    }

    private List<PaymentInt> confirmationJFSETAfterFile(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 1 Start " + param.get("processCode"));
        List<PaymentInt> listApprove = new ArrayList<PaymentInt>();
        int detailLength = (int) param.get("detailLength");
        String[] data = (String[]) param.get("data");
        String bankProcessDate = data[6].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1];
        Media file = (Media) param.get("media");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        Date createdData = new Date();
        String status;

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailLength; i < data.length; i++) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[i])) {
                if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                    LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                            + param.get("processCode"));
                }
                String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String contractNumber = item[1];
                String transactionDate = item[4];

                if (item[9].equals("S") || item[9].equals("W") || item[9].equals("R")) {
                    String reason = item.length > 10 ? item[10] : CONSTANT_UTIL.DEFAULT_EMPTY;
                    Map<String, Object> paramContract = new HashMap<String, Object>();
                    Date processDate = (Date) param.get("processDate");

                    status = (item[9].equals("S") || item[9].equals("W") ? "A" : "R");

                    paramContract.put("contractNumber", contractNumber);
                    paramContract.put("transactionDate", transactionDate);
                    paramContract.put("processDate",
                            dateUtil.getFormattedCurrentDatePattern10(processDate));
                    paramContract.put("status", status);
                    List<PaymentInt> listPaymentInt = PaymentIntDAO
                            .getPaymentETAfterContract(paramContract);
                    for (PaymentInt paymentInt : listPaymentInt) {
                        if (paymentInt != null) {
                            paymentInt.setFilename(file.getName());
                            paymentInt.setDateUpload(createdData);
                            paymentInt.setCreatedBy(userCredential.getUsername());
                            paymentInt.setStatus(status);
                            paymentInt.setReason(reason);
                            // Update status based on file
                            paymentInt.setDtimeStatusUpdated(dateUtil
                                    .getFormattedCurrentDatePattern10(bankProcessDate));
                            paymentInt.setDateBankProcess(dateUtil
                                    .getFormattedCurrentDatePattern10(bankProcessDate));
                        } else {
                            mapContractNotFound.put(contractNumber, contractNumber);
                        }
                    }
                }
            }
        }
        return listApprove;
    }

    @SuppressWarnings("unchecked")
    private void confirmationJFSETWithinFileStep1(Map<String, Object> param) throws Exception {
        LOGGER.debug("Start Step 1 " + param.get("processCode"));
        String[] data = (String[]) param.get("data");
        Map<String, String> parser = (Map<String, String>) param.get("parser");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        int start = Integer.parseInt(parser.get("columnLength"));
        String printedOn = data[data.length - 1].substring(11, data[data.length - 1].length())
                .trim();
        Media file = (Media) param.get("media");
        Date currentDate = new Date();

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = start - 1; i < data.length; i++) {
            if (data[i].contains("HCI")) {
                if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                    LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                            + param.get("processCode"));
                }
                String contractNo = data[i].substring(
                        24,
                        20 + Integer.parseInt(parser.get("column3").split(
                                Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1])).trim();
                ContractStatus contractStatus = PaymentDAO
                        .getJFSContractStatusByContractNumberByStatus(contractNo, "C");
                if (contractStatus == null) {
                    mapContractNotFound.put(contractNo, contractNo);
                } else {
                    contractStatus.setUpdatedBy(userCredential.getUsername());
                    contractStatus.setDtimeUpdated(dateUtil
                            .getFormattedCurrentDatePattern17(printedOn));
                    contractStatus.setDtimeBankProcess(dateUtil
                            .getFormattedCurrentDatePattern17(printedOn));
                    contractStatus.setCreatedBy(userCredential.getUsername());
                    contractStatus.setFilename(file.getName());
                    contractStatus.setDateUpload(currentDate);
                }
            }
        }
        LOGGER.debug("End Step 1 " + param.get("processCode"));
    }

    @SuppressWarnings("unchecked")
    private void confirmationJFSETWithinFileStep3(Map<String, Object> param) throws Exception {
        LOGGER.debug("Start Step 3 " + param.get("processCode"));
        String[] data = (String[]) param.get("data");
        Map<String, String> parser = (Map<String, String>) param.get("parser");
        int start = Integer.parseInt(parser.get("columnLength"));
        List<Contract> listUpdate = new ArrayList<Contract>();
        String printedOn = data[data.length - 1].substring(11, data[data.length - 1].length())
                .trim();
        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = start; i < data.length; i++) {
            if (data[i].equals("HCI")) {
                if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                    LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                            + param.get("processCode"));
                }
                String contractNo = data[i].substring(
                        24,
                        20 + Integer.parseInt(parser.get("column3").split(
                                Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1])).trim();
                // get result
                ContractStatus contractStatus = PaymentDAO
                        .getJFSContractStatusByContractNumberByStatus(contractNo, "C");
                if (contractStatus == null) {
                    mapContractStatusNotFound.put(contractNo, contractNo);
                } else {
                    if (dateUtil.isSameDay(contractStatus.getDtimeUpdated(),
                            dateUtil.getFormattedCurrentDatePattern17(printedOn))) {
                        contractStatus.getTextContractNumber();
                        Contract contract = contractStatus.getTextContractNumber();
                        contract.setStatus(contractStatus.getCodeStatus());
                        listUpdate.add(contract);
                    }
                }
            }
        }
        if (listUpdate.size() > 0) {
            LOGGER.debug("Update Contract ET Data " + param.get("processCode"));
            PaymentDAO.updateByBatch(listUpdate);
            LOGGER.debug("End Update Data " + param.get("processCode"));
        }
        LOGGER.debug("End Step 3 " + param.get("processCode"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void convertJFSEarlyTerminationAfterFile(Map<String, Object> param) throws Exception {
        SystemParameter parameter = systemParameterService
                .getSystemParameterById("VALIDATION_JFS_PAYMENT_ET_AFTER_UPLOAD");
        JSONParser jsonParser = new JSONParser();
        Object obj = jsonParser.parse(parameter.getValue());
        Map<String, String> item = (JSONObject) obj;

        Media file = (Media) param.get("media");

        // Validation
        // header
        StringBuilder content = new StringBuilder();
        String[] data = null;
        if (file.getFormat() != null) {
            data = file.getStringData().split(System.lineSeparator());
            content.append(file.getStringData());
        } else {
            data = new String(file.getByteData()).split(System.lineSeparator());
            content.append(new String(file.getByteData()));
        }
        int length = Integer.parseInt(item.get("lengthHeader"));
        for (int i = 0; i < length; i++) {
            if (!data[i].contains(item.get("header" + (i + 1)))) {
                throw new ApplicationException(ENUM_ERROR.ERR_JFS_PAYMENT_WRONG_HEADER.toString());
            }
        }

        // Process Date
        String tanngalCetak = data[6].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1];
        Date tanggalCetakDate = dateUtil.getFormattedCurrentDatePattern10(tanngalCetak);
        if (!dateUtil.isSameDay(tanggalCetakDate, (Date) param.get("processDate"))) {
            throw new ApplicationException(ENUM_ERROR.ERR_JFS_PROCESS_DATE_INCORRECT.toString());
        }

        // detail
        String detailUploadFile = item.get("detail").trim()
                .replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC, CONSTANT_UTIL.DEFAULT_EMPTY);
        String validationDetail = data[9].trim().replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC,
                CONSTANT_UTIL.DEFAULT_EMPTY);
        if (!detailUploadFile.equals(validationDetail)) {
            throw new ApplicationException("ERR-JFS-PAYMENT-WRONG-DETAIL");
        }

        int detailHeader = Integer.parseInt(item.get("detailHeader"));
        if (data[detailHeader] != null) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[detailHeader])) {
                String temp[] = data[detailHeader].split(Pattern
                        .quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String paymentType = temp[3].replaceAll(CONSTANT_UTIL.DEFAULT_DASH,
                        CONSTANT_UTIL.DEFAULT_UNDERSCORE);
                if (ENUM_PROCESS_TYPE.ET_A.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.PELUNASAN.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_ETA.name());
                    }
                } else {
                    throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_PAYMENT.name());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void convertJFSEarlyTerminationWithinFile(Map<String, Object> param) throws Exception {
        SystemParameter parameter = systemParameterService
                .getSystemParameterById("VALIDATION_JFS_PAYMENT_ET_WITHIN_UPLOAD");
        JSONParser jsonParser = new JSONParser();
        Object obj = jsonParser.parse(parameter.getValue());
        Map<String, String> item = (JSONObject) obj;

        // Validation
        // header
        String[] data = getStringDataFile((Media) param.get("media"));

        int length = Integer.parseInt(item.get("lengthHeader"));
        for (int i = 0; i < length; i++) {
            if (!data[i].contains(item.get("header" + (i + 1)))) {
                throw new ApplicationException(ENUM_ERROR.ERR_JFS_PAYMENT_WRONG_HEADER.toString());
            }
        }

        // detail
        int columnSize = Integer.parseInt(item.get("columnSize"));
        for (int i = 1; i <= columnSize; i++) {
            if (!data[3].contains(item.get("column" + i).split(
                    Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[0])
                    && !data[4].contains(item.get("column" + i).split(
                            Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[0])) {
                throw new ApplicationException(ENUM_ERROR.ERR_JFS_PAYMENT_WRONG_HEADER.toString());
            }
        }

        for (int i = 0; i < data.length; i++) {
            if (data[i].contains("Printed")) {
                String printedOn = data[i].substring(11, 21);
                Date printedOnDate = dateUtil.getFormattedCurrentDatePattern15(printedOn);
                if (!dateUtil.isSameDay(printedOnDate, (Date) param.get("processDate"))) {
                    throw new ApplicationException(
                            ENUM_ERROR.ERR_JFS_PROCESS_DATE_INCORRECT.toString());
                }
            }
        }
    }

    @Override
    public void exportJFSEarlyTerminationAfter15Days(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = batchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory("/WS/Data/JFS/Export/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSEarlyTermninationAfter15Days");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/");

                // instantiate generate file
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.early.termination.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, ",");
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat(CONSTANT_REPORT.ET_AFTER_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_PDF);
                String filenameCsv = processPath.concat(CONSTANT_REPORT.ET_AFTER_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_CSV);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write file
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));

                // Populate file
                List<String> files = new ArrayList<String>();
                String pathTxt = processPath.concat(entry.getFilename());
                files.add(pathTxt);
                files.add(filename);
                files.add(filenameCsv);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.EARLY_TERMINATION_AFTER_15_DAYS);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.EARLY_TERMINATION_AFTER_15_DAYS_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                BigDecimal total1 = BigDecimal.ZERO;
                String[] textLines = text.toString().split("\n");
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(Pattern
                            .quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                    PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                    if (data[0].equals("HCI")) {
                        if (paymentDetailReports.size() == 0) {
                            paymentDetailReport.setRef("-");
                            paymentDetailReport.setTextContractNumber("-");
                            paymentDetailReport.setTransactionDate("-");
                            paymentDetailReport.setTotalEarlyTerminationAmount("-");
                            paymentDetailReport.setSize("0");
                            paymentDetailReport.setTotalAmount("0");
                            paymentDetailReports.add(paymentDetailReport);
                        } else {
                            PaymentDetailReport paymentDetailReportDet = paymentDetailReports
                                    .get(paymentDetailReports.size() - 1);
                            paymentDetailReportDet.setSize(String.valueOf(paymentDetailReports
                                    .size()));
                            paymentDetailReportDet.setTotalAmount(total1.toString());
                            paymentDetailReports.set(paymentDetailReports.size() - 1,
                                    paymentDetailReportDet);
                        }
                    } else {
                        paymentDetailReport.setRef("-");
                        paymentDetailReport.setTextContractNumber(data[0]);
                        paymentDetailReport.setTransactionDate(data[2]);

                        BigDecimal total = BigDecimal.ZERO;
                        BigDecimal installment = new BigDecimal(data[3]);
                        BigDecimal fee = new BigDecimal(data[6]);

                        total = total.add(installment).add(fee);
                        paymentDetailReport.setTotalEarlyTerminationAmount(total.toString());

                        total1 = total1.add(total);
                        paymentDetailReports.add(paymentDetailReport);
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // Create CSV File - Request / Enchance from Kwee Ilona
                if (paymentDetailReports.size() > 0) {
                    // BigDecimal totalAmount =BigDecimal.ZERO;
                    StringBuilder content = new StringBuilder();
                    content.append("REF;TEXT_CONTRACT_NUMBER;TRANSACTION_DATE;AMT_PAYMENT"
                            .concat(System.lineSeparator()));
                    for (PaymentDetailReport item : paymentDetailReports) {
                        content.append(CONSTANT_UTIL.SEMICOLON.concat(item.getTextContractNumber())
                                .concat(CONSTANT_UTIL.SEMICOLON).concat(item.getTransactionDate())
                                .concat(CONSTANT_UTIL.SEMICOLON)
                                .concat(item.getTotalEarlyTerminationAmount())
                                .concat(System.lineSeparator()));
                    }
                    fileUtil.writeToFile(
                            new ByteArrayInputStream(content.toString().getBytes(
                                    StandardCharsets.UTF_8)), filenameCsv);
                    param.put("filename", filenameCsv);
                    param.put("log", batchJobLog);
                    saveGenerateDetailLogExportJFSPayment(param);
                }

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);

                String content = "Export JSF Payment - Early Termination Testing";
                StringWriter outputWriter = new StringWriter();
                outputWriter.write(content);

                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSEarlyTermninationAfter15Days to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY, "Early Termination After 15 Days",
                            htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Sending email failed exportJFSEarlyTermninationAfter15Days");
                }
                LOGGER.debug("End exportJFSEarlyTermninationAfter15Days");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                            + e + "\nThank You");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY, "Early Termination After 15 Days ",
                        htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
            }
        }
    }

    @Override
    public void exportJFSEarlyTerminationWithin15Days(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = batchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory("/WS/Data/JFS/Export/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSEarlyTermninationWithin15Days");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/");

                // instantiate generate file
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.early.termination.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat(CONSTANT_REPORT.ET_WITHIN_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_PDF);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write file
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));

                // Populate file
                List<String> files = new ArrayList<String>();
                String pathTxt = processPath.concat(entry.getFilename());
                files.add(pathTxt);
                files.add(filename);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.EARLY_TERMINATION_WITHIN_15_DAYS);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.EARLY_TERMINATION_WITHIN_15_DAYS_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                String[] textLines = text.toString().split("\n");
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(
                            Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR), -1);
                    PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                    if (data[0].equals("TOTAL") || data[0].contains("TOTAL")) {
                        if (paymentDetailReports.size() == 0) {
                            paymentDetailReport.setRef("-");
                            paymentDetailReport.setDateFinancingBTPNPortionApproval("-");
                            paymentDetailReport.setTextContractNumber("-");
                            paymentDetailReport.setCustomerName("-");
                            paymentDetailReport.setDateCFASigned("-");
                            paymentDetailReport.setFinancingPortionDisbursementAmount("-");
                            paymentDetailReport.setTotalPaidInstallment("-");
                            paymentDetailReport.setTotalEarlyTerminationAmount("-");
                            paymentDetailReports.add(paymentDetailReport);
                        } else {
                            PaymentDetailReport paymentDetailReportDet = paymentDetailReports
                                    .get(paymentDetailReports.size() - 1);
                            paymentDetailReportDet.setSize(String.valueOf(paymentDetailReports
                                    .size()));
                            paymentDetailReportDet.setTotalAmount(data[2]);
                            paymentDetailReports.set(paymentDetailReports.size() - 1,
                                    paymentDetailReportDet);
                        }
                    } else {
                        paymentDetailReport.setRef(data[0]);
                        paymentDetailReport.setDateFinancingBTPNPortionApproval(data[1]);
                        paymentDetailReport.setTextContractNumber(data[2]);
                        paymentDetailReport.setCustomerName(data[3]);
                        paymentDetailReport.setDateCFASigned(data[4]);
                        paymentDetailReport.setFinancingPortionDisbursementAmount(data[5]);
                        paymentDetailReport.setTotalPaidInstallment(data[6]);
                        paymentDetailReport.setTotalEarlyTerminationAmount(data[7]);
                        paymentDetailReports.add(paymentDetailReport);
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);

                // String content = "Export JSF Payment - Early Termination Testing";
                // StringWriter outputWriter = new StringWriter();
                // outputWriter.write(content);
                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSEarlyTerminationWithin15Days to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY, "Early Termination Within 15 Days",
                            htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Send Email failed exportJFSEarlyTermninationWithin15Days");
                }
                LOGGER.debug("End exportJFSEarlyTermninationWithin15Days");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                            + e + "\nThank You");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY, "Early Termination Within 15 Days ",
                        htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
            }
        }
    }

    private String[] getStringDataFile(Media file) {
        return file.getFormat() != null ? file.getStringData().split(System.lineSeparator())
                : new String(file.getByteData()).split(System.lineSeparator());
    }

    private void init() {
        if (mapContractNotFound != null) {
            mapContractNotFound.clear();
        } else {
            mapContractNotFound = new HashMap<String, Object>();
        }
    }
    @Override
    public void mergeJFSETAfterStatus(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 3 Start " + param.get("processCode"));
        int detailLength = (int) param.get("detailLength");
        String[] data = (String[]) param.get("data");
        String dateBankProcess = data[6].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1];
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        Media file = (Media) param.get("media");
        Date currentDate = new Date();

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailLength; i < data.length; i++) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[i])) {
                if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                    LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                            + param.get("processCode"));
                }
                String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String contractNumber = item[1];
                String transactionDate = item[4];
                String status = item[9];
                if ("S".equals(status) || "W".equals(status) || "R".equals(status)) {
                    Map<String, Object> paramContract = new HashMap<String, Object>();
                    Date processDate = (Date) param.get("processDate");

                    status = (item[9].equals("S") || item[9].equals("W") ? "A" : "");

                    paramContract.put("contractNumber", contractNumber);
                    paramContract.put("transactionDate", transactionDate);
                    paramContract.put("processDate",
                            dateUtil.getFormattedCurrentDatePattern10(processDate));
                    paramContract.put("status", status);
                    List<PaymentInt> listPaymentInt = PaymentIntDAO
                            .getPaymentContractByContractWithStatusAndDatePayment(paramContract);

                    if (listPaymentInt != null) {
                        if (listPaymentInt.size() > 0) {
                            List<ContractStatus> listContract = PaymentDAO
                                    .getJFSAllContractStatusByContractNumberByStatus(
                                            contractNumber, "K");
                            if (listContract == null || listContract.isEmpty()) {
                                if (mapContractNotFound.get(contractNumber) == null) {
                                    ContractStatus contractStatus = new ContractStatus();
                                    Contract contract = new Contract();
                                    contract.setTextContractNumber(contractNumber);

                                    contractStatus.setTextContractNumber(contract);
                                    contractStatus.setDtimeCreated(dateUtil
                                            .getFormattedCurrentDatePattern10(dateBankProcess));
                                    contractStatus.setCntDPD(0);
                                    contractStatus.setCodeStatus("K");
                                    contractStatus.setDtimeBankProcess(dateUtil
                                            .getFormattedCurrentDatePattern10(dateBankProcess));
                                    contractStatus.setCreatedBy(userCredential.getUsername());
                                    contractStatus.setFilename(file.getName());
                                    contractStatus.setDateUpload(currentDate);
                                    PaymentDAO.save(contractStatus);
                                }
                            } else {
                                for (ContractStatus contractStatus : listContract) {
                                    contractStatus.setDtimeBankProcess(dateUtil
                                            .getFormattedCurrentDatePattern10(dateBankProcess));
                                    contractStatus.setDtimeUpdated(dateUtil
                                            .getFormattedCurrentDatePattern10(dateBankProcess));
                                    contractStatus.setUpdatedBy(userCredential.getUsername());
                                    contractStatus.setFilename(file.getName());
                                    contractStatus.setDateUpload(currentDate);
                                    PaymentDAO.update(contractStatus);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        LOGGER.debug("Start saveGenerateDetailLogExportJFSPayment " + param.get("processCode"));
        String filename = (String) param.get("filename");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        BatchJobLog log = (BatchJobLog) param.get("log");
        String letterNumber = (String) param.get("letterNumber");

        String name = filename.split(Pattern.quote("\\"))[filename.split(Pattern.quote("\\")).length - 1];
        GenerateFile generateFile = new GenerateFile();
        generateFile.setAgreement(log.getAgreement());
        generateFile.setPartner(log.getPartner());
        generateFile.setProcessType(log.getProcess());
        generateFile.setFileName(filename);
        generateFile.setProceedDate((Date) param.get("proceedDate"));
        generateFile.setCreatedDate(new Date());
        generateFile.setLetterNumber(letterNumber);
        generateFile.setIsCreated(CONSTANT_UTIL.DEFAULT_YES);
        generateFile.setRunIdBatchJobLog(log.getRunId());
        generateFile.setName(name);
        generateFile.setCreatedBy(userCredential.getUsername());
        batchDAO.save(generateFile);
        LOGGER.debug("End saveGenerateDetailLogExportJFSPayment " + param.get("processCode"));
    }

    @Override
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        LOGGER.debug("Start Upload JSP Payment File to Local Temporary " + param.get("processCode"));
        Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                "file.path.upload.early.termination.list", CONSTANT_UTIL.JFS_PROPERTIES,
                CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);

        UserCredential userCredential = (UserCredential) param.get("userCredential");

        String content = null;
        UploadFile uploadFile = new UploadFile();

        ProcessType processType = new ProcessType();
        processType.setCode((String) param.get("processCode"));
        uploadFile.setProcessType(processType);
        uploadFile.setCreatedDate(new Date());
        uploadFile.setSizeFile((String) param.get("size"));
        uploadFile.setChecksum((String) param.get("checksum"));

        if (param.get("isRejected") != null) {
            if (param.get("isRejected").equals(CONSTANT_UTIL.DEFAULT_YES)) {
                uploadFile.setFilepath(((String) param.get("path")).concat((String) param
                        .get("filename")));
                uploadFile.setName((String) param.get("filename"));
                content = (String) param.get("content");
            }
        } else {
            Media file = (Media) param.get("media");
            uploadFile.setName(file.getName());
            uploadFile.setFilepath(mapPath.get(processType.getCode()).concat(file.getName()));

            if (file.getFormat() != null) {
                content = file.getStringData();
            } else {
                content = new String(file.getByteData());
            }
        }

        Agreement agreement = new Agreement();
        agreement.setCode((String) param.get("agreementCode"));
        uploadFile.setAgreement(agreement);

        Partner partner = new Partner();
        partner.setId((String) param.get("partnerCode"));
        uploadFile.setPartner(partner);
        uploadFile.setCreatedBy(userCredential.getUsername());
        uploadFile.setFileType((String) param.get("fileType"));
        batchDAO.save(uploadFile);

        fileUtil.writeToFile(content, uploadFile.getFilepath());
        LOGGER.debug("End Upload JSP Payment File to Local Temporary " + param.get("processCode"));
    }

    public void setApplicationExceptionService(
            ApplicationExceptionService applicationExceptionService) {
        this.applicationExceptionService = applicationExceptionService;
    }

    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setBatchDAO(BatchDAOImpl batchDAO) {
        this.batchDAO = batchDAO;
    }

    public void setContractDAO(ContractDAOImpl contractDAO) {
        this.contractDAO = contractDAO;
    }

    public void setdateUtil(DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    public void setEmailEngine(EmailEngine emailEngine) {
        this.emailEngine = emailEngine;
    }

    public void setFileUtil(FileUtil fileUtil) {
        this.fileUtil = fileUtil;
    }

    public void setPaymentDAO(PaymentDAOImpl paymentDAO) {
        PaymentDAO = paymentDAO;
    }

    public void setPaymentIntDAO(PaymentIntDAOImpl paymentIntDAO) {
        PaymentIntDAO = paymentIntDAO;
    }

    public void setSshManager(SSHManager sshManager) {
        this.sshManager = sshManager;
    }

    public void setSystemParameterService(SystemParameterServiceImpl systemParameterService) {
        this.systemParameterService = systemParameterService;
    }

    @Override
    public void updateContractStatusETAfter(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 2 Start " + param.get("processCode"));
        int detailLength = (int) param.get("detailLength");
        String[] data = (String[]) param.get("data");
        List<Contract> list = new ArrayList<Contract>();
        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailLength; i < data.length; i++) {
            String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
            String contractNumber = item[1];
            String transactionDate = item[4];
            String status = item[9];
            if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                        + param.get("processCode"));
            }
            if ("S".equals(status) || "W".equals(status) || "R".equals(status)) {
                Map<String, Object> paramContract = new HashMap<String, Object>();
                Date processDate = (Date) param.get("processDate");

                status = (item[9].equals("S") || item[9].equals("W") ? "A" : "");

                paramContract.put("contractNumber", contractNumber);
                paramContract.put("transactionDate", transactionDate);
                paramContract.put("processDate",
                        dateUtil.getFormattedCurrentDatePattern10(processDate));
                paramContract.put("status", status);
                List<PaymentInt> listPaymentInt = PaymentIntDAO
                        .getPaymentContractByContractWithStatusAndDatePayment(paramContract);
                if (listPaymentInt != null) {
                    if (listPaymentInt.size() > 0) {
                        Map<String, Object> paramVar = new HashMap<String, Object>();
                        paramVar.put("key", "textContractNumber");
                        paramVar.put("value", contractNumber);
                        // add param
                        List<Map<String, Object>> listParam = new ArrayList<Map<String, Object>>();
                        listParam.add(paramVar);
                        // get data
                        Contract contract = PaymentDAO.getByParam(Contract.class, listParam);
                        if (contract != null) {
                            contract.setStatus("K");
                            list.add(contract);
                        }
                    }
                }
            }
        }
        LOGGER.debug("Step 2 End " + param.get("processCode"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void uploadJFSEarlyTerminationAfterFile(Map<String, Object> param) throws Exception {
        try {
            Media file = (Media) param.get("media");
            SystemParameter parameter = systemParameterService
                    .getSystemParameterById("VALIDATION_JFS_PAYMENT_UPLOAD");
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(parameter.getValue());
            Map<String, String> parser = (JSONObject) obj;

            StringBuilder content = new StringBuilder();
            String[] data = getStringDataFile((Media) param.get("media"));
            if (file.getFormat() != null) {
                content.append(file.getStringData());
            } else {
                content.append(new String(file.getByteData()));
            }
            param.put("headerLength", Integer.parseInt(parser.get("lengthHeader")));
            param.put("detailLength", Integer.parseInt(parser.get("detailHeader")));
            param.put("data", data);
            param.put("parser", parser);

            LOGGER.debug("Start Process Confirmation File " + param.get("processCode"));
            init();
            validationContractETAfter(param);
            confirmationJFSETAfterFile(param);
            updateContractStatusETAfter(param);
            mergeJFSETAfterStatus(param);
            List<String> listRejected = checkingRejectedJFSEarlyTerminationAfterFile(param);

            // get path from properties
            Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                    "file.path.upload.early.termination.list", CONSTANT_UTIL.JFS_PROPERTIES,
                    CONSTANT_UTIL.DEFAULT_SEPARATOR, ",");
            String filenameCsv = "JFS_REJECTED_".concat((String) param.get("processTypeName"))
                    .concat("_").concat((String) param.get("createdDate")).toUpperCase()
                    .concat(".csv");
            String pathfilenameCsv = mapPath.get(param.get("processCode")).concat(filenameCsv);

            if (listRejected != null) {
                if (listRejected.size() > 0) {
                    LOGGER.debug("Create Reporting Rejected");
                    Map<String, Object> paramReject = new HashMap<String, Object>();
                    StringBuilder contentCsv = new StringBuilder();
                    contentCsv
                            .append("CONTRACT_NUMBER;CUSTOMER_NAME;PAYMENT_TYPE;TRANSACTION_DATE;AMT_PAYMENT;STATUS;DESCRIPRION;"
                                    .concat(System.lineSeparator()));
                    for (String item : listRejected) {
                        contentCsv.append(item.concat(System.lineSeparator()));
                    }

                    paramReject.put("isRejected", CONSTANT_UTIL.DEFAULT_YES);
                    paramReject.put("filename", filenameCsv);
                    paramReject.put("content", contentCsv.toString());
                    paramReject.put("path", mapPath.get(param.get("processCode")));
                    paramReject.put("processCode", param.get("processCode"));

                    MessageDigest md = MessageDigest.getInstance("MD5");
                    md.update(contentCsv.toString().getBytes());

                    byte byteData[] = md.digest();
                    // convert the byte to hex format method 1
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < byteData.length; i++) {
                        sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
                    }
                    paramReject.put("checksum", sb.toString());
                    paramReject
                            .put("size", String.valueOf(contentCsv.toString().getBytes().length));
                    paramReject.put("userCredential", param.get("userCredential"));
                    paramReject.put("agreementCode", param.get("agreementCode"));
                    paramReject.put("partnerCode", param.get("partnerCode"));
                    paramReject.put("fileType", param.get("fileType"));
                    saveUploadDetailLogExportJFSPayment(paramReject);

                }
            }
            saveUploadDetailLogExportJFSPayment(param);
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage.append("The process is already done<br>");
            LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
            try {
                if (mapContractNotFound.size() > 0) {
                    htmlMessage.append("Detail Information Contract is not found<br>");
                    int i = 1;
                    for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + entry.getValue() + "<br>");
                        i++;
                    }
                    htmlMessage.append("Please contact our support team.<br>");
                }
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Early Terminination After 15 Days Confirmation",
                        htmlMessage, pathfilenameCsv);
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.debug("Error send email to " + userLogin.getTextEmailAddress()
                        + " with message " + e.getMessage());
            }
            LOGGER.debug("End Process Confirmation File " + param.get("processCode"));
        } catch (Exception e) {
            try {
                Employee userLogin = (Employee) param.get("userLogin");
                StringWriter htmlMessage = new StringWriter();
                htmlMessage.append("The process is unsucessfull<br>");
                if (e instanceof ApplicationException) {
                    ApplicationException ex = (ApplicationException) e;
                    String error = applicationExceptionService.getApplicationExceptionByErrorCode(
                            ex.getErrorCode()).getErrorMessage();
                    htmlMessage.append("For detail information error : <i>" + error + "</i><br>");
                    if (mapContractNotFound.size() > 0) {
                        int i = 1;
                        for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                            htmlMessage.append(i
                                    + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                    + entry.getValue() + "<br>");
                            i++;
                        }
                    }
                } else {
                    htmlMessage.append("For detail information error : \n" + e + "<br>");
                }
                htmlMessage.append("Please contact our support team<br>");
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Early Terminination After 15 Days Confirmation",
                        htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed");
            }
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void uploadJFSEarlyTerminationWithinFile(Map<String, Object> param) throws Exception {
        try {
            SystemParameter parameter = systemParameterService
                    .getSystemParameterById("VALIDATION_JFS_PAYMENT_ET_WITHIN_UPLOAD");
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(parameter.getValue());
            Map<String, String> parser = (JSONObject) obj;

            param.put("headerLength", Integer.parseInt(parser.get("lengthHeader")));
            param.put("detailHeader", Integer.parseInt(parser.get("detailHeader")));
            String[] data = getStringDataFile((Media) param.get("media"));
            param.put("data", data);
            param.put("parser", parser);

            LOGGER.debug("Start confirmation JSP Early Termination Within File "
                    + param.get("processCode"));
            init();
            confirmationJFSETWithinFileStep1(param);
            confirmationJFSETWithinFileStep3(param);
            saveUploadDetailLogExportJFSPayment(param);

            LOGGER.debug("End confirmation JSP Early Termination Within File "
                    + param.get("processCode"));
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage.append("The process is already done.<br>Thank You");
            try {
                if (mapContractNotFound.size() > 0) {
                    htmlMessage.append("Detail Information Contract is not found<br>");
                    int i = 1;
                    for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + entry.getValue() + "<br>");
                        i++;
                    }
                }

                if (mapContractStatusNotFound.size() > 0) {
                    htmlMessage.append("Detail Information Contract status is not found<br>");
                    int i = 1;
                    for (Map.Entry<String, Object> entry : mapContractStatusNotFound.entrySet()) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + entry.getValue() + "<br>");
                        i++;
                    }
                }
                if (mapContractNotFound.size() > 0 || mapContractStatusNotFound.size() > 0) {
                    htmlMessage.append("Please contact our support team or jfs team<br>");
                }
                htmlMessage.append("Thank You<br>");
                emailEngine.sendEmail("ist@homecredit.co.id", userLogin.getTextEmailAddress(),
                        CONSTANT_UTIL.DEFAULT_EMPTY, CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Early Terminination Within 15 Days Confirmation",
                        htmlMessage);
            } catch (Exception e) {
                LOGGER.debug("Error send email to " + userLogin.getTextEmailAddress()
                        + " with message " + e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.<br>For detail information error : "
                            + e);
            if (mapContractNotFound.size() > 0) {
                htmlMessage.append("Detail Information Contract is not found<br>");
                int i = 1;
                for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                    htmlMessage.append(i
                            + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                            + entry.getValue() + "<br>");
                    i++;
                }
            }

            if (mapContractStatusNotFound.size() > 0) {
                htmlMessage.append("Detail Information Contract status is not found<br>");
                int i = 1;
                for (Map.Entry<String, Object> entry : mapContractStatusNotFound.entrySet()) {
                    htmlMessage.append(i
                            + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                            + entry.getValue() + "<br>");
                    i++;
                }
            }
            if (mapContractNotFound.size() > 0 || mapContractStatusNotFound.size() > 0) {
                htmlMessage.append("Please contact our support team or jfs team<br>");
            }
            htmlMessage.append("Thank You<br>");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Early Terminination Within 15 Days Confirmation",
                        htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
            }
            throw e;
        }
    }

    @Override
    public void validationContractETAfter(Map<String, Object> param) throws Exception {
        // Get Type Agreement
        SystemParameter sysParam = systemParameterService
                .getSystemParameterById("JFS_AGREEMENT_REGULAR");
        if (sysParam == null) {
            throw new ApplicationException(
                    ENUM_ERROR.ERR_CONFIGURE_AGREEMENT_TYPE_PROCESS.toString());
        }
        String type = sysParam.getValue();
        // Done
        String[] data = getStringDataFile((Media) param.get("media"));
        int detailLength = (int) param.get("detailLength");

        LOGGER.debug("Validation Data " + param.get("processCode"));
        for (int i = detailLength; i < data.length; i++) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[i])) {
                String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String contractNumber = item[1];
                Contract contract = contractDAO.getContractByAgreementType(contractNumber, type);
                if (contract == null) {
                    mapContractNotFound.put(contractNumber, data[i]);
                }
            }
        }
        LOGGER.debug("Result Size " + mapContractNotFound.size());
        LOGGER.debug("End Validation Data " + param.get("processCode"));
    }
}
