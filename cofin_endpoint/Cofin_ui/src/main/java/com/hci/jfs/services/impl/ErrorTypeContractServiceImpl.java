package com.hci.jfs.services.impl;

import java.util.List;

import com.hci.jfs.dao.ErrorTypeContractDAO;
import com.hci.jfs.entity.ErrorTypeContract;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ErrorTypeContractService;

public class ErrorTypeContractServiceImpl implements ErrorTypeContractService {

	private ErrorTypeContractDAO errorTypeContractDAO;

	public void setErrorTypeContractDAO(ErrorTypeContractDAO errorTypeContractDAO) {
		this.errorTypeContractDAO = errorTypeContractDAO;
	}

	@Override
	public List<ErrorTypeContract> getAllErrorTypeContract()
			throws ApplicationException {
		return errorTypeContractDAO.getAllErrorTypeContract();
	}

	@Override
	public void insertErrorTypeContract(ErrorTypeContract errorTypeContract)
			throws ApplicationException {
		errorTypeContractDAO.insertErrorTypeContract(errorTypeContract);
	}
}
