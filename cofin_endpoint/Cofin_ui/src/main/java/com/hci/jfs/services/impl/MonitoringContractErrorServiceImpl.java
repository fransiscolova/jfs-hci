package com.hci.jfs.services.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hci.jfs.dao.MonitoringContractErrorDAO;
import com.hci.jfs.entity.ErrorContractMonitoring;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.MonitoringContractErrorService;

public class MonitoringContractErrorServiceImpl implements
MonitoringContractErrorService {

	final static Logger logger = LogManager.getLogger(MonitoringContractErrorServiceImpl.class.getName());

	MonitoringContractErrorDAO monitoringContractErrorDAO;

	public void setMonitoringContractErrorDAO(
			MonitoringContractErrorDAO monitoringContractErrorDAO) {
		this.monitoringContractErrorDAO = monitoringContractErrorDAO;
	}

	@Override
	public void insert(List<ErrorContractMonitoring> list)
			throws ApplicationException {
		monitoringContractErrorDAO.insert(list);
	}
}
