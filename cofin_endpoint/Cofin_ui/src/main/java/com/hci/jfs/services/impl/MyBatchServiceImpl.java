package com.hci.jfs.services.impl;

import java.util.List;

import com.hci.jfs.dao.impl.AgreementDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.services.MyBatchService;

public class MyBatchServiceImpl implements MyBatchService {

	private AgreementDAOImpl agreementDAO;

	public void setAgreementDAO(AgreementDAOImpl agreementDAO) {
		this.agreementDAO = agreementDAO;
	}
	
	@Override
	public void batchAgreement() {
		try {
			List<Agreement> result = agreementDAO.getAllAgreement();

			if (result.size() > 0) {	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
