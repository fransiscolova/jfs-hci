package com.hci.jfs.services.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.hci.jfs.composer.partner.PartnerRegistrationComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.dao.impl.ContractDAOImpl;
import com.hci.jfs.dao.impl.PartnerDAOImpl;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.services.PartnerService;
import com.hci.jfs.ws.entity.Commodity;
import com.hci.jfs.ws.entity.Contract;
import com.hci.jfs.ws.entity.ContractJFSPartnership;
import com.hci.jfs.ws.entity.Customer;
import com.hci.jfs.ws.entity.Product;

public class PartnerServiceImpl extends SpringBeanAutowiringSupport implements PartnerService {

    private PartnerDAOImpl partnerDAO;
    private ContractDAOImpl contractDAO;

    @Override
    public void addPartnerData(Partner partner) {
        partnerDAO.save(partner);
        List<Partner> partners = PartnerRegistrationComposer.partners;
        partners.add(partner);
        Collections.sort(partners, (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName()));
    }

    @Override
    public void changeParterNactiveStatus(String partnerId, String flag) {
        Partner partner = partnerDAO.getPartnerById(partnerId);
        partner.setIsDelete(flag);
        partnerDAO.update(partner);
    }

    @Override
    public List<Partner> getAllPartner() {
        return partnerDAO.getAllPartner();
    }

    public ContractJFSPartnership GetContractJFSPartnership(Contract contract, Customer customer,
            Product product, Commodity commodity) {

        com.hci.jfs.entity.Contract item = null;
        if (contract != null) {
            if (contract.getContractCode() != null) {
                try {
                    item = contractDAO.getContractByContractCode(contract.getContractCode());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        ContractJFSPartnership partnership = new ContractJFSPartnership();
        if (item != null) {
            partnership.setPartnerCode(item.getIdAgreement().getPartner().getId()
                    .concat(CONSTANT_UTIL.MINUS)
                    .concat(item.getIdAgreement().getPartner().getName()));
            partnership.setPartnershipAgreementCode(item.getIdAgreement().getCodeAgreement());
            partnership.setPartnershipShare(BigDecimal.ZERO);

            GregorianCalendar gcalFrom = new GregorianCalendar();
            gcalFrom.setTime(item.getIdAgreement().getValidFrom());
            GregorianCalendar gcalTo = new GregorianCalendar();
            gcalTo.setTime(item.getIdAgreement().getValidTo());
            XMLGregorianCalendar xgcalFrom = null;
            XMLGregorianCalendar xgcalTo = null;
            try {
                xgcalFrom = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcalFrom);

                xgcalTo = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcalTo);
            } catch (DatatypeConfigurationException e) {
                e.printStackTrace();
            }

            partnership.setValidFrom(xgcalFrom);
            partnership.setValidTo(xgcalTo);
        }
        return partnership;
    }

    @Override
    public Partner getPartner(String partnerId) {
        return partnerDAO.getPartnerById(partnerId);
    }

    @Override
    public Partner getPartnerByContractNumber(String contractNumber) throws Exception {
        return partnerDAO.getPartnerByContractNumber(contractNumber);
    }

    @Autowired
    public void setcontractDAO(ContractDAOImpl contractDAO) {
        this.contractDAO = contractDAO;
    }

    @Autowired
    public void setPartnerDAO(PartnerDAOImpl partnerDAO) {
        this.partnerDAO = partnerDAO;
    }

    @Override
    public void updatePartnerData(Partner partner) {
        partnerDAO.update(partner);
        List<Partner> partners = PartnerRegistrationComposer.partners;
        for (Partner p : partners) {
            if (p.getId().equals(partner.getId())) {
                p.setName(partner.getName());
                p.setAddress(partner.getAddress());
                p.setMainContact(partner.getMainContact());
                p.setPhoneNumber(partner.getPhoneNumber());
                p.setEmail(partner.getEmail());
            }
        }
        Collections.sort(partners, (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName()));
    }
}
