package com.hci.jfs.services.impl;

import java.io.FileOutputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Pattern;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.util.media.Media;

import com.hci.jfs.constant.CONSTANT_REPORT;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.constant.ENUM_ERROR;
import com.hci.jfs.constant.ENUM_PAYMENT_TYPE;
import com.hci.jfs.constant.ENUM_PROCESS_TYPE;
import com.hci.jfs.dao.impl.BatchDAOImpl;
import com.hci.jfs.dao.impl.ContractDAOImpl;
import com.hci.jfs.dao.impl.PaymentDAOImpl;
import com.hci.jfs.dao.impl.PaymentIntDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.BatchJobLog;
import com.hci.jfs.entity.Contract;
import com.hci.jfs.entity.ContractStatus;
import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.GenerateFile;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.entity.PaymentAllocation;
import com.hci.jfs.entity.PaymentInt;
import com.hci.jfs.entity.ProcessType;
import com.hci.jfs.entity.SystemParameter;
import com.hci.jfs.entity.UploadFile;
import com.hci.jfs.entity.report.PaymentDetailReport;
import com.hci.jfs.entity.report.PaymentReport;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ApplicationExceptionService;
import com.hci.jfs.services.PaymentIntService;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.ApplicationProperties;
import com.hci.jfs.util.DateUtil;
import com.hci.jfs.util.EmailEngine;
import com.hci.jfs.util.FileUtil;
import com.hci.jfs.util.SSHManager;
import com.jcraft.jsch.ChannelSftp;

@Transactional
public class PaymentIntServiceImpl implements PaymentIntService {
    final static Logger LOGGER = LogManager.getLogger(PaymentIntServiceImpl.class.getName());

    private ContractDAOImpl contractDAO;
    private BatchDAOImpl BatchDAO;
    private PaymentIntDAOImpl PaymentIntDAO;
    private SystemParameterServiceImpl systemParameterService;
    private PaymentDAOImpl PaymentDAO;
    private DateUtil dateUtil;
    private ApplicationProperties applicationProperties;
    private EmailEngine emailEngine;
    private SSHManager sshManager;
    private FileUtil fileUtil;

    private ApplicationExceptionService applicationExceptionService;

    private Map<String, Object> mapContractNotFound;

    private List<String> checkingRejectedJFSPaymentFile(Map<String, Object> param) throws Exception {
        List<String> listReject = new ArrayList<String>();
        int detailLength = (int) param.get("detailLength");
        String[] data = (String[]) param.get("data");
        Map<String, Object> paramContract = new HashMap<String, Object>();
        for (int i = detailLength; i < data.length; i++) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[i])) {
                String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String status = item[9];

                if (!status.equals("S")) {
                    String contractNumber = item[1];
                    String transactionDate = item[4];
                    paramContract.put("contractNumber", contractNumber);
                    paramContract.put("transactionDate", transactionDate);
                    List<PaymentInt> listPaymentInt = PaymentIntDAO
                            .getPaymentContractByContractWithStatusAndDatePayment(paramContract);

                    if (listPaymentInt != null) {
                        if (listPaymentInt.size() > 0) {
                            listReject.add(data[i]);
                        }
                    }
                }
            }
        }
        return listReject;
    }

    private void confirmationJFSPaymentFile(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 1 Start " + param.get("processCode"));
        int detailLength = (int) param.get("detailLength");
        String[] data = (String[]) param.get("data");
        String bankProcessDate = data[6].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1];
        Media file = (Media) param.get("media");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        Date createdData = new Date();

        String status;
        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailLength; i < data.length; i++) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[i])) {
                if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                    LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                            + param.get("processCode"));
                }
                String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String contractNumber = item[1];
                String transactionDate = item[4];

                if (item[9].equals("S") || item[9].equals("W") || item[9].equals("R")) {
                    String reason = item.length > 10 ? item[10] : CONSTANT_UTIL.DEFAULT_EMPTY;
                    String processType = (String) param.get("processCode");
                    Map<String, Object> paramContract = new HashMap<String, Object>();
                    Date processDate = (Date) param.get("processDate");
                    List<PaymentInt> listPaymentInt = null;
                    status = (item[9].equals("S") || item[9].equals("W") ? "A" : "R");
                    paramContract.put("contractNumber", contractNumber);
                    paramContract.put("transactionDate", transactionDate);
                    paramContract.put("processDate",
                            dateUtil.getFormattedCurrentDatePattern10(processDate));
                    paramContract.put("status", status);
                    try {
                        if (ENUM_PROCESS_TYPE.P_REGULAR.name().equals(processType)) {
                            listPaymentInt = PaymentIntDAO.getPaymentRegularContract(paramContract);
                        } else if (ENUM_PROCESS_TYPE.P_REVERSAL.name().equals(processType)) {
                            listPaymentInt = PaymentIntDAO.getPaymentRegularContract(paramContract);
                        } else if (ENUM_PROCESS_TYPE.P_GIFT.name().equals(processType)) {
                            listPaymentInt = PaymentIntDAO.getPaymentGiftContract(paramContract);
                        } else if (ENUM_PROCESS_TYPE.P_SU.name().equals(processType)) {
                            listPaymentInt = PaymentIntDAO
                                    .getPaymentSmallUnderContract(paramContract);
                        } else if (ENUM_PROCESS_TYPE.P_LP.name().equals(processType)) {
                            listPaymentInt = PaymentIntDAO
                                    .getPaymentLastPaymentContract(paramContract);
                        }

                        for (PaymentInt paymentInt : listPaymentInt) {
                            if (paymentInt != null) {
                                paymentInt.setFilename(file.getName());
                                paymentInt.setDateUpload(createdData);
                                paymentInt.setCreatedBy(userCredential.getUsername());
                                paymentInt.setStatus(status);
                                paymentInt.setReason(reason);
                                paymentInt.setDtimeStatusUpdated(dateUtil
                                        .getFormattedCurrentDatePattern10(bankProcessDate));
                                paymentInt.setDateBankProcess(dateUtil
                                        .getFormattedCurrentDatePattern10(bankProcessDate));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOGGER.debug("Error confirmationJFSPaymentFile " + param.get("processCode")
                                + " with data : " + data[i]);
                        LOGGER.debug("Error Message " + e);
                        throw e;
                    }
                }
            }
        }
    }

    @Override
    public void convertJFSPaymentAllocationFile(Map<String, Object> param) throws Exception {
        Media file = (Media) param.get("media");
        String[] data = null;
        if (file.getFormat() != null) {
            data = file.getStringData().split(System.lineSeparator());
        } else {
            data = new String(file.getByteData()).split(System.lineSeparator());
        }

        // Validation
        // Filename
        int size = Integer.valueOf(applicationProperties.getProperties(
                "jfs.payment.allocation.file.header.size", CONSTANT_UTIL.JFS_PROPERTIES));
        for (int i = 0; i < size; i++) {
            if (!data[i]
                    .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                    .trim()
                    .contains(
                            applicationProperties
                                    .getProperties("jfs.payment.allocation.file.header." + (i + 1),
                                            CONSTANT_UTIL.JFS_PROPERTIES)
                                    .replace(CONSTANT_UTIL.DEFAULT_DASH,
                                            CONSTANT_UTIL.DEFAULT_EMPTY).trim())
                    && !data[i]
                            .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                            .trim()
                            .equals(applicationProperties
                                    .getProperties("jfs.payment.allocation.file.header." + (i + 1),
                                            CONSTANT_UTIL.JFS_PROPERTIES)
                                    .replace(CONSTANT_UTIL.DEFAULT_DASH,
                                            CONSTANT_UTIL.DEFAULT_EMPTY).trim())) {
                throw new ApplicationException(ENUM_ERROR.ERR_JFS_PAYMENT_WRONG_HEADER.toString());
            }
        }

        // Process Date
        String tanggalCetak = data[6].substring(18, 28);
        Date tanggalCetakDate = dateUtil.getFormattedCurrentDatePattern15(tanggalCetak);
        if (!dateUtil.isSameDay(tanggalCetakDate, (Date) param.get("processDate"))) {
            throw new ApplicationException(ENUM_ERROR.ERR_JFS_PROCESS_DATE_INCORRECT.toString());
        }

        // Detail Column
        if (!data[9]
                .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                .trim()
                .contains(
                        applicationProperties
                                .getProperties("jfs.payment.allocation.file.detail",
                                        CONSTANT_UTIL.JFS_PROPERTIES)
                                .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                                .trim())) {
            throw new ApplicationException("ERR-JFS-PAYMENT-WRONG-DETAIL");
        }

        int detailSize = Integer.parseInt(applicationProperties.getProperties(
                "jfs.payment.allocation.file.detail.size", CONSTANT_UTIL.JFS_PROPERTIES));

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        if (data[detailSize - 1] != null) {
            if (data[detailSize - 1].contains("HCI") && !data[detailSize - 1].contains("HCIJF01")) {
                String paymentType = data[detailSize - 1].substring(185,
                        data[detailSize - 1].length()).trim();
                paymentType = paymentType.replaceAll(CONSTANT_UTIL.DEFAULT_DASH,
                        CONSTANT_UTIL.DEFAULT_UNDERSCORE);
                String contractNumber = data[detailSize - 1].substring(12, 22).trim();

                if (ENUM_PROCESS_TYPE.P_REGULAR.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.PEMB_ANGSURAN.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_REGULAR.name());
                    }
                    String amtBankPrincipal = data[detailSize - 1].substring(87, 105).trim();
                    String amtBankInterest = data[detailSize - 1].substring(105, 124).trim();
                    amtBankInterest = (amtBankInterest == null || amtBankInterest
                            .equals(CONSTANT_UTIL.DEFAULT_EMPTY)) ? "0" : amtBankInterest;

                    BigDecimal bankPrincipal = new BigDecimal(
                            (amtBankPrincipal == null || amtBankPrincipal
                                    .equals(CONSTANT_UTIL.DEFAULT_EMPTY)) ? "0" : amtBankPrincipal
                                    .replaceAll(CONSTANT_UTIL.DEFAULT_COMMA,
                                            CONSTANT_UTIL.DEFAULT_EMPTY));
                    BigDecimal bankInterest = new BigDecimal(
                            (amtBankInterest == null || amtBankInterest
                                    .equals(CONSTANT_UTIL.DEFAULT_EMPTY)) ? "0" : amtBankInterest
                                    .replaceAll(CONSTANT_UTIL.DEFAULT_COMMA,
                                            CONSTANT_UTIL.DEFAULT_EMPTY));

                    if (bankPrincipal.compareTo(BigDecimal.ZERO) < 0
                            && bankInterest.compareTo(BigDecimal.ZERO) < 0) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_REGULAR.name());
                    }

                } else if (ENUM_PROCESS_TYPE.P_REVERSAL.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.PEMB_ANGSURAN.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_REVERSAL.name());
                    }

                    String amtBankPrincipal = data[detailSize - 1].substring(87, 105).trim();
                    String amtBankInterest = data[detailSize - 1].substring(105, 124).trim();
                    amtBankInterest = (amtBankInterest == null || amtBankInterest
                            .equals(CONSTANT_UTIL.DEFAULT_EMPTY)) ? "0" : amtBankInterest;

                    BigDecimal bankPrincipal = new BigDecimal(
                            (amtBankPrincipal == null || amtBankPrincipal
                                    .equals(CONSTANT_UTIL.DEFAULT_EMPTY)) ? "0" : amtBankPrincipal
                                    .replaceAll(CONSTANT_UTIL.DEFAULT_COMMA,
                                            CONSTANT_UTIL.DEFAULT_EMPTY));
                    BigDecimal bankInterest = new BigDecimal(
                            (amtBankInterest == null || amtBankInterest
                                    .equals(CONSTANT_UTIL.DEFAULT_EMPTY)) ? "0" : amtBankInterest
                                    .replaceAll(CONSTANT_UTIL.DEFAULT_COMMA,
                                            CONSTANT_UTIL.DEFAULT_EMPTY));

                    if (bankPrincipal.compareTo(BigDecimal.ZERO) > 0
                            && bankInterest.compareTo(BigDecimal.ZERO) > 0) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_REVERSAL.name());
                    }
                } else if (ENUM_PROCESS_TYPE.P_GIFT.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.GIFT_INST.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_GIFT.name());
                    }
                } else if (ENUM_PROCESS_TYPE.ET_A.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.PEMB_ANGSURAN.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_ETA.name());
                    }

                    Map<String, Object> paramContract = new HashMap<String, Object>();
                    paramContract.put("contractNumber", contractNumber);
                    paramContract.put("paymentType", "T");
                    List<PaymentInt> listPaymentInt = PaymentIntDAO
                            .getPaymentIntByContractNumberAndPaymentType(paramContract);
                    if (listPaymentInt == null || listPaymentInt.size() == 0) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_ETA.name());
                    }
                } else if (ENUM_PROCESS_TYPE.P_SU.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.UNDERPAYS.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_SUP.name());
                    }
                } else if (ENUM_PROCESS_TYPE.P_LP.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.LUNAS.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_LAST.name());
                    }
                } else {
                    throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_PAYMENT.name());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void convertJFSPaymentFile(Map<String, Object> param) throws Exception {
        LOGGER.debug("Start convertJFSPaymentFile");
        SystemParameter parameter = systemParameterService
                .getSystemParameterById("VALIDATION_JFS_PAYMENT_UPLOAD");
        JSONParser jsonParser = new JSONParser();
        Object obj = jsonParser.parse(parameter.getValue());
        Map<String, String> item = (JSONObject) obj;

        // Validation
        String[] data = getStringDataFile((Media) param.get("media"));

        LOGGER.debug("Validation " + param.get("processCode"));
        LOGGER.debug("Header");
        int length = Integer.parseInt(item.get("lengthHeader"));
        for (int i = 0; i < length; i++) {
            if (!data[i].contains(item.get("header" + (i + 1)))) {
                throw new ApplicationException(ENUM_ERROR.ERR_JFS_PAYMENT_WRONG_HEADER.toString());
            }
        }

        LOGGER.debug("Process Date");
        String tanngalCetak = data[6].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1];
        Date tanggalCetakDate = dateUtil.getFormattedCurrentDatePattern10(tanngalCetak);
        if (!dateUtil.isSameDay(tanggalCetakDate, (Date) param.get("processDate"))) {
            throw new ApplicationException(ENUM_ERROR.ERR_JFS_PROCESS_DATE_INCORRECT.toString());
        }
        // detail
        LOGGER.debug("Detail");
        String detailUploadFile = item.get("detail").trim()
                .replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC, CONSTANT_UTIL.DEFAULT_EMPTY);
        String validationDetail = data[9].trim().replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC,
                CONSTANT_UTIL.DEFAULT_EMPTY);
        if (!detailUploadFile.equals(validationDetail)) {
            throw new ApplicationException("ERR-JFS-PAYMENT-WRONG-DETAIL");
        }

        int detailHeader = Integer.parseInt(item.get("detailHeader"));
        if (data[detailHeader] != null) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[detailHeader])) {
                String temp[] = data[detailHeader].split(Pattern
                        .quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String paymentType = temp[3].replaceAll(CONSTANT_UTIL.DEFAULT_DASH,
                        CONSTANT_UTIL.DEFAULT_UNDERSCORE);
                if (ENUM_PROCESS_TYPE.P_REGULAR.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.PEMB_ANGSURAN.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_REGULAR.name());
                    }
                } else if (ENUM_PROCESS_TYPE.P_REVERSAL.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.REVERSAL.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_REVERSAL.name());
                    }
                } else if (ENUM_PROCESS_TYPE.P_GIFT.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.GIFT_INST.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_GIFT.name());
                    }
                } else if (ENUM_PROCESS_TYPE.P_SU.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.UNDERPAYS.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_SUP.name());
                    }
                } else if (ENUM_PROCESS_TYPE.P_LP.name().equals(param.get("processCode"))) {
                    if (!paymentType.equals(ENUM_PAYMENT_TYPE.LUNAS.name())) {
                        throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_P_LAST.name());
                    }
                } else {
                    throw new ApplicationException(ENUM_ERROR.ERR_FILENAME_PAYMENT.name());
                }
            }
        }
        LOGGER.debug("End convertJFSPaymentFile");
    }

    @Override
    public void exportJFSPaymentGift(Map<String, Object> param) throws Exception {

        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory("/WS/Data/JFS/Export/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSPaymentGift");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/");

                // Instantiate generate files
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.payment.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
                String filename = mapPath.get(processCode)
                        .concat(CONSTANT_REPORT.GIFT_PAYMENT_FILENAME).concat(runDate)
                        .concat(CONSTANT_UTIL.TYPE_PDF);
                String filenameTxt = entry.getFilename().replaceAll("DETAIL", "DETAIL1")
                        .replaceAll("csv", "txt");
                String filenameAes = "JFS_PAYMENT_GIFT_"
                        + runDate.concat(CONSTANT_UTIL.TYPE_TXT_AES);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write to files
                fileUtil.writeToFile(sftpChannel.get(filenameTxt),
                        mapPath.get(processCode).concat(filenameTxt));
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()), mapPath.get(processCode)
                        .concat(entry.getFilename()));
                fileUtil.writeToFile(sftpChannel.get(filenameAes),
                        mapPath.get(processCode).concat(filenameAes));

                // Populate files
                List<String> files = new ArrayList<String>();
                String pathTxt = mapPath.get(processCode).concat(filenameTxt);
                String pathAes = mapPath.get(processCode).concat(filenameAes);
                String pathCsv = mapPath.get(processCode).concat(entry.getFilename());
                files.add(pathTxt);
                files.add(pathAes);
                files.add(pathCsv);
                files.add(filename);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_GIFT);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_GIFT_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(CONSTANT_UTIL.SEMICOLON, -1);
                    try {
                        PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                        if (data[0].equals("TOTAL")) {
                            if (paymentDetailReports.size() == 0) {
                                PaymentDetailReport paymentDetailReportDet = new PaymentDetailReport();
                                paymentDetailReport.setRef(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setTextContractNumber(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setTransactionDate(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setAmtPayment(CONSTANT_UTIL.MINUS);
                                paymentDetailReportDet.setSize("0");
                                paymentDetailReportDet.setTotalAmount(data[2]);
                                paymentDetailReports.add(paymentDetailReportDet);
                            } else {
                                PaymentDetailReport paymentDetailReportDet = paymentDetailReports
                                        .get(paymentDetailReports.size() - 1);
                                paymentDetailReportDet.setSize(String.valueOf(paymentDetailReports
                                        .size()));
                                paymentDetailReportDet.setTotalAmount(data[2]);
                                paymentDetailReports.set(paymentDetailReports.size() - 1,
                                        paymentDetailReportDet);
                            }
                        } else {
                            paymentDetailReport.setRef(data[0]);
                            paymentDetailReport.setTextContractNumber(data[1]);
                            paymentDetailReport.setTransactionDate(data[2]);
                            paymentDetailReport.setAmtPayment(data[3]);
                            paymentDetailReports.add(paymentDetailReport);
                        }
                    } catch (Exception e) {
                        LOGGER.debug("Error exportJFSPaymentGift " + data[i] + " with error " + e);
                        throw e;
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);

                // String content = "Export JSF Payment - Payment Regular Testing";
                // StringWriter outputWriter = new StringWriter();
                // outputWriter.write(content);
                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSPaymentGift to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Notification - Generate JFS Payment Gift", htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Send email failed exportJFSPaymentGift");
                }
                LOGGER.debug("End exportJFSPaymentGift");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                            + e + "\nThank You");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY, "Notification - Generate JFS Payment Gift",
                        htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
            }
            throw e;
        }
    }

    @Override
    public void exportJFSPaymentLastPayment(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory("/WS/Data/JFS/Export/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSPaymentLastPayment");
                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/");

                // Instantiate generate files
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.payment.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat(CONSTANT_REPORT.LAST_PAYMENT_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_PDF);
                String filenameTxt = entry.getFilename().replaceAll("DETAIL", "DETAIL1")
                        .replaceAll("csv", "txt");
                String filenameAes = "JFS_PAYMENT_LAST_"
                        + runDate.concat(CONSTANT_UTIL.TYPE_TXT_AES);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write to files
                fileUtil.writeToFile(sftpChannel.get(filenameTxt), processPath.concat(filenameTxt));
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));
                fileUtil.writeToFile(sftpChannel.get(filenameAes), processPath.concat(filenameAes));

                // Populate files
                List<String> files = new ArrayList<String>();
                String pathTxt = processPath.concat(filenameTxt);
                String pathAes = processPath.concat(filenameAes);
                String pathCsv = processPath.concat(entry.getFilename());
                files.add(pathTxt);
                files.add(pathAes);
                files.add(pathCsv);
                files.add(filename);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_LAST_PAYMENT);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_LAST_PAYMENT_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(CONSTANT_UTIL.SEMICOLON, -1);
                    try {
                        PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                        if (data[0].equals("TOTAL")) {
                            if (paymentDetailReports.size() > 0) {
                                PaymentDetailReport paymentDetailReportDet = paymentDetailReports
                                        .get(paymentDetailReports.size() - 1);
                                paymentDetailReportDet.setSize(String.valueOf(paymentDetailReports
                                        .size()));
                                paymentDetailReportDet.setTotalAmount(String
                                        .valueOf(paymentDetailReports.size()));
                                paymentDetailReports.set(paymentDetailReports.size() - 1,
                                        paymentDetailReportDet);
                            } else {
                                paymentDetailReport.setRef(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setTextContractNumber(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setTransactionDate(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setAmtPayment(CONSTANT_UTIL.NUMBER_0);
                                paymentDetailReport.setSize(CONSTANT_UTIL.NUMBER_0);
                                paymentDetailReport.setTotalAmount(CONSTANT_UTIL.NUMBER_0);
                                paymentDetailReports.add(paymentDetailReport);
                            }

                        } else {
                            paymentDetailReport.setRef(data[0]);
                            paymentDetailReport.setTextContractNumber(data[1]);
                            paymentDetailReport.setTransactionDate(data[2]);
                            paymentDetailReport.setAmtPayment(data[3]);
                            paymentDetailReports.add(paymentDetailReport);
                        }
                    } catch (Exception e) {
                        LOGGER.debug("Error exportJFSPaymentLastPayment " + data[i]
                                + " with error " + e);
                        throw e;
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);

                // String content = "Export JSF Payment - Payment Regular Testing";
                // StringWriter outputWriter = new StringWriter();
                // outputWriter.write(content);
                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSPaymentLastPayment to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Notification - Generate JFS Payment Last", htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Send email failed exportJFSPaymentLastPayment");
                }
                LOGGER.debug("End exportJFSPaymentLastPayment");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                            + e + "\nThank You");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Generate JFS Payment Last Payment", htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
            }
        }

    }

    @Override
    public void exportJFSPaymentRegular(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            String path = applicationProperties.getProperties(
                    CONSTANT_UTIL.APPLICATION_PROPERTIES_JFS_PATH_GENERATE_1,
                    CONSTANT_UTIL.APPLICATION_PROPERTIES_JFS_KEY);
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory(path);
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSPaymentRegular");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd(path);

                // Instantiate generate files
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.payment.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
                String pathProcess = mapPath.get(processCode);
                String filename = pathProcess.concat(CONSTANT_REPORT.REGULAR_PAYMENT_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_PDF);
                String filenameXls = pathProcess.concat(CONSTANT_REPORT.REGULAR_PAYMENT_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_XLS);
                LOGGER.debug("Filename " + filename);
                LOGGER.debug("Filename " + filenameXls);
                String filenameTxt = entry.getFilename().replaceAll("DETAIL", "DETAIL1")
                        .replaceAll("csv", "txt");
                String filenameAes = "JFS_PAYMENT_" + runDate.concat(CONSTANT_UTIL.TYPE_TXT_AES);
                String pathTxt = pathProcess.concat(filenameTxt);
                String pathAes = pathProcess.concat(filenameAes);
                String pathCsv = pathProcess.concat(entry.getFilename());

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write to files
                fileUtil.writeToFile(sftpChannel.get(filenameTxt), pathProcess.concat(filenameTxt));
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        pathProcess.concat(entry.getFilename()));
                fileUtil.writeToFile(sftpChannel.get(filenameAes), pathProcess.concat(filenameAes));

                // Populate files
                List<String> files = new ArrayList<String>();
                files.add(filename);
                files.add(filenameXls);
                files.add(pathTxt);
                files.add(pathAes);
                files.add(pathCsv);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_REGULAR);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(CONSTANT_UTIL.SEMICOLON, -1);
                    try {
                        PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                        if (data[0].equals("TOTAL")) {
                            if (paymentDetailReports.size() == 0) {
                                PaymentDetailReport paymentDetailReportDet = new PaymentDetailReport();
                                paymentDetailReport.setRef(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setTextContractNumber(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setTransactionDate(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setAmtPayment(CONSTANT_UTIL.MINUS);
                                paymentDetailReportDet.setSize("0");
                                paymentDetailReportDet.setTotalAmount(data[2]);
                                paymentDetailReports.add(paymentDetailReportDet);
                            } else {
                                PaymentDetailReport paymentDetailReportDet = paymentDetailReports
                                        .get(paymentDetailReports.size() - 1);
                                paymentDetailReportDet
                                        .setSize(String.valueOf(textLines.length - 2));
                                paymentDetailReportDet.setTotalAmount(data[2]);
                                paymentDetailReports.set(paymentDetailReports.size() - 1,
                                        paymentDetailReportDet);
                            }

                        } else {
                            paymentDetailReport.setRef(data[0]);
                            paymentDetailReport.setTextContractNumber(data[1]);
                            paymentDetailReport.setTransactionDate(data[2]);
                            paymentDetailReport.setAmtPayment(data[3]);
                            paymentDetailReports.add(paymentDetailReport);
                        }
                    } catch (Exception e) {
                        LOGGER.debug("Error exportJFSPaymentRegular " + data[i] + " with error "
                                + e);
                        throw e;
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setTotalDebtor(String.valueOf(textLines.length - 2));
                BigDecimal total = BigDecimal.ZERO;
                if (paymentDetailReports.size() > 0) {
                    total = new BigDecimal(String.valueOf(paymentDetailReports.get(
                            paymentDetailReports.size() - 1).getTotalAmount()));
                }
                paymentReport.setTotalInstallment(total.toString());
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);
                LOGGER.debug("Size " + paymentDetailReports.size());

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                LOGGER.debug("Export PDF");
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);
                LOGGER.debug("End Export PDF");

                // Create Excel File
                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet spreadsheet = workbook.createSheet("Detail");
                // Create row object
                XSSFRow row;
                Map<String, Object[]> data = new TreeMap<String, Object[]>();
                int counter = 1;
                data.put(
                        String.valueOf(counter),
                        applicationProperties.getProperties("file.generate.header.excel",
                                CONSTANT_UTIL.JFS_PROPERTIES).split(
                                Pattern.quote(CONSTANT_UTIL.DEFAULT_COMMA)));

                LOGGER.debug("Export Xls");
                for (PaymentDetailReport paymentDetailReport : paymentDetailReports) {
                    counter++;
                    List<String> item = new ArrayList<String>();
                    item.add(paymentDetailReport.getRef());
                    item.add(paymentDetailReport.getTextContractNumber());
                    item.add(paymentDetailReport.getTransactionDate());
                    item.add(paymentDetailReport.getAmtPayment());
                    data.put(String.valueOf(counter), item.toArray());
                }

                // Iterate over data and write to sheet
                int keyid = data.size();
                int rowid = 0;

                // Border Style
                // Font Bold
                XSSFFont fontBold = workbook.createFont();
                fontBold.setFontHeightInPoints((short) 10);
                fontBold.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);

                XSSFCellStyle styleBorderBold = workbook.createCellStyle();
                styleBorderBold.setBorderTop((short) 1);
                styleBorderBold.setBorderBottom((short) 1);
                styleBorderBold.setBorderLeft((short) 1);
                styleBorderBold.setBorderRight((short) 1);
                styleBorderBold.setFont(fontBold);

                // Font Normal
                XSSFFont fontNormal = workbook.createFont();
                fontNormal.setFontHeightInPoints((short) 10);
                fontNormal.setBoldweight(XSSFFont.COLOR_NORMAL);

                XSSFCellStyle styleBorder = workbook.createCellStyle();
                styleBorder.setBorderTop((short) 1);
                styleBorder.setBorderBottom((short) 1);
                styleBorder.setBorderLeft((short) 1);
                styleBorder.setBorderRight((short) 1);
                styleBorder.setFont(fontNormal);

                for (int j = 1; j <= keyid; j++) {
                    row = spreadsheet.createRow(rowid++);
                    Object[] objectArr = data.get(String.valueOf(j));
                    int cellid = 0;
                    for (Object obj : objectArr) {
                        Cell cell = row.createCell(cellid);
                        if (j == 1) {
                            styleBorderBold.setAlignment(CellStyle.ALIGN_CENTER);
                            cell.setCellValue((String) obj);
                            cell.setCellStyle(styleBorderBold);
                        } else {
                            styleBorder.setFont(fontNormal);
                            if (cellid == 1 || cellid == 3) {
                                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                                cell.setCellValue(new Long((String) (obj == null ? "0" : obj)));
                            } else {
                                cell.setCellType(Cell.CELL_TYPE_STRING);
                                cell.setCellValue((String) obj);
                            }
                            cell.setCellStyle(styleBorder);
                        }
                        cellid++;
                    }
                }

                row = spreadsheet.createRow(rowid);
                // Merges the cells
                CellRangeAddress cellRangeAddress = new CellRangeAddress(keyid, keyid, 0, 2);
                spreadsheet.addMergedRegion(cellRangeAddress);
                // Creates the cell
                Cell cell = CellUtil.createCell(row, 0, "Total");
                cell.setCellStyle(styleBorderBold);

                RegionUtil.setBorderBottom(cell.getCellStyle().getBorderBottom(), cellRangeAddress,
                        spreadsheet, spreadsheet.getWorkbook());
                RegionUtil.setBorderTop(cell.getCellStyle().getBorderTop(), cellRangeAddress,
                        spreadsheet, spreadsheet.getWorkbook());
                RegionUtil.setBorderLeft(cell.getCellStyle().getBorderLeft(), cellRangeAddress,
                        spreadsheet, spreadsheet.getWorkbook());
                RegionUtil.setBorderRight(cell.getCellStyle().getBorderRight(), cellRangeAddress,
                        spreadsheet, spreadsheet.getWorkbook());

                // Sets the allignment to the created cell
                CellUtil.setAlignment(cell, workbook, CellStyle.ALIGN_CENTER);

                cell = row.createCell(3);
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                if (paymentDetailReports.size() > 0) {
                    cell.setCellValue(new Long(paymentDetailReports.get(
                            paymentDetailReports.size() - 1).getTotalAmount()));
                } else {
                    cell.setCellValue(new Long(0));
                }
                cell.setCellStyle(styleBorderBold);

                FileOutputStream outputStream = new FileOutputStream(filenameXls);
                workbook.write(outputStream);
                LOGGER.debug("End Export Xls");

                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);
                param.put("filename", filenameXls);
                saveGenerateDetailLogExportJFSPayment(param);

                // Done create Excel
                String content = "Export JSF Payment - Payment Regular Testing";
                StringWriter outputWriter = new StringWriter();
                outputWriter.write(content);

                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Notification - Generate JFS Payment Regular", htmlMessage, files);
                    LOGGER.debug("End Email");
                } catch (Exception e) {
                    LOGGER.debug("Send email failed Notification - Generate JFS Payment Regular");
                }
                LOGGER.debug("End exportJFSPaymentRegular");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                            + e + "\nThank You");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY, "Notification - Generate JFS Payment Regular",
                        htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
                LOGGER.debug("Send email failed Notification - Generate JFS Payment Regular");
            }
            throw e;
        }
    }

    @Override
    public void exportJFSPaymentReversal(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager
                    .getlistsDirectory("/WS/Data/JFS/Export/_processed/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSPaymentReversal");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/_processed/");

                // Instantiate generate files
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.payment.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat(CONSTANT_REPORT.REVERSAL_PAYMENT_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_PDF);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                sftpChannel.cd("/WS/Data/JFS/Export/");
                String filenameAes = "JFS_PAYMENT_REVERSAL_"
                        + runDate.concat(CONSTANT_UTIL.TYPE_TXT_AES);

                // Write file
                fileUtil.writeToFile(sftpChannel.get(filenameAes), processPath.concat(filenameAes));

                // Populate file
                List<String> files = new ArrayList<String>();
                String pathAes = processPath.concat(filenameAes);
                files.add(pathAes);
                files.add(filename);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_REVERSAL);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_REVERSAL_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(
                            Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR), -1);
                    try {
                        PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                        if (data[0].contains("TOTAL") || data[0].contains("HCI")) {
                            if (paymentDetailReports.size() == 0) {
                                PaymentDetailReport paymentDetailReportDet = new PaymentDetailReport();
                                paymentDetailReport.setRef(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setTextContractNumber(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setTransactionDate(CONSTANT_UTIL.MINUS);
                                paymentDetailReport.setAmtPayment("()");
                                paymentDetailReportDet.setSize("0");
                                paymentDetailReportDet.setTotalAmount(data[2]);
                                paymentDetailReports.add(paymentDetailReportDet);
                            } else {
                                PaymentDetailReport paymentDetailReportDet = paymentDetailReports
                                        .get(paymentDetailReports.size() - 1);
                                paymentDetailReportDet.setSize(String.valueOf(paymentDetailReports
                                        .size()));
                                paymentDetailReportDet.setTotalAmount("("
                                        + data[2].replaceAll(CONSTANT_UTIL.MINUS,
                                                CONSTANT_UTIL.DEFAULT_EMPTY) + ")");
                                paymentDetailReports.set(paymentDetailReports.size() - 1,
                                        paymentDetailReportDet);
                            }
                        } else {
                            paymentDetailReport.setRef(CONSTANT_UTIL.MINUS);
                            paymentDetailReport.setTextContractNumber(data[0]);
                            paymentDetailReport.setTransactionDate(data[2]);
                            paymentDetailReport.setAmtPayment("("
                                    + data[3].replaceAll(CONSTANT_UTIL.MINUS,
                                            CONSTANT_UTIL.DEFAULT_EMPTY) + ")");
                            paymentDetailReports.add(paymentDetailReport);
                        }
                    } catch (Exception e) {
                        LOGGER.debug("Error exportJFSPaymentReversal " + data[i] + " with error "
                                + e);
                        throw e;
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);

                // String content = "Export JSF Payment - Payment Regular Testing";
                // StringWriter outputWriter = new StringWriter();
                // outputWriter.write(content);
                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSPaymentReversal to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Notification - Generate JFS Payment Reversal", htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Send email failed exportJFSPaymentReversal");
                }
                LOGGER.debug("End exportJFSPaymentReversal");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                            + e + "\nThank You");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Generate JFS Payment Reversal", htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
                LOGGER.debug("Send email failed Notification - Generate JFS Payment Regular");
            }
            throw e;
        }
    }

    @Override
    public void exportJFSPaymentSmallUnderpayment(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = BatchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory("/WS/Data/JFS/Export/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }
                LOGGER.debug("Start exportJFSPaymentSmallUnderpayment");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/");

                // Instantiate generate files
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.payment.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat(CONSTANT_REPORT.SMALL_UNDER_PAYMENT_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_PDF);
                String filenameTxt = entry.getFilename().replaceAll("DETAIL", "DETAIL1")
                        .replaceAll("csv", "txt");
                String filenameAes = "JFS_SMALL_UNDERPAYMENT_"
                        + runDate.concat(CONSTANT_UTIL.TYPE_TXT_AES);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write to files
                fileUtil.writeToFile(sftpChannel.get(filenameTxt), processPath.concat(filenameTxt));
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));
                fileUtil.writeToFile(sftpChannel.get(filenameAes), processPath.concat(filenameAes));

                // Populate files
                List<String> files = new ArrayList<String>();
                String pathTxt = processPath.concat(filenameTxt);
                String pathAes = processPath.concat(filenameAes);
                String pathCsv = processPath.concat(entry.getFilename());
                files.add(pathTxt);
                files.add(pathAes);
                files.add(pathCsv);
                files.add(filename);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_SMALL_UNDERPAYMENT);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.PAYMENT_SMALL_UNDERPAYMENT_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();
                BigDecimal total = BigDecimal.ZERO;

                // Populate report data
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i < textLines.length; i++) {
                    String data[] = textLines[i].split(CONSTANT_UTIL.SEMICOLON, -1);
                    try {
                        if (!data[0].equals("TOTAL")) {
                            PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                            paymentDetailReport.setRef(data[0]);
                            paymentDetailReport.setTextContractNumber(data[1]);
                            paymentDetailReport.setTransactionDate(data[2]);
                            paymentDetailReport.setAmtPayment(data[3]);
                            BigDecimal amount = new BigDecimal(paymentDetailReport.getAmtPayment());
                            total = total.add(amount);
                            paymentDetailReports.add(paymentDetailReport);
                        }
                    } catch (Exception e) {
                        LOGGER.debug("Error exportJFSSmallUnderPayment " + data[i] + " with error "
                                + e);
                        throw e;
                    }
                }

                if (paymentDetailReports.size() == 0) {
                    PaymentDetailReport detailReport = new PaymentDetailReport();
                    detailReport.setRef(CONSTANT_UTIL.MINUS);
                    detailReport.setTextContractNumber(CONSTANT_UTIL.MINUS);
                    detailReport.setTransactionDate(CONSTANT_UTIL.MINUS);
                    detailReport.setAmtPayment(CONSTANT_UTIL.MINUS);
                    detailReport.setSize("0");
                    detailReport.setTotalAmount("0");
                    paymentDetailReports.add(detailReport);
                } else {
                    PaymentDetailReport detailReport = paymentDetailReports
                            .get(paymentDetailReports.size() - 1);
                    detailReport.setTotalAmount(String.valueOf(paymentDetailReports.size()));
                    paymentDetailReports.set(paymentDetailReports.size() - 1, detailReport);
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export report to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // Save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);

                // String content = "Export JSF Payment - Payment Regular Testing";
                // StringWriter outputWriter = new StringWriter();
                // outputWriter.write(content);
                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSPaymentSmallUnderpayment to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                            userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                            CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Notification - Generate JFS Payment Small Under Payment", htmlMessage,
                            files);
                } catch (Exception e) {
                    LOGGER.debug("Send email failed exportJFSPaymentSmallUnderpayment");
                }
                LOGGER.debug("End exportJFSPaymentSmallUnderpayment");

            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();

            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                            + e + "\nThank You");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Generate JFS Payment Small Under Payment", htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
            }
        }
    }

    private String[] getStringDataFile(Media file) {
        return file.getFormat() != null ? file.getStringData().split(System.lineSeparator())
                : new String(file.getByteData()).split(System.lineSeparator());
    }

    private void init() {
        if (mapContractNotFound != null) {
            mapContractNotFound.clear();
        } else {
            mapContractNotFound = new HashMap<String, Object>();
        }
    }

    @Override
    public void mergeJFSStatus(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 2 Start " + param.get("processCode"));
        int detailLength = (int) param.get("detailLength");
        String[] data = (String[]) param.get("data");
        String dateBankProcess = data[6].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR))[1];
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        Media file = (Media) param.get("media");
        Date currentDate = new Date();

        List<ContractStatus> listAdd = new ArrayList<ContractStatus>();
        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailLength; i < data.length; i++) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[i])) {
                String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String contractNumber = item[1];
                String transactionDate = item[4];
                String status = item[9];
                if ("S".equals(status) || "W".equals(status) || "R".equals(status)) {
                    Map<String, Object> param1 = new HashMap<String, Object>();
                    param1.put("contractNumber", contractNumber);
                    param1.put("transactionDate", transactionDate);

                    List<PaymentInt> listPaymentInt = PaymentIntDAO
                            .getPaymentContractByContractWithStatusAndDatePayment(param1);
                    if (listPaymentInt != null) {
                        if (listPaymentInt.size() > 0) {
                            List<ContractStatus> listContract = PaymentDAO
                                    .getJFSAllContractStatusByContractNumberByStatus(
                                            contractNumber, "K");
                            if (listContract == null || listContract.isEmpty()) {
                                if (mapContractNotFound.get(contractNumber) == null) {
                                    ContractStatus contractStatus = new ContractStatus();
                                    Contract contract = new Contract();
                                    contract.setTextContractNumber(contractNumber);

                                    contractStatus.setTextContractNumber(contract);
                                    contractStatus.setDtimeCreated(dateUtil
                                            .getFormattedCurrentDatePattern10(dateBankProcess));
                                    contractStatus.setCntDPD(0);
                                    contractStatus.setCodeStatus("K");
                                    contractStatus.setDtimeBankProcess(dateUtil
                                            .getFormattedCurrentDatePattern10(dateBankProcess));
                                    contractStatus.setCreatedBy(userCredential.getUsername());
                                    contractStatus.setFilename(file.getName());
                                    contractStatus.setDateUpload(currentDate);
                                    listAdd.add(contractStatus);
                                }
                            } else {
                                for (ContractStatus contractStatus : listContract) {
                                    contractStatus.setDtimeBankProcess(dateUtil
                                            .getFormattedCurrentDatePattern10(dateBankProcess));
                                    contractStatus.setDtimeUpdated(dateUtil
                                            .getFormattedCurrentDatePattern10(dateBankProcess));
                                    contractStatus.setUpdatedBy(userCredential.getUsername());
                                    contractStatus.setFilename(file.getName());
                                    contractStatus.setDateUpload(currentDate);
                                    listAdd.add(contractStatus);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (listAdd.size() > 0) {
            LOGGER.debug("Start Create Or Update Contract Status Data " + param.get("processCode"));
            PaymentDAO.saveOrUpdateByBatch(listAdd);
            LOGGER.debug("End Create Or Update Data " + param.get("processCode"));
        }
        LOGGER.debug("Step 2 End " + param.get("processCode"));
    }

    @Override
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        LOGGER.debug("Start saveGenerateDetailLogExportJFSPayment " + param.get("processCode"));
        String filename = (String) param.get("filename");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        BatchJobLog log = (BatchJobLog) param.get("log");
        String letterNumber = (String) param.get("letterNumber");

        String name = filename.split(Pattern.quote("\\"))[filename.split(Pattern.quote("\\")).length - 1];
        GenerateFile generateFile = new GenerateFile();
        generateFile.setAgreement(log.getAgreement());
        generateFile.setPartner(log.getPartner());
        generateFile.setProcessType(log.getProcess());
        generateFile.setFileName(filename);
        generateFile.setCreatedDate(new Date());
        generateFile.setProceedDate((Date) param.get("proceedDate"));
        generateFile.setLetterNumber(letterNumber);
        generateFile.setIsCreated(CONSTANT_UTIL.DEFAULT_YES);
        generateFile.setRunIdBatchJobLog(log.getRunId());
        generateFile.setName(name);
        generateFile.setCreatedBy(userCredential.getUsername());
        this.BatchDAO.save(generateFile);
        LOGGER.debug("End saveGenerateDetailLogExportJFSPayment " + param.get("processCode"));

    }

    @Override
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        LOGGER.debug("Start Upload JSP Payment File to Local Temporary " + param.get("processCode"));
        Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                "file.path.upload.payment.list", CONSTANT_UTIL.JFS_PROPERTIES,
                CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);

        UserCredential userCredential = (UserCredential) param.get("userCredential");

        String content = null;
        UploadFile uploadFile = new UploadFile();

        ProcessType processType = new ProcessType();
        processType.setCode((String) param.get("processCode"));
        uploadFile.setProcessType(processType);
        uploadFile.setCreatedDate(new Date());
        uploadFile.setSizeFile((String) param.get("size"));
        uploadFile.setChecksum((String) param.get("checksum"));

        if (param.get("isRejected") != null) {
            if (param.get("isRejected").equals(CONSTANT_UTIL.DEFAULT_YES)) {
                uploadFile.setFilepath(((String) param.get("path")).concat((String) param
                        .get("filename")));
                uploadFile.setName((String) param.get("filename"));
                content = (String) param.get("content");
            }
        } else {
            Media file = (Media) param.get("media");
            uploadFile.setName(file.getName());
            uploadFile.setFilepath(mapPath.get(processType.getCode()).concat(file.getName()));

            if (file.getFormat() != null) {
                content = file.getStringData();
            } else {
                content = new String(file.getByteData());
            }
        }

        Agreement agreement = new Agreement();
        agreement.setCode((String) param.get("agreementCode"));
        uploadFile.setAgreement(agreement);

        Partner partner = new Partner();
        partner.setId((String) param.get("partnerCode"));
        uploadFile.setPartner(partner);
        uploadFile.setCreatedBy(userCredential.getUsername());
        uploadFile.setFileType((String) param.get("fileType"));
        BatchDAO.save(uploadFile);

        fileUtil.writeToFile(content, uploadFile.getFilepath());
        LOGGER.debug("End Upload JSP Payment File to Local Temporary " + param.get("processCode"));
    }

    public void setApplicationExceptionService(
            ApplicationExceptionService applicationExceptionService) {
        this.applicationExceptionService = applicationExceptionService;
    }

    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setBatchDAO(BatchDAOImpl batchDAO) {
        BatchDAO = batchDAO;
    }

    public void setContractDAO(ContractDAOImpl contractDAO) {
        this.contractDAO = contractDAO;
    }

    public void setDateUtil(DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    public void setEmailEngine(EmailEngine emailEngine) {
        this.emailEngine = emailEngine;
    }

    public void setfileUtil(FileUtil fileUtil) {
        this.fileUtil = fileUtil;
    }

    public void setPaymentDAO(PaymentDAOImpl paymentDAO) {
        PaymentDAO = paymentDAO;
    }

    public void setPaymentIntDAO(PaymentIntDAOImpl paymentIntDAO) {
        PaymentIntDAO = paymentIntDAO;
    }

    public void setSshManager(SSHManager sshManager) {
        this.sshManager = sshManager;
    }

    public void setSystemParameterService(SystemParameterServiceImpl systemParameterService) {
        this.systemParameterService = systemParameterService;
    }

    @Override
    public void uploadJFSPaymentAllocationFile(Map<String, Object> param) throws Exception {
        try {
            LOGGER.debug("Start File Allocation " + param.get("processCode"));
            init();
            validationContractPaymentAllocation(param);
            uploadJFSPaymentAllocationStep1(param);
            saveUploadDetailLogExportJFSPayment(param);

            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage.append("The process is already done.<br>");
            LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
            try {
                if (mapContractNotFound.size() > 0) {
                    htmlMessage.append("Detail Information Contract is not found<br>");
                    int i = 1;
                    for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + entry.getValue() + "<br>");
                        i++;
                    }
                    htmlMessage.append("Please contact our support team.<br>");
                }
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("ist@homecredit.co.id", userLogin.getTextEmailAddress(),
                        CONSTANT_UTIL.DEFAULT_EMPTY, CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Upload JFS Payment Allocation ".concat((String) param
                                .get("processTypeName")), htmlMessage);
            } catch (Exception e) {
                LOGGER.debug("Error send email to " + userLogin.getTextEmailAddress()
                        + " with message " + e.getMessage());
            }
            LOGGER.debug("End File Allocation " + param.get("processCode"));
        } catch (Exception e) {
            try {
                Employee userLogin = (Employee) param.get("userLogin");
                StringWriter htmlMessage = new StringWriter();
                htmlMessage.append("The process is unsucessfull<br>");
                if (e instanceof ApplicationException) {
                    ApplicationException ex = (ApplicationException) e;
                    String error = applicationExceptionService.getApplicationExceptionByErrorCode(
                            ex.getErrorCode()).getErrorMessage();
                    htmlMessage.append("For detail information error : <i>" + error + "</i><br>");
                    if (mapContractNotFound.size() > 0) {
                        int i = 1;
                        for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                            htmlMessage.append(i
                                    + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                    + entry.getValue() + "<br>");
                            i++;
                        }
                    }
                } else {
                    htmlMessage.append("For detail information error : \n" + e + "<br>");
                }
                htmlMessage.append("Please contact our support team<br>");
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY, "Notification - JFS Payment Allocation "
                                .concat((String) param.get("processTypeName")), htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed");
            }
            throw e;
        }
    }

    private void uploadJFSPaymentAllocationStep1(Map<String, Object> param) throws Exception {
        UserCredential userLogin = (UserCredential) param.get("userCredential");
        LOGGER.debug("Step 1 Start " + param.get("processCode"));
        Media file = (Media) param.get("media");
        String[] data = getStringDataFile((Media) param.get("media"));
        Date dateUpload = new Date();
        int detailSize = Integer.parseInt(applicationProperties.getProperties(
                "jfs.payment.allocation.file.detail.size", CONSTANT_UTIL.JFS_PROPERTIES));

        String datePrinted = data[6].substring(18, 28);
        Date datePrint = dateUtil.getFormattedCurrentDatePattern15(datePrinted.trim());
        List<PaymentAllocation> listInsert = new ArrayList<PaymentAllocation>();
        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailSize - 1; i < data.length; i++) {
            if (data[i] != null) {
                if (data[i].contains("HCI") && !data[i].contains("HCIJF01")) {
                    if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                        LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                                + param.get("processCode"));
                    }
                    String contractNumber = data[i].substring(12, 22).trim();
                    if (mapContractNotFound.get(contractNumber) == null) {
                        try {
                            String partIndex = data[i].substring(71, 73).trim();
                            String dateDue = data[i].substring(76, 86).trim();
                            String amtBankPrincipal = data[i].substring(87, 105).trim();
                            amtBankPrincipal = (amtBankPrincipal == null || amtBankPrincipal
                                    .equals(CONSTANT_UTIL.DEFAULT_EMPTY)) ? "0" : amtBankPrincipal;
                            String amtBankInterest = data[i].substring(105, 124).trim();
                            amtBankInterest = (amtBankInterest == null || amtBankInterest
                                    .equals(CONSTANT_UTIL.DEFAULT_EMPTY)) ? "0" : amtBankInterest;
                            String paymentType = data[i].substring(185, data[i].length()).trim();

                            PaymentAllocation item = new PaymentAllocation();

                            Contract contract = new Contract();
                            contract.setTextContractNumber(contractNumber);
                            item.setContract(contract);

                            item.setAmtInterest(new BigDecimal(amtBankInterest.replace(
                                    CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_EMPTY)));
                            item.setAmtPrincipal(new BigDecimal(amtBankPrincipal.replace(
                                    CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_EMPTY)));
                            item.setDueDate(dateUtil.getFormattedCurrentDatePattern10(dateDue));
                            item.setPartIndex(Integer.parseInt(partIndex));
                            item.setPaymentType(paymentType);
                            item.setDtimeCreated(datePrint);
                            item.setDateUpload(dateUpload);
                            item.setCreatedBy(userLogin.getUsername());
                            item.setFilename(file.getName());

                            String processCode = (String) param.get("processCode");
                            if (processCode.equals("P_REVERSAL")) {
                                item.setPaymentType("REVERSAL");
                            } else {
                                item.setPaymentType(paymentType);
                            }

                            item.setArchieved(0);
                            listInsert.add(item);
                        } catch (Exception e) {
                            LOGGER.debug("Error uploadJFSPaymentAllocationStep1 "
                                    + param.get("processCode") + " With Error " + e);
                            throw e;
                        }
                    }
                }
            }
        }

        if (listInsert.size() > 0) {
            LOGGER.debug("Create Payment Allocation Data " + param.get("processCode"));
            PaymentIntDAO.saveByBatch(listInsert);
            LOGGER.debug("End Create Data " + param.get("processCode"));
        }
        LOGGER.debug("Step 1 End " + param.get("processCode"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void uploadJFSPaymentFile(Map<String, Object> param) throws Exception {
        try {
            // Initial Variable
            init();
            Media file = (Media) param.get("media");
            SystemParameter parameter = systemParameterService
                    .getSystemParameterById("VALIDATION_JFS_PAYMENT_UPLOAD");
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(parameter.getValue());
            Map<String, String> parser = (JSONObject) obj;
            Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                    "file.path.upload.payment.list", CONSTANT_UTIL.JFS_PROPERTIES,
                    CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);

            param.put("headerLength", Integer.parseInt(parser.get("lengthHeader")));
            param.put("detailLength", Integer.parseInt(parser.get("detailHeader")));
            String[] data = getStringDataFile((Media) param.get("media"));
            StringBuilder content = new StringBuilder();
            if (file.getFormat() != null) {
                content.append(file.getStringData());
            } else {
                content.append(new String(file.getByteData()));
            }
            param.put("data", data);
            param.put("parser", parser);

            LOGGER.debug("Start Process Confirmation File " + param.get("processCode"));
            // Step 1 Confirmation
            confirmationJFSPaymentFile(param);
            // Step 2 Update Contract Status
            mergeJFSStatus(param);

            List<String> listRejected = checkingRejectedJFSPaymentFile(param);
            String filenameCsv = "JFS_REJECTED_".concat((String) param.get("processTypeName"))
                    .concat("_").concat((String) param.get("createdDate")).toUpperCase()
                    .concat(".csv");
            String pathfilenameCsv = mapPath.get(param.get("processCode")).concat(filenameCsv);

            if (listRejected != null) {
                if (listRejected.size() > 0) {
                    Map<String, Object> paramReject = new HashMap<String, Object>();
                    LOGGER.debug("Create Reporting Rejected");
                    StringBuilder contentCsv = new StringBuilder();
                    contentCsv
                            .append("CONTRACT_NUMBER;CUSTOMER_NAME;PAYMENT_TYPE;TRANSACTION_DATE;AMT_PAYMENT;STATUS;DESCRIPRION;"
                                    .concat(System.lineSeparator()));
                    for (String item : listRejected) {
                        contentCsv.append(item.concat(System.lineSeparator()));
                    }

                    paramReject.put("isRejected", CONSTANT_UTIL.DEFAULT_YES);
                    paramReject.put("filename", filenameCsv);
                    paramReject.put("content", contentCsv.toString());
                    paramReject.put("path", mapPath.get(param.get("processCode")));
                    paramReject.put("processCode", param.get("processCode"));

                    MessageDigest md = MessageDigest.getInstance("MD5");
                    md.update(contentCsv.toString().getBytes());

                    byte byteData[] = md.digest();
                    // convert the byte to hex format method 1
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < byteData.length; i++) {
                        sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
                    }
                    paramReject.put("checksum", sb.toString());
                    paramReject
                            .put("size", String.valueOf(contentCsv.toString().getBytes().length));
                    paramReject.put("userCredential", param.get("userCredential"));
                    paramReject.put("agreementCode", param.get("agreementCode"));
                    paramReject.put("partnerCode", param.get("partnerCode"));
                    paramReject.put("fileType", param.get("fileType"));
                    saveUploadDetailLogExportJFSPayment(paramReject);
                    LOGGER.debug("End Create Reporting Rejected");
                }
            }
            saveUploadDetailLogExportJFSPayment(param);
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage.append("The process is already done.</br>");
            LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
            try {
                if (mapContractNotFound.size() > 0) {
                    int i = 0;
                    htmlMessage.append("Detail Information Contract is not found<br>");
                    for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                        htmlMessage.append(i
                                + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                + entry.getValue() + "<br>");
                        i++;
                    }
                    htmlMessage.append("Please contact our support team.<br>");
                }
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY, "Notification - JFS Payment Confirmation "
                                .concat((String) param.get("processTypeName")), htmlMessage,
                        pathfilenameCsv);
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.debug("Error send email to " + userLogin.getTextEmailAddress()
                        + " with message " + e.getMessage());
            }
        } catch (Exception e) {
            try {
                Employee userLogin = (Employee) param.get("userLogin");
                StringWriter htmlMessage = new StringWriter();
                htmlMessage.append("The process is unsucessfull<br>");
                if (e instanceof ApplicationException) {
                    ApplicationException ex = (ApplicationException) e;
                    String error = applicationExceptionService.getApplicationExceptionByErrorCode(
                            ex.getErrorCode()).getErrorMessage();
                    htmlMessage.append("For detail information error : <i>" + error + "</i><br>");
                    if (mapContractNotFound.size() > 0) {
                        int i = 0;
                        for (Map.Entry<String, Object> entry : mapContractNotFound.entrySet()) {
                            htmlMessage.append(i
                                    + CONSTANT_UTIL.DEFAULT_DOT.concat(CONSTANT_UTIL.DEFAULT_DASH)
                                    + entry.getValue() + "<br>");
                            i++;
                        }
                    }
                } else {
                    htmlMessage.append("For detail information error : \n" + e + "<br>");
                }
                htmlMessage.append("Please contact our support team<br>");
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY, "Notification - JFS Payment Confirmation "
                                .concat((String) param.get("processTypeName")), htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed");
            }
            throw e;
        }
    }

    @Override
    public void validationContractPaymentAllocation(Map<String, Object> param) throws Exception {
        LOGGER.debug("Validation Contract Payment Allocation " + param.get("processCode"));
        // Get Type Agreement
        SystemParameter sysParam = systemParameterService
                .getSystemParameterById("JFS_AGREEMENT_REGULAR");
        if (sysParam == null) {
            throw new ApplicationException(
                    ENUM_ERROR.ERR_CONFIGURE_AGREEMENT_TYPE_PROCESS.toString());
        }
        String type = sysParam.getValue();
        // Done
        String[] data = getStringDataFile((Media) param.get("media"));
        int detailSize = Integer.parseInt(applicationProperties.getProperties(
                "jfs.payment.allocation.file.detail.size", CONSTANT_UTIL.JFS_PROPERTIES));

        LOGGER.debug("Fetching Data " + param.get("processCode"));
        for (int i = detailSize - 1; i < data.length; i++) {
            if (data[i] != null) {
                if (data[i].contains("HCI") && !data[i].contains("HCIJF01")) {
                    if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                        LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                                + param.get("processCode"));
                    }
                    String contractNumber = data[i].substring(12, 22).trim();
                    Contract contract = contractDAO
                            .getContractByAgreementType(contractNumber, type);
                    if (contract == null) {
                        mapContractNotFound.put(contractNumber, data[i]);
                    }
                }
            }
        }
        LOGGER.debug("Result Size " + mapContractNotFound.size());
        LOGGER.debug("End Validation Contract Payment Allocation " + param.get("processCode"));

    }

    @Override
    public void validationContractPaymentConfirmation(Map<String, Object> param) throws Exception {
        // Get Type Agreement
        SystemParameter sysParam = systemParameterService
                .getSystemParameterById("JFS_AGREEMENT_REGULAR");
        if (sysParam == null) {
            throw new ApplicationException(
                    ENUM_ERROR.ERR_CONFIGURE_AGREEMENT_TYPE_PROCESS.toString());
        }
        String type = sysParam.getValue();
        // Done
        String[] data = getStringDataFile((Media) param.get("media"));
        int detailLength = (int) param.get("detailLength");

        LOGGER.debug("Validation Data");
        for (int i = detailLength; i < data.length; i++) {
            if (!CONSTANT_UTIL.DEFAULT_EMPTY.equals(data[i])) {
                String item[] = data[i].split(Pattern.quote(CONSTANT_UTIL.DEFAULT_SEPARATOR));
                String contractNumber = item[1];
                Contract contract = contractDAO.getContractByAgreementType(contractNumber, type);
                if (contract == null) {
                    mapContractNotFound.put(contractNumber, data[i]);
                }
            }
        }
        LOGGER.debug("Result Size " + mapContractNotFound.size());
        LOGGER.debug("End Validation Data");
    }
}
