package com.hci.jfs.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hci.jfs.dao.ProductDAO;
import com.hci.jfs.entity.OtherCriteriaSetting;
import com.hci.jfs.entity.Product;
import com.hci.jfs.entity.ProductHCI;
import com.hci.jfs.entity.ProductMapping;
import com.hci.jfs.entity.PublicHoliday;
import com.hci.jfs.entity.form.ProductMappingForm;
import com.hci.jfs.services.ProductService;

public class ProductServiceImpl implements ProductService {

	private ProductDAO productDAO;
	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}

	@Override
	public List<Product> getAllProduct() throws Exception {
		return productDAO.getAllProduct();
	}

	@Override
	public List<ProductHCI> getAllProductHCI() throws Exception {
		return productDAO.getAllProductHCI();
	}

	@Override
	public List<ProductMapping> getAllProductMapping() throws Exception {
		return productDAO.getAllProductMapping();
	}

	@Override
	public List<Product> getProductByAgreement(Long agreement) throws Exception {
		return productDAO.getProductByAgreement(agreement);
	}

	@Override
	public List<ProductMapping> getProductMappingByAgreementCode(
			Long agreementCode) throws Exception {
		return productDAO.getProductMappingByAgreementCode(agreementCode);
	}

	@Override
	public List<ProductMappingForm> getProductMappingFormByAgreementCode(
			Long agreementCode) throws Exception {
		List<ProductMappingForm> listMappingForms=null;
		List<ProductMapping> listProductMapping = getProductMappingByAgreementCode(agreementCode);
		if(listProductMapping!=null){
			if(listProductMapping.size()>0){
				List<ProductHCI> listProductHCI = getProductHCIByAgreementCode(agreementCode);
				Map<String,ProductHCI> mapProductHCI = new HashMap<String, ProductHCI>();
				for (ProductHCI productHCI : listProductHCI) {
					mapProductHCI.put(productHCI.getCodeProduct(), productHCI);
				}
				
				List<Product> listProduct = getProductByAgreement(agreementCode);
				Map<String,Product> mapProduct = new HashMap<String, Product>();
				for (Product product : listProduct) {
					mapProduct.put(product.getBankProductCode(), product);
				}
				
				ProductMappingForm form=null;
				listMappingForms = new ArrayList<ProductMappingForm>();
				for (ProductMapping productMapping : listProductMapping) {
					form = new ProductMappingForm();
					form.setIdProduct(productMapping.getIdProduct());
					form.setCodeProduct(productMapping.getCodeProduct());
					form.setAgreement(productMapping.getAgreement());
					form.setValidFrom(productMapping.getValidFrom());
					form.setValidTo(productMapping.getValidTo());
					form.setIsDefault(productMapping.getIsDefault());
					form.setIsDelete(productMapping.getIsDelete());
					form.setBankProductCode(productMapping.getBankProductCode());
					form.setProductHCI(mapProductHCI.get(productMapping.getCodeProduct()));
					form.setProduct(mapProduct.get(productMapping.getBankProductCode()));
					listMappingForms.add(form);
				}
			}
		}
		return listMappingForms;
	}

	@Override
	public List<ProductHCI> getProductHCIByAgreementCode(Long agreementCode)
			throws Exception {
		return productDAO.getProductHCIByAgreementCode(agreementCode);
	}

	@Override
	public ProductHCI getProductHCIByCodeProductByAgreementCode(
			String codeProduct, Long agreementCode) throws Exception {
		return productDAO.getProductHCIByCodeProductByAgreementCode(codeProduct, agreementCode);
	}

	@Override
	public Product getProductByBankProductCodeByAgreementCode(
			String bankProductCode, Long agreementCode) throws Exception {
		return productDAO.getProductByBankProductCodeByAgreementCode(bankProductCode, agreementCode);
	}

	@Override
	public List<PublicHoliday> getCurrentPublicHoliday() throws Exception {
		return productDAO.getCurrentPublicHoliday();
	}

	@Override
	public ProductHCI getProductHCIByIdProduct(String id) throws Exception {
		// TODO Auto-generated method stub
		return productDAO.getProductHCIByIdProduct(id);
	}

	@Override
	public ProductMapping getProductMappingByIdProduct(String id)
			throws Exception {
		// TODO Auto-generated method stub
		return productDAO.getProductMappingByIdProduct(id);
	}

}
