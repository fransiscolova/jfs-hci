package com.hci.jfs.services.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.jfs.dao.BaseDAO;
import com.hci.jfs.entity.SystemParameter;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.SystemParameterService;

public class SystemParameterServiceImpl extends BaseDAO implements SystemParameterService {

	@SuppressWarnings("unchecked")
	@Override
	public List<SystemParameter> getAllSystemParameters()
			throws ApplicationException {
		Criteria criteria =getCurrentSession().createCriteria(SystemParameter.class);
		return criteria.list();
	}

	@Override
	public SystemParameter getSystemParameterById(String idCode)
			throws ApplicationException {
		Criteria criteria = getCurrentSession().createCriteria(SystemParameter.class);
		criteria.add(Restrictions.eq("id", idCode));
		return (SystemParameter) criteria.uniqueResult();
	}

}
