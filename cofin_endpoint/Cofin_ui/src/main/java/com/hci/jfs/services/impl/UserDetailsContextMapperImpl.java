package com.hci.jfs.services.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

import com.hci.jfs.dao.impl.UserDAOImpl;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.util.SpringContextHolder;
/**
*
* @author Muhammad.Agaputra
*/
public class UserDetailsContextMapperImpl implements UserDetailsContextMapper,
		Serializable {
	private static final long serialVersionUID = 3962976258168853954L;

	@Override
	public UserDetails mapUserFromContext(DirContextOperations ctx,
			String username, Collection<? extends GrantedAuthority> authority) {
		
		UserDAOImpl userDAO = (UserDAOImpl) SpringContextHolder.getApplicationContext().getBean("UserDAO");
		
		// Give default ROLE for authenticated user
		List<GrantedAuthority> mappedAuthorities = new ArrayList<GrantedAuthority>();		
		mappedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		
		// Get additional ROLE from database
		List <String> roles = userDAO.getRolesByUserName(username);
		
		// Add to each additional ROLE into mappedAuthorities
		for(String role : roles) {
			mappedAuthorities.add(new SimpleGrantedAuthority(role));
		}

		return new UserCredential(username, "", true, true, true, true, mappedAuthorities);
	}
	
	@Override
	public void mapUserToContext(UserDetails arg0, DirContextAdapter arg1) {
	}
}
