package com.hci.jfs.services.impl;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.hci.jfs.dao.impl.UserManagementDAOImpl;
import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.Role;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.UserManagementService;

@WebService(serviceName="userManagementService")
@Controller
public class UserManagementServiceImpl extends SpringBeanAutowiringSupport implements UserManagementService {

	private UserManagementDAOImpl userManagementDAO;

	@WebMethod(exclude=true)
	@Autowired
	public void setUserManagementDAO(UserManagementDAOImpl userManagementDAO) {
		this.userManagementDAO = userManagementDAO;
	}
	
	@WebMethod(exclude=true)
	private void initLookupService(){
		if(userManagementDAO == null)
			processInjectionBasedOnCurrentContext(this);
	}

	@Override
	@WebMethod	
	public List<Employee> getAllEmployee() throws ApplicationException {
		return userManagementDAO.getAllEmployee();
	}

	@Override
	@WebMethod
	public List<Role> getRolesByUserName(String username)
			throws ApplicationException {
		return userManagementDAO.getRolesByUserName(username);
	}

	@Override
	@WebMethod
	public Employee getUserByUserName(String username)
			throws ApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@WebMethod
	public void deleteRolesByUserName(String username)
			throws ApplicationException {
		userManagementDAO.deleteRolesByUserName(username);		
	}

	@Override
	@WebMethod
	public void insertRolesByUsername(List<Role> listRoles)
			throws ApplicationException {
		userManagementDAO.insertRolesByUsername(listRoles);		
	}

}
