package com.hci.jfs.services.impl;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.util.media.Media;

import com.hci.jfs.constant.CONSTANT_REPORT;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.constant.ENUM_ERROR;
import com.hci.jfs.dao.impl.BatchDAOImpl;
import com.hci.jfs.dao.impl.PaymentDAOImpl;
import com.hci.jfs.entity.Agreement;
import com.hci.jfs.entity.BatchJobLog;
import com.hci.jfs.entity.Contract;
import com.hci.jfs.entity.ContractStatus;
import com.hci.jfs.entity.Employee;
import com.hci.jfs.entity.GenerateFile;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.entity.ProcessType;
import com.hci.jfs.entity.UploadFile;
import com.hci.jfs.entity.report.PaymentDetailReport;
import com.hci.jfs.entity.report.PaymentReport;
import com.hci.jfs.security.ApplicationException;
import com.hci.jfs.services.ApplicationExceptionService;
import com.hci.jfs.services.UserCredential;
import com.hci.jfs.services.WriteOffService;
import com.hci.jfs.util.ApplicationProperties;
import com.hci.jfs.util.DateUtil;
import com.hci.jfs.util.EmailEngine;
import com.hci.jfs.util.FileUtil;
import com.hci.jfs.util.SSHManager;
import com.jcraft.jsch.ChannelSftp;

@Transactional
public class WriteOffServiceImpl implements WriteOffService {
    final static Logger LOGGER = LogManager.getLogger(WriteOffServiceImpl.class.getName());

    private BatchDAOImpl batchDAO;
    private DateUtil dateUtil;

    private PaymentDAOImpl PaymentDAO;
    private ApplicationProperties applicationProperties;

    private EmailEngine emailEngine;
    private SSHManager sshManager;

    private FileUtil fileUtil;
    private ApplicationExceptionService applicationExceptionService;

    private Map<String, Object> mapContractNotFound;

    private void confirmationJFSStep1(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 1 Start");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        Media file = (Media) param.get("media");
        Date currentDate = new Date();
        String[] data = null;
        if (file.getFormat() != null) {
            data = file.getStringData().split(System.lineSeparator());
        } else {
            data = new String(file.getByteData()).split(System.lineSeparator());
        }

        int headerSize = Integer.parseInt(applicationProperties.getProperties(
                "write.off.header.size", CONSTANT_UTIL.JFS_PROPERTIES));
        List<ContractStatus> listUpdate = new ArrayList<ContractStatus>();
        for (int i = headerSize; i < data.length; i++) {
            String item = data[i];
            if (item.replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC, CONSTANT_UTIL.DEFAULT_EMPTY)
                    .trim()
                    .equals(applicationProperties
                            .getProperties("write.off.footer.1", CONSTANT_UTIL.JFS_PROPERTIES)
                            .replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC,
                                    CONSTANT_UTIL.DEFAULT_EMPTY).trim())) {
                break;
            }
            if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                        + param.get("processCode"));
            }
            String noPinjaman = item.substring(10, Integer.parseInt(applicationProperties
                    .getProperties("write.off.detail.size.no.pinjaman",
                            CONSTANT_UTIL.JFS_PROPERTIES)));
            String tglHapusBuku = item.substring(126, 126 + Integer.parseInt(applicationProperties
                    .getProperties("write.off.detail.size.tanggal.tgl.hapus.buku",
                            CONSTANT_UTIL.JFS_PROPERTIES)));

            String contractNo = noPinjaman.replace("HCI", CONSTANT_UTIL.DEFAULT_EMPTY).trim();
            ContractStatus contractStatus = PaymentDAO
                    .getJFSContractStatusByContractNumberByStatus(contractNo, "H");
            if (contractStatus != null) {
                contractStatus.setDtimeBankProcess(dateUtil
                        .getFormattedCurrentDatePattern16(tglHapusBuku.trim()));
                contractStatus.setUpdatedBy(userCredential.getUsername());
                contractStatus.setDtimeUpdated(dateUtil
                        .getFormattedCurrentDatePattern16(tglHapusBuku.trim()));
                contractStatus.setFilename(file.getName());
                contractStatus.setDateUpload(currentDate);
                listUpdate.add(contractStatus);
            }
        }

        if (listUpdate.size() > 0) {
            PaymentDAO.updateByBatch(listUpdate);
        }
        LOGGER.debug("Step 1 End");
    }

    private void confirmationJFSStep2(Map<String, Object> param) throws Exception {
        LOGGER.debug("Step 2 Start");
        Media file = (Media) param.get("media");
        String[] data = null;
        if (file.getFormat() != null) {
            data = file.getStringData().split(System.lineSeparator());
        } else {
            data = new String(file.getByteData()).split(System.lineSeparator());
        }

        int headerSize = Integer.parseInt(applicationProperties.getProperties(
                "write.off.header.size", CONSTANT_UTIL.JFS_PROPERTIES));
        List<Contract> listUpdate = new ArrayList<Contract>();
        for (int i = headerSize; i < data.length; i++) {
            String item = data[i];
            if (item.equals(applicationProperties.getProperties("write.off.footer.1",
                    CONSTANT_UTIL.JFS_PROPERTIES))
                    || item.contains(applicationProperties.getProperties("write.off.footer.1",
                            CONSTANT_UTIL.JFS_PROPERTIES))) {
                break;
            }
            if (i % CONSTANT_UTIL.MAX_ROW == 0) {
                LOGGER.debug("Row " + (i / CONSTANT_UTIL.MAX_ROW) + " - "
                        + param.get("processCode"));
            }
            String noPinjaman = item.substring(10, Integer.parseInt(applicationProperties
                    .getProperties("write.off.detail.size.no.pinjaman",
                            CONSTANT_UTIL.JFS_PROPERTIES)));
            String contractNo = noPinjaman.replace("HCI", CONSTANT_UTIL.DEFAULT_EMPTY).trim();

            Contract contract = PaymentDAO.getJFSContractByContractNumberAndStatus(contractNo, "A");
            if (contract != null) {
                contract.setStatus("H");
                listUpdate.add(contract);
            } else {
                mapContractNotFound.put(contractNo, data[i]);
            }
        }

        if (listUpdate.size() > 0) {
            PaymentDAO.updateByBatch(listUpdate);
        }
        LOGGER.debug("Step 2 End");
    }
    @Override
    public void convertJFSWriteOffFile(Map<String, Object> param) throws Exception {
        Media file = (Media) param.get("media");

        // Filename
        String[] data = null;
        if (file.getFormat() != null) {
            data = file.getStringData().split(System.lineSeparator());
        } else {
            data = new String(file.getByteData()).split(System.lineSeparator());
        }

        // header
        int size = Integer.parseInt(applicationProperties.getProperties("write.off.header.size",
                CONSTANT_UTIL.JFS_PROPERTIES));
        for (int i = 0; i < size; i++) {
            if (!data[i]
                    .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                    .trim()
                    .contains(
                            applicationProperties
                                    .getProperties("write.off.header." + (i + 1),
                                            CONSTANT_UTIL.JFS_PROPERTIES)
                                    .replace(CONSTANT_UTIL.DEFAULT_DASH,
                                            CONSTANT_UTIL.DEFAULT_EMPTY).trim())
                    && !data[i]
                            .replace(CONSTANT_UTIL.DEFAULT_DASH, CONSTANT_UTIL.DEFAULT_EMPTY)
                            .trim()
                            .equals(applicationProperties
                                    .getProperties("write.off.header." + (i + 1),
                                            CONSTANT_UTIL.JFS_PROPERTIES)
                                    .replace(CONSTANT_UTIL.DEFAULT_DASH,
                                            CONSTANT_UTIL.DEFAULT_EMPTY).trim())) {
                throw new ApplicationException("ERR-JFS-PAYMENT-WRONG-DETAIL");
            }
        }

        // footer
        size = Integer.parseInt(applicationProperties.getProperties("write.off.footer.size",
                CONSTANT_UTIL.JFS_PROPERTIES));
        int j = 0;
        for (int i = data.length - size; i < data.length; i++) {
            if (!data[i]
                    .replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC, CONSTANT_UTIL.DEFAULT_EMPTY)
                    .trim()
                    .contains(
                            applicationProperties
                                    .getProperties("write.off.footer." + (j + 1),
                                            CONSTANT_UTIL.JFS_PROPERTIES)
                                    .replaceAll(CONSTANT_UTIL.REGEX_APHANUMERIC,
                                            CONSTANT_UTIL.DEFAULT_EMPTY).trim())) {
                throw new ApplicationException("ERR-JFS-PAYMENT-WRONG-DETAIL");
            }
            j++;
        }

        // Process Date
        for (int i = 0; i < data.length; i++) {
            if (data[i].contains("Printed")) {
                String printedOn = data[i].substring(11, 21);
                Date printedOnDate = dateUtil.getFormattedCurrentDatePattern15(printedOn);
                if (!dateUtil.isSameDay(printedOnDate, (Date) param.get("processDate"))) {
                    throw new ApplicationException(
                            ENUM_ERROR.ERR_JFS_PROCESS_DATE_INCORRECT.toString());
                }
            }
        }
    }

    @Override
    public void exportJFSWriteOffConctractRegistration(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = batchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager
                    .getlistsDirectory("/WS/Data/JFS/Export/_processed/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }

                LOGGER.debug("Start exportJFSWriteOffConctractRegistration");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/_processed/");

                // instantiate generate file
                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.write.off.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat("WRITE_OFF_CONTRACT_REGISTRATION_").concat(
                        runDate + CONSTANT_UTIL.TYPE_PDF);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write to file
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));
                String pathTxt = processPath.concat(entry.getFilename());

                // Populate file
                List<String> files = new ArrayList<String>();
                files.add(pathTxt);
                files.add(filename);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.WRITE_OFF_CONTRACT_REGISTRATION);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.WRITE_OFF_CONTRACT_REGISTRATION_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 1; i <= textLines.length; i++) {
                    String data[] = textLines[i].split(CONSTANT_UTIL.SEMICOLON);
                    if (data[0].equals("HCI")) {
                        if (paymentDetailReports.size() == 0) {
                            PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                            paymentDetailReport.setTextContractNumber("-");
                            paymentDetailReport.setTotalAmount("-");
                            paymentDetailReports.add(paymentDetailReport);
                        } else {
                            PaymentDetailReport detailReport = paymentDetailReports
                                    .get(paymentDetailReports.size() - 1);
                            detailReport.setTotalAmount(data[2]);
                            detailReport.setTotalAmount(data[1]);
                            paymentDetailReports.set(paymentDetailReports.size() - 1, detailReport);
                        }
                    } else {
                        PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                        paymentDetailReport.setTextContractNumber(data[0]);
                        paymentDetailReports.add(paymentDetailReport);
                    }
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // Export to pdf
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);

                String content = "Export JSF Payment - Write Off Testing";
                StringWriter outputWriter = new StringWriter();
                outputWriter.write(content);

                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Send email exportJFSWriteOffConctractRegistration to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("ist@homecredit.co.id", userLogin.getTextEmailAddress(),
                            CONSTANT_UTIL.DEFAULT_EMPTY, CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Generate Write Off Contract Registration", htmlMessage, files);
                } catch (Exception e) {
                    LOGGER.debug("Send Email failed exportJFSWriteOffConctractRegistration");
                }
                LOGGER.debug("End exportJFSWriteOffConctractRegistration");
            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
        }
    }
    @Override
    public void exportJFSWriteOffRecoveryPayment(Map<String, Object> param) throws Exception {
        try {
            String agreementCode = (String) param.get("agreementCode");
            String partnerCode = (String) param.get("partnerCode");
            String processCode = (String) param.get("processCode");
            String runDate = (String) param.get("createdDate");
            String letterNumber = (String) param.get("letterNumber");
            String type = (String) param.get("type");
            Employee userLogin = (Employee) param.get("userLogin");

            // Get original file
            BatchJobLog batchJobLog = batchDAO.getBatchJobByPartnerByAgreementAndProcessCode(
                    partnerCode, agreementCode, processCode, runDate, type);
            String[] splittedBatchJobLog = batchJobLog.getFileName().split("/");
            String originalFile = splittedBatchJobLog[splittedBatchJobLog.length - 1];

            sshManager.connect();
            Vector<ChannelSftp.LsEntry> list = sshManager.getlistsDirectory("/WS/Data/JFS/Export/");
            for (ChannelSftp.LsEntry entry : list) {
                // Process original file
                if (!entry.getFilename().equals(originalFile)) {
                    continue;
                }

                LOGGER.debug("Start exportJFSWriteOffRecoveryPayment");

                ChannelSftp sftpChannel = (ChannelSftp) sshManager.getChannelSftp();
                sftpChannel.cd("/WS/Data/JFS/Export/");

                Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                        "file.path.generate.write.off.list", CONSTANT_UTIL.JFS_PROPERTIES,
                        CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);
                String processPath = mapPath.get(processCode);
                String filename = processPath.concat(CONSTANT_REPORT.WRITE_OFF_RECOVERY_FILENAME)
                        .concat(runDate).concat(CONSTANT_UTIL.TYPE_PDF);

                StringBuilder text = fileUtil.readFile(sftpChannel.get(entry.getFilename()));
                if (text == null) {
                    throw new ApplicationException("ERR-GENERAL-002");
                }

                // Write to file
                fileUtil.writeToFile(sftpChannel.get(entry.getFilename()),
                        processPath.concat(entry.getFilename()));

                // Populate file
                List<String> files = new ArrayList<String>();
                String pathCsv = processPath.concat(entry.getFilename());
                files.add(pathCsv);
                files.add(filename);

                // Instantiate jasper report
                JasperReport jasperReport = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.WRITE_OFF_RECOVERY_PAYMENT);
                JasperReport jasperReportSub = fileUtil
                        .jasperReportCompile(CONSTANT_REPORT.WRITE_OFF_RECOVERY_PAYMENT_SUB_REPORT);
                List<PaymentReport> paymentReports = new ArrayList<PaymentReport>();
                PaymentReport paymentReport = new PaymentReport();
                List<PaymentDetailReport> paymentDetailReports = new ArrayList<PaymentDetailReport>();

                // Populate report data
                BigDecimal total = BigDecimal.ZERO;
                String[] textLines = text.toString().split(CONSTANT_UTIL.DEFAULT_ENTER_CHARACTER);
                for (int i = 0; i < textLines.length; i++) {
                    if (i == 0) {
                        continue;
                    }
                    String data[] = textLines[i].split(CONSTANT_UTIL.SEMICOLON);
                    PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                    paymentDetailReport.setRef(data[0]);
                    paymentDetailReport.setTextContractNumber(data[1]);
                    paymentDetailReport.setCustomerName(data[2]);
                    paymentDetailReport.setRecoveryAmount(data[4]);
                    BigDecimal amount = new BigDecimal(paymentDetailReport.getRecoveryAmount());
                    total = total.add(amount);
                    paymentDetailReports.add(paymentDetailReport);
                }

                if (paymentDetailReports.size() == 0) {
                    PaymentDetailReport paymentDetailReport = new PaymentDetailReport();
                    paymentDetailReport.setRef(CONSTANT_UTIL.MINUS);
                    paymentDetailReport.setTextContractNumber(CONSTANT_UTIL.MINUS);
                    paymentDetailReport.setCustomerName(CONSTANT_UTIL.MINUS);
                    paymentDetailReport.setRecoveryAmount(CONSTANT_UTIL.MINUS);
                    paymentDetailReport.setTotalAmount(CONSTANT_UTIL.NUMBER_0);
                    paymentDetailReports.add(paymentDetailReport);
                } else {
                    PaymentDetailReport detailReport = paymentDetailReports
                            .get(paymentDetailReports.size() - 1);
                    detailReport.setTotalAmount(total.toString());
                    paymentDetailReports.set(paymentDetailReports.size() - 1, detailReport);
                }

                paymentReport.setLetterNumber(letterNumber);
                paymentReport.setLetterDate(runDate);
                paymentReport.setPaymentDetails(paymentDetailReports);
                paymentReports.add(paymentReport);

                // export report to file
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                        paymentReports);
                Map<String, Object> paramReport = new HashMap<String, Object>();
                paramReport.put("SUB_REPORT_PARAM", jasperReportSub);
                fileUtil.exportToPDF(jasperReport, paramReport, dataSource, filename);

                // save generate detail log
                param.put("log", batchJobLog);
                param.put("filename", filename);
                saveGenerateDetailLogExportJFSPayment(param);

                // String content = "Export JSF Payment - Write Off Testing";
                // StringWriter outputWriter = new StringWriter();
                // outputWriter.write(content);
                sftpChannel.exit();
                sshManager.close();

                LOGGER.debug("Sending email exportJFSWriteOffRecoveryPayment to "
                        + userLogin.getTextEmailAddress());
                try {
                    StringWriter htmlMessage = new StringWriter();
                    htmlMessage.append("The process is already done.\nThank You");
                    emailEngine.sendEmail("ist@homecredit.co.id", userLogin.getTextEmailAddress(),
                            CONSTANT_UTIL.DEFAULT_EMPTY, CONSTANT_UTIL.DEFAULT_EMPTY,
                            "Generate Write Off Recovery", htmlMessage, files);
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.debug("Error send email to " + userLogin.getTextEmailAddress()
                            + " with message " + e.getMessage());
                }
                LOGGER.debug("End exportJFSWriteOffRecoveryPayment");

            }
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage
                    .append("The process is unsucessfull please contact our support team.\nFor detail information error : \n"
                            + e + "\nThank You");
            try {
                LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY, "Generate Write Off Recovery", htmlMessage);
                LOGGER.debug("End Email");
            } catch (Exception ex) {
            }
        }
    }

    @Override
    public void saveGenerateDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        LOGGER.debug("Save Generate Detail " + param.get("processCode"));
        String filename = (String) param.get("filename");
        UserCredential userCredential = (UserCredential) param.get("userCredential");
        BatchJobLog log = (BatchJobLog) param.get("log");
        String letterNumber = (String) param.get("letterNumber");

        String name = filename.split(Pattern.quote("\\"))[filename.split(Pattern.quote("\\")).length - 1];
        GenerateFile generateFile = new GenerateFile();
        generateFile.setAgreement(log.getAgreement());
        generateFile.setPartner(log.getPartner());
        generateFile.setProcessType(log.getProcess());
        generateFile.setFileName(filename);
        generateFile.setCreatedDate(new Date());
        generateFile.setProceedDate((Date) param.get("proceedDate"));
        generateFile.setLetterNumber(letterNumber);
        generateFile.setIsCreated(CONSTANT_UTIL.DEFAULT_YES);
        generateFile.setRunIdBatchJobLog(log.getRunId());
        generateFile.setName(name);
        generateFile.setCreatedBy(userCredential.getUsername());
        batchDAO.save(generateFile);
        LOGGER.debug("Done Generate Detail " + param.get("processCode"));
    }

    @Override
    public void saveUploadDetailLogExportJFSPayment(Map<String, Object> param) throws Exception {
        LOGGER.debug("Save Upload Detail " + param.get("processCode"));
        Map<String, String> mapPath = applicationProperties.generateMapByKeyProperties(
                "file.path.upload.write.off.list", CONSTANT_UTIL.JFS_PROPERTIES,
                CONSTANT_UTIL.DEFAULT_SEPARATOR, CONSTANT_UTIL.DEFAULT_COMMA);

        UserCredential userCredential = (UserCredential) param.get("userCredential");

        Media file = (Media) param.get("media");

        UploadFile uploadFile = new UploadFile();
        uploadFile.setCreatedDate(new Date());
        uploadFile.setChecksum((String) param.get("checksum"));
        uploadFile.setName(file.getName());
        uploadFile.setSizeFile((String) param.get("size"));

        ProcessType processType = new ProcessType();
        processType.setCode((String) param.get("processCode"));
        uploadFile.setProcessType(processType);

        uploadFile.setFilepath(mapPath.get(processType.getCode()).concat(file.getName()));
        Agreement agreement = new Agreement();
        agreement.setCode((String) param.get("agreementCode"));
        uploadFile.setAgreement(agreement);

        Partner partner = new Partner();
        partner.setId((String) param.get("partnerCode"));
        uploadFile.setPartner(partner);
        uploadFile.setCreatedBy(userCredential.getUsername());
        uploadFile.setFileType((String) param.get("fileType"));
        batchDAO.save(uploadFile);

        if (file.getFormat() != null) {
            fileUtil.writeToFile((new String(file.getStringData())), uploadFile.getFilepath());

        } else {
            fileUtil.writeToFile((new String(file.getByteData())), uploadFile.getFilepath());

        }
        LOGGER.debug("Done Upload Detail " + param.get("processCode"));
    }

    public void setApplicationExceptionService(
            ApplicationExceptionService applicationExceptionService) {
        this.applicationExceptionService = applicationExceptionService;
    }

    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setBatchDAO(BatchDAOImpl batchDAO) {
        this.batchDAO = batchDAO;
    }

    public void setDateUtil(DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    public void setEmailEngine(EmailEngine emailEngine) {
        this.emailEngine = emailEngine;
    }

    public void setFileUtil(FileUtil fileUtil) {
        this.fileUtil = fileUtil;
    }

    public void setPaymentDAO(PaymentDAOImpl paymentDAO) {
        PaymentDAO = paymentDAO;
    }

    public void setSshManager(SSHManager sshManager) {
        this.sshManager = sshManager;
    }

    @Override
    public void uploadJFSWriteOffFile(Map<String, Object> param) throws Exception {
        try {
            LOGGER.debug("Start confirmation JSP Payment File " + param.get("processCode"));
            if (mapContractNotFound != null) {
                mapContractNotFound.clear();
            } else {
                mapContractNotFound = new HashMap<String, Object>();
            }
            confirmationJFSStep1(param);
            confirmationJFSStep2(param);
            LOGGER.debug("End confirmation JSP Payment File " + param.get("processCode"));

            saveUploadDetailLogExportJFSPayment(param);
            Employee userLogin = (Employee) param.get("userLogin");
            StringWriter htmlMessage = new StringWriter();
            htmlMessage.append("The process is already done.\nThank You");
            LOGGER.debug("Send Email to " + userLogin.getTextEmailAddress());

            htmlMessage.append("Thank You");
            try {
                emailEngine.sendEmail("ist@homecredit.co.id", userLogin.getTextEmailAddress(),
                        CONSTANT_UTIL.DEFAULT_EMPTY, CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Write Off Recovery Payment Confirmation ", htmlMessage);
            } catch (Exception e) {
                LOGGER.debug("Send email failed uploadJFContractRegistration ");
            }
            LOGGER.debug("End Send Email");
        } catch (Exception e) {
            LOGGER.debug("Error " + e.getMessage());
            e.printStackTrace();
            try {
                Employee userLogin = (Employee) param.get("userLogin");
                StringWriter htmlMessage = new StringWriter();
                htmlMessage.append("The process is unsucessfull<br>");
                if (e instanceof ApplicationException) {
                    ApplicationException ex = (ApplicationException) e;
                    String error = applicationExceptionService.getApplicationExceptionByErrorCode(
                            ex.getErrorCode()).getErrorMessage();
                    htmlMessage.append("For detail information error : <i>" + error + "</i><br>");
                } else {
                    htmlMessage.append("For detail information error : <i>" + e + "</i><br>");
                }
                htmlMessage.append("Please contact our support team<br>");
                htmlMessage.append("Thank You");
                emailEngine.sendEmail("jfs.cofin@homecredit.co.id",
                        userLogin.getTextEmailAddress(), CONSTANT_UTIL.DEFAULT_EMPTY,
                        CONSTANT_UTIL.DEFAULT_EMPTY,
                        "Notification - Write Off Recovery Payment Confirmation ", htmlMessage);
            } catch (Exception ex) {
                LOGGER.debug("Send email failed");
            }
            throw e;
        }
    }
}
