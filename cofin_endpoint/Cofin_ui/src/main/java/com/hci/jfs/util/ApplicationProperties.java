package com.hci.jfs.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

public class ApplicationProperties {

	/**
	 * @param key
	 * @return value
	 */
	
	public String getProperties(String key) {
		String result = null;
		
		try {
			Properties prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream input = loader.getResourceAsStream("application.properties");

			//load a properties file from class path, inside static method
			prop.load(input);
			
			result = prop.getProperty(key);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return result;
	}
		
	/**
	 * @param key
	 * @return
	 */
	public List<HashMap<String, String>> getDroplist(String key) {
		List<HashMap<String, String>> result = new ArrayList<HashMap<String,String>>();
		
		try {
			Properties prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream input = loader.getResourceAsStream("application.properties");

			//load a properties file from class path, inside static method
			prop.load(input);
			
			String value = prop.getProperty(key);
			
			String[] parse = value.split(",");
			
			for (int i = 0; i < parse.length; i++) {
				String[] data = parse[i].split(Pattern.quote("|"));
				
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("id", data[0]);
				map.put("value", data[1]);
				
				result.add(map);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return result;
	}
	
	
	/**
	 * @param key
	 * @param resourceName
	 * @return value
	 */
	public String getProperties(String key,String resourceName) {
		String result = null;
		
		try {			
			Properties prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			if(loader==null){
				loader = ApplicationProperties.class.getClassLoader();
			}
			InputStream input = loader.getResourceAsStream(resourceName);
			//load a properties file from class path, inside static method
			prop.load(input);
			
			result = prop.getProperty(key);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * @param key
	 * @param resourceName
	 * @return value
	 */
	public List<HashMap<String, String>> getDroplist(String key,String resourceName) {
		List<HashMap<String, String>> result = new ArrayList<HashMap<String,String>>();
		
		try {
			Properties prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream input = loader.getResourceAsStream(resourceName);

			//load a properties file from class path, inside static method
			prop.load(input);
			
			String value = prop.getProperty(key);
			
			String[] parse = value.split(",");
			
			for (int i = 0; i < parse.length; i++) {
				String[] data = parse[i].split(Pattern.quote("|"));
				
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("id", data[0]);
				map.put("value", data[1]);
				
				result.add(map);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return result;
	}
	
	
	/**
	 * @param key
	 * @param resourceName
	 * @param separator1
	 * @param separator2
	 * @return
	 */
	public Map<String, String> generateMapByKeyProperties(String key,String resourceName,String separator1,String separator2) {
		Map<String, String> result = new HashMap<String,String>();
		
		try {
			Properties prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream input = loader.getResourceAsStream(resourceName);

			//load a properties file from class path, inside static method
			prop.load(input);	
			String value = prop.getProperty(key);			
			String[] parse = value.split(Pattern.quote(separator1));
			
			for (int i = 0; i < parse.length; i++) {
				String[] data = parse[i].split(Pattern.quote(separator2));
				result.put(data[0],data[1]);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return result;
	}
}
