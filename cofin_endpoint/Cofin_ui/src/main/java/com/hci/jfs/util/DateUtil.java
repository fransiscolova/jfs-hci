package com.hci.jfs.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.hci.jfs.constant.CONSTANT_DATE;

public class DateUtil {


	private SimpleDateFormat sdf;

	/**
	 *  @param new Date() -- yyyy-MM-dd  
	 *  @return 2016-01-01
	 */
	public String getFormattedCurrentDatePattern2(Date current) {
		sdf = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_2);
		return sdf.format(current);
	}
	
	
	/**
	 *  @param String date --> dd/MM/yyyy  
	 *  @return Date ()
	 *  @throws ParseException 
	 */
	public Date getFormattedCurrentDatePattern10(String date) throws ParseException {
		DateFormat formatter = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_10);
		Date startDate = (Date)formatter.parse(date); 
		return startDate;
	}

	
	/**
	 *  @param new Date() --> dd/MM/yyyy 
	 *  @return 01/01/2017
	 */
	public String getFormattedCurrentDatePattern10(Date current) {
		sdf = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_10);
		return sdf.format(current);
	}
	
	/**
	 *  @param new Date() --> ddMMyyHHmmss  
	 *  @return 1Dec16170000
	 */
	public String getFormattedCurrentDatePattern14(Date current) {
		sdf = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_14);
		return sdf.format(current);
	}
	
	
	/**
	 *  @param String date --> dd-MM-yyyy 
	 *  @return Date ()
	 *  @throws ParseException 
	 */
	public Date getFormattedCurrentDatePattern15(String date) throws ParseException {
		DateFormat formatter = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_15);
		Date startDate = (Date)formatter.parse(date); 
		return startDate;
	}

	/**
	 *  @param new Date() --> dd-MM-yyyy  
	 *  @return 1-01-2017
	 */
	public String getFormattedCurrentDatePattern15(Date current) {
		sdf = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_15);
		return sdf.format(current);
	}


	/**
	 *  @param new Date() --> yyyy-MM-dd  
	 *  @return 1-JAN-2017
	 */
	public String getFormattedCurrentDatePattern16(Date current) {
		sdf = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_16);
		return sdf.format(current);
	}
	
		
	/**
	 *  @param String date - dd-MM-yyyy  
	 *  @return Date () --> 1-JAN-2017
	 *  @throws ParseException 
	 */
	public Date getFormattedCurrentDatePattern16(String date) throws ParseException {
		DateFormat formatter = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_16);
		Date startDate = (Date)formatter.parse(date); 
		return startDate;
	}

	/**
	 *  @param new Date() --> yyyy-MM-dd  
	 *  @return 02-02-2017 14:13:22
	 */
	public String getFormattedCurrentDatePattern17(Date current) {
		sdf = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_17);
		return sdf.format(current);
	}
	
		
	/**
	 *  @param String date - 02-02-2017 14:13:22 
	 *  @return Date () 
	 *  @throws ParseException 
	 */
	public Date getFormattedCurrentDatePattern17(String date) throws ParseException {
		DateFormat formatter = new SimpleDateFormat(CONSTANT_DATE.DATE_FORMAT_PATTERN_17);
		Date startDate = (Date)formatter.parse(date); 
		return startDate;
	}


	/**
	 *  @param system current time
	 *  @return True or False
	 */
	public boolean isHoliday(){
		Calendar c = Calendar.getInstance();
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		return (dayOfWeek == Calendar.SATURDAY ||dayOfWeek == Calendar.SUNDAY);
	}

	/**
	 * <p>Checks if two dates are on the same day ignoring time.</p>
	 * @param date1  the first date, not altered, not null
	 * @param date2  the second date, not altered, not null
	 * @return true if they represent the same day
	 * @throws IllegalArgumentException if either date is <code>null</code>
	 */
	public boolean isSameDay(Date date1, Date date2) {
		if (date1 == null || date2 == null) {
			throw new IllegalArgumentException("The dates must not be null");
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		return isSameDay(cal1, cal2);
	}

	/**
	 * <p>Checks if two calendars represent the same day ignoring time.</p>
	 * @param cal1  the first calendar, not altered, not null
	 * @param cal2  the second calendar, not altered, not null
	 * @return true if they represent the same day
	 * @throws IllegalArgumentException if either calendar is <code>null</code>
	 */
	public boolean isSameDay(Calendar cal1, Calendar cal2) {
		if (cal1 == null || cal2 == null) {
			throw new IllegalArgumentException("The dates must not be null");
		}
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
				cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
				cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
	}
	
}
