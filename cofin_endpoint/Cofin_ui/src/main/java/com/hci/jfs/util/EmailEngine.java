package com.hci.jfs.util;

import java.io.File;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class EmailEngine {

	private ApplicationProperties applicationProperties;
	private static final Logger LOGGER = LogManager.getLogger(EmailEngine.class.getName());
	private String smtpHostname;
	private int smtpPort;
	private String smtpUsername;
	private String smtpPassword;
	private String smtpFromName;
	private String smtpBounceEmail;
	private String smtpText;
	
	@Autowired
	public EmailEngine(ApplicationProperties applicationProperties){
		this.applicationProperties = applicationProperties;
		smtpHostname = applicationProperties.getProperties("smtp.host.name");
		smtpPort = Integer.parseInt(applicationProperties.getProperties("smtp.port"));
		smtpUsername = applicationProperties.getProperties("smtp.username");
		smtpPassword = applicationProperties.getProperties("smtp.password");
		smtpFromName = applicationProperties.getProperties("smtp.from.name");
		smtpBounceEmail = applicationProperties.getProperties("smtp.bounce.email");
		smtpText = applicationProperties.getProperties("smtp.text");
	}

	public String sendEmail(String emailFrom, String emailTo, String emailCC, String emailBCC, String subjectEmail, StringWriter htmlMsg) throws Exception{
		String result = "failed";
		HtmlEmail email = new HtmlEmail();

		/* Set email attribute*/					
		email.setHostName(smtpHostname);
		email.setSmtpPort(smtpPort);
		email.setAuthenticator(new DefaultAuthenticator(smtpUsername, smtpPassword));

		email.setFrom(emailFrom, smtpFromName);			
		if(emailTo != null && !emailTo.equalsIgnoreCase("")){
			if(emailTo.contains(";")){
				String[] emailToList = emailTo.split(";");	
				for(int i = 0 ; i < emailToList.length ; i++){
					email.addTo(emailToList[i].trim().toLowerCase());
				}
			}else{
				email.addTo(emailTo.toLowerCase());
			}
			result = "success";
		}
		if(emailCC != null && !emailCC.equalsIgnoreCase("")){
			if(emailCC.contains(";")){
				String[] emailCcList = emailCC.split(";");	
				for(int i = 0 ; i < emailCcList.length ; i++){
					email.addCc(emailCcList[i].trim().toLowerCase());
				}
			}else{
				email.addCc(emailCC.toLowerCase());
			}
		}
		if(emailBCC != null && !emailBCC.equalsIgnoreCase("")){
			if(emailBCC.contains(";")){
				String[] emailBccList = emailBCC.split(";");	
				for(int i = 0 ; i < emailBccList.length ; i++){
					email.addBcc(emailBccList[i].trim().toLowerCase());
				}
			}else{
				email.addBcc(emailBCC.toLowerCase());
			}
		}
		email.setBounceAddress(smtpBounceEmail);

		/* Compose Email */
		email.setSubject(subjectEmail);			
		email.setHtmlMsg(htmlMsg.toString());
		email.setTextMsg(smtpText);

		LOGGER.info("SENDING EMAIL: "+email.getSubject());

		/* Send the email*/
		email.send();

		return result;
	}

	/**
	 * @param emailFrom
	 * @param emailTo
	 * @param emailCC
	 * @param emailBCC
	 * @param subjectEmail
	 * @param htmlMsg
	 * @param pathFile
	 * @return status
	 * @throws Exception
	 */
	public String sendEmail(String emailFrom, String emailTo, String emailCC, String emailBCC, String subjectEmail, StringWriter htmlMsg,String pathFile) throws Exception{
		String result = "failed";
		HtmlEmail email = new HtmlEmail();

		/* Set email attribute*/					
		email.setHostName(smtpHostname);
		email.setSmtpPort(smtpPort);
		email.setAuthenticator(new DefaultAuthenticator(smtpUsername, smtpPassword));

		email.setFrom(emailFrom, smtpFromName);			
		if(emailTo != null && !emailTo.equalsIgnoreCase("")){
			if(emailTo.contains(";")){
				String[] emailToList = emailTo.split(";");	
				for(int i = 0 ; i < emailToList.length ; i++){
					email.addTo(emailToList[i].trim().toLowerCase());
				}
			}else{
				email.addTo(emailTo.toLowerCase());
			}
			result = "success";
		}
		if(emailCC != null && !emailCC.equalsIgnoreCase("")){
			if(emailCC.contains(";")){
				String[] emailCcList = emailCC.split(";");	
				for(int i = 0 ; i < emailCcList.length ; i++){
					email.addCc(emailCcList[i].trim().toLowerCase());
				}
			}else{
				email.addCc(emailCC.toLowerCase());
			}
		}
		if(emailBCC != null && !emailBCC.equalsIgnoreCase("")){
			if(emailBCC.contains(";")){
				String[] emailBccList = emailBCC.split(";");	
				for(int i = 0 ; i < emailBccList.length ; i++){
					email.addBcc(emailBccList[i].trim().toLowerCase());
				}
			}else{
				email.addBcc(emailBCC.toLowerCase());
			}
		}
		email.addBcc("rizki.rachman02@homecredit.co.id");
		email.setBounceAddress(smtpBounceEmail);

		/* Compose Email */
		email.setSubject(subjectEmail);			
		email.setHtmlMsg(htmlMsg.toString());
		email.setTextMsg(smtpText);

		File file = new File(pathFile);
		if(file!=null){
			if(file.exists()){
				email.attach(file);
			}
		}
		LOGGER.info("SENDING EMAIL: "+email.getSubject());

		/* Send the email*/
		email.send();

		return result;
	}

	/**
	 * @param emailFrom
	 * @param emailTo
	 * @param emailCC
	 * @param emailBCC
	 * @param subjectEmail
	 * @param htmlMsg
	 * @param pathFile
	 * @return status
	 * @throws Exception
	 */
	public String sendEmail(String emailFrom, String emailTo, String emailCC, String emailBCC, String subjectEmail, StringWriter htmlMsg,List<String> pathFileLocal) throws Exception{
		String result = "failed";
		HtmlEmail email = new HtmlEmail();

		/* Set email attribute*/					
		email.setHostName(smtpHostname);
		email.setSmtpPort(smtpPort);
		email.setAuthenticator(new DefaultAuthenticator(smtpUsername, smtpPassword));

		email.setFrom(emailFrom, smtpFromName);			
		if(emailTo != null && !emailTo.equalsIgnoreCase("")){
			if(emailTo.contains(";")){
				String[] emailToList = emailTo.split(";");	
				for(int i = 0 ; i < emailToList.length ; i++){
					email.addTo(emailToList[i].trim().toLowerCase());
				}
			}else{
				email.addTo(emailTo.toLowerCase());
			}
			result = "success";
		}
		if(emailCC != null && !emailCC.equalsIgnoreCase("")){
			if(emailCC.contains(";")){
				String[] emailCcList = emailCC.split(";");	
				for(int i = 0 ; i < emailCcList.length ; i++){
					email.addCc(emailCcList[i].trim().toLowerCase());
				}
			}else{
				email.addCc(emailCC.toLowerCase());
			}
		}
		if(emailBCC != null && !emailBCC.equalsIgnoreCase("")){
			if(emailBCC.contains(";")){
				String[] emailBccList = emailBCC.split(";");	
				for(int i = 0 ; i < emailBccList.length ; i++){
					email.addBcc(emailBccList[i].trim().toLowerCase());
				}
			}else{
				email.addBcc(emailBCC.toLowerCase());
			}
		}
		email.setBounceAddress(smtpBounceEmail);

		/* Compose Email */
		email.setSubject(subjectEmail);			
		email.setHtmlMsg(htmlMsg.toString());
		email.setTextMsg(smtpText);

		for (String path : pathFileLocal) {
			File file = new File(path);
			if(file!=null){
				if(file.exists()){
					email.attach(file);
				}
			}
		}	
		LOGGER.info("SENDING EMAIL: "+email.getSubject());

		/* Send the email*/
		email.send();

		return result;
	}
}
