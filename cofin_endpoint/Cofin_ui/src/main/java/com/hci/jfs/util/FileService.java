package com.hci.jfs.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dinda.Wahyudi06
 */
@Service
public class FileService{
	final static Logger LOGGER = LogManager.getLogger(FileService.class.getName());

	public static StringBuilder readFile(InputStream stream){
		StringBuilder sb = new StringBuilder();
		try{
			char[] ch_Buffer = new char[0x10000];
			Reader obj_Reader = new InputStreamReader(stream, "UTF-8");
			int int_Line = 0;
			do
			{
				int_Line = obj_Reader.read(ch_Buffer, 0, ch_Buffer.length);
				if (int_Line > 0)
				{ sb.append(ch_Buffer, 0, int_Line);}
			}
			while (int_Line >= 0);
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("File Service - read file "+e.getMessage());
		}
		return sb;
	}

	public static String readFileByPath(String path) throws IOException{
		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(path))){
			String sCurrentLine="";
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine.trim());
			}
		}
		return sb.toString();
	}

	/**
	 * @param inputstream
	 * @param path
	 * write to file using inputstream and path
	 */
	public static void writeToFile(InputStream stream, String path){
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			// read this file into InputStream
			inputStream = stream;
			// write the inputStream to a FileOutputStream
			outputStream =new FileOutputStream(new File(path));

			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
