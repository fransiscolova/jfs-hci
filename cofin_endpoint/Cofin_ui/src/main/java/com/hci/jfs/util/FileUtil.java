package com.hci.jfs.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Map;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dinda.Wahyudi06
 */
@Service
public class FileUtil{
	final  Logger LOGGER = LogManager.getLogger(FileUtil.class.getName());

	public  StringBuilder readFile(InputStream stream){
		StringBuilder sb = new StringBuilder();
		try{
			char[] ch_Buffer = new char[0x10000];
			Reader obj_Reader = new InputStreamReader(stream, "UTF-8");
			int int_Line = 0;
			do
			{
				int_Line = obj_Reader.read(ch_Buffer, 0, ch_Buffer.length);
				if (int_Line > 0)
				{ sb.append(ch_Buffer, 0, int_Line);}
			}
			while (int_Line >= 0);
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("File Service - read file "+e.getMessage());
		}
		return sb;
	}

	public  String readFileByPath(String path) throws IOException{
		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(path))){
			String sCurrentLine="";
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine.trim());
			}
		}
		return sb.toString();
	}

	/**
	 * @param inputstream
	 * @param path
	 * write to file using inputstream and path
	 */
	public  void writeToFile(InputStream stream, String path){
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			// read this file into InputStream
			inputStream = stream;
			// write the inputStream to a FileOutputStream
			outputStream =new FileOutputStream(new File(path));

			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public  void unzipFile(File source, String destination, String password) throws ZipException{

		try {
			ZipFile zipFile = new ZipFile(source);

			if (zipFile.isEncrypted() && password != null) 
				zipFile.setPassword(password);

			zipFile.extractAll(destination);
		} catch (ZipException e) {
			throw new ZipException(e);
		}
	}

	public  String[] getFiles(String folder) throws IOException {
		File _folder = new File(folder);
		String[] filesInFolder;

		if (_folder.isDirectory()) {
			filesInFolder = _folder.list();

			//Sort in alphabetical order 
			Arrays.sort(filesInFolder);

			return filesInFolder;
		} else {
			throw new IOException(folder + " is not a directory");
		}
	}

	/**
	 * @param path file template 
	 * @return
	 * create template jasper report
	 */
	public  JasperReport jasperReportCompile(String path) throws Exception{
		System.out.println("Path "+path);
		BufferedInputStream	is =  new BufferedInputStream(new FileInputStream(path));

		JasperDesign jasperDesign = JRXmlLoader.load(is);
		JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
		return jasperReport;
	}

	/**
	 * @param path file template 
	 * @return
	 * create template jasper report
	 */
	public  void exportToPDF(JasperReport jasperReport,Map<String,Object> parameter, JRBeanCollectionDataSource dataSource,String filename) throws Exception{
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, dataSource);  
		JasperExportManager.exportReportToPdfFile(jasperPrint,filename);
	}


	/**
	 * @param inputstream
	 * @return bytes
	 * @throws IOException
	 */
	public  byte[] getBytes(InputStream is) throws IOException {

		int len;
		int size = 1024;
		byte[] buf;

		if (is instanceof ByteArrayInputStream) {
			size = is.available();
			buf = new byte[size];
			len = is.read(buf, 0, size);
		} else {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			buf = new byte[size];
			while ((len = is.read(buf, 0, size)) != -1)
				bos.write(buf, 0, len);
			buf = bos.toByteArray();
		}
		return buf;
	}

	/**
	 * @param content
	 * @param path
	 */
	public  void writeToFile(String content,String path){
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
			bw.write(content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param content
	 * @param path
	 * @throws Exception 
	 */
	public String getChecksumOnFile(String content) throws Exception{
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(content.getBytes());

		byte byteData[] = md.digest();
		//convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
}
