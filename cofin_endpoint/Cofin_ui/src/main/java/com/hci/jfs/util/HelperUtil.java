package com.hci.jfs.util;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.hci.jfs.constant.CONSTANT_UTIL;

/**
 * @author rizki.rachman02
 *
 */
public class HelperUtil {

  /**
   * @param recipientBankCode
   * @param length
   * @param replaceChar
   * @return recipientBankCode=14, length recipientBankCode.length, replaceChar = 0 --> 014
   *         recipientBankCode=114, length recipientBankCode.length, replaceChar = 0 --> 114
   */
  public static String checkBankCode(String recipientBankCode, int length, String additionalChar) {
    if (recipientBankCode.length() < length && recipientBankCode.length() > 0) {
      for (int i = 0; i < length - (recipientBankCode.length() - 1); i++) {
        recipientBankCode = additionalChar.concat(recipientBankCode);
      }
    }
    return recipientBankCode;
  }

  /**
   * @param text
   * @return text = "text," --> text text = "", --> "" text = "," --> ""
   */
  public static String removeLastDelimiterOnText(String text) {
    return text.length() > 1 ? text.substring(0, text.length() - 1) : CONSTANT_UTIL.DEFAULT_EMPTY;
  }

  /**
   * @param map
   * @param delimiter
   * @return map={"code","test1"},{"code","test2"} , delimiter="," --> test1,test2
   */
  public static String wrapValuesFromMapWithDelimiter(Map<String, Object> map, String delimiter) {
    String temp = CONSTANT_UTIL.DEFAULT_EMPTY;
    for (Map.Entry<String, Object> entry : map.entrySet()) {
      temp = temp.concat(((String) entry.getValue()).concat(delimiter));
    }
    return removeLastDelimiterOnText(temp);
  }

  /**
   * @param character
   * @param length
   * @return character = # length = 5 result --> #####
   */
  public static String insertCharacter(String character, int length) {
    String temp = CONSTANT_UTIL.DEFAULT_EMPTY;
    for (int i = 0; i < length; i++) {
      temp = temp.concat(character);
    }
    return temp;
  }

  /**
   * @param list
   * @return List generic Lists.newArrayList(null, 1, null) --> list{null,1,null} result list{1}
   */
  public static <T> List<T> removeNullElementsFromList(List<T> list) {
    list.removeAll(Collections.singleton(null));
    return list;
  }

}
