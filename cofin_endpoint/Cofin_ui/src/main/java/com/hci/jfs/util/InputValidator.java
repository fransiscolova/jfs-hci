package com.hci.jfs.util;

import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.hci.jfs.composer.partner.PartnerRegistrationComposer;
import com.hci.jfs.constant.CONSTANT_UTIL;
import com.hci.jfs.entity.Partner;
import com.hci.jfs.security.ApplicationException;

public class InputValidator {
    public static final String NUMERIC_REGEX = "\\d+";

    /**
     * Email format validation.
     *
     * @param emailAddress
     * @return
     */
    public static boolean validateEmail(String emailAddress) {
        if (emailAddress != null && !emailAddress.equals("")) {
            return !emailAddress.matches(".+@.+\\.[a-z]+");
        } else {
            return true;
        }
    }

    /**
     * Validate screen input from generate file menu.
     *
     * @param agreementCodeList
     * @param partnerList
     * @param processTypeList
     * @param processDate
     * @throws ApplicationException
     */
    public static void validateGenerateFileInput(Listbox agreementCodeList, Listbox partnerList,
            Listbox processTypeList, Datebox processDate) throws ApplicationException {
        String idAgreement = agreementCodeList.getSelectedItem().getValue();
        String idPartner = partnerList.getSelectedItem().getValue();
        String processType = processTypeList.getSelectedItem().getValue();
        if (idPartner.equals(CONSTANT_UTIL.NUMBER_0)) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-004");

        }
        if (idAgreement.equals(CONSTANT_UTIL.NUMBER_0)) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-005");

        }
        if (processType.equals(CONSTANT_UTIL.NUMBER_0)) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-001");
        }
        if (processDate.getValue() == null) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-006");
        }
    }

    /**
     * Validate screen input from Partner menu.
     *
     * @param txbPartnerName
     * @param txbPartnerAddress
     * @param txbPartnerContactPerson
     * @param txbPartnerPhoneNumber
     * @param txbPartnerEmail
     * @param lbPartnerId
     * @return String contains error description
     */
    public static String validatePartnerInput(Textbox txbPartnerName, Textbox txbPartnerAddress,
            Textbox txbPartnerContactPerson, Textbox txbPartnerPhoneNumber,
            Textbox txbPartnerEmail, Label lbPartnerId) {
        if (txbPartnerName.getValue().equals("") || txbPartnerAddress.getValue().equals("")
                || txbPartnerContactPerson.getValue().equals("")
                || txbPartnerPhoneNumber.getValue().equals("")
                || txbPartnerEmail.getValue().equals("")) {
            return "Please populate all the required field";
        }

        // Name -> 50, address -> 200, contact -> 100, phone -> 20, email -> 100
        if (txbPartnerName.getValue().length() > 50) {
            return "Partner name must not exceed 50 character";
        } else if (txbPartnerAddress.getValue().length() > 200) {
            return "Partner address must not exceed 200 character";
        } else if (txbPartnerContactPerson.getValue().length() > 100) {
            return "Partner contact person must not exceed 100 character";
        } else if (txbPartnerPhoneNumber.getValue().length() > 20) {
            return "Partner phone number must not exceed 20 character";
        } else if (txbPartnerEmail.getValue().length() > 100) {
            return "Partner email must not exceed 100 character";
        }

        if (validateEmail(txbPartnerEmail.getValue())) {
            return "Email format not valid";
        }

        // Name must be unique validation
        if (lbPartnerId == null) {
            for (Partner partner : PartnerRegistrationComposer.partners) {
                if (partner.getName().equals(txbPartnerName.getValue())) {
                    return "Partner name exist";
                }
            }
        } else {
            for (Partner partner : PartnerRegistrationComposer.partners) {
                if (!lbPartnerId.getValue().equals(partner.getId())) {
                    if (partner.getName().equals(txbPartnerName.getValue())) {
                        return "Partner name exist";
                    }
                }
            }
        }

        // Phone must be numeric
        if (!txbPartnerPhoneNumber.getValue().matches(NUMERIC_REGEX)) {
            return "Partner phone number must be number only";
        }

        return null;
    }

    /**
     * Validate screen input from upload file menu.
     *
     * @param partnerUploadList
     * @param agreementCodeUploadList
     * @param processTypeListUpload
     * @param processTypeFileListUpload
     * @param applicationProperties
     * @param processDateUpload
     * @param media
     * @throws ApplicationException
     */
    public static void validateUploadFileInput(Listbox partnerUploadList,
            Listbox agreementCodeUploadList, Listbox processTypeListUpload,
            Listbox processTypeFileListUpload, ApplicationProperties applicationProperties,
            Datebox processDateUpload, Media media) throws ApplicationException {
        if (partnerUploadList.getSelectedItem().getIndex() == 0) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-004");
        }
        if (agreementCodeUploadList.getSelectedItem() == null) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-005");
        }
        if (processTypeListUpload.getSelectedItem() == null) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-001");
        }

        String idAgreement = agreementCodeUploadList.getSelectedItem().getValue();
        String idPartner = partnerUploadList.getSelectedItem().getValue();
        String processType = processTypeListUpload.getSelectedItem().getValue();
        String fileType = processTypeFileListUpload.getSelectedItem().getValue();
        Map<String, String> mapProcessType = applicationProperties.generateMapByKeyProperties(
                "process.type.allocation.file.upload", CONSTANT_UTIL.JFS_PROPERTIES,
                CONSTANT_UTIL.DEFAULT_COMMA, CONSTANT_UTIL.DEFAULT_SEPARATOR);
        if (idPartner.equals(CONSTANT_UTIL.NUMBER_0)) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-004");
        }
        if (idAgreement.equals(CONSTANT_UTIL.NUMBER_0)) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-005");
        }
        if (processType.equals(CONSTANT_UTIL.NUMBER_0)) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-001");
        }
        if (processDateUpload.getValue() == null) {
            throw new ApplicationException("ERR-JFS-FILE-GENERATED-006");
        }
        if (mapProcessType.get(processType) != null) {
            if (fileType.equals(CONSTANT_UTIL.NUMBER_0)) {
                throw new ApplicationException("ERR-JFS-FILE-GENERATED-007");
            }
        }
        if (media == null) {
            throw new ApplicationException("ERR-JFS-FILE-NOT-UPLOAD");
        }
    }
}
