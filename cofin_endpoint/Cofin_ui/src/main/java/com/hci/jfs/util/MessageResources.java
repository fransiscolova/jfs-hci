package com.hci.jfs.util;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

public class MessageResources implements MessageSourceAware {

	private static MessageSource messageSource;
	
	@Override
	public void setMessageSource(MessageSource paramMessageSource) {
		messageSource = paramMessageSource;
	}
	
	public static String getMessageResources(String key) {
		String result = "";
		try {
			result = messageSource.getMessage(key, null, Locale.getDefault());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
