/**
 *
 */
package com.hci.jfs.util;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import com.hci.jfs.entity.Partner;

/**
 * @author muhammad.muflihun
 *         <p>
 *
 *         Custom id generator for entity {@link Partner}.
 *
 */
public class PartnerIdGenerator implements IdentifierGenerator {
    private StringBuilder customId = new StringBuilder(10);

    public PartnerIdGenerator() {
        customId.append(prefixGenerator());
    }

    /*
     * (non-Javadoc)
     * @see
     * org.hibernate.id.IdentifierGenerator#generate(org.hibernate.engine.spi.SessionImplementor,
     * java.lang.Object)
     */
    @Override
    public Serializable generate(SessionImplementor session, Object obj) throws HibernateException {
        Connection connection = session.connection();

        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select count(id) from jfs_partner");

            if (rs.next()) {
                Integer id = rs.getInt(1) + 1;
                customId.append(suffixGenerator(id));
            }
        } catch (SQLException e) {
            new IllegalArgumentException("Error when generate partner id");
        }
        return customId.toString();
    }

    /**
     * Generate 6 char prefix using current date in format "YYmmdd".
     * <p>
     * ex: 170907, 7th Sept 2017
     *
     * @return String
     */
    private String prefixGenerator() {
        SimpleDateFormat sdf = new SimpleDateFormat("YYmmdd");
        return sdf.format(new Date());
    }

    /**
     * Generate 4 char suffix sequential with zero left-padding.
     * <p>
     * ex: 0001
     *
     * @return String
     */
    private String suffixGenerator(Integer integer) {
        String suffix = String.format("%04d", integer);
        return suffix.length() > 4 ? suffix.substring(suffix.length() - 4, suffix.length() - 1)
                : suffix;
    }

}
