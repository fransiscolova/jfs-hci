package com.hci.jfs.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.hci.jfs.constant.CONSTANT_UTIL;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SSHManager {
		
	private ApplicationProperties applicationProperties;	
	private FileUtil fileUtil;
	
	private final static Logger LOGGER = LogManager.getLogger(SSHManager.class.getName());
	private JSch jschSSHChannel;
	private String strUserName;
	private String strConnectionIP;
	private String hostname;
	private int intConnectionPort;
	private String strPassword;
	private Session sesConnection;
	private int intTimeOut=60;

	@Autowired
	public SSHManager(ApplicationProperties applicationProperties,FileUtil fileUtil){
		this.fileUtil=fileUtil;
		this.applicationProperties=applicationProperties;
		strUserName = this.applicationProperties.getProperties("sftp.username");
		strConnectionIP =this.applicationProperties.getProperties("sftp.ip");
		intConnectionPort=Integer.parseInt(this.applicationProperties.getProperties("sftp.port"));
		strPassword=this.applicationProperties.getProperties("sftp.password");
		hostname=this.applicationProperties.getProperties("sftp.host");
	}
	
	public String connect() {
		String errorMessage = null;
		jschSSHChannel = new JSch();
		try {
			sesConnection = jschSSHChannel.getSession(strUserName, hostname, intConnectionPort);
			sesConnection.setPassword(strPassword);

			// UNCOMMENT THIS FOR TESTING PURPOSES, BUT DO NOT USE IN PRODUCTION
			sesConnection.setConfig("StrictHostKeyChecking", "no");

			sesConnection.connect(intTimeOut);
		} catch (JSchException jschX) {
			errorMessage = jschX.getMessage();
		}

		return errorMessage;
	}

	private void logError(String errorMessage) {		
		if (errorMessage != null) 
			LOGGER.error(strConnectionIP+":"+intConnectionPort+" - "+ errorMessage );		
	}

	private void logInfo(String information) {
		if (information != null) 
			LOGGER.info(strConnectionIP+":"+intConnectionPort+" - "+ information );

	}

	public List<String> sendCommand(String command) {
		List<String> message = new ArrayList<String>();
		//		HashSet<String> message = new HashSet<>();

		logInfo("Sending command : " + command);

		try {
			Channel channel = sesConnection.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			InputStream inputStream = channel.getInputStream();
			channel.connect();

			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));			  

			String line;			  
			while ((line = reader.readLine()) != null) {
				logInfo(line);
				message.add(line);
			}

			channel.disconnect();
		} catch (IOException ioX) {
			logError(ioX.getMessage());
			return null;
		} catch (JSchException jschX) {
			logError(jschX.getMessage());
			return null;
		}

		return message;
	}

	public void uploadFile(String remoteDir, String remoteFile, String localFile){

		try {
			Channel channel = sesConnection.openChannel("sftp");			
			channel.connect();

			ChannelSftp channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteDir);			

			channelSftp.put(new FileInputStream(localFile), remoteFile);

			channelSftp.exit();	
		} catch (IOException ioX) {
			logError(ioX.getMessage());					
		} catch (SftpException sftpX) {
			logError(sftpX.getMessage());			
		} catch (JSchException jschX) {
			logError(jschX.getMessage());			
		}
	}

	public void uploadFiles(String sftpWorkingDir, String guid, String localDir){

		try {
			Channel channel = sesConnection.openChannel("sftp");			
			channel.connect();

			ChannelSftp channelSftp = (ChannelSftp) channel;
			channelSftp.cd(sftpWorkingDir);

			String [] files = fileUtil.getFiles(localDir);

			channelSftp.put(new FileInputStream(localDir+"\\"+files[0]), guid+"_"+files[0]);


			channelSftp.exit();	
		} catch (IOException ioX) {
			logError(ioX.getMessage());					
		} catch (SftpException sftpX) {
			logError(sftpX.getMessage());			
		} catch (JSchException jschX) {
			logError(jschX.getMessage());			
		}
	}

	public void close() {
		sesConnection.disconnect();
	}

	public Channel getChannelSftp() throws JSchException{
		Channel channel = sesConnection.openChannel(CONSTANT_UTIL.CONSTANT_SFTP);			
		channel.connect();
		ChannelSftp channelSftp = (ChannelSftp) channel;
		return channelSftp;
	}

	@SuppressWarnings("unchecked")
	public Vector<ChannelSftp.LsEntry> getlistsDirectory(String dir) throws SftpException, JSchException{
		ChannelSftp channelSftp = (ChannelSftp) getChannelSftp();
		return channelSftp.ls(dir);
	}
}