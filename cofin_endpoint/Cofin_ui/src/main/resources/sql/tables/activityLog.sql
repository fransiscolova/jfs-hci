--------------------------------------------------------
--  DDL for Table ACTIVITY_LOG_JFS_FILE
--------------------------------------------------------

  CREATE TABLE "APP_JFS"."ACTIVITY_LOG_JFS_FILE" 
   (	"AGREEMENT_CODE" VARCHAR2(100 BYTE), 
	"PROCESS_TYPE_CODE" VARCHAR2(100 BYTE), 
	"DATE_UPLOAD" DATE, 
	"PARTNER_CODE" VARCHAR2(100 BYTE), 
	"ID" VARCHAR2(100 BYTE), 
	"FILENAME" VARCHAR2(400 BYTE), 
	"FILETYPE" VARCHAR2(20 BYTE)
   ) ;

   COMMENT ON COLUMN "APP_JFS"."ACTIVITY_LOG_JFS_FILE"."FILETYPE" IS '1.Confirmation 2.Allocation';

--------------------------------------------------------
--  Constraints for Table ACTIVITY_LOG_JFS_FILE
--------------------------------------------------------

  ALTER TABLE "APP_JFS"."ACTIVITY_LOG_JFS_FILE" ADD CONSTRAINT "JFS_UPLOADGENERATE_LOG_PK" PRIMARY KEY ("AGREEMENT_CODE", "PROCESS_TYPE_CODE", "PARTNER_CODE", "FILETYPE")
   ALTER TABLE "APP_JFS"."ACTIVITY_LOG_JFS_FILE" MODIFY ("FILETYPE" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."ACTIVITY_LOG_JFS_FILE" MODIFY ("PARTNER_CODE" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."ACTIVITY_LOG_JFS_FILE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."ACTIVITY_LOG_JFS_FILE" MODIFY ("DATE_UPLOAD" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."ACTIVITY_LOG_JFS_FILE" MODIFY ("PROCESS_TYPE_CODE" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."ACTIVITY_LOG_JFS_FILE" MODIFY ("AGREEMENT_CODE" NOT NULL ENABLE);
