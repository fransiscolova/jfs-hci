--------------------------------------------------------
--  DDL for Table APPLICATION_ERROR
--------------------------------------------------------

  CREATE TABLE "APP_JFS"."APPLICATION_ERROR" 
   (	"ERROR_CODE" VARCHAR2(100 BYTE), 
	"ERROR_MESSAGE" VARCHAR2(200 BYTE), 
	"DESCRIPTION" VARCHAR2(400 BYTE)
   ) ;
--------------------------------------------------------
--  Constraints for Table APPLICATION_ERROR
--------------------------------------------------------

  ALTER TABLE "APP_JFS"."APPLICATION_ERROR" MODIFY ("ERROR_CODE" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."APPLICATION_ERROR" MODIFY ("ERROR_MESSAGE" NOT NULL ENABLE);
