  --------------------------------------------------------
--  DDL for Table ERROR_LOG
--------------------------------------------------------

  CREATE TABLE "ERROR_LOG" 
   (	"ID_LOG" VARCHAR2(40 BYTE), 
	"MESSAGE" CLOB, 
	"CREATED_DATE" DATE, 
	"CREATED_BY" VARCHAR2(100 BYTE)
   ) 

--------------------------------------------------------
--  Constraints for Table ERROR_LOG
--------------------------------------------------------

  ALTER TABLE "ERROR_LOG" ADD CONSTRAINT "ERROR_LOG_PK" PRIMARY KEY ("ID_LOG") 
  ALTER TABLE "ERROR_LOG" MODIFY ("ID_LOG" NOT NULL ENABLE);