--------------------------------------------------------
--  DDL for Table JFS_PROCESS_TYPE
--------------------------------------------------------

  CREATE TABLE "APP_JFS"."JFS_PROCESS_TYPE" 
   (	"CODE" NVARCHAR2(100), 
	"NAME" NVARCHAR2(400), 
	"DESCRIPTION" NVARCHAR2(400), 
	"IS_DELETE" NVARCHAR2(1) DEFAULT 'N', 
	"TYPE_FILE" NVARCHAR2(20), 
	"SEQUENCE" NUMBER
   ) 
--------------------------------------------------------
--  Constraints for Table JFS_PROCESS_TYPE
--------------------------------------------------------

  ALTER TABLE "APP_JFS"."JFS_PROCESS_TYPE" MODIFY ("CODE" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."JFS_PROCESS_TYPE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."JFS_PROCESS_TYPE" MODIFY ("IS_DELETE" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."JFS_PROCESS_TYPE" ADD CONSTRAINT "JFS_PROCESS_TYPE_PK" PRIMARY KEY ("CODE")
 
