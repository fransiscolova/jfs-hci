--------------------------------------------------------
--  DDL for Table JFS_PRODUCT_MAPPING
--------------------------------------------------------

  CREATE TABLE "JFS_PRODUCT_MAPPING" 
   (	"CODE" NVARCHAR2(20), 
	"NAME" NVARCHAR2(100), 
	"AGREEMENT_ID" NVARCHAR2(10), 
	"DTIME_INSERTED" DATE DEFAULT sysdate, 
	"VALID_FROM" DATE, 
	"VALID_TO" DATE
   ) ;
--------------------------------------------------------
--  Constraints for Table JFS_PRODUCT_MAPPING
--------------------------------------------------------

  ALTER TABLE "JFS_PRODUCT_MAPPING" MODIFY ("VALID_TO" NOT NULL ENABLE);
  ALTER TABLE "JFS_PRODUCT_MAPPING" MODIFY ("VALID_FROM" NOT NULL ENABLE);
  ALTER TABLE "JFS_PRODUCT_MAPPING" MODIFY ("AGREEMENT_ID" NOT NULL ENABLE);
  ALTER TABLE "JFS_PRODUCT_MAPPING" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "JFS_PRODUCT_MAPPING" MODIFY ("CODE" NOT NULL ENABLE);