--------------------------------------------------------
--  DDL for Table MST_ROLES
--------------------------------------------------------

  CREATE TABLE "APP_JFS"."MST_ROLES" 
   (	"USERNAME" VARCHAR2(50 BYTE), 
	"ROLE" VARCHAR2(50 BYTE), 
	"EXTRA_1" VARCHAR2(225 BYTE), 
	"EXTRA_2" VARCHAR2(225 BYTE), 
	"EXTRA_3" VARCHAR2(225 BYTE), 
	"ID" RAW(100) DEFAULT SYS_GUID()
   );
--------------------------------------------------------
--  Constraints for Table MST_ROLES
--------------------------------------------------------

  ALTER TABLE "APP_JFS"."MST_ROLES" ADD CONSTRAINT "MST_ROLES_PK" PRIMARY KEY ("ID")
  ALTER TABLE "APP_JFS"."MST_ROLES" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."MST_ROLES" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "APP_JFS"."MST_ROLES" MODIFY ("ROLE" NOT NULL ENABLE);
