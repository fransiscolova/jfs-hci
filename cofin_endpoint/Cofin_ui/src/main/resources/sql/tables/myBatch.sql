--------------------------------------------------------
--  DDL for Table MY_BATCH
--------------------------------------------------------

  CREATE TABLE "MY_BATCH" 
   (	"JOB_ID" NVARCHAR2(200), 
	"JOB_NAME" NVARCHAR2(200), 
	"SERVICE" NVARCHAR2(200), 
	"METHOD" NVARCHAR2(200), 
	"CRON_TIME" NVARCHAR2(200), 
	"PARAMETER" NVARCHAR2(200), 
	"IS_ACTIVE" NVARCHAR2(1)
   ) ;
--------------------------------------------------------
--  Constraints for Table MY_BATCH
--------------------------------------------------------

  ALTER TABLE "MY_BATCH" ADD CONSTRAINT "MY_BATCH_PK" PRIMARY KEY ("JOB_ID");
  ALTER TABLE "MY_BATCH" MODIFY ("CRON_TIME" NOT NULL ENABLE);
  ALTER TABLE "MY_BATCH" MODIFY ("METHOD" NOT NULL ENABLE);
  ALTER TABLE "MY_BATCH" MODIFY ("SERVICE" NOT NULL ENABLE);
  ALTER TABLE "MY_BATCH" MODIFY ("JOB_NAME" NOT NULL ENABLE);
  ALTER TABLE "MY_BATCH" MODIFY ("JOB_ID" NOT NULL ENABLE);