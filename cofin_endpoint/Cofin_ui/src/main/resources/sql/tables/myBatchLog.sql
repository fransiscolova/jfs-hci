--------------------------------------------------------
--  DDL for Table MY_BATCH_LOG
--------------------------------------------------------

  CREATE TABLE "MY_BATCH_LOG" 
   (	"ID" NVARCHAR2(40), 
	"JOB_ID" NVARCHAR2(200), 
	"EXECUTE_BY" NVARCHAR2(200), 
	"EXECUTE_DATE" DATE, 
	"IS_ERROR" NVARCHAR2(1), 
	"ERROR_LOG" NVARCHAR2(200)
   ) ;
--------------------------------------------------------
--  Constraints for Table MY_BATCH_LOG
--------------------------------------------------------

  ALTER TABLE "MY_BATCH_LOG" ADD CONSTRAINT "MY_BATCH_LOG_PK" PRIMARY KEY ("ID") 
  ALTER TABLE "MY_BATCH_LOG" MODIFY ("IS_ERROR" NOT NULL ENABLE);
  ALTER TABLE "MY_BATCH_LOG" MODIFY ("EXECUTE_DATE" NOT NULL ENABLE);
  ALTER TABLE "MY_BATCH_LOG" MODIFY ("EXECUTE_BY" NOT NULL ENABLE);
  ALTER TABLE "MY_BATCH_LOG" MODIFY ("JOB_ID" NOT NULL ENABLE);
  ALTER TABLE "MY_BATCH_LOG" MODIFY ("ID" NOT NULL ENABLE);