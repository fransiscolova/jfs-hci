  --------------------------------------------------------
--  DDL for Table SYSTEM_PARAMETER
--------------------------------------------------------

  CREATE TABLE "SYSTEM_PARAMETER" 
   (	"ID" VARCHAR2(200 CHAR), 
	"NAME" VARCHAR2(200 CHAR), 
	"VALUE" VARCHAR2(1000 CHAR), 
	"DESCRIPTION" VARCHAR2(200 CHAR)
   ) ;
--------------------------------------------------------
--  Constraints for Table SYSTEM_PARAMETER
--------------------------------------------------------

  ALTER TABLE "SYSTEM_PARAMETER" ADD CONSTRAINT "SYSTEM_PARAMETER_PK" PRIMARY KEY ("ID"); 
  ALTER TABLE "SYSTEM_PARAMETER" MODIFY ("ID" NOT NULL ENABLE);
