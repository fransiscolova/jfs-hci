package com.hci.batch;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.hci.dao.impl.ContractDAOImpl;
import com.hci.dao.impl.DailyCacheDAOImpl;
import com.hci.dao.impl.PartnerDAOImpl;
import com.hci.dao.impl.PaymentAllocationDAOImpl;
import com.hci.dao.impl.PaymentIntDAOImpl;
import com.hci.model.Contract;
import com.hci.model.DailyCache;
import com.hci.model.Partner;
import com.hci.util.ApplicationProperties;
import com.hci.util.EmailEngine;

/**
 * {@link CacheBatch} provided cache initialization, recalculation, etc for splitting
 * {@link Contract} validation.
 *
 * @author muhammad.muflihun
 *
 */
public class CacheBatch implements ServletContextListener {
    private static final Logger LOG = LogManager.getLogger(CacheBatch.class);

    private static EmailEngine emailEngine;
    private static BigDecimal permataLoanWarning;
    private static Boolean permataSendEmail = true;
    private static String permataName;
    private static String btpnName;
    private static String statusActive;

    /**
     * Send warning email to user when JFS amount capacity over certain capacity.
     */
    public static void sendWarningEmail(String partnerName, BigDecimal partnerLimit) {
        // if reached its limit, send an email
        if (partnerName.equalsIgnoreCase(permataName)) {
            if (partnerLimit.compareTo(permataLoanWarning) >= 0) {
                if (permataSendEmail) {
                    try {
                        StringWriter htmlMessage = new StringWriter();
                        htmlMessage.append("As per subject.\nThank You");
                        emailEngine
                        .sendEmail(
                                "jfs.cofin@homecredit.co.id",
                                "elsa.destiana@homecredit.co.id",
                                "toni@homecredit.co.id;kwee.ilona@homecredit.co.id",
                                "muhammad.muflihun@homecredit.co.id;adhi.pradipta01@homecredit.co.id;rd.abadi@homecredit.co.id",
                                "JFS Cofin - Permata loan reached 90% of max capacity",
                                htmlMessage, "");
                        LOG.info("Email sent");
                        permataSendEmail = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOG.error("Error sending warning email!");
                        LOG.error(e);
                    }
                }
            } else {
                permataSendEmail = true;
            }
        } else if (partnerName.equalsIgnoreCase(btpnName)) {

        }
    }

    private ApplicationContext appContext;

    private ContractDAOImpl contractDAO;
    private PartnerDAOImpl partnerDAO;
    private PaymentIntDAOImpl paymentIntDAO;
    private PaymentAllocationDAOImpl paymentAllocationDAO;
    private DailyCacheDAOImpl dailyCacheDAO;
    private ApplicationProperties applicationProperties;

    /**
     * Recalculate add {@link Partner} loan limit cache.
     *
     * @param partnerName
     * @param isLoan
     * @param amount
     * @return
     */
    public BigDecimal addPartnerLimitCache(String partnerName, BigDecimal amount) throws Exception {
        return recalculateLimitCacheByPartnerNameAndAmount(partnerName, amount);
    }

    /**
     * Calculate {@link Partner} total loan limit.
     */
    private void calculatePartnerLoanLimitCache(List<Partner> partners) {
        for (Partner partner : partners) {
            String partnerName = partner.getName();
            BigDecimal totalPartnerLoan = contractDAO.getTotalProposedLoanByStatusAndPartnerName(
                    statusActive, partnerName);
            // BigDecimal totalPartnerPayment = paymentIntDAO.getTotalPaymentByStatusAndPartnerName(
            // statusActive, partnerName);
            // BigDecimal partnerLimit = totalPartnerLoan.subtract(totalPartnerPayment);
            BigDecimal totalPartnerPaymentAllocation = paymentAllocationDAO
                    .getSumAmtPrincipalByPartnerNameAndStatus(statusActive, partnerName);
            LOG.debug("Total partner loan " + totalPartnerLoan);
            LOG.debug("Total partner payment allocation " + totalPartnerPaymentAllocation);           
            BigDecimal partnerLimit = totalPartnerLoan.subtract(totalPartnerPaymentAllocation);
            LOG.debug("partner " + partnerName + " has limit " + partnerLimit);
            // loanCaches.put(partnerName, partnerLimit);
            updateDailyCacheData(partnerName, partnerLimit);
            sendWarningEmail(partnerName, partnerLimit);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent paramServletContextEvent) {
    }

    /**
     * Main process to caching {@code limitCache} for all active {@link Partner}.
     */
    @Override
    public void contextInitialized(ServletContextEvent paramServletContextEvent) {
        LOG.info("masuk cache batch!");
        initializeSpringBean();
        List<Partner> partners = partnerDAO.getAllActivePartner();
        calculatePartnerLoanLimitCache(partners);
    }

    /**
     * Get cache from DB.
     *
     * @param partnerName
     * @return dailyCache
     */
    private DailyCache getDailyCacheData(String partnerName) {
        return dailyCacheDAO.getDailyCacheByPartnerName(partnerName);
    }

    /**
     * Get {@link Partner} limit.
     *
     * @param partnerName
     * @param amount
     * @throws exeption
     * @return
     */
    public BigDecimal getLimitCacheByPartnerName(String partnerName) throws Exception {
        DailyCache dailyCache = getDailyCacheData(partnerName);
        return dailyCache.getTotalLoan();
    }

    /**
     * Initialize application properties from file to be used.
     */
    private void initializeApplicationProperties() {
        permataLoanWarning = new BigDecimal(
                applicationProperties.getProperties("permata.loan.warning"));
        permataName = applicationProperties.getProperties("permata.name");
        btpnName = applicationProperties.getProperties("btpn.name");
        statusActive = applicationProperties.getProperties("status.active");
    }

    /**
     * Initialize spring beans here.
     */
    private void initializeSpringBean() {
        appContext = SpringContextHolder.getApplicationContext();
        partnerDAO = (PartnerDAOImpl) appContext.getBean("partnerDAO");
        contractDAO = (ContractDAOImpl) appContext.getBean("contractDAO");
        paymentIntDAO = (PaymentIntDAOImpl) appContext.getBean("paymentIntDAO");
        paymentAllocationDAO = (PaymentAllocationDAOImpl) appContext
                .getBean("paymentAllocationDAO");
        dailyCacheDAO = (DailyCacheDAOImpl) appContext.getBean("dailyCacheDAO");
        emailEngine = (EmailEngine) appContext.getBean("emailEngine");
        applicationProperties = (ApplicationProperties) appContext.getBean("applicationProperties");
        initializeApplicationProperties();
    }

    /**
     * Recalculate {@link Partner} limit without save the data to cache.
     *
     * @param partnerName
     * @param amount
     * @throws exeption
     * @return
     */
    private BigDecimal recalculateLimitCacheByPartnerNameAndAmount(String partnerName,
            BigDecimal amount) throws Exception {
        BigDecimal partnerLimit = getLimitCacheByPartnerName(partnerName).add(amount);
        LOG.debug("limit after " + partnerLimit);
        return partnerLimit;
    }

    /**
     * Save current {@link Partner} limit cache.
     *
     * @param partnerName
     * @return
     */
    public BigDecimal savePartnerLimitCache(String partnerName, BigDecimal amount) throws Exception {
        BigDecimal partnerLimit = getLimitCacheByPartnerName(partnerName).add(amount);
        LOG.debug("limit " + partnerName + " after " + partnerLimit);
        updateDailyCacheData(partnerName, partnerLimit);
        return partnerLimit;
    }

    /**
     * Recalculate subtract {@link Partner} loan limit cache.
     *
     * @param partnerName
     * @param isLoan
     * @param amount
     * @return
     */
    public BigDecimal subtractPartnerLimitCache(String partnerName, BigDecimal amount)
            throws Exception {
        BigDecimal minus = BigDecimal.ONE.negate();
        return recalculateLimitCacheByPartnerNameAndAmount(partnerName, amount.multiply(minus));
    }

    /**
     * Public method to be called by scheduler thread based on {@code MyBatch} table.
     */
    public void synchronizeLoanCache() {
        LOG.info("Starting sync");
        voidDailyLimit();
        contextInitialized(null);
        LOG.info("Sync is finished!");
    }

    /**
     * Update partner cache data to DB.
     *
     * @param partnerName
     * @param partnerLimit
     */
    private void updateDailyCacheData(String partnerName, BigDecimal partnerLimit) {
        DailyCache dailyCache = getDailyCacheData(partnerName);
        dailyCache.setTotalLoan(partnerLimit);
        dailyCacheDAO.update(dailyCache);
    }

    /**
     * Private method to reset all daily loan for each partner to zero.
     */
    private void voidDailyLimit() {
        initializeSpringBean();
        List<Partner> partners = partnerDAO.getAllActivePartner();
        for (Partner partner : partners) {
            DailyCache dailyCache = dailyCacheDAO.getDailyCacheByPartnerName(partner.getName());
            dailyCache.setDailyLoan(BigDecimal.ZERO);
            dailyCacheDAO.saveOrUpdate(dailyCache);
        }
    }
}
