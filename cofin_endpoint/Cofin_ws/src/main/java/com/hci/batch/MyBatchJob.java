package com.hci.batch;

import java.lang.reflect.Method;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.hci.dao.impl.MyBatchLogDAOImpl;
import com.hci.model.MyBatchLog;

public class MyBatchJob implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            ApplicationContext applicationContext = SpringContextHolder.getApplicationContext();
            MyBatchLogDAOImpl batchLogDAO = (MyBatchLogDAOImpl) applicationContext
                    .getBean("myBatchLogDAO");

            JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();

            MyBatchLog myBatchLog = new MyBatchLog();
            myBatchLog.setJobId((String) dataMap.get("jobId"));
            myBatchLog.setExecuteBy("SYSTEM");
            myBatchLog.setExecuteDate(new Date());
            myBatchLog.setIsError("P");
            batchLogDAO.save(myBatchLog);

            try {
                Object o = applicationContext.getBean((String) dataMap.get("service"));
                Method m = o.getClass().getDeclaredMethod((String) dataMap.get("method"),
                        new Class[] {});
                m.invoke(o);

                myBatchLog.setIsError("N");
                batchLogDAO.update(myBatchLog);

            } catch (Exception e) {
                e.printStackTrace();
                myBatchLog.setIsError("Y");
                myBatchLog.setErrorLog(e.getMessage());
                batchLogDAO.update(myBatchLog);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
