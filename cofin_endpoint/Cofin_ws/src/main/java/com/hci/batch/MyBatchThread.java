package com.hci.batch;

import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.context.ApplicationContext;

import com.hci.dao.impl.MyBatchDAOImpl;
import com.hci.model.MyBatch;

public class MyBatchThread implements ServletContextListener {
    private static final String SYNC_LOAN_JOB_NAME = "BATCH_DAILY_SYNCHRONIZE_LOAN_CACHE";
    private ApplicationContext appContext;
    private MyBatchDAOImpl myBatchDAO;

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        try {
            System.out.println("masuk Batch Thread ws");
            appContext = SpringContextHolder.getApplicationContext();
            myBatchDAO = (MyBatchDAOImpl) appContext.getBean("myBatchDAO");

            List<MyBatch> batchList = myBatchDAO.getAllMyBatch();
            for (MyBatch batch : batchList) {
                if (!batch.getJobId().equalsIgnoreCase(SYNC_LOAN_JOB_NAME)) {
                    continue;
                }

                JobDetail job = new JobDetail();
                job.setName(batch.getJobName());
                job.setJobClass(MyBatchJob.class);

                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("jobId", batch.getJobId());
                map.put("service", batch.getService());
                map.put("method", batch.getMethod());

                JobDataMap jobDataMap = new JobDataMap(map);
                job.setJobDataMap(jobDataMap);

                CronTrigger trigger = new CronTrigger();
                trigger.setName(batch.getJobName().concat(" ").concat("Trigger"));
                trigger.setCronExpression(batch.getCronTime());

                Scheduler scheduler = new StdSchedulerFactory().getScheduler();
                scheduler.start();
                scheduler.scheduleJob(job, trigger);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
