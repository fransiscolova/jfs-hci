package com.hci.dao;

import java.util.List;

import com.hci.model.Agreement;

public interface AgreementDAO {
    public Agreement getActiveAgreementByCode(String agreementCode) throws Exception;
    public List<Agreement> getActiveAgreement() throws Exception;
    public List<Agreement> getAllActiveAgreementByPartnerName(String partnerName) throws Exception;
    public List<String> getActiveAgreementCode() throws Exception;
}
