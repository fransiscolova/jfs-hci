package com.hci.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("unchecked")
@Transactional
public class BaseDAO extends HibernateDaoSupport {

	public Session getCurrentSession() {
		return getHibernateTemplate().getSessionFactory().getCurrentSession();
	}

	public <T> T save(final T o){
		return (T) getCurrentSession().save(o);
	}
	
	public void commit(){
		getCurrentSession().getTransaction().commit();
	}

	public void update(final Object o){
		getCurrentSession().update(o);
	}

	public void delete(final Object o){
		getCurrentSession().delete(o);
	}

	/***/
	public <T> T get(final Class<T> type, final Long id){
		return (T) getCurrentSession().get(type, id);
	}

	/***/
	public <T> T merge(final T o)   {
		return (T) getCurrentSession().merge(o);
	}

	/***/
	public <T> void saveOrUpdate(final T o){
		getCurrentSession().saveOrUpdate(o);
	}

	/**
	 * @param class type
	 * @return all data from database base on class type
	 */
	public <T> List<T> getAll(final Class<T> type) {
		final Session session = getCurrentSession();
		final Criteria crit = session.createCriteria(type);
		return crit.list();
	}

	/**
	 * @param class type 
	 * @param List of Map -- > Map with key & value as parameter
	 * @param Example get Contract by id
	 * 		  	Map<String,Object> param = new HashMap();
	 * 		  	param.put("key","contractId");
	 * 			param.put("value","12345);
	 * 			List<Map<String,Object>> list = new ArrayList();
	 * 			list.add(param);
	 * @param Using only for equal value, not for like value
	 * @return all data from database base on class type
	 */
	public <T> List<T> getAllByParam(final Class<T> type, final List<Map<String,Object>> param) {
		final Session session = getCurrentSession();
		final Criteria crit = session.createCriteria(type);
		for (Map<String, Object> map : param) {
			crit.add(Restrictions.eq((String) map.get("key"), map.get("value")));
		}
		return crit.list();
	}
	
	public <T> void saveByBatch(final List<T> o){
		for (T t : o) {
			save(t);
		}
	}

	public <T> void saveOrUpdateByBatch(final List<T> o){
		for (T t : o) {
			getCurrentSession().saveOrUpdate(t);
		}
	}

	public <T> void updateByBatch(final List<T> o){
		for (T t : o) {
			update(t);
		}
	}
}
