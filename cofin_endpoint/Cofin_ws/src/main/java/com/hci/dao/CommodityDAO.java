package com.hci.dao;

import com.hci.model.Commodity;

public interface CommodityDAO {
	
	/**
	 * Validate active IMEI.
	 * 
	 * @param imei
	 * @return
	 */
	public Commodity valdiateImei(String imei); 
}
