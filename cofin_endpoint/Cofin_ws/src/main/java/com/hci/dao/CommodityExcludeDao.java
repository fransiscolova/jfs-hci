package com.hci.dao;

import java.util.List;

import com.hci.model.Commodity;
import com.hci.model.ComodityExclude;

public interface CommodityExcludeDao {
	
	/**
	 * Validate exlude commodity
	 * 
	 * @param partnerName
	 * @return
	 */
	public List<ComodityExclude> getExcludeComodity(String partnerName,String code); 
	
	
}
