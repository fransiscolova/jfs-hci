package com.hci.dao;

import java.math.BigDecimal;

import com.hci.model.Contract;
import com.hci.model.Partner;

public interface ContractDAO {
    public Contract getContractByContractCode(String contractCode) throws Exception;

    /**
     * Will return sum of {@code SEND_PRINCIPAL} amount from {@link Contract} by its {@code STATUS}
     * and {@link Partner} {@code CUID}.
     *
     * @param status
     * @param cuid
     * @return
     */
    public BigDecimal getTotalProposedLoanByStatusAndCuid(String status, String cuid);

    /**
     * Will return sum of {@code SEND_PRINCIPAL} amount from {@link Contract} by its {@code STATUS}
     * and {@link Partner} {@code NAME}.
     *
     * @param status
     * @param partnerName
     * @return
     */
    public BigDecimal getTotalProposedLoanByStatusAndPartnerName(String status, String partnerName);
}
