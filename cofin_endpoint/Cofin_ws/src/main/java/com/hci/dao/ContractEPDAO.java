package com.hci.dao;

import com.hci.model.ContractEP;

public interface ContractEPDAO {

    public ContractEP getContractByContractCode(String contractCode) throws Exception;
}
