/**
 *
 */
package com.hci.dao;

import com.hci.model.DailyCache;

/**
 * @author muhammad.muflihun
 *
 */
public interface DailyCacheDAO {

    public DailyCache getDailyCacheByPartnerName(String partnerName);

    public DailyCache updateDailyCache(DailyCache dailyCache);
}
