/**
 *
 */
package com.hci.dao;

import java.util.List;

import com.hci.model.MyBatch;

/**
 * @author muhammad.muflihun
 *
 */
public interface MyBatchDAO {

    public List<MyBatch> getAllMyBatch();

    public MyBatch getMyBatchById(String jobId);
}
