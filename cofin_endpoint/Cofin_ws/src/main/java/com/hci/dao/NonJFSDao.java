package com.hci.dao;

import java.util.List;

import com.hci.model.Commodity;
import com.hci.model.ComodityExclude;
import com.hci.model.NonJFS;

public interface NonJFSDao {
	
	/**
	 * Validate non-jfs contract exist
	 * 
	 * @param partnerName,contractCode
	 * @return
	 */
	public List<NonJFS> isContractExist (String partnerName,String Contractcode); 
		
	
	
	
}
