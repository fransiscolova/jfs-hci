/**
 *
 */
package com.hci.dao;

import java.math.BigDecimal;

import com.hci.model.Partner;

/**
 * @author muhammad.muflihun
 *
 */
public interface OwnerDwhFContractAdDAO {

    /**
     * Calculate total {@code SEND_PRINCIPAL} for specified {@link Partner} which DPD is over
     * certain days.
     *
     * @param partnerName
     * @param dpdDays
     * @return
     */
    public BigDecimal calculateTotalLoanByPartnerNameAndDPDLimit(String partnerName, Integer dpdDays);
}
