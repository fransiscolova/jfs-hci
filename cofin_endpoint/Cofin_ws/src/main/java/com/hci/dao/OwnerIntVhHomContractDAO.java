package com.hci.dao;

import java.math.BigDecimal;

import com.hci.model.Partner;

/**
 * @author muhammad.muflihun
 *
 */
public interface OwnerIntVhHomContractDAO {

    /**
     * Get total loan from {@link Partner} for specified {@code HCI_COMMODITY_CODE}(s).
     *
     * @param partner
     * @param commodityCode
     * @return
     */
    public BigDecimal getPartnerTotalLoanByPartnerNameAndHciCommodityCode(String partnerName,
            String... commodityCode);
}
