/**
 *
 */
package com.hci.dao;

import java.util.Date;

import com.hci.model.PaperlessPos;

/**
 * Dao interface for {@link PaperlessPos}.
 *
 * @author muhammad.muflihun
 *
 */
public interface PaperlessPosDAO {

    /**
     * Get paperless pos by salesroom code, product code, and date.
     *
     * @param salesroomCode
     * @param productCode
     * @param date
     * @return paperlessPos
     */
    public PaperlessPos getPaperlessPosBySalesroomCodeAndProductCode(String salesroomCode,
            String productCode, Date date);
}
