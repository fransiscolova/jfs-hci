package com.hci.dao;

import java.util.List;

import com.hci.model.Partner;

public interface PartnerDAO {
    /**
     * Get all active {@link Partner} where {@code IS_DELETE} is 'N'.
     *
     * @return
     */
    public List<Partner> getAllActivePartner();

    /**
     * Get all active {@link Partner} where {@code IS_DELETE} is 'N',
     * {@code IS_CHECKING_ELIGIBILITY} is 'Y' and ordered by priority.
     *
     * @return
     * @throws Exception
     */
    public List<Partner> getAllActivePartnerOrderedByPriority() throws Exception;

    public Partner getPartnerByCode(String partnerCode) throws Exception;
}
