package com.hci.dao;

import java.math.BigDecimal;

import com.hci.model.Partner;

/**
 * Payment allocation DAO interface.
 *
 * @author muhammad.muflihun
 *
 */
public interface PaymentAllocationDAO {

    /**
     * Get sum of {@code AMTPRINCIPAL} by {@link Partner} {@code NAME} and {@code Contract}
     * {@code STATUS}.
     *
     * @param partnerName
     * @param status
     * @return
     */
    public BigDecimal getSumAmtPrincipalByPartnerNameAndStatus(String partnerName, String status);
}
