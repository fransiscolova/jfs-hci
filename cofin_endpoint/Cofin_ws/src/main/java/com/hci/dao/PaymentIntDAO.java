/**
 *
 */
package com.hci.dao;

import java.math.BigDecimal;

import com.hci.model.Partner;
import com.hci.model.PaymentInt;

/**
 * @author muhammad.muflihun
 *
 */
public interface PaymentIntDAO {
    /**
     * Will return sum of {@code PMT_INSTALMENT} amount from {@link PaymentInt} by its
     * {@code STATUS} and {@link Partner} {@code NAME}.
     *
     * @param status
     * @param partnerName
     * @return
     */
    public BigDecimal getTotalPaymentByStatusAndPartnerName(String status, String partnerName);
}
