package com.hci.dao;

import java.math.BigDecimal;

import com.hci.model.Product;

public interface ProductDAO {
	public Product getProductByCode(String productCode) throws Exception;
	public Product getActiveProductByAgreementIdAndBankProductCode(BigDecimal agreementId, String productCode) throws Exception;
}
