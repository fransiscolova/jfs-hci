package com.hci.dao;

import com.hci.model.ProductHci;

public interface ProductHciDAO {
    // public ProductHci getProductHciByCode(String productCode, String idAgreement) throws
    // Exception;
    public ProductHci getProductHciByCode(String productCode) throws Exception;
}
