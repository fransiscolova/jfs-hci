package com.hci.dao;

import java.util.List;

import com.hci.model.ProductMapping;

public interface ProductMappingDAO {

    /**
     * Get all active {@link ProductMapping} by product code and bank product code.
     *
     * @param productCode
     * @param bankProductCode
     * @return
     * @throws Exception
     */
    public ProductMapping getActiveMappingByProductCodeAndBankProductCode(String productCode,
            String bankProductCode) throws Exception;

    /**
     * Get all active {@link ProductMapping} entity by its {@code product code}.
     *
     * @param productCode
     * @return
     * @throws Exception
     */
    public List<ProductMapping> getAllActiveProductMappingByProductCode(String productCode)
            throws Exception;
}
