package com.hci.dao;

import java.util.List;

import com.hci.model.Tac;

public interface TacDAO {
    /**
     * Get all TAC by TAC number.
     *
     * @param tacNumber
     * @return
     */
    public List<Tac> getTacByTacNumber(String tacNumber);

    /**
     * Find TAC by TAC number and today's date.
     *
     * @param tacNumber
     * @return
     */
    public Tac getTacByTacNumberAndTodaysDate(String tacNumber);
}
