package com.hci.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.AgreementDAO;
import com.hci.dao.BaseDAO;
import com.hci.model.Agreement;

public class AgreementDAOImpl extends BaseDAO implements AgreementDAO {
    private static final Logger LOG = LogManager.getLogger(AgreementDAOImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<Agreement> getActiveAgreement() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(Agreement.class);
        Date today = new Date();
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        return criteria.list();
    }

    @Override
    public Agreement getActiveAgreementByCode(String agreementCode) throws Exception {
        Date today = new Date();
        LOG.debug("get agreement by code " + agreementCode + " and date " + today);
        Criteria criteria = getCurrentSession().createCriteria(Agreement.class);
        criteria.add(Restrictions.eq("code", agreementCode));
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        return (Agreement) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getActiveAgreementCode() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(Agreement.class);
        Date today = new Date();
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        criteria.setProjection(Projections.property("code"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Agreement> getAllActiveAgreementByPartnerName(String partnerName) throws Exception {
        LOG.debug("get all agreement by partner name " + partnerName);
        Criteria criteria = getCurrentSession().createCriteria(Agreement.class);
        Date today = new Date();
        criteria.createAlias("partner", "partner");
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        criteria.add(Restrictions.eq("partner.name", partnerName));
        return criteria.list();
    }
}
