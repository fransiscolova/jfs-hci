package com.hci.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.CommodityDAO;
import com.hci.model.Commodity;

public class CommodityDAOImpl extends BaseDAO implements CommodityDAO {

    @Override
    public Commodity valdiateImei(String imei) {
        Criteria criteria = getCurrentSession().createCriteria(Commodity.class);
        criteria.add(Restrictions.eq("imei", imei));
        return (Commodity) criteria.uniqueResult();
    }
}
