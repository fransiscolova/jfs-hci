package com.hci.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.CommodityDAO;
import com.hci.dao.CommodityExcludeDao;
import com.hci.model.Commodity;
import com.hci.model.ComodityExclude;

public class CommodityExcludeDAOImpl extends BaseDAO implements CommodityExcludeDao {

   

	@Override
	public List<ComodityExclude> getExcludeComodity(String partnerName,String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getCurrentSession().createCriteria(ComodityExclude.class);
        criteria.add(Restrictions.eq("partnerName", partnerName));
        criteria.add(Restrictions.eq("commodityCategoryCode", code));
        criteria.add(Restrictions.eq("isActive", "Y"));
        
        return  criteria.list();
	}
}
