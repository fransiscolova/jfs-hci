package com.hci.dao.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.ContractDAO;
import com.hci.model.Contract;

public class ContractDAOImpl extends BaseDAO implements ContractDAO {
    private static final Logger LOG = LogManager.getLogger(ContractDAOImpl.class);

    public Contract getContractByContractCode(String contractCode) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(Contract.class);
        criteria.add(Restrictions.eq("textContractNumber", contractCode));
        return (Contract) criteria.uniqueResult();
    }

    @Override
    public BigDecimal getTotalProposedLoanByStatusAndCuid(String status, String cuid) {
        LOG.debug("get sum proposed loan by status " + status + " and cuid " + cuid);
        Criteria criteria = getCurrentSession().createCriteria(Contract.class);
        criteria.add(Restrictions.eq("cuid", cuid));
        criteria.add(Restrictions.eq("status", status));
        criteria.setProjection(Projections.sum("sendPrincipal"));
        BigDecimal total = (BigDecimal) criteria.uniqueResult();
        return total == null ? BigDecimal.ZERO : total;
    }

    @Override
    public BigDecimal getTotalProposedLoanByStatusAndPartnerName(String status, String partnerName) {
        LOG.debug("get sum proposed loan by status " + status + " and partner name " + partnerName);
        Criteria criteria = getCurrentSession().createCriteria(Contract.class);
        criteria.createAlias("idAgreement", "agreement");
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("partner.name", partnerName));
        criteria.add(Restrictions.eq("status", status));
        criteria.setProjection(Projections.sum("sendPrincipal"));
        BigDecimal total = (BigDecimal) criteria.uniqueResult();
        return total == null ? BigDecimal.ZERO : total;
    }

}
