/**
 *
 */
package com.hci.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.ContractEPDAO;
import com.hci.model.ContractEP;

/**
 * @author muhammad.muflihun
 *
 */
public class ContractEPDAOImpl extends BaseDAO implements ContractEPDAO {

    @Override
    public ContractEP getContractByContractCode(String contractCode) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ContractEP.class);
        criteria.add(Restrictions.eq("textContractNumber", contractCode));
        return (ContractEP) criteria.uniqueResult();
    }
}
