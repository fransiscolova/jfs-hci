package com.hci.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.DailyCacheDAO;
import com.hci.model.DailyCache;

public class DailyCacheDAOImpl extends BaseDAO implements DailyCacheDAO {
    private static final Logger LOG = LogManager.getLogger(DailyCacheDAOImpl.class);

    @Override
    public DailyCache getDailyCacheByPartnerName(String partnerName) {
        LOG.debug("get daily cache by partner name {}", partnerName);
        Criteria criteria = getCurrentSession().createCriteria(DailyCache.class);
        criteria.add(Restrictions.eq("partnerName", partnerName));
        return (DailyCache) criteria.uniqueResult();
    }

    @Override
    public DailyCache updateDailyCache(DailyCache dailyCache) {
        LOG.debug("update daily cache {}", dailyCache);
        saveOrUpdate(dailyCache);
        return dailyCache;
    }

}
