/**
 *
 */
package com.hci.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.MyBatchDAO;
import com.hci.model.MyBatch;

/**
 * @author muhammad.muflihun
 *
 */
public class MyBatchDAOImpl extends BaseDAO implements MyBatchDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<MyBatch> getAllMyBatch() {
        Criteria criteria = getCurrentSession().createCriteria(MyBatch.class);
        criteria.add(Restrictions.eq("isActive", "Y"));
        return criteria.list();
    }

    @Override
    public MyBatch getMyBatchById(String jobId) {
        Criteria criteria = getCurrentSession().createCriteria(MyBatch.class);
        criteria.add(Restrictions.eq("isActive", "Y"));
        criteria.add(Restrictions.eq("jobId", jobId));
        return (MyBatch) criteria.uniqueResult();
    }
}
