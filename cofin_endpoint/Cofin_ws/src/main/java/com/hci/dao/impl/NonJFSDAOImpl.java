package com.hci.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.CommodityDAO;
import com.hci.dao.CommodityExcludeDao;
import com.hci.dao.NonJFSDao;
import com.hci.model.Commodity;
import com.hci.model.ComodityExclude;
import com.hci.model.NonJFS;

public class NonJFSDAOImpl extends BaseDAO implements NonJFSDao {

   

	@Override
	public List<NonJFS> isContractExist(String partnerName, String Contractcode) {
		// TODO Auto-generated method stub
		Criteria criteria = getCurrentSession().createCriteria(NonJFS.class);
        criteria.add(Restrictions.eq("bankPartner", partnerName));
        criteria.add(Restrictions.eq("textContractNumber", Contractcode));
		
		return criteria.list();
	}

	
}
