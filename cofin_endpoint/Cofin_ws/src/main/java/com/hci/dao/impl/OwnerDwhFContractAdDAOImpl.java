/**
 *
 */
package com.hci.dao.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.OwnerDwhFContractAdDAO;
import com.hci.model.OwnerDwhFContractAd;

/**
 * @author muhammad.muflihun
 *
 */
public class OwnerDwhFContractAdDAOImpl extends BaseDAO implements OwnerDwhFContractAdDAO {
    private static final Logger LOG = LogManager.getLogger(OwnerDwhFContractAdDAOImpl.class);

    @Override
    public BigDecimal calculateTotalLoanByPartnerNameAndDPDLimit(String partnerName, Integer dpdDays) {
        LOG.debug("partner " + partnerName + " with params " + dpdDays);
        Criteria criteria = getCurrentSession().createCriteria(OwnerDwhFContractAd.class);
        criteria.createAlias("textContractNumber", "contract");
        criteria.createAlias("contract.idAgreement", "agreement");
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("partner.name", partnerName));
        criteria.add(Restrictions.ge("cntDaysPastDueTolerance", dpdDays));
        criteria.setProjection(Projections.sum("contract.sendPrincipal"));
        BigDecimal result = (BigDecimal) criteria.uniqueResult();
        return result == null ? BigDecimal.ZERO : result;
    }

}