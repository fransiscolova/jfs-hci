/**
 *
 */
package com.hci.dao.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.OwnerIntVhHomContractDAO;
import com.hci.model.OwnerIntVhHomeContract;

/**
 * @author muhammad.muflihun
 *
 */
public class OwnerIntVhHomContractDAOImpl extends BaseDAO implements OwnerIntVhHomContractDAO {
    private static final Logger LOG = LogManager.getLogger(OwnerIntVhHomContractDAOImpl.class);

    @Override
    public BigDecimal getPartnerTotalLoanByPartnerNameAndHciCommodityCode(String partnerName,
            String... commodityCode) {
        LOG.debug("partner " + partnerName + " with params " + commodityCode.toString());
        Criteria criteria = getCurrentSession().createCriteria(OwnerIntVhHomeContract.class);
        criteria.createAlias("id", "vhHomCommodity");
        criteria.createAlias("vhHomCommodity.commodityTypeId", "vhHomcommodityType");
        criteria.createAlias("vhHomcommodityType.commodityCategoryCode", "commodityType");
        criteria.createAlias("contractCode", "contract");
        criteria.createAlias("contract.idAgreement", "agreement");
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("partner.name", partnerName));
        criteria.add(Restrictions.in("commodityType.hciCodeCommodityCategory", commodityCode));
        criteria.setProjection(Projections.sum("contract.sendPrincipal"));
        BigDecimal result = (BigDecimal) criteria.uniqueResult();
        return result == null ? BigDecimal.ZERO : result;
    }

}
