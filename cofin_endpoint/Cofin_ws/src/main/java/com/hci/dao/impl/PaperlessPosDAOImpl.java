/**
 *
 */
package com.hci.dao.impl;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.PaperlessPosDAO;
import com.hci.model.PaperlessPos;

/**
 * Dao implement class for {@link PaperlessPos}.
 *
 * @author muhammad.muflihun
 *
 */
public class PaperlessPosDAOImpl extends BaseDAO implements PaperlessPosDAO {
    private static final Logger LOG = LogManager.getLogger(PaperlessPosDAOImpl.class);

    @Override
    public PaperlessPos getPaperlessPosBySalesroomCodeAndProductCode(String salesroomCode,
            String productCode, Date date) {
        LOG.debug("get paperless pos by salesroom code " + salesroomCode + ", product code "
                + productCode + ", and date " + date);
        Criteria criteria = getCurrentSession().createCriteria(PaperlessPos.class);
        criteria.add(Restrictions.eq("salesroomCode", salesroomCode));
        criteria.add(Restrictions.eq("productCode", productCode));
        criteria.add(Restrictions.lt("validFrom", date));
        criteria.add(Restrictions.gt("validTo", date));
        return (PaperlessPos) criteria.uniqueResult();
    }

}
