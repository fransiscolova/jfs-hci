package com.hci.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.PartnerDAO;
import com.hci.model.Partner;

public class PartnerDAOImpl extends BaseDAO implements PartnerDAO {
    private static final Logger LOG = LogManager.getLogger(PartnerDAOImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<Partner> getAllActivePartner() {
        LOG.debug("get all active partners");
        Criteria criteria = getCurrentSession().createCriteria(Partner.class);
        criteria.add(Restrictions.eq("isDelete", "N"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Partner> getAllActivePartnerOrderedByPriority() throws Exception {
        LOG.debug("get all active partners");
        Criteria criteria = getCurrentSession().createCriteria(Partner.class);
        criteria.add(Restrictions.eq("isDelete", "N"));
        criteria.add(Restrictions.eq("isCheckingEligibility", "Y"));
        criteria.addOrder(Order.asc("priority"));
        return criteria.list();
    }

    @Override
    public Partner getPartnerByCode(String partnerCode) throws Exception {
        LOG.debug("get partner by code " + partnerCode);
        Criteria criteria = getCurrentSession().createCriteria(Partner.class);
        criteria.add(Restrictions.eq("id", partnerCode));
        return (Partner) criteria.uniqueResult();
    }

}
