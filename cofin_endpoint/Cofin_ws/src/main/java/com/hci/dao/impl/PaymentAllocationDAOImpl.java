package com.hci.dao.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.PaymentAllocationDAO;
import com.hci.model.PaymentAllocation;

/**
 * Payment Allocation DAO implement class.
 *
 * @author muhammad.muflihun
 *
 */
public class PaymentAllocationDAOImpl extends BaseDAO implements PaymentAllocationDAO {
    private static final Logger LOG = LogManager.getLogger(PaymentAllocationDAOImpl.class);

    @Override
    public BigDecimal getSumAmtPrincipalByPartnerNameAndStatus(String partnerName, String status) {
        LOG.debug("get sum total payment allocation by status " + status + " and partner name "
                + partnerName);
        Criteria criteria = getCurrentSession().createCriteria(PaymentAllocation.class);
        criteria.createAlias("contract", "contract");
        criteria.createAlias("contract.idAgreement", "agreement");
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("partner.name", partnerName));
        criteria.add(Restrictions.eq("contract.status", status));
        criteria.setProjection(Projections.sum("amtPrincipal"));
        BigDecimal total = (BigDecimal) criteria.uniqueResult();
        return total == null ? BigDecimal.ZERO : total;
    }

}
