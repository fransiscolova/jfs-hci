/**
 *
 */
package com.hci.dao.impl;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.PaymentIntDAO;
import com.hci.model.PaymentInt;

/**
 * @author muhammad.muflihun
 *
 */
public class PaymentIntDAOImpl extends BaseDAO implements PaymentIntDAO {
    private static final Logger LOG = LogManager.getLogger(PaymentIntDAOImpl.class);

    @Override
    public BigDecimal getTotalPaymentByStatusAndPartnerName(String status, String partnerName) {
        LOG.debug("get sum total payment by status " + status + " and partner name " + partnerName);
        Criteria criteria = getCurrentSession().createCriteria(PaymentInt.class);
        criteria.createAlias("textContractNumber", "contract");
        criteria.createAlias("contract.idAgreement", "agreement");
        criteria.createAlias("agreement.partner", "partner");
        criteria.add(Restrictions.eq("partner.name", partnerName));
        criteria.add(Restrictions.eq("status", status));
        criteria.add(Restrictions.eq("contract.status", status));
        criteria.setProjection(Projections.sum("pmtInstalment"));
        BigDecimal total = (BigDecimal) criteria.uniqueResult();
        return total == null ? BigDecimal.ZERO : total;
    }

}
