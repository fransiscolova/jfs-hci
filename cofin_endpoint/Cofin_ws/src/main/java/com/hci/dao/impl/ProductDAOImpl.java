package com.hci.dao.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.ProductDAO;
import com.hci.model.Product;

public class ProductDAOImpl extends BaseDAO implements ProductDAO {
    private static final Logger LOG = LogManager.getLogger(ProductDAOImpl.class);

    public Product getActiveProductByAgreementIdAndBankProductCode(BigDecimal agreementId,
            String bankProductCode) throws Exception {
        Date today = new Date();
        LOG.debug("get active product by agreement id " + agreementId + ", bank product code "
                + bankProductCode + ", and date " + today);
        Criteria criteria = getCurrentSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("bankProductCode", bankProductCode));
        criteria.add(Restrictions.eq("idAgreement", agreementId));
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        return (Product) criteria.uniqueResult();
    }

    public Product getProductByCode(String productCode) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("bankProductCode", productCode));
        return (Product) criteria.uniqueResult();
    }
}