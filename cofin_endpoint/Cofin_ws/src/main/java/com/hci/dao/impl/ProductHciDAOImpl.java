package com.hci.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.ProductHciDAO;
import com.hci.model.ProductHci;

public class ProductHciDAOImpl extends BaseDAO implements ProductHciDAO {
    private static final Logger LOG = LogManager.getLogger(ProductHciDAOImpl.class);

    /*
     * public ProductHci getProductHciByCode(String productCode, String idAgreement) throws
     * Exception { Criteria criteria = getCurrentSession().createCriteria(ProductHci.class);
     * criteria.add(Restrictions.eq("codeProduct", productCode));
     * criteria.add(Restrictions.eq("idAgreement", idAgreement)); return (ProductHci)
     * criteria.uniqueResult(); }
     */

    public ProductHci getProductHciByCode(String productCode) throws Exception {
        LOG.debug("get product by code " + productCode);
        Criteria criteria = getCurrentSession().createCriteria(ProductHci.class);
        criteria.add(Restrictions.eq("codeProduct", productCode));
        return (ProductHci) criteria.uniqueResult();
    }
}