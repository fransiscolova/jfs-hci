package com.hci.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.ProductMappingDAO;
import com.hci.model.ProductMapping;

public class ProductMappingDAOImpl extends BaseDAO implements ProductMappingDAO {
    private static final Logger LOG = LogManager.getLogger(ProductMappingDAOImpl.class);

    public ProductMapping getActiveMappingByProductCodeAndBankProductCode(String productCode,
            String bankProductCode) throws Exception {
        Date today = new Date();
        LOG.debug("get product mapping by product code " + productCode + ", bank product code "
                + bankProductCode + ", and date " + today);
        Criteria criteria = getCurrentSession().createCriteria(ProductMapping.class);
        criteria.add(Restrictions.eq("codeProduct", productCode));
        criteria.add(Restrictions.eq("bankProductCode", bankProductCode));
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        return (ProductMapping) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductMapping> getAllActiveProductMappingByProductCode(String productCode)
            throws Exception {
        Date today = new Date();
        Criteria criteria = getCurrentSession().createCriteria(ProductMapping.class);
        criteria.add(Restrictions.eq("codeProduct", productCode));
        criteria.add(Restrictions.le("validFrom", today));
        criteria.add(Restrictions.ge("validTo", today));
        return criteria.list();
    }
}