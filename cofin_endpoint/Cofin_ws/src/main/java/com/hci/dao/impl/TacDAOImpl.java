package com.hci.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.hci.dao.BaseDAO;
import com.hci.dao.TacDAO;
import com.hci.model.Tac;

public class TacDAOImpl extends BaseDAO implements TacDAO {
    private static final Logger LOG = LogManager.getLogger(TacDAOImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<Tac> getTacByTacNumber(String tacNumber) {
        Criteria criteria = getCurrentSession().createCriteria(Tac.class);
        criteria.add(Restrictions.eq("tac", tacNumber));
        return criteria.list();
    }

    @Override
    public Tac getTacByTacNumberAndTodaysDate(String tacNumber) {
        Date today = new Date();
        LOG.debug("get tac by tac number " + tacNumber + " and date " + today);
        Criteria criteria = getCurrentSession().createCriteria(Tac.class);
        criteria.add(Restrictions.eq("tac", tacNumber));
        criteria.add(Restrictions.ge("validFrom", today));
        criteria.add(Restrictions.le("validTo", today));
        return (Tac) criteria.uniqueResult();
    }

}
