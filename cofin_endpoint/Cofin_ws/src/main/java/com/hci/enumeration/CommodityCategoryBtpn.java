package com.hci.enumeration;

/**
 * Reflect code from entity {@link CommodityCategory} for partner BTPN. <br>
 * Contains minimum & maximum tenor and also minimum & maximum amount for each {@link CommodityCategory}. 
 *
 * @author muhammad.muflihun
 *
 */
public enum CommodityCategoryBtpn {
    CAT_CD("6", "40", "600000", "25000000"), 
    CAT_MP("6", "40", "600000", "25000000"), 
    CAT_CD_FUR("6", "40", "600000", "25000000"), 
    CAT_TV("6", "40", "600000", "25000000"), 
    CAT_LAP("6","40", "600000", "25000000");

    private String tenorMin;
    private String tenorMax;
    private String amountMin;
    private String amountMax;

    CommodityCategoryBtpn(String tenorMin, String tenorMax, String amountMin, String amountMax) {
        this.tenorMin = tenorMin;
        this.tenorMax = tenorMax;
        this.amountMin = amountMin;
        this.amountMax = amountMax;
    }

    public String getAmountMax() {
        return amountMax;
    }

    public String getAmountMin() {
        return amountMin;
    }

    public String getTenorMax() {
        return tenorMax;
    }

    public String getTenorMin() {
        return tenorMin;
    }

}
