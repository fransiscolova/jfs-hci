/**
 *
 */
package com.hci.enumeration;

import com.hci.model.CommodityType;

/**
 * Reflect code from entity {@link CommodityType} for partner BTPN. <br>
 * Contains minimum & maximum tenor and also minimum & maximum amount for each {@link CommodityType}.
 *
 * @author muhammad.muflihun
 *
 */
public enum CommodityTypeBtpn {
    CT_CT_MTAB("6", "40", "600000", "25000000"),
    CT_CT_PDA("6", "40", "600000", "25000000"),
    CAT_GG_SM("6", "40", "600000", "25000000"),
    CD_HA_AS("6", "40", "600000", "25000000"),
    CT_CT_GC("6", "40", "600000", "25000000");

    private String tenorMin;
    private String tenorMax;
    private String amountMin;
    private String amountMax;

    CommodityTypeBtpn(String tenorMin, String tenorMax, String amountMin, String amountMax) {
        this.tenorMin = tenorMin;
        this.tenorMax = tenorMax;
        this.amountMin = amountMin;
        this.amountMax = amountMax;
    }

    public String getAmountMax() {
        return amountMax;
    }

    public String getAmountMin() {
        return amountMin;
    }

    public String getTenorMax() {
        return tenorMax;
    }

    public String getTenorMin() {
        return tenorMin;
    }

}
