/**
 *
 */
package com.hci.enumeration;

/**
 * Service Type code enumeration that will be excluded for BTPN.
 *
 * @author muhammad.muflihun
 *
 */
public enum ServiceTypeCodeBtpn {
    GRPER
    // , GIFTP
}
