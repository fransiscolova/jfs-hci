/**
 *
 */
package com.hci.enumeration;

/**
 * @author muhammad.muflihun
 *
 */
public enum TacExternalCode {
    EM("EM", "IMEI is empty."), ER("ER", "Error."), NF("NF", "TAC not found."), IA("IA",
            "TAC is currently not active."), AC("AC", "TAC is active.");

    private String externalCode;
    private String description;

    TacExternalCode(String externalCode, String description) {
        this.externalCode = externalCode;
        this.description = description;
    }

    public String description() {
        return description;
    }

    public String externalCode() {
        return externalCode;
    }
}
