package com.hci.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_agreement")
public class Agreement implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_AGREEMENT", length = 50)
    private String code;

    @Column(name = "NAME_AGREEMENT", length = 100)
    private String name;

    @Column(name = "CODE_AGREEMENT")
    private String codeAgreement;

    @ManyToOne
    @JoinColumn(name = "partner_id")
    private Partner partner;

    @Column(name = "valid_from")
    private Timestamp validFrom;

    @Column(name = "valid_to")
    private Timestamp validTo;

    @Column(name = "int_rate", precision = 5, scale = 2)
    private BigDecimal interestRate;

    @Column(name = "prin_split_rate", precision = 5, scale = 2)
    private BigDecimal principalSplitRate;

    @Column(name = "adm_fee_rate", precision = 5, scale = 2)
    private BigDecimal adminFeeRate;

    @Column(name = "description", length = 100)
    private String description;

    @Column(name = "is_delete", length = 1)
    private String isDelete;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @Column(name = "UPDATED_DATE")
    private Date updatedDate;

    public BigDecimal getAdminFeeRate() {
        return adminFeeRate;
    }

    public String getCode() {
        return code;
    }

    public String getCodeAgreement() {
        return codeAgreement;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public String getName() {
        return name;
    }

    public Partner getPartner() {
        return partner;
    }

    public BigDecimal getPrincipalSplitRate() {
        return principalSplitRate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public Timestamp getValidFrom() {
        return validFrom;
    }

    public Timestamp getValidTo() {
        return validTo;
    }

    public void setAdminFeeRate(BigDecimal adminFeeRate) {
        this.adminFeeRate = adminFeeRate;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCodeAgreement(String codeAgreement) {
        this.codeAgreement = codeAgreement;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public void setPrincipalSplitRate(BigDecimal principalSplitRate) {
        this.principalSplitRate = principalSplitRate;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public void setValidFrom(Timestamp validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTo(Timestamp validTo) {
        this.validTo = validTo;
    }

    @Override
    public String toString() {
        return "Agreement [code=" + code + ", name=" + name + ", codeAgreement=" + codeAgreement
                + ", partner=" + partner + ", validFrom=" + validFrom + ", validTo=" + validTo
                + ", interestRate=" + interestRate + ", principalSplitRate=" + principalSplitRate
                + ", adminFeeRate=" + adminFeeRate + ", description=" + description + ", isDelete="
                + isDelete + ", createdBy=" + createdBy + ", createdDate=" + createdDate
                + ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate + "]";
    }

}
