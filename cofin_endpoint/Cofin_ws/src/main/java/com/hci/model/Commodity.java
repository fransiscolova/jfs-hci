package com.hci.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "JFS_COMMODITY")
public class Commodity implements Serializable {

    private static final long serialVersionUID = 1L;
    private String imei;
    private String code;

    /**
     * Code flag in two char string. Refers to External Code above.
     */
    @Column(name = "code", length = 2, nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * IMEI data from manufacturer.
     */
    @Id
    @Column(name = "IMEI", length = 255, unique = true, nullable = false)
    public String getImei() {
        return imei;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
