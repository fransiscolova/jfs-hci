/**
 *
 */
package com.hci.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author muhammad.muflihun
 *
 */

@Entity
@Table(name = "JFS_COMMODITY_TYPE")
public class CommodityType {
    private String hciCodeCommodityCategory;
    private String btpnCodeCommodityCategory;
    private String hciCodeCommodityType;
    private String btpnCodeCommodityType;
    private String description;
    private String id;
    private Date createdDate;
    private String createdBy;
    private Date updatedDate;
    private String updatedBy;
    private String isDelete;

    @Column(name = "BTPN_CODE_COMMODITY_CATEGORY")
    public String getBtpnCodeCommodityCategory() {
        return btpnCodeCommodityCategory;
    }

    @Column(name = "BTPN_CODE_COMMODITY_TYPE")
    public String getBtpnCodeCommodityType() {
        return btpnCodeCommodityType;
    }

    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    @Column(name = "CREATED_DATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    @Column(name = "HCI_CODE_COMMODITY_CATEGORY")
    public String getHciCodeCommodityCategory() {
        return hciCodeCommodityCategory;
    }

    @Column(name = "HCI_CODE_COMMODITY_TYPE")
    public String getHciCodeCommodityType() {
        return hciCodeCommodityType;
    }

    @Id
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    @Column(name = "IS_DELETE")
    public String getIsDelete() {
        return isDelete;
    }

    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Column(name = "UPDATED_DATE")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setBtpnCodeCommodityCategory(String btpnCodeCommodityCategory) {
        this.btpnCodeCommodityCategory = btpnCodeCommodityCategory;
    }

    public void setBtpnCodeCommodityType(String btpnCodeCommodityType) {
        this.btpnCodeCommodityType = btpnCodeCommodityType;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHciCodeCommodityCategory(String hciCodeCommodityCategory) {
        this.hciCodeCommodityCategory = hciCodeCommodityCategory;
    }

    public void setHciCodeCommodityType(String hciCodeCommodityType) {
        this.hciCodeCommodityType = hciCodeCommodityType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "CommodityType [hciCodeCommodityCategory=" + hciCodeCommodityCategory
                + ", btpnCodeCommodityCategory=" + btpnCodeCommodityCategory
                + ", hciCodeCommodityType=" + hciCodeCommodityType + ", btpnCodeCommodityType="
                + btpnCodeCommodityType + ", description=" + description + ", id=" + id
                + ", createdDate=" + createdDate + ", createdBy=" + createdBy + ", updatedDate="
                + updatedDate + ", updatedBy=" + updatedBy + ", isDelete=" + isDelete + "]";
    }

}
