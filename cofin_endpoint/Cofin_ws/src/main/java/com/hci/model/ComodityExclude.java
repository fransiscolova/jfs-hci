package com.hci.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_exclude_commodity_category")
public class ComodityExclude implements Serializable {


    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "partner_name")
    private String partnerName;
    
    @Column(name = "commodity_category_code")
    private String commodityCategoryCode;

    @Column(name = "request_date")
    private Date codeAgreement;

    @Column(name = "valid_from")
    private Timestamp validFrom;

    @Column(name = "valid_to")
    private Timestamp validTo;

    @Column(name = "isactive")
    private String isActive;

	public String getId() {
		return id;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public String getCommodityCategoryCode() {
		return commodityCategoryCode;
	}

	public Date getCodeAgreement() {
		return codeAgreement;
	}

	public Timestamp getValidFrom() {
		return validFrom;
	}

	public Timestamp getValidTo() {
		return validTo;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public void setCommodityCategoryCode(String commodityCategoryCode) {
		this.commodityCategoryCode = commodityCategoryCode;
	}

	public void setCodeAgreement(Date codeAgreement) {
		this.codeAgreement = codeAgreement;
	}

	public void setValidFrom(Timestamp validFrom) {
		this.validFrom = validFrom;
	}

	public void setValidTo(Timestamp validTo) {
		this.validTo = validTo;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

    

}
