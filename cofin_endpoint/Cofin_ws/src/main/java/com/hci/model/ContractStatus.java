package com.hci.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_contract_status")
public class ContractStatus implements Serializable {
    private static final long serialVersionUID = 2526609872839502954L;

    @Id
    @Column(name = "TEXT_CONTRACT_NUMBER")
    private String textContractNumber;

    @Id
    @Column(name = "DTIME_CREATED")
    private Date dtimeCreated;

    @Column(name = "CNT_DPD")
    private BigDecimal cntDpd;

    @Column(name = "CODE_STATUS")
    private String codeStatus;

    @Column(name = "DTIME_BANK_PROCESS")
    private Date dtimeBankProcess;

    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @Column(name = "DTIME_UPDATED")
    private Date dtimeUpdated;

    @Column(name = "CREATED_BY")
    private String createdBy;

    // TODO : open this
    // @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "FILENAME")
    private String filename;

    @Column(name = "DATE_UPLOAD")
    private Date dateUpload;

    public BigDecimal getCntDpd() {
        return cntDpd;
    }

    public String getCodeStatus() {
        return codeStatus;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getDateUpload() {
        return dateUpload;
    }

    public Date getDtimeBankProcess() {
        return dtimeBankProcess;
    }

    public Date getDtimeCreated() {
        return dtimeCreated;
    }

    public Date getDtimeUpdated() {
        return dtimeUpdated;
    }

    public String getFilename() {
        return filename;
    }

    public String getId() {
        return id;
    }

    public String getTextContractNumber() {
        return textContractNumber;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setCntDpd(BigDecimal cntDpd) {
        this.cntDpd = cntDpd;
    }

    public void setCodeStatus(String codeStatus) {
        this.codeStatus = codeStatus;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setDateUpload(Date dateUpload) {
        this.dateUpload = dateUpload;
    }

    public void setDtimeBankProcess(Date dtimeBankProcess) {
        this.dtimeBankProcess = dtimeBankProcess;
    }

    public void setDtimeCreated(Date dtimeCreated) {
        this.dtimeCreated = dtimeCreated;
    }

    public void setDtimeUpdated(Date dtimeUpdated) {
        this.dtimeUpdated = dtimeUpdated;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTextContractNumber(String textContractNumber) {
        this.textContractNumber = textContractNumber;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "Contract Status [ textContractNumber=" + textContractNumber + ", dtimeCreated="
                + dtimeCreated + ", cntDpd=" + cntDpd + ", codeStatus=" + codeStatus
                + ", dtimeBankProcess=" + dtimeBankProcess + ", updatedBy=" + updatedBy
                + ", dtimeUpdated=" + dtimeUpdated + ", createdBy=" + createdBy + ", id=" + id
                + ", filename=" + filename + ", dateUpload=" + dateUpload + "]";
    }
}
