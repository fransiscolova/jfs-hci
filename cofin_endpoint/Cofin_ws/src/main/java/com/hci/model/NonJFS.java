package com.hci.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/*
 * @author :Fransisco situmorang
 * 
 * 
 */
		
		
@Entity
@Table(name = "jfs_non_jfs_contract")
public class NonJFS implements Serializable {


  
	@Id
    @Column(name = "TEXT_CONTRACT_NUMBER")
    private String textContractNumber;

    @Column(name = "BANK_PARTNER")
    private String bankPartner;
    
   
    @Column(name = "EXPORT_DATE")
    private Date exportDate;

    @Column(name = "DTIME_CREATED")
    private Date dTimeCreated;

    @Column(name="PRODUCT_CODE")
    private String productCode;

    
    @Column(name="SALESROOM_CODE")
    private String salesRoomCode;
    
    
    @Column(name="CREDIT_AMOUNT")
    private BigDecimal creditAmount;
    
    
    @Column(name="ANNUITY_AMOUNT")
    private BigDecimal annuityAmount;
    
    @Column(name="INSTALLMENT_AMOUNT")
    private BigDecimal installmentAmount;
    
    
    @Column(name="TENOR")
    private int tenor;
    
    @Column(name="FIRST_DUE_DATE")
    private Date fisrtDueDate;

	public String getTextContractNumber() {
		return textContractNumber;
	}

	public void setTextContractNumber(String textContractNumber) {
		this.textContractNumber = textContractNumber;
	}

	public String getBankPartner() {
		return bankPartner;
	}

	public void setBankPartner(String bankPartner) {
		this.bankPartner = bankPartner;
	}

	public Date getExportDate() {
		return exportDate;
	}

	public void setExportDate(Date exportDate) {
		this.exportDate = exportDate;
	}

	public Date getdTimeCreated() {
		return dTimeCreated;
	}

	public void setdTimeCreated(Date dTimeCreated) {
		this.dTimeCreated = dTimeCreated;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getSalesRoomCode() {
		return salesRoomCode;
	}

	public void setSalesRoomCode(String salesRoomCode) {
		this.salesRoomCode = salesRoomCode;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public BigDecimal getAnnuityAmount() {
		return annuityAmount;
	}

	public void setAnnuityAmount(BigDecimal annuityAmount) {
		this.annuityAmount = annuityAmount;
	}

	public BigDecimal getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(BigDecimal installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public int getTenor() {
		return tenor;
	}

	public void setTenor(int tenor) {
		this.tenor = tenor;
	}

	public Date getFisrtDueDate() {
		return fisrtDueDate;
	}

	public void setFisrtDueDate(Date fisrtDueDate) {
		this.fisrtDueDate = fisrtDueDate;
	}


    

}
