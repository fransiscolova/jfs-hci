/**
 *
 */
package com.hci.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity model for view {@code F_CONTRACT_AD} from user {@code OWNER_DWH}.
 * <p>
 * Not all column provided by this entity because the number of columns is too much.
 *
 * @author muhammad.muflihun
 *
 */

@Entity
@Table(schema = "OWNER_DWH", name = "F_CONTRACT_AD")
public class OwnerDwhFContractAd implements Serializable {

    private static final long serialVersionUID = 8407639543204712336L;

    private Contract textContractNumber;
    private Integer cntDaysPastDueTolerance;

    @Column(name = "CNT_DAYS_PAST_DUE_TOLERANCE")
    public Integer getCntDaysPastDueTolerance() {
        return cntDaysPastDueTolerance;
    }

    @Id
    @OneToOne
    @JoinColumn(name = "TEXT_CONTRACT_NUMBER", referencedColumnName = "TEXT_CONTRACT_NUMBER")
    public Contract getTextContractNumber() {
        return textContractNumber;
    }

    public void setCntDaysPastDueTolerance(Integer cntDaysPastDueTolerance) {
        this.cntDaysPastDueTolerance = cntDaysPastDueTolerance;
    }

    public void setTextContractNumber(Contract textContractNumber) {
        this.textContractNumber = textContractNumber;
    }

    @Override
    public String toString() {
        return "OwnerDwhFContractAd [textContractNumber=" + textContractNumber
                + ", cntDaysPastDueTolerance=" + cntDaysPastDueTolerance + "]";
    }

}
