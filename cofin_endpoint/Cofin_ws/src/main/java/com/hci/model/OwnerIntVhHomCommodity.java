/**
 *
 */
package com.hci.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity model for view {@code VH_HOM_COMMODITY} from user {@code OWNER_INT}.
 * <p>
 * Not all column provided by this entity because the number of columns is too much.
 *
 * @author muhammad.muflihun
 *
 */

@Entity
@Table(schema = "OWNER_INT", name = "VH_HOM_COMMODITY")
public class OwnerIntVhHomCommodity {
    private Long version;
    private Long updatedBy;
    private Date updateDate;
    private String name;
    private Long id;
    private Date creationDate;
    private Long createdBy;
    private OwnerIntVhHomCommodityType commodityTypeId;
    private String code;

    @Column(name = "CODE")
    public String getCode() {
        return code;
    }

    @ManyToOne
    @JoinColumn(name = "COMMODITY_TYPE_ID", referencedColumnName = "ID")
    public OwnerIntVhHomCommodityType getCommodityTypeId() {
        return commodityTypeId;
    }

    @Column(name = "CREATED_BY")
    public Long getCreatedBy() {
        return createdBy;
    }

    @Column(name = "CREATION_DATE")
    public Date getCreationDate() {
        return creationDate;
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    @Column(name = "UPDATE_DATE")
    public Date getUpdateDate() {
        return updateDate;
    }

    @Column(name = "UPDATED_BY")
    public Long getUpdatedBy() {
        return updatedBy;
    }

    @Column(name = "VERSION")
    public Long getVersion() {
        return version;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCommodityTypeId(OwnerIntVhHomCommodityType commodityTypeId) {
        this.commodityTypeId = commodityTypeId;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "OwnerIntVhHomCommodity [version=" + version + ", updatedBy=" + updatedBy
                + ", updateDate=" + updateDate + ", name=" + name + ", id=" + id
                + ", creationDate=" + creationDate + ", createdBy=" + createdBy
                + ", commodityTypeId=" + commodityTypeId + ", code=" + code + "]";
    }

}
