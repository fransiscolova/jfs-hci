/**
 *
 */
package com.hci.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity model for view {@code VH_HOM_COMMODITY_TYPE} from user {@code OWNER_INT}.
 *
 * @author muhammad.muflihun
 *
 */

@Entity
@Table(schema = "OWNER_INT", name = "VH_HOM_COMMODITY_TYPE")
public class OwnerIntVhHomCommodityType implements Serializable {

    private static final long serialVersionUID = -2244333903771964886L;

    private Long version;
    private Long updatedBy;
    private Date updateDate;
    private BigDecimal taxRate;
    private String name;
    private Long id;
    private Date creationDate;
    private Long createdBy;
    private CommodityType commodityCategoryCode;
    private CommodityType code;
    private Integer activeFlag;

    @Column(name = "ACIVE_FLAG")
    public Integer getActiveFlag() {
        return activeFlag;
    }

    @ManyToOne
    @JoinColumn(name = "CODE", referencedColumnName = "HCI_CODE_COMMODITY_TYPE")
    public CommodityType getCode() {
        return code;
    }

    @ManyToOne
    @JoinColumn(name = "COMMODITY_CATEGORY_CODE", referencedColumnName = "HCI_CODE_COMMODITY_CATEGORY")
    public CommodityType getCommodityCategoryCode() {
        return commodityCategoryCode;
    }

    @Column(name = "CREATED_BY")
    public Long getCreatedBy() {
        return createdBy;
    }

    @Column(name = "CREATION_DATE")
    public Date getCreationDate() {
        return creationDate;
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    @Column(name = "TAX_RATE")
    public BigDecimal getTaxRate() {
        return taxRate;
    }

    @Column(name = "UPDATE_DATE")
    public Date getUpdateDate() {
        return updateDate;
    }

    @Column(name = "UPDATED_BY")
    public Long getUpdatedBy() {
        return updatedBy;
    }

    @Column(name = "VERSION")
    public Long getVersion() {
        return version;
    }

    public void setActiveFlag(Integer activeFlag) {
        this.activeFlag = activeFlag;
    }

    public void setCode(CommodityType code) {
        this.code = code;
    }

    public void setCommodityCategoryCode(CommodityType commodityCategoryCode) {
        this.commodityCategoryCode = commodityCategoryCode;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "OwnerIntVhHomCommodityType [version=" + version + ", updatedBy=" + updatedBy
                + ", updateDate=" + updateDate + ", taxRate=" + taxRate + ", name=" + name
                + ", id=" + id + ", creationDate=" + creationDate + ", createdBy=" + createdBy
                + ", commodityCategoryCode=" + commodityCategoryCode + ", code=" + code
                + ", activeFlag=" + activeFlag + "]";
    }

}
