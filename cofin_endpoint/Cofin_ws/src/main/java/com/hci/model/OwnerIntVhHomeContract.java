/**
 *
 */
package com.hci.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity model for view {@code VH_HOM_CONTRACT} from user {@code OWNER_INT}.
 *
 * @author muhammad.muflihun
 *
 */

@Entity
@Table(schema = "OWNER_INT", name = "VH_HOM_CONTRACT")
public class OwnerIntVhHomeContract implements Serializable {

    private static final long serialVersionUID = 3295590721825854722L;

    private OwnerIntVhHomCommodity id;
    private Long version;
    private Contract contractCode;
    private String dealCode;
    private Long productId;
    private Long salesroomId;
    private String salesAgentPhoneNumber;
    private String additionalInformation;
    private Date lastChangeDate;
    private Date preparedToSignDate;
    private Date sentToEvaluationDate;
    private Date sentToIndentifDate;
    private String internalCodeType;
    private String status;
    private String substatus;
    private Date creationDate;
    private Long createdBy;
    private Date updateDate;
    private Long updatedBy;
    private String contractProcessingType;
    private String internalCode2Type;
    private String registrationStatus;
    private String conditionsAccepted;
    private String salesComment;
    private String loanPurpose;
    private Integer preferredPaymentDay;
    private Long salesmanId;
    private Integer signedOnBackOffice;
    private Long dealId;
    private String paymentStatus;
    private String contractType;
    private String creditAccountNumber;
    private Integer xsellOfferRequested;
    private String xsellOfferCode;
    private String salesAreaCode;
    private Integer createdExternally;
    private Integer createdRemotely;
    private Long signedOnSalesroom;

    @Column(name = "ADDITIONAL_INFORMATION")
    public String getAdditionalInformation() {
        return additionalInformation;
    }

    @Column(name = "CONDITIONS_ACCEPTED")
    public String getConditionsAccepted() {
        return conditionsAccepted;
    }

    @ManyToOne
    @JoinColumn(name = "CONTRACT_CODE", referencedColumnName = "TEXT_CONTRACT_NUMBER")
    public Contract getContractCode() {
        return contractCode;
    }

    @Column(name = "CONTRACT_PROCESSING_TYPE")
    public String getContractProcessingType() {
        return contractProcessingType;
    }

    @Column(name = "CONTRACT_TYPE")
    public String getContractType() {
        return contractType;
    }

    @Column(name = "CREATED_BY")
    public Long getCreatedBy() {
        return createdBy;
    }

    @Column(name = "CREATED_EXTERNALLY")
    public Integer getCreatedExternally() {
        return createdExternally;
    }

    @Column(name = "CREATED_REMOTELY")
    public Integer getCreatedRemotely() {
        return createdRemotely;
    }

    @Column(name = "CREATION_DATE")
    public Date getCreationDate() {
        return creationDate;
    }

    @Column(name = "CREDIT_ACCOUNT_NUMBER")
    public String getCreditAccountNumber() {
        return creditAccountNumber;
    }

    @Column(name = "DEAL_CODE")
    public String getDealCode() {
        return dealCode;
    }

    @Column(name = "DEAL_ID")
    public Long getDealId() {
        return dealId;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "ID", referencedColumnName = "ID")
    public OwnerIntVhHomCommodity getId() {
        return id;
    }

    @Column(name = "INTERNAL_CODE2_TYPE")
    public String getInternalCode2Type() {
        return internalCode2Type;
    }

    @Column(name = "INTERNAL_CODE_TYPE")
    public String getInternalCodeType() {
        return internalCodeType;
    }

    @Column(name = "LAST_CHANGE_DATE")
    public Date getLastChangeDate() {
        return lastChangeDate;
    }

    @Column(name = "LOAN_PURPOSE")
    public String getLoanPurpose() {
        return loanPurpose;
    }

    @Column(name = "PAYMENT_STATUS")
    public String getPaymentStatus() {
        return paymentStatus;
    }

    @Column(name = "PREFERRED_PAYMENT_DAY")
    public Integer getPreferredPaymentDay() {
        return preferredPaymentDay;
    }

    @Column(name = "PREPARED_TO_SIGN_DATE")
    public Date getPreparedToSignDate() {
        return preparedToSignDate;
    }

    @Column(name = "PRODUCT_ID")
    public Long getProductId() {
        return productId;
    }

    @Column(name = "REGISTRATION_STATUS")
    public String getRegistrationStatus() {
        return registrationStatus;
    }

    @Column(name = "SALES_AGENT_PHONE_NUMBER")
    public String getSalesAgentPhoneNumber() {
        return salesAgentPhoneNumber;
    }

    @Column(name = "SALES_AREA_CODE")
    public String getSalesAreaCode() {
        return salesAreaCode;
    }

    @Column(name = "SALES_COMMENT")
    public String getSalesComment() {
        return salesComment;
    }

    @Column(name = "SALESMAN_ID")
    public Long getSalesmanId() {
        return salesmanId;
    }

    @Column(name = "SALESROOM_ID")
    public Long getSalesroomId() {
        return salesroomId;
    }

    @Column(name = "SENT_TO_EVALUATION_DATE")
    public Date getSentToEvaluationDate() {
        return sentToEvaluationDate;
    }

    @Column(name = "SENT_TO_IDENTIF_DATE")
    public Date getSentToIndentifDate() {
        return sentToIndentifDate;
    }

    @Column(name = "SIGNED_ON_BACK_OFFICE")
    public Integer getSignedOnBackOffice() {
        return signedOnBackOffice;
    }

    @Column(name = "SIGNED_ON_SALESROOM")
    public Long getSignedOnSalesroom() {
        return signedOnSalesroom;
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status;
    }

    @Column(name = "SUBSTATUS")
    public String getSubstatus() {
        return substatus;
    }

    @Column(name = "UPDATE_DATE")
    public Date getUpdateDate() {
        return updateDate;
    }

    @Column(name = "UPDATE_BY")
    public Long getUpdatedBy() {
        return updatedBy;
    }

    @Column(name = "VERSION")
    public Long getVersion() {
        return version;
    }

    @Column(name = "XSELL_OFFER_CODE")
    public String getXsellOfferCode() {
        return xsellOfferCode;
    }

    @Column(name = "XSELL_OFFER_REQUESTED")
    public Integer getXsellOfferRequested() {
        return xsellOfferRequested;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public void setConditionsAccepted(String conditionsAccepted) {
        this.conditionsAccepted = conditionsAccepted;
    }

    public void setContractCode(Contract contractCode) {
        this.contractCode = contractCode;
    }

    public void setContractProcessingType(String contractProcessingType) {
        this.contractProcessingType = contractProcessingType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedExternally(Integer createdExternally) {
        this.createdExternally = createdExternally;
    }

    public void setCreatedRemotely(Integer createdRemotely) {
        this.createdRemotely = createdRemotely;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setCreditAccountNumber(String creditAccountNumber) {
        this.creditAccountNumber = creditAccountNumber;
    }

    public void setDealCode(String dealCode) {
        this.dealCode = dealCode;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public void setId(OwnerIntVhHomCommodity id) {
        this.id = id;
    }

    public void setInternalCode2Type(String internalCode2Type) {
        this.internalCode2Type = internalCode2Type;
    }

    public void setInternalCodeType(String internalCodeType) {
        this.internalCodeType = internalCodeType;
    }

    public void setLastChangeDate(Date lastChangeDate) {
        this.lastChangeDate = lastChangeDate;
    }

    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public void setPreferredPaymentDay(Integer preferredPaymentDay) {
        this.preferredPaymentDay = preferredPaymentDay;
    }

    public void setPreparedToSignDate(Date preparedToSignDate) {
        this.preparedToSignDate = preparedToSignDate;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public void setRegistrationStatus(String registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public void setSalesAgentPhoneNumber(String salesAgentPhoneNumber) {
        this.salesAgentPhoneNumber = salesAgentPhoneNumber;
    }

    public void setSalesAreaCode(String salesAreaCode) {
        this.salesAreaCode = salesAreaCode;
    }

    public void setSalesComment(String salesComment) {
        this.salesComment = salesComment;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public void setSalesroomId(Long salesroomId) {
        this.salesroomId = salesroomId;
    }

    public void setSentToEvaluationDate(Date sentToEvaluationDate) {
        this.sentToEvaluationDate = sentToEvaluationDate;
    }

    public void setSentToIndentifDate(Date sentToIndentifDate) {
        this.sentToIndentifDate = sentToIndentifDate;
    }

    public void setSignedOnBackOffice(Integer signedOnBackOffice) {
        this.signedOnBackOffice = signedOnBackOffice;
    }

    public void setSignedOnSalesroom(Long signedOnSalesroom) {
        this.signedOnSalesroom = signedOnSalesroom;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSubstatus(String substatus) {
        this.substatus = substatus;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public void setXsellOfferCode(String xsellOfferCode) {
        this.xsellOfferCode = xsellOfferCode;
    }

    public void setXsellOfferRequested(Integer xsellOfferRequested) {
        this.xsellOfferRequested = xsellOfferRequested;
    }

    @Override
    public String toString() {
        return "OwnerIntVhHomeContract [id=" + id + ", version=" + version + ", contractCode="
                + contractCode + ", dealCode=" + dealCode + ", productId=" + productId
                + ", salesroomId=" + salesroomId + ", salesAgentPhoneNumber="
                + salesAgentPhoneNumber + ", additionalInformation=" + additionalInformation
                + ", lastChangeDate=" + lastChangeDate + ", preparedToSignDate="
                + preparedToSignDate + ", sentToEvaluationDate=" + sentToEvaluationDate
                + ", sentToIndentifDate=" + sentToIndentifDate + ", internalCodeType="
                + internalCodeType + ", status=" + status + ", substatus=" + substatus
                + ", creationDate=" + creationDate + ", createdBy=" + createdBy + ", updateDate="
                + updateDate + ", updatedBy=" + updatedBy + ", contractProcessingType="
                + contractProcessingType + ", internalCode2Type=" + internalCode2Type
                + ", registrationStatus=" + registrationStatus + ", conditionsAccepted="
                + conditionsAccepted + ", salesComment=" + salesComment + ", loanPurpose="
                + loanPurpose + ", preferredPaymentDay=" + preferredPaymentDay + ", salesmanId="
                + salesmanId + ", signedOnBackOffice=" + signedOnBackOffice + ", dealId=" + dealId
                + ", paymentStatus=" + paymentStatus + ", contractType=" + contractType
                + ", creditAccountNumber=" + creditAccountNumber + ", xsellOfferRequested="
                + xsellOfferRequested + ", xsellOfferCode=" + xsellOfferCode + ", salesAreaCode="
                + salesAreaCode + ", createdExternally=" + createdExternally + ", createdRemotely="
                + createdRemotely + ", signedOnSalesroom=" + signedOnSalesroom + "]";
    }

}
