/**
 *
 */
package com.hci.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for table {@code JFS_WS_PAPERLESS_POS}
 *
 * @author muhammad.muflihun
 *
 */
@Entity
@Table(name = "jfs_ws_paperless_pos")
public class PaperlessPos implements Serializable {
    private Long id;
    private String salesroomCode;
    private String salesroomName;
    private String partnerName;
    private String productCode;
    private Date validFrom;
    private Date validTo;

    @Id
    @Column(name = "ID", unique = true)
    public Long getId() {
        return id;
    }

    @Column(name = "PARTNER_NAME")
    public String getPartnerName() {
        return partnerName;
    }

    @Column(name = "PRODUCT_CODE")
    public String getProductCode() {
        return productCode;
    }

    @Column(name = "SALESROOM_CODE")
    public String getSalesroomCode() {
        return salesroomCode;
    }

    @Column(name = "SALESROOM_NAME")
    public String getSalesroomName() {
        return salesroomName;
    }

    @Column(name = "VALID_FROM")
    public Date getValidFrom() {
        return validFrom;
    }

    @Column(name = "VALID_TO")
    public Date getValidTo() {
        return validTo;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setSalesroomCode(String salesroomCode) {
        this.salesroomCode = salesroomCode;
    }

    public void setSalesroomName(String salesroomName) {
        this.salesroomName = salesroomName;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

}
