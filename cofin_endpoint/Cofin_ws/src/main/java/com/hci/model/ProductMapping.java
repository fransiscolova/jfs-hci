package com.hci.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jfs_product_mapping")
public class ProductMapping implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", length = 40)
    private String id;

    @Column(name = "CODE_PRODUCT", length = 30)
    private String codeProduct;

    @Column(name = "BANK_PRODUCT_CODE")
    private String bankProductCode;

    @Column(name = "VALID_FROM")
    private Timestamp validFrom;

    @Column(name = "VALID_TO")
    private Timestamp validTo;

    @Column(name = "ID_AGREEMENT", precision = 5, scale = 2)
    private BigDecimal idAgreement;

    @Column(name = "IS_DELETE")
    private String isDelete;

    @Column(name = "IS_DEFAULT")
    private String isDefault;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Timestamp createdDate;

    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @Column(name = "UPDATED_DATE")
    private Timestamp updaetdDate;

    public String getBankProductCode() {
        return bankProductCode;
    }

    public String getCodeProduct() {
        return codeProduct;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getIdAgreement() {
        return idAgreement;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public Timestamp getUpdaetdDate() {
        return updaetdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Timestamp getValidFrom() {
        return validFrom;
    }

    public Timestamp getValidTo() {
        return validTo;
    }

    public void setBankProductCode(String bankProductCode) {
        this.bankProductCode = bankProductCode;
    }

    public void setCodeProduct(String codeProduct) {
        this.codeProduct = codeProduct;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdAgreement(BigDecimal idAgreement) {
        this.idAgreement = idAgreement;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public void setUpdaetdDate(Timestamp updaetdDate) {
        this.updaetdDate = updaetdDate;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setValidFrom(Timestamp validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTo(Timestamp validTo) {
        this.validTo = validTo;
    }

    @Override
    public String toString() {
        return "Product Mapping [ID " + id + ", CODE_PRODUCT " + codeProduct
                + ", BANK_PRODUCT_CODE " + bankProductCode + ", VALID_FROM " + validFrom
                + ", VALID_TO " + validTo + ", ID_AGREEMENT " + idAgreement + ", IS_DELETE "
                + isDelete + ", IS_DEFAULT " + isDefault + ", CREATED_BY " + createdBy
                + ", CREATED_DATE " + createdDate + ", UPDATED_BY " + updatedBy + ", UPDATED_DATE "
                + updaetdDate + "]";
    }
}
