/**
 *
 */
package com.hci.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.List;

import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Commodity;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.hci.dao.impl.ProductDAOImpl;
import com.hci.dao.impl.ProductHciDAOImpl;
import com.hci.dao.impl.ProductMappingDAOImpl;
import com.hci.enumeration.CommodityCategoryBtpn;
import com.hci.enumeration.CommodityTypeBtpn;
import com.hci.enumeration.ServiceTypeCodeBtpn;
import com.hci.model.Product;
import com.hci.model.ProductHci;
import com.hci.model.ProductMapping;
import com.hci.util.ApplicationProperties;

/**
 * @author muhammad.muflihun
 *
 */
public class BtpnPartnershipService {
    private static final Logger LOG = LogManager.getLogger(BtpnPartnershipService.class);

    private static String btpnName;
    private static String btpnExcludeServiceCode;
    private static String[] btpnExcludeCommodityTypes;
    private static String mpfVirtualSalesroomCode;
    private static String mpfPaperlessSalesroomCode;
    private static BigDecimal btpnMpfLoanTop;
    private static BigDecimal btpnMpfLoanBot;
    private static Integer btpnMpfTenorTop;
    private static Integer btpnMpfTenorBot;

    @Autowired
    private ProductDAOImpl productDAO;
    @Autowired
    private ProductHciDAOImpl productHciDAO;
    @Autowired
    private ProductMappingDAOImpl productMappingDAO;

    public BtpnPartnershipService() {

    }

    /**
     * Get application properties here.
     *
     * @param applicationProperties
     */
    @Autowired
    public BtpnPartnershipService(ApplicationProperties applicationProperties) {
        btpnName = applicationProperties.getProperties("btpn.name");
        btpnExcludeServiceCode = applicationProperties.getProperties("btpn.exclude.service.code");
        btpnExcludeCommodityTypes = applicationProperties.getProperties(
                "btpn.exclude.commodity.types").split(",");
        mpfVirtualSalesroomCode = applicationProperties.getProperties("mpf.virtual.salesroom.code");
        mpfPaperlessSalesroomCode = applicationProperties
                .getProperties("mpf.paperless.salesroom.code");
        btpnMpfLoanTop = new BigDecimal(applicationProperties.getProperties("btpn.mpf.loan.top"));
        btpnMpfLoanBot = new BigDecimal(applicationProperties.getProperties("btpn.mpf.loan.bot"));
        btpnMpfTenorTop = Integer.parseInt(applicationProperties
                .getProperties("btpn.mpf.tenor.top"));
        btpnMpfTenorBot = Integer.parseInt(applicationProperties
                .getProperties("btpn.mpf.tenor.bot"));
    }

    /**
     * Business eligibility for BTPN. Return null value if one of eligibility criteria doesn't
     * match.
     *
     * @param request
     * @return
     * @throws Exception
     */
    public String btpnValidation(GetContractJFSPartnershipRequest request) throws Exception {
        String salesroomCode = request.getContract().getSalesroomCode();
        LOG.debug("salesroom code " + salesroomCode);
        if (salesroomCode.equals(mpfVirtualSalesroomCode)
                || salesroomCode.equals(mpfPaperlessSalesroomCode)) {
            return btpnValidationMPF(request);
        } else {
            return btpnValidationRegular(request);
        }
    }

    /**
     * Filter eligibility specifically for FlexiFast / MPF.
     *
     * @param request
     * @return
     * @throws Exception
     */
    private String btpnValidationMPF(GetContractJFSPartnershipRequest request) throws Exception {
        String contractNumber = request.getContract().getContractCode();
        LOG.info("btpn mpf validation for contract " + contractNumber);
        BigDecimal reqAmount = request.getContract().getOfferFinancialParameters()
                .getCreditAmount().getAmount();

        // Check product code is active
        ProductHci productHci = productHciDAO.getProductHciByCode(request.getProduct()
                .getProductCode());
        Product product = null;
        try {
            ProductMapping productMapping = productMappingDAO
                    .getActiveMappingByProductCodeAndBankProductCode(productHci.getCodeProduct(),
                            btpnName.concat("_PRODUCT").toUpperCase());
            product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
                    productMapping.getIdAgreement(), productMapping.getBankProductCode());
            if (product == null) {
                throw new Exception("Product is not mapped");
            }
        } catch (Exception e) {
            LOG.error("btpn mpf exception " + e);
            return null;
        }

        // Check max loan portion value
        BigDecimal partnerAmount = reqAmount.multiply(product.getPrincipalSplitRate()).setScale(0,
                RoundingMode.HALF_UP);
        if (partnerAmount.compareTo(btpnMpfLoanTop) > 0
                || partnerAmount.compareTo(btpnMpfLoanBot) < 0) {
            LOG.info("btpn amount " + partnerAmount + " not inside range " + btpnMpfLoanBot
                    + " and " + btpnMpfLoanTop);
            return null;
        }

        // Check max tenor
        Integer term = request.getContract().getOfferFinancialParameters().getTerms();
        if (term > btpnMpfTenorTop || term < btpnMpfTenorBot) {
            LOG.info("btpn terms " + term + " not inside range " + btpnMpfTenorBot + " and "
                    + btpnMpfTenorTop);
            return null;
        }

        // Check service type code
        List<net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service> services = request
                .getProduct().getServices();
        for (net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service service : services) {
            for (ServiceTypeCodeBtpn enu : ServiceTypeCodeBtpn.values()) {
                if (enu.name().equalsIgnoreCase(service.getServiceTypeCode())) {
                    LOG.info("btpn service " + enu.name());
                    return null;
                }
            }
        }

        // Check age between 21 and 65 when contract signed
        Calendar firstDueDate = request.getContract().getOfferFinancialParameters()
                .getFirstDueDate().toGregorianCalendar();
        // minus 1 month
        firstDueDate.add(Calendar.MONTH, -1);
        LocalDate signingContract = LocalDate.of(firstDueDate.get(Calendar.YEAR),
                firstDueDate.get(Calendar.MONTH) + 1, firstDueDate.get(Calendar.DAY_OF_MONTH));
        Calendar dob = request.getCustomer().getBirthDate().toGregorianCalendar();
        LocalDate dateOfBirth = LocalDate.of(dob.get(Calendar.YEAR), dob.get(Calendar.MONTH) + 1,
                dob.get(Calendar.DAY_OF_MONTH));
        Period period = Period.between(dateOfBirth, signingContract);
        if (period.getYears() < 21 || period.getYears() > 65) {
            LOG.info("btpn dob " + dateOfBirth + " compared to " + signingContract);
            return null;
        }

        // TODO : check KTP valid date
        // // Check KTP active more than 15 days
        // // Calendar ktp =
        // request.getCustomer().getPrimaryIDDocuments().get(0).getDocumentAttributes().get(0);
        // Calendar ktp = new GregorianCalendar();
        // LocalDate ktpActiveDate = LocalDate.of(ktp.get(Calendar.YEAR), ktp.get(Calendar.MONTH) +
        // 1,
        // ktp.get(Calendar.DAY_OF_MONTH));
        // period = Period.between(ktpActiveDate, today.plusDays(15));
        // if (period.getDays() < 0) {
        // return null;
        // }

        // TODO: check loan purpose

        return btpnName;
    }

    /**
     * Filter eligibility specifically for regular.
     *
     * @param request
     * @return
     * @throws Exception
     */
    private String btpnValidationRegular(GetContractJFSPartnershipRequest request) throws Exception {
        String contractNumber = request.getContract().getContractCode();
        LOG.info("btpn regular validation for contract " + contractNumber);
        BigDecimal reqAmount = request.getContract().getOfferFinancialParameters()
                .getCreditAmount().getAmount();

        // Check product code is active
        ProductHci productHci = productHciDAO.getProductHciByCode(request.getProduct()
                .getProductCode());
        Product product = null;
        try {
            ProductMapping productMapping = productMappingDAO
                    .getActiveMappingByProductCodeAndBankProductCode(productHci.getCodeProduct(),
                            btpnName.concat("_PRODUCT").toUpperCase());
            product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
                    productMapping.getIdAgreement(), productMapping.getBankProductCode());
            if (product == null) {
                throw new Exception("Product is not mapped");
            }
        } catch (Exception e) {
            LOG.error("btpn exception " + e);
            return null;
        }

        // Check commodity
        List<Commodity> commodities = request.getCommodity();
        if (commodities.size() > 1) {
            LOG.info("request has commodity size of " + commodities.size());
            return null;
        }

        // Check excluded commodity
        String commodityType = commodities.get(0).getCommodityTypeCode();
        for (String excludedCommodity : btpnExcludeCommodityTypes) {
            if (excludedCommodity.equals(commodityType)) {
                LOG.info("commodity type excluded for btpn " + commodityType);
                return null;
            }
        }

        // Check commodity to find terms and amount limit
        String commodityCategory = commodities.get(0).getCommodityCategoryCode();
        BigDecimal btpnAmountMin = BigDecimal.ZERO;
        BigDecimal btpnAmountMax = BigDecimal.ZERO;
        Integer btpnTenorMin = 0;
        Integer btpnTenorMax = 0;
        Boolean isSearching = true;
        if (isSearching) {
            for (CommodityCategoryBtpn enu : CommodityCategoryBtpn.values()) {
                if (enu.name().equalsIgnoreCase(commodityCategory)) {
                    LOG.info("commodity category matched " + enu.name());
                    isSearching = false;
                    btpnTenorMin = Integer.parseInt(enu.getTenorMin());
                    btpnTenorMax = Integer.parseInt(enu.getTenorMax());
                    btpnAmountMin = new BigDecimal(enu.getAmountMin());
                    btpnAmountMax = new BigDecimal(enu.getAmountMax());
                    LOG.info("tenor min " + btpnTenorMin);
                    LOG.info("tenor max " + btpnTenorMax);
                    LOG.info("amount min " + btpnAmountMin);
                    LOG.info("amount max " + btpnAmountMax);
                    break;
                }
            }
        }
        if (isSearching) {
            for (CommodityTypeBtpn enu : CommodityTypeBtpn.values()) {
                if (enu.name().equalsIgnoreCase(commodityType)) {
                    LOG.info("commodity type matched " + enu.name());
                    isSearching = false;
                    btpnTenorMin = Integer.parseInt(enu.getTenorMin());
                    btpnTenorMax = Integer.parseInt(enu.getTenorMax());
                    btpnAmountMin = new BigDecimal(enu.getAmountMin());
                    btpnAmountMax = new BigDecimal(enu.getAmountMax());
                    LOG.info("tenor min " + btpnTenorMin);
                    LOG.info("tenor max " + btpnTenorMax);
                    LOG.info("amoun min " + btpnAmountMin);
                    LOG.info("amount max " + btpnAmountMax);
                    break;
                }
            }
        }
        if (isSearching) {
            LOG.info("type or category didn't match");
            return null;
        }

        // Check max loan portion value
        BigDecimal partnerAmount = reqAmount.multiply(product.getPrincipalSplitRate()).setScale(0,
                RoundingMode.HALF_UP);
        if (partnerAmount.compareTo(btpnAmountMax) > 0
                || partnerAmount.compareTo(btpnAmountMin) < 0) {
            LOG.info("btpn amount " + partnerAmount + " not inside range " + btpnAmountMin
                    + " and " + btpnAmountMax);
            return null;
        }

        // Check max tenor
        Integer term = request.getContract().getOfferFinancialParameters().getTerms();
        if (term > btpnTenorMax || term < btpnTenorMin) {
            LOG.info("btpn terms " + term + " not inside range " + btpnTenorMin + " and "
                    + btpnTenorMax);
            return null;
        }

        // Check service
        List<net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service> services = request
                .getProduct().getServices();
        for (net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service service : services) {
            if (service.getServiceCode().equalsIgnoreCase(btpnExcludeServiceCode)) {
                LOG.info("btpn exclude service " + service.getServiceCode());
                return null;
            }
            for (ServiceTypeCodeBtpn enu : ServiceTypeCodeBtpn.values()) {
                if (enu.name().equalsIgnoreCase(service.getServiceTypeCode())) {
                    LOG.info("btpn service type " + enu.name());
                    return null;
                }
            }
        }

        // Check age between 21 and 65 when contract signed
        Calendar firstDueDate = request.getContract().getOfferFinancialParameters()
                .getFirstDueDate().toGregorianCalendar();
        // minus 1 month
        firstDueDate.add(Calendar.MONTH, -1);
        LocalDate signingContract = LocalDate.of(firstDueDate.get(Calendar.YEAR),
                firstDueDate.get(Calendar.MONTH) + 1, firstDueDate.get(Calendar.DAY_OF_MONTH));
        Calendar dob = request.getCustomer().getBirthDate().toGregorianCalendar();
        LocalDate dateOfBirth = LocalDate.of(dob.get(Calendar.YEAR), dob.get(Calendar.MONTH) + 1,
                dob.get(Calendar.DAY_OF_MONTH));
        Period period = Period.between(dateOfBirth, signingContract);
        if (period.getYears() < 21 || period.getYears() > 65) {
            LOG.info("btpn dob " + dateOfBirth + " compared to " + signingContract);
            return null;
        }

        request.getContract().getOfferFinancialParameters().getDownPayment().getAmount();

        // TODO : check KTP valid date
        // // Check KTP active more than 15 days
        // // Calendar ktp =
        // request.getCustomer().getPrimaryIDDocuments().get(0).getDocumentAttributes().get(0);
        // Calendar ktp = new GregorianCalendar();
        // LocalDate ktpActiveDate = LocalDate.of(ktp.get(Calendar.YEAR), ktp.get(Calendar.MONTH) +
        // 1,
        // ktp.get(Calendar.DAY_OF_MONTH));
        // period = Period.between(ktpActiveDate, today.plusDays(15));
        // if (period.getDays() < 0) {
        // return null;
        // }
        return btpnName;
    }

}
