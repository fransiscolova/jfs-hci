package com.hci.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hci.dao.AgreementDAO;
import com.hci.dao.ContractDAO;
import com.hci.dao.PartnerDAO;
import com.hci.dao.ProductDAO;
import com.hci.dao.ProductHciDAO;
import com.hci.dao.impl.AgreementDAOImpl;
import com.hci.dao.impl.ProductHciDAOImpl;
import com.hci.model.Agreement;
import com.hci.model.ProductHci;

public class PartnerService {
	@Autowired
	PartnerDAO partnerDAO;

	@Autowired
	ContractDAO contractDAO;
	
	@Autowired
	AgreementDAO agreementDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	ProductHciDAO productHciDAO;
	
	public String getPartnerAgreement(String productCode) throws Exception {
		Agreement agreement = new Agreement();
		
		List<String> lAgreementId = new ArrayList<String>();
		List<Agreement> lAgreement = agreementDAO.getActiveAgreement();
		for(Agreement agr : lAgreement) {
			lAgreementId.add(agr.getCode());
		}
		
		ProductHci pHci = (ProductHci) productHciDAO.getProductHciByCode(productCode);
		
		
		return pHci.getCodeProduct();
	}
}
