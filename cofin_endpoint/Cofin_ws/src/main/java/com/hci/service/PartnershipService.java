/**
 *
 */
package com.hci.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.ContractJFSPartnership;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;
import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hci.batch.CacheBatch;
import com.hci.dao.impl.AgreementDAOImpl;
import com.hci.dao.impl.ContractDAOImpl;
import com.hci.dao.impl.ContractEPDAOImpl;
import com.hci.dao.impl.ContractStatusDAOImpl;
import com.hci.dao.impl.DailyCacheDAOImpl;
import com.hci.dao.impl.NonJFSDAOImpl;
import com.hci.dao.impl.PaperlessPosDAOImpl;
import com.hci.dao.impl.PartnerDAOImpl;
import com.hci.dao.impl.ProductDAOImpl;
import com.hci.dao.impl.ProductHciDAOImpl;
import com.hci.dao.impl.ProductMappingDAOImpl;
import com.hci.enumeration.ProductProfileCode;
import com.hci.model.Agreement;
import com.hci.model.Contract;
import com.hci.model.ContractEP;
import com.hci.model.DailyCache;
import com.hci.model.NonJFS;
import com.hci.model.Partner;
import com.hci.model.Product;
import com.hci.model.ProductHci;
import com.hci.model.ProductMapping;
import com.hci.util.ApplicationProperties;

/**
 * Component class for implement business logic of partnership service endpoint.
 *
 * @author muhammad.muflihun
 *
 */
@Transactional
@Service("partnershipServiceMain")
public class PartnershipService {
	private static final Logger LOG = LogManager.getLogger(PartnershipService.class);
	private static String nonJfsName;
	private static String permataName;
	private static String btpnName;

	@Autowired
	private PartnerDAOImpl partnerDAO;
	@Autowired
	private ContractDAOImpl contractDAO;
	@Autowired
	private ContractEPDAOImpl contractEPDAO;
	@Autowired
	private AgreementDAOImpl agreementDAO;
	@Autowired
	private ProductDAOImpl productDAO;
	@Autowired
	private ProductHciDAOImpl productHciDAO;
	@Autowired
	private ProductMappingDAOImpl productMappingDAO;
	@Autowired
	private ContractStatusDAOImpl contractStatusDAO;
	@Autowired
	private DailyCacheDAOImpl dailyCacheDAO;
	@Autowired
	private PaperlessPosDAOImpl paperlessPosDAO;
	@Autowired
	private PermataPartnershipService permataService;
	@Autowired
	private BtpnPartnershipService btpnService;

	@Autowired
	private NonJFSDAOImpl nonJFSDAO;

	public PartnershipService() {

	}

	/**
	 * Get application properties here.
	 *
	 * @param applicationProperties
	 */
	@Autowired
	public PartnershipService(ApplicationProperties applicationProperties) {
		nonJfsName = applicationProperties.getProperties("non.jfs.name");
		permataName = applicationProperties.getProperties("permata.name");
		btpnName = applicationProperties.getProperties("btpn.name");
	}

	/**
	 * Convert date to XMLGregorian date.
	 *
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private XMLGregorianCalendar dateToXmlDate(Date date) throws DatatypeConfigurationException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
	}

	/**
	 * Generate response. Use BTPN by default if null.
	 *
	 * @param partnerCode
	 * @param agreementCode
	 * @param partnershipShare
	 * @param validFrom
	 * @param validTo
	 * @return response
	 */
	private GetContractJFSPartnershipResponse generateResponse(String partnerCode, String agreementCode,
			BigDecimal partnershipShare, XMLGregorianCalendar validFrom, XMLGregorianCalendar validTo) {
		// TODO : temporary solution
		partnerCode = partnerCode == null ? btpnName : partnerCode;
		partnershipShare = partnershipShare == null ? BigDecimal.ZERO : partnershipShare;
		GetContractJFSPartnershipResponse response = new GetContractJFSPartnershipResponse();
		ContractJFSPartnership contractJFSPartnership = new ContractJFSPartnership();
		contractJFSPartnership.setPartnerCode(partnerCode);
		contractJFSPartnership.setPartnershipAgreementCode(agreementCode);
		contractJFSPartnership.setPartnershipShare(partnershipShare);
		contractJFSPartnership.setValidFrom(validFrom);
		contractJFSPartnership.setValidTo(validTo);
		response.setContractFinancialPartnership(contractJFSPartnership);
		return response;
	}

	/**
	 * Process contract data to be determined which bank partner will we use as
	 * financing partner by eligibility criteria.
	 *
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GetContractJFSPartnershipResponse getContractJFSPartnershipRequest(GetContractJFSPartnershipRequest request)
			throws Exception {
		try {
			// check if contract is already exist
			String contractCode = request.getContract().getContractCode();
			ContractEP existContract = contractEPDAO.getContractByContractCode(contractCode);
			if (existContract != null) {
				LOG.debug("contract " + contractCode + " is already in jfs contract ep");
				Agreement agreement = existContract.getIdAgreement();
				Product product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
						new BigDecimal(agreement.getCode()), existContract.getBankProductCode());
				return generateResponse(agreement.getPartner().getName(), agreement.getCodeAgreement(),
						product.getPrincipalSplitRate(), dateToXmlDate(new Date(agreement.getValidFrom().getTime())),
						dateToXmlDate(new Date(agreement.getValidTo().getTime())));
			}

			// check if contract come from online profile
			String productProfile = request.getProduct().getProductProfileCode();
			if (productProfile.equalsIgnoreCase(ProductProfileCode.PP_ONLINE.name())) {
				LOG.debug("product profile is from online " + productProfile);
				return generateResponse(null, null, null, null, null);
			}

			// check if salesroom code and product is for paperless
			String salesroomCode = request.getContract().getSalesroomCode();
			String productCode = request.getProduct().getProductCode();
			if (paperlessPosDAO.getPaperlessPosBySalesroomCodeAndProductCode(salesroomCode, productCode,
					new Date()) != null) {
				LOG.debug("salesroom and product for paperless " + salesroomCode + " and " + productCode);
				return generateResponse(null, null, null, null, null);
			}

			String partnerName = splittingContractValidation(request);
			LOG.info("partner validation result is " + partnerName);

			if (partnerName.equalsIgnoreCase(nonJfsName)) {

				try {
					BigDecimal amtCredit = request.getContract().getOfferFinancialParameters().getCreditAmount().getAmount();
					BigDecimal amtAnuity = request.getContract().getOfferFinancialParameters().getAnnuity().getAmount();
					int tenor=request.getContract().getOfferFinancialParameters().getTerms();
					Date firstDueDate=request.getContract().getOfferFinancialParameters().getFirstDueDate().toGregorianCalendar().getTime();
					BigDecimal instalmentAmount=request.getContract().getOfferFinancialParameters().getInitialAmount().getAmount();
					
					
					Date dt = new Date();
					Calendar c = Calendar.getInstance();
					c.setTime(dt);
					c.add(Calendar.DATE, 1);
					dt = c.getTime();
										
					
					// save to table_non_jfs
					NonJFS nonJFS = new NonJFS();					
					nonJFS.setBankPartner(btpnName);
					nonJFS.setdTimeCreated(new Date());
					nonJFS.setTextContractNumber(request.getContract().getContractCode());
					nonJFS.setExportDate(dt);
					nonJFS.setSalesRoomCode(salesroomCode);
					nonJFS.setProductCode(productCode);
					nonJFS.setCreditAmount(amtCredit);
					nonJFS.setAnnuityAmount(amtAnuity);
					nonJFS.setTenor(tenor);
					nonJFS.setInstallmentAmount(instalmentAmount);
					nonJFS.setFisrtDueDate(firstDueDate);
					nonJFSDAO.saveOrUpdate(nonJFS);
					LOG.info("Success save to table nonjfs");
				} catch (Exception e) {
					// TODO: handle exception
					LOG.error("Erorr while save to non jfs");
					LOG.error(e.getMessage());
				}

				return generateResponse(null, null, null, null, null);
			}

			ProductHci productHci = productHciDAO.getProductHciByCode(request.getProduct().getProductCode());
			ProductMapping productMapping = productMappingDAO.getActiveMappingByProductCodeAndBankProductCode(
					productHci.getCodeProduct(), partnerName.concat("_PRODUCT").toUpperCase());
			Product product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
					productMapping.getIdAgreement(), productMapping.getBankProductCode());
			Agreement agreement = agreementDAO.getActiveAgreementByCode(product.getIdAgreement().toString());

			ContractEP contractEP = new ContractEP();
			// ContractStatus contractStatus = new ContractStatus();
			BigDecimal amtCredit = request.getContract().getOfferFinancialParameters().getCreditAmount().getAmount();
			int trm = request.getContract().getOfferFinancialParameters().getTerms();
			BigDecimal split = product.getPrincipalSplitRate();
			BigDecimal interest = product.getInterestRate();
			BigDecimal partnerAmount = amtCredit.multiply(split).setScale(0, RoundingMode.HALF_UP);

			MathContext mathContext = new MathContext(50);
			BigDecimal term = new BigDecimal(
					BigDecimal.ONE.add(interest.divide(new BigDecimal("1200"), mathContext)).pow(trm).toString());
			contractEP.setTextContractNumber(request.getContract().getContractCode());
			contractEP.setStatus("X");
			// set export date to be tomorrow
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DATE, 1);
			contractEP.setExportDate(cal.getTime());
			contractEP.setSkpContract(BigDecimal.ZERO);
			contractEP.setSendPrincipal(partnerAmount);
			// change formula if term equals one
			if (term.equals(BigDecimal.ONE)) {
				contractEP.setSendInstalment(
						partnerAmount.multiply(interest.divide(new BigDecimal("1200"), mathContext).multiply(term))
								.divide(BigDecimal.ONE).setScale(0, 4));
			} else {
				contractEP.setSendInstalment(
						partnerAmount.multiply(interest.divide(new BigDecimal("1200"), mathContext).multiply(term))
								.divide(term.subtract(BigDecimal.ONE), mathContext).setScale(0, 4));
			}
			contractEP.setSendTenor(new BigDecimal(trm));
			contractEP.setSendRate(interest);
			contractEP.setDateFirstDue(request.getContract().getOfferFinancialParameters().getFirstDueDate()
					.toGregorianCalendar().getTime());
			contractEP.setAmtInstalment(request.getContract().getOfferFinancialParameters().getAnnuity().getAmount());
			contractEP.setAmtMonthlyfee(BigDecimal.ZERO);
			contractEP.setIdAgreement(agreement);
			contractEP.setBankProductCode(productMapping.getBankProductCode());
			contractEP.setCuid(request.getCustomer().getClientCuid());

			// contractStatus.setTextContractNumber(request.getContract().getContractCode());
			// contractStatus.setDtimeCreated(new Date());
			// contractStatus.setCntDpd(BigDecimal.ZERO);
			// contractStatus.setCodeStatus("X");
			// contractStatus.setCreatedBy("cofin_ws");
			// contractStatus.setId(UUID.randomUUID().toString().replace("-", ""));
			contractEPDAO.save(contractEP);
			// contractStatusDAO.save(contractStatus);

			// update cache for JFS contract
			if (!partnerName.equals(nonJfsName)) {
				DailyCache dailyCache = dailyCacheDAO.getDailyCacheByPartnerName(partnerName);
				BigDecimal updatedLimit = dailyCache.getTotalLoan().add(partnerAmount);
				dailyCache.setTotalLoan(updatedLimit);
				dailyCache.setDailyLoan(dailyCache.getDailyLoan().add(partnerAmount));
				dailyCacheDAO.saveOrUpdate(dailyCache);
				CacheBatch.sendWarningEmail(partnerName, updatedLimit);
			}
			return generateResponse(agreement.getPartner().getName(), agreement.getCodeAgreement(),
					product.getPrincipalSplitRate(), dateToXmlDate(new Date(agreement.getValidFrom().getTime())),
					dateToXmlDate(new Date(agreement.getValidTo().getTime())));
		} catch (Exception e) {
			LOG.error("main exception is" + e);
		}
		return generateResponse(null, null, null, null, null);
	}

	/**
	 * Validate business criteria to determine {@link Contract} will using certain
	 * {@link Partner} or not.
	 * <p>
	 * Priority of checking partner is stored in {@link Partner} and a flag about
	 * whether if system must check the partner criteria.
	 *
	 * @param request
	 * @return partnerName
	 * @throws Exception
	 */
	private String splittingContractValidation(GetContractJFSPartnershipRequest request) throws Exception {
		String partnerName;
		List<Partner> partners = partnerDAO.getAllActivePartnerOrderedByPriority();
		for (Partner partner : partners) {
			if (partner.getName().equalsIgnoreCase(permataName)) {
				LOG.debug("Run Validation permata");
				partnerName = permataService.permataValidation(request);
				if (partnerName != null) {
					return partnerName;
				}
			} else if (partner.getName().equalsIgnoreCase(btpnName)) {
				LOG.debug("Run Validation btpn");
				partnerName = btpnService.btpnValidation(request);
				if (partnerName != null) {
					return partnerName;
				}
			}
		}

		LOG.debug("return " + nonJfsName);
		return nonJfsName;
	}
}
