/**
 *
 */
package com.hci.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.List;

import net.homecredit.homerselect.ws.financialpartnership.partnership.v2.GetContractJFSPartnershipRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.hci.dao.CommodityExcludeDao;
import com.hci.dao.impl.CommodityExcludeDAOImpl;
import com.hci.dao.impl.ContractDAOImpl;
import com.hci.dao.impl.DailyCacheDAOImpl;
import com.hci.dao.impl.NonJFSDAOImpl;
import com.hci.dao.impl.ProductDAOImpl;
import com.hci.dao.impl.ProductHciDAOImpl;
import com.hci.dao.impl.ProductMappingDAOImpl;
import com.hci.enumeration.CommodityCategoryPermata;
import com.hci.enumeration.ServiceTypeCodePermata;
import com.hci.model.ComodityExclude;
import com.hci.model.DailyCache;
import com.hci.model.Product;
import com.hci.model.ProductHci;
import com.hci.model.ProductMapping;
import com.hci.util.ApplicationProperties;

/**
 * @author muhammad.muflihun
 *
 */
public class PermataPartnershipService {
    private static final Logger LOG = LogManager.getLogger(PermataPartnershipService.class);

    private static String permataName,btpnName;
    private static BigDecimal permataLoanMax;
    private static BigDecimal permataLoanTop;
    private static String statusActive;
    private static String mpfSalesroomCode;

    @Autowired
    private ProductDAOImpl productDAO;
    
    @Autowired
    private ContractDAOImpl contractDAO;
    
    @Autowired
    private ProductHciDAOImpl productHciDAO;
    
    @Autowired
    private ProductMappingDAOImpl productMappingDAO;
    
    @Autowired
    private DailyCacheDAOImpl dailyCacheDAO;
    
    @Autowired
    private CommodityExcludeDAOImpl commodityExcludeDao;
    
    @Autowired
    private NonJFSDAOImpl nonJFSDAO;

    public PermataPartnershipService() {

    }

    /**
     * Get application properties here.
     *
     * @param applicationProperties
     */
    @Autowired
    public PermataPartnershipService(ApplicationProperties applicationProperties) {
        permataLoanMax = new BigDecimal(applicationProperties.getProperties("permata.loan.max"));
        permataLoanTop = new BigDecimal(applicationProperties.getProperties("permata.loan.top"));
        permataName = applicationProperties.getProperties("permata.name");
        btpnName = applicationProperties.getProperties("btpn.name");
        statusActive = applicationProperties.getProperties("status.active");
        mpfSalesroomCode = applicationProperties.getProperties("mpf.salesroom.code");
    }

    /**
     * Calculate dp amount from request amount and dp percentage.
     * 
     * @param reqAmount
     * @param percentage
     * @return dpAmount
     */
    private BigDecimal percentCalculation(BigDecimal reqAmount, BigDecimal percentage) {
        return reqAmount.multiply(percentage).divide(new BigDecimal("100"));
    }

    /**
     * Business eligibility for Permata. Return null value if one of eligibility criteria doesn't
     * match.
     *
     * @param request
     * @return
     * @throws Exception
     */
    public String permataValidation(GetContractJFSPartnershipRequest request) throws Exception {
        String contractNumber = request.getContract().getContractCode();
        LOG.info("permata validation for contract " + contractNumber);
        BigDecimal reqAmount = request.getContract().getOfferFinancialParameters()
                .getGoodsPrice().getAmount();
        
        
        // Check salesroom code
        String salesroomCode = request.getContract().getSalesroomCode();
        if (salesroomCode.equals(mpfSalesroomCode)) {
            LOG.info("salesroom code for mpf " + salesroomCode);
            return null;
        }

        // Check daily limit
        DailyCache dailyCache = dailyCacheDAO.getDailyCacheByPartnerName(permataName);
        if (dailyCache.getDailyLoan().add(reqAmount).compareTo(dailyCache.getDailyLimit()) > 0) {
            LOG.info("permata daily limit is reached");
            return null;
        }
        
       
        //exclude commodity
        List<ComodityExclude> comodityExclude=commodityExcludeDao.getExcludeComodity(permataName,request.getCommodity().get(0).getCommodityCategoryCode());        
        LOG.info("TOTAL EXCLUDE " + String.valueOf(comodityExclude.size()));        
        if(comodityExclude.size()>0) {
        	 LOG.info("permata exclude ");
        	 return null;
        }

        
        // Check product is active
        ProductHci productHci = productHciDAO.getProductHciByCode(request.getProduct()
                .getProductCode());
        Product product = null;
        try {
            ProductMapping productMapping = productMappingDAO
                    .getActiveMappingByProductCodeAndBankProductCode(productHci.getCodeProduct(),
                            permataName.concat("_PRODUCT").toUpperCase());
            product = productDAO.getActiveProductByAgreementIdAndBankProductCode(
                    productMapping.getIdAgreement(), productMapping.getBankProductCode());
            if (product == null) {
                throw new Exception("Product is not mapped");
            }
        } catch (Exception e) {
            LOG.error("permata exceptions " + e);
            return null;
        }

        // Check max loan portion value
        String cuid = request.getCustomer().getClientCuid();
        BigDecimal partnerAmount = reqAmount.multiply(product.getPrincipalSplitRate()).setScale(0,
                RoundingMode.HALF_UP);
        BigDecimal existingLoan = contractDAO.getTotalProposedLoanByStatusAndCuid(statusActive,
                cuid).add(partnerAmount);
        if (existingLoan.compareTo(permataLoanTop) > 0) {
            LOG.info("customer with " + cuid + " total amount is " + existingLoan + " more than "
                    + permataLoanTop);
            return null;
        }

        // Check max total loan
        // BigDecimal partnerLimit = CacheBatch.addPartnerLimitCache(permataName, partnerAmount);
        BigDecimal partnerLimit = dailyCacheDAO.getDailyCacheByPartnerName(permataName)
                .getTotalLoan().add(partnerAmount);
        if (partnerLimit.compareTo(permataLoanMax) == 1) {
            LOG.info("permata total loan " + partnerLimit + " more than " + permataLoanMax);
            return null;
        }

        // TODO : refractor this to read multiple commodities.
        // Check commodity
        String commodityCategoryCode = request.getCommodity().get(0).getCommodityCategoryCode();
        CommodityCategoryPermata category = CommodityCategoryPermata.valueOf(commodityCategoryCode);

        // Check min down payment per category
        BigDecimal downPayment = request.getContract().getOfferFinancialParameters()
                .getDownPayment().getAmount();
        String categoryDP = category.getDownPayment();
        BigDecimal dpAmount = percentCalculation(reqAmount, new BigDecimal(categoryDP));
        
        LOG.info("Downpayment " + downPayment + " jumlah persen dp " + dpAmount);
        if (categoryDP != null && !categoryDP.isEmpty() && downPayment.compareTo(dpAmount) < 0) {
            LOG.info("permata dp " + downPayment + " lesser than " + dpAmount);
            return null;
        }

        // Check max tenor per category
        String categoryTenor = category.getTenor();
        BigDecimal tenor = BigDecimal.valueOf(request.getContract().getOfferFinancialParameters()
                .getTerms());
        if (categoryTenor != null && !categoryTenor.isEmpty()
                && tenor.compareTo(new BigDecimal(categoryTenor)) > 0) {
            LOG.info("permata terms " + tenor + " more than " + categoryTenor);
            return null;
        }

        // Check service type code
        List<net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service> services = request
                .getProduct().getServices();
        for (net.homecredit.homerselect.ws.financialpartnership.partnership.v2.Service service : services) {
            for (ServiceTypeCodePermata enu : ServiceTypeCodePermata.values()) {
                if (enu.name().equalsIgnoreCase(service.getServiceTypeCode())) {
                    LOG.info("permata service " + enu.name());
                    return null;
                }
            }
        }

        // Check age between 21 and 65 when contract signed
        Calendar firstDueDate = request.getContract().getOfferFinancialParameters()
                .getFirstDueDate().toGregorianCalendar();
        // minus 1 month
        firstDueDate.add(Calendar.MONTH, -1);
        LocalDate signingContract = LocalDate.of(firstDueDate.get(Calendar.YEAR),
                firstDueDate.get(Calendar.MONTH) + 1, firstDueDate.get(Calendar.DAY_OF_MONTH));
        Calendar dob = request.getCustomer().getBirthDate().toGregorianCalendar();
        LocalDate dateOfBirth = LocalDate.of(dob.get(Calendar.YEAR), dob.get(Calendar.MONTH) + 1,
                dob.get(Calendar.DAY_OF_MONTH));
        Period period = Period.between(dateOfBirth, signingContract);
        if (period.getYears() < 21 || period.getYears() > 65) {
            LOG.info("permata dob " + dateOfBirth + " compared to " + signingContract);
            return null;
        }
        
        
        //check is it already as non jfs       
        LOG.info("check table jfs");
        if(nonJFSDAO.isContractExist(btpnName, request.getContract().getContractCode()).size()>0){
         LOG.info("already define on table non jfs " + request.getContract().getContractCode());
       	 return null;
        }
        
        // TODO : check KTP valid date
        // // Check KTP active more than 15 days
        // // Calendar ktp =
        // request.getCustomer().getPrimaryIDDocuments().get(0).getDocumentAttributes().get(0);
        // Calendar ktp = new GregorianCalendar();
        // LocalDate ktpActiveDate = LocalDate.of(ktp.get(Calendar.YEAR), ktp.get(Calendar.MONTH) +
        // 1,
        // ktp.get(Calendar.DAY_OF_MONTH));
        // period = Period.between(ktpActiveDate, today.plusDays(15));
        // if (period.getDays() < 0) {
        // LOG.debug("permata ktp " +partnerAmount+ " more than " +permataLoanTop);
        // return null;
        // }

        return permataName;
    }

}
