package com.hci.ws;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import net.homecredit.ws.commoditymanufacturer.v1.ResultCodeType;
import net.homecredit.ws.commoditymanufacturer.v1.ValidateCommodityRequest;
import net.homecredit.ws.commoditymanufacturer.v1.ValidateCommodityResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.hci.dao.impl.TacDAOImpl;
import com.hci.enumeration.TacExternalCode;
import com.hci.model.Tac;

/**
 * The Class End-point Service of Commodity.
 */
@Endpoint
public class CommodityServiceEndpoint {
    private static final Logger LOG = LogManager.getLogger(CommodityServiceEndpoint.class);
    private static final String TARGET_NAMESPACE = "http://homecredit.net/ws/commodityManufacturer/v1";

    private ValidateCommodityResponse response = new ValidateCommodityResponse();

    @Autowired
    TacDAOImpl tacDAO;

    /**
     * Generate Validate Commodity Response output.
     *
     * @param note
     * @param externalCode
     * @param resultCodeType
     */
    private void generateOutgoingResponse(String note, String externalCode,
            ResultCodeType resultCodeType) {
        response.setNote(note);
        response.setExternalCode(externalCode);
        response.setResultCode(resultCodeType);
    }

    /**
     * Main service to validate IMEI from Commodity Manufacture.
     *
     * @param validateCommodityRequest
     * @param validateCommodityResponse
     * @throws JAXBException
     */
    @PayloadRoot(localPart = "ValidateCommodityRequest", namespace = TARGET_NAMESPACE)
    public @ResponsePayload ValidateCommodityResponse getAccountDetails(
            @RequestPayload ValidateCommodityRequest request) throws JAXBException {
        try {
            printRequest(request);
            String imei = request.getIMEI();
            if (imei == null || imei.equals("")) {
                generateOutgoingResponse(TacExternalCode.EM.description(),
                        TacExternalCode.EM.externalCode(), ResultCodeType.ERROR);
                printResponse(response);
                return response;
            } else if (imei.length() > 255) {
                throw new IllegalArgumentException("IMEI is too long!");
            }

            // TODO: decrypt IMEI before substring if needed & fix return detail.
            // TAC validation
            String tacNumber = imei.substring(0, 6);
            List<Tac> tacList = tacDAO.getTacByTacNumber(tacNumber);
            if (tacList == null || tacList.isEmpty()) {
                generateOutgoingResponse(TacExternalCode.NF.description(),
                        TacExternalCode.NF.externalCode(), ResultCodeType.NOT_VALIDATED);
            } else {
                // find active tac using today's date
                Date today = new Date();
                Boolean isActive = false;
                for (Tac tac : tacList) {
                    if (tac.getValidFrom().compareTo(today) < 1
                            && today.compareTo(tac.getValidTo()) < 1) {
                        isActive = true;
                        break;
                    }
                }
                if (isActive) {
                    generateOutgoingResponse(TacExternalCode.AC.description(),
                            TacExternalCode.AC.externalCode(), ResultCodeType.VALIDATED);
                } else {
                    generateOutgoingResponse(TacExternalCode.IA.description(),
                            TacExternalCode.IA.externalCode(), ResultCodeType.NOT_VALIDATED);
                }
            }
        } catch (Exception e) {
            generateOutgoingResponse(e.toString(), TacExternalCode.ER.externalCode(),
                    ResultCodeType.ERROR);
        }
        printResponse(response);
        return response;
    }

    /**
     * Print request to log.
     *
     * @param request
     * @return
     * @throws JAXBException
     */
    private void printRequest(ValidateCommodityRequest request) throws JAXBException {
        StringWriter sw = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(ValidateCommodityRequest.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(request, sw);
        LOG.debug("incoming request : " + sw.toString());
    }

    /**
     * Print response to log.
     *
     * @param request
     * @return
     * @throws JAXBException
     */
    private void printResponse(ValidateCommodityResponse response) throws JAXBException {
        StringWriter sw = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(ValidateCommodityResponse.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(response, sw);
        LOG.debug("outgoing response : " + sw.toString());
    }
}